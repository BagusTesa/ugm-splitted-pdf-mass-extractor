/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamabox.splittedpdfmassextractor.processor;
import java.io.IOException;
import java.io.File;
import org.apache.pdfbox.io.RandomAccessFile;
import org.apache.pdfbox.pdfparser.PDFParser;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.text.PDFTextStripper;
import org.apache.pdfbox.tools.PDFText2HTML;

/**
 * <p>Handles extracting a pdf into html and passes it to {@link Processor} class</p>
 * @author Tezla
 */
public class PDFExtractor {
    /**
     * <p>Output Plain Text.</p>
     * 
     * @param path
     * @return
     */
    public String fetchPlain(String path){
        String out = "";
        File f = new File(path);
        try{
            RandomAccessFile file = new RandomAccessFile(f, "r");
            PDFTextStripper txStrip = new PDFTextStripper();
            
            PDFParser parser = new PDFParser(file);
                parser.parse();
            PDDocument pd = parser.getPDDocument();
            out = txStrip.getText(pd);
            
            pd.close();
        }
        catch(IOException ex){
            
        }
        return out;
    }
    
    @Deprecated
    public String fetchHtml(String path){
        String out = "";
        File f = new File(path);
        try{
            RandomAccessFile file = new RandomAccessFile(f, "r");
            PDFText2HTML htmlizer = new PDFText2HTML();
            
            PDFParser parser = new PDFParser(file);
                parser.parse();
            PDDocument pd = parser.getPDDocument();
            out = htmlizer.getText(pd);
            
            pd.close();
        }
        catch(IOException ex){
            
        }
        return out;
    }
    
}
