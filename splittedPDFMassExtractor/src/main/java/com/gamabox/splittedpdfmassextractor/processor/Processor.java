/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamabox.splittedpdfmassextractor.processor;
import java.util.Queue;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.gamabox.splittedpdfmassextractor.crawler.Entity;
import com.gamabox.splittedpdfmassextractor.cleanser.Cleanser;

/**
 * <p>Process the {@link Queue}.</p>
 * @author Tezla
 */
public class Processor {
    private static final Log LOG = LogFactory.getLog(Processor.class);
    
    private final Cleanser cl = new Cleanser();
    private final OutputWriter ow = new OutputWriter();
    private final PDFExtractor pde = new PDFExtractor();
    
    public void processAsPlain(Queue<Entity> que, boolean cleanse, boolean stripNewLine){
        for(Entity ent: que){
            LOG.info(ent.getDirName());
            
            String out = "";
            for(String tgt: ent.getFileTarget()){
                String in = this.pde.fetchPlain(tgt);
                if(cleanse){
                    out += cl.cleanPlain(in);
                }
                else{
                    out += in;
                }
                
                if(stripNewLine){
                    out += " ";
                }
                else{
                    out += System.getProperty("line.separator");
                }
            }
            this.ow.output(ent.getDirName(), out);
        }
    }
    
    public String getOutputPath(){
        return this.ow.getOutputPath();
    }
    
    public void setOutputPath(String output){
        if(output != null){
            this.ow.setOutputPath(output);
        }
    }
    
}
