/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamabox.splittedpdfmassextractor.processor;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * <p>Writes thing into given path.</p>
 * @author Tezla
 */
public class OutputWriter {
    private String outputPath = "output";
    
    public void output(String name, String output){
        try {
            PrintWriter wr = new PrintWriter(outputPath + "\\" + name + ".txt");
            wr.write(output);
            wr.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(OutputWriter.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }

    /**
     * @return the outputPath
     */
    public String getOutputPath() {
        return outputPath;
    }

    /**
     * @param outputPath the outputPath to set
     */
    public void setOutputPath(String outputPath) {
        this.outputPath = outputPath;
    }
}
