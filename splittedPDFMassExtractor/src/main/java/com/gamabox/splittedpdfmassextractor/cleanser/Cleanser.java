/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamabox.splittedpdfmassextractor.cleanser;
import java.util.LinkedList;
import java.util.List;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.jsoup.nodes.Document;
import org.jsoup.Jsoup;

/**
 * <p>It's cleansing the w- documents. Stripping NIM, page number (including Roman numerals) using <i>
 * Heuristic</i> approach.</p>
 * 
 * @author Tezla
 */
public class Cleanser {
    private String nim = "\\s*[\\p{N}]+\\s*/\\s*[\\p{N}]+\\s*/\\s*[\\p{L}]+\\s*/\\s*[\\p{N}]+\\s*";
    private String noisyNim = "\\s*[^\\p{L}\\p{N}]*" + this.nim + "[^\\p{L}\\p{N}]*\\s*";
    private String number = "\\p{N}+";
    private String romanNumeral = "m{0,4}(cm|cd|d?c{0,3})(xc|xl|l?x{0,3})(ix|iv|v?i{0,3})(ix|iv|v?i{0,3})";
    private String capitalCase = "(\\s*(\\b(\\p{Lu}+)\\b)\\s*)+";
    private String lowerCase = "(\\s*(\\b(\\p{Ll}+)\\b)\\s*)+";
    private String titleCase = "(\\s*(\\b(\\p{Lu}[\\p{Ll}|\\p{Lu}]+)\\b)\\s*)+";
    private String subChapterMarker = "([\\pN|\\pL]+\\.)+\\s*" + "(" + this.titleCase + "|" + this.capitalCase + ")";
    
    /**
     * <p>Adds ^ at the beginning and $ at the end of {@param regex} if does
     * not exist.</p>
     * 
     * @param regex
     * @return
     */
    protected String ensureStrBeginEnd(String regex){
        String out = regex;
        
        if(!out.startsWith("^")){
            out = "^" + out;
        }
        
        if(!out.endsWith("$")){
            out += "$";
        }
        
        return out;
    }
    
    private boolean isNim(String in){
        return in.matches(this.ensureStrBeginEnd(this.getNoisyNim()));
    }
    
    private boolean isNumber(String in){
        return in.matches(this.ensureStrBeginEnd(this.getNumber()));
    }
    
    private boolean isRomanNumeral(String in){
        return in.toLowerCase().matches(this.ensureStrBeginEnd(this.getRomanNumeral()));
    }
    
    private boolean isAllCapital(String in){
        return in.replaceAll("[^\\p{L}\\s]", " ").matches(this.ensureStrBeginEnd(this.getCapitalCase()));
    }
    
    private boolean isAllLower(String in){
        return in.replaceAll("[^\\p{L}\\s]", " ").matches(this.ensureStrBeginEnd(this.getLowerCase()));
    }
    
    private boolean isTitleCase(String in){
        return in.replaceAll("[^\\p{L}\\s]", "").matches(this.ensureStrBeginEnd(this.getTitleCase()));
    }
    
    protected boolean isSubChapterMarker(String in){
        return in.matches(this.ensureStrBeginEnd(this.getSubChapterMarker()));
    }
    
    public String cleanPlain(String text){
        String out = "";
        
        if((text != null) && (text.length() > 0)){
            String[] tmp = text.split("\\n");
            List<String> _tmp = new LinkedList<>();
            
            //weed out empty lines
            for(int i = 0; i < tmp.length; i++){
                String _t = tmp[i].trim();
                if(_t.length() > 0){
                    _tmp.add(_t);
                }
            }
            //parse, checks for unused part (trying to take paragraph only
            int tenLastIdx = 0;
            if(_tmp.size() > 10){
                tenLastIdx = _tmp.size() - 10;
            }
            for(int i = 0; i < _tmp.size(); i++){
                String _t = _tmp.get(i);
                if(!isNumber(_t) && !isRomanNumeral(_t) && !isNim(_t) && !isAllCapital(_t) && !isTitleCase(_t)){
                    //for each of 10-first item, check whether it's a number or roman numeral (page number suspect), all capitalized or all lower (for title, by, abstract, whatever label we might find)
                    out += _t;
                    out += System.getProperty("line.separator");
                }
            }
        }
        
        return out;
    }

    /**
     * @return the nim
     */
    public String getNim() {
        return nim;
    }

    /**
     * @return the noisyNim
     */
    public String getNoisyNim() {
        return noisyNim;
    }

    /**
     * @return the number
     */
    public String getNumber() {
        return number;
    }

    /**
     * @return the romanNumeral
     */
    public String getRomanNumeral() {
        return romanNumeral;
    }

    /**
     * @return the capitalCase
     */
    public String getCapitalCase() {
        return capitalCase;
    }

    /**
     * @return the lowerCase
     */
    public String getLowerCase() {
        return lowerCase;
    }

    /**
     * @return the titleCase
     */
    public String getTitleCase() {
        return titleCase;
    }

    /**
     * @return the subChapterMarker
     */
    public String getSubChapterMarker() {
        return subChapterMarker;
    }
    
}
