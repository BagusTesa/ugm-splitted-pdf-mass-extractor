/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamabox.splittedpdfmassextractor.crawler;
import java.util.List;
import java.util.LinkedList;

/**
 * <p>At most store one research, however there's a possibility for one directory contains
 * more than single research pack - two or more with identical title being crawled.</p>
 * 
 * @author Tezla
 */
public class Entity {
    private final String dirName;
    private final List<String> fileTarget;
    
    public Entity(String dirName){
        this.dirName = dirName;
        this.fileTarget = new LinkedList<>();
    }
    
    @Override
    public String toString(){
        String out = this.dirName + "(" + fileTarget.size() + ")";
        
        for(String f: fileTarget){
            out += "\n" + f;
        }
        
        return out;
    }
    
    /**
     * @return the dirName
     */
    public String getDirName() {
        return dirName;
    }

    /**
     * @return the fileTarget
     */
    public List<String> getFileTarget() {
        return fileTarget;
    }
    
}
