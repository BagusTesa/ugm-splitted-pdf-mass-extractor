/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.gamabox.splittedpdfmassextractor.crawler;
import java.util.Set;
import java.util.Queue;
import java.util.LinkedList;
import java.util.LinkedHashSet;
import java.io.File;

/**
 * <p>Crawl directories and discern multiple
 * report in single title. This crawler assume it'll crawl an ETD Walker output
 * directory.</p>
 * 
 * @author Tezla
 */
public class Crawler {
    private final Set<String> pickList = new LinkedHashSet<>();
    private final String separator = "-";
    
    public Crawler(){
        pickList.add("chapter1.pdf");pickList.add("chapter5.pdf");
        pickList.add("introduction.pdf");pickList.add("conclusion.pdf");
    }
    
    public Crawler(Set<String> pickList){
        for(String pick: pickList){
            //ensure its a lower-cased
            this.pickList.add(pick.toLowerCase());
        }
    }
    
    private Queue<Entity> discern(String path){
        Queue<Entity> out = new LinkedList<>();
        
        File accessor = new File(path);
        
        if(accessor.isDirectory()){
            String dirName = accessor.getName();
            File[] lf = accessor.listFiles();
            String lastPattern = null;
            Entity lastEntity = null;
            
            int itera = 0;
            for(File f: lf){
                String fpth = f.getAbsolutePath();
                String fname = f.getName();
                
                String newPattern = fname.substring(0, fname.lastIndexOf(this.separator)).toLowerCase();
                String flTitle = fname.substring(fname.lastIndexOf(this.separator) + 1).toLowerCase(); //beware of extension or file type
                
                if(lastPattern == null){
                    //seeding
                    lastPattern = newPattern;
                    lastEntity = new Entity(dirName);
                    
                    if(this.pickList.contains(flTitle)){
                        lastEntity.getFileTarget().add(fpth);
                    }
                }
                else{
                    //the following foo-bar
                    if(lastPattern.equalsIgnoreCase(newPattern)){
                        //still the same pattern
                        if(this.pickList.contains(flTitle)){
                            lastEntity.getFileTarget().add(fpth);
                        }
                    }
                    else{
                        //new pattern (again)
                        //first, put the ol one..
                        out.add(lastEntity);
                        itera++;
                        
                        //then put the new one
                        lastEntity = new Entity(dirName + "[" + itera + "]");
                        
                        if(this.pickList.contains(flTitle)){
                            lastEntity.getFileTarget().add(fpth);
                        }
                    }
                }
            }
            
            //ensure the last one inserted
            out.add(lastEntity);
        }
        
        return out;
    }
    
    /**
     * <p>Takes directory path to be scanned.
     * This method assumes the directory contains directories that
     * named after the research report title.</p>
     * 
     * @param path
     * @return
     */
    public Queue<Entity> crawl(String path){
        Queue<Entity> out = new LinkedList<>();
        
        File accessor = new File(path);
        if(accessor.isDirectory()){
            File[] lfiles = accessor.listFiles();
            for(File f: lfiles){
                if(f.isDirectory()){
                    out.addAll(this.discern(f.getAbsolutePath()));
                }
            }
        }
        
        return out;
    }
}
