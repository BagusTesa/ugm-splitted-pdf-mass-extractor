Switch system using button on electronic appliances is less efficient for
some people who have physical limitations. Humans could control the switching
system of electronic appliances easier using their speech by utilizing the speech
recognition.
In this research, speech recognition system receives human speech input
through the microphone and then the speech processed by Raspberry Pi which
also control the switching system. Speech recognition is built using Hidden
Markov Model covered in Hidden Markov Model Toolkit (HTK) and run by
Julius software. Instruction that will be recognized are clustered in a grammar and
added with a transcription of the word to be trained as an acoustic model. Speech
recognition method use the acoustic model to be compared with speech signal.
The output of speech recognition is then used as an instruction in electronics
switching system. Speech recognition using English language with American
English dialect that refers from VoxForge dictionary.
The result of this research is a prototype of speech recognition system as a
switching system control using Raspberry Pi as processing unit using microphone
from Logitech C920 webcam. The system trial is done by testing the effect of
voice signal gain and speech recognition test in environment condition with noise
level of 20-30 dB. The best result is achieved when the gain is 30-40 dB. The
success rate of speech recognition using processing unit Raspberry Pi is 83,50%.
This system can control the switching of electronic appliances based on human
speech input using Raspberry Pi.
Keywords: speech recognition, Raspberry Pi, Hidden Markov Model Toolkit

