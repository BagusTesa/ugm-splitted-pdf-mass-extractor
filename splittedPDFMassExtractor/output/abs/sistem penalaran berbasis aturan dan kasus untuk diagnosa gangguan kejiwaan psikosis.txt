Penelitian mengenai penyakit mental �The Global Burden of Disease� yang
dilakukan oleh Murray (1996) bekerjasama dengan WHO dan World Bank
memprediksikan bahwa penyakit mental akan menduduki posisi kedua setelah
penyakit kardiovaskuler pada tahun 2020. Salah satu penyakit mental tersebut adalah
penyakit Psikosis. Hal ini mengakibatkan sistem komputer yang memanfaatkan
metode kecerdasan buatan untuk membantu kerja para profesional dibidang
kedokteran kejiwaan menjadi sangat diperlukan.
Penelitian ini menggunakan penalaran berbasis aturan (RBR) untuk
melakukan diagnosa awal gangguan psikosis yang terdiri dari gangguan Skizofrenia,
gangguan waham menetap dan gangguan psikosis akut dan sementara dimana
digunakan certainty factor (CF) dalam menangani ketidakpastian yang muncul serta
penggunaan penalaran berbasis kasus (CBR) untuk melakukan diagnosa jenis
gangguan psikosis Skizofrenia. Proses diagnosa dilakukan dengan cara memasukkan
gejala yang dirasakan oleh pasien oleh paramedis. Jika pasien memiliki gangguan
Skizofrenia, maka digunakan CBR untuk melakukan diagnosa jenis Skizofrenia nya.
Setiap kasus baru Skizofrenia akan di hitung tingkat similaritas dengan menggunakan
metode Weighted Nearest Neighbor.
Hasil pengujian yang dilakukan oleh pakar menunjukkan bahwa sistem RBR
mampu melakukan diagnosa gangguan Psikosis dengan benar sedangkan hasil
pengujian sistem CBR menggunakan data rekam medis menunjukan bahwa sistem
mampu mengenali jenis Skizofrenia secara benar dengan kriteria similaritas sangat
mirip (0,8-1) sebesar 80% dan kriteria mirip (0,6 � 0.79) sebesar 20%.
Kata kunci: RBR, CBR , psikosis, similaritas, weighted nearest neighbor
by
Research on mental illness "The Global Burden of Disease" conducted by
Murray (1996) in collaboration with WHO and the World Bank predicts that mental
illnesses will occupy the second position after cardiovascular disease in 2020. One of
the mental illness is a disease Psychosis. This resulted in a computer system that
utilizes artificial intelligence methods to help professionals working in the field of
psychiatric medicine becomes indispensable.
This study uses a rule-based reasoning (RBR) to conduct an assessment
consisting of psychosis Schizophrenia disorders, settled delusional disorders and
acute transient psychotic disorders with Certainty Factor (CF) method to handle
uncertanty and the use of case-based reasoning (CBR) for diagnosing types of
Schizophrenia disorders. The process of diagnosis is done by entering the patients
symptoms by paramedics. If the patient has a Schizophrenia disorder, then use CBR
to diagnose kinds of Schizophrenia. Each Schizophrenia new case will be calculated
using a rate of similarity Weighted Nearest Neighbor method.
Results of tests performed by experts have shown that the system is able to
diagnose correctly, while the tests results using medical records show that the system
is able to correctly identify the kind of Skizofrenia with criteria similarity at 80%
(0,8-1) and 20% (0,6-0,79)
Keywords: RBR, CBR, psychosis, similarity, weighted nearest neighbor

