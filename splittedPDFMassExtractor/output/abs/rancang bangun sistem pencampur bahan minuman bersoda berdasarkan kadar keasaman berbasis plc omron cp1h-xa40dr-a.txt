Telah dibuat purwarupa sistem pencampur bahan minuman bersoda
berdasarkan kadar keasaman berbasis PLC OMRON CP1H-XA40DR-A.
Pembuatan sistem ini didasari oleh pentingnya kadar pH dari bahan minuman
bersoda, karena kadar pH pada suatu minuman sangat mempengaruhi kualitas
dari minuman tersebut. Sistem pencampuran bahan minuman ini juga
mempunyai Human Machine Interface (HMI) sebagai perantara kontrol dan
masukan pada sistem. Keakuratan tinggi diperlukan dalam proses pencampuran
bahan minuman bersoda untuk menjaga kualitas dari produk tersebut. Salah satu
faktor yang diperhatikan untuk menjaga kualitas tersebut adalah dengan
memastikan pH cairan dari hasil pencampuran sesuai dengan standar produksi.
Sistem otomasi pencampuran bahan minuman bersoda yang diteliti ini
berbasis pada PLC buatan OMRON dengan seri CP1H-XA40DR-A. Sistem ini
dilengkapi dengan sensor ketinggian eTape sebagai sensor pendeteksi
ketinggian agar sistem dapat membaca volume cairan yang dihasilkan, motor
washer sebagai media penyemprot cairan. Serta sensor elektroda pH PE-03
sebagai pembaca kadar keasaman. Pengaturan pencampuran berdasarkan kadar
keasaman diatur setelah dilakukan kalibrasi sensor elektroda pH PE-03 sebagai
pembaca kadar keasaman. Sistem ini dikontrol melalui HMI dengan program
Hasil dari penelitian ini adalah sebuah sistem pencampuran bahan
minuman bersoda secara otomatis yang dapat mencampurkan cairan sesuai
masukan pH yang ditentukan oleh pengguna. Terdapat rentang pilihan kadar
keasaman yang tersedia dalam sistem ini, yaitu pH 4 sampai pH 6. Keakuratan
proses pencampuran cairan ini yaitu sebesar 98,45%.
Kata Kunci : sistem pencampuran, PLC, sensor elektroda pH, HMI
Have been made a prototype of soft drinks mixing ingredients system
based on acidity controled by PLC OMRON CP1H-XA40DR-A. This system
have been made based on the importance of pH levels on soft drinks as pH
levels in a drink has greatly affects the quality of it. This soft drinks mixing
ingredients system has Human Machine Interface (HMI) as an intermediary
control and input on the system. High accuracy is needed in the process of
mixing the ingredients of soft drinks to mantain the quality of the product. One
of the factor that is considered to mantain it quality is to ensure pH levels of the
mixing accordance to standard production regulation.
The automation of soft drinks mixing ingredients systems is based on
PLC by OMRON with serial number CP1H-XA40DR-A. This system equipped
with eTape height sensor to detect height of the fluid then calculate the volume
of the mixing process, motor washer to pump the fluid. Then a pH electrode PE-
03 sensor to read the acidity. The adjustment of mixing controled by acidity has
been set after calibration of pH electrode PE-03 sensors.The system is
controlled through the Wonderware InTouch HMI program.
The results of this study is a soft drinks mixing ingredients systems that
automatically mixes the fluid according to the pH input specified by the user.
There are a range of acidity options available in this system, namely pH 4 to pH
6. The accuracy of the fluid mixing process is equal to 98,45%.
Keyword: mixing system, PLC, pH electrode sensor , HMI

