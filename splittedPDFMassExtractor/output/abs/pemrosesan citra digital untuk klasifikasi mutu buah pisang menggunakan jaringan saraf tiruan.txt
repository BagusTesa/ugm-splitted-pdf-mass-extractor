Buah pisang memasok kebutuhan tidak hanya pasar dalam negeri, tetapi juga
pasar internasional. Oleh karena itu, mutu buah pisang harus selalu dijaga. Saat ini
sortasi mutu pisang masih dilakukan secara manual oleh manusia, akibatnya
menghasilkan keragaman mutu yang kurang baik. Untuk itu diperlukan suatu
sistem yang dapat mengklasifikasikan mutu buah pisang menggunakan pengolahan
citra digital dan jaringan saraf tiruan.
Citra pisang diambil dengan kamera digital dan diolah menggunakan Matlab.
Pemrosesan citra digital digunakan untuk mengekstrak fitur warna dan tekstur buah
pisang. Sedangkan jaringan saraf tiruan digunakan untuk klasifikasi mutu pisang.
Penelitian ini menggunakan 125 pisang untuk data pelatihan dan 100 pisang untuk
data pengujian. Mutu pisang dibagi menjadi 5 kelas, yaitu kelas Super, kelas A,
kelas B, luar mutu I dan luar mutu II.
Parameter yang digunakan untuk masukan jaringan saraf yaitu luas cacat,
nilai red, green, blue, energy, homogeneity, dan contrast. Konfigurasi terbaik
model jaringan backpropagation untuk sistem klasifikasi mutu pisang adalah
dengan laju pembelajaran sebesar 0,3 dan jumlah neuron pada lapisan tersembunyi
sebanyak 10 neuron. Dengan konfigurasi tersebut, sistem mampu
mengklasifikasikan mutu dengan tingkat keberhasilan sebesar 94 % dari 100 data
uji pisang.
Kata kunci : klasifikasi mutu pisang, pengolahan citra digital, jaringan saraf
tiruan.
Bananas does not only supply the domestic market, but also the international
market. Therefore, the quality of banana fruit should be maintained. Currently,
quality sorting process of bananas are still done manually by humans, consequently
the result is not good. So we need a system that can classify quality of bananas by
using image processing and artificial neural network.
Banana image captured by a digital camera and processed using Matlab.
Digital image processing is used to extract color and texture features of banana.
While artificial neural networks used for classification of the quality of bananas.
This study uses 125 bananas for training data and 100 bananas for testing data.
Quality of bananas are divided into 5 classes, Super, class A, class B, external
quality I and external quality II.
Input parameters used for the neural network are area defects, red, green, blue,
energy, homogeneity, and contrast. Best configuration of backpropagation network
model for a classification system of banana quality is the learning rate of 0.3 and
10 neurons in the hidden layer. With the best configuration, the system is able to
classify the quality of banana fruit with 94% accuracy rate from 100 bananas test
data.
Keywords : classification of banana quality, digital image processing, artificial
neural networks.

