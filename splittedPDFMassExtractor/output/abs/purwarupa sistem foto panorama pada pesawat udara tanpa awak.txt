Pesawat sayap tanpa awak mulai diminati oleh warga sipil untuk melakukan
misi-misi pemantauan. Dalam misi tersebut muncul berbagai masalah seperti
bagaimana melakukan pemantauan menggunakan pesawat tanpa awak dengan
mudah dan memiliki cakupan pemanataun yang luas. Berdasarkan permasalahan
tersebut dibuatlah pengembangan sistem foto panorama untuk pesawat udara
tanpa awak. Penelitian ini bertujuan untuk mengimplementasikan sistem foto
panorama untuk mendukung pengoperasian pesawat udara sayap tetap dalam
melakukan misi pemantauan, dengan tujuan memudahkan pengguna dalam
memantau suatu wilayah dengan teknologi nirkabel. Sistem Foto panorama ini
didukung oleh unit pengiriman yang terdiri dari webcam, modul Raspberry Pi,
wireless USB dongle dan motor servo.
Pengujian dilakukan dengan memvariasikan sudut servo terhadap jarak
fokus, mencocokan ukuran pixel dengan ukuran yang sebenarnya dan mencoba
sistem foto panorama dengan pesawat udara tanpa awak. Hasil dari penelitian ini
menunjukan berhasilnya implementasi sistem foto panorama yang di aplikasikan
pada pesawat udara tanpa awak dengan sudut pemotretan terbaik sebesar 10
derajat. Untuk ukuran foto dengan resolusi 640x480 dengan jarak objek 1meter
didapatkan ukuran sebenarnya 1x0,75 meter
Kata kunci : UAV, foto panorama, Raspberry Pi, nirkabel
Unmanned aerial vehicle began interested by civilian used to monitoring
mission. On that mission appear a problem, like how to monitoring using
unmanned aerial vehicle that have long mileage easily and has a board scope of
monitoring. Based on that problem, be made development of a system panoramic
photo of unmanned aerial vehicle. This study aims to implement a system to
support the operation of a panoramic photograph that support  unmanned aerial
vehicle monitoring mission, which allows users to monitor an area with wireless
technology. This panoramic picture is supported by transmission unit that consist
of a webcam, Raspberry Pi module, wireless USB dongle and servo motor
The test performed by varying teh angle of the servo to the focus distance,
matching teh pixel sixe with the actual size of teh system and try panoramic
photos with unmanned aerial vehicle. The result of this study shows the successful
implementation of the system panoramic photo with unmanned aerial vehicle with
the best shooting angle by 10 degree. For the size image with a resolution
640x480 and a distance of  1 meter object obtained actual size 1x0,75 meters.
Keyword : UAV , panoramic photo, Raspberry Pi, Wireless

