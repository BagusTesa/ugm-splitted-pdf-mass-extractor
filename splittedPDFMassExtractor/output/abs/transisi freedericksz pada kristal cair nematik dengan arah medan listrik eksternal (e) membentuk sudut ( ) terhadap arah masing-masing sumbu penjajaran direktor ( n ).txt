Freedericksz Transition in the Nematic Liquid Crystal with External Field (E)
is Forming an Angle  (? ) to Each Axis of Director ( n ) Alignment
Freedericksz transition can be understood as a state when the liquid crystal
molecules are starting distortion. Freedericksz transition occurs when the electric
field (E) is greater than the critical electric field (Ec). The potential when the
Freedericksz transition will occur is called the critical potential (Vc). In this study,
the formulated of critical field equations (Ec) and critical potential (Vc) by
assuming the electric field (E) that applied is forming the slope of angle  A against
the x-axis (planar condition) in the first case. Then in the second case, the electric
field that applied is forming the slope of angle ? against  the y-axis (homogeneous
conditions) and in the third case, the electric field (E) that applied is forming the
z-axis (homeotropik conditions). The liquid crystal molecules are considered rod-
like and after the field is applied, the liquid crystal molecules are considered tend
to do an alignment with the direction of the electric field (E) applied . The Euler-
Lagrange equation is used for the first solution of this research. The strong
anchoring Conditions are used as initial and boundary conditions in the final
solution of the Diferiansial Ordinary Equations (DOE).  In this study, the smallest
of the critical potential (Vc) is occurred when electric field applied is forming the
slope of  90o to each axis alignment. When the electric field (E) are applied in
interval o0 45?? ? , then the Freedericksz transition is not found. In This case is
consistent with the initial assumption that when the liquid crystal molecules are
electric field (E) applied, then the liquid crystal molecules tend to do the
alignment with the direction of the electric field (E) and was only found when the
slope of the electric field applied is at interval o o45 90?< ? .
Key words: Freedericksz transition, critical electric field (Ec),  critical potential

