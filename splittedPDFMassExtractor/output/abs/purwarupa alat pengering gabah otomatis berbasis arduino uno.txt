Perubahan cuaca saat ini bisa dikatakan tidak stabil. Dengan perubahan
cuaca yang tidak menentu ini dapat mengganggu aktifitas para petani setelah
panen yaitu pada saat penjemuran atau pengeringan gabah. Untuk itu dibuat alat
yang dapat mengeringkan gabah dari perubahan cuaca yang tidak menentu.
Alat pengering gabah ini menggunakan Arduino Uno R3 sebagai otak dari
keseluruhan sistem. Sistem mekanik motor sebagai pengaduk gabah. Sistem
pengering (dryer) ini menggunakan relay yang diatur on/off secara otomatis sesuai
dengan lampu indikator, serta set point suhu udara yang dapat dimasukan dengan
menggunakan keypad 3x4 sesuai dengan kebutuhan pengeringan.
Hasil masukan set point suhu serta pengukuran suhu dan kelembaban dari
Sensor SHT 11 akan ditampilkan pada layar LCD karakter 16x2. Sehingga dengan
adanya alat pengering gabah ini para petani lebih mudah untuk mengeringkan
gabah pada saat kondisi cuaca mendung atau hujan.
Kata kunci : sensor SHT 11, keypad 3x4, dryer, Arduino Uno R3, LCD, relay,
lampu indikator
Climate condition nowadays has become hard to predict. The condition
alike prompted inefficiencies of the farmers activities after cultivating the crops
when farmers drying their grain using the sunlight. Therefore created the system
that could dry the grain without compromising the anomali of climate condition
nowadays.
This system using Arduino Uno R3 as the brain. Motor mechanic system
as steering tool to steer grain. The system using relay which automatically
triggered on/off according to lamp indicator, also air temperature set point which
could be adjusted using 3x4 keypad according to user needs for drying progress.
Temperature set point result and measurement from temperature and
humidity from SHT 11 displayed in 16x2 LCD. This rice grain dryer tool could
ease the farmers job to drying rice grain while the climate is hard to predict
especially in the rainy or cloudy day.
Keywords: SHT 11 sensor, 3x4 keypad, Arduino Uno R3, relay, lamp indicator.

