Proses pengelolaan inventori di apotek Almas Farma meliputi proses
pencatatan faktur dan data stok barang. Faktur dicatat dalam buku faktur
sedangkan data stok barang ditulis dalam kartu stok barang. Dari hasil pencatatan
dan pengolahan data tersebut dibuatlah laporan faktur dan laporan stok barang.
Berdasarkan masalah di atas, maka dibuatlah sebuah sistem yang mampu
menangani proses pencatatan faktur, pengelolaan data barang hingga pembuatan
laporan. Pembuatan sistem informasi ini menggunakan bahasa pemrograman PHP
dengan framework codeigniter, Javascript dengan framework Jquery dan MySQL
untuk pengelolaaan basis datanya.
Sistem Informasi Inventory Apotek Almas Farma dapat digunakan untuk
pencatatan faktur dan pengelolaan data stok barang. Selain itu sistem juga
memberikan informasi daftar faktur hampir jatuh tempo, daftar barang hampir
habis dan daftar barang kadaluarsa.
Kata kunci : Sistem informasi, inventori, apotek, PHP, MySQL.
Item management processing in Almas Farma pharmacy is includes
recording invoice and item stocks. A new invoice noted in a invoice book while
the item stocks noted in inventory cards. After that, it is used to make invoice
report and inventory report based on  the invoice book and inventory cards.
Based on the problem above, information system is needed to solve the
recording process until make the report. This information system is built using
PHP with codeigniter framework, framework jQuery for javascript and MySQL
for the database.
Inventory of Almas Farma pharmacy information system can used to
manage the items data and the invoice data, Beside that, in this information
system can shows invoice data which has almost due date, the data items which
has low in stock,and the data item which has almost expired.

