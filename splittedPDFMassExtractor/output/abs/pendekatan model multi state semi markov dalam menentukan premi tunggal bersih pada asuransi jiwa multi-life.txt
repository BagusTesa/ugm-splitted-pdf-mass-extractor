Untuk memodelkan ketidakbebasan antara sisa usia hidup suami dan istri
digunakan model semi markov yang merupakan pengembangan dari model
markov dimana transisi dalam model semi markov selain mempertimbangkan
state sekarang pasangan itu berada juga mempertimbangkan lamanya waktu yang
dihabiskan pada state tersebut. Estimasi probabilitas transisi dan intensitas transisi
pada model semi markov menggunakan metode maksimum likelihood. Hasil
estimasi tersebut digunakan dalam menghitung premi tunggal bersih pada asuransi
jiwa multi life dengan status last survivor.
To model the dependence between the lifetimes of a husband and wife we
used semi markov model that derived from markov model which its transition
intensities depend not only on current state but also the time elapsed since the last
transition. The estimation on this model used maximum likelihood estimation and
it�s results were used to determine net single premium of multi life insurance
survivorship.

