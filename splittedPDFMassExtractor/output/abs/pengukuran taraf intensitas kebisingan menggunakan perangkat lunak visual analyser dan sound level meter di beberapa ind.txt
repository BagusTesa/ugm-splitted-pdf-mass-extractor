by
This research is caused by the presence of noise during the cutting wood
processand the lack of working equipment in several sawmills in Kalasan. This
research aims to measure the noise intensity level in several sawmill industry by
using visual analyser software and to recognize the constanta equivalent between
visual analyser software and sound level meter. The samples were drawn from
UD.Glugu and UD. Glugu �Langkah Usaha�. Data collection was conducted over
6 days in which 5 days using a laptop equipped Visual Analyser 2011 (VA 2011)
software which has been calibrated with Sound Level Meter every 3 minutesin 1
hour per day and 1 day direct measurement using SLM SL-4012 every 30 seconds
for 1 hour . The result is a continuous noise intensity level (LAeq) contained in (1)
UD. Glugu is equal to LAeq � ?LAeq = (104 � 2) dB(A) and measurement using the
SLM SL-4012 indicate LAeq � ?LAeq = (98 � 3) dB(A), (2)UD. Glugu "Langkah
Usaha" LAeq � ?LAeq = (92 � 5) dB(A) and direct measurement SLM SL-4012
provides resultLAeq � ?LAeq = (89 � 1) dB(A). The frequency producing the highest
intensity in the UD. Glugu is in the range of 0-1 kHz with a percentage of 34.5 %
and the frequency of 1-2 kHz of 22.6%, and the frequency that produces the
highest intensity in the UD. Glugu "Langkah Usaha" is in the range of 0-1 kHz
frequency by 33% and by 17,6% 4-5 kHz and 5-6 kHz of 16,8%. In
conclusion,noise intensity level in the two places have passed the recommended
noise threshold of 70 dB(A) by Minister of Health and 85 dB(A) by Minister of
labor for 8 hours per day exposure time.
Keywords : noise intensity, visual analyser,  sawmill industry

