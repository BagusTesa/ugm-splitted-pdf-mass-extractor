by
Indonesia has many culture in form of traditional fabrics, one of them is
woven fabric from Nusa Tenggara Timur (NTT). Each NTT ethnic has motif
characteristic which is a manifestation of daily life, culture and the belief of local
community. For NTT woven fabric observers, the origin of a woven fabric can be
known from the motif. But its difficult to identified the origin of a woven fabrics
because it is hard to define the characteristics of woven fabric motif from a region
and the diversities both of the motifs and the color compositions.
Texture analysis is the image analysis technique based on the assumption
that an image was formed by pixels intensity variations, both gray and color
images. Woven fabric motif is formed from the color intensity variations that can
be seen as color texture of woven fabric. This study aims to find out among
texture analysis using GLCM combined with color moment and texture analysis
using CCM, which method gives better results for NTT woven fabric motif
classification.
The results showed that for NTT woven fabric motif classification, texture
analysis using CCM gives better results, both on NMC and KNN classification
method, with respectively 75% and 80%, than the combination of GLCM � color
moment that give accuracy of 45% and 50%. With 640x480 pixels input image,
CCM gives the trainning time 19 seconds and GLCM�color moment gives 11
seconds, while for the classification time CCM gives 378 milliseconds and
GLCM�color moment gives 223 milliseconds.
Keywords : image classification, Gray Level Co-occurrence Matrix, Color Co-
occurrence Matrix, color moment, NTT woven fabric motif

