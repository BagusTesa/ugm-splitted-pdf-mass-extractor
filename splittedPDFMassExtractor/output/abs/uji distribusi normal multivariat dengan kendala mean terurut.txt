by
D i a n a
In this thesis, the test ordered means of multivariate normal distribution
against all alternatives for case when the covariance matrices are known. Use the
likelihood ratio method and isotonic application, we obtain the test statistic and
study its null distribution. to propose the critical values and simulation study for
compute power of test and p-value from bivariate normal distribution.
Keyword: Isotonic regression, Likelihood ratio test, Multivariate normal distribution,

