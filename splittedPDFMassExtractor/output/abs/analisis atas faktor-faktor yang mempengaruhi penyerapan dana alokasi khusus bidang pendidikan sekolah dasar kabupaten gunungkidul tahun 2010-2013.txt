 
 
Penelitian ini bertujuan untuk menganalisis faktor-faktor yang menyebabkan
rendahnya penyerapan Dana Alokasi Khusus (DAK) Bidang Pendidikan SD
Kabupaten Gunungkidul Tahun 2010 sampai dengan 2013. Penelitian ini
merupakan penelitian kualitatif dengan pendekatan studi kasus, yaitu meneliti
secara mendalam penyebab rendahnya penyerapan DAK Bidang Pendidikan SD.
DAK Bidang Pendidikan SD merupakan alokasi dana dari Pusat kepada
Pemerintah Daerah untuk membiayai urusan wajib pendidikan di tingkat SD yang
dilaksanakan oleh Pemda. DAK merupakan bagian dari dana perimbangan.
Pelaksanaan DAK Bidang pendidikan SD diatur dengan petunjuk teknis dari
Kementerian Pendidikan yang terbit setiap tahun. DAK digunakan untuk
pembangunan/rehab ruang kelas dan perpustakaan dan untuk pengadaan sarana
pembelajaran, yaitu pengadaan alat peraga, buku-buku, dan media pembelajaran
lainnya. Dalam pelaksanaannya di Gunungkidul, realisasi penyerapan DAK
Bidang Pendidikan SD masih rendah. Berdasarkan hasil analisis dokumen dan
wawancara dengan pihak terkait, penyebab rendahnya penyerapan ini adalah
keterlambatan terbitnya juknis dari Kementerian Pendidikan dan sering terjadinya
revisi atas juknis di tahun berjalan yang tidak memperhitungkan waktu
kemampuan Pemda untuk menyerap DAK tersebut. Kegiatan DAK masuk ke
dalam batang tubuh APBD Pemda, sehingga apabila juknis terlambat atau
direvisi, maka Pemda menyesuaikan perubahan tersebut ke dalam Perubahan
APBD. Perda Perubahan APBD ditetapkan menjelang akhir tahun anggaran,
sekitar bulan Oktober, sehingga Pemda hanya punya waktu efektif dua bulan
untuk melaksanakan kegiatan DAK tersebut. Kondisi ini menyebabkan tidak
optimalnya penyerapan DAK tersebut. Sedangkan untuk faktor-faktor lain seperti
perencanaan anggaran, pelaksanaan anggaran, metode pengadaan barang/jasa
secara swakelola, dan faktor sumberdaya manusia tidak menjadi penyebab
rendahnya penyerapan DAK tersebut.
Kata kunci: Penyerapan Anggaran, Dana Alokasi Khusus Bidang Pendidikan
xii 
 
 
 
This study aimed to analyze the factors that led to the low absorption of the
Special Allocation Fund (DAK) Elementary School in the Gunungkidul Regency
for year 2010 to 2013. This research is a qualitative case study approach, which
examines in depth the causes of low  absorption of DAK Elementary School
DAK Elementary School is an allocation of funds from the center to local
governments to finance the obligatory education at the primary level that carried
out by local government. The use of this fund is regulated by Ministry of
Education. The guidelines are published every year. DAK is used for the
construction/rehabilitation of classrooms and libraries and to procure the means of
learning, namely the procurement of props, books, and other learning media. In its
implementation in Gunung Kidul, the absorption of the Special Allocation Fund
(DAK) Elementary School is low. Based on the analysis of documents and
interviews with stakeholders, the cause of the low absorption are delays in the
publication of the guidelines from the Ministry of Education and the frequent
occurrence of a revision of the guidelines in the current year which does not take
into account the ability of local governments to absorb it. DAK activities is
include into the body of the local government budget, so if late or revised
guidelines, the local government must adjust the changes to the budget changes.
Bylaw Changes in the budget set by the end of the fiscal year, around October, so
local government had only two months to implement the DAK activities. This
condition causes no optimal absorption of the DAK. As for other factors such as
budget planning, budget execution, and the method of procurement of
goods/services by self-management,  are not the cause of the low absorption of
xiii 
 

