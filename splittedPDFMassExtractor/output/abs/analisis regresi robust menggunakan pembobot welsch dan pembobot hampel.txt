Analisis Regresi Robust menggunakan Pembobot Welsch dan Pembobot Hampel
oleh :
Dalam beberapa studi kasus yang ditemukan saat melakukan penelitian, kita sering
menjumpai adanya data yang memiliki pencilan. Untuk melakukan analisis regresi
dengan data tersebut, maka kita dapat melakukan analisis regresi robust. Analisis ini
memiliki beberapa metode dan banyak fungsi yang dapat digunakan untuk mencari
parameter koefisien untuk variabel independen dalam menentukan variabel dependen.
Salah satu metode yang dapat digunakan adalah Estimasi-S. Metode estimasi-S
memiliki breakdown point yang tinggi (50%) dan fungsi yang digunakan adalah
fungsi dengan pembobot welsch dan pembobot hampel. Laporan skripsi kali ini
bertujuan untuk membandingkan kedua fungsi tersebut dengan menggunakan
estimasi S serta membandingkan pula dengan metode kuadrat terkecil (dengan/ tanpa
pencilan). Data yang digunakan dalam studi kasus laporan skripsi ini adalah data
kualitas air dari Laporan Hasil Uji di Balai Laboratorium Kesehatan Yogyakarta
dengan periode pada tahun 2012.
Kata kunci : Pencilan, Analisis Regresi Robust, Estimasi-S, Pembobot Welsch,
Analysis of Robust Regression using Weighted Welsch and Weighted Hampel
by :
In several case studies that found when doing research, we often find the data that
have outliers. To perform regression analysis with the data, then we can perform
robust regression analysis. This analysis has several methods and many functions that
can be used to find the parameter coefficients for the independent variables in
determining the dependent variable. One method that can be used is estimated-S.
Estimation methods-S has a high breakdown point (50%) and the function that used
are weighted function Welsch and weighting function Hampel. This time the report
thesis aims to compare both these functions by using the estimated S and compares
well with the least squares method (with / without outliers). The data used in this
report is a case study of water quality data from the Report of Test Results in Health
Laboratory Yogyakarta with the period in 2012.
Keywords: Outlier, Robust Regression Analysis, Estimation-S, weighted Welsch,
weighted Hampel.

