Analisis regresi merupakan salah satu analisis statistik yang sering
digunakan untuk menyelidiki pola hubungan fungsional antara variabel prediktor
dan variabel respon. Pola hubungan yang terbentuk digunakan untuk mengetahui
pendekatan yang sesuai dalam mengestimasi fungsi regresi, yaitu pendekatan
parametrik atau nonparametrik. Tesis ini berkonsentrasi pada estimator kernel
Nadaraya-Watson adapatif yang merupakan pendekatan regresi nonparametrik
menggunakan metode kernel dengan bandwidth bervariasi dari satu titik ke titik
yang lain. Pada dasarnya, estimator kernel Nadaraya-Watson adapatif merupakan
pengembangan dari estimator kernel Nadaraya-Watson. Lebih lanjut akan
dibandingkan kriteria performa dari kedu estimator dengan melihat nilai MSE
yang dihasilkan. Hasil penelitian ini menunjukkan bahwa pada estimator kernel
Nadaraya-Watson adapatif memiliki performa yang lebih baik dibandingkan
dengan pada estimator kernel Nadaraya-Watson karena menghasilkan nilai MSE
yang lebih kecil.
Regression analysis is one of statistical analysis usually used to investigate
the pattern of functional relation between predictor and response. The formed
pattern of this relation is used to find the proper approach in estimating regression
function between parametric or non parametric approach. This paper focuses on
adaptive Nadaraya-Watson Kernel regression estimator in which is non parametric
regression approach using kernel method with varying bandwidth of one point to
another. Basically, adaptive Nadaraya-Watson Kernel estimator is an expansion of
Nadaraya-Watson Kernel estimator. Furthermore, the performance criteria of both
estimators are compared here by checking the obtained MSE values. The results
of this study show that the adaptive Nadaraya-Watson kernel estimators have
better performance than Nadaraya-Watson kernel estimators because they obtain
smaller MSE values.

