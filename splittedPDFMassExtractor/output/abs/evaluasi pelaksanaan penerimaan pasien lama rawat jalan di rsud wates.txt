Latar belakang : Bagian Pendaftaran Rawat Jalan merupakan tempat pertama yang
dikunjungi oleh pasien atau keluarga pasien sebelum mereka mendapatkan pelayanan
kesehatan. Pada Tempat Pendaftaran Pasien inilah proses pelayanan pertama kali
dimulai. Proses penerimaan pasien rawat jalan ini termasuk kedalam penyelenggaraan
rekam medis. Pada penelitian ini, penulis lebih fokus pada pelaksanaan penerimaan
pasien lama  rawat jalan. Proses pendaftaran pasien lama  rawat jalan tersebut
memerlukan waktu yang lebih lama apabila dibandingakan dengan standar yang ada.
Sesuai standar, waktu yang dibutuhkan untuk mendaftar pasien lima menit akan tetapi
dalam pelaksanaanya memerlukan waktu tujuh menit lebih lama. Hal tersebut
memungkinkan pasien melakukan komplen dan berpengaruh terhadap kepuasan
pelanggan. Dalam penelitian ini penulis akan melihat bagaimana pelaksanaan
pendaftaran pasien rawat jalan lama dan mengetahui hambatan yang terjadi dalam
kegiatan  penerimaan pasien lama rawat jalan di RSUD Wates.
Tujuan : Untuk mengetahui pelaksanaan penerimaan pasien lama rawat jalan di RSUD
Wates serta mengetahui hambatan-hambatan dalam pelaksanaan penerimaan pasien
lama rawat jalan.
Metode Penelitian : Jenis penelitian yang digunakan dalam penelitian ini adalah
penelitian deskriptif dengan pendekatan kualitatif. Rancangan penelitian yang
digunakan  dalam penelitian ini adalah adalah cross sectional. Subjek penelitian ini
adalah tiga orang petugas pendaftaran sedangkan triangulasi sumber peneliti
menggunakan hasil wawancara dengan kapala bagian pelayanan penunjang medis.
Pengumpulan data menggunakan wawancara, observasi, dan studi dokumentasi.
Hasil : Pelaksanaan penerimaan pasien sudah ada SOP nya akan tetapi belum ada
penggunaan nomor antrian pendaftaran seperti yang tertera pada SOP. Belum ada
SOP untuk penerimaan pasien Jamkseda, Jamkesda non Kartu, Jamkesos, Jampersal,
dan Jamsostek. Ada beberapa hambatan yang terjadi diantaranya dari faktor man
(manusia), machine (alat), method (cara), dan environment (lingkungan). Seperti
lamanya pembuatan jaminan, terlambatnya berkas rekam medis sampai di poliklinik
tujuan, adanya duplikasi berkas, kurang telitinya petugas saat mewawancarai pasien,
serta jauhnya poliklinik yang menghambat distribusi berkas. Hal-hal tersebut yang
menghambat pelayanan serta memunculkan komplen dari pasien dan berakibat
terhadap ketidakpuasan pelayanan.
Kata kunci : Evaluasi, Penerimaan Pasien, Pasien Lama Rawat Jalan
Background: Outpatient Registration Section is the first place visited by the patient
or the patient's family before they get health care. The Patient Registration on the
service process is first started. Outpatient admissions process is included into the
organization of medical records. In this study, the authors focus on the
implementation of patient acceptance of outpatient long. Old patient registration
process requires outpatient longer time if dibandingakan with existing standards. By
default, the time required to enroll patients in five minutes but implementation takes
seven minutes longer. It allows the patient did complain and the effect on customer
satisfaction. In this study the authors will see how the implementation of the
outpatient registration time and know the obstacles that occur in patients receiving
long activities in the hospital outpatient Wates.
Objective: To determine the execution time of patients receiving outpatient care in
hospitals Wates and know the obstacles in the implementation of patient acceptance
of outpatient long.
Method: The type of research used in this study was a descriptive study with a
qualitative approach. The research design used in this study is a cross sectional. The
subjects were three admissions while researchers used triangulation Kapala part on
interviews with medical support services. Collecting data using interviews,
observations, and documentation.
Results: Implementation of patient acceptance of its existing SOP but there has
been no use of the registration queue number as indicated on the SOP. There is no
SOP for patients receiving Jamkseda, Jamkesda non Cards, Jamkesos, Jampersal,
and Social Security. There are several obstacles that occur from factors such as man
(human), machine (tool), method (how), and environment (environment). Such as the
length of making bail, delay in medical record file in the clinic until the destination, file
duplication, less rigorous officers when interviewing the patient, as well as away
polyclinic which inhibit the distribution of files. Those things that hinder service and
peeps complain of patient care and lead to dissatisfaction.

