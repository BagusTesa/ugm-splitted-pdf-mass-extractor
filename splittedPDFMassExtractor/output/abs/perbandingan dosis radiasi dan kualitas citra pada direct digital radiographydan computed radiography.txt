Telah dilakukan penelitian perbandingan dosis radiasi dan kualitas citra
pada Direct Digital Radiography (DR) dan Computed Radiography (CR)
menggunakan phantom bayi sebagai obyek dengan daerah pemeriksaan abdomen
(AP). Pada penelitian ini pengukuran laju paparan radiasi hambur dilakukan
menggunakan surveymeter pada jarak 10 cm dari titik fokus sinar-X. Analisis
kualitas citra dilakukan menggunakan software ImageJ meliputi analisis
histogram dan perhitungan nilai SNR (Signal to Noise Ratio). Dari hasil penelitian
diperoleh bahwa laju paparan radiasi tertinggi untuk DR diperoleh 0.137 mR/jam
pada tegangan tabung 40 kV dengan variasi tanpa penggunaan grid dan laju
paparan terendah diperoleh 0.108 mR/jam pada tegangan tabung 60 kVdengan
penggunaan grid. Untuk sistem CR laju paparan tertinggi diperoleh 0.105 mR/jam
pada tegangan tabung 40 kV dengan variasi tanpa penggunaan grid dan laju
paparan terendah diperoleh 0.083 mR/jam pada tegangan tabung 60 kV dengan
penggunaan grid. Untuk kedua variasi pengambilan data, citra hasil sistem CR
mempunyai nilai SNR lebih besar dibandingkan citra hasil sistem DR. Histogram
pada citra hasil pencitraan pada DR mempunyai rentang gray level yang lebih
besar (distribusi gray level lebih lebar) dibandingkan hasil pencitraan sistem CR.
Kata kunci :  Direct Digital Radiography, Computed Radiography, Dosis Radiasi,
Kualitas Citra, Signal to Noise Ratio.
The measurement for comparison of radiation dose and image quality on
Direct Digital Radiography (DR) and Computed Radiography (CR) had been
conducted by using baby phantom as the object with abdomen (AP) examining
area. The measurement of scattered radiation exposure was conducted using
surveymeter at a distance of 10 cms from the focus point of X-ray. Image quality
was analized using ImageJ software i.e. histogram analysis and SNR (Signal to
Noise Ratio) calculation. The results indicate that in exposure measurement of
scattered radiation for DR system yield maximum value on 40 kV of 0.137
mR/hour without grid and minimum value on 60 kV of 0.108 mR/hour with grid.
The results of CR system yield maximum value on 40 kV of 0.105 mR/hour
without grid and minimum value on 60 kV of 0.083 mR/hour with grid. Value of
SNR analysis on CR is higher than DR for both measurement results. Gray level
distribution of histogram analysis on DR wider than CR.
Image Quality, Signal to Noise Ratio.

