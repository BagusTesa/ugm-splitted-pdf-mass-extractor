Di dalam skripsi ini dibahas tentang beberapa sifat geometri pada ruang Ba-
nach. Pertama dibahas mengenai konveksitas pada ruang Banach seperti konvek-
sitas tegas, konveksitas seragam, dan modulus konveksitas. Selanjutnya, dibahas
mengenai pemetaan dualitas. Pada pembahasan pemetaan dualitas dibahas pula
mengenai subdiferensial fungsi konveks sejati serta hubungan subdiferensial deng-
an pemetaan dualitas. Terakhir dibahas mengenai kehalusan pada ruang Banach.
In this final project we discuss about some geometric properties of a Banach
space. First we discuss about convexity of a Banach space such as strict convexity,
uniform convexity, and modulus of convexity. We continue by discussing about du-
ality mappings. In the discussion of the duality mappings we also discusses subdif-
ferential of proper convex functions and the relation of subdifferential with duality
mappings. Finally we discuss about smoothness of Banach spaces.

