Kerajinan Keramik dari Kasongan Bantul Yogyakarta
Telah dilakukan penelitian untuk menganalisa kualitas bahan tanah liat
menggunakan teknik radiografi digital sinar-X. Pengujian dilakukan terhadap tiga
jenis tanah liat earthenware abu-abu, earthenware merah, dan stoneware yang
merupakan hasil proses pengayakan dengan nomor ayakan 40, 60, 80, 100, dan
120 mesh. Parameter kualitas bahan tanah liat pada sistem radiografi digital sinar-
X diukur dengan cara menghitung koefisien serapan linear (�) dan persentase
susut kering tanah liat. Tanah liat earthenware abu-abu tanpa pengayakan
memiliki koefisien serapan linear 0,462 cm
-1
dan susut kering 3,6 %. Setelah
melalui proses pengayakan tanah liat earthenware abu-abu memiliki koefisien
serapan linear yang praktis sama dengan tanah tanpa pengayakan dan memiliki
susut kering 4,5% - 7,9 %. Tanah liat earthenware merah tanpa pengayakan
memiliki koefisien serapan linear 0,482 cm
-1
dan susut kering 6,1 %. Setelah
melalui proses pengayakan tanah liat earthenware merah memiliki koefisien
serapan linear yang praktis sama dengan tanah tanpa pengayakan dan memiliki
susut kering 7,0 % - 10,4 %. Tanah liat stoneware memiliki koefisien serapan
linear 0,378 cm
-1
dan susut kering 2,6 %. Setelah melalui proses pengayakan
tanah liat stoneware memiliki koefisien serapan linear yang praktis sama dengan
tanah tanpa pengayakan dan memiliki susut kering 3,6% - 7,0 %.
Kata kunci: Radiografi digital, tanah liat, pengayakan, mesh, serapan radiasi.
Clay Materials Quality Analysis of Kasongan Bantul Yogyakarta
by
The clay materials quality analysis uses digital X-ray radiography
technique had been conducted. Tests carried out on three types of clay, namely
gray earthenware, red earthenware and stoneware which are the results of
screening process, with 40, 60, 80, 100, and 120 mesh screen number. Parameters
for clay material quality analysis on a digital X-ray radiography measured by
calculate the linear absorption coefficient (�) and shrinkage percentage of dry
clay. Gray earthenware clay before screening has a linear absorption coefficient
0,462 cm
-1
and shrinkage of 3.6 %. After screening, gray earthenware has a linear
absorption coefficient is practically equal to the ground before screening and
shrinkage of 4.5 % - 7.9 %. Red earthenware clay before screening has a linear
absorption coefficient 0.482 cm
-1
and shrinkage of 6.1 %. After screening, linear
absorption coefficient of red earthenware is practically equal to the ground before
screening and shrinkage of 7.0 % - 10.4 %. Stoneware clay has a linear absorption
coefficient of 0.378 cm
-1
and shrinkage of 2.6 %. After screening, linear
absorption coefficient of stoneware is practically equal to the ground before
screening and shrinkage of 3.6 % - 7.0 %.
Keywords: digital radiography, clay, screening, mesh, radiation absorption.

