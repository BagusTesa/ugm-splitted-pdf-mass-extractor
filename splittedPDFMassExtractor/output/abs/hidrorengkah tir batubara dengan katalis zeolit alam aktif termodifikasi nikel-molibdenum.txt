Telah dilakukan hidrorengkah tir batubara dengan katalis zeolit alam aktif
termodifikasi nikel-molibdenum, yakni zeolit alam aktif (ZAA), Ni-Mo/ZAA dan Ni-
Mo/ZAA sesudah pencucian EDTA (Ni-Mo/ZAA(E)). Modifikasi katalis yang
dilakukan dalam penelitian ini meliputi aktivasi asam dengan HCl 6 N, pengembanan
logam Ni-Mo dengan metode impregnasi basah dan pencucian katalis Ni-Mo/ZAA
dengan larutan EDTA. Setelah proses aktivasi, katalis dianaliis menggunakan
difraktometer sinar-x dan metode absorpsi N2 isotermal. Parameter katalis yang
dikarakterisasi adalah jumlah situs asam, rasio Si/Al secara kualitatif, kristalinitas,
kandungan logam Mo dan Ni, luas permukaan spesifik, volume total pori dan jejari
pori. Proses hidrorengkah katalitik tir batubara dilakukan pada temperatur optimum
produk cair (450 oC) dengan laju alir gas H2 20 mL/menit sampai tidak terbentuk
produk cair dengan perbandingan umpan : katalis adalah 10:1. Produk cair yang
dihasilkan kemudian dianalisis dengan kromatografi gas dan kromatografi gas-
spektrometer massa.
Berdasarkan penelitian ini, katalis ZAA menghasilkan konversi tertinggi
produk cair yaitu 40,51% (b/b) sedangkan katalis Ni-Mo/ZAA dan Ni-Mo/ZAA(E)
memiliki konversi 28,61% dan 28,06% (b/b) berturut-turut. Fraksi bensin tertinggi
dihasilkan dengan menggunakan katalis Ni-Mo/ZAA (67,63% (b/b)) dan fraksi diesel
tertinggi dihasilkan oleh katalis ZAA sebesar 36,61% (b/b). Katalis Ni-Mo/ZAA(E)
menghasilkan konversi total yang paling tinggi yaitu 50,05% (b/b). Katalis yang
paling baik digunakan untuk hidrorengkah tir batubara adalah ZAA. Analisis GC-MS
pada 9 puncak tertinggi menunjukkan bahwa senyawa-senyawa yang terkandung
dalam hasil proses perengkahan tir batubara dengan menggunakan katalis Ni-
Mo/ZAA sebagian besar adalah senyawa phenol dan turunannya.
Kata kunci: hidrorengkah, katalitik, zeolit alam, tir batubara.
The hydrocracking  of coal tar  have been done using actived natural zeolite
catalysts modified by nickel and molybdenum, then called as ZAA, Ni-Mo/ZAA and
Ni-Mo/ZAA after wash by EDTA (Ni-Mo/ZAA(E)). The Modification of catalyst
conducted in this research was activation of zeolite by 6 N HCl, wet impregnation of
Ni-Mo metal and leaching the Ni-Mo from zeolite surfaces with EDTA solution.
After activation, then the catalyts were characterized using X-ray diffractometry and
BET/Isothermal adsorption methods.  The parameters of catalyst characterized to
determined acid site number, the ratio of Si/Al, crystallinity, Mo and Ni metal
content, specific surface area, total pore volume and pore average diameter. The
hydrocracking of coal tar was carried out at the optimum temperature (450 �C) by
flowing  H2 gas (flow rate of 20 mL/min) on the feed/liquid coal tar (ratio of
feed:catalyst was 10:1). The resulting liquid product was analyzed by gas
chromatography and gas chromatography-mass spectrometry methods.
In this research, catalytic hydrocracking with ZAA catalyst had the highest
conversion of liquid products, which were 40.51% (w/w) compared to Ni-Mo/ZAA
catalyst gave 28.61% and Ni-Mo/ZAA(E) gives 28.06% (w/w). The highest gasoline
fractions (67.63% (w/w)) produced by using Ni-Mo/ZAA catalyst and the highest
diesel fractions produced over ZAA catalyst which was 36.61% (w/w) and the highest
total conversion on the product with Ni-Mo/ZAA(E) catalyst produced was 50.05%
(w/w). The best catalysts for hydrocracking coal tar was ZAA. Product was analyzed
with GC-MS result on the 9th highest peak showed that the hydrocracking products
resulted over Ni-Mo/ZAA mostly were phenol and its derivatives.
Keyword : hydrocracking, catalytic, natural zeolite, coal tar.

