Telah dilakukan imobilisasi enzim lipase pada silika gel dengan metode
enkapsulasi (pemerangkapan). Penelitian ini bertujuan untuk meningkatkan
kemampuan penggunaan ulang enzim lipase terimobilisasi silika gel, serta
kestabilan enzim terhadap pengaruh perubahan suhu dan pH dalam reaksi
hidrolisis minyak kelapa sawit. Imobilisasi enzim lipase dilakukan dengan
mencampur larutan enzim lipase dalam bufer fosfat dengan larutan sol gel silika
pada suhu kamar sampai terbentuk padatan gel yang selanjutnya didiamkan
selama 24 jam. Persentase jumlah enzim yang terenkapsulasi oleh silika dianalisis
dengan metode biuret menggunakan spektrofotometer UV-Vis. Enzim lipase yang
telah terimobilisasi pada silika gel diuji aktivitas katalitik dan kemampuan
penggunaan secara berulang pada reaksi hidrolisis, serta stabilitas terhadap
perubahan suhu dan pH reaksi hidrolisis.
Hasil penelitian menunjukkan bahwa persentase jumlah lipase yang
terimobilisasi dalam silika gel sebesar 64,024%, tetapi setelah 5 kali pencucian
jumlah persentase lipase yang masih terimobilisasi tinggal 28,041%. Aktivitas
enzim lipase terimobilisasi silika gel lebih rendah dari pada enzim lipase bebas.
Akan tetapi enzim lipase terimobilisasi silika gel mampu digunakan sampai 5 kali
penggunaan pada reaksi hidrolisis dengan nilai konversi persentase produk asam
lemak bebas tertinggi sebesar 17,407% dan terendah sebesar 8,601%. Enzim
lipase terimobilisasi silika gel lebih stabil terhadap perubahan suhu dan pH
dibandingkan enzim lipase bebas, masing-masing enzim memiliki suhu optimum
35 �C dan pH optimum 7.
Kata kunci : lipase, silika gel, imobilisasi, hidrolisis, minyak kelapa sawit.
Immobilization of lipase enzyme on silica gel with encapsulation method
has been performed. This study aims to improve reuse of lipase immobilized by
silica gel, as well as the enzyme stability to the effects of temperature changes and
pH in the hydrolysis reaction of palm oil. The immobilized lipase enzyme made
by mixing a solution of lipase in phosphate buffer solution of silica sol gel at
room temperature to form a solid gel which then allowed to stand for 24 hours.
Percentage of silica encapsulated enzyme were analyzed by the biuret method
using spectrophotometer UV-Vis. The catalytic activity and the ability to use
repeatedly in the hydrolysis reaction of the immobilized lipase enzyme on silica
gel has been tested, as well as its stability against temperature changes and pH of
the hydrolysis reaction.
The results showed that the percentage of lipase immobilized on silica gel
is 64.024%, but after 5 washes, percentage of immobilized lipase only 28.041%.
Activity of lipase immobilized silica gel is lower than the free lipase. However,
the lipase enzyme immobilized by silica gel can be used up to 5 times in
hydrolysis reaction with the highest reaction percentage of free fatty acid product
is 17.407% and the lowest is 8.601%. Lipase immobilized silica gel is more stable
against temperature changes and pH than free lipase enzymes, each enzyme has an
optimum temperature at 35 �C and optimum pH 7.
Keywords : lipase, silica gel, immobilization, hydrolysis, palm oil.

