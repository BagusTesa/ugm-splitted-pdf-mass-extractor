Nowadays, the wedding packages ordering service in Larasati Griya Rias
dan Dekorasi only made through manual recording in the reservation book. This
recording method is quite suffice enough, but less effective and efficient because
it cannot be transformed into a report. Furthermore, it is harder to check all the
transaction. Therefore, a web based information system of wedding packages
ordering service is needed for Larasati Griya Rias dan Dekorasi in order to
improve service for customerís satisfaction. The purpose of making this
information system is to provide a solution to the problems mentioned above.
This web based information system is expected to ease the process of
ordering a wedding package into something that is simple, straightforward, and
practical needs. It is essential, especially to cope with competition in the world of
wedding organizer.
Also this information system can help spreading information, managing
order, and help to ease customers to do the ordering process. This information
system is created using HTML programming language, Javascript, jQuery PHP,
and MySQL database server.

