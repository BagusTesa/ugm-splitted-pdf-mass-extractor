Pemantau kondisi lingkungan dari udara dengan menggunakan Unmanned
Aerial Vehicle (UAV) yang dilengkapi dengan kamera diperlukan suatu perangkat
lunak yang mampu mengirimkan data ke Ground Control Station (GCS). Selain
itu, diperlukan suatu pengiriman perintah autopilot agar UAV dapat menuju ke
titik pemantauan. Berdasarkan permasalahan diatas dibuatlah pengembangan
perangkat lunak agar GCS mampu membantu misi pemantauan.
Antarmuka GCS ini dibuat dengan menggunakan Microsoft Visual Studio
2010 dengan bahasa C# yang dijalankan pada laptop. Komunikasi antara GCS dan
UAV menggunakan wi-fi dengan spesifikasi IEEE 802.11n. Unit pengiriman dari
UAV terdiri dari modul ADAHRS dan webcam yang terhubung dengan
Pengujian dilakukan dengan melakukan penerimaan data ADAHRS,
penerimaan video streaming dan pengujian pembacaan perintah autopilot.
Kemampuan maksimal penerimaan data dari ADAHRS adalah 17 paket tiap
detiknya. Pemantauan video streaming optimal dilakukan pada jarak 80 m dengan
resolusi 320x240 pixel pada ketinggian 1 m. Modul ADAHRS mampu membaca
perintah autopilot dan membutuhkan waktu 8.78 � 0.01 ms untuk perintah
menyalakan autopilot dan 6.75 � 0.01 ms untuk perintah mematikan autopilot
(manual).
Kata Kunci: UAV, GCS, Raspberry Pi, wi-fi, Video streaming, Autopilot
In order to monitor the environmental conditions of the air by using an
Unmanned Aerial Vehicle (UAV) equipped with a camera, we need a Ground
Control Station (GCS) equipped with software capable of sending data to the
GCS. In addition, we need a delivery order UAV autopilot command to go to the
monitoring point.  Based on the above issues made software development so that
GCS is able to assist the monitoring mission.
GCS interface is built using Microsoft Visual Studio 2010 with C #
language and running on a laptop. Communication between GCS and UAV use
wi-fi with the IEEE 802.11n specification. Unit shipments of UAV consists of
modules ADAHRS and webcam connected to the Raspberry Pi.
Testing is done by performing data reception ADAHRS, video streaming
and acceptance testing of the autopilot commands readings. Maximum capability
of receiving data from ADAHRS is 17 packets per second. Monitoring of
streaming video with optimum distance is 80 m with a resolution of 320x240
pixels at 1 m height. ADAHRS Module is able to read the commands the autopilot
and took  8.78 � 0,01  ms to turn on the autopilot commands and 6.75 � 0,01   ms
to turn off the autopilot commands (manual).
Keyword: UAV, GCS, Raspberry Pi, wi-fi, Video streaming, Autopilot.

