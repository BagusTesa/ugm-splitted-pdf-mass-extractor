oleh
Telah dibuat purwarupa sistem deteksi bara rokok dengan menggunakan
sensor api berbasis ATMega32 yang digunakan sebagai sarana himbauan atau
informasi kepada publik mengenai kawasan bebas rokok. Dengan dibuatnya alat
ini akan memudahkan para perokok untuk mengetahui bahwa kawasan atau
ruangan ini adalah ruangan bebas rokok. Alat ini di kemas secara menarik dengan
tampilan dot matrix sebagai daya tarik tersendiri sehingga para perokok dapat
mengikuti himbauan yang dituliskan pada dot matrix yang berupa himbauan untuk
tidak merokok di tempat ini.
Alat ini terdiri dari sensor api, driver dot matrix, papan informasi dot
matrix dan mikrokontroler ATMega32 sebagai pusat pengontrolan sistem alat ini.
Sensor api sangat peka terhadap panas, maka sensor ini digunakan sebagai alat
deteksi api dan bara rokok.
Dari percobaan yang telah dilakukan, sensor api hanya dapat mendeteksi
api maksimal berjarak 100 cm dengan lebar derajat pengukurannya sebesar 60
derajat. Ruang lingkup kerja alat ini hanya dapat digunakan pada ruangan yang
memiliki lebar 4 m
dengan empat buah sensor yang di pasang pada setiap sisi
ruangan.
Kata kunci: sensor api, BASCOM AVR, flame sensor, dot matrix, buzzer
Written by
.
Cigarette ember prototype systems detection has been made by using
sensor-based fire ATMega32 used as tools of appeal or information to the public
regarding the non-smoking area. The establishment of these tools will make
smokers easier to know that this is the area or room with air conditioning
system. This tool contains with attractive Dot matrix display appeal so that
smokers can follow a written rules to not smoke in this place on a dot matrix
form.
This tool consists of a flame sensor, driver dot matrix, dot matrix
information boards and ATmega32 microcontroller as the central control system.
Flame sensor is very sensitive to heat so it is used as a means of fire detection
and smoking embers.
By the experiments that have been carried out, the sensor can only detect
100 cm fire flame range with a maximum width of 60 degree measurement. The
scope of work of these tools can only be used in a room that has a width of 4 m
with four sensors installed on each side of the room.
Keywords: flame sensor, BASCOM AVR, flame sensor, dot matrix, buzzer

