Analysis of the acoustic impedance inversion and seismic attribute are
usually used to map the subsurface conditions. In this research , these methods
are used to determine the distribution of porosity and lithology along the study
area. From those results, it expected to know the prospect location of injection
wells.
Acoustic impedance inversion method used to distinguish qualitatively
between the porous and non porous limestone. After knowing the porous area, to
determine the range of porosity qualitative analyasis is perfomed values prospect
area. Instantaneous frequency attribute processing is performed to determine the
instantaneous frequency of the dominant in the study area. Attribute processing is
also conducted by using the spectral decomposition attributes on dominant
instantaneous frequency.
Based on the result of acoustic impedance inversin, obtained the range of
acoustic impedance in research areas between 8000 - 17000 ((m/s).(gr/cc))  areas
with low acoustic impedance between 8000 - 10000 ((m/s).(gr/cc)) oriented
northeast - southwest. In the study area, the porosity value between 15-24 % with
the dominant instantaneous frequency at 23 Hz. In the spectral decomposition
results at 23 Hz has the same distribution pattern qualitatively with porosity. From
the results, it is recommended to FM-6, FM-T6, FM-T40, FM-T61 and FM-T63
wells as injection wells .

