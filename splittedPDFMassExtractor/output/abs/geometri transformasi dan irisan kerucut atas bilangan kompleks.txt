Di dalam Tugas Akhir ini dibahas mengenai geometri transformasi dan
irisan kerucut atas bilangan kompleks. Pembahasan diawali dengan definisi
transformasi, yang diteruskan dengan transformasi secara khusus yaitu translasi,
rotasi, homoteti, refleksi dan inversi. Kemudian dilanjutkan dengan pembahasan
irisan kerucut yaitu lingkaran, parabola, ellips dan hiperbola. Dari pembahasan-
pembahasan tersebut ditemukan teorema-teorema yang merupakan penjelasan
tentang geometri transformasi dan irisan kerucut atas bilangan kompleks serta hal-
hal yang berkaitan dengan keduanya.
This final project discusses transformation geometry and conic sections
in complex numbers. This discussion will begin by the definition of
transformation, followed by special transformation which are translation, rotation,
homothety, reflection and inversion. Then it discusses conic section which are
circle, parabola, ellipse and hyperbola. The theorems that explains transformation
geometry and conic sections in complex numbers, and the theorems related to
them will follow after the discussions mentiond above.

