Study of acoustic  impedance  inversion  method has been known and
utilized as a method in reservoir characterization, for lithology and fluid
discrimination. This Acoustic Impedance inversion has a limitation in
discriminating lithology and fluid effects, and it is often found in many cases
where the porous sand and shalestone have a similar impedance value. Because
of that reason, there is needs a new invention method that can discriminate
lithology and fluid effect better.
The simultaneous inversion method was used in this research, using angle
stack data as the input and then it was inverted simultaneously together to
produce P impedance, S impedance, and density. The P impedance and S
impedance were derived to produce lambda-mu-rho which are sensitive to the
presence of fluid. The results of the sensitivity analysis showed that parameter of
P impedance, S impedance lambda-rho and mu-rho could define lithology
differences and fluid properly. Map of inversion result  show that P impedance,
density, and lamda-rho are able to identify the zone of reservoir and fluid in the
porous sand clearly. There were three prospect areas namely Prospects-A,
prospect-B, and prospect-C in ALMULK field around FM-E1 and FM-E2 wells
that spread to the west and the north. In this area, the inversion result was were
indicated by low P Impedance, low density, low lambda-rho, high S Impedance
and high mu-rho.
Key words: Lamda-Mu-Rho, P-impedance, S-impedance, Simultaneous inversion.

