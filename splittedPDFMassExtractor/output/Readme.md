* **dal** contains a Daliance-based (Daliance 1b to be precise) crawled pdfs, includes language analysis to discern english abstract page and indonesia abstract page. It also uses strict k-first and k-last line to wipe out unnecessary chapter title and author name - including NIM.
* **abs** uses Daliance new regex cleansing rule, but disregard k-first and k-last line rule.
* **c1c5** just like **abs** but for chapter 1 (introduction) and chapter 5 (conclusion).

ps. **dal** achieved by rip-molding this source code with Daliance 1b components
```
private class crawlerEmbedded {

  private static final Log LOG = LogFactory.getLog(crawlerEmbedded.class);

  private class Entity{
      private final String dirName;
      private final List<String> fileTarget;

      public Entity(String dirName){
          this.dirName = dirName;
          this.fileTarget = new LinkedList<>();
      }

      @Override
      public String toString(){
          String out = this.dirName + "(" + fileTarget.size() + ")";

          for(String f: fileTarget){
              out += "\n" + f;
          }

          return out;
      }

      /**
       * @return the dirName
       */
      public String getDirName() {
          return dirName;
      }

      /**
       * @return the fileTarget
       */
      public List<String> getFileTarget() {
          return fileTarget;
      }
  }


  private class Crawler {
      private final Set<String> pickList = new LinkedHashSet<>();
      private final String separator = "-";

      public Crawler(){
          pickList.add("chapter1.pdf");pickList.add("chapter5.pdf");
          pickList.add("introduction.pdf");pickList.add("conclusion.pdf");
          pickList.add("abstract.pdf");
      }

      public Crawler(Set<String> pickList){
          for(String pick: pickList){
              //ensure its a lower-cased
              this.pickList.add(pick.toLowerCase());
          }
      }

      private Queue<Entity> discern(String path){
          Queue<Entity> out = new LinkedList<>();

          File accessor = new File(path);

          if(accessor.isDirectory()){
              String dirName = accessor.getName();
              File[] lf = accessor.listFiles();
              String lastPattern = null;
              Entity lastEntity = null;

              int itera = 0;
              for(File f: lf){
                  String fpth = f.getAbsolutePath();
                  String fname = f.getName();

                  String newPattern = fname.substring(0, fname.lastIndexOf(this.separator)).toLowerCase();
                  String flTitle = fname.substring(fname.lastIndexOf(this.separator) + 1).toLowerCase(); //beware of extension or file type

                  if(lastPattern == null){
                      //seeding
                      lastPattern = newPattern;
                      lastEntity = new Entity(dirName);

                      if(this.pickList.contains(flTitle)){
                          lastEntity.getFileTarget().add(fpth);
                      }
                  }
                  else{
                      //the following foo-bar
                      if(lastPattern.equalsIgnoreCase(newPattern)){
                          //still the same pattern
                          if(this.pickList.contains(flTitle)){
                              lastEntity.getFileTarget().add(fpth);
                          }
                      }
                      else{
                          //new pattern (again)
                          //first, put the ol one..
                          out.add(lastEntity);
                          itera++;

                          //then put the new one
                          lastEntity = new Entity(dirName + "[" + itera + "]");

                          if(this.pickList.contains(flTitle)){
                              lastEntity.getFileTarget().add(fpth);
                          }
                      }
                  }
              }

              //ensure the last one inserted
              out.add(lastEntity);
          }

          return out;
      }

      /**
       * <p>Takes directory path to be scanned.
       * This method assumes the directory contains directories that
       * named after the research report title.</p>
       *
       * @param path
       * @return
       */
      public Queue<Entity> crawl(String path){
          Queue<Entity> out = new LinkedList<>();

          File accessor = new File(path);
          if(accessor.isDirectory()){
              File[] lfiles = accessor.listFiles();
              for(File f: lfiles){
                  if(f.isDirectory()){
                      out.addAll(this.discern(f.getAbsolutePath()));
                  }
              }
          }

          return out;
      }
  }

  private class OutputWriter{
      private String outputPath = "output";

      public OutputWriter(String outputPath){
          this.outputPath = outputPath;
      }

      public void output(String docTitle, String fileName, String output){
          String path = outputPath + "\\" + docTitle + "\\";
          try {
              File i = new File(path);
              i.mkdirs();

              crawlerEmbedded.LOG.info(path + fileName);

              PrintWriter wr = new PrintWriter(path + fileName + ".txt");
              wr.write(output);
              wr.close();
          } catch (FileNotFoundException ex) {
              //some message?
              crawlerEmbedded.LOG.error(path);
          }

      }

      /**
       * @return the outputPath
       */
      public String getOutputPath() {
          return outputPath;
      }

      /**
       * @param outputPath the outputPath to set
       */
      public void setOutputPath(String outputPath) {
          this.outputPath = outputPath;
      }
  }

  private void runMe(String sourcePath, String targetPath){
      Crawler cr = new Crawler();
      OutputWriter wr = new OutputWriter(targetPath);
      Queue<Entity> que = cr.crawl(sourcePath);

      AbstractExtractor abs = new AbstractExtractor();
      ChapterComponentExtractor cce = new ChapterComponentExtractor();

      String separator = System.getProperty("line.separator");

      for(Entity e: que){
          crawlerEmbedded.LOG.info("Begin processing: \"" + e.dirName + "\"");
          for(String tgt: e.fileTarget){
              InputStream is = null;
              try {
                  is = new FileInputStream(new File(tgt));
                  DocumentModel _tmp = new DocumentModel("E1");
                  if(tgt.contains("abstract")){
                      _tmp = abs.modelize(_tmp, is);
                      if(_tmp.getAbstract_en() != null){
                          wr.output(e.getDirName(), "abstract_en", _tmp.getAbstract_en());
                      }
                      if(_tmp.getAbstract_id() != null){
                          wr.output(e.getDirName(), "abstract_id", _tmp.getAbstract_id());
                      }
                  }
                  else if(tgt.contains("chapter1") || tgt.contains("introduction")){
                      _tmp = cce.modelize(_tmp, is);
                      List<ChapterComponent> chaps = _tmp.getChapters();
                      String out = "";
                      for(ChapterComponent ch: chaps){
                          for(SubChapter sc: ch.getComponents()){
                              out += sc.getStructureContent() + separator;
                          }
                      }
                      wr.output(e.getDirName(), "ch1", out);
                  }
                  else if(tgt.contains("chapter5") || tgt.contains("conclusion")){
                      _tmp = cce.modelize(_tmp, is);
                      List<ChapterComponent> chaps = _tmp.getChapters();
                      String out = "";
                      for(ChapterComponent ch: chaps){
                          for(SubChapter sc: ch.getComponents()){
                              out += sc.getStructureContent() + separator;
                          }
                      }
                      wr.output(e.getDirName(), "ch5", out);
                  }
              } catch (FileNotFoundException | DalianceException ex) {
                  crawlerEmbedded.LOG.error(ex);
              } finally {
                  try {
                      if(is != null)
                          is.close();
                  } catch (IOException ex) {
                      crawlerEmbedded.LOG.error(ex);
                  }
              }
          }
      }
  }


  /**
   * @param args the command line arguments
   */
  public static void main(String[] args) {
      crawlerEmbedded me = new crawlerEmbedded();
      me.runMe("E:\\Temporary Folder\\Tugas Akhir\\sampl-bot-dumps", "E:\\Temporary Folder\\out");

  }
}
```
