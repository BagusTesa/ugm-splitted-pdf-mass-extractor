MICROCONTROLLER ATMEGA16
A prototype of human heartbeat counter system has been created using
acoustic stethoscope based on microcontroller ATMega16. It designed for ease
measuring the human heartbeat. The result of data calculation was displayed on
the LCD (Liquid Crystal Display) units in Beat per Minutes (Bpm).
The design of this system is using minimum system ATMega16, Electric
Mic Condensor as the sensor of sound, RTC (Real Time Clock), and LCD. In this
system, the result displayed on the LCD and not saved in a storage.
Overall, this system can perform data retrieval heart rate per 30 seconds to
determine the second heartbeat. Furthermore, the heart rate multiplied by the
number 60 to get the number of heartbeats. The error of reading the heartbeat in
this system is 0.45 Bpm.
Keyword: heartbeat, stethoscope, microcontroller, LCD

