1.1. Latar Belakang Masalah
Pada sebuah penelitian, apabila suatu populasi yang ingin diteliti berjumlah
sangat besar, maka akan membutuhkan biaya dan waktu yang besar pula. Hal ini
tidak efisien. Dengan demikian, diambil sampel dari populasi tersebut, untuk
diteliti sehingga hasil dari penelitiannya dapat mewakili populasi.
Sampel yang baik adalah sampel yang dapat merepresentasikan
populasinya. Untuk memperoleh sampel yang representatif, ada dua kriteria yang
harus dipenuhi yaitu teknik penarikan sampel yang sesuai dan perhitungan ukuran
sampel terkecil yang harus diikutkan dalam penelitian. Untuk itu diperlukan
perhitungan ukuran sampel minimum. Ukuran sampel adalah banyaknya individu
yang diamati dalam suatu penelitian atau percobaan. Perhitungan ukuran sampel
dilakukan berdasarkan beberapa kriteria statistik mengendalikan tipe I dan / atau
kesalahan tipe II . Kesalahan tipe dua adalah menerima Ho salah, sedangkan
power adalah penolakan Ho salah. Perhitungan ukuran sampel yang valid hanya
dapat dilakukan berdasarkan uji statistik yang sesuai untuk hipotesis yang dapat
mencerminkan tujuan penelitian berdasarkan desain penelitiannya. Hal ini berarti,
setiap hipotesis memiliki kebutuhan ukuran sampel yang berbeda untuk mencapai
power yang diinginkan.
Dalam studi klinis, sering kali ditemui data longitudinal yaitu sekumpulan
data yang terdiri dari pengukuran berulang variabel respon, dan sekumpulan
variabel bebas pada sejumlah subjek/unit. Data tersebut mengkombinasikan
elemen dari data multivariat dan runtun waktu, dalam data longitudinal terdapat
saling ketergantungan antar pengukuran dan merupakan data runtun waktu yang
pendek. Studi longitudinal didefinisikan sebagai studi yang mempelajari
perubahan respon untuk setiap unit eksperimen yang diobservasi pada dua waktu
yang berbeda atau lebih.

