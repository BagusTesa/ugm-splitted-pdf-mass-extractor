LIBRARY INFORMATION SYSTEMS OF SENIOR HIGH SCHOOL 3
Bina Tunas library is a library was managed by Senior High School  3
Pemalang. The main function of this library is to provide services to members,
particularly related to students lending. Non computerized system is still used in this
library to handle data management and circulation processes, this fact enable the
occurrence of violations and disadvantages for both students and the library. The
problem start from a slow circulation, sloppy data management, inaccurate fines and
possibility of book losing.
According to the issues, it is necessary to build a system that  capable to
handle data management and circulation of books, so the library?s service could
become more optimal. The system is capable to handle data management process,
integrated transaction processes, lending activity report, fines report  and report of
books collection.
Library Information Systems of Senior High School 3 Pemalang was built
using laravel framework and MySQL database. Expected outcomes of this research is
the library service would be more efficient when the system is implemented.
Keywords: Library, Data Management, Circulation, Information System

