REDUCING AGENT AND  THE INFLUENCE OF N2 GAS ADDITION
Nurrohmah Dwi mahesti
Study of reduction method using organic acid from starfruit, orange peel,
spinach wastes as reducing agent to recovery Ag metal from photography wastes has
been done. The research start with dissolution time and temperature optimization
from starfruit, orange peel, spinach leaf and spinach stem wastes, and then acids
identification and determine the concentration with titrimetric method.  The reduction
process was done by mixing the photography waste and organic acid solution with
mole ratio variation for 24 hours. In this research the influence of N2 gas addition on
Ag(I) ions reduction effectiveness was also studied. The reduction effectiveness is
expressed as % reduction of Ag(I) ions, and it is calculated based on the difference
between the number of Ag(I) ions before and after reduced, using AAS instrument.
The solid reduction results were analyzed using XRD and TEM instruments.
The results showed that the dissolution of organic acids from starfruit, orange
peel and spinach wastes in an aqueous medium optimum at 90 minutes and 60�C.
Concentration of total acid from starfruit, orange peel, spinach leaf and spinach stem
wastes from the optimum dissolution conditions were 0.0141, 0.0166, 0.0277 and
0.0185 M, and the reducing acid were 0.0128, 0.0147, 0.0193 and 0.0128 M,
respectively. The result of reduced Ag(I) ions with starfruit, orange peel, spinach leaf
and spinach stem wastes with 1:3 mole ratio were 67.83, 76.43, 96.87 and 96.56%,
respectively. Based on XRD and TEM analysis of the solid reduction results without
N2 gas addition showed that the solids are Ag2O3, Ag2S, S and Ag, and then with N2
gas addition could decrease amount of  Ag2O3 and increase amount of  Ag metal.
Keyword : Ag(I), Reduction, Organic Acids, N2 Gas, photography wastes

