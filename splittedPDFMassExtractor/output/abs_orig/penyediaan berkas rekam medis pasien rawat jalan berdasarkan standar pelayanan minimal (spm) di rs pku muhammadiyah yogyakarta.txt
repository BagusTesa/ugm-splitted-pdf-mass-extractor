Latar Belakang: Pelayanan yang cepat dan tepat merupakan keinginan semua
konsumen baik pemberi pelayanan maupun penerima pelayanan. Kecepatan
penyediaan berkas rekam medis ke klinik juga dapat menjadi salah satu indikator
dalam mengukur kepuasan. Semakin cepat rekam medis sampai ke klinik maka
semakin cepat pelayanan yang dapat diberikan kepada pasien. Di RS PKU
Muhammadiyah Yogyakarta telah mengeluarkan Standar Pelayanan Minimal
dalam penyediaan berkas rekam medis sampai ke poliklinik yaitu maksimal 10
menit. Akan tetapi masih dijumpai keterlambatan dalam penyediaan berkas
rekam medis, sehingga perawat klinik mengambil sendiri berkas rekam medis
kebagian pendaftaran.
Tujuan:  Mengetahui proses penyediaan berkas rekam medis , mengetahui rata-
rata kecepatan dalam penyedian berkas rekam medis pasien rawat jalan terkait
dengan SPM serta mengetahui faktor-faktor yang mempengaruhi kecepatan
dalam penyediaan berkas rekam medis.
Metode Penelitian: Jenis penelitian ini adalah penelitian deskriptif dengan
pendekatan kualitatif dengan pengumpulan data secara cross sectional. Objek
pada penelitian ini adalah penyediaan berkas rekam medis pasien rawat jalan
sebanyak 99 berkas. Metode pengumpulan data dengan metode observasi dan
wawancara.
Hasil: Hasil dari penelitian ini adalah rata-rata kecepatan dalam penyediaan
berkas rekam medis pasien rawat jalan adalah 14,52 menit dan prosentase
keterlambatan 76,76% tepat waktu 23,23% dan faktor-faktor yang
mempengaruhinya adalah faktor machine (alat), Man (manusia), Method (cara) ,
Environment  (lingkungan)
Kata kunci: penyediaan berkas rekam medis, Standar Pelayanan Minimal
vii
ABSTRACT
Background: service prompt is desire all consumers good giver service and
receiver service. Speed provision file rollin medical clinic to can be also one
indicator in measuring satisfaction. The faster rollin medical to the clinic the more
rapid service which may given to patients. At rspkumuhammadiyahjogjakarta has
issued minimum service standards in providing file rollin medical get to polyclinic
namely maximum 10 minutes. But still finds delay in providing file rollin medical,
so that nurse clinic helped himself to file get chair rollin medical registration.
Purpose: knowing process of providing file rollin medical, knowing average
speed in penyedian file rollin medical an outpatient relating to spm and knowing
factors affecting speed in providing file rollin medevac.
Method: type this research is descriptive by approach qualitative data with in
cross sectional.objek on this research is provision file rollin medical an outpatient
99 document. A method of data by method observation and interview.
Results: the results of this research is the average speed in providing file
outpatient medical record is 14,52 minutes and percentage of delays 76,76%
23,23% timely and factors which affected it is machine (tool), a Man (human),
Method (a way), Environment (environment).
Keywords:  providing file rollin medical, minimum service standars.

