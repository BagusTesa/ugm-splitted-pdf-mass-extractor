Latar belakang: Sensus harian rawat inap di RSUD Kota Yogyakarta dilakukan
oleh perawat di ruang perawatan secara manual. Walaupun menu sensus harian
rawat inap sudah terdapat di dalam SIMRS tetapi belum dimanfaatkan secara
maksimal karena dalam pelaksanaannya masih ditemukan kendala, yaitu
ketidaksesuaian sensus harian rawat inap manual dengan sensus harian rawat
inap yang ada di SIMRS. Hal tersebut dimungkinkan disebabkan oleh faktor SDM.
Oleh karena itu, diperlukan evaluasi kepada sumber daya manusia sebagai
pengguna dalam mengimplementasikan Sistem Informasi Rumah Sakit terkait
Sensus Harian di RSUD Kota Yogyakarta.
Tujuan: Mengetahui persepsi pengguna dalam mengimplementasikan Sistem
Informasi Manajemen Rumah Sakit terkait sensus harian di RSUD Kota
Yogyakarta.
Metode Penelitian: Jenis penelitian ini yaitu deskriptif dengan menggunakan
pendekatan kualitatif. Rancangan penelitian adalah cross-sectional. Teknik
pengumpulan data dalam penelitian ini adalah wawancara, observasi dan studi
dokumentasi.
Hasil: Pengolahan sensus harian rawat inap di RSUD Kota Yogyakarta di-input
pertama oleh petugas pendaftaran rawat inap secara komputerisasi melalui
SIMRS, kemudian perawat membuat SHRI secara manual lalu diserahkan ke
Instalasi Rekam Medis untuk diolah. Pengolahan sensus harian rawat inap belum
sesuai dengan prosedur pekerjaan yang terdapat di SOP atau Prosedur Tetap
RSUD Kota Yogyakarta. SIMRS terkait SHRI di RSUD Kota Yogyakarta
bermanfaat bagi pengguna karena menjadikan pekerjaan lebih cepat, bermanfaat,
mempertinggi efektifitas, mengembangkan kinerja pekerjaan, dan mempermudah
pekerjaan. Namun untuk produktifitas pekerjaan tidak bertambah dan relative tetap
karena kualitas data yang tidak sesuai dengan kualitas data sesungguhnya
sehingga tidak dapat menjadi data acuan. SIMRS terkait SHRI di RSUD Kota
Yogyakarta memberikan kemudahan bagi penggunanya karena mudah dipelajari,
mudah digunakan, menambah keterampilan pengguna dan fleksibel. Namun,
untuk indikator controllable dan clear & understandable belum dapat terpenuhi
karena tidak adanya kebijakan mengenai alur pemulangan pasien rawat inap yang
benar. Sikap pengguna terhadap Sistem Informasi Manajemen Rumah Sakit di
RSUD Kota Yogyakarta sudah dinilai baik dan pengguna mau menggunakannya.
Tetapi Sistem Informasi Manajemen Rumah Sakit terkait sensus harian rawat inap
belum cukup memuaskan penggunanya. Minat perilaku pengguna Sistem
Informasi Manajemen Rumah Sakit terkait sensus harian rawat inap di RSUD Kota
Yogyakarta yaitu ingin mengikuti pelatihan mengenai SIMRS.
Kata Kunci: Evaluasi, user, SIMRS, Sensus Harian Rawat Inap, TAM
viii
ABSTRACT
Background: Inpatient daily census in RSUD Yogyakarta done by a nurse in a
treatment room manually. Although the menu daily inpatient census was found in
SIMRS but not optimally utilized for the implementation constraints are found, that
is daily inpatient census mismatch manual with daily inpatient census is in SIMRS.
This is possible due to the human factor. Therefore, it is necessary to evaluate the
human resources as the user in the implementation of Hospital Management
Information System related hospital inpatient daily census in Local Government
General Hospital Yogyakarta.
Goal: To knowing the user perception in implementing management information
system-related hospital inpatient daily census in Local Government General
Hospital Yogyakarta.
Method: This type of research is descriptive qualitative approach. The study
design was cross-sectional. Data collection techniques in this study were
interviews, observation, and study documentation.
Result: Daily census of inpatient treatment in Yogyakarta in-RSUD first input by
the staff in computerized inpatient register through SIMRS, then the nurses make
SHRI manually and then submitted to the Installation Medical Record to be
processed. Daily census of inpatient treatment not in accordance with the
procedures contained in the SOP job or Procedures RSUD Yogyakarta. SIMRS
related SHRI in RSUD Yogyakarta useful to users because make work more
quickly, useful, enchance effectiveness, improve job performance, and makes job
easier. However, for productivity and employment hasn�t increased due to the
relatively fixed data quality doesn�t match the quality of real data so it can�t be a
reference data. SIMRS related SHRI in RSUD Yogyakarta makes it easy for users
because it is easy to learn, easy to use, easy to become skillful, and flexible.
However, for the indicator clear and understandable controllable and can�t be
fulfilled because of the absence of a policy regarding the return flow inpatients
correct. The attitude of the user of the Hospital Management Information System
in Yogyakarta City Hospital has been rated good and the users want to use. But
the Hospital Management Information System related inpatient daily census has
not been enough to satisfy users. Interest in user behavior Hospital Management
Information System related daily census of inpatient hospital Yogyakarta that want
to follow training on SIMRS.
Keywords: Evaluation, User, Human Resources, Daily Inpatient Census, TAM

