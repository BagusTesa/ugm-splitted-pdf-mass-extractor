This system was made because of the state of agriculture in Indonesia,
especially the use of a greenhouse farm. Currently many farmers in indonesia using a
pipe directly connect to the tap water or using sprinklers direct interest in flush to the
plants. Therefore created a prototype plant watering system automatically and the
expected with this prototype can be developed better to help the farmers in the
process of watering plants automatically.
Automatic plant watering system using arduino uno, soil moisture sensor for
measuring the level of soil moisture in potted plants, dht11 sensor for measuring the
air temperature and the air humidity in potted plants, grove relay as the switch to turn
on and off the water pump and the LCD Viewer 16x2 as the value of the soil moisture
sensors and sensor dht11. The overall test results show the automatic sprinkler system
is capable of irrigating the soil on the plant in the pot. When soil moisture is below
300, the water pump will turn on water the soil in the flush pipe into the pot and when
soil moisture reaches 700 water pump will stop watering the soil at the plant.
Keywords: Automatic Watering System, Arduino UNO, Soil Moisture Sensor,
DHT11 Sensor, Grove Relay, LCD 16x2.

