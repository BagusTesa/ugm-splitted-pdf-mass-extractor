by :
Zupparella Delicious Zuppa is a Restaurant a themed angkringan that provides
French-style soup covered with whole-wheat pastry and also sells French-style foods such
as spaghetti, Chicken Steak, Beef Steak, salad, and pasta. Recording the transactions in
Zupparella still uses papers, ordering to the kitchen also uses oral orders.
Based on these circumstances, he built a Zupparella Delicious Zuppa Booking
Application and Desktop-Based Android, built using the Java Android programming
language, Java, XML, MySQL, Jasper iReports.
This application helps the daily transaction data management with the help of
WiFi networking as a means to integrate data between server and client, and the
application can print receipts which are intended for kitchen orders and are also devoted
to customer transactions. So with this app in zupparella transaction data management can
be more accurate.
Keywords: Booking Application, Android, Zupparella Delicious Zuppa

