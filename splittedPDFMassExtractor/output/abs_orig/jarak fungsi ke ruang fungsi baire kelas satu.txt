Jarak suatu titik x ke himpunan A dalam suatu ruang metrik dinyatakan se-
bagai supremum jarak titik tersebut ke setiap titik di dalam himpunan A. Dalam
tesis ini, akan dibahas jarak fungsi f : X ? E ke B1(X,E), dengan B1(X,E)
menyatakan ruang fungsi Baire kelas satu dari ruang metrik X ke ruang Banach E.
Untuk mencari estimasi jarak tersebut, akan digunakan dua konsep, yaitu konsep in-
deks fragmentability fungsi dan konsep osilasi fungsi. Sebagai salah satu akibat dari
hasil estimasi dengan menggunakan konsep osilasi, diperoleh bahwa untuk setiap
fungsi kontinu terpisah (separately continuous) f : X �K ? R dengan X ruang
topologi completely metrizable dan K ruang topologi kompak, terdapat himpunan
D ? X sehingga D himpunan G? dan dense di X serta f kontinu pada D �K.
xi
ABSTRACT
THE DISTANCE OF A FUNCTION TO SPACES OF BAIRE
ONE FUNCTIONS
By
I MADE ARI SUSENA
11/324312/PPA/03675
The distance of a point x to a set A in a metric space is defined by the supre-
mum of the distances of x to each point in the set A. In this thesis, we will discuss
about the distance of a function f : X ? E to B1(X,E), where B1(X,E) denotes
the space of Baire one functions from a metric space X to a Banach space E. In
order to find the distance estimation, we will use two concepts, that are concept
of index fragmentability and concept of oscillation function. As a consequence of
the estimation result by using oscillation concept is for every separately continuous
function f : X � K ? R where X is a completely metrizable topological space
and K is a compact topological space, there is a G?-dense set D ? X such that f
continuous on D �K.

