Permasalahan utama untuk mengidentifikasi susunan DNA pada genome eu-
karyoit yang diolah melalui proses komputasi belum terpecahkan. Metode Hidden
Markov Model diterapkan untuk dapat mengatasi permasalahan tersebut, dimana da-
pat mengklasifikasi susunan data DNA. Penelitian ini mengusulkan penerapan me-
tode Hybrid HMM dengan SOM untuk mengatasi permasalahan identifikasi wilayah
pada genome eukaryoit. Penerapan metode SOM diterapkan untuk mengatasi keter-
batasan pada metode HMM terutama kasus overfitting. Data yang dipergunakan pada
penelitian ini ialah genome tumbuhan dengan nama latin Arabidopsis Thaliana.
Akurasi metode Hybrid HMM dengan SOM akan dibandingkan dengan me-
tode HMM akan dievaluasi pada dua level, yakni level nukleotida dan exon. Pada
level nukleotida akan menguji dari segi sensitivitas(Sn), spesitiviti(Sp) dan koefisi-
en korelasi(CC), sama halnya pada level exon yang akan menguji dari segi sensi-
tivitas(Sn) dan spesitiviti (Sp). Hasil pengujian dengan menerapkan metode HMM
menunjukkan nilai SnHMM = 94% (level nukleotida), SpHMM = 90% (level nukle-
otida), CCHMM = 2.9%, SnHMM = 82% (level exon), dan SnHMM = 90% (level
exon). Berbeda dengan hasil pengujian pada metode Hybrid HMM dengan SOM
menunjukkan nilai SnHybrid = 98% (level nukleotida), SpHybrid = 94% (level nukle-
otida), CcHybrid = 2.6%, SnHybrid = 88% (level exon), dan SnHybrid = 94% (level
exon).
Kata-kata kunci : Hybrid HMM/SOM, Self Organizing Map (SOM), HMM,
Gene prediction
xv
ABSTRACT
HYBRID HMM AND SOM METHOD FOR IDENTIFYING
PROTEIN CODING REGIONS IN THE GENOME OF
ARADOPSIS THALIANA
By
Muhammad Fajar Hartanto
11/322972/PPA/03598
Main issues to identify the order DNA in genome eurkaryote are processed
through the process computing has not been solved. The method Hidden Markov can
overcome Model is applied to these issues, which can be classify, and dis-integrity
data structure DNA. This Research suggests the application methods Hybrid well
with SOM to address the problems in the region identification genome eurkaryote.
The SOM algorithm is applied to overcome the shortage method well especially over-
fitting case. Data that is used in this research is genome plants with latin name Ara-
bidopsis Thaliana.
Accuracy of Hybrid method HMM with SOM will be compared with the
HMM method will be evaluated on two levels, the level nucleotide and exon. At
a level nucleotide will test in terms of sensitivity(Sn), specificity(Sp) and the drag
coefficient correlation(CC), in the same thing at a level exon that will test in terms
of sensitivity(Sn) and specificity (Sp). Test result by applying methods HMM in the
SnHMM = 94% (level nukleotida), SpHMM = 90% (level nukleotida), CCHMM =
2.9%, SnHMM = 82% (level exon), and SnHMM = 90% (level exon). Different
with test result in Hybrid method HMM with SOM shows the SnHybrid = 98% (level
nukleotida), SpHybrid = 94% (level nukleotida), CcHybrid = 2.6%, SnHybrid = 88%
(level exon), and SnHybrid = 94% (level exon).
Keywords : Hybrid HMM/SOM, Self Organizing Map (SOM), HMM, Gene
prediction

