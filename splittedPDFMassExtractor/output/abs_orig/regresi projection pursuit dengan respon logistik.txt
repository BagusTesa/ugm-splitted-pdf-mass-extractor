Banyak teknik nonparametrik, seperti Running mean, Polynomial, Nearest
Neighhour, bekerja dengan prinsip rata�rata persekitaran yang tidak dapat bekerja
dengan baik untuk data berdimensi tinggi (cursed of dimensionality). Reduksi
dimensi menjadi salah satu solusi permasalahan tersebut. Proyeksi yang
mempertahankan struktur data, dapat mereduksi dimensi data. Pemikiran ini
menjadi ide dasar Projection Pursuit Regression (PPR).
Di dalam kerangka kerjanya,  PPR melakukan smoothing terhadap data
setelah mengalami proyeksi. Pemilihan teknik smoothing yang memiliki akurasi
yang lebih baik  akan mempengaruhi PPR untuk memberikan aproksimasi  bidang
permukaan fungsi-fungsi non linier, terlebih lagi untuk data yang variabel
responnya logistik (0 atau 1)
Kata kunci : Regresi, Smoothing, Projection pursuit, respon logistik.
xI
xii
ABSTRACT
PROJECTION PURSUIT REGRESSION WITH RESPONSE LOGISTIC
by
Kristianus Boby Anggiat Marito Siahaan
11/322685/PPA/03574
There are many non-parametric techniques, such as Running-mean,
Polynomial, Nearest Neighbour, based on local averaging which is can not work
well for high-dimensional data (cursed of dimensionality). Dimension reduction
become a solution to these problems. Projection which maintains data structures,
can reduce the dimension of data. This is the basic idea of Projection Pursuit
Regression (PPR).
Within its framework, PPR perform smoothing technique to smooth the
data after it were projected. The choice of smoothing technique that has better
accuracy will affect the PPR to provide a surface approximation of nonlinear
functions, especially for data with logistics response (0 or 1).
Keywords :  Regression, Smoothing, Projection Pursuit, logistic response.

