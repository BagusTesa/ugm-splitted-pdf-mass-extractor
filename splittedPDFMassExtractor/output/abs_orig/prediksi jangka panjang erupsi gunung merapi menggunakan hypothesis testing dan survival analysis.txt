Long-term forecasting of volcanic eruption is an effort to mitigate the
volcanic hazard, especially in the volcano�s inhabited slope area, such as Merapi
Volcano. Long-term forecasting of Merapi Volcano�s consist of regimes
identification, distribution pattern indentification in each regimes, and forecasting
the time of first eruption after 2010�s eruption.
The results of KS2test with significance level (?) 0,05 on interevent time of
Merapi Volcano�s eruptions from 1768 until 2010 showed that old regime
happened before 1878, and new regime has started from 1878 until 2010. Datas in
each regimes then processed using Akaike Information Criterion (AIC). The
AIC�s processing results showed that gamma and Weibull distributions are the
most suitable patterns for Merapi�s old regime datas (AIC 95,75) and new regime
datas (AIC 160,29). These differences are the results of eruptions mechanism�s
change of Merapi Volcano from fountain-collapse-dominated eruptions to dome-
collapse-dominated eruptions. The application of Weibull fit on new regime datas
of Merapi got intercept value 1,60, Weibull scale 4,97, and Weibull shape 2,65.
These results then used to find the value of survival function, hazard rate, and
probability of eruption during 2 - 18 years after 2010�s eruption. From those
results, we can conclude that the first Merapi�s eruption after 2010�s is going to be
happen between 2014 � 2016, and the characteristics are seem to be dominated by
dome-collapse eruptions with 6 years is the longest interevent time.
Key words : long-term forecasting, hypothesis testing, Kolmogorov-Smirnov
two-sample test (KS2test), Akaike Information Criterion (AIC),
survival analysis, interevent time.

