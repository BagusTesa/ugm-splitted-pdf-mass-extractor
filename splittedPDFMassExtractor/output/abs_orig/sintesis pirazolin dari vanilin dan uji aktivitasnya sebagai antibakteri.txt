Synthesis of Pyrazoline from Vanillin and Its Activities Test as Antibacterial
Synthesis and antibacterial activity of pyrazoline from vanillin have been
investigated. Synthesis was conducted throught three steps. The first step was
methylation of vanillin to give veratraldehyde. Veratraldehyde then experienced
by aldol condensation to give chalcone. The final step was reaction of chalcone
with phenylhydrazine to give pyrazoline.
Methylation of vanillin was conducted using dimethylsulfonate (DMS)
and NaOH that added repetedly. Aldol condensation was performed by reacting
chalcone and phentlhydrazine in acetic acid glacial at 70-80
o
C for 1 hour. The
products were analyzed by FTIR, GCMS, and
1
H-NMR spectrometers.
Antibacterial test was conducted by well-methode againts gram positive
(Staphylococcus aureus, Bacillus cereus, Bacillus substilis) and negative
(Eschericia coli) bacteries, tetrasiklin and kanamisin as positive control and
dimethyl sulfonate (DMSO) as negative control.
Based on research that has been done the methylation of vanillin gave
89.61% in yield, chalcone and pyrazoline that have been formed are 81.94 and
82.72%. Antibacterial test showed that pyrazoline compound at concentration of
500 ppm only active against bacteria Bacillus substilis with DDH value 2.1 and
2.6  mm.
Keywords : pyrazoline, vertraldehyde, DMS, antibacterial, DDH

