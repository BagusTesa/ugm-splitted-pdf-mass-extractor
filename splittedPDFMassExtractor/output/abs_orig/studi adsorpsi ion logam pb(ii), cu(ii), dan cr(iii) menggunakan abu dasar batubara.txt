STUDI ADSORPSI CAMPURAN ION LOGAM Pb(II), Cu(II), DAN Cr(III)
Dalam penelitian ini telah dilakukan adsorpsi campuran ionlogam Pb(II),
Cu(II), dan Cr(III) menggunakan abu dasar batubara sebagai adsorben yang
merupakan limbah hasil pembakaran batubara dari PS. Madukismo, PT.
Madubaru Yogyakarta. Penelitian ini diawali dengan karakterisasi komponen
yang terdapat pada limbah pembakaran batubara menggunakan FTIR dan XRD.
Proses adsorpsi campuran ion logam Pb(II), Cu(II), dan Cr(III) dilakukan dengan
cara menambahkan adsorben ke dalam larutan campuran logam. Hasil yang
berupa filtrat dianalisis menggunakan Spektrofotometer Serapan Atom untuk
mengetahui konsentrasi ion logam yang tidak teradsorpsi. Pada penelitian ini telah
dipelajari pengaruh massa adsorben, pH larutan, waktu adsorpsi dan konsentrasi
awal ion-ion logam.
Hasil penelitian menunjukkan bahwa limbah pembakaran batubara berupa
abu dasar mengandung SiO2 dan Al2O3.Hasil adsorpsi campuran ion logam
terhadap 15 mL campuran logam ion Pb(II), Cu(II), dan Cr(III) masing-masing 50
mg/L,maksimum dicapai dengan penggunaan 0,200 g adsorben selama 60 menit.
Kinetika adsorpsiketiga ion logam tersebutmengikuti reaksi pseudo orde 2 Ho
dengan konstanta laju adsorpsi untuk Pb(II)sebesar 0,031 g mg-1 menit-1, untuk
Cu(II)0,418 g mg-1 menit-1 dan untuk Cr(III)sebesar0,094 g mg-1 menit-1. Adsorpsi
campuran ion logam oleh abu dasar mengikuti isoterm adsorpsi Langmuir dengan
kapasitas adsorpsi sebesar 0,031mmol g
-1
untuk Pb(II)sedangkan0,080mmol g
-1
untuk Cu(II) dan untuk Cr(III) sebesar 0,125mmol g
-1
.
Kata kunci: adsorpsi, abu dasar batubara, ion logam berat.
viii
ABSTRACT
ADSORPTION OF Pb(II), Cu(II) and Cr(III) METAL IONS USING COAL
BOTTOM ASH
By
Anies Septiana
08/269689/PA/12033
Heavy metals removal from aqueous solution using coal bottom ash as an
adsorbent which is thewasteof coal combustionfrom thePS. Madukismo,
PT.MadubaruYogyakarta has been studied. The aim of this research was to
examine the ability of bottom ash in adsorbing mixture of metal ions.  The
research consisted of characterization andidentification ofthe
componentscontained incoalcombustionwaste using FTIR and XRD. The process
of adsorption Pb(II), Cu(II), and Cr(III) metal ionswas done byadding
theadsorbent into the solutionmixture. The unabsorbed heavy metal was
determined by Atomic AbsorptionSpectrophotometer. The influences of mass,
also pH solution, time of adsorption and initial concentration of heavy metal ions
have been studied.
The results of this research showedthatcoalcombustionwastesin the form
ofbottom ashis composed ofSiO2andAl2O3 as the major components. The highest
adsorption of 15 mL solution containing Pb(II), Cu(II), and Cr(III) metal ions with
50 mg/L of each concentration can be obtained using0.2gadsorbentfor 60
minutes.The adsorption of Pb(II), Cu(II), and Cr(III) metal ions was fitted by
Pseudo second order model, with the adsorption rate constant was0.031 mgg-1min-
forPb(II) metal ion, 0.418mgg-1min-1forCu(II) metal ion, and 0.094 mgg-1min-1 for
Cr(III) metal ions. From the isotherm study, Langmuir isotherm model was found
to be the best fit for thissystem, withthe adsorptioncapacityof0.031;
0.080;0.125mmol g
-1
for Pb(II), Cu(II), and Cr(III) metal ions, respectively.
Keywords: adsorption, coal bottom ash, heavy metal ions.

