by
Annisa� Hanif
Quantile regression has received increasing attention both from a
theoretical and from an empirical view point. It is a statistical procedure that
minimizing sums of asymmetrically weighted absolute and can be used to explore
the relationship between quantile of response distribution. Quantile regression
can be used  to overcome the limitation of linear regression to analyze data not
symmetric and quantile regression is useful if the distribution of datais not
homogeneous.
Quantile regression can be estimated using Bayesian method. Bayesian
method is a method of analysis based on information from sample and prior
information. Combination of those informations is called posterior.  For looking
posterior dsitribution often result in calculation can not be solved with analytical
so Gibbs sampling approach is used. Estimation of parameters in the model is the
mean of the posterior distribution that obtained from Gibbs sampling process.  In
this paper discused quantile regression using an asymmetric Laplace distribution
from Bayesian point of view.  Gibbs sampling is used to find the estimator of
quantile regression model based on a location-scale mixture representation
asymmetric Laplace distribution.
The case study in this paper discusses the  factors that effect the gold
price. The estimation result of quantile regression using Bayesian method will be
compared with linear regression using OLS method, and compared with quantile
regression method. And then it was concluded that the Bayesian method is better
than the otherconclusion.
Keywords: Quantile Regression, Bayesian, Gibbs sampling,  Asymmetric Laplace

