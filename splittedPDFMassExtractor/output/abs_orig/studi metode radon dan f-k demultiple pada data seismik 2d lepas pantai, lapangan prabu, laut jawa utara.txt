SEISMIK 2D LEPAS PANTAI LAPANGAN PRABU, LAUT JAWA UTARA
Penelitian ini bertujuan memisahkan multiple dari event primer pada data
seismik 2D lapangan Prabu Laut Jawa Utara dengan menggunakan metode Radon
dan F-K sekaligus menganalisis kemampuan kedua metode tersebut terhadap
pelemahan multiple sehingga menghasilkan penampilan data seismik yang baik.
Metode Radon bekerja dengan mengubah data dari domain t-x (time-offset)
menjadi domain ?-p (intercept time-ray parameter). Sedangkan Metode F-K
menggunakan prinsip transformasi Fourier dengan mengubah data dari domain t-x
(time-offset) menjadi domain f-k (frequency-wavenumber).
Kedua metode tersebut mampu melemahkan multiple, namun masih
meninggalkan sisa multiple. Metode Radon mampu melemahkan multiple lebih
baik dari pada metode F-K.
Kata Kunci: Radon, F-K, Pelemahan Multiple
xii
ABSTRACT
A STUDY OF RADON AND F-K DEMULTIPLE ON 2D OFFSHORE
SEISMIC DATA OF PRABU FIELD, NORTH JAVA SEA
Saktian Arief Noor Farikhin
09/283259/PA/12498
The research is to separate multiples from the primary event on 2D
offshore seismic data in the Prabu Field North Java Sea using Radon and FK
Demultiple, then to analyze the ability of these methods to multiple suppression so
as resulting seismic data looks good.
Radon method works by converting the data from the t-x domain (time-
offset) into the t-p domain (intercept time-ray parameter). While FK method uses
the principle of the Fourier transform converts the data from the t-x domain (time-
offset) into the f-k domain (frequency-wavenumber).
Both methods are able to suppress multiples, but still leave the residual of
multiples. Radon method can suppress multiples be better than FK method.
Keywords: Radon, F-K, Multiple Suppression

