Theoretical Study of Radiative Transfer Equation of Light Propagation in Skin
by
Radiative transfer equation of light propagation in skin tissue has been studied.
Homogeneous medium has been used as an approach to the real tissue. Radiative
transfer equation has been solved using two methods, namely diffusion equation and
Monte Carlo (MC). The solution of diffusion equation uses specific assumption,
boundary condition, and approach, while the solution of MC uses simulation process.
Results of both methods are presented in graphic fluence versus time. Both methods
showed similar results. The difference between these methods is on use of assumption,
approach, and time-consumption.
Keywords: radiative transfer equation, diffusion equation, Monte Carlo.

