ATMEGA32
Written by
The navigation system is a method which is used to determine the position
and direction of travel from the initial state. Currently, various studies appear to
create a navigation system that applied in human by using some multiple sensors
with high accuracy to obtain position, information and driving directions. In this
case, it can be done by detecting the number of steps and distance by using
Magnitude Threshold method . This  step detection and distance,  is an alternative
from GPS to measure the distance traveled by people while they are in a room.
A device that can detect and count the footsteps when people walk has
been made on this research. This device runs by using ATMega32 as a data
processing center, then ADXL345 accelerometer sensor and gyroscope sensor
L3G4200D were used to detect the change of position, as well as a digital
compass HMC5883L which was used to determine the direction of the Earth's
magnetic field movement. For every transformation of position is detected, the
data will be automatically updated then will be displayed on the Visual Basic 6.0
interface program.
From the experiments that have been done, the accelerometer sensor can
detect a footstep when the value of its magnitude is greater than 79 LSB with
measurement error value 2.02 steps. While the detection step gyroscope is used to
improve the detection of steps which was taken by the accelerometer.
Keywords: step detection, accelerometer, gyroscope, digital compass.

