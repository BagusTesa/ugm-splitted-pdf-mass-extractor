Web-Based Social Network Development Efforts as Productive Activities in
by
The hight population of women in Indonesia raises concerns in various
fields. Existing problems triggered women's empowerment. Empowerment of
women is manifested in a variety of activities. On the other hand, the number of
women who use social networking is quite high. Social networking can be used
for women's empowerment activities. The use of social networks allows many
women in different places cooperate in efforts to empower women. These
activities require a lot of resources. Meeting the needs od women�s empowerment
resource can use crowdsourcing methods, a method that uses a lot of human
resources to complete a job.
Development of web-based social networks for women�s empowerment in
productive activities is a women's social network in Indonesia with the main
function handles the coordination of joint activities. Social networking is built
using PHP programming language, the framework Laravel, MySQL database and
twitter bootstrap for the user interface.
It provides social networking services such as management of activities,
announcements and comments management, logging management, resource
management, product management, member management and notification
management of activities. This social networking allows women to work together
in a productive activity in an effort to empower women.
Keywords : women's empowerment, social networking, crowdsourcing, laravel

