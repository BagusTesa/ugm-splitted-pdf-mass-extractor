Telah dilakukan penelitian mengenai pembagian fasies lahar berdasarkan
analisis dari nilai amplitudo Real Time Seismic Amplitude Measurement (RSAM),
seismogram dan spektrum frekuensi dari sinyal seismik mikro yang terekam di
lereng Gunung Merapi, Jawa Tengah. Karena tingkat aktivitasnya yang tinggi ini,
Gunung Merapi banyak menghasilkan material rombakan. Terhitung 140 juta m3
material rombakan terdeposisi dan berpotensi untuk menjadi bencana lahar.
Penelitian ini dilakukan di 3 stasiun pengamatan Sungai Boyong, yakni SBY1,
SBY2 dan 1 lainnya di Sungai Gendol, GDN1. Pengumpulan data dilakukan
dengan seismometer 1 komponen dengan frekuensi diri 1 Hz dan samplingnya
100 Hz selama bulan Januari-Juli 2011. Data awal berupa data statistik RSAM
dan data seismogram yang memiliki format .seisan yang harus dikonversi ke .sac
sehingga bisa diolah dengan menggunakan software swarm atau Geopsy untuk
menganalisis pola seismogram dan frekuensi spektrumnya. Nilai distribusi
amplitudo dan frekuesni dari pengolahan data, dapat menunjukkan karakteristik
dari kejadian lahar.
Hasil pengolahan data menunjukkan bahwa dari stasiun pengamatan
amplitudo RSAM menunjukkan suatu kejadian lahar berada pada kisaran 1000-
10.500 ditemukan pada 6 tanggal kejadian. Amplitudo pada seismogramnya
antara 1.075-16.235 counts dengan durasi kejadian minimal 1 jam. Frekuensi
stream flow berada pada nilai rata-rata 2,51 Hz dengan amplitudo rata-ratanya 270
counts. Dari analisis spektrum terdapat 3 karakteristik aliran, yakni aliran debris
dengan frekuensi dan amplitudo 15-16 Hz dan 9.035-16.235 counts di GDN1, 11-
12 Hz dan 2.001-4.943 counts di SBY2 dan 7-10Hz dan 1.925-11.519 counts di
SBY1. Aliran hyperconcentrated pada rentang 15-20 Hz dan 2.797-3.616 counts
di GDN1, 15 Hz dan 6.690 counts di SBY1 dan 12-15Hz dan 1.075-1.864 counts
di SBY2. Amplitudo meningkat menunjukkan energi yang meningkat akibat
penambahan material, begitu pula dengan pengurangan nilai amplitudo.
Amplitudo menurun juga dikarenakan atenuasi karena deposisi endapan material
sebelumnya. Frekuensi meningkat dikarenakan terjadinya penambahan fluida
yang memberi ruang gerak pada partikel untuk bertumbukan satu sama lain.
Kata kunci : RSAM, Merapi, lahar
xv
CHARACTERIZATION OF LAHAR BY REAL TIME SEISMIC
AMPLITUDE MEASUREMENT (RSAM) ANALYSIS AND SPECTRUM
ANALYSIS OF MICROSEISMIC AT THE THREE MONITORING
STATIONS OF MERAPI VULCANO
by
Paula Ascaryani Effrianto
09/284062/PA/12792
Study of the distribution values of Real Time Seismic Amplitude
Measurement, seismogram time series and frequency spectrum from Micro tremor
signal of lahars has been conducted in January to July 2011 at the mountainside of
Merapi volcano. The activities of Merapi produce the high level of materialís
concentration. 140millions m3 of the eruptionsí materials have been deposited
around Merapi, and because of the high level of rainfalls, lahar hazard would be
triggered. This study has been done in the 3 monitoring stations of Sungai
Boyong, by names SBY1, SBY2 and near to Sungai Gendol by name GDN1. Data
was collected using 1 component seismometers with a sampling rate 100 ms and a
frequency of 1Hz. The first data is statistic values of RSAM and seismic data
which have .seisan format that used to be converted to .sac format and processed
by software Swarm and Geopsy for analyzing seismogramís pattern and the
frequency of its spectrum. These distribution values can show the characteristic of
lahars.
The results of data processing showed that the RSAM amplitudes have
values by interval 1.000-10.500 for 6 events of lahars and 1.075-16.235 counts for
their seismogramís amplitude by at least 1 hour of each durations. The average
frequency of the stream flows is 2.51 Hz with average amplitude 270 counts.
There are 3 characteristics of lahars related to the frequency spectrum. Debris flow
with frequency and amplitude : 15-16 Hz and 9.035-16.235 counts  at GDN1, 11-
12 Hz and 2.001-4.943 counts at SBY2, 7-10 Hz and 1.925-11.519 counts at
SBY1. Hyperconcentrated flow with frequency and amplitude : 15-20 Hz and
2.797-3.616 counts at GDN1, 15 Hz and 6.690 counts at SBY1, 12-15 Hz and
1.075-1.864 counts at SBY2. The increasing of amplitude value show the
increasing of materialís concentration.  The decreasing of amplitude value is
related to the atenuation caused by the last frozen deposition. The increasing of
frequency value happens by the increasing of fluid volumes. The large
concentration of fluids would give the space motion for particles and gain the
collisions.
Keynotes: RSAM, Merapi, lahar

