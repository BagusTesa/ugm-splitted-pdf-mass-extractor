Background: The procedure codes is one of the obligations of the medical
records officer. In determining the procedure coding, medical records officer shall
determine the rules of the ICD-9-CM. If the charging of procedure coding is not
accurate, it can lead to difficulties and errors in the indexing process and the
process of reporting and presentation of these statistics. Determination of the
procedure codes is determined by the ability of officers in charge of procedure
codes. In the coding, we need a audit of codes or evaluation. Audit of codes done
so that the results of the coding process and the resulting accurate, timely, in
accordance with the rules, regulations and policies applicable legislation. In Panti
Rapih Hospital Yogyakarta, procedure of codes has not been done audit of codes
or evaluation.
Objective: To determine the accuracy of the implementation and investigate the
percentage of procedure codes inpatient surgical cases, as well as knowing the
factors that affect the inaccuracies of the procedure codes inpatient surgical
cases.
Methodology Research: The research used a qualitative descriptive design was
cross-sectional, population in this study, the subjects were medical records officer
40 people, as many as 26 surgeons. Object of study includes all data from the
existing measures in the data sheet Diary Room Service Activity Surgery (Report
of Operations) of 4925 procedure of codes. Samples of subjects in this study
include 1 coding officer of JAMKESMAS, 1 coding officer of inpatient, Chief
Medical Record Installation Panti Rapih Yogyakarta and 1 surgeon. Object
sample data collection techniques with interviews, documentation and
observation studies.
Results: The implementation of the encoding of procedures in Panti Rapih
Hospital was performed using computerized Hospital Information System (SIRS)
Inpatient coding is done by officers at the processing of data. Encoding process
is done by looking at the procedures in the Daily Activity Data Room Services
Surgery (Report of Operations) of the operating room. Encoding facility coding is
used in the implementation of ICD-9 book-CM Volume 3, medical dictionary, a
standardized list of abbreviations, a list of international abbreviation. The
accuracy of the procedure codes on SIRS Hospitalization for 57.12%. Factors
influencing of inaccuracies of procedure codes is medical record officers, had
never done renewal program or update the database of Inpatient on the SIRS,
had never performed an audit or evaluation of coding.
Keywords: accuracy, coding, procedure codes, ICD-9-CM

