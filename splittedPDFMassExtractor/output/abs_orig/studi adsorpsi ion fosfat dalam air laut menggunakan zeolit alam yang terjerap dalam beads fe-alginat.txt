Pada penelitian ini dibuat adsorben berupa zeolit alam yang terjerap dalam
beads Fe-Alginat untuk mengadsorpsi ion fosfat dalam air laut. Proses pembuatan
beads dilakukan dengan metode ekstrusi yaitu dengan meneteskan campuran
zeolit-alginat (3:3) ke dalam larutan berisi kation Fe3+ 0,2 M dan sebagai
pembanding dibuat beads Fe-Alginat. Beads dikarakterisasi dengan spektroskopi
infra merah (FTIR). Pengaruh variasi massa adsorben, pH larutan, waktu adsorpsi,
dan konsentrasi adsorbat terhadap kapasitas adsorpsi ion fosfat dipelajari dalam
penelitian ini.
Hasil penelitian menunjukkan bahwa zeolit yang terjerap dalam beads Fe-
Alginat dapat terbentuk dan digunakan sebagai adsorben ion fosfat dengan
kemampuan adsorpsi yang baik. Adsorpsi optimum [PO4
3-] dengan beads Zeolit-
Fe-Alginat dan beads Fe-Alginat terjadi pada pH 7 dan 6, intensitas adsorpsi
mengikuti model isoterm Freundlich untuk beads Zeolit-Fe-Alginat sebesar 1,150
dengan konstanta Freundlich (Kf) sebesar 0,043 L/g dan untuk beads Fe-Alginat
sebesar 1,069 dengan Kf sebesar 0,070 L/g.  Energi adsorpsi ditentukan dari
model isoterm Dubinin-Radushkevich sebesar 0,298 kJ/mol untuk beads Zeolit-
Fe-Alginat dan 0,408 kJ/mol untuk beads Fe-Alginat. Proses adsorpsi yang terjadi
adalah fisisorpsi. Adsorpsi mengikuti model kinetika pseudo order dua dengan
konstanta laju adsorpsi (k2�) untuk beads Zeolit-Fe-Alginat sebesar 0,029 g.mg
-
1min-1 dan 0,040 g.mg-1min-1 untuk beads Fe-Alginat. Proses adsorpsi ini
dikendalikan oleh transfer massa (mass transfer control) dari larutan ke
permukaan adsorben.
Kata kunci : Zeolit, Alginat, Adsorpsi, Ion Fosfat
xii
ABSTRACT
ADSORPTION STUDY OF PHOSPHATE FROM SEAWATER USING
NATURAL ZEOLITE ENTRAPPED IN Fe-ALGINATE BEADS
by
Fea Punini Mayangsari
09/283884/PA/12735
Synthesis of beads of natural zeolite entrapped in Fe-Alginate, and its
adsorption characteristic for PO4
3- has been conducted. Beads of Zeolite-Fe-
Alginate (Z-Fe-A) was made by extrusion method in multivalent cation solution
containing Fe3+ of 0.2 M. As comparison, beads Fe-Alginate was also synthesized.
The beads were characterized by FTIR method. Various parameters such as
equilibrium contact time, pH, initial phosphate concentration, and adsorbent dose,
were studied in adsorption experiment.
It was found that the beads of alginate gel and natural zeolite (Z-Fe-A) was
successfully made and was capable of removing phosphate from seawater. The
optimum adsorption was investigated at pH 7 for beads of Z-Fe-A and pH 6 for
beads of Fe-A. Adsorption intensity was analyzed by Freundlich model, and it
showed that Z-Fe-A beads has an intensity of 1.150 with Freundlich constant (Kf)
of 0.043 L/g and Fe-A beads has an intensity of  1.069 with Kf of 0.070 L/g.
Beads of Zeolite-Fe-Alginate and Fe-Alginate has an energy of adsorption of
0.298 kJ/mol and 0.408 kJ/mol, repectively, which belongs to physisorption. The
sorption kinetics of phosphate follows pseudo-second order model, having rate
constant of 0.029 g.mg-1min-1 for Z-Fe-A beads and 0.040 g.mg-1min-1 for Fe-A
beads. Adsorption process was controlled by mass transfer from bulk to the
surface of adsorbent.
Keyword : Zeolite, Alginate, Adsorption, Phosphate

