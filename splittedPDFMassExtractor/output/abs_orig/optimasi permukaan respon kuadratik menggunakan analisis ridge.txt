Optimization of Quadratic Response Surface Using Ridge Analysis
by
Response surfaces method is a method that aims to determine the optimal
operating conditions in an experiment. The response characteristics of operating
conditions can be a maximum, minimum, or saddle point. In some situations where a
stationary point has no value, for example, a stationary point is at the saddle point
and stationary point which causes minimum or maximum response point outside the
original region, needs to be done a specific analysis to handle that. This analysis
called ridge analysis.
Ridge analysis is a useful procedure to optimize response prediction in
hyperspherical centralized quadratic model. This procedure calculates the estimated
ridge on the optimum response for increasing radii from the center of the actual
experiments.
The case study used in this thesis is the optimization of the power torque TL
lights against low air, low air cooling, and natural gas pressure. Experimental design
using central composite design (CCD) with the number of experiments at 23 runs.
The analysis showed that the optimal condition is obtained when the pressure of low
air 3,80125 Kpa, low air cooling 7,3515 Kpa, and natural gas 4,97 Kpa, that is when
the strength of the torque reaches 3,11125 Newton meter.
Keywords: response surface methods, ridge analysis on response surfaces, central
composite design (CCD)

