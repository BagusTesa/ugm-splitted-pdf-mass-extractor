Steam generator is a boiler that is used to convert water into steam with
varying quality at a certain discharge pressure. Input parameters of steam
genarator need to be controlled to obtain the output parameters according to the
state that are required when it is used, so steam generator can work optimally.
Steam generator has problem such as dependent variable on the output
parameters, therefore it is needed a way to get system behavior of steam
generator which is represented by model of the plant.
Model predictive control is controller that uses the model of the plant as a
compiler unit of control. In this research, the method that was used to get the
model was using ANFIS (Adaptive Neuro Fuzzy Inference System). In designing
the model predictive control, it was performed to make structures of two different
models, each of structures would serve as a control unit and prediction unit.
Implementation of the model predictive control in this research was represented
by using simulation, so the object that was controlled was virtual steam
generator. In this research was used MATLAB software as a virtual steam
generator and also for computes ANFIS, whereas the LabVIEW software was
used as a representation of control room.
From the research, it was found the best parameters for each ANFIS that
was used as a model unit in the model predictive control, that was by using
historical data 4
th
as much as 800 datas, the percentage ratio of learning for
training data and checking data on each ANFIS for each model structures
sequentially by 90% and 10%, except the percentage ratio for ANFIS on water
flow parameter sequentially by 80% and 20%. The results of validation RMSE
(Root Mean Square Error) by testing for 100 datas, it was obtained values as
follows: water flow=1.9941, water pressure=48.0236, air flow=604.0621, fuel
gas pressure=0.7087, fuel gas temperature=18.6594, O2 content=0.9591, steam
pressure=76.1557, steam quality=3.9734 and steam flow=264.9173.
Keywords: Steam generator, model predictive control, modeling, ANFIS,
MATLAB, LabVIEW

