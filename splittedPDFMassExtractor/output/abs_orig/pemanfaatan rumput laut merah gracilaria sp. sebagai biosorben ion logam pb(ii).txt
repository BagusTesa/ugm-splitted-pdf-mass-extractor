Pemanfaatan Rumput Laut Merah Gracilaria sp. sebagai Biosorben ion
Logam Pb(II)
oleh
Penelitian pemanfaatan rumput laut Gracilaria sp. sebagai biosorben
ion logam Pb(II) telah dilaksanakan. Gracilaria sp. dapat digunakan sebagai
biosorben alternatif yang murah, mudah dan melimpah, serta memiliki
kemampuan yang mirip dengan biosorben lainnya. Kemampuan adsorpsi
tersebut disebabkan adanya kandungan dua macam gugus fungsi yang dapat
menyerap ion logam Pb(II).
Keberadaan gugus fungsi telah ditunjukkan dengan karakterisasi
analisis FTIR (Fourier Transform Infrared). Parameter-parameter serapan
optimum seperti, pH, waktu kontak, massa biosorben, dan konsentrasi ion
logam Pb(II) telah ditentukan dengan menggunakan analisis AAS (Atomic
Absorption Spektrometer). Dari penelitian ini diperoleh kemampuan adsorpsi
dan telah ditentukan model kinetika adsorpsi serta model isotermal adsorpsinya.
Keywords: Gracilaria sp., adsorpsi, biosorben
xi
ABSTRACT
Utilization of Gracilaria sp Red Seaweed as Biosorbent of Pb(II) Metal Ion
by
ADISTIA
08/270154/PA/12229
Research about utilization of Gracilaria sp. as biosorbent of Pb(II) ion
has been done. Gracilaria sp. can be used as alternative biosorbent which is
cheap, easy, and large abundance. It has similiar ability with other adsorbent. It
has two kinds of functional group which is able to adsorp Pb(II) ion.
The existence of functional groups is characterized using FTIR
(Fourier Transform Infrared) analysis. Optimum adsorption parameters, such
as pH, contact time, biosorbent mass, and concentration of Pb(II) ion, have
been determined by AAS (Atomic Absorption Spektrometer) analysis. The
results from this research are the adsorption capability of Gracilaria sp., and
the determination of reaction kinetics model and isoterm adsorption model.
Keywords: Gracilaria sp., adsorption, biosorbent

