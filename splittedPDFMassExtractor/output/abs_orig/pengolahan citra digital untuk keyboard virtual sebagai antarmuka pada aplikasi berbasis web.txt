Since the first computer was founded, keyboard  is always  been a primary
tool for interaction between humans and computers. Today, many computers use
the  image  processing  technology  to  make  interaction between  computers and
humans.
The author try to apply image processing technology  that implemented to
virtual keyboard on web application. Using a webcam to capture the tip of index
finger and the results will be sent to the localhost server for processing with image
processing.  Using  Haar  Cascade  Classifier  method  to  detect  the  tip  of  index
finger, it will produce coordinates that sent to the web application and it used as a
reference for determining button positions on virtual keyboard.  Virtual keyboard
characters will display after appointed by the tip of  index finger.
From testing results, optimal distance from index finger to webcam is 20 �
35 cm.  System can recognize the tip of index finger on white background and
room  with  few  furnitures.  The  average  response  time  for  displaying  virtual
keyboard characters is 5.156 seconds. So the virtual keyboard on this system was
not able to be used as interface on web application, because it  difficult to use in
directing the tip of index finger to the character keys.
Keywords: web application,  object  detection,  Haar  Cascade  Classifier,  virtual
keyboard, digital image processing, localhost server.

