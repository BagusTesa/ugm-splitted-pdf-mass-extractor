Sintesis dan Karakterisasi Fe-TiO2, N-TiO2, dan Fe-N-TiO2
Sintesis dan karakterisasi Fe-TiO2, N-TiO2, dan Fe-N-TiO2 telah selesai
dilakukan. Penelitian ini bertujuan untuk mengetahui pengaruh konsentrasi dopan
terhadap energi celah pita TiO2 dengan variasi 2,5; 5,0; dan 7,5% b/b serta
mempelajari pengaruh dopan tunggal dan dopan ganda terhadap energi celah pita
TiO2. Semua material tersebut disintesis dengan menggunakan metode impregnasi
dengan prekursor TiO2, garam FeCl3.6H2O sebagai sumber ion Fe3+, dan urea
sebagai sumber nitrogen. Semua sampel dikarakterisasi dengan menggunakan
spektroskopi FT-IR, spektroskopi UV-Vis specular, dan difraksi sinar-X.
Hasil penelitian menunjukkan bahwa energi celah pita optimum Fe-TiO2
dan N-TiO2 berdasarkan variasi konsentrasi berturut-turut adalah 3,10 dan
3,07 eV, sedangkan TiO2 murni memiliki energi celah pita sebesar 3,19 eV.
Dibandingkan dengan nilai energi celah pita TiO2 terdoping tunggal dan TiO2
murni, TiO2 terdoping Fe3+ dan nitrogen mempunyai nilai energi celah pita
terkecil dengan nilai optimum sebesar 2,96 eV. Kehadiran dopan di TiO2
memberikan level energi baru di antara pita valensi dan pita konduksi TiO2
sehingga energi celah pita material hasil sintesis mengalami penurunan.
Fe-N-TiO2 memiliki energi celah pita terkecil disebabkan adanya efek sinergis
dari level energi baru di atas pita valensi dan di bawah pita konduksi TiO2 yang
terinduksi adanya ion Fe3+ dan nitrogen. Kehadiran dopan tidak hanya
mempengaruhi struktur elektronik tetapi juga struktur Kristal TiO2. Semua
material hasil sintesis mempunyai kristalinitas lebih rendah dibandingkan dengan
TiO2 murni, dimana Fe-N-TiO2 memiliki kristalinitas paling rendah karena
kehadiran dopan ganda di TiO2
Kata Kunci: Fe-TiO2, N-TiO2, Fe-N-TiO2, energi celah pita
xii
ABSTRACT
Synthesis and Characterization of Fe-TiO2, N-TiO2, and Fe-N-TiO2
By
Arief Rahman Hakim
09/289248/PA/12934
Synthesis and characterization of Fe-TiO2, N-TiO2, and Fe-N-TiO2 have
been carried out. This research was done to determine the effect of dopant
concentration of 2.5, 5.0, and 7.5% w/w on the band gap energy of TiO2 and the
effect of single and double dopants to the band gap energy of TiO2. All materials
have been synthesized by using impregnation method with TiO2 as precursor,
FeCl3.6H2O salt as Fe3+ ions source, and urea as nitrogen source. All samples
were characterized by using FT-IR spectroscopy, UV-Vis specular reflectance
spectroscopy, and X-ray diffraction.
The results showed that the optimum band gap energy of Fe-TiO2 and
N-TiO2 yielded from the dopant variation were 3.10 and 3.07 eV. Compared to
band gap energy of single doped-TiO2 and pure TiO2, Fe-N-codoped TiO2 had the
smallest band gap energy with the optimum value of 2.96 eV. The presence of
dopant in TiO2 introduced new level energy between valence and conduction
bands of TiO2 so the band gap energy decreased. Fe-N-TiO2 has the smallest band
gap energy due to the synergistic effect of new levels energy above valence band
and below conduction band of TiO2  induced by both of Fe3+ ions and nitrogen.
The presence of dopant  into TiO2 not only affect the electronic structure but also
crystalline structure of TiO2. All synthesized materials have lower crystallinity
compared to pure TiO2, where Fe-N-TiO2 has the lowest crystallinity due to the
presence of double dopants in TiO2.
Keywords: Fe-TiO2, N-TiO2, Fe-N-TiO2, band gap energy

