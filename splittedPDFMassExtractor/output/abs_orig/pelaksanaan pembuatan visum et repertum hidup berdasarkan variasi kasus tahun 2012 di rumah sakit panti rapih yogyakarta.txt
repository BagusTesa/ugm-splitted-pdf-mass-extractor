Latar   belakang: Jenis   surat keterangan   medis untuk pengadilan adalah visum et
repertum.  Visum  et  repertum  adalah  keterangan  yang  dibuat  oleh  dokter  forensik  atas
permintaan   tertulis   dari   penyidik   berdasarkan   sumpah   tentang   apa   yang   dilihat   dan
ditemukan  pada  benda  yang  diperiksa  berdasarkan  pengetahuan  yang  sebaik  baiknya
untuk kepentingan pengadilan Pembuatan visum et repertum yang lama dapat
mengakibatkan   tertundanya   persidangan,   sehingga   dibutuhkan   pembuatan visum   et
repertum  yang  tepat  waktu  untuk  kepentingan  persidangan.  Penyimpanan  arsip  visum  et
repertum   yang   baik   dapat   memudahkan   dalam   pencarian   kembali   jika   sewaktu   waktu
diperlukan.
Tujuan:   Mengetahui   pelaksanaan   pembuatan   Visum   et   Repertum   hidup   berdasarkan
variasi kasus tahun 2012   di Rumah Sakit Panti Rapih Yogyakarta.
Metode  : Jenis  penelitian  yang  digunakan  adalah  deskriptif  kualitatif  dengan  rancangan
cross  sectional.  Subyek  penelitian  terdiri  dari dokter  yang  berwenang  dalam  pembuatan
visum et repertum, petugas rekam medis, dan petugas khusu pembuat visum et
repertum..  Penyajian  data  disajikan  dalam  bentuk  uraian  berupa  teks-teks  ,  sedangkan
data hasil wawancara disajikan dalam bentuk kutipan .
Hasil   Penelitian   :   Di   Rumah   Sakit   Panti   Rapih   Yogyakarta   pelaksanaan   pembuatan
visum  et  repertum  belum  sepenuhnya  sesuai  dengan  standar  operasional  prosedur  yang
ada.  Banyak  visum  et  repertum  yang  memerlukan  perawatan  rawat  inap  dan  yang  dibuat
oleh   dokter   umum   atau   IGD   dibuat   lebih   dari   7   hari.   Variasi   kasus   yang   banyak
menyebabkan  keterlambatan  adalah  kasus  kecelakaan  lalu  lintas  dan  penganiayaan.  Di
Rumah  Sakit  Panti  Rapih  Yogyakarta  tidak  melaksanakan  pembuatan  visum  et  repertum
sementara.   Kendala   kendala   yang   dialami   dalam   pelaksanaan   pengeluaran   visum   et
repertum   dibedakan menjadi   dua   macam   yaitu   sumber daya   manusia   dan sarana
prasarana.  Sumber  daya  manusia  meliputi  ketidakterbacaan  tulisan  dokter,  jadwal  dokter
yang  tidak  setiap  hari  ada,  dan  juga  ketidakterisian  berkas  rekam  medis.  Untuk  sarana
dan   prasaran   berupa   standar   operasinal   prosedur   yang   belum   tersosialisasi   secara
optimal sehingga banyak dokter  yang belum mengetahui   lama standar pembuatan  visum
et repertum.
Obyek Penelitian : pelaksanaan pengeluaran Visum et Repertum hidup berdasarkan
variasi kasus tahun 2012   di Rumah Sakit Panti Rapih Yogyakarta
Kata Kunci:   Pelaksanaan, Pengeluaran , Visum Et Repertum, Kasus, Variasi.

