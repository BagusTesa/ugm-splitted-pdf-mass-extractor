1. Pelaksaan pengeluaran visum et repertum di Rumah Sakit Panti
Rapih Yogyakarta adalah sebagai berikut :
a. Prosedur   permintaan   visum   et   repertum   di   Rumah   Sakit
Panti Rapih Yogyakarta telah memenuhi prosedur
permohonan  visum  et  repertum  menurut  Waluyadi  (2005),
dimana  secara  garis  besar  permohonan  visum  et  repertum
harus memperhatikan bahwa permohonan harus
dilaksanakan  secara  tertulis,  dan  dilakukan  oleh pihak-pihak
yang diperkenankan untuk itu.
b. Pihak yang berhak meminta visum et repertum di Rumah Sakit
Panti Rapih Yogyakarta sudah sesuai dengan Waluyadi (2005)
yaitu penyidik.
c. Berdasarkan  ketentuan  pemaparan  informasi  rekam  medis
menurut Depkes RI (1997), permohonan pasien untuk
memperoleh  informasi  mengenai  dirinya  diserahkan  kepada
dokter yang merawat. Berdasarkan Permenkes RI
No.269/Menkes/Per/III/2008 pasal 12 ayat 1 dijelaskan
bahwa  pemaparan  isi  rekam  medis  hanya  boleh  dilakukan
oleh dokter yang merawat pasien dengan ijin tertulis pasien
d. Bentuk dan isi visum et repertum yang dikeluarkan oleh
Rumah   Sakit   Panti   Rapih   Yogyakarta   sudah   sesuai   dengan
Idris (1997) dimana secara garis besar pada visum et
repertum   harus   tercantum   �pro   justicia�,   data   sosial   pasien,
emuat  identitas  dokter  pemeriksa,  memuat  hasil  pemeriksaan,
kesimpulan, dan penutup  yang menyatakan  visum et  repertum
tersebut dibuat atas dasar sumpah dokter
2. Di   rumah   sakit   Panti   rapih   Yogyakarta   belum   melaksanakan
pembuatan visum et repertum sementara. Hal ini belum
sepenuhnya sesuai dengan pembagian visum et repertum hidup
menurut Soeparmono(2002).
Lama pembuatan visum et repertum di Panti Rapih
yogyakarta belum sepenuhnya sesuai dengan standar
operasional   prosedurnya,   sebagian   besar   visum   et   repertum
dibuat  lebih  dari  standar  yang  telah  ditetapkan.  Keterlambatan
banyak ditemui di permintaan visum et repertum yang
memerlukan  rawat  inap  dan  dikerjakan  oleh  dokter  umum/IGD
sedangkan  untuk  variasi  kasus  yang  paling  banyak  terlambat
adalah kasus penganiayaan dan kecelakaan lalu lintas.
Penyimpanan arsip copy visum et repertum belum
semuanya  disimpan  dalam  stockholder  ,  saat  penelitian  masih
banyak   dijumpai   konsep visum   et repertum yang   disimpan
bersama berkas rekam medis pasien.
3. Kendala � kendala yang dihadapi dalam pelaksanaan
pengeluaran   visum   et   repertum   hidup   di   Rumah   Sakit   Panti
Rapih Yogyakarta dibedakan menjadi dua macam yaitu Sumber
Daya   Manusia  seperti  ketidakterbacaan   tulisan   dokter,   jadwal
dokter  yang  tidak  setiap  hari  praktek  dan  juga  kelengkapan  isi
berkas  rekam  medis.  Selain  itu  kendala  dalam  hal  sarana  dan
prasarana  yaitu  belum  tersosialisasikannya  standar  operasional
prosedur kepada dokter pembuat visum et repertum.

1. Sebaiknya dilakukan revisi terhadap standar prosedur
operasional  pembuatan  visum  et  repertum  yang  ada,  terutama
dilakukan penambahan item  pada standar  operasional prosedur
tersebut terkait   tentang   pelaksanaan kegiatan   penyimpanan
arsip   copy   visum et repertum dalam stockholder   dan
pengecekan kesesuaian isi berkas rekam medis dan isi visum et
repertum yang dilakukan oleh Kepala Instalasi Rekam Medis.
2. Sebaiknya   dilakukan   pengkajian   ulang   terhadap   standar
prosedur operasional pembuatan visum et repertum terkait
dengan lama waktu pembuatan visum et repertum.
3. Sebaiknya   dilakukan   sosialisasi   standar   operasional   prosedur
mengenai  lama  waktu  pembuatan  visum  et  repertum kepada
semua  pihak  yang  bertanggung  jawab  dalam  pembuatan  visum
et  repertum,  agar  masing  masing  pihak  mengetahui  dan  dapat
bekerja sesuai dengan standar operasional yang ada.
4. Sebaiknya  semua visum  et  repertum  yang  telah  selesai  dibuat
di  copy  untuk  kemudian  arsip  copy  nya  disimpan  menjadi  satu
dalam   stockholder agar   memudahkan pencarian apabila
diperlukan kembali.

