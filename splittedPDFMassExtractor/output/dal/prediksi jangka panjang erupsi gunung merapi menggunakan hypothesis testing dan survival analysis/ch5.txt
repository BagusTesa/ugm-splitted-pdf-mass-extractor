Dari hasil identifikasi rezim menggunakan hypothesis testing metode
Kolmogorov-Smirnov two-sample test (KS2test), identifikasi pola distribusi tiap
rezim menggunakan Akaike Information Criterion (AIC), dan prediksi
kemungkinan terjadinya erupsi menggunakan survival analysis terhadap data
waktu jeda antar erupsi Gunung Merapi tahun 1768 hingga 2010, dapat
disimpulkan bahwa:
1. Perubahan rezim Gunung Merapi terjadi pada tahun 1878, sehingga rezim
lama Gunung Merapi terjadi sebelum tahun 1878, sedangkan rezim baru
dimulai dari tahun 1878 hingga 2010.
2. Pola distribusi waktu jeda antar erupsi rezim lama Gunung Merapi adalah
gamma dengan nilai AIC 95,75, sedangkan polanya pada rezim baru
adalah Weibull dengan nilai AIC 160,29.
3. Aktivitas erupsi pada rezim lama Gunung Merapi cenderung memiliki
indeks letusan dan waktu jeda antar erupsi yang lebih fluktuatif
dibandingkan aktivitas erupsi pada rezim baru yang didominasi oleh VEI 2
dan waktu jeda antar erupsi 1 � 6 tahun.
4. Perubahan karakter aktivitas rezim lama dan rezim baru Gunung Merapi
diduga diakibatkan oleh perubahan mekanisme erupsi Gunung Merapi dari
yang sebelumnya didominasi oleh fountain-collapse eruption, menjadi
dome-collapse eruption.
5. Erupsi pertama Gunung Merapi setelah erupsi tahun 2010 diduga terjadi
pada rentang waktu antara tahun 2014 hingga 2016.
6. Karakter erupsi Gunung Merapi setelah erupsi tahun 2010 diduga masih
akan didominasi oleh dome-collapse eruption dengan waktu jeda antar
erupsi terpanjang adalah 6 tahun.

Berdasarkan hasil penelitian ini, maka saran yang dapat diberikan untuk
penelitian serupa pada waktu yang akan datang adalah:
1. Dilakukan pengujian terhadap waktu jeda antar erupsi erupsi-erupsi
terdahulu untuk mengetahui keakuratan hasil penelitian.
2. Dilakukan studi lebih lanjut menggunakan data yang serupa tapi
kriterianya lebih dipersempit, misalnya khusus untuk erupsi dengan VEI
2, 3, atau 4 saja, sehingga dapat diketahui pola yang lebih spesifik untuk
masing-masing tipe erupsi.
3. Dilakukan studi lebih lanjut menggunakan data dengan rentang waktu
yang lebih sempit, tetapi lebih rinci, untuk mengidentifikas perubahan-
perubahan kecil dalam aktivitas Gunung Merapi beserta faktor-faktor
perubahannya.
4. Dilakukan studi lebih lanjut menggunakan parameter selain waktu jeda
antar erupsi, seperti volume erupsi, laju erupsi, perubahan kandungan
SiO2, dan lain lain, agar dapat diketahui hubungan antara rezim-rezim
waktu jeda antar erupsi Gunung Merapi dengan rezim-rezim lain dari
parameter yang berbeda.
5. Dilakukan pendekatan menggunakan metode selain Kolmogorov-Smirnov
two-sample test (KS2test), Akaike Information Criterion (AIC), dan
survival analysis untuk identifikasi rezim, penentuan pola distribusi tiap
rezim, dan prediksi kemungkinan terjadi erupsi.

