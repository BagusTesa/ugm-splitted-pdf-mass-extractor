Penentuan Harga Barrier Call Option dengan Estimasi Volatilitas Metode
Penentuan harga opsi secara teoritis dapat membantu seorang investor guna
mendapatkan gambaran mengenai harga opsi di pasar pada saat jatuh tempo. Dengan
begitu, seorang investor dapat mengambil keputusan yang tepat apakah membeli atau
menjual opsi yang dia miliki. Selain itu, jenis opsi juga berpengaruh untuk
mendapatkan keuntungan yang besar. Salah satu jenis opsi yang lebih murah dari
harga standar opsi biasanya adalah opsi jenis barrier. Opsi barrier tersebut,
mempunyai nilai batasan yang mempengaruhi harga opsi itu sendiri.
Penetapan harga opsi secara teoritis juga mempunyai banyak cara, yaitu salah
satu diantaranya adalah model Black-Scholes. Dimana model Black-scholes tersebut
mempunyai beberapa asumsi, yaitu opsi yang digunakan adalah opsi tipe Eropa,
tidak ada biaya transaksi atau pajak, tidak ada pembayaran deviden selama periode
opsi, tidak ada kesempatan bagi arbitrasi, dan perdagangan aset berlangsung secara
kontinu. Untuk menghitung nilai opsi dengan model Black-Scholes tipe barrier
diperlukan enam parameter yaitu harga asset awal (S0), harga patokan opsi (K),
waktu jatuh tempo opsi (T), suku bunga bebas risiko ( r ), nilai barrier (B), dan
volatilitas harga aset dasar (?).
Dari keenam parameter yang digunakan untuk penghitungan nilai opsi secara
teoritis tersebut hanya nilai dari volatilitas (?) yang tidak dapat diketahui secara
langsung. Oleh karena itu, pada skripsi ini akan dibahas bagaimana cara
mengestimasikan nilai volatilitas (?) dari harga saham tersebut untuk menghitung
nilai harga opsi yang hamper mendekati harga di pasar. Di skripsi ini menggunakan
metode Exponential Weighted Moving Average (EWMA) untuk pengestimasian
volatilitasnya.
Kata kunci : Barrier, Volatilitas, Moving Average, Forecasting Volatility

