Analisis Regresi merupakan salah satu alat statistika yang banyak
digunakan untuk mengetahui hubungan antara sepasang variable atau lebih.
Analisis regresi dibagi dua yaitu regresi parametrik dan regresi
nonparametrik. Regresi nonparametric memiliki beberapa metode
smoothing, seperti regresi spline dan kernel. Tujuan utamanya adalah
membandingkan kedua metode tersebut untuk mengestimasi model regresi
nonparametrik. Data yang digunakan untuk membandingkan kedua metode
tersebut yaitu data pertuumbuhanbalita.
Kata kunci :Regresi nonparametrik, regresi spline, regresi kernel.

