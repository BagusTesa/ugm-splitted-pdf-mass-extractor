x i
K e g i a t a n  p e m b a n g u n a n  s e t i a p  d a e r a h  t i d a k  d a p a t  t e r l e p a s  d a r i
k e t i m p a n g a n  p e n d a p a t a n .  K e t i m p a n g a n  p e n d a p a t a n  d a p a t  m e n i m b u l k a n  d a m p a k
p o s i t i f  m a u p u n  n e g a t i f .  D a m p a k  p o s i t i f  d a r i  k e t i m p a n g a n  p e n d a p a t a n  a d a l a h
k e t i m p a n g a n  p e n d a p a t a n  a k a n  m e m b e r i k a n  d o r o n g a n  k e p a d a  d a e r a h  y a n g
t e r b e l a k a n g  u n t u k  d a p a t  b e r u s a h a  m e n i n g k a t k a n  k u a l i t a s  h i d u p n y a .  D a m p a k
n e g a t i f  y a n g  d i t i m b u l k a n  d e n g a n  s e m a k i n  t i n g g i n y a  k e t i m p a n g a n  p e n d a p a r a n
a n t a r - w i l a y a h  a d a l a h  b e r u p a  i n e f i s i e n s i  e k o n o m i ,  m e l e m a h k a n  s t a b i l i t a s  s o s i a l ,
d a n  s o l i d a r i t a s .  K e t i m p a n g a n  p e n d a p a t a n  p e r l u  d i m i n i m a l k a n  k a r e n a  d a p a t
m e n i m b u l k a n  k e t i d a k a d i l a n  d a n  k o n f l i k  s o s i a l .  G u n a  m e m i n i m a l i s i r  k e t i m p a n g a n
p e n d a p a t a n  m a k a  t e r l e b i h  d a h u l u  h a r u s  d i k e t a h u i  f a k t o r  y a n g  d a p a t  m e n y e b a b k a n
k e t i m p a n g a n  p e n d a p a t a n .  T u j u a n  d a r i  p e n e l i t i a n  i n i  a d a l a h  u n t u k  m e m b u k t i k a n
a p a k a h  d e n g a n  m e n i n g k a t n y a  p e r t u m b u h a n  e k o n o m i ,  k o n t r i b u s i  p e r t a n i a n  d a n
I n d e k s  P e m b a n g u n a n  M a n u s i a  d a p a t  m e n g u r a n g i  t i n g k a t  k e t i m p a n g a n  p e n d a p a t a n
d i  K a l i m a n t a n  T i m u r  p a d a  t a h u n  2 0 0 9 - 2 0 1 3 .
J e n i s  p e n e l i t i a n  y a n g  a k a n  d i g u n a k a n  d a l a m  p e n y u s u n a n  t e s i s  i n i  a d a l a h
p e n e l i t i a n  e k s p l a n a t o r i  ( e x p l a n a t o r y  r e s e a r c h ) .  J e n i s  d a t a  d a l a m  p e n e l i t i a n  i n i
a d a l a h  d a t a  s k u n d e r  y a n g  p e n y u s u n  p e r o l e h  d a r i  p u b l i k a s i  B P S  k a b u p a t e n / k o t a  d i
K a l i m a n t a n  T i m u r  p a d a  t a h u n  2 0 0 9 - 2 0 1 3 .  P e n e l i t i a n  i n i  d i l a k u k a n  p a d a  1 4
k a b u p a t e n / k o t a  d i  K a l i m a n t a n  T i m u r .  A d a p u n  t e k n i k  a n a l i s i s  d a t a  d a l a m
p e n e l i t i a n  i n i  a d a l a h  d e n g a n  m e n g g u n a k a n  m e t o d e  r e g r e s i  d a t a  p a n e l .
H a s i l  p e n e l i t i a n  m e m b u k t i k a n  b a h w a  p e r t u m b u h a n  e k o n o m i  d i  P r o v i n s i
K a l i m a n t a n  T i m u r  t e r b u k t i  t i d a k  m e m b e r i k a n  p e n g a r u h  s i g n i f i k a n  t e r h a d a p
k e t i m p a n g a n  p e n d a p a t a n .  K o n t r i b u s i  p e r t a n i a n  b e r p e n g a r u h  p o s i t i f  t e r h a d a p
k e t i m p a n g a n  p e n d a p a t a n  d a n  I P M  m e m i l i k i  p e n g a r u h  p o s i t i f  t e r h a d a p
k e t i m p a n g a n  p e n d a p a t a n .  K o n t r i b u s i  p e n e l i t i a n  i n i  a d a l a h  p e r t u m b u h a n  e k o n o m i
y a n g  b a i k  b e l u m  t e n t u  m e n u r u n k a n  k e t i m p a n g a n  p e n d a p a t a n  j i k a  t i d a k  d i i k u t i
o l e h  l u a s n y a  l a p a n g a n  p e k e r j a a n .
K a t a  K u n c i :  K e t i m p a n g a n  P e n d a p a t a n ,  P e r t u m b u h a n  E k o n o m i ,  K o n t r i b u s i
p e r t a n i a n  d a n  I P M

x i
K e g i a t a n  p e m b a n g u n a n  s e t i a p  d a e r a h  t i d a k  d a p a t  t e r l e p a s  d a r i
k e t i m p a n g a n  p e n d a p a t a n .  K e t i m p a n g a n  p e n d a p a t a n  d a p a t  m e n i m b u l k a n  d a m p a k
p o s i t i f  m a u p u n  n e g a t i f .  D a m p a k  p o s i t i f  d a r i  k e t i m p a n g a n  p e n d a p a t a n  a d a l a h
k e t i m p a n g a n  p e n d a p a t a n  a k a n  m e m b e r i k a n  d o r o n g a n  k e p a d a  d a e r a h  y a n g
t e r b e l a k a n g  u n t u k  d a p a t  b e r u s a h a  m e n i n g k a t k a n  k u a l i t a s  h i d u p n y a .  D a m p a k
n e g a t i f  y a n g  d i t i m b u l k a n  d e n g a n  s e m a k i n  t i n g g i n y a  k e t i m p a n g a n  p e n d a p a r a n
a n t a r - w i l a y a h  a d a l a h  b e r u p a  i n e f i s i e n s i  e k o n o m i ,  m e l e m a h k a n  s t a b i l i t a s  s o s i a l ,
d a n  s o l i d a r i t a s .  K e t i m p a n g a n  p e n d a p a t a n  p e r l u  d i m i n i m a l k a n  k a r e n a  d a p a t
m e n i m b u l k a n  k e t i d a k a d i l a n  d a n  k o n f l i k  s o s i a l .  G u n a  m e m i n i m a l i s i r  k e t i m p a n g a n
p e n d a p a t a n  m a k a  t e r l e b i h  d a h u l u  h a r u s  d i k e t a h u i  f a k t o r  y a n g  d a p a t  m e n y e b a b k a n
k e t i m p a n g a n  p e n d a p a t a n .  T u j u a n  d a r i  p e n e l i t i a n  i n i  a d a l a h  u n t u k  m e m b u k t i k a n
a p a k a h  d e n g a n  m e n i n g k a t n y a  p e r t u m b u h a n  e k o n o m i ,  k o n t r i b u s i  p e r t a n i a n  d a n
I n d e k s  P e m b a n g u n a n  M a n u s i a  d a p a t  m e n g u r a n g i  t i n g k a t  k e t i m p a n g a n  p e n d a p a t a n
d i  K a l i m a n t a n  T i m u r  p a d a  t a h u n  2 0 0 9 - 2 0 1 3 .
J e n i s  p e n e l i t i a n  y a n g  a k a n  d i g u n a k a n  d a l a m  p e n y u s u n a n  t e s i s  i n i  a d a l a h
p e n e l i t i a n  e k s p l a n a t o r i  ( e x p l a n a t o r y  r e s e a r c h ) .  J e n i s  d a t a  d a l a m  p e n e l i t i a n  i n i
a d a l a h  d a t a  s k u n d e r  y a n g  p e n y u s u n  p e r o l e h  d a r i  p u b l i k a s i  B P S  k a b u p a t e n / k o t a  d i
K a l i m a n t a n  T i m u r  p a d a  t a h u n  2 0 0 9 - 2 0 1 3 .  P e n e l i t i a n  i n i  d i l a k u k a n  p a d a  1 4
k a b u p a t e n / k o t a  d i  K a l i m a n t a n  T i m u r .  A d a p u n  t e k n i k  a n a l i s i s  d a t a  d a l a m
p e n e l i t i a n  i n i  a d a l a h  d e n g a n  m e n g g u n a k a n  m e t o d e  r e g r e s i  d a t a  p a n e l .
H a s i l  p e n e l i t i a n  m e m b u k t i k a n  b a h w a  p e r t u m b u h a n  e k o n o m i  d i  P r o v i n s i
K a l i m a n t a n  T i m u r  t e r b u k t i  t i d a k  m e m b e r i k a n  p e n g a r u h  s i g n i f i k a n  t e r h a d a p
k e t i m p a n g a n  p e n d a p a t a n .  K o n t r i b u s i  p e r t a n i a n  b e r p e n g a r u h  p o s i t i f  t e r h a d a p
k e t i m p a n g a n  p e n d a p a t a n  d a n  I P M  m e m i l i k i  p e n g a r u h  p o s i t i f  t e r h a d a p
k e t i m p a n g a n  p e n d a p a t a n .  K o n t r i b u s i  p e n e l i t i a n  i n i  a d a l a h  p e r t u m b u h a n  e k o n o m i
y a n g  b a i k  b e l u m  t e n t u  m e n u r u n k a n  k e t i m p a n g a n  p e n d a p a t a n  j i k a  t i d a k  d i i k u t i
o l e h  l u a s n y a  l a p a n g a n  p e k e r j a a n .
K a t a  K u n c i :  K e t i m p a n g a n  P e n d a p a t a n ,  P e r t u m b u h a n  E k o n o m i ,  K o n t r i b u s i
p e r t a n i a n  d a n  I P M

x i i
E a c h  r e g i o n a l  d e v e l o p m e n t  a c t i v i t i e s  c a n n o t  b e  s e p a r a t e d  f r o m  t h e
i n e q u a l i t y .  T h e  i m b a l a n c e  c a n  c a u s e  a  p o s i t i v e  i m p a c t  n o r  n e g a t i v e .  T h e  p o s i t i v e
i m p a c t  o f  i n e q u a l i t y  i s  t h e  i n e q u a l i t y  w i l l  g i v e  a  b o o s t  t o  t h e  l e s s  d e v e l o p e d
r e g i o n s  i n  o r d e r  t o  t r y  t o  i m p r o v e  t h e i r  q u a l i t y  o f  l i f e .  W h i l e  t h e  n e g a t i v e  i m p a c t
c a u s e d  b y  t h e  i n c r e a s i n g  i n e q u a l i t y  b e t w e e n  r e g i o n s  i s  a  f o r m  o f  e c o n o m i c
i n e f f i c i e n c y ,  w e a k e n  s o c i a l  s t a b i l i t y  a n d  s o l i d a r i t y .  I n e q u a l i t y  n e e d s  t o  b e
m i n i m i z e d  b e c a u s e  i t  c a n  l e a d  t o  i n j u s t i c e  a n d  s o c i a l  c o n f l i c t .  I n  o r d e r  t o  m i n i m i z e
t h e  i m b a l a n c e  i t  m u s t  f i r s t  k n o w  t h e  f a c t o r s  w h i c h  c a n  c a u s e  l a m e n e s s .  T h e
p u r p o s e  o f  t h i s  s t u d y  i s  t o  p r o v e  w h e t h e r  t h e  i n c r e a s e  i n  e c o n o m i c  g r o w t h ,  t h e
c o n t r i b u t i o n  o f  a g r i c u l t u r e  a n d  t h e  H u m a n  D e v e l o p m e n t  I n d e x  c a n  r e d u c e  t h e
l e v e l  o f  i n c o m e  i n e q u a l i t y  i n  E a s t  K a l i m a n t a n  i n  t h e  y e a r  2 0 0 9 - 2 0 1 3
T h i s  t y p e  o f  r e s e a r c h  t h a t  w i l l  b e  u s e d  i n  t h e  p r e p a r a t i o n  o f  t h i s  t h e s i s  i s
e x p l a n a t o r y  r e s e a r c h .  T h e  t y p e  o f  d a t a  i n  t h i s  r e s e a r c h  i s  s e c o n d a r y  d a t a  o b t a i n e d
f r o m  t h e  p u b l i s h e d  a u t h o r  o f  d i s t r i c t s  /  C i t y  i n  E a s t  K a l i m a n t a n  i n  t h e  y e a r  2 0 0 9 -
2 0 1 3 .  T h i s  s t u d y  w a s  c o n d u c t e d  i n  1 4  d i s t r i c t s / C i t y  i n  E a s t  K a l i m a n t a n .  T h e  d a t a
a n a l y s i s  t e c h n i q u e s  i n  t h i s  r e s e a r c h  i s  t o  u s e  p a n e l  d a t a  r e g r e s s i o n  m e t h o d .
R e s e a r c h  r e s u l t  t h a t  e c o n o m i c  g r o w t h  i n  E a s t  K a l i m a n t a n  p r o v e d  a
s i g n i f i c a n t  e f f e c t  o n  i n c o m e  i n e q u a l i t y .  A g r i c u l t u r e  c o n t r i b u t e s  a  p o s i t i v e  e f f e c t
o n  i n e q u a l i t y  a n d  t h e  H D I  h a s  a  p o s i t i v e  e f f e c t  o n  i n c o m e  i n e q u a l i t y .  C o n t r i b u t i o n
o f  t h i s  r e s e a r c h  i s  a  g o o d  e c o n o m i c  g r o w t h  i s  n o t  n e c e s s a r i l y  l o w e r  t h e  i n c o m e
i n e q u a l i t y  i f  n o t  f o l l o w e d  b y  t h e  e x t e n t  o f  e m p l o y m e n t .
K e y w o r d s :  I n c o m e  I n e q u a l i t y ,  E c o n o m i c  G r o w t h ,  T h e  c o n t r i b u t i o n  o f  a g r i c u l t u r e
a n d  I P M

