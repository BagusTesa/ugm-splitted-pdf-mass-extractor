Kesimpulan yang dapat dikemukakan berdasarkan hasil penelitian dan
pembahasan adalah:
1. magnetit hasil sintesis teknik pengadukan ultrasonik memiliki rendemen
sebesar 99,1% dan kristalinitas lebih tinggi bila dibandingkan dengan
pengadukan mekanik yang memiliki rendemen sebesar 94,4%
2. pelapisan magnetit dengan silika dan MPTMS melalui proses sol-gel
dapat dilakukan dalam satu tahap
3. pelapisan magnetit meningkatkan ukuran partikel dan kestabilan terhadap
asam
4. semakin besar ukuran magnetit terlapis maka semakin besar nilai medan
koersifitas (Hc) yang dimiliki. Sebaliknya semakin besar ukuran magnetit
terlapis maka semakin kecil nilai medan saturasi (Ms) yang dimiliki.
Saran yang dapat penulis berikan untuk pengembangan penelitian
selanjutnya adalah:
1. perlu digunakannya medium pendispersi partikel magnetit ketika proses
pelapisan dilakukan
2. perlu dilakukannya kajian lebih lanjut mengenai efektivitas adsorpsi
magnetit terlapis terhadap ion logam.

