Meubel Cahaya Indah adalah suatu perusahaan furniture di kota
Purworejo yang melayani penjualan mebel. Sistem penjualan Meubel Cahaya
Indah masih menggunakan sistem yang konvensional dengan penjualan dan
pengelolaan stok barang yang masih dibukukan dengan tulisan tangan. Hal ini
memungkinkan terjadi kesalahan dalam penghitungan stok dan kesalahan dalam
transaksi masih sering terjadi.
Dari permasalahan diatas, maka dibutuhkan sebuah sistem informasi
yang mampu mengolah data transaksi dan data pengelolaan barang guna
meningkatkan kualitas penyimpanan data. Sistem yang dibuat adalah sistem
informasi penjualan di toko Meubel Cahaya Indah.
Dengan adanya sistem informasi penjualan pada Toko Meubel Cahaya
Indah, dapat mengurangi kesalahan dalam pedataan dan transaksi. Informasi yang
ditampilkan meliputi informasi petugas, informasi barang, informasi merk,
informasi kategori, informasi transaksi jual, dan informasi transaksi beli.
Kata kunci : Sistem Informasi, Penjualan di Toko Meubel Cahaya Indah

