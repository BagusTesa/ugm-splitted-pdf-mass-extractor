Perkembangan dunia pada era globalisasi memungkinkan kegiatan
perekonomian berkembang sedemikian rupa. Sejalan dengan meningkatnya
masyarakat yang memiliki pekerjaan sebagai karyawan, timbul suatu kesadaran
bahwa hidup mereka ini sangat bergantung pada perusahaan di mana mereka
bekerja. Pada saat-saat mereka masih aktif, penghasilan nampaknya bukanlah
menjadi persoalan. Sebaliknya, jika suatu saat karyawan tersebut tidak dapat
bekerja lagi pada perusahaan karena sesuatu hal, misalnya karena kecelakaan
kerja atau usia lanjut, maka kontinuitas kehidupan mereka akan terganggu. Semua
orang tentunya mendambakan masa tua yang sejahtera dan menyenangkan. Pola
pikir ini terpatri mendalam di benak sanubari setiap individu, sehingga hampir
setiap individu berusaha untuk meningkatkan penghasilannya dengan berbagai
cara agar dapat menyisihkan atau menginvestasikan penghasilannya untuk
kesejahteraan di masa datang. Berbagai cara dan metode dilakukan setiap individu
dalam menghadapi masa pensiunnya, salah satunya dengan melalui dana pensiun.
Program dana pensiun merupakan suatu sistem yang menyediakan uang
atau dana untuk pegawai atau karyawan ketika mereka sudah pensiun.  Faktor-
faktor yang menyebabkan seorang pegawai atau karyawan memasuki masa
pensiun, yaitu karena berhenti/keluar dari pekerjaan (withdrawal), cacat
(disability), pensiun normal (normal retirement) dan kematian (mortality).
Jaminan hari tua dalam bentuk Program Dana Pensiun pada dasarnya memiliki 3
fungsi utama, yaitu fungsi asuransi, fungsi tabungan dan fungsi pensiun. Program
ini memiliki fungsi asuransi karena memberikan jaminan kepada peserta untuk
mengatasi risiko kehilangan pendapatan yang disebabkan oleh kematian atau usia
pensiun. Selain itu, program pensiun memiliki fungsi tabungan, karena selama
masa program peserta diwajibkan untuk membayar iuran secara periodik. Program
pensiun bertugas untuk mengumpulkan dan mengembangkan dana yang
merupakan dana terakumulasi dari iuran peserta, yang diperlakukan seperti
program tabungan di Bank. Fungsi selanjutnya adalah fungsi pensiun, yang
merupakan jaminan atas kelangsungan pendapatan peserta setelah memasuki usia
pensiun.
Pemberi kerja yang menyelenggarakan program pensiun dalam hal ini
program pensiun manfaat pasti bertanggung jawab untuk menjaga agar dana
pensiun berada dalam keadaan dana terpenuhi. Perhitungan program dana pensiun
membutuhkan suatu asumsi yang dapat digunakan untuk memprediksi besarnya
gaji pada saat pensiun, yang biasa disebut fungsi gaji (salary function). Fungsi
gaji dipengaruhi oleh beberapa faktor antara lain inflasi dan merit (seniority).
Penelitian ini membahas penurunan model fungsi gaji yang berbentuk fungsi
eksponensial. Model fungsi gaji tersebut dibedakan berdasarkan usia (age-based
model) dan berdasarkan masa kerja (service-based model). Kedua model tersebut
diestimasi menggunakan metode Levenberg Marquardt. Selanjutnya, model yang
diperoleh akan digunakan untuk perhitungan besarnya Actuarial Liability (AL)
dan juga untuk menghitung besarnya gains dan losses dengan menggunakan
metode pendanaan pensiun Projected Unit Credit (PUC) dan Entry Age Normal
Berdasarkan latar belakang tersebut, maka tujuan penelitian ini antara lain:
1. Menyusun model fungsi gaji eksponensial berdasarkan usia (age-based
model) dan berdasarkan masa kerja (service-based model).
2. Mengestimasi parameter dari model fungsi gaji yang diperoleh dengan
menggunakan metode Levenberg-Marquardt.
3. Menghitung besarnya Actuarial Liability (AL), gains dan losses dengan
menggunakan metode pendanaan pensiun Projected Unit Credit (PUC)
dan Entry Age Normal (EAN).
Untuk menjamin keabsahan dalam kesimpulan yang diperoleh maka
pembatasan masalah sangat diperlukan. Dalam pembahasan tesis ini akan dibatasi
hal-hal sebagai berikut :
1. Faktor inflasi adalah konstan.
2. Faktor merit hanya dipengaruhi oleh salah satu faktor yaitu usia (age) atau
masa kerja (service).
3. Penelitian dilakukan terhadap data karyawan aktif yang sama dan masih
hidup, berupa usia, masa kerja dan penghasilan/gaji (salary) dari masing-
masing karyawan selama 3 tahun berturut-turut.
Program dana pensiun merupakan suatu sistem yang menyediakan uang
atau dana untuk pegawai atau karyawan ketika mereka sudah pensiun. Konsep
dasar dana pensiun yaitu pay-as-you-go dan full funded system telah dibahas
dengan jelas oleh Trowbrigde dan Farr (1976). Aitken (1996) membahas
mengenai program dana pensiun dan metode-metode pendanaan pensiun. Peneliti
selanjutnya yaitu Anderson (2006) dalam bukunya Pension Mathematics for
Actuaries juga membahas mengenai program dana pensiun secara rinci.
Pada tesis ini penulis akan melakukan kajian tentang fungsi gaji dalam
program dana pensiun. Fungsi gaji telah banyak dibahas oleh para peneliti, antara
lain oleh Marples (1962) yang membahas mengenai Salary Scale (Skala gaji).
Menurut Bowers dkk. (1997) fungsi gaji dalam Actuarial Mathematics bertujuan
untuk mengestimasi gaji yang akan datang dalam program pensiun. Model fungsi
gaji yang digunakan dalam penelitian ini adalah model fungsi gaji yang berbentuk
fungsi eksponensial seperti yang dijelaskan oleh Carriere dan Shand (1997).
Selanjutnya, pemodelan fungsi gaji dipaparkan dalam penelitian Koskinen dkk.
(2007). Zhang (2010) juga membahas model gaji seseorang yang berbentuk fungsi
eksponensial. Fungsi gaji dinotasikan sebagai ??  dengan ? merupakan usia
seorang pegawai dalam program pensiun. Secara umum ??  merupakan fungsi
tidak menurun (nondecreasing function) dalam ? yang menggambarkan kenaikan
gaji, serta berkaitan dengan inflasi dan merit (seniority). Model fungsi gaji
eksponensial dibedakan berdasarkan usia (age-based model) dan berdasarkan
masa kerja (service-based model) (Carriere dan Shand, 1997). Kedua model
tersebut diestimasi menggunakan metode regresi nonlinear Levenberg-Marquardt
(Marquardt, 1963). Metode Levenberg-Marquardt telah banyak dibahas oleh
peneliti antara lain oleh Ranganathan (2004) dalam penelitiannya membahas
tentang algoritma metode Levenberg-Marquardt dan Budiasih (2009) yang
membahas metode Levenberg-Marquardt untuk masalah kuadrat terkecil
nonlinear.
Bowers dkk. (1997) membahas fungsi survival untuk mencari peluang
seseorang akan bertahan atau tetap aktif dalam program dana pensiun. Data
sekunder yang digunakan dalam penelitian ini tidak mencukupi untuk
menyimpulkan peluang kematian dan peluang hidup yang dapat diandalkan,
sehingga penulis menggunakan Tabel Mortalitas Indonesia 2011 (TMI 2011).
Penulis akan menghitung nilai anuitas jiwa peserta setelah memasuki usia pensiun
dengan menggunakan asumsi tingkat suku bunga 10%. Kellison (2009) membahas
anuitas, teori bunga dan tingkat diskonto yang digunakan untuk mencari nilai
sekarang kewajiban aktuaria. Winklevoss (1993) telah menguraikan metode
accrued benefit yang digunakan untuk mencari kewajiban aktuaria dan iuran
normal. Penulis menggunakan metode dana pensiun Projected Unit Credit (PUC)
dan Entry Age Normal (EAN) dalam perhitungan aktuaria (Aitken, 1996).
Selanjutnya, penulis akan menghitung keuntungan (gain) dan kerugian (loss)
aktuaria (Aitken, 1996).
Penelitian tentang model fungsi gaji ini dilakukan dengan studi
kepustakaan dan bimbingan langsung dari dosen pembimbing. Penelitian ini
dimulai dengan membentuk suatu model fungsi gaji eksponensial, kemudian
memilih model terbaik antara age based model (berdasarkan usia) dan service
based model (berdasarkan masa kerja). Model yang diperoleh akan digunakan
untuk perhitungan besarnya actuarial liability (AL) dan juga untuk menghitung
besarnya gains dan losses dengan menggunakan metode pendanaan pensiun
Projected Unit Credit (PUC) dan Entry Age Normal (EAN). Bagian terakhir dari
penelitian ini adalah membuat contoh kasus dengan menggunakan data karyawan
dalam suatu instansi.
Tesis ini disusun dengan sistematika penulisan sebagai berikut:
Bab ini membahas mengenai latar belakang, pembatasan masalah,
tujuan penelitian, manfaat penelitian, metodologi penelitian,
tinjauan pustaka, serta sistematika penulisan.
Bab ini membahas tentang pengertian dasar dana pensiun, macam
dana pensiun, konsep dasar program dana pensiun, perhitungan
aktuaria, asumsi aktuaria, fungsi survival, dan fungsi dasar
aktuaria.
BAB III  PEMBAHASAN
Bab ini membahas mengenai model fungsi gaji berdasarkan usia
(age-based model) dan berdasarkan masa kerja (service-based
model), estimasi parameter menggunakan metode Levenberg-
Marquardt, metode pendanaan pensiun dan perhitungan gain dan
loss.
Bab ini membahas mengenai hasil penelitian sebuah studi kasus
dari model-model yang telah dibahas pada bab tiga.
Bab ini berisi kesimpulan yang diperoleh dari pembahasan masalah
dan saran sebagai konsekuensi dari kekurangan maupun kelebihan
dari hasil pembahasan.

