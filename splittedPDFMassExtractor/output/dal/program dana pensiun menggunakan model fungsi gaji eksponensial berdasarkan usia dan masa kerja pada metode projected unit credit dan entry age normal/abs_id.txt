Program dana pensiun merupakan suatu sistem yang menyediakan uang atau
dana untuk pegawai ketika mereka sudah pensiun.  Faktor-faktor yang menyebabkan
seorang pegawai memasuki masa pensiun, yaitu karena berhenti/keluar dari pekerjaan
(withdrawal), cacat (disability), pensiun normal (normal retirement) dan kematian
(mortality). Perhitungan program dana pensiun membutuhkan suatu asumsi yang
dapat digunakan untuk memprediksi besarnya gaji pada saat pensiun, yang biasa
disebut fungsi gaji (salary function). Fungsi gaji dipengaruhi oleh beberapa faktor
antara lain inflasi dan merit (seniority). Penelitian ini membahas penurunan model
fungsi gaji yang berbentuk fungsi eksponensial. Model fungsi gaji tersebut dibedakan
berdasarkan usia (age-based model) dan berdasarkan masa kerja (service-based
model). Kedua model tersebut diestimasi menggunakan metode Levenberg
Marquardt. Selanjutnya, model yang diperoleh akan digunakan untuk perhitungan
besarnya Actuarial Liability (AL) dan juga untuk menghitung besarnya gains dan
losses dengan menggunakan metode pendanaan pensiun Projected Unit Credit (PUC)
dan Entry Age Normal (EAN). Hasil studi kasus yang diperoleh menunjukkan bahwa
model fungsi gaji yang paling cocok dalam penelitian ini adalah model fungsi gaji
berdasarkan usia (age-based model), dengan menggunakan metode dana pensiun
Kata kunci: dana pensiun, fungsi gaji eksponensial, model fungsi gaji berdasarkan
usia, model fungsi gaji berdasarkan masa kerja, metode Levenberg
keuntungan dan kerugian aktuaria.

