Kesadaran manusia akan pentingnya penggunaan teknologi
semakin tinggi, teknologi dapat memberikan kemudahan, ketepatan dan
kecepatan dalam prosesnya. Teknologi komputer menjadi salah satu
contoh pengaplikasian teknologi dalam kehidupan, cara kerja komputer
yang cepat, tepat dan memberikan kemudahan dapat menggantikan cara
kerja konversional atau manual yang selama ini dilakukan oleh manusia.
Pemanfaatan teknologi komputer ini tidak hanya di gunakan pada
bidang yang berkaitan dengan komputer. Tapi dimanfaatkan pada berbagai
bidang kehidupan. Mulai dari bidang transportasi, pendidikan, perusahaan
swasta, instansi pemerintahan sampai ke bidang organisasi.
Organisasi adalah aktivitas kerjasama yang dilakukan oleh dua
orang atau lebih untuk mencapai suatu tujuan yang sama. Organisasi
sangat banyak sekali di Indonesia, hampir setiap instansi pemerintahan
atau swasta memiliki organisasi, dan anggota masing-masing organisasi
lebih dari sepuluh orang sehingga membutuhkan cara komunikasi yang
baik agar tujuan itu tercapai.
IPMKS (Ikatan Pelajar dan Mahasiswa Kabupaten Subang) adalah
organisasi yang sudah lama berdiri di Yogyakarta, organisasi ini bertujuan
untuk mengumpulkan warga atau masyarakat Kabupaten Subang yang
berprofesi sebagai pelajar atau mahasiswa di Yogyakarta agar tetap bisa
merasakan suasana kampung halaman.
Sebagai organisasi tentu ada banyak kegiatan yang melibatkan
anggotanya, dengan melibatkan anggota maka akan terjalin tali
persaudaraan yang sangat erat, maka dibutuhkan komunikasi yang baik
supaya tujuan itu tercapai. Pada saat ini IPMKS masih kesulitan untuk
mengumpulkan anggotanya dalam menghadiri kegiatan yang
diselenggarakan, kesulitan itu dikarenakan publikasi kegiatan kepada
anggota kurang tepat.
Selain itu IPMKS masih kebingungan dengan jumlah anggotanya,
karena masih banyak pelajar dan mahasiswa asal Kabupaten Subang ada di
Yogyakarta yang masih belum terdaftar. Saat ini IPMKS masih
menggunakan surat untuk menyebarkan publikasi kegiatan dan buku untuk
mendata anggotanya, untuk mengefektifkan kinerja IPMKS dalam
mengumpulkan data pelajar dan mahasiswa asal Kabupaten Subang maka
di butuhkan sebuah sistem yang dapat membantu kerja IPMKS menjadi
lebih efisien.
Salah satu teknologi yang dapat membantu permasalahan bagi
pendaftaran anggota dan penyebaran informasi kegiatan adalah aplikasi
publikasi kegiatan dengan menggunakan SMS Gateway. SMS Gateway
merupakan penyebaran informasi menggunakan SMS, SMS Gateway
dapat mengirim pesan ke semua nomer-nomer yang ada di database,
sehingga tidak perlu menuliskan nomer satu persatu saat mengirim pesan
karena langsung terintegrasi pada basis data. Selain itu teknologi yang
dapat membantu pendaftaran anggota IPMKS adalah dengan membuat
halaman pada website, sehingga pelajar dan mahasiswa yang ingin
mendaftar sebagai anggota IPMKS dapat mendaftar pada website tersebut
tanpa mendatangi sekretarit IPMKS.
Dari permasalahan tersebut maka pemakaian Sistem Informasi
IPMKS yang berbasis web dan dilengkapi dengan SMS Gateway
merupakan solusi untuk menyelesaikannya. Sistem informasi dalam
pengaturan manajemen IPMKS memberikan proses yang cepat dan tepat
bagi organisasi ini, seperti pendaftaran anggota dan penyebaran informasi
kegiatan. Selain itu mahasiswa asal Subang hampir semuanya
menggunakan handphone untuk melakukan komunikasi satu sama lainnya,
sehingga publikasi kegiatan dengan sms sangat efisien dilakukan.
Dari latar belakang permasalahan tersebut rumusan masalah yang
dibahas adalah:
Bagaimana membangun sebuah Sistem Informasi yang dapat
membantu pendaftaran anggota, mengirimkan publikasi kegiatan dengan
sms gateway dan menampilkan keseluruhan data anggota di IPMKS
(Ikatan Pelajar dan Mahasiswa Kabupaten Subang).
Batasan masalah dalam pengembangan Sistem Informasi IPMKS
ini adalah :
Sistem informasi ini hanya mengatur pendaftaran keanggotaan, dan
menginformasikan kegiatan kepada setiap anggota IPMKS di Daerah
Tujuan penelitian ini adalah membangun dan
mengimplementasikan �Sistem Informasi publikasi kegiatan kepada
anggota IPMKS dengan SMS Gateway� yang dapat mempermudah
organisasi melakukan pendaftaran dan penginformasian kegiatan.
Manfaat yang dapat di ambil dari penelitian ini adalah:
1. Membantu proses pengumpulan data keanggotaan IPMKS.
2. Mengurangi terjadinya kesalahan pada pendaftaran anggota
terutama dalam pengumpulan data.
3. Menginformasikan kegiatan kepada seluruh anggota IPMKS
Metode Penelitian yang dipakai dalam penelitian ini antara lain :

Meninjau langsung mekanisme pengelolaan yang dipakai oleh
intansi yang bersangkutan yaitu IPMKS (Ikatan Pelajar dan

Mengajukan pertanyaan � pertanyaan secara langsung kepada
pengurus yang terkait dengan informasi dan data yang diperlukan
dalam pengembangan Sistem Informasi IPMKS.

Studi literatur dilakukan dengan mengumpulkan data � data yang
berhubungan dengan penulisan, termasuk dalam perancangan,
analisis, dan implementasi sistem.

Membaca beberapa buku yang berkaitan dengan sistem yang akan
dirancang bisa menambah pengetahuan.
5. Pengembangan sistem
� Analisis sistem
Pengisian data keanggotaan dan kas di lakukan oleh admin
sehingga admin memegang penuh pengelolaan sistem,
modul anggota bisa di rubah menjadi pdf sehingga dapat di
bukukan untuk arsip, kegiatan akan di broadcast dengan
menggunakan sms-gatway dua minggu sebelum kegiatan
dilaksanakan, khusus untuk pengurus brodcast bukan untuk
kegiatan saja ditambah untuk persiapan kegiatan dan yang
berhubungan dengan kegiatan.
� Perancangan sistem
Sistem akan dirancang sedemikian rinci agar tidak
membingungkan pengguna dengan menggunakan DAD
(Diagram Alir Data) seperti Diagram level 1, 2, 3 dan 4
untuk menjelaskan proses-proses yang terdapat pada sistem
informasi publikasi kegiatan IPMKS, perancangan basis
data menggunakan Basic Flowchar yang terdapat pada Ms
Visio, untuk perancangan interfacenya menggunakan
aplikasi Pencil, aplikasi ini merupakan aplikasi open source
sehingga dapat di gunakan kapan saja dan dimana saja.
Sistem informasi ini menggunakan HTML, PHP, Javascript
untuk mendukung sistem dan basisdatanya menggunakan
� Uji coba
Sistem ini akan digunakan pada IPMKS (Ikatan Pelajar dan
Mahasiswa Kabupaten Subang) yang ada di Jl. Nitikan
Baru UH VI/1093 Yogyakarta 55162.
Berikut merupakan sistematika penulisan yang digunakan untuk
penyusunan penelitian sistem :
Bab ini menjelaskan tentang latar belakang masalah,
rumusan masalah, tujuan penelitian, manfaat penelitian,
batasan masalah, tujuan penelitian, manfaat penelitian,
metode penelitian dan sistematika penulisan.
Bab ini membahas dan menjelaskan tinjauan pustaka dalam
pembuatan sistem informasi ini.
Bab ini menjelaskan dan membahas landasan teori yang
menunjukan topik pembahasan pengembangan sistem.
Bab ini menjelaskan tentang analisa kebutuhan sistem untuk
menentukan rancangan yang digunakan untuk
pengembangan sistem informasi IPMKS dengan fitur SMS
Bab ini menjelaskan rinci penerapan dari sistem serta
rancangan Sistem Informasi IPMKS dengan fitur SMS
Gateway. Penerapan didukung pengujian sistem.
Bab ini berisi tentang hasil penelitian dan pembahasan.
Bab ini berisi tentang kesimpulan -  kesimpulan Sistem
Informasi IPMKS berdasarkan pembahasan dan pemaparan
dari bab sebelumnya serta saran untuk pengembangan
sistem berikutnya untuk dikembangkan.

