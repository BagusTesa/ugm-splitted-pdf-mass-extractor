Flight model is one of importing thing in fixed-wing unmanned aerial
vehicle (UAV) control system development, mainly in the aircraft autopilot.
Through this flight model, the aircraft motion can be represented and simulated.
In this research, the flight model is obtained through system identification
and system modelling of Bixler fixed-wing  unmanned aerial vehicle. System
identification is based on experiment data and use state-space model structure.
There are three stages in this research, aircraft motion system modelling, flight
data collecting, and flight model identification. Through those three stages, Bixler
fixed-wing unmanned aerial vehicle flight model is obtained as represented in two
modes, longitudinal mode and lateral mode
The Bixler fixed-wing unmanned aerial vehicle longitudinal mode flight
model is obtained using 13 parameters. The lateral mode is obtained using 11
parameters. All modes are in 4
th
order state space model structure.

