Indonesia merupakan salah satu pemasok minyak sawit terbesar di dunia
yaitu sebesar 31% sehingga dapat dikatakan bahwa ketersediaan minyak sawit di
Indonesia sangat melimpah. Minyak sawit dapat digunakan untuk bermacam
kebutuhan. Penggunaan yang paling banyak adalah untuk kebutuhan rumah
tangga sebagai minyak goreng. Selain itu juga digunakan sebagai salah satu
bahan baku dalam industri kosmetik, sabun, dan industri makanan.
Pemerintah melalui Peraturan Kementrian Perindustrian nomor 13 Tahun
2010 menginstruksikan bahwa 60 persen produksi CPO nasional harus diolah di
dalam negeri dan 40 persen sisanya masih boleh diekspor. Langkah tersebut
merupakan upaya pemerintah untuk  meningkatkan nilai tambah produk CPO di
dalam negeri. Untuk  mendukung program pemerintah tersebut, maka
diperlukan diversifikasi pengolahan minyak sawit menjadi produk turunannya.
Salah satu produk pengolahan minyak sawit yang banyak manfaatnya adalah
surfaktan.
Surfaktan merupakan senyawa yang mempunyai dua ujung aktif, yaitu sisi
polar dan non-polar. Peran surfaktan dalam suatu sistem emulsi adalah
menurunkan tegangan antara muka (antara minyak dan air). Selain itu surfaktan
yang diproduksi secara sintesis kimiawi maupun biokimiawi dapat berperan
sebagai  komponen bahan adhesif, bahan penggumpal, pembasah, pembusa,
pengemulsi, dan bahan penetrasi, serta telah diaplikasikan secara luas pada
berbagai bidang industri yang menggunakan sistem multifasa, seperti pada
industri makanan, farmasi, kosmetika, tekstil, polimer, cat, detergen, dan
agrokimia. Secara sederhana, kegunaan penting surfaktan dapat kita dapati
dalam kehidupan sehari-hari yaitu pada sabun dan agen pembersih lainnya.
Tidak dapat dipungkiri bahwa kebutuhan akan surfaktan di seluruh dunia
semakin lama semakin meningkat seiring dengan pertambahan jumlah populasi
penduduk serta karena penggunaannya dalam kehidupan sehari-hari maupun
dalam industri. Hal tersebut bisa dilihat dari data permintaan surfaktan yang
disajikan Global Surfactant Market bulan Juni 2013,  yaitu mencapai 26,8 miliar
dollar Amerika dan akan bertambah hingga 3,8% per tahun sehingga akan
mencapai 31,2 miliar dollar pada 2016 dan 36,1 miliar dollar pada 2020.
Selain itu, akhir-akhir ini industri migas dihadapkan dengan masalah yang
cukup serius yaitu perolehan minyak bumi yang semakin menurun karena cukup
banyak minyak yang tertinggal di reservoir. Dalam pengambilan minyak bumi
biasanya meninggalkan sisa sekitar 35 - 40%. Sisa minyak yang tertinggal di
reservoir tersebut bisa diperoleh menggunakan surfaktan.
Terdapat berbagai jenis surfaktan komersial, namun kebanyakan
surfaktan komersial tersebut berbasis petrokimia, contohnya Linear Alkyl
Benzene Sulfonat (LABS), yang memiliki cukup banyak kekurangan antara lain
tidak dapat diperbaharui, harganya mahal, tidak tahan pada kesadahan tinggi,
dan tidak ramah lingkungan karena sulit didegradasi (Hidayati, 2009). Surfaktan
berbasis minyak nabati merupakan salah satu alternatif pengganti surfaktan
berbasis petrokimia. Metil Ester Sulfonat (MES) merupakan surfaktan anionik
berbasis minyak nabati yang mengandung asam lemak rantai C16-C18 yang
ramah lingkungan dan bersifat biodegradable. Biaya produksinya juga relatif
lebih murah dibandingkan dengan biaya produksi surfaktan berbasis petrokimia.
Biaya untuk proses produksi surfaktan berbasis petrokimia sebesar 928 dollar/
ton sedangkan MES sebesar 525 dollar/ton (Watkins, 2001).
Proses sulfonasi dapat dilakukan dengan mereaksikan agen sulfonasi
dengan minyak, asam lemak, ataupun ester asam lemak. Untuk sintesis MES
agen sulfonasi direaksikan dengan suatu metil ester asam lemak. Menurut
Bernardini (1983) dan Pore (1993) agen sulfonasi yang dapat digunakan untuk
membuat surfaktan adalah gas sulfur trioksida (SO3), H2SO4 pekat, oleum,
NaHSO3, NH2SO3H, dan ClSO3H. Untuk menghasilkan kualitas produk terbaik,
beberapa variabel yang harus diperhatikan adalah rasio mol, temperatur reaksi,
konsentrasi sulfat yang ditambahkan, waktu netralisasi, jenis dan konsentrasi
katalis, pH, dan temperatur netralisasi (Foster, 1996).
Penelitian sintesis metil ester sulfonat telah banyak dilakukan dengan
berbagai metode. Hovda (1996) telah mendapatkan paten dengan melakukan
sintesis MES dengan cara mereaksikan metil ester asam lemak dengan gas SO3.
Sintesis MES dengan gas SO3 juga dilakukan oleh Sheats dan MacArthur (2002).
Menurut Foster (1996), proses sulfonasi menggunakan gas SO3 dilakukan dengan
melarutkan gas SO3 dalam udara yang sangat kering kemudian  direaksikan
secara langsung dengan bahan baku organik yang digunakan. Reaksi SO3 dengan
bahan organik cukup cepat dan bersifat stoikiometrik. Proses ini cukup rumit
sehingga diperlukan kontrol proses yang ketat dan hanya sesuai untuk proses
bersifat kontinyu dengan volume produksi yang besar. Selain itu dibutuhkan
peralatan produksi yang mahal dengan tingkat ketepatan yang tinggi.
Selain gas SO3, NaHSO3 dapat digunakansebagai agen pensulfonasi.
Hidayati (2009) telah melakukan sintesis metil ester sulfonat dari minyak nabati
berbasis CPO dengan melakukan optimasi pada rasio mol, temperatur, dan lama
reaksi menggunakan agen pensulfonasi NaHSO3. Hasil optimum didapatkan pada
rasio mol reaktan 1:1,5 pada temperatur 106 ?C dan lama reaksi 4,5 jam.
Menurut Smith (1981), pada umumnya proses sulfonasi menggunakan
katalis vanadium pentoksida (V2O5) . Oleh karena harga katalis ini sangat mahal
yaitu  sekitar 16 juta/kg maka perlu penggunaan alternatif katalis yang dapat
digunakan pada proses sulfonasi yang memiliki sifat hampir menyerupai
vanadium pentoksida dalam mempercepat proses sulfonasi dan harganya lebih
murah dibandingkan vanadium pentoksida.
Ariani (2011) telah melakukan penelitian sintesis metil ester sulfonat dari
minyak sawit dengan natrium bisulfit dengan variasi berbagai macam katalis
padat, yaitu Al2O3, CaO, dan TiO2 sebanyak masing-masing 1%. Diketahui bahwa
dengan katalis CaO pada temperatur reaksi 90 ?C dan waktu reaksi 4,5 jam
memberikan persen hasil metil ester sulfonat terbanyak sebesar 59,13%.
Berdasarkan penelitian yang telah dilakukan, penulis mencoba melakukan
penelitian membuat metil ester sulfonat dari bahan baku minyak sawit dengan
adanya katalis CaO dalam proses sulfonasi. Berat katalis CaO dioptimasi  0,5 - 2%
dari berat sistem. Selanjutnya, diuji sifat dan kinerja dari metil ester sulfonat
hasil sintesis.
Penelitian ini bertujuan untuk
1. Membuat metil ester dari minyak sawit sebagai bahan baku metil ester
sulfonat melalui reaksi transesterifikasi minyak sawit dengan metanol
menggunakan katalis natrium hidroksida.
2. Membuat metil ester sulfonat dari metil ester minyak sawit melalui proses
sulfonasi dengan agen pensulfonasi NaHSO3 dan katalis CaO.
3. Mengetahui berat katalis CaO dalam reaksi sulfonasi yang menghasilkan MES
optimum.
Jika penelitian ini berhasil, diharapkan dapat memberikan informasi
pengaruh katalis CaO pada sifat dan kinerja metil ester sulfonat dari metil ester
minyak sawit, supaya didapatkan surfaktan metil ester sulfonat yang kinerjanya
lebih baik dan lebih ramah lingkungan. Selain itu juga memberikan salah satu
warna baru dari produk turunan CPO yang lebih bernilai jual dan lebih
bermanfaat di berbagai bidang.

