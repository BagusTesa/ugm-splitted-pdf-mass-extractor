Berdasarkan hasil perancangan, implementasi dan pengujian terhadap
sistem yang dibangun, dapat diambil kesimpulan sebagai berikut :
1) Telah berhasil dibangun server beserta web service yang menopang
berjalannya sebuah aplikasi LBA yang berjalan pada platform Android.
2) Server dapat melakukan pengolahan data untuk menopang berjalannya
sebuah client aplikasi LBA.
3) Iklan yang ditampilkan pada aplikasi berupa file teks dan gambarr.
4) Proses penginputan dan pencarian data toko serta tempat umum menjadi
lebih mudah dengan adanya integrasi dengan Google Maps.
Saran yang dapat diberikan untuk penelitian selanjutnya adalah sebagai
berikut :
1) Perlu ditambahkan fitur messaging antar pengguna (admin) sehingga
mempermudah proses komunikasi antar pengguna.
2) Iklan yang dapat ditampilkan nantinya diharapkan tidak hanya berupa file
teks dan gambar, tetapi juga video maupun suara.

