Research on reservoir characterization to determine reservoir physics
properties, area of hydrocarbon prospect zone, and calculate reserved original oil
in place (OOIP) has been done on GRAHA field, in South Sumatera Basin.
Petrophysics data analysis is used to determine shale volume, porosity, water
saturation and netpay thickness. Seismic data analysis provides the area of
hydrocarbon prospect zone. The result of petrophysiscs and seismic data analysis
is used to estimate reserved original oil in place (OOIP) of the research area.
Petrophysics analysis showed average thickness of hydrocarbon prospect
zone is 14.92m, with 8.96 %  average shale volume, 19.72% average effective
porosity, and 47% average water saturation. Seismic data analysis showed that the
area of hydrocarbon prospect zone is 9.95 km2. Total reserved original oil in place
on GRAHA field is 16.98 million barrels.

