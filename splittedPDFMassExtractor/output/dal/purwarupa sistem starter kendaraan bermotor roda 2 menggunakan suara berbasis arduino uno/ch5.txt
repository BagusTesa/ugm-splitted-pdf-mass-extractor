Berdasarkan perancangan, pengujian, dan pembahasan mengenai
purwarupa sistem starter digital otomatis dengan kode suara yang berbasis Arduino
Uno dapat ditarik beberapa kesimpulan dari sistem tersebut, diantarannya :
1. Purwarupa starter kendaraan motor roda 2 dengan kode pengamanan
menggunakan suara telah diimplementasikan dengan baik.
2. Sensor suara dengan menggunakan condenser microphone yang
dihubungkan ke modul EasyVR dapat bekerja dengan baik dan efektif,
frekuensi kepekaan sensor dalam mendeteksi suara dapat diatur.
3. Motor DC akan berputar setelah kode suara perintah masukan diberikan
yakni perintah suara START dan STOP untuk mematikan motor.
4. Alat dapat berjalan dengan input masukan suara yang diberikan berbeda
setelah modul EasyVR sudah disetting penerimaan masukan suaranya.
5. Alarm akan berbunyi jika pengucapan kata pada perintah suara tidak tepat
dan akan berbunyi terus sampai perintah masukan suara benar.
Berikut ini adalah saran-saran untuk keperluan pengembangan lebih lanjut:
1. Sensor suara yang digunakan pada sistem ini kepekaannya dalam
menangkap suara sangatlah kuat atau terlalu sensitif, suara apapun yang
tertangkap oleh sensor dapat menjadi input sistem.
2. Rancangan mekanik pada purwarupa starter motor digital otomatis ini sangat
sederhana. Dapat diubah ke desain mekanik yang lebih bagus dengan sistem
keamanan yang lebih baik.
3. Sistem sensor suara menggunakan modul EasyVR masih berupa sensor suara
yang sederhana, dapat diganti dengan pendeteksi suara yang lebih bagus
yakni DSK TMS320C6713.
4. Bentuk kemasan yang dibuat lebih menarik dan efisien sehingga dapat
menarik  daya  beli  konsumen.

