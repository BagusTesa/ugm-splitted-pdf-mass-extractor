Berdasarkan hasil penelitian yang telah dilakukan pada pembuatan sistem
PLL (Phase-Locked Loop) menggunakan TSA5511 pada pemancar FM
(Frekuensi Modulasi) ini dapat diperoleh beberapa kesimpulan yaitu sebagai
berikut:
1. Sistem PLL ini dapat bekerja dengan baik yaitu dengan kemampuannya
mengunci seluruh frekuensi osilasi dalam range 87,5 MHz hingga 108 MHz.
2. Nilai ralat dari hasil pengukuran output frekuensi pada sistem PLL adalah
sebesar � 0,924 KHz.
3. Besar penguatan (gain) yang dimiliki oleh bagian VCO pada sistem PLL ini
adalah sebesar 5,32 MHz/Volt.

Setelah pengujian pada sistem ini, masih terdapat kekurangan yang
terdapat di dalamnya, oleh karena itu untuk penelitian lebih lanjut mengenai
sistem PLL menggunakan TSA5511 ini terdapat beberapa saran yang perlu
diperhatikan, antara lain:
1. Agar didapatkan sebuah sistem pemancar FM secara utuh, maka perlu
ditambahkan bagian-bagian penunjang seperti buffer, penguat akhir, serta low
pass filter pada sistem ini.
2. Perlu dilakukan penyesuaian ulang pada komponen bagian loop filter sehingga
deviasi frekuensi yang dimiliki menjadi lebih kecil.
3. Untuk dapat menghasilkan frekuensi output yang lebih tinggi lagi untuk
penerapan fungsi yang lainnya maka perlu dilakukan penyesuaian pada bagian
VCO (Voltage Controlled Oscillator) yaitu dengan mengganti nilai induktansi
atau mengganti dioda varaktor dengan range kapasitansi yang lebih lebar.
4. Tegangan input sinyal yang terlalu besar dapat mengganggu kemampuan
TSA5511 dalam melakukan komparasi frekuensi. Kedepannya diharapkan agar
sistem PLL ini mampu untuk menerima input sinyal dengan tegangan yang
lebih besar.

