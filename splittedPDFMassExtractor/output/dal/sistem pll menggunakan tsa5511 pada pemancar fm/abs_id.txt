Kebutuhan akan adanya informasi menjadi suatu kebutuhan yang sangat
penting belakangan ini. Tak terkecuali informasi yang bersumber dari siaran radio
FM lokal yang masih memiliki banyak pendengar setia, berbagai macam
informasi yang diberikan di dalamnya dapat dijadikan bahan referensi murah
meriah yang dapat di rasakan oleh berbagai lapisan masyarakat.
Sejak tahun 2000 perkembangan pendirian stasiun radio utamanya radio
komunitas mulai merebak dikarenakan hanya membutuhkan teknologi sederhana
dengan biaya yang murah karena siarannya dapat dijangkau secara gratis (Kasmita
dkk., 2009).
Terkait dengan pendirian stasiun radio tersebut, berdasarkan surat
keputusan Direktur Jenderal Pos dan Telekomunikasi Nomor 46/Dirjen/2004
tertanggal 10 Mei 2004 mengenai persyaratan teknis alat dan perangkat radio
siaran modulasi frekuensi (frequency modulation/FM) bahwa persyaratan
perangkat pemancar pada bagian RF (radio frequency) carrier harus memiliki
stabilitas frekuensi dalam range � 1 KHz dari frekuensi utama (Dirjen Pos dan
Dari persyaratan tersebut di atas maka diperlukan sebuah pemancar yang
memiliki tingkat kestabilan output frekuensi yang baik untuk dapat membangun
radio siaran FM. Yang menjadi permasalahan adalah bahwa menurut Anonima
(2000) kestabilan frekuensi dari osilator direct FM tidak cukup bagus. Oleh
karena itu dibutuhkan sebuah sistem otomatis yang digunakan untuk mengontrol
frekuensi yang dibangkitkan oleh osilator tersebut. Dengan adanya sistem
pengontrol ini diharapkan frekuensi yang dibangkitkan osilator akan menjadi
lebih stabil dan terkontrol.
Salah satu jenis sistem pengontrolan frekuensi otomatis yang dapat
digunakan adalah kalang fasa terkunci atau biasa disebut phase-locked loop

