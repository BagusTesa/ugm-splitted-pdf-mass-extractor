Pisang adalah salah satu komoditas buah unggulan Indonesia yang
memberikan kontribusi besar terhadap angka produksi buah nasional. Buah pisang
Indonesia memasok kebutuhan tidak hanya pasar dalam negeri, tetapi juga pasar
internasional. Luas panen dan produksi pisang selalu menempati posisi pertama.
Setiap tahunnya, produksi pisang di Indonesia menunjukkan peningkatan, terlihat
pada Tabel 1.1.
Tabel 1.1 Produksi pisang di Indonesia
2004 4.874.439
2005 5.177.607
2006 5.037.472
2007 5.454.226
2008 6.004.615
2009 6.373.533
Peningkatan produksi tersebut tentu harus dibarengi dengan peningkatan
kualitas dan mutu buah pisang. Keseragaman kualitas dan kemasakan buah-buah
tersebut tetap menjadi faktor penentu pada semua tahapan berikutnya. Pemerintah
melalui Badan Standardisasi Nasional telah menetapkan standar untuk buah pisang,
yaitu SNI 7422:2009. Standar ini mengakomodasi karakteristik semua jenis pisang
konsumsi yang ada dan memperhatikan kondisi di lapangan.
Untuk meningkatkan mutu buah pisang, hal yang paling berpengaruh adalah
penanganan pascapanen. Penanganan pascapanen meliputi pembersihan, sortasi,
penyimpanan, pengemasan, dan pemasaran. Proses sortasi merupakan titik kritis
dalam penanganan pascapanen produk pertanian. Proses sortasi yang dilakukan
secara manual oleh manusia, menghasilkan produk dengan keragaman kurang baik
dan juga waktu yang relatif lama. Hal ini disebabkan antara lain karena keragaman
visual manusia, aspek ergonomika yaitu kelelahan manusia, dan perbedaan persepsi
tentang mutu dari produk yang dihasilkan. Melihat pentingnya proses sortasi, maka
diperlukan adanya sistem yang mampu mengklasifikasikan buah pisang sesuai
standar mutu yang telah ditetapkan.
Teknologi yang dapat diterapkan pada proses sortasi salah satunya
pengolahan citra. Teknologi pengolahan citra adalah salah satu teknologi yang
dikembangkan untuk mendapatkan informasi dari citra dengan cara memodifikasi
bagian dari citra yang diperlukan sehingga menghasilkan citra lain yang lebih
informatif. Penggunaan teknologi pengolahan citra diharapkan dapat meningkatkan
akurasi sortasi dan pemutuan buah berdasarkan kualitas dan kemasakannya.
Kondisi buah dapat didekati dari ukuran obyek dalam citra bila diambil dengan latar
belakang yang kontras dengan warna buah yang diamati. Tingkat kemasakan buah
bisa didekati dari pengamatan warna dan tekstur kulit buah.
Hubungan antara parameter mutu dengan kelompok mutu produk pertanian
biasanya sangat komplek, oleh sebab itu diperlukan suatu teknik yang dapat
menggambarkan hubungan tersebut secara baik seperti yang dilakukan oleh otak
manusia. Jaringan saraf tiruan (JST) merupakan sebuah struktur komputasi yang
dikembangkan dari proses sistem jaringan saraf biologi di dalam otak. Pada
dasarnya JST terdiri dari beberapa lapisan node yaitu sebuah lapisan masukan,
lapisan tersembunyi dan sebuah lapisan keluaran, unit komputasi yang paling
sederhana dalam setiap lapisan disebut node dan terhubung satu sama lain.
Keuntungan dari metode JST adalah dapat membangun fungsi non linier dan hanya
memerlukan data masukan dan keluaran tanpa mengetahui dengan jelas proses
dalam jaringan. Hal ini cocok diterapkan pada data citra.
Bagaimana membuat sebuah program yang dapat mengklasifikasikan mutu
buah pisang berdasarkan SNI Pisang 7422:2009 menggunakan pengolahan citra
digital untuk mengekstrak fitur citra dan jaringan saraf tiruan untuk mengidentifkasi
mutunya.
Batasan masalah dalam penelitian ini adalah :
1. Jenis buah pisang yang digunakan adalah pisang emas.
2. Citra pisang diambil 2 kali, sisi atas dan sisi bawah, menggunakan kamera
digital CANON IXUS 230HS dengan resolusi 4000 x 2248 pixel.
3. Citra pisang diambil per buah di dalam kotak khusus pengambilan citra
dengan posisi pisang harus pas mewakili tiap sisinya.
4. Kriteria mutu buah pisang berdasarkan SNI Pisang 7422:2009.
5. Parameter yang dilihat adalah warna, ukuran dan tekstur buah pisang.
6. Pengolahan citra digital dan jaringan saraf tiruan diimplementasikan
menggunakan program Matlab R2012a.
Tujuan dari penelitian ini dapat dituliskan sebagai berikut :
1. Menentukan parameter klasifikasi mutu buah pisang berdasarkan SNI
dengan teknik pengolahan citra digital.
2. Merancang jaringan saraf tiruan untuk klasifikasi mutu buah pisang.
3. Melakukan validasi rancangan jaringan saraf tiruan untuk klasifikasi mutu
buah pisang.
1.5 Manfaat Penelitian
Hasil dari tugas akhir ini diharapkan dapat memberikan manfaat untuk
pengembangan sistem sortasi buah pisang yang diintegrasikan dengan mesin sortasi
sehingga dapat mengklasifikasikan buah pisang secara otomatis.
Metodologi penelitian yang akan dilakukan pada penelitian ini meliputi :

Dilakukan  untuk mendapatkan acuan dalam menentukan tema.
2. Kajian dan Pembelajaran
Melakukan kajian mengenai sistem yang akan dibuat dengan mempelajari
artikel, jurnal, buku, dan karya tulis yang berkaitan. Selain itu juga melakukan
konsultasi dengan dosen pembimbing untuk mendapatkan masukan.

Membuat rancangan sistem meliputi kotak pengambilan citra, program
segmentasi, program ekstraksi fitur, program deteksi cacat, klasifikasi dengan
jaringan saraf tiruan sampai pembuatan GUI (Graphical User Interface).

Rancangan yang telah dibuat diimplementasikan menggunakan Matlab.

Pada tahap ini, dilakukan pengujian untuk melihat kinerjanya apakah sudah
sesuai dengan tujuan atau belum dan dianalisis jika terdapat kekurangan.
Sistematika dari penulisan laporan ini yaitu :
Berisi tentang latar belakang penelitian, rumusan masalah, batasan masalah,
tujuan penelitian, metodologi penelitian serta sistematika penulisan.
BAB II: TINJAUAN PUSTAKA
Berisikan tentang hasil penelitian terdahulu yang mempunyai keterkaitan
dengan penelitian yang dilakukan oleh penulis.
BAB III: DASAR TEORI
Berisi penjelasan dan teori mengenai konsep dan landasan teori yang menjadi
dasar penyusunan tugas akhir ini.
Berisi penjelasan tentang perancangan sistem yang dibuat prinsip kerja
sistem yang berupa diagram alir dan diagram blok.
Berisikan tentang implementasi dari perancangan dalam bentuk nyata
terhadap kinerja perangkat lunak yang telah dibuat.
Bab ini berisi tentang hasil pengujian sistem secara keseluruhan pada objek
pengamatan. Hasil pengujian kemudian dianalisis dan dibahas hasil dan kinerjanya.
Bab ini berisi kesimpulan dan saran-saran sehingga sistem ini dapat
dikembangkan lebih lanjut dengan perancangan dan metode yang lebih baik.

