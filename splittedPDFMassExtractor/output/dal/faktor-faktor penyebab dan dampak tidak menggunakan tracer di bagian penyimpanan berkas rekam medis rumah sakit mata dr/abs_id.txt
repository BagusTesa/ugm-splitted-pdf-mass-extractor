Latar belakang: Penggunaan tracer bertujuan untuk mempermudah petugas dalam
menelusuri keberadaan rekam medis yang tidak berada di rak penyimpanan. Petugas di
bagian penyimpanan tidak menggunakan tracer pada waktu mengambil berkas rekam
medis. Selain itu di Rumah Sakit Mata �Dr. Yap� Yogyakarta akan ada akreditasi.
Tujuan: Penelitian ini bertujuan mengetahui faktor-faktor penyebab dan dampak tidak
menggunakan tracer di bagian penyimpanan berkas rekam medis di Rumah Sakit Mata
Metodologi Penelitian: Jenis penelitian yang digunakan adalah deskriptif dan
pendekatan kualitataif dengan rancangan cross sectional. Metode pengambilan data
dengan menggunakan wawancara, observasi dan studi dokumentasi.
Hasil Penelitian: faktor-faktor penyebab tidak menggunakan tracer di bagian
penyimpanan berkas rekam medis di Rumah Sakit Mata �Dr. Yap� Yogyakarta yaitu
Sumber Daya Manusia (SDM) yaitu petugasnya tergesa-gesa, sarana di bagian
penyimpanan yaitu rak penyimpanan sudah penuh dan Prosedur Tetap pengambilan dan
penyimpanan rekam medis terkait penggunaan tracer yang tidak dijalankan. Dampak
tidak menggunakan tracer di bagian penyimpanan berkas rekam medis di Rumah Sakit
Mata �Dr. Yap� Yogyakarta yaitu misfile dan berkas rekam medis sulit terlacak.

