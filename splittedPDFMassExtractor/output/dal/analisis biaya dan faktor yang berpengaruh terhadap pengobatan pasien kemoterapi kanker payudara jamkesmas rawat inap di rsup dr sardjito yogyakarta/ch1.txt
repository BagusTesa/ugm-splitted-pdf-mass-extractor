Kesehatan adalah hak dan investasi, dan semua warga negara berhak atas
kesehatannya termasuk masyarakat miskin. Diperlukan suatu sistem yang
mengatur pelaksanaan bagi upaya pemenuhan hak warga negara untuk tetap hidup
sehat, dengan mengutamakan pada pelayanan kesehatan bagi masyarakat miskin.
Di Indonesia, falsafah dan dasar negara Pancasila terutama sila ke-5 juga
mengakui hak asasi warga atas kesehatan. Hak ini juga termaktub dalam UUD 45
pasal 28H dan pasal 34, dan diatur dalam UU No. 23/1992 yang kemudian diganti
dengan UU 36/2009 tentang kesehatan. Dalam UU 36/2009 ditegaskan bahwa
setiap orang mempunyai hak yang sama dalam memperoleh akses atas sumber
daya di bidang kesehatan dan memperoleh pelayanan kesehatan yang aman,
bermutu, dan terjangkau. Sebaliknya, setiap orang juga mempunyai kewajiban
turut serta dalam program jaminan kesehatan sosial.
Untuk mewujudkan komitmen global dan konstitusi di atas, pemerintah
bertanggung jawab atas pelaksanaan jaminan kesehatan masyarakat melalui
Jaminan Kesehatan Nasional (JKN) bagi kesehatan perorangan. Jaminan
Kesehatan Nasional (JKN) yang dikembangkan di Indonesia merupakan bagian
dari Sistem Jaminan Sosial Nasional (SJSN). Sistem jaminan sosial nasional ini
diselenggarakan melalui mekanisme asuransi kesehatan sosial yang bersifat wajib
(mandatory) berdasarkan Undang-Undang No.40 Tahun 2004 tentang sistem
jaminan sosial nasional. Tujuannya adalah agar semua penduduk Indonesia
terlindungi dalam sistem asuransi, sehingga mereka dapat memenuhi kebutuhan
dasar kesehatan masyarakat yang layak (Kemenkes, 2014).
Misi pembangunan kesehatan Indonesia adalah memelihara dan
meningkatkan pelayanan kesehatan yang bermutu, merata dan terjangkau. Dalam
rangka meningkatkan mutu pelayanan kesehatan khususnya untuk masyarakat
miskin, kementrian kesehatan Republik Indonesia telah menentukan kebijakan
penerapan konsep INA-CBG�s (Indonesia Case Base Groups) sebagai sistem
pembayaran pelayanan kesehatan. Hal ini sesuai dengan keputusan menteri
kesehatan RI nomor 903/MENKES/PER/V/2011 tentang pedoman pelaksanaan
program Jaminan Kesehatan Masyarakat (Jamkesmas) Tahun 2011.
Meningkatnya biaya kesehatan telah menjadi perhatian besar dan menjadi
salah satu isu penting bagi pasien, praktisi kesehatan, pihak asuransi, pemerintah,
dan masyarakat secara umum, dikarenakan tidak semua bentuk produk dan
pelayanan kesehatan dapat dengan mudah untuk diperoleh oleh pasien. Hal ini
membuat suatu analisis ekonomi di bidang kesehatan memainkan peranan penting
sebagai sumber informasi kesehatan, dengan tujuan untuk meningkatkan
penggunaan produk dan pelayanan kesehatan secara optimal dan efisien
(Wertheimer dan Chaney, 2003).
Kanker payudara adalah suatu penyakit dimana terjadi pertumbuhan
berlebihan atau perkembangan tidak terkontrol dari sel � sel jaringan payudara.
Kanker payudara merupakan jenis kanker yang sering ditemukan pada
kebanyakan wanita. Menurut penelitian Globocan (2008) yang dilakukan
International agency for research on cancer di Indonesia, kanker payudara
merupakan kanker paling banyak terjadi dengan angka kematian nomor 3 setelah
kanker paru dan kolon. Berdasar data SIRS tahun 2007, kanker payudara
menempati urutan pertama pada pasien rawat inap diseluruh rumah sakit  di
Indonesia yaitu 16, 85 % (Depkes, 2008).
Kanker payudara dapat diterapi melalui beberapa cara, yaitu melalui cara
pembedahan, radioterapi, terapi endokrin, terapi biologis tertarget dengan antibodi
monoklonal, dan kemoterapi. Kemoterapi dengan menggunakan obat anti-kanker
merupakan salah satu terapi yang paling banyak digunakan pada kasus kanker
payudara, baik itu sebagai terapi adjuvan dan neoadjuvan pada kanker payudara
stadium awal, maupun sebagai terapi utama pada kanker payudara stadium lanjut
(American cancer society, 2013). Pemberian kemoterapi pada pasien kanker
payudara berbeda-beda pada setiap pasien tergantung pada stadium kanker
payudara yang dideritanya. Perbedaan tersebut terletak pada regimen kemoterapi
yang diberikan, yang meliputi jenis dan dosis obat sitotoksik yang diberikan,
interval waktu pemberian obat sitotoksik, serta jumlah siklus kemoterapi yang
dijalani oleh pasien. Hal ini menjadi salah satu faktor yang mempengaruhi
perbedaan pada lamanya perawatan pasien dan besarnya biaya yang ditanggung
setiap pasien kanker payudara (Lidgren, 2007).
Penggunaan obat-obat kemoterapi juga dapat menimbulkan berbagai
masalah seperti efek samping dan toksisitas akibat penggunaan kemoterapi,
dimana hal ini juga dapat mempengaruhi besarnya biaya yang perlu dikeluarkan
oleh pasien. Dari data yang diketahui, kerugian ekonomi di seluruh dunia akibat
kanker payudara pada tahun 2011 diperkirakan mencapai $US300-400 milyar
dengan $US100-140 milyar diantaranya adalah pengeluaran untuk biaya medis
langsung. Pada satu dekade terakhir, sekitar $US500 milyar dikeluarkan untuk
menangani penyakit mematikan ini (Estim, 2011). Berdasarkan latar belakang
tersebut, peneliti merasa perlu melakukan penelitian untuk mengetahui rerata
besarnya biaya terapi yang dikeluarkan oleh pasien kemoterapi kanker payudara
yang menjalani rawat inap di RSUP Dr. Sardjito Yogyakarta, yang merupakan
rumah sakit rujukan utama untuk pengobatan kanker payudara di Daerah Istimewa

Permasalahan yang timbul dari permasalahan ini adalah :
1. Berapa rerata biaya dan komponen biaya apa yang paling besar pada
pasien kemoterapi kanker payudara rawat inap Jamkesmas di RSUP Dr
2. Bagaimana gambaran pola dan rerata biaya kemoterapi tiap regimen pada
pasien kemoterapi kanker payudara rawat inap Jamkesmas di RSUP Dr
3. Faktor apa saja yang mempengaruhi biaya riil pada pasien kemoterapi
kanker payudara rawat inap Jamkesmas di RSUP Dr. Sardjito Yogyakarta?

1. Untuk mengetahui rerata biaya dan komponen biaya mana yang
merupakan komponen biaya tertinggi pada pelayanan pasien kemoterapi
kanker payudara rawat inap Jamkesmas di RSUP Dr Sardjito ?
2. Untuk melihat pola dan rerata biaya kemoterapi tiap regimen pada pasien
kemoterapi kanker payudara rawat inap Jamkesmas di RSUP Dr Sardjito
3. Untuk mengetahui faktor apa saja yang mempengaruhi biaya riil pada
pasien kemoterapi kanker payudara rawat inap Jamkesmas di RSUP Dr.

1. 1. Untuk Depkes RI, sebagai bahan masukan dalam menentukan kebijakan
pemeliharaan pelayanan kesehatan masyarakat miskin.
2. Untuk pihak Asuransi / Pembayar, penelitian ini bisa menjadi salah satu
acuan/dasar pertimbangan dalam penentuan tarif INA-CBG�s.
3. Untuk pihak RSUP Dr Sardjito Yogyakarta, penelitian ini diharapkan
dapat menjadi masukan untuk pihak manajemen dalam mengambil
kebijakan terkait dengan efisiensi dan efektivitas serta peningkatan mutu
kualitas pelayanan pasien kanker payudara rawat inap jamkesmas
4. Penelitian ini diharapkan dapat menjadi dasar bagi penelitian selanjutnya

Penelitian tentang Analisis biaya terapi pasien kemoterapi kanker
payudara jamkesmas rawat ianp  di RSUP Dr Sardjito Yogyakarta sepanjang
penelusuran belum pernah dilakukan. Pelaksanaan penelitian yang sudah
dilaksanakan dan ada hubungannya dengan penelitian tersebut :
1. Rohana Namsa (2008) yang meneliti tentang Evaluasi Biaya Terapi Pasien
Askeskin di RS PKU Muhammadiyah Yogyakarta tahun 2007. Tujuan dari
penelitian ini adalah untuk mengetahui selisih biaya terapi pasien
Askeskin rawat inap, mengetahui besar biaya penggunaan bahan & Alkes
habis pakai di depo rawat inap yang tidak ditanggung PT Askes, dan
mengetahui besar biaya penggunaan obat diluar standar yang ditetapkan
yang tidak ditanggung PT Askes di RS PKU Muhammadiyah Yogyakarta.
2. Mantik Uji (2013) yang meneliti tentang � Analisis Biaya Terapi pada
Pasien Gagal Ginjal Kronik Rawat Inap dengan Hemodialisa di RSUP Dr.
Sardjito Yogyakarta Tahun 2011�. Penelitian ini bertujuan untuk
mengetahui rata-rata biaya total & komponen biaya terapi pasien penderita
gagal ginjal kronik yang melakukan hemodialisa, mengetahui faktor yang
berpengaruh terhadap total biaya terapi pasien gagal ginjal kronik dengan
hemodialisa.
Perbedaan penelitian ini dengan penelitian lainnya adalah pada subyek penelitian,
tempat, metode dan analisis datanya.

