Perkembangan teknologi sebagai hasil peradaban manusia yang semakin
maju dirasakan sangat membantu dan mempermudah manusia dalam memenuhi
kebutuhan hidupnya di zaman modern seperti sekarang ini. Dalam dunia
laboratorium teknologi yang semakin canggih memegang peran penting dalam
proses meningkatkan kinerja dari suatu kegiatan.
Sampai saat ini banyak peralatan-peralatan laboratorium yang masih bersifat
manual artinya pemakaian alat tersebut di dalam penggunaanya, kurang begitu
efisien, banyak waktu yang terbuang, serta kurang begitu aman bagi pengguna
ataupun peralatan yang lainnya.
Purwarupa otomatisasi robot lengan ini dibuat untuk membantu dalam
proses kegiatan di Laboratorium Riset Elektronika dan Instrumentasi. Purwarupa
otomatisasi robot lengan sistem sensor rasa adalah alat yang digunakan untuk
membawa sensor rasa yang terdiri dari beberapa channel.
Selama ini pengukuran rasa dasar maupun sampel larutan rasa yang
diberikan dengan menggunakan sensor rasa yang dilakukan di Laboratorium Riset
Elektronika dan Instrumentasi masih dilakukan secara manual, dimana sensor
diletakkan ke dalam larutan sampel kemudian diukur dan data disimpan. Pada
Gambar 1.1 sistem sensor rasa komersial misalnya model SA402 Anritsu (Toko,
2000), telah menggunakan auto sampler sebagai penggerak sampel yang bergerak
secara otomatis, sampel larutan yang diletakkan pada sejumlah tempat yang
tersedia kemudian secara otomatis komputer akan menggerakkan dan mengukur
sampel larutan secara bergantian, kemudian data ditampilkan secara online.
Gambar 1.1 Auto sampler sensor rasa komersial model SA402 anritsu
Berdasarkan  permasalahan  tersebut  maka  untuk  memudahkan  pengukuran
sensor rasa tersebut yang dapat bekerja, maka diperlukan sebuah auto sampler
yang berbentuk lengan robot untuk memudahkan pengukuran dengan
menggunakan sensor rasa.
1.3 Tujuan dan Manfaat
Penulisan Tugas Akhir Purwarupa Otomatisasi Robot Lengan ini bertujuan
untuk mengotomatisasi lengan robot sistem sensor rasa berbasis komputer. Tujuan
umum lainnya dalam penulisan Tugas Akhir ini adalah mengembangkan dan
mengimplementasikan purwarupa robot lengan. Manfaat dari Tugas Akhir ini dipakai
untuk menangani sensor rasa dalam melakukan pengukuran.
Dalam perancangan dan penulisan tugas akhir yang berjudul �Purwarupa
Otomatisasi Robot Lengan Berbasis Komputer� akan ditentukan batasan-batasan
masalah yang meliputi :
1. Perancangan ulang sistem minimum dan driver motor dikarenakan yang
sebelumnya kurang berfungsi dengan baik
2. Penggunaan mikrokontroler ATmega32 dan komputer yang berfungsi
sebagai pengendali utama sistem ini
3. Penggunaan driver motor dengan metode jembatan H, sebagai pengatur
putaran motor DC 12 volt
4. Pemrograman sistem dengan bahasa basic menggunakan Basic Compiler
dan menggunakan Antarmuka serial menggunakan kabel prolific dengan
pemrograman Visual Basic 6.0
5. Otomasi Robot lengan sistem sensor rasa ini dapat digunakan tiga mode,
yaitu mode otomatis, mode menggunakan antarmuka secara manual, dan
mode menggunakan antarmuka secara otomatis.
Metode-metode  yang  digunakan  dalam  melakukan  penelitian  ini  adalah
sebagai berikut:
1. Menentukan tujuan, manfaat, dan batasan masalah dengan melihat faktor-
faktor dan sistem lain yang bermanfaat bagi pengembangan sistem ini
2. Membuat perancangan sistem yang terdiri dari dua bagian,yaitu:
a. Perangkat Keras (Hardware)
Meliputi pembuatan desain rangkaian skematik dan PCB (Printed Circuit
Board) sistem minimum mikrokontroller ATmega32 dengan software
eagle, dan driver motor.
b. Perangkat Lunak (Software)
Meliputi pembuatan program untuk mikrokontroler ATmega32 dengan
software BASCOM (Basic Compiler) AVR serta user interface
menggunakan software Visual Basic 6.0.
3. Setelah sistem dirancang, selanjutnya direalisasikan secara keseluruhan.
4. Sistem yang telah dibuat, diujicoba, dan dianalisa dengan mengintegrasikan
semua bagian secara keseluruhan. Sehingga dapat memastikan bahwa sistem
telah bekerja dengan baik dan hasilnya sesuai dengan yang diinginkan.
Penyusunan   laporan   Tugas   Akhir   Purwarupa   Robot   Lengan ini dilakukan
dengan sistematika sebagai berikut:
Meliputi latar belakang dan permasalahan, rumusan masalah, batasan masalah,
tujuan penelitian, metode penelitian, dan sistematika penulisan.
Memuat tentang informasi-informasi tentang hasil penelitian yang telah dilakukan
terkait dengan perancangan robot lengan pembawa sensor rasa ini.
Memuat tentang landasan teori setiap komponen yang menunjang dalam
pembuatan dan pembahasan tugas akhir ini.
BAB IV: PERANCANGAN SISTEM
Bab ini berisi perancangan sistem yang terdiri atas bagian sistem perangkat keras
(Hardware) dan bagian perangkat lunak (software) yang digunakan.
Memuat uraian tentang implementasi sistem secara detail sesuai dengan
rancangan dan berdasarkan komponen yang digunakan.
Bab ini memuat sistem secara keseluruhan, baik hardware maupun software, serta
uraian dan penjelasan tentang sistem dari auto sampler sensor rasa berdasarkan
listing program,mikrokontroler dan antarmuka.
Berisi kesimpulan yang memuat uraian singkat tentang hasil penelitian yang
diperoleh sesuai dengan tujuan penelitian, serta saran untuk penelitian lebih lanjut
serta kesimpulan secara teori maupun praktek, dan saran-saran yang dianggap
perlu diperhatikan sehubungan dengan pembuatan alat tersebu

