1.1 Latar Belakang dan Permasalahan
Bidang ilmu analisis merupakan salah satu cabang ilmu matematika yang
di dalamnya banyak membicarakan konsep, aksioma, teorema, lemma disertai
pembuktian dan contoh- contoh baik yang bersifat ilustrasi maupun penyangkal.
Di dalam analisis khususnya analisis fungsional, beberapa ruang yang sering
dibicarakan adalah ruang linear, ruang bernorma, ruang Banach, ruang  re-Hilbert,
dan ruang Hilbert. Ruang pre-Hilbert merupakan ruang linear X yang dilengkapi
dengan fungsi yang memetakan setiap anggota X X?  ke suatu bilangan
kompleks dan memenuhi aksioma-aksioma tertentu. Fungsi inilah yang kemudian
dikenal dengan produk skalar (inner product) pada X. Ruang pre-Hilbert yang
lengkap disebut ruang Hilbert. Pemetaan dari suatu ruang linear ke ruang linear
yang lain atau dari suatu ruang linear ke ruang linear yang sama disebut operator.
Diberikan ruang Hilbert X dan Y atas lapangan yang sama, yaitu F. Lapangan F
yang dimaksud pada tulisan ini adalah ?  atau ? . Operator :T X Y?  dikatakan
linear jika untuk setiap ,x y X?  dan F??  berlaku ( ) ( ) ( )T x y T x T y? ? ?  dan
( ) ( )T x T x? ?? . Operator linear :T X Y?  dikatakan terbatas jika terdapat
konstanta 0M ?  sehingga ( )T x M x?  untuk setiap x X? . Himpunan semua
operator linear terbatas dari X  ke Y  ditulis ? ?,B X Y . Lebih lanjut, dalam hal X =
Y, ? ?,B X X  dituliskan ? ? ? ? atau B X B Y .
Diberikan ruang Hilbert X atas lapangan F dan ( )T B X? , operator T
dapat didekomposisikan menjadi T U T?  dengan :U X X?  operator isometri
parsial dan T  akar kuadrat positif dari
*T T . Selanjutnya, untuk bilangan p>0,
operator linear kontinu T yang memiliki sifat ? ? ? ?* *
p p
T T TT?  disebut sebagai
operator hiponormal-p yang ekuivalen mengatakan
22 *
pp
T T? , dengan
2 *T T T? . Untuk p=1, operator T disebut operator hiponormal dan untuk
p ?  ,
operator T disebut operator semi-hiponormal. Untuk 0<p<1, operator hiponormal-
p telah diteliti oleh matematikawan, diantaranya Aluthge (1999), Duggal (1995),
dan Xia (1980). Dalam Aluthge (1999) disebutkan bahwa apabila T operator
hiponormal-p dan T U T?  dekomposisi dari T dengan U  operator isometri
parsial, maka operator
1 1
2 2T T U T??  disebut transformasi Aluthge. Selain itu,
dalam Furuta (1996) disebutkan bahwa untuk 1 p? >0, apabila T operator
hiponormal-p, maka untuk setiap q dengan q p? ,
1
hiponormal- 1
2
q q p
T T U T
q
? ?
? ?? ?
? ?
? . Untuk bilangan q>0, operator
q q
T T U T??  merupakan perluasan dari transformasi Aluthge. Dalam Berberian
(1961) disebutkan bahwa apabila ( )T B H?  operator kompak dan
? ?( ) \ 0ap T? ?? , maka ? ?( ) \ 0p T? ?? . Dalam Akkouchi (2008), disebutkan sifat-
sifat spektrum operator linear terbatas diantaranya jika ? ?TS ? ??  dan T?  tidak
surjektif, maka ( )r T? ?? . Selanjutnya, dalam Patel (1995) disebutkan juga sifat-
sifat spektrum hiponormal-p diantaranya jika untuk
1
0< <
2
p , T hiponormal-p ,
maka ( ) ( )jp pT T? ?? .
Hal tersebut kemudian membawa pemikiran untuk menyelidiki
karakteristik operator T yang memiliki sifat ? ? ? ?* *
p p
T T TT? . Pembahasan
mengenai karakteristik operator hiponormal-p pada tulisan ini, lebih ditekankan
pada sifat- sifat operator hiponormal-p pada ruang Hilbert, hubungan operator
hiponormal-p dengan operator hiponormal-w, Class (A), paranormal, dan sifat-
sifat spektrum titik operator hiponormal-p. Oleh karena itu, perlu dikaji lebih
lanjut tentang karakteristik dan sifat- sifat operator hiponormal-p pada ruang
1.2 Tujuan dan Manfaat Penelitian
Tujuan penelitian ini adalah untuk memberikan pemahaman dan
pengetahuan mengenai sifat- sifat dan karakteristik operator hiponormal-p (p > 0)
pada ruang Hilbert. Pembahasan mengenai operator hiponormal-p pada ruang
Hilbert bermanfaat membantu mengembangkan ilmu matematika dan aplikasinya,
khususnya analisis fungsional.
Untuk p>0, pembahasan tentang operator hiponormal-p pada ruang
Hilbert diawali dengan pendefinisian ruang Hilbert terlebih dahulu kemudian
dilanjutkan dengan pembahasan mengenai operator pada ruang Hilbert dan
terakhir dibahas tentang operator hiponormal-p. Kreyszig (1978) dan Berberian
(1961), menjelaskan bahwa ruang bernorma X adalah ruang linear yang dilengkapi
dengan norma yang didefinisikan padanya dan dituliskan dengan pasangan
? ?, .X , sedangkan ruang linear X yang dilengkapi dengan fungsi produk skalar
disebut ruang pre-Hilbert dan ruang bernorma ? ?, .X  dikatakan lengkap jika
setiap barisan Cauchy di dalam X konvergen di X. Berberian (1961), memberikan
penjelasan mengenai definisi dan konsep dasar tentang ruang Hilbert diantaranya
setiap ruang pre-Hilbert X merupakan ruang bernorma terhadap norma .  :
,x x x?  untuk setiap x X?
dan ruang pre-Hilbert yang lengkap disebut ruang Hilbert. Kreyszig (1978),
memberikan penjelasan, untuk X dan Y ruang bernorma atas lapangan yang sama,
yaitu F, operator :T X Y?  dikatakan linear jika ( ) ( ) ( )T x y T x T y? ? ?  untuk
setiap ,x y X?  dan ( ) ( )T x T x? ??  untuk setiap x X?  dan F?? . Selanjutnya,
operator linear :T X Y?  dikatakan terbatas jika terdapat konstanta 0M ?
sehingga ( )T x M x?  untuk setiap x X? . Lebih lanjut, himpunan semua
operator linear terbatas dari X  ke Y  ditulis ? ?,B X Y dengan X dan Y ruang
bernorma.
Selanjutnya, Kreyszig (1978) memberikan penjelasan mengenai definisi
operator linear tertutup pada ruang bernorma, diketahui X dan Y ruang bernorma
dan operator linear :T X Y? dikatakan tertutup jika grafik dari T, yaitu
? ?? ?( ) , : ( ), ( )G T x y x D T y T x? ? ?
tertutup pada ruang bernorma X Y? . Berberian (1961) juga memberikan
penjelasan mengenai definisi dan konsep operator adjoint pada ruang Hilbert yang
diawali dengan membahas Teorema Representasi Riesz. Selain itu, Kreyszig
(1978) juga memberikan penjelasan mengenai konsep dasar spektrum pada ruang
Hilbert yang nantinya akan digunakan untuk menyelidiki sifat-sifat spektrum titik
operator hiponormal-p pada ruang Hilbert.
Konsep mengenai operator hiponormal-p merupakan salah satu konsep
yang dipelajari dalam teori analisis fungsional khususnya operator. Furuta (1996)
dan Huruya (1997) menjelaskan bahwa untuk p>0, Operator T dikatakan
hiponormal-p jika ? ? ? ?* *
p p
T T TT? , ekuivalen mengatakan
22 *
pp
T T? , dengan
2 *T T T? . Aluthge (1999) mendefinisikan, T operator linear terbatas pada H
dengan H ruang Hilbert, T operator hiponormal-p, dan T U T?  dekomposisi dari
T dengan ( )U B H? isometri parsial, operator
1 1
2 2T T U T??  disebut transformasi
Aluthge. Furuta (1996) memberikan sifat-sifat berkaitan dengan operator
hiponormal-p pada ruang Hilbert, diantaranya untuk 1 p? >0, jika T operator
hiponormal-p maka untuk setiap q sehingga q p? ,
1
hiponormal- 1
2
q q p
q
? ?
? ?? ?
? ?
? .
Aluthge dan Wang (1999) memberikan penjelasan mengenai hubungan
operator hiponormal-p dengan operator hiponormal-w, Class (A), paranormal
pada ruang Hilbert. Selain itu, Cho dan Jin (1995) memberikan penjelasan bahwa
T merupakan operator Class (A) tetapi untuk setiap p>0, T bukan operator
hiponormal-p . Berberian (1961) dan Akkouchi (2008), memberikan penjelasan
mengenai sifat-sifat spektrum operator linear terbatas pada ruang Hilbert. Selain
itu, Patel (1995), menjelaskan mengenai sifat-sifat spektrum operator hiponormal-
p pada ruang Hilbert.
Metode yang digunakan dalam penyusunan tesis ini adalah studi literatur
mengkaji semua konsep yang berkaitan dengan operator hiponormal-p pada ruang
Hilbert serta sifat- sifatnya dari buku, paper, dan sumber- sumber lain yang
bersesuaian. Namun, pada buku, paper maupun sumber yang lain bukti tidak
lengkap sehingga peneliti akan melengkapi bukti-buktinya. Selain itu, penulis
juga senantiasa berkonsultasi mengenai materi pembahasan dengan dosen
pembimbing. Adapun langkah-langkah penelitian di dalam tesis sebagai berikut:
1. Memahami definisi operator hiponormal-p pada ruang Hilbert.
2. Menyelidiki sifat-sifat hiponormal-p.
3. Menyelidiki hubungan operator hiponormal-p dengan operator hiponormal-w,
Class (A), dan paranormal.
4. Menyelidiki sifat-sifat spektrum operator hiponormal-p.
Pada langkah ini, dibahas mengenai sifat-sifat spektrum titik operator
hiponormal-p, spektrum pendekatan (approximate spectrum), dan spektrum
titik hubungan (joint point spectrum). Selain itu, dibahas mengenai hubungan
antara spektrum titik operator hiponormal-p, spektrum pendekatan
(approximate spectrum), dan spektrum titik hubungan (joint point spectrum).
Tesis ini terdiri dari empat bab. Di dalam BAB I, yaitu pendahuluan,
dibahas mengenai latar belakang dan permasalahan, tujuan dan manfaat
penelitian, tinjauan pustaka, metode penelitian serta sistematika penulisan tesis.
Dilanjutkan ke BAB II, yang berisi dasar teori. Dalam bab ini, dibahas mengenai
konsep yang akan digunakan dalam pembahasan selanjutnya, diantaranya konsep
ruang bernorma, ruang Hilbert, basis ortonormal, operator linear, operator linear
tertutup, operator adjoint, operator pada ruang Hilbert, dan spektrum titik operator
linear pada ruang Hilbert. Kemudian dilanjutkan ke dalam BAB III yang berisi
pembahasan dari hasil penelitian. Dalam BAB III difokuskan untuk membahas
definisi dan sifat- sifat operator hiponormal-p pada ruang Hilbert, hubungan
operator hiponormal-p dengan operator hiponormal-w, Class (A), paranormal,
dan sifat- sifat spektrum operator hiponormal-p. Terakhir, BAB IV memuat
tentang kesimpulan dari hasil penelitian.

