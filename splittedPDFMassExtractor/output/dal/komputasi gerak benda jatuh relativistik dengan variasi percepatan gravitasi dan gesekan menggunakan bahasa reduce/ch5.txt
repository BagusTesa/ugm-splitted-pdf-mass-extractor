Dalam penelitian ini telah berhasil dilakukan komputasi dua kondisi antara
gerak benda jatuh non relativistik dengan gerak benda yang dikoreksi relativistik
serta variasi percepatan gravitasi dan gesekan menggunakan bahasa Reduce.
Dalam komputasi ada ralat pemotongan sehingga hasil komputasi adalah ? =
1,0999 � 0,001 (satuan panjang). Perbedaan nilai antara gerak benda jatuh non
relativistik dengan gerak benda yang dikoreksi relativistik serta variasi percepatan
gravitasi dan gesekan adalah sebesar 0,00537 (satuan panjang).

Dalam tulisan ini masih banyak yang perlu dikaji dan dikembangkan lebih
jauh lagi, seperti materi gerak benda dengan menggunakan variasi besaran yang
berbeda, atau materi gerak benda jatuh relativistik variasi percepatan gravitasi
dengan nilai konstanta yang sudah ada.

