Laboratorium komputer merupakan salah satu fasilitas di program studi
D3 Komputer dan Sistem Informasi Universitas Gadjah Mada untuk menunjang
pembelajaran praktikum mahasiswa. Layaknya pembelajaran teori, dalam
pembelajaran praktikum di laboratorium komputer terdapat kuis sebagai metode
evaluasi pembelajaran dan pengambilan nilai. Selama ini kuis dilakukan dengan
cara yang belum terkomputerisasi. Pertanyaan kuis ditulis di papan tulis atau
melalui media proyektor kemudian dikerjakan oleh mahasiswa. Pengerjaan kuis
juga masih menggunakan kertas kemudian dikumpulkan kepada asisten untuk
dikoreksi. Ketika proses pengerjaan kuis, mahasiswa terkadang merasa perlu
untuk mencatat pertanyaan sehingga membutuhkan tambahan waktu. Selain itu
pada proses koreksi kuis memungkinkan tulisan jawaban mahasiswa untuk tidak
terbaca, atau lembar jawaban sobek, kotor, maupun hilang. Ketika kuis selesai
dilakukan, seringkali mahasiswa tidak mengetahui nilainya sehingga tidak bisa
menjadi bahan koreksi bagi mahasiswa tersebut. Berdasarkan permasalah tersebut,
diperlukan sebuah sistem yang mampu menangani proses kuis praktikum yang
dapat mengurangi inefisiensi dan kesalahan pada proses kuis.
Sistem Informasi Kuis Elektronik (e-Quiz) Praktikum Laboratorium
komputer D3 Komputer dan Sistem Informasi Univeritas Gadjah Mada dibangun
sebagai sarana pemecahan masalah di atas. Diharapkan dengan adanya sistem ini
proses kuis tersebut dapat lebih efisien dan dapat mengurangi kesalahan pada
proses koreksi maupun penghitungan nilai.
Sistem Informasi Kuis Elektronik (e-Quiz) Praktikum Laboratorium
komputer D3 Komputer dan Sistem Informasi Univeritas Gadjah Mada ini
dibangun menggunakan framework Laravel dan DBMS MySQL. Sistem ini
mampu menangani  proses manajemen data praktikum, proses kuis dan koreksi,
bank soal, dan manajemen data nilai.
Kata kunci: sistem informasi, kuis, praktikum, laravel.

