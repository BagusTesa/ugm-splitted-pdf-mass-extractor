This prototype using a several component , such as door security system
based matrix keypad used to enter the PIN for the security door, and a temperature
sensor (LM35) as a component that will detect the presence of dangerous heat in
the house, that to all of its components to connect to automatically send messages
(SMS) to your cell phone if the homeowner has indicated the existence of danger
detected by these tools.
From the test results, this warning system devices will be send a message
to the user cell phone when if the PIN which is in the program is wrong, so the
devices automatically send a message and turn on a buzzer as a indicator and
when PIN correct, servo turn on and the door will be open. Then with the
temperature detector, this devices will send a message to the user cell phone if a
temperature in the area or room reached 40 degree celsius.
Keywords:  SMS gateway, temperature Detector, ATmega32, Indicators, keypad
matrix, servo

