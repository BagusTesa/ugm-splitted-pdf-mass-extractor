Telah dilakukan analisa petrofisika sumur pemboran dan interpretasi
seismik untuk mengidentifikasi reservoar hingga perhitungan volumetrik
cadangan minyak bumi pada lapangan M formasi Talang Akar cekungan Jawa
Barat Utara yang menjadi wilayah kerja Asset 3 PT. Pertamina EP. Penelitian ini
bertujuan guna mengetahui parameter petrofisika batuan (kandungan serpih,
porositas,  saturasi air, permeabilitas) dan mengidentifikasi suatu reservoar beserta
kandungan hidrokarbonnya dengan hasil akhir berupa besar cadangan
hidrokarbonnya.
Dalam analisa petrofisika, untuk memperoleh parameter petrofisika seperti
volume serpih digunakan persamaan non-linear Clavier, porositas efektif dengan
persamaan densitas/neutron Bateman-Konen, saturasi air dengan persamaan
Modified Simandoux, dan permeabilitas dengan persamaan Coates FFI.
Interpretasi seismik dilakukan untuk memperoleh volume reservoar berdasarkan
geometrinya sehingga dapat dihitung besar cadangan hidrokarbon dengan
persamaan volumetrik.
Dari hasil analisa yang difokuskan pada formasi Talang Akar cekungan
Jawa Barat Utara diketahui tiga reservoar Zone-01, Zone-02, dan Zone-03,
dimana kemudian Zone-01 dan Zone-02 digabungkan menjadi satu reservoar
Zone-01-02. Hasil petrofisika secara rata-rata diperoleh reservoar yang tergolong
cukup baik dengan volume serpih dibawah 30%,  porositas efektif diatas 10%,
saturasi air efektif di bawah 70%, permeabilitas diatas 15 mD dengan kandungan
minyak bumi. Pada akhirnya diketahui jumlah cadangan minyak bumi pada Zona-
01-02 sebesar 1,27 juta barrel dan Zone-03 sebesar 1,73 juta barrel.
Kata Kunci : Wireline Log, Petrofisika, Interpretasi seismik, Volumetrik

