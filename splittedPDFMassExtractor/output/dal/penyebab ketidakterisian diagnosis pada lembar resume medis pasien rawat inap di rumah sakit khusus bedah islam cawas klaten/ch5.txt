1. Penyebab ketidakterisian diagnosis pada lembar resume medis rawat
inap di Rumah Sakit Khusus Bedah Islam Cawas Klaten ada
beberapa faktor yaitu :


Kesibukan dokter di Rumah Sakit Khusus Bedah Islam Cawas
Klaten adalah karena banyaknya pasien yang harus ditangani.
Para dokter spesialis akan visite jika ada jadwal praktek
poliklinik saja, visite dilakukan sebelum dokter spesialis
praktek di poliklinik. sedangkan di poliklinik pasien telah
menunggu, hal tersebut yang membuat dokter spesialis
tergesa gesa dan tidak menulis diagnosis saat pasien akan
pulang.

Usaha pihak perawat untuk mengingatkan dokter mengisi
diagnosis pada lembar resume medis tidak membuahkan hasil
dan berkas rekam medis hanya tertumpuk. Karena hal
tersebut maka perawat  sudah jarang mengingatkan dokter
dan langsung mengembalikan berkas rekam medis ke unit
rekam medis.
3. Petugas rekam medis
Petugas rekam medis juga tidak melakukan assembling
sehingga berkas rekam medis yang kembali tidak di
assembling dan hanya ditulis dibuku register kemudian
dikembalikan ke ruang filing.
Selain itu petugas rekam medis juga mengusahakan agar
diagnosis pada lembar resume medis terisi dengan
mengingatkan kepada koordinator untuk disampaikan kepada
dokter, tetapi langkah petugas rekam medis tersebut tidak
membawa perubahan.
2. Dampak ketidakterisian diagnosis pada lembar resume medis pasien
rawat inap di Rumah Sakit Khusus Bedah Islam Cawas Klaten.
Di Rumah Sakit Khusus Bedah Islam Cawas Klaten kesulitan dalam
pembuatan surat keterangan medis di karenakan diagnosis pada
lembar resume medis tidak terisi, hal tersebut menyulitkan petugas
karena harus menghubungi dokter yang merawat untuk  mengisi
diagnosisnya. Hal tersebut akan memakan waktu yang lebih lama
dalam pembuatan SKM.

1. Sebaiknya petugas rekam medis melaksanakan assembling untuk
mengecek kembali berkas rekam medis yang kembali dari
bangsal.
2. Sebaiknya dokter meluangkan waktu melengkapi berkas rekam
medis.
3. Sebaiknya direktur, komite medik, dan petugas rekam medis
melakukan sosialisasi kebijakan dan standar prosedur operasional
mengenai pengisian resume medis.

