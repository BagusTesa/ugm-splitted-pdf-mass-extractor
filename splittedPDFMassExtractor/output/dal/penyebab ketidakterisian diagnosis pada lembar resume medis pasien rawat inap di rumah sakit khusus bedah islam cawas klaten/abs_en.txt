Background: Resume or summary of the history of home is a summary of the
entire period of care and treatment of patients as has been attempted by the
health workers and other interested parties. This sheet must be signed by the
doctor who treated patients (Hatta, 2008). Resume should be written in the
patient's home. Resumes should contain a summary of the findings and
important events during admission, the state time to go home, and subsequent
treatment plan. Medical resumes a sheet that is used as the manufacture of
medical certificate for the purposes of third parties such as insurance claims,
medical record file when it is time to eradicate the medical resume sheet is still
stored and maintained. This is a resume sheet sheet that can be copied by the
patient, according to the Minister of Health no. 269 of 2008 Chapter IV Article 12,
paragraph 4 is described "as a summary of the medical records referred to in
Section 3 can be given, be noted by the patient or the authorized person or the
patient's written consent or the patient's family is entitled to it".
Objective: Knowing the diagnosis unfilled resume sheet rawatt hospitalized
medical patients at the Hospital for Special Surgery Islam Cawas Klaten.
Special purpose: to find the cause unfilled diagnosis on inpatient medical resume
sheet at the Hospital for Special Surgery Islam Cawas Klaten. And the impact of
the diagnosis on the sheet unfilled resume inpatient medical at the Hospital for
Special Surgery Islam Cawas Klaten.
Method: a descriptive study using a qualitative approach to data collection is
cross-sectional. The subjects in this study were each a medical records clerk,
nurse, doctor. Techniques of data collection by interview, observation, and
documentation.
Results: Causes unfilled diagnosis on inpatient medical resume sheet at the
Hospital for Special Surgery Cawas Klaten Islam there are several factors:
human factors: 1.) Doctor: Busy physician at the Hospital for Special Surgery
Islam Cawas Klaten. The specialist will visite if no scheduled practice clinic alone,
visite performed before a specialist practice in the clinic. while in the clinic
patients have been waiting for, that's what makes specialist a hurry and do not
write resumes diagnosis pda sheet. 2.) Nurse: the nurse's efforts to alert
physicians to fill medical diagnosis on a resume sheet does not produce results
and medical record file just stacked. Because it has been rarely reminding the
nurses and doctors instantly restore the file medical records to medical records.
3.) The records officer: medical record file back in assembling and just not written
in the book of registers and then restored to the filing room. Unfilled impact on
sheet resume medical diagnosis is the difficulty in making a medical certificate in
because of the diagnosis on a sheet of medical resume is not filled, the official
medical record should contact the treating physician to fill the diagnosis and take
a longer time in the manufacture of Medical Certificate.
Keywords: unfilled, diagnosis, medical resume.

