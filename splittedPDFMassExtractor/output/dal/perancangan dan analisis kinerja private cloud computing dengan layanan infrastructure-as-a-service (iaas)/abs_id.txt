 
 
I Gusti Ngurah Wikranta Arsa
Mesin server merupakan salah satu penunjang dan komponen utama
yang harus ada dalam mengembangkan suatu karya ilmiah dengan berbasis
web. Mahalnya server menjadi kendala mahasiswa/mahasiswi dalam
menghasilkan suatu karya ilmiah. Konfigurasi server yang dapat dilakukan
dimana saja dan kapan saja menjadi sebuah keinginan mendasar, selain
pemesanan mesin yang mudah, cepat dan fleksibel. Untuk itu  diperlukan
sebuah sistem yang dapat menangani permasalahan tersebut. Cloud computing
dengan layanan Infrastructure-As-A-Serveice (IAAS) dapat menyediakan sebuah
infrastruktur yang handal. Untuk mengetahui kinerja sistem diperlukan suatu
analisis performance antara server cloud (instance) dengan server
konvensional. Hasil penelitian dari analisis kinerja private cloud computing
dengan layanan Infrastructure-As-A-Service (IAAS) ini menunjukkan bahwa
perbandingan kinerja satu server cloud atau server virtual cloud dengan satu
server konvensional tidak jauh berbeda namun akan terlihat perbedaan kinerja
yang signifikan jika dalam satu server node terdapat lebih sari satu server
virtual dan sistem ini memberikan tingkat penggunaan resource server yang
lebih maksimal.
Kata kunci: Cloud Computing, Infrastructure As-A-Service (IAAS),
analisis Performance.
 

