Upaya para fisikawan, khususnya fisikawan teoretik  untuk mengungkap feno-
mena alam adalah dengan diajukannya berbagai macam model hukum alam berda-
sarkan data-data empiris yang telah dimiliki. Sejauh ini, ada tiga jenis pemodelan
hukum alam yang dikenal, yaitu model fisis, model matematis, dan model metafisis.
Model fisis adalah model yang digunakan untuk menerjemahkan suatu masalah
yang dikaitkan dengan konsep-konsep ilmiah. Model matematis adalah  realitas ma-
tematis yang dipilih untuk mewakili realitas fisis. Model metafisis adalah model
yang digunakan untuk memahami penyebab segala sesuatu menjadi ada. Akan
tetapi pada prakteknya, model matematislah yang lebih sering digunakan di bidang
fisika karena dianggap lebih operasional dibanding model lainnya. Selain itu, hasil
model matematis dapat berupa bilangan sehingga dapat dibandingkan dengan eks-
perimen. Model matematis atau teori itu, suatu saat akan dinyatakan tidak lulus uji
apabila terdapat satu saja hasil eksperimen yang tidak sesuai dengan teori tersebut
Salah satu teori klasik yang cukup mashur di bidang fisika adalah mekanika
dan teori gravitasi. Mekanika klasik diusulkan oleh Newton untuk menjelaskan
gerak. Sementara, gravitasi dimaksudkan sebagai gaya tarik-menarik antara dua
benda bermassa yang bergantung pada massa dan jarak antara keduanya. Newton
pun berpendapat bahwa ruang dan waktu bersifat mutlak. Newton melihat ruang
dan waktu secara objektif dan harus berlaku secara universal. Konsep pemikiran ini
menitikberatkan pada gerakan yang terjadi pada suatu benda, baik dalam keadaan
diam maupun dalam keadaan bergerak. Benda bergerak atau diam, akan tetap
bergerak atau tetap pada posisi semula, kecuali ada gaya luar yang
memengaruhinya. Oleh karena setiap benda yang bergerak akan tetap bergerak dan
pergerakan tersebut terjadi dalam ruang dan waktu, maka kemutlakan juga berlaku
pada ruang dan waktu. Newton pun berpendapat bahwa waktu mutlak dapat berge-
rak dan mengalir tanpa mengacu pada peristiwa tertentu. Pemutlakan ruang dan
waktu ini bertahan dalam kurun waktu yang cukup lama, yaitu kurang lebih dua
abad sejak perumusan mekanika klasik dan gravitasi disempurnakan oleh Newton.
Akhirnya, pemutlakan ruang dan waktu itu dimandulkan oleh Einstein pada 1905.
Pada saat itu, Einstein berusaha agar Teori Relativitas Khusus (TRK) yang diaju-
kannya konsisten dengan Teori Elektromagnetik Maxwell (TEM).
Menurut Einstein, ruang dan waktu tidak lagi bersifat mutlak tetapi relatif.
Ruang dianggap bersifat relatif karena dipandang sebagai hubungan antara benda-
benda yang diukur dengan cara-cara tertentu. Jika pengukurannya dilakukan deng-
an cara berbeda (pengamat berbeda), maka hasilnya juga berbeda.  Waktu pun di-
anggap bersifat relatif karena hasil pengukuran terhadap hubungan-hubungan yang
menyangkut waktu bergantung pada pengertian keserempakan. Einstein berpenda-
pat ruang dan waktu jalin-menjalin secara tidak terpisahkan, yang satu tidak mung-
kin ada tanpa yang lainnya; keduanya merupakan satu kesatuan yang membentuk
ruang-waktu yang ditimbulkan oleh segenap peristiwa. Oleh karena itu, Einstein
menganggap ruang-waktu bukanlah sesuatu yang dapat memiliki eksistensi
mandiri, tidak bergantung pada benda-benda nyata dalam kenyataan fisika.
Eksistensi ruang-waktu itu ditentukan oleh materi dan energi. Gravitasi pun
dianggap Einstein bukan  sebagai gaya, akan tetapi lebih sebagai manifestasi ke-
lengkungan ruang-waktu.
Berdasarkan uraian di atas, maka pada tesis ini akan dipelajari model matema-
tis yang diajukan oleh Einstein pada 1915, yaitu Teori Relativitas Umum (TRU).
TRU adalah teori geometri yang menjelaskan gravitasi, pengaruh sebaran massa,
dan energi yang mengakibatkan perubahan ruang-waktu. Menurut Einstein, sebaran
massa dan energi membuat ruang-waktu terpilin atau melengkung. Semakin besar
sebaran massanya, maka semakin melengkung pula ruang-waktunya. Pada saat se-
baran massa dan energi terpusat pada suatu tempat, hingga mencapai batas mak-
simal dengan kelengkungan ruang-waktu yang sudah tidak dapat dipertahankan lagi
dan akhirnya �berlubang�, maka terbentuklah singularitas ruang-waktu. Peristiwa
yang terjadi pada singularitas ruang-waktu itu akan sangat aneh jika dibandingkan
dengan ruang-waktu normal. Contohnya adalah lubang hitam (black hole).
Pada tesis ini, ruang-waktu dikatakan terdapat singularitas jika kelengkungan
ruang-waktunya bernilai tak berhingga. Singularitas itu sendiri, terdiri atas dua
jenis, yaitu singularitas semu dan singularitas nyata. Singularitas semu adalah
singularitas yang dapat dihindari dengan alih-ragam koordinat. Sementara,
singularitas nyata adalah singularitas yang tidak dapat dihindari dengan alih-ragam
koordinat. Singularitas semu dapat dicari dengan menggunakan kriteria tensor
metrik. Sementara, singularitas nyata dapat dicari dengan menggunakan dua kriteria
skalar, yaitu skalar Ricci dan skalar Kretchmann. Oleh karena skalar Ricci berhu-
bungan dengan daerah yang ada sebaran massanya di seluruh ruang, sementara pada
tesis ini  adanya massa hanya di daerah singularitas saja, di selain itu ruangnya va-
kum, maka dipilihlah skalar Kretchmann. Skalar Kretchmann menunjukkan ada-
nya singularitas ruang-waktu jika bernilai tak berhingga.
1.2 Rumusan Masalah
Berdasarkan latar belakang masalah di atas, maka rumusan masalah pada tesis
ini adalah
1. Bagaimanakah watak geometri ruang-waktu di daerah sekitar singularitas un-
tuk metrik Kerr, Kerr-Newman, dan de Sitter?
2. Bagaimanakah dinamika partikel uji di daerah sekitar singularitas untuk metrik
Kerr, Kerr-Newman, dan de Sitter?
Beberapa batasan perlu dikemukakan pada penelitian ini agar pembahasannya
lebih terarah, antara lain
1. Pembahasan watak geometri ruang-waktu di sekitar daerah singularitas dipu-
satkan pada pencarian daerah singularitas dengan menggunakan kriteria skalar
2. Pembahasan dinamika partikel uji di sekitar daerah singularitas dengan meng-
abaikan gaya luar apapun.
Tujuan penelitian ini adalah
1. Mengetahui watak geometri ruang-waktu di daerah sekitar singularitas untuk
metrik Kerr, Kerr-Newman, dan de Sitter.
2. Mengetahui dinamika partikel uji di daerah sekitar singularitas untuk metrik
Kerr, Kerr-Newman, dan de Sitter.
Hasil kajian ini dapat diterapkan untuk menelaah fenomena-fenomena astrofi-
sis, khususnya terkait dengan benda-benda antap (kompak).
Singularitas ruang-waktu adalah daerah tertentu dengan kelengkungan bernilai
tak berhingga tetapi daerah itu dapat dicapai oleh kurva geodesik berhingga. Krite-
ria yang digunakan adalah kriteria skalar yang diperkenalkan oleh Kretchmann pa-
da 1915. Kemudian, skalar itu dinamakan skalar Kretchmann. Pada saat itu, point
coincidence argument dalam relativitas juga diperkenalkan oleh Kretchmann. Ide
serupa juga muncul dalam tulisan Einstein tentang relativitas umum. Bedanya, ar-
gumen Kretchmann lebih bersifat topologis. Sementara, argumen Einstein melibat-
kan adanya pengukuran fisis (Geovanelli, 2012).
Satu tahun kemudian, yaitu pada 1916 Schwarzschild menerbitkan makalah.
Pada makalah itu, persamaan medan Einstein untuk medan statis bersimetri bola
dapat diselesaikan (Schwarzschild, 1916). Selain itu, radius keruntuhan (collaps)
juga ditunjukkan keberadaannya. Pada radius itu, waktu menghilang (1 ?
2??
?
= 0),
dan ruang menjadi tak berhingga ((1 ?
2??
?
)
?1
= ?). Radius itu kini dinamakan
radius Schwarzschild (Bernstein, 2007).
Pada tahun yang sama dengan Schwarzschild, ruang-waktu empat dimensi
yang cocok dengan model kosmologi berdasarkan relativitas umum diusulkan oleh
de Sitter. Kemudian, pada 1917 de Sitter menerbitkan makalah. Pada makalah itu,
solusi persamaan medan Einstein tanpa adanya materi ditemukan. Selain itu, kon-
sep yang berbeda dari Einstein juga disampaikan. Jika alam semesta dianggap
Einstein adalah statis dan tidak berubah ukurannya, maka alam semesta dianggap
de Sitter itu mengembang secara konstan (de Sitter, 1917).
Selanjutnya, pada 1939 Einstein menerbitkan makalah yang membahas tentang
singularitas Schwarzschild yang kemudian dikenal sebagai lubang hitam. Einstein
berusaha membuktikan bahwa lubang hitam tidak mungkin eksis. Lubang hitam
adalah objek angkasa yang begitu rapat sehingga gravitasinya mencegah materi se-
kalipun cahaya untuk melepaskan diri. Ironisnya, teori yang digunakan Einstein
adalah TRU yang pada mulanya digunakan untuk membuktikan bahwa lubang hi-
tam tidak hanya memungkinkan tetapi juga tak terhindarkan bagi objek astronomis
(Bernstein, 2007).
Beberapa bulan kemudian, Oppenheimer dan Snyder menerbitkan makalah.
Pada makalah itu, TRU digunakan untuk mengetahui hal-hal yang terjadi jika bin-
tang keruntuhan dibiarkan melampaui radius Schwarzschild-nya. Ternyata, bintang
tersebut dengan cukup massa yang melampaui radius Schwarzschild-nya dapat di-
tunjukkan membentuk lubang hitam. Hasil ini diperoleh dengan mengabaikan per-
timbangan teknis, seperti rotasi bintang karena dianggap tidak mengubah sesuatu
yang esensial (Bernstein, 2007).
Pada 1963, Kerr memperoleh metrik khusus, yaitu yang sekarang disebut me-
trik Kerr. Solusi bagi persamaan medan Einstein untuk partikel tidak bermuatan
tetapi berputar diberikan oleh metrik Kerr ini. Metrik ini merupakan perumuman
bagi metrik Schwarzschild yang menggambarkan geometri ruang-waktu untuk par-
tikel tidak bermuatan sekaligus tidak berputar (Kerr, 1963).
Satu tahun kemudian, yaitu pada 1964 Newman dkk menerbitkan makalah. Pa-
da makalah itu, metrik Kerr-Newman diperkenalkan. Solusi bagi persamaan medan
Einstein untuk partikel bermuatan sekaligus berputar diberikan oleh metrik Kerr-
Newman. Metrik ini dapat terreduksi menjadi metrik Kerr untuk partikel yang ber-
muatan netral, metrik Reissner-Nordstr�m untuk partikel yang tidak berputar, dan
metrik Schwarzschild untuk partikel yang netral serta tidak berputar (Newman dkk,
1964).
Pada tahun yang sama dengan Newman, Penrose menerbitkan makalah yang
di dalamnya terdapat penjelasan tentang keruntuhan sebuah bintang dikarenakan
gravitasinya sendiri. Bintang tersebut akan menyusut sampai radius Schwarzschild
dan tidak akan mengembang lagi. Kemudian, bintang itu berubah menjadi horizon
peristiwa hingga akhirnya terbentuk singularitas dengan kerapatan massa tak ber-
hingga. Horizon peristiwa adalah batas fisik dari titik pusat lubang hitam di mana
materi dan energi tidak dapat melepaskan diri dari jerat gravitasi lubang hitam
Selanjutnya, pada 1970 Hawking dan Penrose menerbitkan makalah. Pada ma-
kalah itu, teorema baru tentang singularitas ruang-waktu diperkenalkan. Menurut
teorema itu, singularitas ruang-waktu diprediksi akan terjadi pada benda yang
mengalami keruntuhan gravitasi dengan indikator adanya permukaan terperangkap
tertutup (close trapped surface). Permukaan terperangkap tertutup adalah permu-
kaan yang luasnya akan terus berkurang sepanjang berkas cahaya yang awalnya
tegak lurus terhadap permukaan tersebut. Selain itu, ketidaklengkapan geodesik
bak-cahaya atau bak-waktu juga dapat dijadikan indikator adanya singularitas
ruang-waktu (Hawking dan Penrose, 1970).
Selanjutnya, pada 1996 dalam buku yang ditulis oleh Hawking dan Penrose
disimpulkan bahwa tidak hanya di dalam lubang hitam yang semestinya terdapat
singularitas ruang-waktu. Keadaan lain yang memungkinkan adanya singularitas
ruang-waktu adalah sesaat setelah dentuman besar (big bang). Kemudian, pengga-
bungan dari lubang hitam (big crunch) juga diduga akan menjadi daerah sing-
ularitas dengan kerapatan massa tak berhingga (Hawking dan Penrose, 1996).
Pada 1999, Henry menerbitkan makalah yang di dalamnya terdapat penjelasan
tentang perhitungan skalar Kretchmann untuk lubang hitam Kerr-Newman, yaitu
lubang hitam yang memiliki massa ?, momentum sudut persatuan massa ? dan
muatan listrik ?. Kelengkungan ruang-waktu sebagai fungsi posisi di dekat lubang
hitam Kerr-Newman diketahui dari skalar Kretchmann tersebut (Henry, 1999).
Selanjutnya, pada 2008 Visser menerbitkan makalah. Pada makalah itu, peng-
enalan singkat tentang ruang-waktu Kerr dan lubang hitam berotasi diberikan. Pem-
bahasannya difokuskan pada perwakilan koordinat yang paling banyak digunakan
dari metrik ruang-waktu. Selain itu, sifat utama dari geometri, yaitu kehadiran ho-
rizon peristiwa dan ergosphere juga dibahas. Ergosphere adalah daerah di sekitar
lubang hitam berotasi yang berada di antara horizon peristiwa dan batas statis
Pada 2012, skalar Kretchmann juga digunakan Jihad untuk mencari daerah
singularitas nyata untuk tiga jenis metrik, yaitu metrik Schwarzschild, Reissner-
Nordstorm, dan Robertson-Walker. Daerah singularitas nyata baik untuk metrik
Schwarzschild maupun Reissner-Nordstorm disimpulkan terletak di ? = 0. Jari-jari
Schwarzschild pada metrik Schwarzschild hanyalah singularitas semu, bukan
merupakan singularitas nyata. Sementara, daerah singularitas nyata untuk metrik
Robertson-Walker terletak di ? = 0 (Jihad, 2012).
Satu tahun kemudian, yaitu pada 2013 metrik  de Sitter dibahas oleh Ripken.
Metrik ini adalah solusi persamaan medan Einstein dengan konstanta kosmologi
positif dengan model alam semesta yang mengembang. Menurut Ripken, singula-
ritas semu pada metrik de Sitter berhubungan dengan horizon peristiwa. Hal ini
karena horizon peristiwa bergantung pada pemilihan pusat sistem koordinat. jika
pusat koordinat yang dipilih berbeda, maka horizon peristiwanya juga berbeda
Dalam penelitian ini, skalar Kretchmann akan dihitung dan digunakan untuk
mengetahui watak geometri ruang-waktu, khususnya keberadaan singularitas untuk
tiga jenis metrik. Metrik itu adalah Kerr, Kerr-Newman, dan de-Sitter. Kemudian,
persamaan geodesik dari ketiga jenis metrik tersebut juga akan dicari.
Penelitian ini merupakan kajian teoritis. Oleh sebab itu, akan banyak dilakukan
perhitungan matematis yang bersifat tedesius sehingga digunakanlah alat bantu pro-
gram simbolik, yaitu Maple 13.
Menemukan Ide dengan latar belakang dan tujuan yang mendasari






3. Vektor dan Medan Vektor
4. Tensor dan Medan Tensor
5. Ruang-Waktu Melengkung

7. Metrik Ruang-Waktu
8. Singularitas Ruang-Waktu

Desain Penelitian
Tahap I
Meninjau tiga jenis metrik, yaitu Kerr, Kerr-Newman, dan de Sitter
Menghitung lambang Cristoffel untuk metrik Kerr, Kerr-Newman, dan de Sitter
Menghitung tensor kelengkungan Riemann metrik Kerr, Kerr-Newman, & de Sitter
Menghitung skalar Kretchmann untuk metrik Kerr, Kerr-Newman, dan de Sitter
Tujuan I
Menghitung daerah singularitas untuk metrik Kerr, Kerr-Newman, dan de Sitter
Menghitung persamaan geodesik untuk metrik Kerr, Kerr-Newman, dan de Sitter
1.7.2 Rancangan Penelitian secara Keseluruhan
Gambar 1.2 Rancangan Penelitian secara Keseluruhan
Tahap I
Meninjau tiga jenis metrik, yaitu Kerr,
Kerr-Newman, dan de Sitter
Output I
Metrik Kerr, Kerr-Newman, dan  de
Menghitung lambang Cristoffel untuk
metrik Kerr, Kerr-Newman, dan de
Sitter
Output II
Lambang Cristoffel metrik Kerr,
Kerr-Newman, dan de Sitter
Tujuan II
Menghitung persamaan geodesik
untuk metrik Kerr, Kerr-Newman, dan
de Sitter
Tahap III
Menghitung tensor kelengkungan
Riemann untuk metrik Kerr, Kerr-
Newman, dan de Sitter
Output III
Tensor Kelengkungan Riemann
metrik Kerr, Kerr-Newman, dan de
Sitter
Tahap IV
Menghitung skalar Kretchmann untuk
metrik Kerr, Kerr-Newman, dan de
Sitter
Output IV
Skalar Kretchmann metrik Kerr,
Kerr-Newman, dan de Sitter
Tujuan I
Menghitung daerah singularitas untuk
metrik Kerr, Kerr-Newman, dan de
Output V
Daerah singularitas metrik Kerr,
Kerr-Newman, dan de Sitter
Persamaan geodesik metrik Kerr,
Kerr-Newman, dan de Sitter

