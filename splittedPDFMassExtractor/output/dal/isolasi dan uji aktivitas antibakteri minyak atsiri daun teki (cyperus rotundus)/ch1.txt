Penyakit infeksi masih merupakan jenis penyakit yang paling banyak
diderita oleh penduduk di negara berkembang, termasuk Indonesia. Infeksi terjadi
bila mikroorganisme yang masuk ke dalam tubuh menyebabkan berbagai
gangguan fisiologi normal tubuh sehingga timbul penyakit infeksi. Penyakit
infeksi mempunyai kemampuan menular pada orang lain yang sehat sehingga
populasi penderita dapat meluas. Salah satu penyebab penyakit infeksi adalah
bakteri. Bakteri merupakan mikroorganisme yang tidak dapat dilihat dengan mata
telanjang, tetapi hanya dapat dilihat dengan bantuan mikroskop (Radji, 2011).
Bagi negara berkembang timbulnya strain bakteri yang resisten terhadap
antibiotik pada penyakit infeksi merupakan masalah penting. Kekebalan bakteri
terhadap antibiotik menyebabkan angka kematian semakin meningkat.
Penanganan penyakit infeksi tersebut tidak hanya meningkatkan biaya kesehatan
karena diperlukan penanganan kombinasi antibiotik, tetapi juga menyebabkan
meningkatnya kematian terutama di negara berkembang karena antibiotik yang
diperlukan tidak tersedia. Dana yang harus dikeluarkan oleh pemerintah Indonesia
untuk mengimpor bahan baku antibiotik setiap tahunnya berkisar antara 18,6 �
122,4 milyar rupiah (Akmal, 1996). Bakteri yang dapat menyebabkan penyakit
infeksi diantaranya adalah bakteri Staphylooccus aureus (S. aureus) dan
Escherichia coli (E. coli).
Staphylococcus aureus merupakan salah satu bakteri gram positif
berbentuk bulat yang merupakan bakteri patogen bagi manusia. Hampir tiap orang
akan mengalami beberapa tipe infeksi S. aureus sepanjang hidupnya. Setiap
jaringan ataupun alat tubuh dapat terinfeksi dan menyebabkan timbulnya penyakit
dengan tanda-tanda khas yaitu peradangan, nekrosis, dan pembentukan abses.
Infeksinya dapat berupa furunkel yang ringan pada kulit sampai berupa suatu
piemia yang fatal. Umumnya bakteri S. aureus menimbulkan penyakit yang
bersifat sporadik (Anonim, 1994) sedangkan E. coli termasuk organisme enterik
2�
�
golongan heterogen gram negatif, berbentuk batang, tidak berspora yang
merupakan flora normal dalam usus. Bakteri ini dapat mengakibatkan infeksi
klinis apabila mencapai jaringan di luar intestinal normal.
Escherichia coli merupakan bakteri yang dapat memfermentasikan
karbohidrat kecuali laktosa dan penyebab paling banyak untuk infeksi saluran
kencing terutama pada wanita muda serta penyebab diare. Escherichia coli dapat
menyebabkan gastroenteritis dan meningitis pada bayi, peritonitis, infeksi luka,
kolesistitis, syok bakteremia karena masuknya organisme ke dalam darah dari
uretra, kateterisasi atau sistoskopi atau dari daerah sepsis pada abdomen atau
pelvis (Gibson, 1996).
Bakteri patogen lebih berbahaya dan menyebabkan infeksi baik secara
sporadik maupun endemik, antara lain S. aureus, E. coli dan P. aeruginosa.
Diketahui jenis bakteri S. aureus dan E. coli telah menjadi kebal terhadap
antibiotika, seperti metisilin dan eritromisin (Kumala et al., 2007). Umumnya
masyarakat dalam mengobati penyakit infeksi sering menggunakan obat antibiotik
seperti tetrasiklin atau ampisilin atau antibiotika jenis lainnya yang dengan mudah
dapat diperoleh. Pemakaian antibiotika secara berlebihan dan kurang terarah dapat
mengakibatkan terjadinya resistensi, dengan timbulnya resistensi pada beberapa
antibiotik tertentu, dapat menyebabkan kegagalan dalam pengobatan berbagai
jenis penyakit infeksi, sehingga untuk mengatasinya diperlukan pencarian bahan
alami sebagai alternatif pengobatan. Bahan alami yang dapat dimanfaatkan
sebagai antibiotik alami salah satunya berasal dari spesies flora.
Indonesia sangat kaya dengan berbagai spesies flora, dari 40.000 jenis
flora yang tumbuh di dunia, 30.000 diantaranya tumbuh di Indonesia. Kegiatan
budidaya flora telah mencapai 26% dari total flora yang tumbuh di Indonesia.
Jenis flora yang sudah dibudidayakan � 940 jenis. Flora tersebut digunakan
sebagai tanaman obat tradisional  (Syukur dan Hernani, 2002). Tumbuhan obat
terkesan sebagai tanaman liar sehingga keberadaanya sering dianggap
mengganggu keindahan atau mengganggu kehidupan tumbuhan lainnya.
Pemanfaatan tanaman obat tradisional pada saat ini terus meningkat. Hal ini
disebabkan oleh adanya anggapan dari sebagian besar masyarakat bahwa efek
3�
�
samping yang ditimbulkan oleh tanaman obat tersebut tidak berbahaya, sehingga
timbul pemikiran dari masyarakat untuk kembali ke cara alamiah dengan
memanfaatkan tanaman obat sebagai salah satu alternatif untuk mencegah dan
mengobati berbagai macam penyakit.
Indonesia memiliki berbagai spesies tanaman yang sebenarnya dapat
memberikan banyak manfaat, namun belum dibudidayakan secara khusus. Salah
satunya adalah teki (Cyperus rotundus). Selain belum dibudidayakan secara
khusus, teki juga sangat mudah didapatkan bahkan hampir tidak memerlukan
biaya sama sekali. Teki merupakan herba menahun yang tumbuh liar dan kurang
mendapat perhatian, padahal bagian tumbuhan ini terutama umbinya dapat
digunakan sebagai analgetik (Sudarsono et al., 1996).
Umbi teki mengandung komponen-komponen kimia antara lain minyak
atsiri, alkaloid, flavonoid, polifenol, resin, amilum tanin, triterpen, d-glukosa, d-
fruktosa dan gula tak mereduksi. Kandungan minyak atsiri pada umbi teki sebesar
0,45 � 1%, berat jenis 0,989 � 0,991 dan indeks bias 1,513 (Atal dan Kapur,
1982). Minyak atsiri teki yang berasal dari Cina mengandung siperen, paskolenon,
sedangkan yang berasal dari Jepang mengandung siperol, siperen, ?-siperone,
siperotundon dan siperulon, di samping itu ditemukan pula alkaloid, flavonoid dan
triterpen. Minyak atsiri yang terkandung dalam umbi teki dilaporkan memiliki
potensi sebagai antibiotik terhadap bakteri S. aureus (Hembing et al., 1993).
Kandungan kimia dalam umbi teki sebagian besar memberikan efek
farmakologi, akan tetapi komponen aktif utamanya adalah kelompok senyawa
seskuiterpen yang terdapat dalam minyak atsiri.  Minyak atsiri merupakan
senyawa yang pada umumnya berwujud cairan, yang diperoleh dari bagian
tanaman, akar, kulit, batang, daun, buah, biji, maupun dari bunga dengan cara
penyulingan. Meskipun kenyataan untuk memperoleh minyak atsiri dapat
menggunakan cara lain seperti ekstraksi menggunakan pelarut organik atau
dengan cara dipres (Sastrohamidjojo, 2004). Beberapa minyak atsiri yang
digunakan sebagai pewangi yaitu minyak atsiri dari bunga kenanga, bunga mawar,
jeruk manis, jeruk nipis dan lemon, selain itu minyak atsiri mampu bertindak
sebagai bahan terapi (aromaterapi).
4�
�
Dalam bidang kesehatan, minyak atsiri merupakan minyak yang mudah
menguap yang akhir-akhir ini menarik perhatian dunia, hal ini disebabkan minyak
atsiri dari beberapa tanaman bersifat aktif biologis sebagai antibakteri. Minyak
atsiri dapat menghambat beberapa jenis bakteri merugikan seperti E. coli,
Salmonella sp, S. aureus, Klebsiella dan Pasteurella (Agusta, 2000).
Penelitian yang telah dilakukan yaitu umbi teki memiliki kandungan kimia
berupa minyak atsiri yang memiliki aktivitas sebagai antibakteri, tetapi belum ada
penelitian mengenai daun teki. Berdasarkan latar belakang tersebut, maka muncul
beberapa permasalahan yaitu :
1. Apakah minyak atsiri daun teki dapat diisolasi dengan metode destilasi air dan
apa saja komponen kimia penyusun minyak atsiri tersebut?
2. Apakah minyak atsiri daun teki dapat dipisahkan dengan metode KLTP?
3. Bagaimana aktivitas antibakteri minyak atsiri daun teki dan fraksi-fraksinya
terhadap bakteri S. aureus dan E. coli?
1.2 Tujuan Penelitian
Tujuan dari penelitian ini adalah :
1. Mengisolasi minyak atsiri daun teki (Cyperus rotundus) dengan metode
destilasi air dan mengidentifikasi komponen senyawa yang terkandung
didalamnya menggunakan GC-MS.
2. Memperoleh fraksi-fraksi minyak atsiri daun teki (Cyperus rotundus) melalui
pemisahan kimia dengan metode Kromatografi Lapis Tipis Preparatif (KLTP).
3. Mengetahui aktivitas minyak atsiri daun teki dan fraksi-fraksinya sebagai
antibakteri terhadap bakteri S. aureus dan E. coli.
Hasil penelitian ini diharapkan dapat memberikan informasi tentang bahan
alam Indonesia yang dapat digunakan sebagai antibakteri terhadap bakteri S.
aureus dan E. coli terutama minyak atsiri. Selain itu diharapkan diperoleh data
jenis senyawa yang berperan sebagai antibakteri sebagai dasar pengendalian yang
tepat sasaran, efektif, dan efisien serta dapat memberikan kontribusi dalam
pengembangan ilmu pengetahuan dan teknologi dalam bidang kimia dan
aplikasinya.

