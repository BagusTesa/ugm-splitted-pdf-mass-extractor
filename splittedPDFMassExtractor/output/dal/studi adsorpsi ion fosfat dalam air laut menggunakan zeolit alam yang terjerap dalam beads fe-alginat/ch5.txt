Berdasarkan hasil penelitian adsorpsi ion fosfat dalam air laut dapat
disimpulkan bahwa :
1. Pembuatan beads Fe-Alginat dapat dilakukan dengan metode ekstrusi
(meneteskan) larutan Na-Alginat kedalam larutan kation multivalent (Fe3+
2. Beads Zeolit-Fe-Alginat dapat dibentuk dengan mencampurkan zeolit
kedalam larutan alginate yang akan diteteskan ke dalam larutan FeCl3 0,2
M. Beads ini dapat digunakan sebagai adsorben ion fosfat dalam air laut.
3. Kapasitas adsorpsi ion fosfat dalam air laut mengalami penurunan pada pH
10 (basa) karena permukaan adsorben dipenuhi oleh anion OH- sehingga
terjadi tolakan dengan ion fosfat.
4. Proses adsorpsi dipengaruhi oleh perpindahan massa dari larutan ke
adsorben dan sesuai dengan model kinetika pseudo order dua dengan
konstanta laju adsorpsi sebesar 0.029 (g/mg.min) untuk beads Zeolit-Fe-
Alginat dan 0,040 (g/mg.min) untuk beads Fe-Alginat.
5. Jenis adsorpsi yang terjadi adalah fisisorpsi dengan nilai ED sebesar 0,289
kJ/mol untuk beads Zeolit-Fe-Alginat dan 0,408 kJ/mol untuk beads Fe-
1. Perlu dilakukan studi lebih lanjut dalam pembuatan beads agar lebih kuat
dan tidak mudah pecah seperti beads Fe-Alginat
2. Perlu dilakukan studi tentang proses pengeringan beads sebelum
digunakan dalam proses adsorpsi
3. Perlu dilakukan studi lanjut untuk mempelajari adsorpsi ion fosfat dalam
air laut pada konsentrasi rendah

