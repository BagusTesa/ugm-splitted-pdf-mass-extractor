by
Synthesis of beads of natural zeolite entrapped in Fe-Alginate, and its
adsorption characteristic for PO4
3- has been conducted. Beads of Zeolite-Fe-
Alginate (Z-Fe-A) was made by extrusion method in multivalent cation solution
containing Fe3+ of 0.2 M. As comparison, beads Fe-Alginate was also synthesized.
The beads were characterized by FTIR method. Various parameters such as
equilibrium contact time, pH, initial phosphate concentration, and adsorbent dose,
were studied in adsorption experiment.
It was found that the beads of alginate gel and natural zeolite (Z-Fe-A) was
successfully made and was capable of removing phosphate from seawater. The
optimum adsorption was investigated at pH 7 for beads of Z-Fe-A and pH 6 for
beads of Fe-A. Adsorption intensity was analyzed by Freundlich model, and it
showed that Z-Fe-A beads has an intensity of 1.150 with Freundlich constant (Kf)
of 0.043 L/g and Fe-A beads has an intensity of  1.069 with Kf of 0.070 L/g.
Beads of Zeolite-Fe-Alginate and Fe-Alginate has an energy of adsorption of
0.298 kJ/mol and 0.408 kJ/mol, repectively, which belongs to physisorption. The
sorption kinetics of phosphate follows pseudo-second order model, having rate
constant of 0.029 g.mg-1min-1 for Z-Fe-A beads and 0.040 g.mg-1min-1 for Fe-A
beads. Adsorption process was controlled by mass transfer from bulk to the
surface of adsorbent.

