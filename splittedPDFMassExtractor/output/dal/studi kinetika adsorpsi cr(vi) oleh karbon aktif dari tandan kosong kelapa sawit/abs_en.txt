Activated carbon from palm empty fruit bunches has been successfully
synthesized through thermal (physical) activation at 800
o
C for 15 minutes and
washing by HCl processes. The research results showed that iodine adsorption
capacity was 448.91 mg/g with a total water content of 1.17 %, volatile matter
content 12.01 %, ash content 5.50 %, carbon content 81.32 % and the carboxylic,
hydroxyl, carbonyl and S=O groups were present on the surface of adsorbent. The
optimum pH for adsorption Cr(VI) by the activated carbon was at pH= 2.
Application of reversible adsorption-desorption kinetics gave the value of k1; k-1
and  K were 5.75 x 10
-4
-1
.minutes
-1
, 2.20 x 10
-3
minutes
-1
and 0.26;
respectively, while 21.74 mg/g of adsorption capacity could be determined from
Langmuir isotherm model.
Keywords : adsorption, Cr(VI), activated carbon, kinetics

