Krom (VI) merupakan krom yang memiliki tingkat toksisitas yang paling
tinggi dibandingkan bentuk krom lain. Keracunan krom mengakibatkan kerusakan
hati dan ginjal, kerusakan saluran pernapasan, gangguan saluran pencernaan,
kerusakan embrio, dan kanker paru-paru, bahkan kontak langsung krom
heksavalen dengan kulit dapat menyebabkan reaksi alergi kulit yang umumnya
ditandai dengan munculnya warna kekeruhan pada kulit dan bisul-bisul (EPA,
1998). Sumber pencemar krom dapat berasal dari limbah industri pelapisan
logam, penghambat korosi besi, penyamakan kulit, cat, tekstil, dan bahan
pengawet kayu (Kusnoputranto, 1996).
Sungai Code merupakan salah satu sungai yang mengalir melintasi kota
Yogyakarta. Sungai ini sering menjadi tempat buangan limbah cair industri, salah
satunya adalah industri penyamakan kulit. Bila industri ini tidak mengolah
limbahnya dengan baik dapat menimbulkan pencemaran krom di Sungai Code.
Penggunaan lahan yang bervariasi menyebabkan Sungai Code terancam berbagai
sumber pencemar, antara lain dari kegiatan industri, pertanian, rumah makan,
hotel, dan rumah sakit. Limbah hasil kegiatan sumber pencemar tersebut
langsung dibuang ke Sungai Code sehingga dapat mengakibatkan penurunan
kualitas air sungai (BLH, 2012).
Berdasarkan data BLH DIY mengenai evaluasi kualitas sungai Code
hingga tahun 2012, secara umum hampir seluruhnya masuk ke dalam klasifikasi
air kelas IV, yakni hanya layak dipakai untuk tujuan irigasi, maka diperkirakan
perairan Sungai Code tidak memenuhi syarat baku mutu air. Air merupakan
sistem multi komponen yang terdiri dari berbagai unsur yang ada dalam berbagai
bentuk fisiko kimia yang berbeda ukuran dan muatannya. Selain itu unsur-unsur
tersebut juga terdapat dalam berbagai bentuk seperti ion bebas, molekul,
kompleks atau teradsorpsi dalam sedimen organik dan sedimen anorganik.
Banyaknya unsur-unsur yang terdapat dalam air, menyebabkan perhitungan
bentuk spesies logam tidak dapat dhitung secara manual, oleh sebab itu  para ahli
kimia memanfaatkan teknologi komputer untuk membuat berbagai program
pemodelan lingkungan.
Pemodelan merupakan sebuah proses yang mencoba melakukan
pendekatan dan prakiraan berdasarkan kondisi nyata di lapangan yang sangat
kompleks. Hasil dari sebuah pemodelan yang utama adalah prediksi keadaan pada
kondisi dan waktu yang berbeda. Pemodelan yang tepat dapat menggambarkan
perilaku dari parameter kualitas air dan proses-proses yang terjadi dalam air
sungai. Visual MINTEQ merupakan program komputer yang didasari oleh
kesetimbangan kimia yang digunakan untuk menghitung spesiasi, kelarutan, dan
keseimbangan antara fase padat dan terlarut dalam suatu larutan (Gustaffson,
2004). Selain itu, program untuk pemodelan lingkungan yang digunakan pada
penelitian ini adalah WASP7,5 (Water Quality Analysis Simulation Program).
Software WASP7,5 digunakan  untuk menafsirkan dan memprediksi kualitas air
yang diakibatkan fenomena alam maupun ulah manusia. WASP7,5 adalah
program kompartemen-pemodelan dinamis untuk sistem perairan yang mengamati
hubungan kesetimbangan logam terlarut dan sedimen (Ambrose,dkk.,1993).
Penelitian ini bertujuan untuk:
1. Menentukan spesies Cr(VI) dalam sungai Code.
2. Mempelajari pengaruh dampak aktivitas industri, kekasaran dasar sungai, debit
sungai Code pada musim kemarau dan penghujan terhadap konsentrasi Cr(VI)
di Sungai Code menggunakan software WASP7,5.
3. Menentukan sebaran konsentrasi Cr(VI) di air dan sedimen dengan software
Penelitian ini diharapkan dapat memberikan informasi tentang penerapan
aplikasi praktis model Visual MINTEQ dan WASP7,5 pada Sungai Code dalam
mempelajari pengaruh pH terhadap pembentukan spesies Cr(VI) menggunakan
Visual MINTEQ dan pengaruh dari aktivitas industri, kekasaran dasar sungai,
debit sungai, serta nilai Kd terhadap konsentrasi Cr(VI) di Sungai Code
menggunakan model WASP7,5. Hasil prediksi model Visual MINTEQ dan
WASP7,5 diharapkan dapat digunakan sebagai landasan kebijakan strategis dalam
pengembangan lingkungan daerah sekitar Sungai Code dan pelestarian Sungai
Code dengan mempertimbangkan aspek lingkungan, sehingga kualitas air Sungai
Code tetap dapat dimanfaatkan oleh manusia dan ekologi di sekitarnya
sebagaimana mestinya.

