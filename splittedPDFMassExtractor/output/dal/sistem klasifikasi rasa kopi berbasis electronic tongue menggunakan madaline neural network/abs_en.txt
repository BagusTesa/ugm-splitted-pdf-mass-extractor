This research is motivated by the lack of the nowadays taste sensor
development and this study aims ti implement eight computer-based taste sensor with
ammonium chloride (TOMA), Dodecylamine(DDC), DA:OA 5:5, DA:DOP 5:5, and
DDC:TOMA 5:5 membranes with semi auto sampler and it could show the
measuring result and store the data from eight sensors as one. System implemented
on few instant coffees, and patterned characterization on the coffees with physical
detection comparation.
The membrane character testing was did everyday with some instant coffee
samples and then the pattern characterization be done. Tool that used as ADC was
PhidgetInterFaceKit 8/8/8 that was an electrometer for this research. And uses
program based on Microsoft Visual Basic 2010 as the interface so it can be interacted
with the tool. And used the toolbox of Matlab R2009a program for madaline neural
network utilization.
The results showed a pattern characterized using this system can be identified
using the madaline neural network. Data results from this system can be stored in the
form of excel.
Keyword : madaline neural network, membrane, taste sensor, interface, coffee

