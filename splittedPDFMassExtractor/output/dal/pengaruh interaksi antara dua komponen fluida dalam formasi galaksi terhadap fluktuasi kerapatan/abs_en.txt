Dynamics of the universe from radiation-dominated to matter dominated has been
discussed. Interaction between two fluid components which contain plasma and radiation for
galaxy formation and its influence to unstable density fluctuations in the universe has been
investigated. The method for investigating this interaction is dispersion-relation method.
Keywords : radiation-dominated, matter-dominated, plasma, radiation, density

