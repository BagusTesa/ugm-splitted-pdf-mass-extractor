Analisis Bayesian untuk Regresi Binari Kuantil Terpenalti dengan
Regresi binari kuantil terpenalti merupakan perluasan dari regresi kuantildi
mana variabel dependennya berskala biner/dikotomus.Regresi ini digunakan untuk
menganalisis data yang mengandung pencilan dan heterokedastisitas.
Regresi binari kuantil denganpenalti LASSO (Least Absolute Shrinkage and
Selection Operator) akan menghasilkan estimasi parameter yang lebih tahan (robust)
terhadap data yang mengandung pe ncilan, dapat mengecilkan galat, serta dapat
mengidentifikasikan variabel-variabel prediktor yang penting untuk variabel respon.
Regresi binari kuantil dengan penalti LASSO dapat diestimasi dengan
menggunakanmetode Bayesian.Metode Bayesian adalah metode analisis yang
berdasarkanpada informasi yang berasal dari sampel dan informasi prior. Gabungan
informasi ini disebut posterior. Untuk mencari distribusi posterior seringkali
menghasilkan perhitungan yang tidak dapat diselesaikan secara analiti k sehingga
digunakan pendekatan Gibbs sampling. Estimasi parameter dari model adalah mean
dari distribusi posterior yang diperoleh dari proses Gibbs sampling tersebut. Dalam
skripsi ini dibahas regresi binari kuantil terpenalti menggunakan Asymmetric Laplace
Distribution dari sudut pandang Bayesian.
Dalam skripsi ini, regresi binari kuantil Bayesian terpenalti diaplikasikan
untuk menganalisis faktor-faktor yang mempengaruhi kualitas air sungai di
Kabupaten Bantul. Hasil estimasi regresi binari kuantil Bayesian terpenalti akan
dibandingkan dengan regresi logistik dan regresi probit. Dengan berdasarkan akurasi
model dari ke tiga metode yang digunakan, diperoleh kesimpulan bahwa estimasi
regresi binari kuantilBayesian terpenalti lebih baik dari estimasi regresi logistikdan
regresi probit karena menghasilkan model yang lebih presisi.
Kata Kunci: Regresi Binari Kuantil, Penalti Lasso, Bayesian, Gibbs sampling,

