1.1 Latar Belakang dan Permasalahan
Sebuah pertunjukkan wayang memiliki beberapa elemen utama
yang mempengaruhi performanya, diantaranya adalah seni drama (sanggit),
musik (vocal-instrumen), rupa, gerak (tari), dan seni sastra. Salah satu elemen
terpenting dalam pertunjukkan wayang adalah seni gerak wayang. Dalam
dunia wayang seni gerak ini disebut dengan istilah sabetan. Sabetan (gerak
wayang) berasal dari kata sabet, yang artinya pengembat, sebat; disabet
berarti diembat, disekat, dibingkah; disabeti berarti dibelasah; nyabet artinya
menjatuhkan kartu, melakukan wayang kulit; dan sabet dalam krama inggil
berarti pedang (Prawiroatmojo, 1981). Pengertian sabetan, nyabet yang diacu
yakni melakukan tarian wayang kulit, menggerakkan, menjalankan,
memainkan boneka wayang. Gerak wayang menyangkut bagaimana tokoh
berbicara, bersikap, dan bertindak dalam hubungannnya dengan tokoh yang
lainnya. Dalam suatu gerakan wayang terjadi perpindahan atau perubahan
pada tubuh atau sebagian kecil anggota tubuh boneka-boneka wayang.
Gerakan pada wayang meliputi gerakan menyembah, berjalan,
berlari, menari, terbang, dan perang. Gerak wayang tersebut berprinsip pada
status sosial, tua-muda (usia), klasifikasi, dan wanda tokoh-tokoh wayang.
Dalam seni gerak wayang memperhatikan pula prinsip wiraga (benar dan
tepatnya action dalam gerak), wirasa (benar dan tepatnya penghayatan dalam
gerak), dan wirama (benar dan tepatnya irama dalam gerak). Sebagai contoh
adalah dalam karakter Anoman. Pada cerita Ramayana, Anoman berstatus
sebagai seorang ksatria yang mengabdi pada Prabu Ramawijaya sehingga
pada gerakan menyembah, Anoman digerakkan secara halus dan secara tegas
sehingga memeperlihatkan sosok yang penuh dengan kesetiaan dan
keberanian. Anoman juga merupakan jenis wanara (kera). Maka dari itu
gerakan yang dilakukan Anoman pada saat perang merupakan gerakan yang
cepat dan tangkas. Gerakan-gerakan yang dihasilkan pada saat tersebut
merupakan gerakan yang kompleks, seperti jungkir balik, berlari menghindar,
menggigit. Selain itu karena Anoman merupakan keturunan jawata, maka
Anoman memiliki gerakan terbang. Dikarenakan Anoman ini memiliki
gerakan-gerakan yang paling lengkap dalam tokoh seperti pada penjelasan
sebelumnya, maka usulan penelitian S1 ini dititikberatkan pada gerakan
karakter wayang  Anoman.

Bagaimana cara membuat purwarupa sebuah wayang Anoman
yang melakukan gerakan sembah saat dia menjadi ksatria dan menjadi
seorang resi  secara otomatis sehingga menghasilkan gerakan yang sesuai
ataupun mendekati dengan gerakan karakter asli dari tokoh wayang Anoman
yang asli dalam pertunjukkan wayang.
1.3.      Tujuan dan Manfaat Penelitian
a. Dapat digunakan sebagai alat peraga untuk mendemonstrasikan
pertunjukan wayang kulit dalam skala kecil
b. Memberikan wacana bahwa wayang juga dapat berkembang selaras
dengan kemajuan teknologi
c. Untuk mengimplementasikan gerakan sembah tokoh wayang Anoman
dengan memanfaatkan teknologi modern yaitu digerakkan dengan
teknologi mikrokontroler

Dari rumusan masalah yang telah ditulis pada bagian latar
belakang dan permasalahan, maka usulan penelitian S1 ini dibatasi hanya
dengan membuat sebuah robot wayang Anoman dikontrol menggunakan
mikrokontroler ATMega16. Mikrokontroler ATMega16 ini akan mengontrol
gerakan motor-motor listrik yang sebagai implementasi gerakan wayang.
Motor-motor listrik inilah yang digunakan sebagai acuan dari derajat
kebebasan gerak wayang. Terdapat 5 macam gerakan wayang tersebut,
meliputi gerakan rotasi yaitu kedua lengan, gerakan sambungan antara tangan
dan lengan, dan gerakan berputar vertikal. Oleh karena terdapat beberapa
motor listrik yang memiliki sudut putar dan kecepatan putar yang terbatas,
maka gerakan-gerakan wayang yang dilakukan juga terbatas sehingga hanya
mencakup gerakan sembah wayang Anoman
.

Metodologi penelitian yang dilakukan dalam pembuatan skripsi
yang akan dilakukan meliputi:

Studi pustaka dilakukan untuk mempelajari literature yang
digunakan dalam perancangan sistem. Literatur yang dipelajari meliputi
mikrokontroler ATMEGA 16, Bahasa C yang digunakanebagai program
tertanam dalam mikrokontroler ATMEGA 16, beberapa jenis dan sifat
motor listrik. Selain itu diperlikan juga referensi gerakan dan desain
wayang agar sistem bekerja sesuai dengan tujuan.
2. Perancangan sistem.
Perancangan sistem ini meliputi rancangan sistem pada perangkat
keras atau hardware yaitu sistem mekanik gerakan wayang itu sendiri
serta rancangan sistem pada perangkat lunak atau software yang
merupakanprogram tertanam yang nantinya mengatur korelasi gerak
motor-motor listrik sehingga menghasilkan gerakan terpadu.
3. Konsultasi dan diskusi dengan orang yang ahli dalam pemrograman serta
gerak mekanik sehingga dapat ditemukan solusi dalam proses
pengerjaannya.
4. Pengujian sistem.
Pengujian sistem yang dilakukan mulai dari masing-masing
komponen yang ada. Kemudian dilanjutkan untuk sistem perbagian sistem
dan yang terakhir dilanjutkan dengan pengujian sistem secara
keseluruhan.

Penulisan usulan penelitian ini meliputi :
Pada bab ini berisi tentang latar belakang, rumusan masalah, tujuan dan
manfaat penelitian, batasan masalah, metodologi penelitian, dan sistematika
penulisan skripsi ini.
Dalam bab ini memuat beberapa penelitian yang menjadi landasan dan
acuan dalam perancangan alat serta perbandingan terhadap penelitian-
penelitian sebelumnya
Bab ini memuat beberapa teori yang melandasi beberapa pengertian dan
komponen-komponen yang sebagai acuan utama tugas akhir ini.
Berisi tentang analisis sistem yang dilakukan beserta perancangan pada
bagian-bagian sistem alat yang digunakan.
Bab ini menjelaskan tentang spesifikasi sistem serta beberapa poin dari
karakteristik pada robot wayang yang telah dibuat.
Berupa hasil akhir dari pengujian sistem yang kemudian dibahas secara
dengan penjelasan yang ringkas.
Bab ini berisi tentang kesimpulan dari tugas akhir ini dan beberapa saran
untuk penelitian selanjutnya.

