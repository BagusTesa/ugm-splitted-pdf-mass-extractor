Curcuma mangga Val. RHIZOMES MEASURED BY 1,1-DIPHENYL-
The study of antioxidant activities of the essential oil from Curcuma
mangga Val. rhizomes have been done. This research aims to find out the effect of
preparation method and harvest time on antioxidant activities of the essential oil
of C.mangga rhizomes determined by spectrophotometric method. Variation of
the sample solvents and comparison of antioxidant activities of essential oil and
synthetic antioxidant were also studied.
The study consisted of three stages which are sample preparation, isolation
of essential oil, and antioxidant activity assay. Sample preparation of C.mangga
rhizomes can be divided into wet and wind�dried samples with harvest time in
November and February. Furthermore, the isolations of essential oil were carried
out by water and steam distillation method for 6 hours. Identification of chemical
constituents of essential oil of C.mangga were done by Gas Chromatography-
Mass Spectrometry (GC-MS). Antioxidant activity assays on essential oil and
butyl hidroxy toluene (BHT) were performed with DPPH radical scavenging and
inhibition of TBARS formation methods with solvent variations. Significant tests
by ANOVA method was performed on antioxidant activities of essential oil and
BHT by DPPH and TBARS methods (P < 0,05).
The percentage yield of essential oil from wind-dried samples are more
than that of wet samples either harvest time in November or February.
The GC-MS analysis of essential oil components resulted in the identification of
25 compounds (November) and 6 compounds (February). The major constituent
components of essential oil of C.mangga were ?-myrcene and ?-pinene. The
essential oil of C.mangga showed strong antioxidant activities againts DPPH
radical and TBARS formation. The antioxidant activities of essential oil and BHT
with DPPH and TBARS methods didn�t show significant differences.
Key word : Curcuma mangga, essential oil, antioxidants, GC-MS, DPPH, TBARS

