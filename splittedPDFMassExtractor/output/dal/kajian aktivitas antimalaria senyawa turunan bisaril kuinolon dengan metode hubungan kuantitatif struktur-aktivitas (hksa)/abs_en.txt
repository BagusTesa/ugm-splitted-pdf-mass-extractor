by
Quantitative Structure-Activity Relationship (QSAR) analysis of bisaryl
quinolone derivatives as antimalarial agents has been conducted using electronic
and molecular descriptors. The descriptors were obtained from semi-empirical
AM1 calculation. A multiple linear regression and artificial neural network
(ANN) methods were used for QSAR models building on antimalarial activity and
inhibition of PfNDH2 (Plasmodium falciparum type II NADH: quinone
oxidoreductase) activity studies, respectively.
The  best  equation of QSAR model on antimalarial activity study is
pIC50= 2.064 � 0.003 (V) � 59.693 (qN1) � 11.338 (qC5)  � 2.068 (qC6) + 2.036
(qC7) + 49.063 (qO11) with statistical parameters r2 = 0.751 and PRESS = 2.164.
QSAR models of PfNDH2 inhibition study was resulted the best equation with
polarizability and atomic net charges of N1 and C22 descriptors (r2 = 0.834). The
best final equation was applied to design and predict more potent compounds as
antimalarial agent. A new compound, 6,7-difluoro-2-(3-(4-(trifluoromethoxy)
phenoxy)phenyl)quinoline-4(1H)-one with high antimalarial activity and
inhibition of PfNDH2 activity has been proposed. The compound showed
antimalarial activity against P. falciparum of 3.387 nM and inhibitory IC50 against
PfNDH2 of 7.588 nM.
C10
C9
N1
C2
C3
C4
C6
C7
Y
C5
C12
C17
C16
C15
C14
Keyword : bisaryl quinolone, antimalarial, QSAR, activity, PfNDH2

