Sistem verifikasi maupun identifikasi semakin banyak digunakan untuk
berbagai aplikasi mengacu kebutuhan akan keamanan. Akan tetapi sampai saat
ini, metode konvensional yang masih banyak digunakan secara luas, seperti pin
dan password. Seiring perkembangan teknologi tentunya sistem baru terus
dikembangkan, terutama sistem yang mengacu pada konsep biometrik.
Konsep biometrik mengacu pada karakteristik alami manusia. Identifikasi
dengan konsep biometrik ini, salah satunya dengan menggunakan karakteristik
manusia yang bersifat khas untuk membedakan antara orang yang satu dengan
lainnya adalah sidik jari (fingerprint). Sidik jari memiliki pola-pola yang khas
yang disebut sebagai minutiae atau titik minusi. Minutiae pada setiap orang
berbeda-beda, entah itu jumlah maupun letaknya, sehingga dapat dijadikan
pembeda antara orang yang satu dengan yang lainnya.
Sistem dengan konsep biometrik (terutama pada citra sidik jari) masih
dijumpai beberapa kelemahan, yaitu masalah deformasi citra dan lokalisasi titik
minusi. Deformasi citra, yang mana sistem tidak cukup mampu mengkompensasi
masalah informasi dan kesamaan citra, karena metode atau algoritma pembatas
yang digunakan tidak dapat mentoleransi kesamaan set minutiae dari citra sidik
jari yang dibandingkan. Sedangkan masalah lokalisasi titik minusi, yang mana
sistem kurang mampu menentukan titik-titik minusi dengan tepat.

Deformasi citra dan lokalisasi titik minusi pada sidik jari sering kali
menjadi masalah dalam mengidentifikasi suatu sidik jari. Dikarenakan dua hal ini
yang menentukan keakuratan dari sistem. Adanya kesalahan sedikit saja dari dua
hal ini akan berpengaruh besar terhadap sistem dalam mengenali. Berdasarkan hal
tersebut, maka dibutuhkan sistem yang mampu mengatasi dua masalah tersebut,
sehingga dalam proses identifikasinya bisa berjalan optimal dan didapatkan
tingkat keakuratan yang tinggi.

Tujuan dari penelitian ini adalah membuat sebuah sistem yang mampu
untuk menganalisis, mengambil informasi, dan mengidentifikasi atau mengenali
sidik jari, dengan tingkat keakuratan yang lebih baik. Selain itu, juga bertujuan
untuk meneliti tingkat pengenalan (keakuratan) dari algoritma yang digunakan
dalam mengenali citra sidik jari yang diujikan.

Manfaat dari penelitian ini adalah:
1. Mampu mengatasi masalah deformasi citra dan lokalisasi titik minusi sidik
jari, sehingga didapatkan tingkat presisi dan pengenalan yang tinggi dari
perbandingan dua citra sidik jari.
2. Mengetahui tingkat keberhasilan sistem dalam memroses citra dan
mengenali citra sidik jari dengan metode yang digunakan.

Adapun batasan masalah pada penelitian ini adalah sebagai berikut :
1. Citra input yang digunakan berformat BMP dan berdimensi 144x144
piksel.
2. Fitur yang diekstraksi dari citra input adalah titik-titik minutiae dari
citra sidik jari yang berupa titik-titik akhir (termination) dan
percabangan (bifurcation).
3. Beberapa tahapan seperti perbaikan kualitas citra (image pre-
processing), menentukan titik minusi (intermediete processing), dan
orientasi minutiae (post-processing) adalah beberapa tahapan yang
mendukung dalam proses pengenalan dan pencocokan citra sidik jari.
4. Threshold yang dipakai dalam penentuan minutiae palsu adalah jarak
Euclidean (Euclidean distance).
5. Parameter untuk pencocokan adalah jumlah minutiae terdeteksi antara
kedua sampel sidik jari yang dibandingkan dan transformasi (rotasi
dan translasi) kedua titik minusi yang dibandingkan terhadap titik
minusi referensi, yang nanti dalam implementasinya menerapkan
metode penyelarasan (Aligment Stage) dan pencocokan (Matching
6. Metode penyelarasan dan pencocokan mengacu pada algoritma
pembatas adaptif.
7. Dalam pengujian diambil 230 buah sampel sidik jari yang berbeda
dari 10 responden. Dari 230 buah, 80 buah digunakan sebagai citra
pelatihan (basis data), sedangkan sisanya 150 buah sebagai data uji.

Metode yang digunakan dalam penelitian tugas akhir ini adalah sebagai
berikut:
1. Studi pustaka dan referensi
Tahap pengumpulan informasi yang diperlukan untuk pengerjaan tugas
akhir sekaligus mempelajarinya, yaitu pengumpulan literatur, diskusi,
serta pemahaman topik tugas akhir, di antaranya tentang:
a. Pengamatan penelitian terdahulu yang berkaitan dengan sistem yang
akan dibuat sekaligus menganalisanya.
b. Teori sidik jari, biometrik, minutiae, euclidean distance, algoritma
pencocokan, dan beberapa teori lain yang berhubungan dengannya.
c. Berbagai teknik perbaikan kualitas citra dan teknik pengolahan citra
digital lainnya, yang akan digunakan dalam proses menentukan
informasi dari citra sidik jari.
d. Teori tentang garis, representasi garis, dan optimasi garis.
e. Algoritma pembatas adaptif dan beberapa variasinya.
2. Studi konsultasi, konsultasi kepada dosen pembimbing atau dosen lain
yang ahli pemrosesan citra di jurusan Ilmu Komputer dan Elektronika
Instrumentasi.
3. Perancangan sistem, berupa penyusunan langkah-langkah dan metode
penelitian, perancangan proses pengambilan citra dan program.
4. Implementasi dan pembuatan system
Pengimplementasian sistem identifikasi sidik jari sesuai rancangan
sistem dengan menggunakan toolbox pemrosesan citra dan algoritma
dalam fungsi-fungsi pada Matlab.
5. Pembahasan dan analisis, terkait dengan hasil yang diperoleh dari
pengolahan data.
6. Penyusunan laporan tugas akhir
Pada tahap ini disusun buku sebagai dokumentasi dari pelaksanaan
tugas akhir, yang mencakup seluruh konsep, teori, implementasi, serta
hasil yang telah dikerjakan.

Laporan penelitian tugas akhir ini disusun dengan sistematika sebagai
berikut:
Meliputi latar belakang dan permasalahan, tujuan penelitian, batasan
masalah, metodologi penelitian, dan sistematika penulisan.
Memuat tentang informasi-informasi tentang hasil penelitian yang telah
dilakukan terkait dengan identifikasi sidik jari, pengolahan citra, dan
metode-metodenya.
Memuat tentang konsep dan landasan teori yang menunjang dalam
penyusunan sistem dan pembahasan tugas akhir ini.
- BAB IV : RANCANGAN SISTEM
Memuat analisis atau penjelasan rancangan sistem dan prinsip kerja
sistem yang berupa diagram alir dan diagram blok.
Memuat uraian tentang implementasi sistem secara detail sesuai dengan
rancangan sistem yang telah dibuat yang secara logis dapat
menerangkan alasan diperolehnya hasil data dari penelitian.
Menjelaskan tentang pengujian sistem secara keseluruan pada objek
pengamatan serta pembahasan dan analisis dari hasil pengujian sistem
tersebut.
Berisi kesimpulan yang memuat uraian singkat tentang hasil penelitian
yang diperoleh sesuai dengan tujuan penelitian, serta saran untuk
penelitian lebih lanjut.

