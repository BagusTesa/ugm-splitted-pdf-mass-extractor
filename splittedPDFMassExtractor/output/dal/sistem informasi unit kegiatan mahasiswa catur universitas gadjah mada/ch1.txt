Catur merupakan permainan atau olahraga yang dimainkan oleh dua
orang. Pecatur adalah orang yang memainkan catur, baik dalam pertandingan satu
lawan satu maupun satu melawan banyak orang. Sebelum bertanding, pecatur
memilih biji catur yang akan ia mainkan. Terdapat dua warna yang
membedakan bidak atau biji catur, yaitu hitam dan putih. Pemegang buah putih
memulai langkah pertama yang selanjutnya diikuti oleh pemegang buah hitam
secara bergantian sampai permainan selesai.
Unit Kegiatan Mahasiswa (UKM) Universitas Gadjah Mada merupakan
suatu lembaga yang memberikan sarana dan prasarana bagi mahasiswa
Universitas Gadjah Mada untuk menyalurkan hobi, minat, bakat, seni dan
sebagainya, khususnya UKM Catur yang menyediakan sarana dan prasarana bagi
mahasiswa Universitas Gadjah Mada ataupun masyarakat umum untuk berlatih
dan berprestasi dalam bidang catur.
Unit Kegiatan Mahasiswa Catur UGM memerlukan media untuk
menyebarkan informasi dari UKM Catur Universitas Gadjah Mada seperti profil
UKM Catur UGM, prestasi-prestasi yang diraih, dokumentasi kegiatan, dan data
keanggotaan UKM Catur, bukan hanya dari media sosial Facebook ataupun
Twitter yang sudah ada tetapi juga dari website resmi UKM Catur Universitas
Gadjah Mada. Proses pendaftaran anggota UKM Catur yang masih menggunakan
formulir pendaftaran, dimana membuat calon anggota harus datang ke sekretariat
UKM Catur sehingga kurang efektif dan efisien. Pendaftaran seperti ini juga
membuat penumpukan dokumen atau formulir pendaftaran di sekretariat UKM
Catur Universitas Gadjah Mada. Dokumen-dokumen tersebut memiliki
kemungkinan untuk hilang sehingga membuat pengurus UKM Catur harus
mendata ulang anggota, sehingga UKM Catur UGM juga memerlukan suatu
sistem pendaftaran online yang mampu mengolah semua data-data pendaftaran
yang masuk per periode. Hal ini dapat mempermudah ketua UKM Catur dalam
mendata anggota dan mempermudah dalam pencarian anggota. UKM Catur UGM
juga memerlukan sistem yang dapat menilai anggota-anggota UKM Catur UGM
untuk mewakili UGM dalam suatu ajang perlombaan.
Untuk itu diperlukan suatu sistem informasi berbasis web yang
memberikan informasi dengan cepat dan akurat terkait dengan kegiatan, prestasi-
prestasi yang diraih, dokumentasi kegiatan, dan data keanggotaan UKM Catur itu
sendiri. Selain itu, sistem menyediakan fitur pendaftaran online sehingga
memudahkan mahasiswa Universitas Gadjah Mada untuk mendaftar sebagai
anggota UKM Catur UGM. Sistem ini diharapkan mampu membantu kinerja
pengurus UKM Catur UGM agar lebih efektif dan efisien, sehingga pengurus
tidak perlu menunggu mahasiswa datang untuk mendaftar. Sebaliknya mahasiswa
tidak perlu datang ke sekretariat UKM Catur UGM untuk melakukan pendaftaran.
Di samping itu diharapkan mampu melakukan pencarian data anggota dengan
cepat jika diperlukan. Sistem ini diharapkan mampu menampung kritik dan saran
dari mahasiswa Universitas Gadjah Mada maupun masyarakat umum demi
membangun UKM Catur Universitas Gadjah Mada yang lebih baik. Sistem juga
diharapkan mampu memberikan penilaian anggota UKM Catur UGM yang
mewakili UGM bertanding pada suatu ajang pertandingan.
Permasalahan pada tugas akhir ini adalah bagaimana membuat Sistem
Informasi Unit Kegiatan Mahasiswa Catur Universitas Gadjah Mada yang
memiliki fitur pendaftaran online serta dapat memberikan informasi tentang
UKM catur UGM seperti event, profil, data anggota dan galeri UKM Catur UGM,
materi, artikel tentang catur dan sistem dapat memberikan penilaian anggota
UKM Catur UGM yang layak untuk mewakili UGM bertanding pada suatu ajang
pertandingan.
Dalam pembuatan sistem informasi ini penulis memberikan beberapa
batasan masalah agar dalam menyusun sistem informasi ini menjadi terarah dan
sesuai dengan ketentuan yang ada. Batasan-batasan masalah meliputi:
1. Sistem tidak membahas tentang pendaftaran kejuaraan yang akan
diselenggarakan oleh UKM Catur UGM.
2. Sistem menampilkan informasi mengenai UKM Catur UGM.
3. Sistem dapat melayani pendaftaran anggota dan hanya dibuka
berdasarkan waktu pendaftaran yang telah ditentukan pengurus UKM
4. Sistem mengkalkulasi penilaian anggota UKM Catur UGM yang
mewakili UGM bertanding pada suatu ajang pertandingan.
Tujuan dari penelitian ini adalah untuk merancang dan membangun sistem
informasi untuk pendaftaran anggota baru secara online dan menampilkan
informasi seperti profil UKM Catur UGM, data anggota, dokumentasi kegiatan
UKM Catur UGM, informasi seputar catur dan kegiatan-kegiatan baik yang
diadakan oleh UKM catur UGM atau dari non-UKM catur UGM serta dapat
memberikan penilaian anggota UKM Catur UGM yang mewakili UGM
bertanding pada suatu ajang pertandingan.
Ada beberapa manfaat yang ingin dicapai dalam penelitian adalah:
1. Memberikan kemudahan kepada mahasiswa dalam melakukan
pendaftaran anggota baru UKM Catur UGM.
2. Memberikan informasi kepada masyarakat mengenai profil UKM Catur
UGM dan event-event terbaru yang akan diselenggarakan di dalam atau di
luar lingkup UKM catur UGM.
3. Memberikan kemudahan kepada pengurus UKM Catur UGM dalam
pengumpulan data anggota baru.
4. Memberikan informasi umum/spesifik mengenai catur dan
mempublikasikan materi tentang catur dari pengenalan hingga taktik
bermain catur kepada pengunjung website.
5. Memberikan penilaian anggota UKM Catur UGM yang mewakili UGM
bertanding pada suatu ajang pertandingan.
Metode-metode yang digunakan dalam penelitian ini adalah sebagai berikut :

Metode pengumpulan data adalah hal yang pertama kali dilakukan
untuk membuat perencanaan sebuah sistem. Metode ini dibagi menjadi
beberapa metode, antara lain:

Metode ini dilakukan untuk mengumpulkan data secara langsung
dari UKM Catur UGM.

Wawancara dilakukan untuk mengumpulkan informasi-informasi
mengenai UKM Catur UGM. Wawancara dilakukan dengan ketua

Studi literatur dilakukan untuk mengumpulkan data melalui
berbagai referensi seperti buku, artikel dan materi yang dapat
dijadikan acuan dalam pembuatan Sistem Informasi UKM Catur

Tahap analisis kebutuhan sistem ini menganalisis kebutuhan yang
dibutuhkan oleh sistem. Menjelaskan kebutuhan fungsional dan non-
fungsional pada sistem.

Dalam tahap ini dilakukan penerapan data pada sistem
menggunakan Diagram Alir Data (DAD), proses bisnis yang bekerja pada
sistem menggunakan activity diagram, perancangan basis data,
perancangan struktur menu dan tahap perancangan antarmuka.

Pada tahap ini dilakukan pembangunan sistem berdasarkan analisis
dan perancangan yang telah dibuat sebelumnya.

Tahap pengujian merupakan tahap terakhir dari pembuatan sistem.
User mencoba sistem untuk memakai sistem dan melaporkan jika masih
terjadi kekurangan untuk kemudian disempurnakan lagi.
Untuk menjelaskan sistem yang dibuat, maka sistematika penulisan
laporan proyek sistem informasi ini dibagi menjadi beberapa bab, antara lain :
Bab satu menguraikan tentang latar belakang masalah, perumusan
masalah, batasan masalah, tujuan penelitian, manfaat penelitian, metode
pengumpulan data serta sistematika penulisan.
Bab dua berisi uraian sistematis tentang informasi dari hasil penelitian
yang menjadi referensi yang berkaitan dengan penelitian yang sedang
dilakukan.
Bab tiga berisi penjelasan mengenai teori-teori dan prinsip ilmu yang
menjadi landasan dalam penelitian yang dilakukan.
Bab empat berisi uraian analisis serta perancangan sistem yang menjadi
konsep dalam pembuatan sistem yang akan dibuat. Analisis kebutuhan
ini meliputi analisis sistem, rancangan proses, rancangan basis data, dan
rancangan antar muka pengguna.
Bab lima menguraikan tentang implementasi hasil perancangan desain
yang menampilkan aplikasi antar muka menggunakan bahasa
pemrograman PHP dan MySQL disertai cara kerja dan penggunaan
program.
Bab enam ini membahas tentang pengujian dan pembahasan sistem yang
menguji kesesuaian sistem dengan perancangan yang sudah dibuat.
Pengujian bertujuan untuk mengevaluasi kinerja sistem, mengurangi
adanya kesalahan serta memastikan sistem dapat digunakan dengan
baik.
Bab ini berisi kesimpulan dan saran dari hasil penelitian yang telah
dilakukan.

