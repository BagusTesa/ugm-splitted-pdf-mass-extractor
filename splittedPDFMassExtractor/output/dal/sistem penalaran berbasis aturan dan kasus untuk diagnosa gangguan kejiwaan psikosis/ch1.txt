Istilah gangguan kejiwaan/gangguan mental adalah seluruh gejala atau pola
perilaku seseorang yang dapat ditemukan secara klinis yang berkaitan dengan
tekanan/distress pada kebanyakan kasus dan berkaitan dengan terganggunya fungsi
seseorang (Anonim1, 2008). Prevelensi gangguan kejiwaan nasional Indonesia
menurut laporan Riset Kesehatan Dasar (RISKESDAS) yang disusun oleh Badan
Penelitian dan Pengembangan Kesehatan RI berdasarkan survey terhadap penduduk
Indonesia yang berumur ?15 tahun adalah 11,6% yang berarti diantara sekitar 19 juta
jiwa terdapat sekitar 2.2 juta jiwa yang memiliki gangguan kejiwaan (Anonim1,
2008).
Menurut Retnowati (2011)  yang merupakan Guru Besar Fakultas Psikologi UGM
mengatakan bahwa berdasarkan jumlah penduduk Indonesia pada tahun 2011 yang
berjumlah sekitar 241 juta jiwa, hanya terdapat 600 orang psikiater dan 365 orang
psikolog. Hal ini mengakibatkan terdapat perbandingan 1 psikiater untuk 401.000
jiwa dan 1 psikolog untuk 660.000 jiwa.
Penelitian mengenai penyakit mental �The Global Burden of Disease� yang
dilakukan oleh Murray (1996) bekerjasama dengan WHO dan World Bank
memprediksikan bahwa penyakit mental akan menduduki posisi kedua setelah
penyakit kardiovaskuler pada tahun 2020. Lima besar penyakit kejiwaan yang paling
besar memiliki dampak adalah depresi unipolar, penggunaan alkohol, gangguan
afektif bipolar, skizofrenia dan gangguan obsesif kompulsif. Mengingat banyaknya
gangguan pada kejiwaan, maka pada penelitian ini akan berfokus kepada gangguan
kejiwaan psikosis khususnya gangguan kejiwaan Skizofrenia.
Gangguan kejiwaan psikosis adalah gangguan jiwa yang ditandai dengan
terjadinya kehilangan kontak dengan realitas dengan gejala halusinasi, waham,
kelainan perilaku (Puri dkk, 2011). Gangguan kejiwaan psikosis dalam PPDGJ III
(Pedoman Penggolongan dan Diagnosis Gangguan Jiwa III) terbagi ke dalam 2
bagian utama yaitu Skizofrenia dan gangguan yang terkait (Kode F2) dan gangguan
afektif (Kode F3).
Gangguan mental Psikosis tipe Skizofrenia adalah gangguan mental berat yang
mengakibatkan ketidakmampuan untuk melihat realita, kebingungan dalam
membedakan mana yang realita dan mana yang bukan realita dimana penderita
mengalami ciri-ciri yaitu adanya halusinasi serta kehilangan kontrol dan integrasi
terhadap perilakunya sendiri (Razzouk, 2006). Razzouk,dkk (2006) juga telah
membuat SPK untuk mendiagnosa penyakit Skizofrenia, namun akuisisi pengetahuan
dari 3 orang pakar mengalami kesulitan karena tidak adanya kesepakatan antara 1
pakar dengan pakar yang lain dalam melihat gejala-gejala yang berhubungan dengan
Skizofrenia. Hal ini memberikan ketidaktepatan klasifikasi yang mencapai 34% dan
tingkat akurasi antara 60-82%. Oleh karena itu, peneliti merasa metode yang
digunakan oleh Razzouk untuk mendiagnosa gangguan kejiwaan Skizofrenia kurang
mengakomodir dalam melakukan diagnosa Skizofrenia sehingga perlu diteliti dengan
metode lain.
Dengan mengetahui fakta-fakta di atas, mengakibatkan sistem komputer yang
memanfaatkan metode kecerdasan buatan untuk membantu kerja para profesional
dibidang kedokteran kejiwaan menjadi sangat diperlukan. Peneliti merasa perlu untuk
melakukan penelitian dengan membangun sebuah sistem yang dapat membantu
paramedis dalam menegakkan diagnosa gangguan kejiwaan psikosis. Dengan
tegaknya diagnosa gangguan kejiwaan yang tepat maka penyembuhan akan dilakukan
secara tepat pula dikarenakan penyembuhan antara orang yang satu dengan yang lain
tidaklah sama.
Sistem yang dibangun menggunakan penalaran berbasis aturan (rule-based
reasoning/RBR) dan berbasis kasus (case-based reasoning/CBR). RBR dilakukan
terlebih dahulu untuk mendiagnosa gangguan psikosis pasien yang terdiri dari
gangguan Skizofrenia (F20), gangguan waham menetap (F22) dan gangguan psikotik
akut dan sementara (F23). Jika pasien terdiagnosa Skizofrenia maka akan digunakan
CBR untuk menentukan jenis Skizofrenia yang diderita yang terdiri dari Skizofrenia
paranoid (F20.0), Skizofrenia hebefrenik (F20.1), Skizofrenia katatonik (F20.2),
Depresi pasca-skizofrenia (F20.4) dan Skizofrenia residual (F20.5).
Diagnosa gangguan psikosis dilakukan dengan menggunakan RBR berdasarkan
aturan-aturan yang sudah ditentukan dalam PPDGJ III sedangkan diagnosa jenis
Skizofrenia dilakukan dengan RBR dikarenakan gejala-gejala yang muncul saling
tumpang tindih antara satu jenis Skizofrenia dengan jenis Skizofrenia yang lain
sehingga tidak dimungkinkan untuk menggunakan RBR.
Dalam CBR, pemecahan kasus baru dilakukan dengan mengadaptasi solusi dari
kasus-kasus lama yang sudah terjadi dimana mencari similaritas atau tingkat
kesamaan antara kasus baru dengan kasus lama adalah merupakan tahapan yang
paling penting (Pal dan Shiu, 2004). Penelitian ini menggunakan metode similaritas
Wieghted Nearest Neighbor (WNN) untuk mendiagnosa jenis Skizofrenia yang
dimiliki pasien. WNN merupakan suatu pendekatan untuk mecari kasus dengan
menghitung kedekatan antara kasus baru dengan kasus lama berdasarkan pada
pencocokan bobot dari sejumlah fitur yang ada (Kusrini dan Luthfi, 2009).
Berdasarkan latar belakang yang sudah diuraikan, dapat dirumuskan beberapa
masalah yaitu:
1. Bagaimana melakukan diagnosa awal gangguan kejiwaan Psikosis dengan
menggunakan penalaran berbasis aturan (RBR)
2. Bagaimana mendiagnosa jenis penyakit Skizofrenia yang diderita pasien
dengan menggunakan penalaran berbasis kasus (CBR)
Untuk dapat melakukan penelitian secara jelas dan terarah maka penelitian ini
akan dibatasi oleh :
1. Penalaran berbasis aturan yang digunakan untuk diagnosa gangguan kejiwaan
psikosis adalah gangguan dengan kode F2 yang terdiri dari gangguan
Skizofrenia (F20) , gangguan waham menetap (F22) dan gangguan psikotik
akut dan sementara (F23) yang terdapat pada PPDGJ III
2. Penalaran berbasis kasus yang digunakan untuk diagnosa jenis Skizofrenia
adalah gangguan yang terdiri dari Skizofrenia paranoid (F20.0), Skizofrenia
hebefrenik (F20.1), Skizofrenia katatonik (F20.2), Depresi pasca-skizofrenia
(F20.4) dan Skizofrenia residual (F20.5) yang terdapat pada PPDGJ III
3. Diagnosa gangguan kejiwaan ini tidak mencakup diagnosa gangguan
kejiwaan pada anak-anak.
4. Sistem ini tidak menerapkan manajemen ketidakpastian dan tidak
memperhitungkan tingkat kepercayaan paramedik sebagai pengguna terhadap
gejala yang muncul pada saat melakukan diagnosa jenis penyakit Skizofrenia.
Penelitian ini bertujuan untuk menghasilkan sebuah sistem dengan menggunakan
penalaran berbasis aturan untuk diagnosa awal gangguan kejiwaan psikosis dan
menggunakan penalaran berbasis kasus untuk diagnosa jenis-jenis gangguan psikosis
Penelitian ini akan memberi manfaat kepada tenaga medis dalam melakukan
diagnosa awal gangguan kejiwaan psikosis dan diagnosa jenis-jenis penyakit
Penelitian untuk mediagnosa penyakit kejiwaan sudah dilakukan diantaranya
adalah rancang-bangun sistem pendukung keputusan kelompok untuk amnesis,
diagnosis dan terapi gangguan jiwa yang dilakukan untuk gangguan jiwa non psikosis
(Kusumadewi,dkk., 2008). Kemudian sistem pakar diagnosa awal gangguan jiwa
dengan metode certainty factor berbasis mobile cellular untuk mendiagnosa
gangguan jiwa neurosis (Wita,dkk., 2012). Razzouk,dkk (2006) membuat SPK untuk
mendiagnosa penyakit Skizofrenia, namun akuisisi pengetahuan dari 3 orang pakar
mengalami kesulitan karena tidak adanya kesepakatan antara 1 pakar dengan pakar
yang lain dalam melihat gejala-gejala yang berhubungan dengan Skizofrenia. Oleh
karena itu, peneliti merasa metode yang diajukan oleh Razzouk untuk mendiagnosa
gangguan kejiwaan Skizofrenia kurang tepat sehingga perlu diteliti dengan metode
lain. Metode yang akan digunakan peneliti untuk melakukan diagnosa gangguan
kejiwaan Psikosis dan gangguan kejiwaan jenis-jenis Skizofrenia dengan
menggunakan penalaran berbasis aturan dan berbasis kasus belum pernah dilakukan.
1.7 Metodologi Penelitian
Penelitian ini menggunakan metode pengembangan perangkat lunak yang terdiri
dari tahap-tahap berikut ini:

Studi pustaka merupakan kegiatan untuk mempelajari literatur-literatur dan
teori yang mendukung dalam melakukan penalaran berbasis aturan dan
berbasis kasus untuk melakukan diagnosa penyakit jiwa psikosis. Studi
pustaka dibagi menjadi dua bagian yaitu penelitian yang dilakukan dalam
domain gangguan kejiwaan dan penelitian yang dikembangkan dengan
menggunakan CBR. Pada tahap ini akan dilakukan pula pembuatan proposal
penelitian.

Pengumpulan data akan diperoleh dari rekam medis penyakit kejiwaan serta
konsultasi dengan pakar yaitu seorang dokter spesialis keokteran jiwa di
rumah sakit GRHASIA Yogyakarta.
3. Analisis Kebutuhan dan Perancangan Sistem
Analisis kebutuhan merupakan tahap untuk melakukan analisis pada data
yang telah diperoleh sehingga didapatkan fitur-fitur untuk digunakan dalam
penalaran berbasis aturan dan kasus, menganalisis kebutuhan input, proses
dan kebutuhan output.
Merupakan tahap pembuatan sistem berdasarkan hasil perancangan sistem.
Rancangan sistem akan dibangun dengan menggunakan sistem operasi
Windows 7 dengan bahasa pemrograman Delphi dan database MySql.

Tahap ini merupakan pengujian terhadap sistem yang dibuat yang dilakukan
dengan menggunakan data uji kasus yang baru untuk memastikan bahwa
aplikasi telah berjalan dengan baik dan menghasilkan keputusan yang tepat.
Pengujian sistem dilakukan dengan membandingkan hasil diagnosa dokter
dan hasil diagnosa yang diberikan melalui sistem.
6. Evaluasi dan Perbaikan Kesalahan
Tahapan ini merupakan tahapan untuk melakukan evaluasi dan perbaikan
setelah dilakukan implemetasi sistem. Jika hasil pengujian menunjukan bahwa
aplikasi yang dikembangan sudah benar maka proses perbaikan tidak perlu
dilakukan dan jika belum benar maka proses perbaikan dapat dilakukan
diantaranya dengan melakukan perbaikan pada basis aturan, basis kasus,
retrieval, dan perhitungan pencarian similaritas antara kasus lama dengan
kasus baru.

Merupakan tahap akhir dalam penelitian ini dengan memberikan hasil dan
membuat laporan penelitian.
Penulisan Tesis ini akan dibagi dalam 7 bab, dengan rincian masing-masing
sebagai berikut :
Dalam pendahuluan ini membahas mengenai latar belakang penelitian, perumusan
masalah, batasan masalah, tujuan penelitian, manfaat penelitian, keaslian
penelitian, metodologi penelitian, dan sistematika penulisan penelitian.
Pada bab ini akan diuraikan secara sistematis tentang penelitian yang terdahulu
dan menghubungkan dengan penelitian yang sedang dilakukan.
Pembahasan dalam landasan teori meliputi teori-teori yang digunakan dalam
penelitian yaitu penalaran berbasis aturan, penalaran berbasis kasus, similaritas
wieghted nearest neighbor dan penyakit psikosis
BAB IV. ANALISA DAN PERANCANGAN SISTEM
Dalam bab ini akan dibahas mengenai rancangan sistem penalaran berbasis aturan
untuk diagnosa awal gangguan psikosis dan penalaran berbasis kasus untuk
diagnosa jenis Skizofrenia
Di dalam bab ini berisi implementasi dari rancangan sistem yang sudah dibuat
sebelumnya.
Dalam bab ini membahas hasil dari implementasi yang sudah dilakukan dan di
dalam bab ini juga ditampilkan hasil dari implementasi.
Dalam bab terakhir ini berisi kesimpulan dari penelitian dan juga diberikan saran-
saran yang mungkin bisa dipertimbangkan untuk dapat menghasilkan suatu sistem
yang lebih baik.

