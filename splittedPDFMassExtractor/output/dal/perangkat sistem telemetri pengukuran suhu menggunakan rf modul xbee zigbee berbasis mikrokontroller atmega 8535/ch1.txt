Suhu merupakan salah satu faktor yang sangat penting bagi kehidupan.
Suhu sangat mempengaruhi keseimbangan alam, makhluk hidup dan tidak hanya
makan mengalami suatu ketidaknyamanan ketika suhu tidak beraturan. Biasanya
untuk mengetahui kondisi suhu biasanya masih menggunakan kabel. Namun ada
cara yang yang dapat dilakukan dengan lebih efisiensi tanpa kabel. Oleh karena
itu perlu dirancang suatu alat yang dapat mendeteksi suhu dari jarak jauh dan
memberikan peringatan jika suhu sudah tidak baik bagi kesehatan. Dengan
menggunakan Sistem telemetri,  pendeteksian suhu jarak jauh dapat dilakukan.
Telemetri merupakan sebuah teknologi  yang membolehkan
Pengukuran jarak jauh dan pelaporan informasi kepada perancang atau
operator sistem atau dengan kata lain suatu proses pengukuran parameter suatu
obyek (kondisi alam, benda ataupun ruang) yang mana hasil pengukuran akan
dikirimkan ke tempat lain melalui proses pengiriman data baik menggunakan
kabel maupun nirkabel (wireless). Selanjutnya data tersebut dapat diproses dan
dianalisis untuk keperluan tertentu.
Pada perancangan dan implementasi pendeteksian suhu menggunakan RF
Modul XBEE Zigbee. Perangkat ini sendiri terdiri dari hardware serta software,
dimana perangkat di bagi menjadi pengirim (transmitter) serta penerima
(receiver). Pada perangkat pengirim terdapat sensor suhu LM35 yang terintergrasi
dengan mikrokontroller AT8535 kemudian ditransmisikan dengan menggunakan
perangkat XBEE.  Setelah transmisi berhasil, dibagian penerima akan diterima
oleh perangkat XBEE yang terhubung dengan PC. Dan PC pun akan
menampilkan kondisi suhu yang terdeteksi pada user interface serta mengaktifkan
buzzer dan speaker jika suhu telah di ambang batas yang telah ditentukan.
Bagaimana membuat suatu Perangkat sistem Telemetri pengukuran Suhu
menggunakan RF Modul XBEE ZIGBEE Berbasis Mikrokontroller ATMega
8535.
Tujuan dari penelitian ini adalah membuat untuk mendesain serta
mengimplementasi sistem telemetri untuk pengukuran suhu.
Dalam perancangan dan penulisan tugas akhir ini akan ditentukan batasan-
batasan masalah yang meliputi:
1. Pembuatan sistem pendeteksi berupa pemograman bascom mikrokotroller
dan Grafic Using Interface (GUI) yang menggunakan software visual
basic.06.
2. Perfomansi yang akan diukur seperti keakuratan sensor, jarak maksimum
serta kerja fungsional sistem yang telah dibuat.
3. Tugas akhir ini hanya berupa Prototipe yang disimulasikan dengan
mengukur suhu dari sebuah sumber.
1.5     Metode penelitian
Metode penelitian yang dilakukan dalam penelitian tugas akhir ini adalah
sebagai berikut:
1. Studi kepustakaan :
Membaca serta mengumpulkan bahan bahan materi yang berkaitan
dengan sistem telemetri, mikrokontroller, XBEE, buzzer dan sensor suhu
seperti buku buku, e-book maupun jurnal.
2. Analisa kebutuhan
Kebutuhan software :
- Aplikasi Tampilan Perangkat menggunakan visual basic.06.
- menggunakan program bascom untuk mikrokontroller.
Kebutuhan hardware:
- XBEE sebagai komunikasi serial untuk sistem telemetri
- Mikrokontroller ATMega 8535 sebagai sistem minimum
- LM35 sebagai sensor suhu
- LCD sebagai tampilan pada sistim minimum
- Buzzer sebagai indikator peringatan
- Speaker sebagai indikator peringatan
3. Perancangan alat
- Perancangan rangkaian sistem minimum mikrokontroller
- Perancangan XBEE sebagai komunikasi serial telemetri
- Perancangan Speaker
4. Pengujian alat
Setelah tahap perancangan berdasarkan standar yang ada dan dilakukan
troubleshooting untuk mengetahui seluruh komponen telah terhubung atau tidak.
Tahap selanjutnya adalah melakukan simulasi sistem yang telah dibuat.
5.  Menganalisis data hasil pengujian alat.
Laporan penelitian tugas akhir ini disusun dengan sistematika sebagai
berikut:
Meliputi latar belakang dan permasalahan, batasan masalah, tujuan
penelitian, metodologi, dan sistematika penulisan.
Memuat tentang informasi-informasi tentang hasil penelitian yang telah
dilakukan terkait dengan pembuatan deteksi suhu menggunakan sistem telemetri
berbasis mikrokontroller ATMega 8535
Memuat tentang landasan teori setiap komponen yang menunjang dalam
pembuatan dan pembahasan tugas akhir ini.
Memuat analisis dan perancangan sistem perangkat keras dan perangkat
lunak sistem. Sistem tersebut meliputi desain sistem secara keseluruhan, skematik
rangkaian, diagram blok, dan diagram alir dari sistem ini.
Memuat uraian tentang implementasi sistem secara detail sesuai dengan
rancangan dan berdasarkan komponen serta bahasa pemrograman yang
digunakan. Serta menjelaskan pengujian dari setiap bagian sistem yang dibuat.
Memuat analisa hasil pengujian sistem secara keseluruhan yang meliputi
pengecekan hardware dan software.
Berisi kesimpulan yang memuat uraian singkat tentang hasil penelitian yang
diperoleh sesuai dengan tujuan penelitian, serta saran untuk penelitian lebih lanjut.

