 
 

Kemajuan teknologi saat ini berkembang sangat pesat. Perkembangan
teknologi informasi sangat membantu manusia dalam menghadapi era
globalisasi. Sebuah konsekuensi yang logis dari penerapan teknologi informasi
yang unggul adalah kemajuan suatu bidang atau usaha yang memanfaatkan
teknologi informasi yang berdaya guna dengan tepat guna disertai dengan
kebutuhannya. Teknologi informasi tidak hanya memiliki dampak  besar pada
pendidikan, namun juga dunia perdagangan atau bisnis. Berbagai bidang usaha
telah memanfaatkan tekonologi informasi untuk meningkatkan produktivitas
usahanya.
Toko BIMAJAYA merupakan sebuah bidang usaha yang melayani
penjualan sepeda bekas dan aksesoris sepeda yang saat ini dalam melakukan
pemesanan produk sepeda melalui cara konvensional yaitu dengan cara (on
site) atau pemesanan dilakukan di toko yang menyebabkan wilayah pemasaran
produk masih terbatas oleh jarak dan waktu. Selain itu, pencatatan transaksi
penjualan belum terkomputerisasi, sehingga data transaksi penjualan tidak
tersimpan dengan baik dan memiliki resiko kesalahan yang tinggi dalam
pengolahan transaksi penjualan sepeda bekas.
Jika dilihat dari segi kemudahan pengelolaaan data penjualan produk
sepeda, maka pengelolaan data dengan sistem tersebut tidak efektif. Oleh
karena itu, diperlukan sistem terkomputerisasi yang dapat mempermudah
pemesanan produk, pengelolaan data penjualan dan pembuatan laporan
transaksi. Sistem informasi penjualan sepeda bekas ini dapat mempermudah
dalam memberikan informasi mengenai sepeda bekas yang tersedia dan
pemesanan sepeda yang dapat dilakukan secara online. Selain itu, sistem
informasi penjualan sepeda bekas ini juga akan mempermudah dalam
pengelolaan data penjualan, sehingga data yang dikelola menghasilkan laporan
yang akurat.

Berdasarkan latar belakang yang telah dikemukan maka rumusan
masalahnya adalah bagaimana membangun sistem informasi penjualan sepeda
bekas untuk memberi kemudahan proses pemesanan sepeda bekas serta
meningkatkan produktivitas usaha dan jangkauan pemasaran produk.

Berdasarkan latar belakang yang telah dikemukan, maka batasan
masalah yang ada pada sistem adalah sebagai berikut :
1. Sistem tidak menangani pembayaran serta konfirmasi pembayaran secara
online
2. Sistem tidak menangani laporan pengiriman barang.
3. Sistem tidak menangani cetak nota transaksi dengan perhitungan
pembayaran dan pengembalian yang merupakan konfirmasi dari
pemesanan.
4. Sistem tidak dilengkapi dengan keamanan jaringan dan keamanan data.

Tujuan membuat sistem informasi ini adalah membangun sistem
informasi penjualan sepeda bekas berbasis web di toko sepeda bekas
BIMAJAYA yang memberikan kemudahan dalam melakukan pemesanan
produk sepeda bagi pelanggan dan meningkatkan produktivitas usaha bagi
pemilik.

Manfaat Penelitian ini antara lain :
1. Membantu pemilik dalam menyampaikan informasi produk yang tersedia
2. Memudahkan pelanggan melakukan pemesanan produk
3. Memudahkan dalam mengelola data pemesanan dan transaksi penjualan.
4. Mempermudah pencarian data produk, data pelanggan, data transaksi
pemesanan, dan laporan penjualan.
5. Menampilkan data transaksi penjualan dalam bentuk laporan dan grafik
berdasarkan periode.

Metode penelitian yang dilakukan oleh penulis dalam menyusun Tugas
Akhir ini adalah:

Melakukan pengamatan dan pencatatan dengan cara melakukan penelitian
langsung ke Toko BIMAJAYA sehingga diperoleh data akurat dalam
proses aktivitas usahanya.

Dalam metode ini penulis mengadakan wawancara secara langsung
dengan pemilik usaha untuk mengetahui alur bisnis.

Studi literatur berisi tentang teori, temuan, dan bahan penelitian lain yang
dijadikan bahan acuan untuk kegiatan penelitian studi literatur dilakukan
dengan mengumpulkan dan mempelajari informasi-informasi yang
berkaitan dengan sistem informasi penjualan sepeda bekas berbasis web.
4. Pengembangan sistem

Analisis yang dilakukan yaitu analisis terhadap sistem yang lama dan
analisis kebutuhan sistem yang baru.

Sistem yang baru dirancang berdasarkan analisis yang telah
dilakukan. Sistem yang dirancang meliputi perancangan basis data,
perancangan menu, dan perancangan interface.

Implementasi dan perancangan yang telah dilakukan menggunakan
bahasa pemrograman PHP dan DBMS MySQL.

Sistematika penulisan tugas akhir ini merupakan kerangka pokok yang
terkandung dalam bab-bab yang akan memudahkan memahami isi dari tugas
akhir ini. Sistematika penulisan tugas akhir ini adalah sebagai berikut :
BAB I  PENDAHULUAN
Bab ini berisi tentang latar belakang masalah, rumusan masalah,
batasan masalah, tujuan, manfaat, metode penelitian, dan sistematika
penulisan
BAB II  TINJAUAN PUSTAKA
Bab ini berisi referensi-referensi yang relevan mengenai sistem
sejenis yang sudah pernah dibuat sebelumnya. Tinjauan pustaka
berupa Tugas Akhir, Skripsi, Jurnal, dan Tesis.
Bab ini berisi mengenai teori-teori yaitu konsep dasar sistem, siklus
hidup perkembangan sistem, konsep dasar informasi, konsep dasar
sistem informasi, basis data, HTML, PHP, CSS, javascript,
jQuery,MVC, dan CodeIgniter.
Bab ini menerangkan analisa terhadap sistem dan proses dalam
pembuatan sistem. Analisa terhadap sistem yang berjalan saat ini dan
sistem yang akan dibuat, perancangan database sistem, pembuatan
DAD dan Flowchart sistem, serta perancangan antarmuka pengguna.
Bab ini berisi implementasi hasil dari pembuatan sistem.
Bab ini berisi hasil penelitian dan pembahasan dari pembuatan
sistem dan pengujian program.
Bab ini berisi kesimpulan dari sistem yang telah dibuat dan saran
untuk pengembangan sistem yang lebih lanjut.

