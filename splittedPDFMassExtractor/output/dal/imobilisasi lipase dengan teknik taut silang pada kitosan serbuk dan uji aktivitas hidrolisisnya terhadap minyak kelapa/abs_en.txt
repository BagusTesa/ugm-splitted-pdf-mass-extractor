Immobilization of lipase on chitosan powder was performed via cross
linking technique. The aim of this research is to determine the optimum
conditions for the immobilization process. Chitosan was prepared from crab shell
through continous deacetylation, and crosslinked with glutaraldehyde.
Immobilization of lipase on chitosan powder was studied under the optimum
conditions e.i. pH optimum of lipase solubility and mole ratio of
chitosan:glutaraldehyde. Immobilized lipase on chitosan powder was tested for
the activity, thermal stability, multiple-use stability and kinetic parameters
through the hydrolysis reaction of palm oil.
Continous deacetylation produced chitosans A, B and C with deacetylation
degrees of 81.34% , 90.23%, and 92.80%, respectively. The optimum pH  for
enzyme dissolution was obtained at pH 6. Mole ratio of chitosan:glutaraldehyde
for the enzyme immobilization reached optimum condition at a ratio of 2:1.
Optimum efficiency of immobilization on chitosans A, B, and  C, were 53.01;
43.84 and 40.21%, respectively. Specific activity of free lipase, immobilized
lipase on chitosan A, immobilized lipase on chitosan B and  immobilized lipase
on chitosan C were 23.94; 0.0472; 0.0151; and 0.0144 U/g,  respectively.
Immobilized lipase was able to maintain its activity at 50 �C. The immobilized
lipase was also able to maintain its activity after multiple-uses up to five reaction
cycles. Affinity of lipase immobilized on the substrate was decreased than free
lipase.
Keywords: lipase, immobilization, chitosan, cross linking, hydrolase.

