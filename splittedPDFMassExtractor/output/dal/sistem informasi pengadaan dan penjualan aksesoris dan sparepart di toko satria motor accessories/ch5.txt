Berdasarkan pengujian yang telah dilakukan pada Sistem informasi
Pengadaan dan Penjualan Toko Stria Motor maka dapat diambil kesimpulan
sebagai berikut :
1. Sistem Informasi Pengadaan dan Penjualan Satria Motor
dapatmempermudah dalam pengolahan data Pengadaan dan penjualan.
2. Sistem memiliki fitur pengelolaan data, yaitu :
a. Pengelolaan data pemasok,
b.  Pengelolaan data  master barang
c.  Pengelolaan data merk
d.  Pengelolaan data satuan
e.  Pengelolaan data barang
f.  Pengelolaan transaksi Pengadaan
g. Pengelolaan laporan Pengadaan
h.   Pengelolaan laporan stok
i. Pengelolaan transaksi penjualan
j.  Pengelolaan laporan penjualan
k.  Pengelolaan statistik penjualan
l.  Pengelolaan data user
Untuk melakukan pengembangan yang lebih baik pada sistem informasi
pengadaan dan penjualan yang ada pada Satria Motor selanjutnya dapat diambil
saran, yaitu sistem tidak hanya menangani pengadaan dan penjualan saja, tetapi
juga menangani keuangan toko Satria Motor secara keseluruhan.

