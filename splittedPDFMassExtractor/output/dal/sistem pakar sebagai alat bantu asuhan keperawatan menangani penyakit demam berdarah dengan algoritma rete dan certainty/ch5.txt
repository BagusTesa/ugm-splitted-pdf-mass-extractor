Hasil penelitian yang telah dilakukan, menghasilkan sistem yang dapat
mendiagnosa penyakit Dengam Berdarah Dengue(DBD) dengan kesimpulan sebagai
berikut:
1. Sistem pakar dengan mesin inferensi Forward Chaining, Algoritma Rete dan
Certainty Factor dapat dipergunakan untuk mendiangnosa tingkat resiko
penyakit Demam Berdarah Dengue (DBD), dengan masukkan berupa gejala-
gejala yang dimiliki pasien. Dari beberapa kasus yang diujicobakan diperoleh
hasil diagnosa yang sama antara perhitungan sistem dan perhitungan manual.
2. Dengan mengimplementasi Sistem Pakar ini, dapat membantu asuhan
keperawatan di Puskesmas untuk mendiangnosa tingkat resiko penyakit
Demam Berdarah Dengue (DBD), dan menemukan solusi serta memberikan
saran-saran dalam penanganan secara umum, sebelum pemeriksaan oleh
dokter ahli.

Berdasarkan hasil pengujian yang dilakukan terhadap Sistem Pakar sebagai
alat bantu  asuhan keperawatan  dalam mendiagnosa penyakit DBD, tentunya masih
ada kekurangan dan kelemahannya, oleh karena itu untuk pengembangan sistem
dimasa yang akan datang maka sebaiknya perlu saran-saran sebagai berikut:
1. Sistem Pakar yang dihasilkan baru sebatas menghasilkan diagnosis dan saran
beserta nilai keyakinan tetapi belum termasuk menghasilkan saran yang lebih
spesifik yang dilengkapi dengan obat-obat yang dibutuhkan beserta prediksi
waktu untuk penyembuhan.
2. Sistem Pakar yang dihasilkan baru dapat berjalan pada sistem operasi windows
dan linux dengan menggunakan Personal Komputer (PC), penelitian lebih lanjut
diharapkan dapat mengembangkan model Sistem Pakar yang dapat dijalankan
dengan sistem operasi berbasis android yang dapat dimasukkan kedalam smart
phone, sehingga mudah dibawa kemana-mana oleh petugas kesehatan.
3. Penelitian lebih lanjut diharapkan dapat mengunakan metode yang berbeda
misalnya menggunakan metode penelususan backword chaining dengan
algoritma Snort yang juga didukung oleh JESS, serta bisa membandingkan
efisiensi dan akurasi dengan metode algoritma Rete dan Certainty Factor.

