Puskesmas Salam merupakan salah satu puskesmas di Kabupaten Magelang
yang menangani ratusan pasien setiap harinya. Sebagai puskesmas teladan,
pelayanan kepada masyarakat adalah hal yang diprioritaskan di Puskesmas Salam.
Selain pelayanan kesehatan, Puskesmas Salam juga aktif dalam mengadakan
sosialisasi dan bakti sosial di desa � desa guna memberi wawasan kepada
masyarakat mengenai pentingnya menjaga kesehatan dan kebersihan lingkungan.
Namun dalam penerapannya, penentuan tempat sosialisasi dan bakti sosial masih
kurang tepat sasaran dikarenakan pemilihan daerah yang masih acak.
Implementasi Data Warehouse dan Business Intelligence dapat membantu
mengatasi permasalahan di atas, karena di dalamnya terdapat laporan daerah �
daerah yang memenuhi kriteria untuk diadakan sosialisasi dan bakti sosial. Selain
itu, Implementasi Data Warehouse dan Business Intelligence ini juga mampu
menghasilkan laporan yang berguna  untuk evaluasi tiap tahunnya.
Implementasi Data Warehouse dan Business Intelligence ini dibangun untuk
tujuan tersebut dengan memanfaatkan data histori jumlah kunjungan pasien
Puskesmas Salam. Selain itu, Implementasi Data Warehouse dan Business
Intelligence ini juga menghasilkan informasi berupa laporan dalam beragam
bentuk tampilan.
Kata kunci : data histori pasien Puskesmas Salam, data warehouse, business
intelligence, laporan.

