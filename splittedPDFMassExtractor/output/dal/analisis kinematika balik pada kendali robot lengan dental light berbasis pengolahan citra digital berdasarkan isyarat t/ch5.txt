Berdasarkan penelitian yang dilakukan maka dapat diambil beberapa
kesimpulan sebagai berikut :
1. Telah berhasil diimplementasikan integrasi kendali robot lengan
menggunakan analisis kinematik balik berdasarkan pengolahan citra
digital (OpenCV) terhadap isyarat tangan pada alat dental light.
2. Kedua metode yang digunakan menunjukkan performansi keakuratan yang
tinggi dilihat dari selisih hasil sudut joint displacement yang mencapai tiga
angka di belakang koma.
3. Hasil algoritma kinematika balik dengan menggunakan metode analisis
aljabar dibuktikan akurat dan sesuai dalam penurunan persamaannya
dengan membandingkan hasil dengan perhitungan algoritma kinematika
maju.
4. Data pengujian algoritma kinematika balik metode analisis aljabar secara
hardware menunjukkan kinerja yang baik ditunjukkan dengan nilai error
yang kecil dengan selisih antara nilai sumbu uji dengan nilai sumbu input
berada pada range -1 sampai 2 mm.
5. Waktu eksekusi yang dibutukan untuk satu kali looping algoritma
kinematika balik berada pada range 207-208 ms, sedangkan waktu
eksekusi untuk instruksi subsistem focus pedal berada pada range 36-38
ms.
6. Data instruksi subsistem focus pedal dapat terkirim semua (100%) dalam
rangkaian pengujian pada range 1 meter sampai 3.5 meter.
7. Data instruksi subsistem pengenal isyarat jari tangan terkirim dan
dieksekusi seluruhnya (100%) untuk semua arah isyarat dengan aturan
jarak jari dari webcam pada range 60-65 cm.
Berikut saran-saran yang dapat dilakukan untuk pengembangan dalam
penelitian selanjutnya yang sejenis :
1. Algoritma pengolahan citra dapat menggunakan metode lain untuk
mengenali bentuk jari dan sarung tangan dari user dikarenakan untuk saat
ini sangat terbatas pada penggunaan sarung tangan warna kuning.
2. Implementasi wireless module antara subsistem pengenal isyarat jari
tangan dan subsistem kendali robot lengan agar lebih fleksibel dalam
bekerja.
3. Desain mekanik dari robot lengan ditingkatkan untuk mengakomodir
mekanisme rangka penahan beban, bukan aktuator yang menahan beban.
Bahan rangka dipilih dari bahan yang memiliki tingkat stiffness tinggi
namun ringan serta dapat mudah dibentuk.
4. Aktuator yang dipilih untuk skala robot lengan yang identik atau lebih
besar menggunakan motor jenis lain yang durabilitasnya lebih tinggi dan
antara variabel torsi dengan keakuratan sudutnya berimbang.
5. Dilakukan analisis lanjutan dari inverse kinematics berupa analisis gerakan
differensial dari manipulator, yaitu trajectory planning, dan joint
velocities tiap sendi.
6. Dilakukan analisis lanjutan dari analisis gerakan differensial manipulator
berupa pemodelan dinamis untuk mengatasi problem keseimbangan links
dan sendi-sendi dengan memberi input torsi aktuator melalui persamaan
respon dinamis.

