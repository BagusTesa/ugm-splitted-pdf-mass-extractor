This study is focused on the analysis of elements and matrix parameters, known as matrix
characterization. For this necessity, chemical characteristics of input to output materials from 8
biogas plants in Lower Saxony, Germany, were investigated. Due to the diversity of the input
materials, 3 types of manure, e.g., pig, bovine and chicken dry manure taken from several
biogas plants in combination with maize silages were analyzed. Moreover, fermenter
materials, post-fermenter materials as well as fermentation residues as the output materials of
biogas plants were studied to complete the investigation purpose. In the elements analysis, 4
digestion were compared to determine the most optimum condition, e.g., aqua regia digestion,
aqua regia microwave assisted digestion, nitric acid microwave assisted digestion and nitric
acid/hydrogen peroxide microwave assisted digestion. The measurements were performed
using ICP-OES instrument. To confirm the reliability of the ICP-OES results, some samples
were measured using more sensitive instrument such as ICP-MS. Corresponding to the
element analysis, some matrix parameters were analyzed in this study, e.g., dry substance,
pH, total nitrogen content, ammonium nitrogen content and total organic carbon content. For
both elements and matrix parameters analysis, the samples were analyzed in duplicates. In
order to check for the variability of the sampling period, the samples of biogas plant (BGP) 3
which were taken from autumn 2012 to spring 2013 were analyzed. Meanwhile, to check for
the variability of the sampling techniques, the samples of biogas plant (BGP) 3 and 8 were
analyzed in 4 replicates.
In this study, storages and animal production types did not relevantly influence the basic
parameters and element concentrations. Except for dry substance, basic parameters and
element contents were not influenced by the season. In element analysis, different elements
gave different responses to the digestion techniques. However, most of elements, e.g., CaO,
K2O, Na, Mn, S and Zn, gave the highest response to microwave assisted aqua regia
digestion. Both ICP-OES and ICP-MS gave relatively similar results for input and output
materials of biogas plants. Basic parameters and macronutrient contents in every sample
matrix under study were relatively similar with the values which were reported by other
authors. Micronutrient contents for Co, Mo, Ni, Se and Mn were below the optimum
concentration which required in biogas reactors. Cu and Zn concentrations, however, were
higher than threshold values. In addition, Ni contents were below the threshold value of
fertilizers. The pollutant contents, e.g., Cd, Pb, Cr and U, were higher in pig manures than in
fermentation residues and higher in winter period than in fall period. However, the
concentration were below the threshold values. Hence, a risk of soil contamination cannot be
derived from these results.

