Pesatnya pertumbuhan pasar pada sektor telekomunikasi seluler semakin
terlihat dengan banyaknya jumlah pelanggan yang ada pada setiap para penyedia jasa
telepon seluler. Model bisnis baru, inovatif produk, dan layanan yang lebih baik
menjadi daya tarik penyedia jasa jaringan seluler terhadap pasar. Pada umumnya
terlihat jelas cara untuk memperbanyak pengguna jasa adalah dengan mendapatkan
pelanggan baru, namun di samping itu perlu adanya tindakan untuk mempertahankan
pelanggan lama. Menurut penelitian tahun 2004, mendapatkan pelanggan baru jauh
lebih sulit dan lebih mahal daripada mempertahankan pelanggan (Brown.K, 2004).
Medapatkan pelanggan baru memerlukan langkah yang lebih atraktif karena
terkadang penyedia jasa jaringan telepon selular harus menarik pelanggan yang
mungkin berasal dari provider lain.
Pada situasi ini cara lain yang dilakukan perusahaan penyedia jasa jaringan
telekomunikasi seluler adalah dengan mempertahankan pelanggan lama yang telah
ada. Disinilah konsep customers churn classification digunakan, perusahaan
mengklasifikasikan pelanggan mana saja yang mempunyai kemungkinan untuk
menghentikan penggunaan jasa dari perusahaan tersebut atau pindah menggunakan
jasa provider yang lainnya. Klasifikasi dari konsep ini dapat dilakukan dengan
memperhatikan informasi individual pelanggan, kebiasaan pelanggan, serta  catatan
penggunaan jasa telepon selular. Untuk mengoptimumkan biaya dan keefektivitasan
metode, maka customer churn prediction harus menghasilkan hasil seakurat
mungkin. Kegunaannya adalah agar langkah mempertahankan pelanggan yang
hendak berpindah benar-benar tepat sasaran dan terlebih lagi dapat menjaga
pelanggan yang potensial agar tetap menggunakan jasa perusahaan.
Dalam skripsi ini akan dibahas penggunaan metode Decision Tree (Pohon
Keputusan) C4.5 salah satu jenis teknik Data Mining untuk melakukan klasifikasi
customer churn. Dengan menggunakan algoritma C4.5 diharapkan prediksi
pelanggan yang akan berpindah jauh lebih akurat dalam hal pengklasifikasian dan
model prengklasifikasian ini dapat digunakan untuk dataset untuk pelanggan masa
depan. Diharapkan klasifikasi ini dapat membantu perusahaan untuk memilih
pelanggan mana yang ingin dipertahankan.

Berdasarkan latar belakang masalah di atas dapat dirumuskan hal-hal sebagai
berikut:
1. Bagaimana cara kerja metode algoritma C4.5 melakukan klasifikasi pelanggan
yang churn?
2. Apakah teknik metode algoritma C4.5 bisa memodelkan bentuk klasifikasi
pelanggan yang churn  pada data studi kasus?
3. Bagaimana keakurasian pengklasifikasian pelanggan yang churn dari dataset
yang ada?

Adapun batasan-batasan masalah pada penelitian tugas akhir ini, adalah:
1. Software yang digunakan pada penelitian tugas akhir ini adalah menggunakan
software open source WEKA.
2. Data yang digunakan merupakan data bawaan dari software Clementine
3. Analisis ini hanya akan melakukan klasifikasi apakah pelanggan akan churn
atau tidak.

Tujuan utama yang ingin dicapai adalah penulis dapat menunjukkan bahwa
metode algoritma Pohon Keputusan C4.5 dapat digunakan dalam customers churn
classification pada masalah perpindahan pelanggan pada perusahaan penyedia jasa
telepon selular dan menghasilkan keakuratan pengklasifikasian yang tinggi.

Manfaat yang diharapkan diperoleh dari penelitian ini adalah:
1. Menunjukkan bahwa teknik metode algoritma pohon keputusan C4.5
dapat digunakan dalam customers churn classification pada perusahaan
provider telekomunikasi seluler.
2. Memberi informasi apakah customer churn itu sebenarnya, dan
menjelaskan bahwa dengan metode algoritma C4.5, dapat menjadi salah
satu alternatif alat klasifikasi yang akurat.

Customer churn pada masa ini termasuk salah satu topik hangat untuk
dibicarakan bagaimana solusi yang tepat untuk melakukan klasifikasi atupun prediksi
model yang akurat. Di satu pihak apabila kita ingin melakukan klasifikasi yang
akurat, salah satu jenis pengklasifikasiannya adalah Algoritma Pohon Keputusan
C4.5.
Saptarini (2012) pada tesisnya membahas algoritma C4.5 untuk menjadi alat
klasifikasi talenta karyawan digabungkan dengan logika fuzzy agar hasil
pengklasifikasiannya lebih baik. Data yang digunakan adalah data studi kasus
Politeknik Negeri Bali di provinsi Bali. Kesimpulan akhir dari penelitian ini
mengatakan bahwa C4.5 merupakan alat pengklasifikasian berbentuk pohon yang
mudah dipahami oleh manusia dengan tingkat akurasi yang tinggi.
Abdul (2012) menulis tesis tentang rancang bangun aplikasi evakuasi rasio
anggaran menggunakan algoritma C4.5. Dalam tesis ini dijelaskan bahwa C4.5
memiliki kelebihan memiliki nilai ketelitian yang baik serta dapat membuat hasil
keputusan yang kompleks menjadi lebih simple dan spesifik dilihat dari output yang
diberikan.
Fadilla (2012) membahas tentang credit scoring menggunakan metode hybrid
kombinasi K-means cluster dan algoritma C4.5. penelitian ini menggunakan data
sekunder German Credit Dataset bernama credit-g.arff yang diperoleh dari UCI
(University of Claifornia, Irvine)Machine Learning Reporsitory. Dari hasil analisis
yang didapat, menyimpulkan bahwa metode hybrid dapat menyempurnakan metode
yang telah digunakan untuk menganalisis credit scoring. Kredit scoring dalam data
mining biasanya hanya menggunakan satu jenis alat supervised atau unsupervised.
Namun, dengan adanya eksperimen ini telah membuktikan keduanya bisa
digabungkan.

Metode yang digunakan dalam penulisan tugas akhir ini lebih kepada studi
literatur, seperti literatur dari buku-buku, jurnal-jurnal ilmiah maupun media lain
seperti internet serta bahan pendukung lainnya yang dapat penulis gunakan sebagai
referensi dalam penulisan tugas akhir ini. Penulis akan menyelesaikan penelitian ini
menggunakan bantuan software WEKA 3.4.12 dan Microsoft Excel 2010. Data yang
digunakan dalam penelitian ini adalah data bawaan software Clementine.

Sistematika penulisan yang digunakan dalam penyusunan tugas akhir ini
adalah sebagai berikut:
BAB I  Pendahuluan
Bab ini menjelaskan tentang latar belakang dibuatnya skripsi ini, perumusan
masalah, batasan masalah, tujuan penelitian, manfaat penelitian, serta tinjauan
pustaka yang digunakan sebagai bahan referensi dalam penulisan tugas akhir,
metode penelitian yang digunakan, serta sistematika penulisan tugas akhir.
Bab ini membahas teori-teori yang menjadi landasan pada topik yang dibahas
pada tugas akhir, yaitu mengenai customer churn pada perusahaan jaringan
telepon baik secara dasar maupun umum yang akan digunakan lebih lanjut di
bab-bab selanjutnya.
Bab ini akan membahas tentang penggunaan metode algoritma Pohon
Keputusan C4.5 yang akan digunakan untuk mengklasifikasikan customer
churn.
Bab ini membahas tentang data yang digunakan sebagai contoh  kasus customer
churn. Pada bab ini akan dibahas bagaimana metode algoritma C4.5 akan
melakukan klasifikasi yang akurat yang hasilnya mengidentifikasi apakah
pelanggan akan berpindah atau tidak.
BAB V Penutup
Bab ini berisi tentang kesimpulan yang telah diperoleh, pemecahan masalah
serta saran yang diberikan akibat kekurangan maupun kelebihan dari hasil
penelitian.

