Transisi pola hidup masyarakat tradisional ke masyarakat modern berdampak
pada pola konsumsi makanan.Masyarakat modern yang hidup serba praktis sulit
untuk menghindari masakan cepat saji (fast food). Fast food memiliki beberapa
kelebihan antara lain adalah penyajian makananya yang cepat sehingga tidak perlu
menunggu waktu lama untuk dihidangkan kapan dan dimana saja (Irianto, 2007).
Dengan adanya transisi pola hidup saja tetapi transisi ekonomi juga, berpengaruh
terhadap pola konsumsi dan gaya hidup masyarakat maka berkembang pula
beberapa makanan fast food salah satunya adalah burger.
Menurut survei yang dilakukan oleh AC Nilsen bahwa 69% masyarakat kota
besar di Indonesia mengkonsumsi fast food yaitu 33% menyatakan makan siang
sebagai waktu yang tepat untuk makan direstoran fast food, 25% menyatakan untuk
makan malam, 9% untuk makanan selingan dan 2% menyatakan untuk makan pagi
(Nielsen, 2008). Hal tersebut diperkirakan akan berkembang seiring dengan
meningkatnya tingkat makanan fast food di indonesia.
Adapun Survei yang dilakukan oleh SurveyOne mengenai kebiasaan makan
direstoran atau fast food. Survei yang melibatkan 150 responden pelajar dan
mahasiswa yang berdomisili dijakarta. Hasil survei dilakukan pada awal tahun 2007
menunjukkan bahwa dominasi restoran siap saji (fast food) sangat kuat (SurveyOne,
2007).
Perangkat bergerak adalah sebuah perangkat mobile yang menyediakan
kemampuan komputasi dan konektivitas yang lebih maju daripada perangkat
mobile dasar. Perangkat mobile tersebut berjalan dengan sistem operasi dan dapat
ditambahkan berbagai aplikasi add-on (Litchfield, 2010). Penambahan aplikasi
tambahan dapat dilakukan sesuai dengan keinginan pengguna. Smartphone
merupakan teknologi yang mudah untuk dibawa kemana dan kapan saja menjadi
alasan  pengembangan aplikasi layanan berbasis lokasi pengguna. Sebuah
perangkat memiliki sistem operasi yang bertujuan  menjalankan sistem pada
perangkat mobile tersebut, salah satu sistem perangkat mobile yang terkenal pada
saat ini adalah perangkat mobile dengan menggunakan sistem operasi google
Android adalah sistem operasi yang dipublikasikan oleh google pada tahun
2009, yang sering disebut sebagai google�s mobile android. Kemudian seiring
dengan perkembangan sistem operasi tersebut dan sampai sekarang sering disebut
sebagai Android (Google, 2009). Pembangunan Aplikasi mobile android
menggunakan java. Kemudian kode yang ditulis tersebut dapat mengontrol
perangkat mobile melalui library yang disediakan oleh google. Bagi para
pengembang yang mampu membuat suatu aplikasi berbasis Android, pengembang
aplikasi dapat mengirim aplikasinya kemudian google memverifikasi aplikasi
tersebut. Setiap aplikasi Android nantinya akan ditanamkan pada suatu smartphone
dengan vendor tertentu sehingga memiliki pemetaan sistem ke situs yang
disediakan khusus oleh google untuk menjalankan aplikasi Android.
Berdasarkan survey yang dilakukan Gartner, pengguna perangkat mobile
berbasis Android pada akhir 2010 sudah mencapai 67.224.500 unit, apabila
dibandingkan dengan pengguna Android di tahun 2009 baru 6.784.000 unit jika
dihitung dalam jangla waktu satu tahun pertumbuhan pengguna Android mencapai
888,88%. Bukan hanya dari sisi penjualan, pertumbuhan pengembang aplikasi
android juga mengalami peningkatan 861,5% dibanding tahun 2009 (Gartner,
2011). Kenaikan ini didukung dengan sifat Android yang open source ditambah
dengan adanya Google Market sehingga tidak menutup kemungkinan untuk
kedepannya pengguna perangkat mobile Android akan mengingkat.
Permasalahan yang sering dihadapi dalam melakukan pemesanan burger
adalah kurangnya informasi mengenai keberadaan outlet burger disekitar konsumen
dan kemudahan dalam melakukan pemesanan burger. Sehingga aplikasi ini
diharapkan mampu memudahkan konsumen dalam melakukan pencarian outlet
burger dan melakukan pemesanan burger.
Dengan didukung oleh perangkat bergerak yang dibekali dengan sistem
operasi dari google Android serta dengan adanya permasalahan yang sering
dihadapi dalam melakukan pemesanan burger, maka perlu dibangun suatu aplikasi
mobile pemesanan burger berbasis platform android. Diharapkan dengan
pembuatan aplikasi ini para penikmat makanan fast food khususnya burger dapat
menjadi alternatif aplikasi pemesanan burger dan sebagai sarana informasi
mengenai outlet burger yang tersebar di kota Yogyakarta.
Berdasarkan latar belakang permasalahan tersebut, dapat dirumuskan
permasalahan sebagai berikut :
1. Bagaimana merancang dan mengimplementasikan aplikasi mobile android
yang dapat melakukan pemesanan burger
2. Bagaimana menampilkan informasi outlet burger terdekat
3. Bagaimana menampilkan rute menuju outlet burger
Dalam penelitian ini pembahasan dibatasi dengan beberapa hal berikut :
1. Aplikasi diimplementasikan pada smartphone berbasis Android 4.1.2
2. Aplikasi yang diimplementasikan hanya sebagai prototype.
3. Peta yang digunakan pada aplikasi ini diambil secara real-time dari
http://maps.google.com.
4. Lokasi layanan sistem hanya pada daerah Kota Yogyakarta.
Penelitian ini bertujuan mengembangkan sebuah aplikasi layanan yang dapat
memudahkan pembeli dalam mencari outlet burger terdekat dan layanan delivery
order dengan memanfaatkan teknologi Location Based Service dalam mengirim
titik lokasi koordinat pembeli dan koordinat lokasi outlet burger terdekat dengan
memanfaatkan peta google maps api untuk menampilkan titik lokasi koordinat
didalam aplikasi serta menampilkan rute terdekat untuk menuju lokasi outlet burger
dari posisi anda berada.
Dengan adanya penelitian ini diharapkan :
1. Dapat mengetahui lokasi outlet burger terdekat secara detail berdasarkan titik
koordinat.
2. Mempermudah navigasi bagi pembeli dalam mencari lokasi outlet burger
terdekat.
1.6 Metode Penelitian
Metode yang digunakan dalam penelitian ini adalah sebagai berikut :

a. Membaca dan mempelajari buku, ebook, jurnal, tugas akhir dan sumber
� sumber tertulis lainnya yang berhubungan dengan android, sistem
location based service, dan delivery order sebagai acuan penelitian.
b. Mencari informasi pendukung lainnnya menggunakan internet sebagai
salah satu sumber informasi.

Analisis kebutuhan dengan pengumpulan informasi yang dibutuhkan untuk
membangun sistem. Seperti spesifikasi sistem, kriteria sistem yang akan
dipakai dalam menentukan lokasi, rute menuju lokasi serta melakukan
pengolahan data sesuai kebutuhan.

Berdasarkan hasil analisis yang dilakukan, dilakukan perancangan sistem,
rancangan sistem akan menjadi cetak biru sebagai landasan implementasi
sistem.

Merupakan tahap mengubah rancangan sistem menjadi kode program
komputer. Aplikasi yang dibuat akan menggunakan arsitertur Java program,
dimana J2ME merupakan alat utama dalam membuat aplikasi.

Pada tahap ini sistem yang telah dibangun akan dilakukan pengujian, apakah
sistem yang dibuat dapat berjalan dengan baik dan sesuai dengan yang
diharapkan.

Merupakan tahap terakhir pada penelitian ini yaitu menuliskan seluruh hasil
dari penelitian yang telah dilakukan.
Sistemantika penulisan yang digunakan dalam penyusunan tugas akhir ini adalah
sebagai berikut ;
Bab ini menjelaskan latar belakang, perumusan masalah, batasan masalah,
tujuan penelitian, manfaat penelitian, tinjauan pustaka yang digunakan sebagai
bahan referensi dalam penulisan tugas akhir, metode peneitian yang digunakan, dan
sistematika penulisan tugas akhir
Bab ini menjelaskan penelitian � penelitian sebelumnya yang  akan dibahas
dalam penelitian ini.
Bab ini membahas materi dasar pada penelitian ini, yaitu membahas definisi
Aplikasi mobile, Aplikasi web, Location Based Service, konsep dasar Web Service,
JSON, UML, dan arsitektur aplikasi Android.
Bab ini menjelaskan tentang analisis dan desain sistem, termasuk
didalamnya terdapat pembagian aktifitas antara user dalam perangkat mobile,
admin pengelola web pemesanan burger dan operator outlet burger.
Bab ini membahas implementasi sistem secara detail sesuai dengan
rancangan sistem berbasiskan web yang terhubung dengan perangkat mobile
Bab ini membahas tentang pengujian dan pembahasan dari implementasi
perangkat lunak yang telah dibuat oleh peneliti dan dapat berjalan di Android sesuai
dengan harapan.
Bab ini memaparkan tentang kesimpulan dan saran yang diambil dari
penelitian yang telah dilakukan serta memberikan saran untuk pengembangan
selajutnya.

