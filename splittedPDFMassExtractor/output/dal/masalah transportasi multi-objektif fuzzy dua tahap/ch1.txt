1.1 Latar Belakang dan Permasalahan
Pendistribusian suatu barang dalam suatu proses produksi dari suatu tempat
ke tempat lain merupakan salah satu pengaplikasian matematika dalam dunia
nyata. Pendistribusian barang disini biasanya terdiri dari dua hal. Yang pertama
adalah berupa barang mentah yang berasal dari beberapa sumber yang akan
dikirim kepada pabrik-pabrik atau tempat pengolahan barang. Yang kedua adalah
barang-barang jadi yang berasal dari beberapa produsen  yang akan dikirim
menuju gudang-gudang distributor untuk kemudian dijual kepada konsumen.
Dalam pelaksanaannya tentunya terdapat beberapa hal yang ingin dicapai.
Diantaranya adalah barang yang dikirimkan memenuhi permintaan minimal dari
pemesan barang (destination) sekaligus barang yang diminta tersedia pada
penyedia barang (origin), biaya pengiriman yang serendah mungkin, dan waktu
distribusi yang sesingkat mungkin. Pengoptimalan pendistribusian barang agar
tujuan-tujuan diatas tercapai biasanya disebut masalah transportasi multi-objektif.
Pada kondisi tertentu, masalah daya tampung menjadi hal yang terkadang
terjadi pada masalah transportasi. Dalam hal ini yang dimaksud masalah daya
tampung adalah ketidakmampuan penerima barang dalam menampung barang
yang didistribusikan dari sumber (origin) menuju pemesan barang (destination).
Masalah ini dapat diselesaikan dengan cara pengiriman dua tahap. Caranya adalah
mengirim barang kepada pemesan (destination) sesuai dengan kapasitas tampung
maksimal penerima, setelah pengiriman pertama dilakukan, pengirim (origin)
menunggu barang pada tahap pertama terpakai atau terkonsumsi baru kemudian
mengirim sisanya pada tahap kedua. Hal ini tentunya menimbulkan masalah baru
yaitu biaya pengiriman menjadi lebih mahal dan waktu yang diperlukan semakin
lama.
Dalam kaitannya dengan asal dari barang yang akan didistribusikan (origin),
jumlah ketersediaan barang yang akan ditawarkan tidak selalu dalam bentuk yang
jelas. Ada faktor-faktor yang mempengaruhi kejelasan jumlah barang yang akan
didistribusikan. Dari segi bahan mentah misalnya, ketidakpastian ketersediaan di
alam bisa menjadi salah satu faktor yang mempengaruhi ketidakjelasan tersebut.
Dari segi produsen pengolahan barang jadi, faktor-faktor yang mempengaruhi bisa
meliputi kerusakan mesin, karyawan yeng berhalangan, ataupun kerusakan barang
pada saat proses produksi. Dalam kaitannya dengan penerima barang
(destination), permintaan juga tidak selalu berupa angka yang jelas. Faktor yang
mempengaruhi dapat berasal dari penerima itu sendiri yaitu penerima mungkin
menginginkan jumlah barang yang diminta dalam bentuk interval. Dengan adanya
ketidakpastian jumlah dalam permintaan dan penawaran, bilangan fuzzy menjadi
alternatif yang sangat sesuai untuk menyatakan ketidakpastian tersebut.
Berdasarkan hal-hal diatas, maka pada penelitian ini akan dibahas masalah
transportasi fuzzy dua tahap yang fungsi tujuannya bersifat multi-objektif dengan
permintaan dan penawaran adalah bilangan fuzzy trapesium. Pada tesis ini akan
ditemukan solusi pareto terbaik diantara himpunan solusi yang layak (feasible)
untuk masalah transportasi multi-objektif fuzzy dua tahap.
1.2 Tujuan dan Manfaat Penelitian
Tujuan dari penelitian ini adalah menemukan solusi pareto terbaik diantara
himpunan solusi feasible untuk masalah transportasi multi-objektif fuzzy dua
tahap dengan permintaan dan penawaran adalah bilangan fuzzy trapesium.
Manfaat dari penelitian ini adalah memberikan gambaran tentang masalah
transportasi khususnya masalah transportasi multi-objektif fuzzy dua tahap dan
diharapkan dapat membantu pembaca dalam memecahkan masalah yang serupa
dalam kehidupan nyata.
Jurnal utama yang dijadikan acuan dalam penulisan tesis ini adalah Multi-
objective Two Stage Fuzzy Transportation Problem yang ditulis oleh Ritha dan
Vinotha (2009) yang membahas tentang masalah transportasi multi-objektif fuzzy
dua tahap dengan permintaan dan penawaran adalah bilangan fuzzy trapesium.
Untuk mempelajari masalah transportasi multi-objektif fuzzy dua tahap
dengan permintaan dan penawaran adalah bilangan fuzzy diperlukan pengetahuan
dasar tentang himpunan konveks, program linear multi-objektif, masalah
transportasi, himpunan fuzzy, bilangan fuzzy, keputusan fuzzy, dan program  linear
multi-objektif  fuzzy sebagai landasan teori. Mital (1976) memberikan pengertian
tentang himpinan konveks, sedangkan program linear multi-objektif diberikan
dalam buku Sakawa (1993). Dalam pembahasan lebih lanjut, salah satu masalah
khusus dari program linear adalah masalah transportasi. Pembahasan tentang
masalah transportasi terutama masalah transportasi multi-objektif fuzzy dikaji
dalam tesis Rani (2010).
Dalam kehidupan nyata lebih sering ditemukan masalah yang bersifat samar
(fuzzy) daripada masalah yang jelas (crisp). Sebuah terobosan yang sangat penting
dalam ilmu pengetahuan, Zadeh (1965) mendefinisikan suatu himpunan fuzzy
yang membantu dalam memecahkan masalah tersebut. Kemudian dalam buku
Sakawa (1993) diberikan beberapa definisi pendukung dalam memahami
himpunan fuzzy. Bector dan Chandra (2005) memberikan beberapa definisi
tentang bilangan fuzzy, bilangan fuzzy trapesium, dan bilangan fuzzy segitiga.
Demikian juga halnya dalam masalah transportasi, tidak semua informasi
yang disajikan dalam bentuk data yang akurat, oleh karena itu penggunaan
bilangan fuzzy merupakan solusi yang sangat baik dalam menemukan solusi
optimal dari masalah transportasi. Selanjutnya Bellman dan Zadeh (1970) dalam
Sakawa (1993) memperkenalkan tentang fuzzy decision yaitu irisan dari fuzzy goal
dan fuzzy constraint untuk menentukan keputusan maksimum yang akan diambil
pengambil keputusan sehingga dapat dicari solusi optimal untuk masalah program
linear multi-objektif fuzzy.
Metode penelitian yang digunakan dalam penulisan tesis ini adalah studi
literatur. Langkah awal dari penelitian ini adalah menentukan bilangan supply dan
demand yaitu pertama membuat interval pada masing-masing bilangan supply dan
demand berdasarkan ?-levelnya, yang kedua adalah pengambil keputusan memilih
masing-masing satu nilai di dalam interval supply dan demand secara subjektif.
Langkah selanjutnya adalah mengkonstruksi masalah transportasi dua tahap.
Pengambil keputusan secara subjektif membagi nilai dari masing-masing supply
dan demand menjadi dua bagian. Untuk bagian pertama dibuat masalah
transportasi multi-objektif fuzzy tahap pertama dan bagian kedua dibuat masalah
transportasi multi-objektif fuzzy tahap kedua.
Pada masalah transportasi multi-objektif fuzzy tahap pertama dicari nilai
minimum dari masing-masing fungsi objektif kemudian mengevaluasi semua nilai
minimum pada semua fungsi objektif untuk mencari nilai maksimum dan
minimum dari masing-masing fungsi objektif. Selanjutnya dibentuk fuzzy goal
dan fuzzy constraint dengan membuat fungsi keanggotaan linear dari masing-
masing fungsi objektif. Langkah selanjutnya adalah membuat keputusan fuzzy dan
maksimum keputusan fuzzy. Kemudian mentransformasikan fungsi-fungsi objektif
yang sudah difuzzykan dari bentuk maksimum keputusan fuzzy menjadi masalah
program linear single objektif deterministik sehingga dapat diselesaikan dengan
metode simpleks atau bantuan software komputer.
Langkah pada masalah transportasi multi-objektif fuzzy tahap kedua analog
dengan langkah pada tahap pertama. Sehingga langkah terakhir adalah
menjumlahkan nilai optimal dari masalah transportasi multi-objektif fuzzy tahap
pertama dan tahap kedua.
Tesis ini terdiri dari 4 bab yaitu : BAB I Pendahuluan, BAB II Landasan
Teori, BAB III Pembahasan dan BAB IV Penutup.
BAB I memuat latar belakang dan permasalahan, tujuan dan manfaat
penelitian, tinjauan pustaka, metode penelitian, sertasistematika penelitian.
BAB II memuat landasan teori yang digunakan dalam penelitian ini yaitu
himpunan konveks, program linear multi-objektif, masalah transportasi, himpunan
fuzzy, bilangan fuzzy, keputusan fuzzy, dan program linear multi-objektif fuzzy.
BAB III memuat masalah transportasi multi-objektif fuzzy dua tahap serta
aplikasi pada masalah transportasi.
BAB IV berisi kesimpulan dan saran. Kesimpulan berisi rangkuman dari
penelitian, sedangkan saran berisi masukan dari peneliti kepada pembaca untuk
melakukan sesuatu yang berkaitan dengan penelitian ini.

