In this research, the innovation development of smart home,
instrumentation system and control room conditions with fuzzy logic control. This
system controls the temperature and humidity of the room and set the levels of
indoor air pollution. This system combined with automated fire safety system that
can detect the source of the fire and fighters automatically.
The system is designed using the AT Mega 328 Arduino IDE-based as
microcontroller, XBee transceiver as a communication system, DHT11 sensor for
detecting the temperature and humidity of the room, TGS 2600 sensor for
detecting indoor air pollution, sensor phototransistor as a infrared detector ignite
by fire and a sensor MPX5700AP as fire extinguisher pressure detection. This
system will use the value of the temperature, air pollution, and exposure of
infrared to determine the presence of fire. In addition, the system uses the value of
the temperature, humidity, and air pollution to be further processed in the
processing for the fuzzy control of DC fan as a regulator of room conditions.
This system has a response time for each 1 � C drop in temperature is
6206.75 ms, every 1% RH increase in humidity is 2361.50 ms, and every 1 ppm
decrease in CO gas is 1529.72 ms, whereas when not using the fuzzy logic control
response time for each 1 � C drop in temperature is 7931.15 ms, every 1% RH
increase in moisture is 4020.33 ms and every 1 ppm decrease in CO gas is
1697.60 ms. Automatic fire suppression has a response time of approximately 30
seconds immediately after the source of flame. In addition, data communication
can send and receive data without any loss of data at a distance of approximately
30 meters. Interfacing in LabView can be displayed properly and data logging can
be done every single second.
Keyword:  fuzzy control, arduino, XBee, LabView

