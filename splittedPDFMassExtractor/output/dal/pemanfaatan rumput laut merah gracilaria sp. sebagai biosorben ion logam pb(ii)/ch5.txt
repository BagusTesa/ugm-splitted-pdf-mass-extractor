Berdasarkan penelitian yang telah dilakukan dapat diambil beberapa
kesimpulan, yaitu:
a. Hasil karakterisasi FTIR terhadap bead rumput laut Gracilaria sp.
menunjukkan gugus-gugus aktif yang dapat mengadsorp ion logam Pb(II)
yaitu gugus O-H, dan gugus sulfat.
b. Kemampuan 0,1 g biosorben rumput laut Gracilaria sp. untuk mengadsorp
ion logam Pb(II) pada pH 4, selama 15 menit dengan konsentrasi adsorbat
50 ppm.
c. Pola adsorpsi mengikuti model kinetika adsorpsi orde satu Santosa-
Muzakky dan adsorpsi isotermal Freundlich dengan linieritas grafik
sebesar 0,976, serta energi adsorpsi yang didapat yaitu sebesar 12473,4
kJ/mol. Kapasitas adsorpsi optimum mencapai 656,25 ?g/g.
a. Perlu dilakukan penelitian lebih lanjut dengan menentukan ukuran pori,
jumlah dan kapasitas desorpsinya.
b. Perlu dilakukan penelitian lebih lanjut tentang pengembangan penggunaan
biosorben rumput laut yang digunakan.

