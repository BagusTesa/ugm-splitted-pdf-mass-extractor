The hydrocracking  of coal tar  have been done using actived natural zeolite
catalysts modified by nickel and molybdenum, then called as ZAA, Ni-Mo/ZAA and
Ni-Mo/ZAA after wash by EDTA (Ni-Mo/ZAA(E)). The Modification of catalyst
conducted in this research was activation of zeolite by 6 N HCl, wet impregnation of
Ni-Mo metal and leaching the Ni-Mo from zeolite surfaces with EDTA solution.
After activation, then the catalyts were characterized using X-ray diffractometry and
BET/Isothermal adsorption methods.  The parameters of catalyst characterized to
determined acid site number, the ratio of Si/Al, crystallinity, Mo and Ni metal
content, specific surface area, total pore volume and pore average diameter. The
hydrocracking of coal tar was carried out at the optimum temperature (450 �C) by
flowing  H2 gas (flow rate of 20 mL/min) on the feed/liquid coal tar (ratio of
feed:catalyst was 10:1). The resulting liquid product was analyzed by gas
chromatography and gas chromatography-mass spectrometry methods.
In this research, catalytic hydrocracking with ZAA catalyst had the highest
conversion of liquid products, which were 40.51% (w/w) compared to Ni-Mo/ZAA
catalyst gave 28.61% and Ni-Mo/ZAA(E) gives 28.06% (w/w). The highest gasoline
fractions (67.63% (w/w)) produced by using Ni-Mo/ZAA catalyst and the highest
diesel fractions produced over ZAA catalyst which was 36.61% (w/w) and the highest
total conversion on the product with Ni-Mo/ZAA(E) catalyst produced was 50.05%
(w/w). The best catalysts for hydrocracking coal tar was ZAA. Product was analyzed
with GC-MS result on the 9th highest peak showed that the hydrocracking products
resulted over Ni-Mo/ZAA mostly were phenol and its derivatives.
Keyword : hydrocracking, catalytic, natural zeolite, coal tar.

