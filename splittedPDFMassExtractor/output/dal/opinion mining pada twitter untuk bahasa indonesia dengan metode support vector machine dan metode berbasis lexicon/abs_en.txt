Opinions in the decision-making process are often used as a reference.
Nowadays, with the internet and social media there are a lot of people often argue,
complain or criticize about something. So as to know what others think of
something that we do not need to ask them one by one. We can develop a method
that can search and analyze text on social media which produces output in the
form an opinion about something.
The method to obtain information in the form of opinions contained in the
text is an "Opinion Mining" or "Sentiment Analysis". In this research, using a
combined Unsupervised method based on Indonesian Lexicon and Supervised
based on Support Vector Machine.
Results obtained in the form of sentiment is negative polarity, positive, or
neutral. From the test results obtained in this study 77,7% accuracy.
Keyword : opinion mining, sentiment analysis, lexicon, support vector machine

