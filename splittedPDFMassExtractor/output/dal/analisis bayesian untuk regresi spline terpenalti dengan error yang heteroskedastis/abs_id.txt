Analisis Bayesian untuk Regresi Spline Terpenalti
dengan Error yang Heteroskedastis
Pada tesis ini akan dibahas mengenai analisis Bayesian untuk regresi
spline terpenalti dengan error yang heteroskedastis. Parameter pada model regresi
spline terpenalti dengan error yang heteroskedastis dapat diestimasi dengan
menggunakan pendekatan Gibbs sampling dan Metropolis Hasting. Analisis
Bayesian untuk regresi spline terpenalti dengan error yang heteroskedastis akan
diaplikasikan untuk menganalisis hubungan antara nilai tukar Rupiah dengan
Indeks Harga Saham Gabungan (IHSG). Hasil analisis data menggunakan analisis
Bayesian untuk regresi spline terpenalti dengan error yang heteroskedastis
dibandingkan dengan hasil analisis Bayesian untuk regresi spline terpenalti
dengan error yang homoskedastis dan regresi linear sederhana. Diperoleh
kesimpulan bahwa hasil analisis Bayesian untuk regresi spline terpenalti dengan
error yang heteroskedastis menghasilkan nilai MSE terkecil dan R
terbesar.
Kata kunci: regresi spline terpenalti, heteroskedastisitas error, Gibbs sampling,

