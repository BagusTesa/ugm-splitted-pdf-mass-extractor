Berdasarkan hasil penelitian yang telaha dilakukan, dapat diambil beberapa
kesimpulan sebagai berikut
1. Hasil inversi memperlihatkan range nilai impedansi akustik daerah
penelitian berkisar antara 8000 � 17000 ((m/s).(gr/cc)).
2. Pola persebaran batugamping dengan nilai impedansi akustik yang rendah
( 8000 � 10000 ((m/s) . (gr/cc))) berorientasi timur laut � barat daya.
3. Pola persebaran batugamping dengan nilai impedansi akustik yang tinggi
(15000 � 17000 ((m/s) . (gr/cc)) ) berorientasi timur laut � barat daya.
4. Nilai porositas pada area target antara 15 � 24 %.
5. Pada area penelitian, range frekuensi sesaat antara 10 � 50 Hz.
6. Pada area penelitian, dominasi frekuensi sesaat pada 23 Hz.
7. Hasil atribut spectral decomposition pada frekuensi 23 Hz menunjukan
kesamaan pola dengan peta porositas secara kualitatif.
8. FM-6, FM-T6, FM-T40, FM-T61, FM-T63 direkomendasikan sebagai
sumur injeksi.

Berikut beberapa saran yang mungkin bisa menjadi bahan masukan apabila
ada yang berminat untuk mengembangkan lebih baik studi tentang tema ini
1. Diperlukan data analisa petrofisika guna mengetahui secara lebih detail
area prospek hidrokarbon maupun kandungan fluida reservoar.
2. Dalam penentuan lokasi sumur injeksi sangat disarankan menggunakan
data seismik 4D.
3. Perlu dilakukan konversi data seismik dari domain waktu ke kedalaman
guna mendapatkan hasil yang lebih baik.
4. Dapat digunakan analisa atribut yang lain guna merepresentasikan lebih
baik akan kehadiran hidrokarbon.

