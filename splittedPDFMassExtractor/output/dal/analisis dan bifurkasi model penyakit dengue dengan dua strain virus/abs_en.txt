Dengue fever (DF) and dengue haemorrhagic fever (DHF) are the disease
caused by the dengue virus which is transmitted to the human by infected female
mosquitoes. The disease is endemic in more than 100 countries over the world.
Dengue virus has four distinct serotypes which are closely related to each other
antigenically. A person infected by the dengue virus will never be infected again by
the same serotype, but he looses immunity from the three other serotypes. Infec-
tion with one serotype does not provide cross-protective immunity against to others.
Here we analyze the model of dengue fever with two infections from the different
serotype. In this model, the recovered class of the first infections become the sus-
ceptible of the second infection. We analyze the model of dengue fever with two
infections from the different serotype by linear analysis, then we analyze the bifur-
cation of some parameter. In numerical simulation, we use Runge-Kutta order 4 to
integrate the solution of the system.

