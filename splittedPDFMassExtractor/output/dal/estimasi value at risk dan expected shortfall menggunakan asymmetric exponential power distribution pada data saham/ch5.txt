Berdasarkan pembahasan dalam penelitian ini dapat diambil kesimpulan
sebagai berikut:
1. Asymmetric Exponential Power Distribution (AEPD) merupakan perluasan
dari distribusi eksponensial, dimana memiliki karakteristik heavy tailed serta
parameter tail kiri dan kanan yang berbeda, sehingga membentuk distribusi
yang asimetris.
2. Kejadian khusus dari distribusi AEPD, dimana ketika nilai parameter
1 2 2,  0.5p p ?? ? ? , maka nilai (kurtosis = 3) yang berarti data mengikuti
distribusi normal. Hal ini tidak berlaku untuk ?  yang lebih besar atau lebih
kecil dari 0.5 , walaupun nilai parameter
1 2 2p p? ? .
3. Dalam tataran praktis cukup sulit ditemukan data return Asymmetric
Exponential Power Distribution (AEPD), namun data return yang bersifat
heavy tailed dan asimetris dapat menggunakan pendekatan AEPD  dalam
estimasi VaR dan ES.
Adapun saran-saran yang dapat disampaikan untuk penelitian ini adalah
sebagai berikut:
1. Dalam penelitian ini, penulis membahas model estimasi VaR dan ES
Asymmetric Exponential Power Distribution (AEPD) untuk return aset
tunggal, sehingga dapat dikembangkan penelitian untuk beberapa aset, baik
yang memiliki distribusi sama maupun kombinasi dengan Asymmetric
2. Model estimasi VaR dan ES return Asymmetric Exponential Power
Distribution (AEPD) dapat diperluas dengan return portofolio atau distribusi
lainnya yang bersifat heavy tailed dan asimetris.
.

