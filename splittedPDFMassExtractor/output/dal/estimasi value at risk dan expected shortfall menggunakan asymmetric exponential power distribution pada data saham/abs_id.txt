VaR adalah suatu metode pengukuran resiko yang secara statistik
memperkirakan kerugian maksimum yang mungkin terjadi atas suatu aset pada
jangka waktu tertentu dan pada tingkat kepercayaan tertentu. Namun demikian,
sering kali nilai kerugian melebihi nilai VaR yang diestimasi. VaR tidak dapat
menginformasikan besarnya kerugian di bagian tail loss, sehingga dikenalkan ukuran
risiko yang dapat menjelaskan nilai kerugian tersebut yaitu Expected Shortfall (ES).
ES merupakan rata-rata dari tail loss atau kerugian yang melebihi nilai VaR pada
tingkat kepercayaan tertentu. Karena pada prakteknya data return sering kali tidak
simetris (asimetris) terhadap luasan sebelah kiri dan luasan sebelah kanan, maka
sulit menangkap sifat-sifat fat tail dan skewness pada distribusi return. Oleh sebab
itu, diperlukan distribusi Asymmetric Exponential Power Distribution (AEPD) yang
merupakan perluasan dari distribusi eksponensial, dimana AEPD memiliki
karakteristik heavy tailed serta parameter tail kiri dan kanan yang berbeda, sehingga
distribusi ini dapat menangkap sifat-sifat tersebut.
Value at Risk, Expected Shortfall, Maximum Likelihood Estimator dan Asymmetric

