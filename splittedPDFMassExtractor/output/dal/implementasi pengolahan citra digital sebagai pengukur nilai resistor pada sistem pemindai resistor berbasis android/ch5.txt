Berdasarkan penelitian yang telah dilakukan maka dapat diambil beberapa
kesimpulan sebagai berikut:
1. Telah berhasil diimplementasikan pengolahan citra digital sebagai
pengukur nilai resistor pada sistem pemindai resistor berbasis android.
2. Sistem mampu mendeteksi gelang-gelang warna resistor dengan baik.
3. Kinerja sistem paling baik didapatkan pada kondisi lingkungan dengan
intensitas cahaya ruangan 400 lux hingga 1200 lux dengan jarak antar
kamera dan resistor maksimal 20cm.

Berikut saran  yang dapat dilakukan untuk pengembangan dalam
penelitian selanjutnya yang berkaitan dengan sistem pemindai resistor
berbasis android adalah :
1. Hendaknya proses pembacaan resistor dilakukan secara serentak tidak
mendeteksi satu-persatu gelang warna resistor.
2. Hendaknya dapat diintegrasikan pada device yang memiliki spesifikasi
yang lebih baik.

