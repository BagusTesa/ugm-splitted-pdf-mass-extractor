Zeolit merupakan salah satu mineral yang banyak terkandung di bumi
Indonesia yang pemanfaatannya belum maksimal (Rini dan Anthonius, 2010).
Berdasarkan karakteristik kimia, fisika dan struktural, zeolit cocok untuk
diaplikasikan dalam berbagai bidang seperti adsorpsi kation, pemisahan,
pertukaran ion dan katalisis. Zeolit dengan muatan negatif  merupakan adsorben
yang efisien untuk adsorpsi kation seperti polutan logam berat dengan muatan
positif. Selain itu, penggunaan zeolit alam sebagai adsorben telah banyak diminati
para peneliti, terutama karena sifat penyerapnya memberikan kombinasi
pertukaran ion dan bersifat sebagai saringan molekuler yang juga dapat dengan
mudah dimodifikasi (Cincotti dkk., 2006). Oleh karena itu, zeolit alam Indonesia
apabila dimodifikasi dapat dijadikan salah satu material yang lebih berkualitas dan
berdaya guna tinggi.
Struktur yang khas dari zeolit, yakni hampir sebagian besar merupakan
pori, menyebabkan zeolit memilki luas permukaan yang besar. Keadaan ini dapat
dijelaskan bahwa masing-masing pori dan kanal dalam maupun antar kristal
dianggap berbentuk silinder, maka luas permukaan total zeolit adalah akumulasi
dari luas permukaan (dinding) pori dan kanal-kanal penyusun zeolit. Semakin
banyak jumlah pori yang dimiliki, semakin besar luas permukaan total yang
dimiliki zeolit. Menurut Dyer (1988), luas permukaan internal zeolit dapat
mencapai puluhan bahkan ratusan kali lebih besar dibanding bagian permukaaan
luarnya. Luas permukaan yang besar ini sangat menguntungkan dalam
pemanfaatan zeolit baik sebagai adsorben ataupun sebagai katalis heterogen.
Hal ini yang menyebabkan zeolit mempunyai kapasitas yang tinggi
sebagai adsorben. Pada umumnya zeolit alam masih mengandung pengotor-
pengotor organik dan anorganik yang menutupi porinya. Sehingga untuk
meningkatkan kemampuan daya adsorpsi zeolit alam harus dilakukan aktivasi
terlebih dahulu agar jumlah pori-pori yang terbuka lebih banyak sehingga luas
permukaan pori-pori bertambah . Proses aktivasi zeolit dapat dilakukan dengan 2
cara yaitu secara fisis dan kimia. Aktivasi secara fisis berupa pemanasan zeolit
dengan tujuan untuk menguapkan air yang terperangkap dalam pori-pori kristal
zeolit sehingga luas permukaan pori-pori bertambah. Sedangkan aktivasi secara
kimia dilakukan melalui pengasaman menggunakan asam klorida atau asam nitrat.
Tujuannya untuk menghilangkan pengotor anorganik. Pengasaman ini akan
menyebabkan terjadinya pertukaran kation dengan H+ (Ertan, 2005).
Seiring dengan perkembangan teknologi, penelitian mengenai adsorben
zeolit juga juga semakin berkembang dengan dilakukan modifikasi pada zeolit
alam. Menurut Mockov?iakov� dkk. (2006), zeolit alam dapat dimodifikasi untuk
memperbaiki sifat-sifat fisika dan sifat-sifat kimianya. Salah satu modifikasi yang
dapat dilakukan adalah dengan memberi sifat kemagnetan. Seperti penelitian yang
dilakukan oleh Mockov?iakov�  yaitu zeolit alam yang dimodifikasi dengan
oksida besi  jenis maghemit (?-Fe2O3) dan dapat disimpulkan bahwa
pengembanan maghemit dapat meningkatkan sifat permukaan dan porinya.
Pemberian sifat kemagnetan ini dimaksudkan agar recovery zeolit dari medium
cair setelah proses adsorpsi dapat dilakukan dengan lebih mudah dan cepat, yaitu
dengan menggunakan medan magnet eksternal (Oliveira dkk., 2004). Magnetit
(Fe3O4) merupakan oksida besi yang juga memiliki sifat kemagnetan di samping
maghemit. Magnetit relatif mudah disintesis, di antaranya dengan metode
kopresipitasi (Qu dkk., 1999). Szab� dkk. (2007) juga menyatakan bahwa
magnetit dan komposit-kompositnya sangat baik untuk digunakan sebagai
adsorben logam-logam berat dalam air minum. Fe3O4 magnet memiliki
keuntungan sebagai bahan adsorben komposit karena dapat dengan mudah
dipisahkan oleh medan magnet eksternal.
Dengan semakin pesatnya perkembangan industri di Indonesia dan
semakin ketatnya peraturan mengenai limbah industri serta tuntutan untuk
mewujudkan pembangunan yang berwawasan lingkungan, maka teknologi
pengolahan limbah yang efektif dan efisien menjadi sangat penting.
Perkembangan industri yang semakin pesat selain banyak memberikan dampak
yang positif bagi masyarakat, juga memberikan dampak yang negatif. Dampak
negatif yang dapat muncul seperti dihasilkannya limbah bahan berbahaya yang
berasal dari proses produksi. Salah satu limbah yang berbahaya adalah limbah
logam Cadmium (Cd(II)) yang biasanya berasal dari Air Asam Tambang (Acid
Mine Drainage/ AMD). Air Asam Tambang (AMD) merupakan salah satu
ancaman serius bagi kesehatan manusia, hewan dan sistem ekologi. Hal ini karena
AMD mengandung kontaminan logam berat, seperti Cu2+, Fe3+, Mn2+, Zn2+, Cd2+
dan Pb2+ yang tidak biodegradable dan dengan demikian cenderung menumpuk
dalam hidup organisme, menyebabkan berbagai penyakit dan gangguan (Moreno
dkk.,2001; Myroslav dkk., 2006; Bailey dkk., 1999 dalam Motsi dkk., 2009).
Sumber utama AMD adalah operasi penambangan ,fasilitas plating logam dan
penyamakan kulit (Leinonen dan Letho, 2001). Efek racun kadmium antara lain
menyebabkan tekanan darah tinggi, kerusakan sel darah merah, ginjal, pembuluh
darah dan gangguan akut sistem pernafasan dan pencernaan (Stoeppler, 1992;
Manahan, 1994; Noll, 2002).
Proses pengolahan untuk limbah yang terkontaminasi oleh logam meliputi
pengendapan secara kimia, pertukaran ion, adsorpsi dan ultra-filtrasi. Metode
yang dipilih berdasarkan konsentrasi logam berat dalam larutan dan biaya
pengolahannya (Richardson dan Harker, 2002). Adsorpsi adalah metode yang
populer untuk menghilangkan logam berat dari air limbah (Omer dkk., 2003 dan
Heping dkk., 2006 dalam Motsi dkk., 2009), terutama ketika bahan-bahan alami
yang tersedia dalam jumlah besar atau produk limbah tertentu dari industri atau
kegiatan pertanian mungkin memiliki potensi sebagai bahan penyerap yang murah
(Bailey dkk., 1999). Bahan penyerap yang dimaksudkan seperti fly ash, tanah liat
dan zeolit alam.
1. Melakukan modifikasi magnetik pada zeolit alam dengan mengkaji
pengaruh variasi temperatur dan pH.
2. Mengetahui perbedaan  kinerja  secara kinetika antara  zeolit dan  zeolit-
magnetit dalam mengadsorp logam Cd(II)
3. Mengetahui perbedaan laju recovery  antara kedua adsorben tersebut
setelah aplikasi
Manfaat yang diharapkan dari penelitian ini adalah diperoleh komposit
zeolit-magnetit yang memiliki sifat kemagnetan sehingga dapat dimanfaatkan
sebagai adsorben dengan kinerja yang lebih baik karena dapat lebih mudah dan
cepat dipisahkan dari medium cairnya dengan menggunakan medan magnet
eksternal.

