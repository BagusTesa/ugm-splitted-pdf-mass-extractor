Telah dilakukan modifikasi zeolit alam dengan oksida besi berdasarkan
metode kopresipitasi dengan mengkaji pengaruh temperatur dan pH terhadap jenis
oksida besi yang terbentuk pada komposit.Untuk mengkaji pengaruh temperatur,
dilakukan modifikasi magnetic zeolit alam pada variasi temperature kamar, 50?C
dan 85 ?C pada pH 9. Selanjutnya untuk mengkaji pengaruh pH, dilakukan
modifikasi magnetic zeolit alam pada variasi pH 10, 11 dan 12 dengan temperatur
yang ditentukan dari variasi temperatur sebelumnya. Zeolit-magnetit pada kondisi
temperature dan pH yang optimal diaplikasikan sebagai adsorben Cd(II)
Karakterisasi komposit dilakukan dengan metode difraksi sinar-X, spektrofoto
metri inframerah, spektrofotometri serapan atom dan adsorpsi isoterm.
Hasil penelitian menunjukkan bahwa pada temperatur 85 oC dan pH 11
tercapai kondisi optimal  dalam modifikasi magnetik zeolit alam, dengan karakter
magnetit pada komposit tertinggi. Zeolit-magnetit mampu menyerap  Cd(II)
dengan konstanta laju adsorpsi (k) yang lebih besar dibanding zeolit teraktivasi,
dengan konstanta laju adsorpsi 0,0160 (g/mg menit) pada adsorben zeolit
teraktivasi dan  0,0648 (g/mg menit) pada adsorben zeolit-magnetit dengan
mengikuti kinetika adsorpsi pseudo orde dua Ho. Dengan menggunakan medan
magnet eksternal recovery dsorben zeolit magnetit dapat dilakukan dengan lebih
cepat dibanding adsorben zeolit teraktivasi.
Kata kunci:zeolitalam, zeolit-magnetit, kopresipitasi, Cd(II)

