Analisis fungsional merupakan salah satu cabang matematika analisis yang
pembahasannya cukup kompleks karena mencakup banyak konsep, diantaranya ru-
ang vektor, ruang bernorma, ruang Banach dan ruang inner product. Banyak hal
yang bisa dikaji terkait ruang bernorma antara lain operator linear, kekontinuan,
kekonvergenan, dan orthogonalitas yang akan dibahas lebih lanjut. Di ruang in-
ner product (X , ?., .?), vektor x ? X dikatakan orthogonal terhadap y ? X jika
?x, y? = 0. Inner product dapat membangkitkan suatu norma, tetapi tidak semua
ruang bernorma, normanya dibangkitkan oleh inner product. Konsep orthogonali-
tas di ruang inner product real dapat diperumum untuk semua ruang bernorma atas
lapangan real termasuk ruang bernorma yang normanya tidak dibangkitkan oleh
inner product. Konsep orthogonalitas di ruang bernorma tersebut diberikan oleh
G.Birkhoff (1935) dan dikenal dengan orthogonalitas-Birkhoff. Diberikan ruang
bernorma (X , ?.?) atas lapangan R, vektor x ? X dikatakan orthogonal-Birkhoff
terhadap y jika ?x+ ky? ? ?x? untuk setiap k ? R.
Di dalam analisis fungsional juga dikenal semi-inner product Lumer-Giles
yang ditulis [., .]. Semi-inner product Lumer-Giles lebih lemah daripada inner pro-
duct. Sifat inner product yang diperlemah tersebut adalah sifat simetri konjugat ya-
itu ?x, y? = ?y, x?, pada semi-inner product Lumer-Giles kesamaan [x, y] = [y, x]
belum tentu berlaku. Selanjutnya pada ruang bernorma atas real dapat dibentuk su-
atu semi-inner product superior-inferior yang dibangun dari norma tersebut. Semi-
inner product superior-inferior ini memiliki cakupan yang lebih luas dari inner pro-
duct sebab dapat dibangun pada semua ruang bernorma. Dari sini dapat dilihat
bahwa semi-inner product superior-inferior, semi-inner product Lumer-Giles, dan
inner product memiliki hubungan masing-masing. Dengan menggunakan hubung-
an tersebut dan konsep orthogonalitas yang terdapat pada masing-masing ruang
dapat dikaji hungangan orthogonalitas di ruang semi-inner product Lumer-Giles,
semi-inner product superior-onferior dengan orthogonalitas-Birkhof. Dari sini da-
pat dilihat bahwa banyak yang dapat dikaji dari orthogonalitas-Birkhoff, diantara-
nya definisi, sifat, dan hubunganya dengan konsep orthogonalitas lainnya, sehingga
orthogonalitas-Birkhoff ini menarik untuk dijadikan bahasan pokok dalam skripsi
ini.
Fungsional linear terbatas merupakan suatu operator yang khusus dan me-
miliki kaitan dengan ruang bernorma. Pada skripsi ini juga akan dibahas hubungan
antara fungsional linear dengan orthogonalitas menurut birkhoff.

Rumusan masalah yang dibahas dalam skripsi ini adalah sebagai berikut,
1. Orthogonalitas di ruang bernorma real menurut Birkhoff, serta sifat-sifat yang
berlaku.
2. Pengertian serta sifat-sifat orthogonalitas pada semi-inner product Lumer-
Giles dan semi-inner product superior-inferior.
3. Hubungan dan sifat-sifat orthogonalitas yang berlaku di ruang semi-inner pro-
duct Lumer-Giles, semi-inner product superior-inferior, dan fungsional linear
dengan orthogonalitas-Birkhoff.
1.3. Maksud dan Tujuan
Penulisan skripsi ini mempunyai tujuan umum, yaitu sebagai salah satu sya-
rat memperoleh kelulusan pada jenjang Starta-1(S1) Program Studi Matematika,
Jurusan Matematika, Fakultas Matematika dan ilmu pengetahuan Alam, universitas
Gadjah Mada. Selanjutnya, tujuan khusus dari penulisan skripsi ini adalah untuk
mempelajari dan menambah wawasan tentang materi-materi dibidang analisis khu-
susnya mengenai orthogonalitas-Birkhoff pada ruang bernorma, dan hubungannya
dengan ruang inner product, ruang semi-inner product, semi-inner product superior-
inferior, serta sifat-sifat yang berlaku di dalamnya.

Dalam mempelajari analisis fungsional, pada dasarnya perlu dipahami ter-
lebih dahulu konsep ruang vektor. Dengan adanya konsep ruang vektor, dikem-
bangkan konsep ruang bernorma, yaitu ruang vektor yang dilengkapi norma. Lebih
khusus lagi, terdapat ruang inner product, yaitu ruang vektor yang dilengkapi inner
product seperti yang disajikan dalam Berberian (1971) dan Kreyszig (1989). Ruang
inner product diakatakan lebih khusus karena inner product dapat membangkitkan
norma. Tetapi tidak semua norma dibangun oleh inner product. Selain itu, pada ru-
ang inner product juga terdapat konsep orthogonalitas, yaitu jika x, y elemen ruang
inner product, x dikatakan orthogonal terhadap y jika ?x, y? = 0 dan ditulis x ? y.
Dari kedua hal tersebut diperluas konsep orthogonalitas di ruang inner product un-
tuk sebarang ruang bernorma real, termasuk yang normanya tidak dibangkitkan oleh
inner product. G. Birkhoff (1935) mengemukakan konsep orthogonalitas di ruang
bernorma real, yaitu x, y elemen ruang bernorma, x dikatakan orthogonal-birkhoff
terhadap y jika
?x+ ky? ? ?x?
untuk setiap k ? R dan ditulis x ? y(B).
Selanjutnya, dari norma dan inner product dapat dipelajari konsep dual. Da-
lam konsep dual, dikenal adanya fungsional linear terbatas. Konsep fungsional li-
near terbatas ini memunculkan teorema perluasan fungsional yang dikenal dengan
Teorama Hahn-Banach. Jika diberikan ruang bernorma X atas R atau C dan f
fungsional linear terbatas yang terdefinisi pada subruang Z ? X , maka terdapat
fungsional linear terbatas f? padaX sehingga ?f??X = ?f?Z dan f?(x) = f(x) untuk
setiap x ? Z (Kreyszig, 1989). Pada S.S Dragomir(2004) dibahas juga hubungan
orthogonalitas-Birkhoff dengan fungsional linear.
Dari konsep inner product, muncul gagasan untuk membangun fungsi yang
sifatnya lebih lemah dari inner product. Dari gagasan ini munculah konsep semi-
inner product oleh Lumer-Giles, yaitu inner product yang sifat konjugat simetrinya
diperlemah. Pada konsep semi-inner product Lumer-Giles juga terdapat konsep
orthogonalitas. Pada S.S Dragomir (2004) dan Gangadharan (2009), x, y elemen
ruang bernorma dan [., .] semi-inner product yang membangkitkan norma ?.?, di-
definisikan x orthogonal-Giles terhadap y jika [y, x] = 0 dan ditulis x ? y(G).
Selanjutnya dibahas hubungan orthogonalitas-Birkhoff dan orthogonalitas-Giles di-
ruang bernorma dan [., .] semi inner product yang membangkitkan normanya, yaitu
x ? y(G) maka x ? y(B).
Selanjutnya, dari norma muncul konsep semi-inner product superior-inferior
dari x, y yang berturut-turut ditulis ?x, y?s dan ?x, y?i dan didefinisikan pada S.S
Dragomir (2004). Pada konsep semi-inner product superior-inferior ini juga ter-
dapat konsep orthogonalitas dari x, y elemen ruang bernorma real, yaitu x dika-
takan orthogonal-superior atau orthogonal-inferior terhadap y jika berturut-turut
?y, x?s = 0 atau ?y, x?i = 0 dan ditulis x ? y(s) atau x ? y(i). Adapula hubungan
orthogonalitas-Birkhoff dengan orthogonalitas-superior atau orthogonalitas-inferior
adalah sebagai berikut, x ? y(s) atau x ? y(i) maka x ? y(B) tetapi sebaliknya
belum tentu berlaku.

Metode penelitian skripsi ini yaitu dengan melakukan studi literatur. Litera-
tur yang digunakan dalam skripsi ini adalah S.S Dragomir (2004), Gangadharan N.
(2009), G. Birkhoff (1935), dan Robert C. James (1947).
Pada skripsi ini, akan dibahas mengenai Orthogonalitas menurut Birkhoff
pada ruang bernorma, ruang inner product. Pembahasan diawali dengan membahas
definisi serta sifat-sifat yang berlaku pada Orthogonalitas-Birkhoff. Selanjutnya,
dibahas definisi dan sifat orthogonalitas di ruang inner product, semi-inner produ-
ct Lumer-Giles, dan semi-inner product duperior-inferior. Kemudian, dilihat hu-
bungan orthogonalitas di ruang inner product, semi-inner product, dan semi-inner
product duperior-inferior dengan Orthogonalitas-Birkhoff. Selanjutnya, dipelajari
hubungan antara fungsional linear dengan Orthogonalitas menurut Birkhoff.

Skripsi ini ditulis dalam lima bab. Bab I, yaitu pendahuluan, memuat latar
belakang, perumusan masalah, maksud dan tujuan, tinjauan pustaka, metodologi
penelitian, dan sistematika penulisan. Bab II, yaitu konsep-konsep dasar, memuat
pembahasan mengenai ruang vektor, ruang bernorma, ruang inner product, fungsi-
onal linear, Teorema Hahn-Banach, dan fungsi konveks. Bab III, yaitu semi-inner
product Lumer-Giles dan semi-inner product superior-inferior yang memuat pe-
ngertian dan sifat-sifat yang berlaku, serta dibahas juga mengenai fungsi dualitas.
Bab IV, yaitu pembahasan, memuat definisi, sifat dan hubungan orthogonalitas-
Birkhoff dengan orthogonalitas di ruang semi-inner product dan semi-inner produ-
ct superior-inferior, serta dengan fungaional linear. Selanjutnya, pada Bab V berisi
kesimpulan.

