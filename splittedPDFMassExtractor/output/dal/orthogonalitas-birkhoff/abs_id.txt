Pada skripsi ini akan dibahas orthogonalitas pada ruang bernorma real yaitu
orthogonalitas menurut G.Birkhoff. Orthogonalitas-Birkhoff merupakan perumum-
an orthogonalitas di ruang inner product. Pengertian orthogonalitas pada ruang
inner product real ekuivalen dengan orthogonalitas-Birkhoff. Selain itu, terdapat
konsep yang lebih lemah daripada inner product yaitu semi-inner product Lumer-
Giles dan semi-inner product superior-inferior. Pada skripsi ini dipelajari juga hu-
bungan orthogonalitas-Birkhoff dengan orthogonalitas di ruang semi-inner product
Lumer-Giles dan ruang semi-inner product superior-inferior. Pada akhir skripsi ini
dibahas hubungan orthogonalitas-Birkhoff dengan fungsional linear.

