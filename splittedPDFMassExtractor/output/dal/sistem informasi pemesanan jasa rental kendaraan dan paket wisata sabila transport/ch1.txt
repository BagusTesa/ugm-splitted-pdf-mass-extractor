Dengan  lahir  dan  pesatnya perkembangan  dari  Internet  menjadi  salah
satu infrastruktur  komunikasi  yang  termurah  dan  dengan  tingkat  penerimaan
yang  luas, maka penggunaan  Internet sebagai fasilitas pendukung dan bahkan
sebagai urat nadi bisnis menjadi  semakin nyata  keunggulannya. Salah  satu  tren
yang menyertai  bisnis dalam  jaringan  Internet  adalah  e-commerce  baik
business-tocustomer  maupun busines-to-business.  Dengan  membawa
keunggulan  Internet  seperti  pelayanan  24 jam,  akses  dari  segala  penjuru
dengan  biaya  yang  relatif  murah  dan  kemudahan-kemudahan  lainnya,  maka
tidaklah  mengherankan  jika  sekarang  banyak  organisasi bisnis yang merambah
ke dalam e-commerce.
Selain digunakan sebagai media promosi, Internet juga digunakan sebagai
proses  pembelian  dan  penjulan  produk,  jasa  dan  informasi  secara  online
yang disebut  dengan  e-commerce.  Perkembangan  e-commerce  membawa
banyak perubahan  terhadap  sektor  aktivitas  bisnis  yang  selama  ini  dijalankan
di  dunia nyata.  Perubahan  tersebut  ditandai  dengan  adanya  sejumlah  upaya
dari  sektor aktivitas  bisnis  yang  semula  berbasis  di  dunia  nyata  (real),
kemudian mengembangkan ke dunia maya (virtual).
Selama ini masih banyak penyedia jasa kendaraan dan jasa paket wisata
yang masih melakukan promosi dan pemesanan melalui media cetak dan media
elektronik sehingga jangkauan promosi masih terbatas dan belum bisa menyebar
ke seluruh daerah. Kendala lain yang dialami oleh penyedia jasa ini adalah
ketatnya persaingan antar penyedia jasa yang sama, hal itu membuat pengelola
jasa ini harus mempunyai strategi pemasaran yang berbeda yang dapat
memperluas jangkauan pemasarannya sehingga dapat mendatangkan banyak
pelanggan baru.
Sabila Transport adalah salah satu penyedia jasa penyewaan kendaraan
bermotor dan penyedia jasa paket wisata yang belum memiliki fasilitas
pemesanan untuk pelanggan tanpa datang langsung ke kantor. Pelanggan harus
merelakan waktunya untuk datang ke kantor Sabila Transport untuk memesan
kendaraan maupun paket wisata yang diinginkan. Untuk itu dibutuhkan aplikasi
berbasis web berupa website e-commerce yang mampu memberikan informasi
yang lengkap dan akurat kepada pelanggan melalui Internet kapanpun dan
dimanapun pelanggan berada. Dengan adanya website e-commerce  pelanggan
bisa melakukan pemesanan  secara  online tanpa  harus  mendatangi  tempat
penyedia jasa itu berada. Selain  melakukan pembelian  dan  pemesanan  secara
online,  fasilitas  yang  dapat  dimiliki  sebuah website  e-commerce  adalah
adanya  transaksi  pembayaran  dengan  cara  online maupun off-line melalui
transfer antar rekening bank.
Dengan lokasi perusahaan Sabila Transport yang jauh dari pusat kota
Yogyakarta membuat para pelanggan yang ingin memesan jasa rental kendaraan
dan paket wisata kembali menemui masalah dalam biaya transportasi yang tidak
sedikit untuk menjangkau lokasi perusahaan Sabila Transport. Belum lagi jika
kendaraan atau paket wisata yang ingin dipesan sedang tidak tersedia, sekali lagi
pelanggan akan merasa kecewa jika hal itu benar terjadi.
Perumusan  masalah  berdasarkan  latar  belakang  permasalahan  adalah
bagaimana membangun sebuah website yang dapat melakukan pemesanan
kendaraan dan paket wisata secara online.
Batasan-batasan masalah dalam perancangan penulisan tugas akhir ini
adalah sebagai berikut :
1. Pembayaran dilakukan diluar sistem.
2. Sistem hanya mencatat transaksi pemesanan yang dilakukan melaui
sistem.
3. Biaya penyewaan ditentukan dari negoisasi pelanggan dengan Sabila
Penelitian ini bertujuan untuk membangun sebuah website yang nantinya
dapat melakukan pemesanan kendaraan dan paket wisata secara online.
Manfaat dari penulisan tugas akhir aplikasi berbasis web ini antara lain :
1. Mempermudah pemesanan berbagai jenis alat transportasi dan paket
wisata.
2. Mempersingkat waktu pemesanan berbagai jenis alat transportasi dan
paket wisata.
3. Mengurangi biaya pemesanan berbagai jenis alat transportasi dan paket
wisata.
1.6  Metodologi Penelitian
Penelitian ini dilakukan dengan tahapan-tahapan sebagai berikut :

Studi literatur dilakukan dengan mengkaji dan mempelajari paper, e-book,
dan situs web yang terkait dengan aplikasi berbasis WEB, kombinasi antara PHP,
HTML, JavaScript, dan CSS.

Pada tahap analisis dilakukan analisis kebutuhan sistem dalam
membangun aplikasi berbasis web dengan cara wawancara kepada perusahaan
yang bersangkutan, dalam hal ini adalah Sabila Transport.

Metode  ini  merupakan  perancangan  sistem,  dilakukan  dengan
menggambarkan  bagaimana  sistem  akan  dibangun  dan  persiapan  untuk
merancang  sistem  itu  sendiri.  Dalam  mendesain  aplikasi  akan menggunakan
Microsoft Office Visio dan MySql Workbench yang didalamnya terdapat ERD
(Entity Relational Diagram), DAD, dan Relasi antar Tabel.

Merupakan tahapan merubah rancangan sistem menjadi kode program
komputer.

Merupakan tahap akhir, yaitu menguji program yang sudah jadi apakah
berjalan dengan baik ataukah masih memerlukan perbaikan.
Tahap pengujian berupa :
1. Pengujian aplikasi pada komputer.
2. Pengujian pemesanan kendaraan.
3. Pengujian pemesanan paket wisata.
Sistematika penulisan yang digunakan dalam penyusunan tugas akhir
adalah sebagai berikut:
BAB I PENDAHULUAN
Bab ini menjelaskan tentang latar belakang, perumusan masalah, batasan
masalah, tujuan  penelitian,  manfaat  penelitian,  yang  digunakan  sebagai
referensi  dalam penelitian  tugas  akhir, metode  penelitian  yang  akan
digunakan  dan  sistematikan penulisan tugas akhir.
Bab  ini menguraikan tentang  informasi beberapa hasil penelitian sudah
dilakukan sebelumnya. Kemudian  informasi  tersebut  dijadikan  sebagai  bahan
pembanding dalam pembuatan aplikasi yang sedang diteliti saat ini.
Berisi tentang materi � materi, teori ataupun pendapat dari sumber lain
yang dapat digunakan  sebagai  acuan  dalam  penelitian.  Dasar  teori  ini  harus
dapat dipertanggungjawabkan  secara  ilmiah  dengan  menyertakan  sumber  �
sumber referensi yang sudah diakui kebenarannya.
Bab  ini  menjelaskan  tentang  analisis  dan  desain  sistem,  didalamnya
terdapat perancangan  analisis  sistem,  perancangan  proses  dan  perancangan
antarmuka pengguna sistem.
Bab  ini  membahas  tentang  implementasi sistem  sesuai  dengan
perancangan sistem yang dilakukan sebelumnya.
Membahas  tentang  bagaimana  aplikasi  dapat  berjalan  pada  platform
Android serta  sistem  location  based  advertising  dapat  berjalan  sesuai  dengan
yang diharapkan.
Bab  penutup  berisi  tentang  kesimpulan  hasil  penelitian  yang
diperoleh  sesuai dengan  tujuan  penelitian.  Selain  itu  berisi  pula  saran
sebagai  acuan  untuk penelitian berikutnya agar dapat menghasilkan aplikasi yang
lebih baik.

