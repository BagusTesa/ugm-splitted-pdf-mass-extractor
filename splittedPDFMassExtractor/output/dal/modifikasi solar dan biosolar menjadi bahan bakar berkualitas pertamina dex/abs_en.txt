Modification of Solar and Biosolar into Fuel With Pertamina DEX Quality
Modification of Solar and Biosolar into fuel with Pertamina DEX Quality
had been carried out. In this research, Solar and Biosolar are product of
Pertamina. The physical properties test showed that Pertamina DEX has better
quality than Solar and Biosolar. The result of component analysis showed that
both of fuels have similar chromatogram, but Pertamina DEX has narrow pattern
than Solar and Biosolar. The light and heavy fractions in Solar and Biosolar were
separated by distillation. The results of this modification were analyzed in their
density, viscosity and heat of combustion.
Based on the four fraction of Biosolar, the analysis result  showed that 2nd
(Bp. 45-68 oC), 3rd (Bp. 46-70 oC), and 4th (Bp. 146-150 oC) fractions have the
same retention time with Pertamina DEX. Based on three fractions of Solar , the
analysis result  showed that 2nd (Bp. 64-65 oC) and 3rd (Bp. 66-50 oC) fractions
have the same retention time with Pertamina DEX. The analysis of density and
viscosity results showed that modification produced fuel with lower density and
viscosity, but higher on heat of combustion. The results showed that Solar and
Biosolar modification characters still have distinct characters with Pertamina
Key words: Solar, Biosolar, Pertamina DEX, distillation.

