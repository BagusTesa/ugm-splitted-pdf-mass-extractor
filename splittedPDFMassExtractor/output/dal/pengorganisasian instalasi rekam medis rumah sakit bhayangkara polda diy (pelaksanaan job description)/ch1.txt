Rumah sakit merupakan salah satu institusi pelayanan kesehatan,
oleh karena itu rumah sakit dituntut untuk meningkatkan mutu
pelayanan kesehatan demi kepuasan masyarakat yang menggunakan
jasanya. Semua kegiatan pelayanan yang diberikan kepada pasien wajib
untuk ditulis dalam rekam medis, karena rekam medis merupakan suatu
dokumen yang sangat penting. Sesuai dengan Peraturan Menteri
Kesehatan RI Nomor 269/MENKES/PER/III/2008 tentang Rekam Medis,
untuk mewujudkan peningkatan mutu pelayanan kesehatan,
penyelenggaraan pelayanan kesehatan harus didukung adanya sarana
penunjang yang memadai antara lain melalui penyelenggaraan rekam
medis di setiap instansi pelayanan kesehatan termasuk di rumah sakit.
Penyelenggaraan  rekam medis di rumah sakit merupakan salah
satu unsur penting dalam mencapai tertib administrasi, dimana tertib
administrasi merupakan salah satu faktor yang menentukan baik atau
buruknya pelayanan kesehatan di rumah sakit. Agar penyelenggaraan
rekam medis di rumah sakit dapat berjalan baik  ketentuan yang berlaku
maka diperlukan pengelolaan rekam medis yang baik pula, sesuai dengan
Organisasi dan Tata Kerja Sarana Pelayanan Kesehatan, selain itu
diperlukan Administrator Informasi Kesehatan atau dapat disebut sebagai
Dalam memenuhi kebutuhan data atau informasi kesehatan dan
tuntutan akan pelayanan yang cepat dan tepat, penyelenggaraan rekam
medis harus dilaksanakan secara terarah dan berkualitas. Dalam hal ini
peranan sumber daya manusia (petugas rekam medis) dalam Unit Kerja
Rekam Medis menjadi komponen yang sangat penting. Petugas rekam
medis saat ini harus mampu tumbuh berkembang sesuai dengan tuntutan
manajemen rumah sakit modern yang lebih menekankan pada informasi
kesehatan. Selain itu, indikator keberhasilan rumah sakit yang efektif dan
efisien adalah tersedianya sumber daya manusia yang cukup dengan
kualitas yang tinggi, profesional sesuai dengan fungsi dan tugas setiap
personel (Ilyas, 2000). Salah satu faktor untuk meningkatkan mutu
pelayanan yaitu adanya suatu sistem yang diatur oleh sumber daya
manusia yang baik dalam suatu wadah yang disebut organisasi.
Organisasi adalah suatu sistem yang mengatur kerjasama antara dua
orang atau lebih sedemikian rupa sehingga segala kegiatan dapat
diarahkan untuk mencapai tujuan yang telah ditetapkan. Untuk mencapai
tujuan yang telah ditetapkan, maka perlu adanya pembagian kerja, fungsi,
wewenang serta penerapan hirarki dan tanggung jawab masing-masing
jabatan. Oleh karena itu, pengorganisasian Instalasi Rekam Medis terkait
job description-nya perlu dilakukan agar dapat menghasilkan efektivitas
dan efisiensi pelayanan serta produktivitas kerja yang mengarah pada
peningkatan kualitas manajemen rekam medis dan informasi kesehatan.
Deskripsi pekerjaan (job description) harus berisi pernyataan ringkas
dan akurat yang menunjukkan apa yang dikerjakan oleh karyawan,
bagaimana mereka mengerjakannya, dan kondisi di mana tugas
dilaksanakan. Deskripsi pekerjaan menjadi landasan bagi banyak aktivitas
sumber daya manusia. Deskripsi pekerjaan memberikan standar objektif
untuk pengisian setiap pekerjaan, serta merupakan alat untuk mengisi
pekerjaan tersebut melalui promosi dan pengangkatan. Tujuan deskripsi
pekerjaan adalah untuk menyediakan informasi organisasional dan
struktural di samping informasi fungsional (Sunyoto, 2012).
Berdasarkan hasil studi pendahuluan dengan metode observasi
dan wawancara kepada Kepala Instalasi Rekam Medis Rumah Sakit
Bhayangkara Polda DIY yang telah dilakukan pada tanggal 30 Oktober
dan 17 Desember 2012, peneliti mengetahui bahwa peraturan atau
prosedur tentang job description telah dibuat dan ada penanggungjawab
tiap bagiannya, namun belum terlaksana secara optimal sehingga peran
petugas rekam medis menjadi multifungsi dan distribusi pekerjaan belum
sepenuhnya terlaksana sesuai dengan job description masing-masing
petugas rekam medis. Jadi pekerjaan seperti pendaftran, assembling,
coding, filing dilakukan oleh semua petugas yang sedang bertugas pada
saat itu. Oleh karena itu, peneliti tertarik untuk mengangkat judul
Polda DIY (Pelaksanaan Job Description)� sebagai masalah yang diteliti.
Hal ini didukung dengan pertimbangan pembagian kerja yang jelas, maka
diharapkan dapat memperoleh keefektivan kerja serta menghindari
pemborosan.

Berdasarkan latar belakang di atas, maka rumusan masalah dalam
penelitian ini adalah: �Bagaimana pengorganisasian Instalasi Rekam

Dari rumusan masalah di atas, maka tujuan penelitian yang ingin
dicapai adalah:
1. Mengetahui pelaksanaan job description petugas rekam medis di
2. Menganalisis job description petugas rekam medis berdasarkan teori
yang sesuai dan kebijakan yang berlaku di Instalasi Rekam Medis
3. Memperbaiki job description dan struktur organisasi di Instalasi



Dapat digunakan sebagai bahan pertimbangan,
usulan/masukan bagi pihak rumah sakit dalam hal
pengorganisasian Instalasi Rekam Medis terkait pelaksanaan job
description serta analis job description dan struktur organisasi
berdasarkan kebijakan yang berlaku di Instalasi Rekam Medis

Dapat menambah pengetahuan tentang rekam medis
serta menerapkan teori yang telah peneliti dapatkan dari
institusi pendidikan dan mengetahui khususnya tentang
pengorganisasian Instalasi Rekam Medis terkait pelaksanaan job
description serta serta analis job description dan struktur
organisasi berdasarkan kebijakan yang berlaku di Instalasi


Dapat menambah pengetahuan dan wawasan serta
pengalaman mengenai pengorganisasian Instalasi Rekam Medis
terkait pelaksanaan job description petugas rekam medis untuk
menilai produktivitas kerja, dengan menerapkan teori yang
peneliti peroleh dari institusi pendidikan, serta sebagai bahan
masukan dalam meningkatkan mutu pendidikan dan
perngukuran kemampuan mahasiswa dalam menerapkan
ilmunya.

Sebagai dasar atau acuan dalam pendalaman materi serta
referensi yang berhubungan untuk kelanjutan penelitian yang
relevan dan hampir serupa.

Penelitian berjudul �Pengorganisasian Instalasi Rekam
Description)� belum pernah dilakukan sebelumnya. Namun
penelitian dengan tema yang serupa pernah dilakukan, yaitu
antara lain:
1. Yulianitasari (2012) dengan judul �Peran dan Fungsi Perekam
Medis serta Pengorganisasian Unit Kerja dalam
Penyelenggaraan Rekam Medis di Rumah Sakit AU Dr. S
Metode penelitian yang digunakan adalah metode
penelitian deskriptif dengan menggunakan pendekatan
kualitatif dan rancangan penelitian fenomenologis. Tujuan
penelitian Yulianitasari (2012) ini adalah untuk mengetahui
kegiatan dalam penyelenggaraan rekam medis di Rumah Sakit
Dr. S. Harjolukito dengan melihat peran dan fungsi perekam
medis serta melihat pengorganisasian di unit kerja rekam
medis. Hasil penelitian Yulianitasari (2012) menunjukkan
bahwa kegiatan  penyelenggaraan rekam medis di Rumah Sakit
AU Dr. S. Harjolukito meliputi penerimaan pasien, filing, coding,
pelaporan dan SKM. Koding hanya dilakukan untuk klaim
askes saja dan pelaporan tidak seluruhnya dikerjakan di bagian
rekam medis. Tidak ada kegiatan pengolahan data medis yang
meliputi assembling dan indexing. Masih terdapat peran dan
fungsi perekam medis yang belum dilaksanakan dalam
penyelenggaraan rekam medis yaitu tidak dilaksanakannya
klasifikasi dan kodefikasi penyakit selain itu kurang sempurna
dalam menjalankan manajemen rekam medis, statistik
kesehatan dan manajemen unit kerja rekam medis.
Persamaan penelitian Yulianitasari (2012) dengan
penelitian yang dilakukan peneliti adalah melihat peran
petugas rekam medis. Perbedaannya bahwa penelitian
Yulianitasari (2012) menitikberatkan pada penyelenggaraan
kegiatan dan pengorganisasian rekam medis, sedangkan
peneliti akan menitikberatkan pada pengorganisasian Instalasi
Rekam Medis terkait pelaksanaan job description petugas rekam
medis.
2. Haryati (2008) dengan judul �Distribusi Pekerjaan Petugas
Metode penelitian yang digunakan adalah metode
penelitian deskriptif dengan menggunakan pendekatan
kualitatif dan rancangan cross sectional. Tujuan penelitian
Haryati (2008) ini adalah untuk mengetahui distribusi pekerjaan
(job description) petugas rekam medis dengan menghitung beban
kerja di Instalasi Rekam Medis Rumah Sakit PKU
Muhammadiyah Bantul. Hasil penelitian Haryati (2008) adalah
distribusi pekerjaan (job description) di Instalasi Rekam Medis
Rumah Sakit PKU Muhammadiyah Bantul sudah ada, namaun
belum mencerminkan adanya penanggung jawab yang tetap
dan pembebanan kerja yang jelas sehingga menyebabkan
pekerjaan menjadi multifungsi dan kegiatan pelaporan belum
dapat diselesaikan dengan optimal. Hasil perhitungan beban
kerja diperoleh 8 petugas yang bertanggung jawab khususnya
pada bagian pelayanan, dan untuk hail perhitungan jumlah jam
kerja ada beberapa petugas yang jam kerja efektifnya di bawah
jam kerja pokok. Hal ini merupakan faktor pendukung
terselesaikannya pekerjaan di Instalasi Rekam Medis Rumah
Sakit PKU Muhammadiyah Bantul padahal jumlah petugasnya
11 orang, nilai yang tinggi dibandingkan dengan nilai hasil
perhitungan.
Persamaan penelitian yang akan dilakukan peneliti
dengan penelitian Haryati (2008) adalah mengetahui distribusi
pekerjaan (job description) petugas rekam medis, sedangkan
perbedaannya adalah Haryati (2008) meneliti dengan
menghitung beban kerja sedangkan peneliti meneliti tentang
pengorganisasian Instalasi Rekam Medis terkait pelaksanaan
job description dan memperbandingkan dengan kebijakan yang
berlaku termasuk memperbaiki job description struktur
organisasi di Instalasi Rekam Medis Rumah Sakit Bhayangkara
3. Nurhayati (2004) dengan judul �Analisis Pelaksanaan Job-
Description Petugas Koding di Unit Rekam Medis Rumah Sakit
Metode penelitian yang digunakan adalah metode
penelitian deskriptif dengan menggunakan pendekatan
kualitatif. Tujuan penelitian Nurhayati (2004) ini adalah
mengetahui job-descriptions petugas koding yang meliputi tugas
pokok, fungsi, wewenang, dan tanggungjawab petugas koding
di Rumah Sakit Islam Klaten; mengetahui tolok ukur
pelaksanaan job-descriptions petugas koding berdasarkan hasil
kode yang sesuai dengan tolok ukur job-descriptions yang ada;
mengetahui jumlah petugas yang ada serta peralatan
penunjang dalam mendukung kondisi pekerjaan. Hasil
penelitian Nurhayati (2004) bahwa tolok ukur job-descriptions
pelaksanaan koding selain kesesuaian dan kelengkapan kode
rawat inap dan rawat jalan juga kesesuaian kode tindakan atau
operasi dengan ICOPIM. Job-descriptions petugas koding di
Rumah Sakit Islam Klaten masih menjadi satu dengan job-
descriptions pelaksana pengolah data. Job-descriptions teRumah
Sakitebut belum dipilahkan sendiri perbagian, misalnya: job-
descriptions petugas assembling, koding, filing dan pelaporan.
Jumlah petugas koding yang ada di Rumah Sakit Islam Klaten
hanya satu orang dan ruang petugas koding masih menjadi
satu dengan ruang assembling.
Persamaan penelitian Nurhayati (2004) dengan penelitian
yang akan dilakukan peneliti adalah meneliti tentang job-
descriptions. Perbedaannya bahwa penelitian Nurhayati hanya
meneliti tentang analisis job-descriptions petugas koding di
Rumah Sakit Islam Klaten, sedangkan peneliti melakukan
penelitian tentang pengorganisasian Instalasi Rekam Medis
terkait pelaksanaan job descriptions petugas rekam medis di


tahun 2012, Rumah Sakit Bhayangkara Polda Daerah Istimewa
Yogyakarta adalah Rumah Sakit milik Polda DIY sebagai sarana
pelayanan kesehatan terhadap seluruh personel Polri, PNS Polri
Polda DIY, dan keluarganya serta memberikan dukungan
kedokteran kepolisian terhadap tugas operasional Polda DIY.
Rumah Sakit Bhayangkara Polda DIY dipimpin oleh dr.
Nariyana selaku Kepala Rumah Sakit (Karumkit). Rumah Sakit
Bhayangkara Polda DIY beralamatkan di Jalan Yogyakarta-Solo
km.14 Kalasan Kabupaten Sleman. Rumah Sakit Bhayangkara
Polda DIY memiliki luas bangunan lama 600 m� (satu lantai),
luas bangunan baru 1200 m� (dua lantai), total luas tanah secara
keseluruhan + 14.033 m�.
Struktur organisasi Rumah Sakit Bhayangkara Polda DIY
mengacu pada Peraturan Kapolri nomor 11 tahun 2011 tentang
Struktur Organisasi dan Tata Kerja Rumah Sakit Bhayangkara
Kepolisian Republik Indonesia, yaitu dipimpin oleh seorang
Kepala Rumah Sakit (Karumkit) yang setara kedudukannya
dengan Dewan Pengawas dan dibantu oleh Wakil Rumah Sakit
(Wakarumkit) yang mempimpin jabatan secara struktural
membawahi tiga Sub Bagian yaitu Sub Bagian Wasintern, Sub
Bagian Renmin, Sub Bagian Binfung dan 2 Sub Bidang  yaitu
Sub Bidang Yanmed Dokpol dan Sub Bidang Medum.
Sesuai dengan Peraturan Kapolri nomor 11 tahun 2011
tentang Susunan Organisasi dan Tata Kerja Rumah Sakit
Sakit Bhayangkara Polda DIY mempunyai tugas pokok
menyelenggarakan kegiatan pelayanan kedokteran kepolisian
untuk mendukung tugas operasional Polri dan pelayanan
kesehatan kepolisian bagi pegawai negeri pada Polri dan
keluarganya serta masyarakat umum secara prima.
Visi dan Misi Rumah Sakit Bhayangkara Polda DIY:

Terwujudnya Rumah Sakit Polri yang profesional dan
menjadi pilihan masyarakat.

i) Melaksanakan pelayanan kesehatan yang prima yaitu
cepat, tepat, ramah, dan informatif serta peduli
lingkungan.
ii) Mengembangkan kemampuan dan kekuatan sumber
daya manusia yang berkualitas dalam rangka
mewujudkan pelayanan profesional.
iii) Melaksanakan pelayanan kedokteran kepolisian dalam
rangka mendukung tugas operasional Polri.
iv) Meningkatkan kualitas dan kuantitas sarana dan
prasarana sesuai perkembangan ilmu pengetahuan dan
teknologi serta ramah lingkungan.
v) Melaksanakan pendidikan dan penelitian.

Rumah Sakit Bhayangkara Polda DIY saat ini telah lulus
akreditasi lima bidang pelayanan oleh Komite Akreditasi
Rumah Sakit. Pelayanan yang diberikan oleh Rumah Sakit
Bhayangkara Polda DIY dari instalasi rawat jalan, instalasi
rawat inap, instalasi penunjang medis, dan instalasi penunjang
non medis, meliputi:
1. Instalasi pelayanan rawat jalan, meliputi:

b. Klinik Gigi dan Mulut













2. Instalasi pelayanan  rawat inap, meliputi:
Pelayanan rawat inap terbagi menjadi 2, yaitu perawatan
untuk pasien dengan kasus penyakit umum dan perawatan
untuk kasus kandungan dan kebidanan yang letaknya
terpisah. Fasilitas kamar yang tersedia untuk rawat inap
umum seluruhnya ada 51 tempat tidur dengan pembagian :
a. 8 TT kelas I
b. 15 TT kelas II
c. 14 TT kelas III
d. 3 TT tahanan
e. 1 TT VIP
Sedangkan untuk rawat kebidanan dan kandungan adalah
sebagai berikut :
a. 2 TT kelas I
b. 4 TT kelas II
c. 4 TT kelas III
d. 13 box ruang bayi
e. 2 buah incubator
f. 2 bed ruang bersalin
3. Instalasi penunjang medis, meliputi:








4. Instalasi penunjang non medis, meliputi:


c. Parkir luas
d. Free hostpot area
2. Perfomance Rumah Sakit dan Jumlah Kunjungan Pasien
Perfomance yang dimiliki Rumah Sakit Bhayangkara
Polda DIY pada tahun 2012 adalah sebagai berikut:
Perfomance Rumah Sakit Bhayangkara Polda DIY tahun 2010 �  2012
Length of Stay (LOS) 3,8 / hr 2,9 / hr 3,3/ hr
Turn Over Interval (TOI) 5,3 / hr 6,9 / hr 8,9/ hr
Bed Turn Over (BTO) 30,20 kali 36,98 kali 43,59 kali
Jumlah kunjungan rawat jalan 19.168 pasien 23.908 pasien 23.455 pasien
Jumlah kunjungan rawat inap 2.024 pasien 2.050 pasien 1.606 pasien
Sumber: Laporan Kegiatan Praktek Lapangan �Desain Formulir dan
Analisis Berkas Rekam Medis di Rumah Sakit Bhayangkara� tahun 2012

