The measurement for comparison of radiation dose and image quality on
Direct Digital Radiography (DR) and Computed Radiography (CR) had been
conducted by using baby phantom as the object with abdomen (AP) examining
area. The measurement of scattered radiation exposure was conducted using
surveymeter at a distance of 10 cms from the focus point of X-ray. Image quality
was analized using ImageJ software i.e. histogram analysis and SNR (Signal to
Noise Ratio) calculation. The results indicate that in exposure measurement of
scattered radiation for DR system yield maximum value on 40 kV of 0.137
mR/hour without grid and minimum value on 60 kV of 0.108 mR/hour with grid.
The results of CR system yield maximum value on 40 kV of 0.105 mR/hour
without grid and minimum value on 60 kV of 0.083 mR/hour with grid. Value of
SNR analysis on CR is higher than DR for both measurement results. Gray level
distribution of histogram analysis on DR wider than CR.
Image Quality, Signal to Noise Ratio.

