Penelitian ini membahas tentang klasifikasi sidik jari, yang bertujuan
untuk mengklasifikasi sidik jari manusia dalam tiga kelas yaitu: whorl, Arch, dan
loop. Tahap yang dilakukan adalah preprocessing, segmentasi, ekstrasi ciri dan
klasifikasi. Dalam preprocessing yang dilakukan grayscale, median filter,
peregangan kontras, histogram. Segmentasi menggunakan metode otsu
thresholding dan ekstrasi ciri menggunakan gray level cooccurence matrix
(GLCM). Fitur yang digunakan adalah correlation, contrast, energy, homogeneity,
dan entropy. Klasifikasi tersebut menggunakan jaringan syaraf tiruan
backpropagation. Hasil penelitian system ini dapat mengklasifikasi sidik jari
dengan akurasi 87,5%.
Kata kunci : GLCM, backpropagation neural network

