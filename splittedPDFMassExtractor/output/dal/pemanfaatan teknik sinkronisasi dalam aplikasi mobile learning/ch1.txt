Perkembangan teknologi perangkat bergerak saat ini memicu munculnya
berbagai perangkat bergerak seperti smartphone, PDA dan tablet. Berbagai kelebihan
dan fasilitas memberi kemungkinan kepada perangkat bergerak untuk menggantikan
peran komputer saat pengguna berada di luar ruangan (Firdausillah, 2012).
Perangkat bergerak lebih fleksibel dalam hal portabilitas dibanding dengan
komputer. Di samping itu, perangkat bergerak memiliki fasilitas yang hampir sama
seperti komputer misalnya untuk aktivitas komputasi.  Aplikasi-aplikasi desktop kini
telah tersedia untuk versi perangkat bergerak misalnya aplikasi office seperti word
processor, spreadsheet, dan calendar organizer. Perangkat bergerak juga dilengkapi
dengan mobile browser yang mendukung bahasa HTML5, CSS3 dan Javascript yang
memungkinkan untuk mengakses aplikasi berbasis web kompleks. Kelebihan-
kelebihan tersebut menjadikan pengguna untuk selalu bisa mengakses informasi,
melakukan aktivitas komputasi meski tidak berada di kantor atau tidak tersedia
komputer.
Meskipun perangkat bergerak memiliki berbagai kelebihan terutama dalam
hal portabilitas, perangkat bergerak juga tidak terlepas dari kekurangan. Perangkat
bergerak tidak memiliki kemampuan komputasi yang sebanding dengan komputer
dan sangat bergantung pada daya baterai. Koneksi jaringan pun terkadang terputus
dikarenakan sinyal yang tidak stabil. Di samping itu, penggunaan koneksi jaringan
secara terus menerus justru akan lebih menurunkan daya konsumsi baterai (Ramya,
2012).
Kekurangan tersebut diatasi dengan adanya basis data lokal pada perangkat
bergerak untuk mencapai tujuan pemrosesan data yang stabil. Basis data lokal
merupakan basis data yang terdapat pada perangkat bergerak. Penggunaan basis data
lokal tidak terlepas dari penerapan teknik sinkronisasi. Menurut Kim(2006),
perangkat bergerak akan mereplikasi data tertentu yang ada pada server
menggunakan teknik sinkronisasi melalui jaringan yang stabil. Kemudian, pengguna
dapat mengakses informasi yang telah disimpan di basis data lokal dan melakukan
pekerjaan komputasi tanpa harus terhubung dengan jaringan internet. Adanya
modifikasi pada basis data lokal menimbulkan terjadinya inkonsistensi data antara
basis data di server dan basis data lokal. Oleh karena itu, teknik sinkronisasi  menjadi
hal yang penting dalam hal portabilitas perangkat bergerak.
Berdasar pada tinjauan tersebut, penelitian ini akan membahas mengenai
ketersediaan data melalui teknik sinkronisasi data untuk menjaga konsistensi data
antara basis data lokal dan server. Oleh karena penelitian ini terkait dengan perangkat
bergerak, maka akan dibangun pula aplikasi perangkat bergerak untuk dapat
menunjukkan proses sinkronisasi antara perangkat bergerak dengan server.
Aplikasi yang dibangun merupakan aplikasi mobile learning untuk mengakses
LMS (Learning Management System) melalui perangkat bergerak. Pengembangan
aplikasi mobile learning tersebut dimaksudkan agar pengguna tidak perlu harus
bergantung pada perangkat desktop dan koneksi internet dalam mengakses LMS.
Berdasar pada latar belakang, maka rumusan masalah penelitian ini yaitu:
1. Apakah teknik sinkronisasi mampu mengatasi ketersediaan data pada aplikasi
mobile learning?
2. Apakah teknik sinkronisasi mampu meningkatkan kecepatan akses terhadap
aplikasi mobile learning?
Tujuan dari penelitian yaitu menguji penerapan teknik sinkronisasi data untuk
menangani kesiapan dan kemudahan penggunaan data di aplikasi mobile learning
pada mode online dan offline.
Hasil dari penerapan teknik sinkronisasi pada pengembangan aplikasi mobile
learning ini dapat dimanfaatkan untuk penerapan aplikasi pada perangkat bergerak
untuk keperluan pembelajaran lain secara lebih fleksibel dalam mengakses aplikasi
menggunakan perangkat bergerak meskipun tidak terhubung dengan internet.
Pengembangan aplikasi mobile learning pada penelitian ini memiliki beberapa
batasan agar fokus terhadap tujuan penelitian dan dapat selesai sesuai dengan jadwal
penelitian. Beberapa batasan penelitian yaitu sebagai berikut :
a. Pengembangan aplikasi mobile learning disertai dengan pemanfaatan LMS
Claroline dan hanya menggunakan beberapa modul yaitu course description,
announcement, agenda, dan assignment.
b. Aplikasi yang dikembangkan hanya ditujukan untuk perangkat bergerak yang
memiliki sistem operasi Android.
c. Teknik sinkronisasi yang digunakan pada penelitian ini yaitu teknik sinkronisasi
Penelitian ini dilakukan dengan mengacu pada urutan beberapa aktivitas yaitu
studi pustaka, analisis kebutuhan sistem, rancangan sistem, implementasi, uji coba
dan penulisan laporan. Dalam penelitian disertai dengan studi kasus untuk
memudahkan dalam melihat hasil pengujian dari usulan penelitian yang
dikembangkan. Berikut detail dari setiap aktivitas yang dilakukan pada penelitian ini:

Studi pustaka dilakukan dengan melakukan studi terhadap referensi mengenai
penelitian yang pernah dilakukan sebelumnya sesuai dengan topik yang diangkat
yaitu model sinkronisasi pada lingkungan perangkat bergerak. Selain mengenai
penelitian terkait juga dilakukan studi pustaka mengenai teori pendukung yang
berhubungan dengan topik aplikasi mobile learning dan LMS.

Setelah melakukan studi pustaka maka aktivitas selanjutnya yaitu analisa
kebutuhan sistem. Dalam tahap ini akan dilakukan analisis terhadap komponen yang
digunakan dalam pembuatan aplikasi sebagai produk pada penelitian ini. Komponen
yang dibutuhkan yaitu:
a. Komputer atau laptop
Komputer atau laptop menjadi komponen utama dalam pengembangan aplikasi
mobile learning.

Smartphone digunakan sebagai alat uji coba atas aplikasi yang sedang dibangun
untuk mengetahui apakah aplikasi telah dibangun sesuai dengan spesifikasi.
Aplikasi yang berjalan pada emulator atau computer belum tentu berjalan baik
pada perangkat yang nantinya akan diimplementasikan. Hal ini tergantung pada
spesifikasi pada perangkat yang digunakan. Dalam hal ini, perangkat yang
digunakan adalah smartphone yang telah dilengkapi dengan sistem operasi

Tahap perancangan sistem merupakan tahap pengembangan desain sistem
sebagai solusi atas masalah yang dirumuskan pada penelitian ini. Rancangan sistem
didasarkan pada studi pustaka dan dideskripsikan dalam bentuk gambar atau diagram.
Hasil dari rancangan sistem berupa gambaran berisi solusi yang menjadi dasar dalam
proses pengembangan sistem.

Membuat dan mengembangkan sistem sesuai dengan desain yang telah
ditentukan merupakan langkah yang dilakukan setelah menyelesaikan tahap
perancangan sistem. Dalam penelitian ini akan dibangun middleware yang dapat
menjembatani antara sisi client dan server dalam melakukan proses sinkronisasi.
Untuk memudahkan dalam mengimplementasikan teknik sinkronisasi, maka
dibangun aplikasi untuk di sisi client (perangkat bergerak) dan aplikasi untuk server.
Aplikasi yang dibangun pada sisi client menggunakan Phonegap, yang mendukung
penggunaan bahasa HTML5, Javascript dan CSS sedangkan pada sisi server akan
memanfaatkan LMS dengan Claroline.
Penerapan teknik sinkronisasi mengambil studi kasus yang dapat
menggambarkan teknik sinkronisasi pada perangkat bergerak sehingga dapat
diketahui keberhasilan atas solusi dalam penelitian ini. Oleh karena dalam
implementasinya sangat sulit dilakukan pada perusahaan, maka penelitian ini
mengambil studi kasus mengenai pembelajaran mobile dengan mata kuliah �English
Basic�. Setelah penentuan studi kasus, maka pada proses selanjutnya dapat dilakukan
pengembangan aplikasi berdasar pada rancangan yang dibuat.

Uji coba sistem akan menguji aplikasi secara keseluruhan. Proses uji coba
dilakukan untuk mengetahui kinerja aplikasi apakah sesuai dengan spesifikasi yang
telah ditetapkan. Dalam penelitian ini ada dua metode pengujian yang digunakan
yaitu :
a. Unit testing : memastikan tiap komponen aplikasi berfungsi sesuai dengan yang
dibutuhkan.
b. Uji performa : tes untuk mengetahui performa dari sistem yang dibangun yaitu
dengan menguji kecepatan atau waktu yang dibutuhkan untuk melakukan proses
sinkronisasi data.

Tahap ini akan melakukan penulisan laporan dari hasil penelitian yang
dilakukan selama pembuatan aplikasi sebagai bentuk dokumentasi akhir. Penulisan
laporan diharapkan dapat menjadi bahan referensi untuk pengembangan penelitian
berikutnya mengenai topik terkait.

Bab Pendahuluan memberikan uraian singkat mengenai latar belakang
masalah atas penelitian, rumusan dari masalah, batasan penelitian, tujuan, dan
manfaat penelitian. Di samping itu bab ini menjelaskan pula mengenai
metodologi penelitian dan sistematika penulisan
Bab tinjauan pustaka merupakan bab yang membahas pustaka yang ditinjau
sebagai bahan referensi untuk penelitian ini.
Bab ini memberikan uraian teori dasar yang berhubungan dengan topik
penelitian dan digunakan sebagai bahan acuan dalam memecahkan masalah.
Bab ini menguraikan analisis dan perancangan sistem sebagai acuan dalam
memecahkan masalah yang dibahas dalam penelitian. Bab ini juga membahas
mengenai desain tampilan antarmuka yang merupakan penghubung antara
sistem dengan pengguna.
Bab implementasi membahas mengenai uraian tahap dalam mengembangkan
hasil perancangan pada bab sebelumnya. Bab ini menjelaskan kode program
untuk mengimplementasikan hasil rancangan menjadi sebuah sistem.
Bab hasil dan pembahasan menguraikan hasil uji program setelah digunakan
dan hasil temuan saat pengujian disertai dengan pembahasan terhadap
pengujian yang dilakukan
Bab terakhir dalam penelitian yaitu bab kesimpulan yang merangkum hasil
penelitian dan memberikan saran yang dapat menjadi ide bagi penelitian
selanjutnya.
Pemanfaatan teknologi perangkat bergerak dalam pembelajaran memberi
kesempatan kepada peneliti untuk membangun aplikasi perangkat bergerak dan
mengetahui efektifitasnya dalam proses pembelajaran. Upaya penerapan teknologi
tersebut memicu penelitian tentang fasilitas yang dimiliki perangkat bergerak untuk
meningkatkan performa dan kelebihan perangkat bergerak. Beberapa penelitian
tersebut terkait dengan masalah ketersediaan data pada perangkat bergerak meliputi
sinkronisasi data dimana hasil dari penelitian tersebut akan diuraikan dalam bab ini.
Penelitian terkait dengan sinkronisasi data pada perangkat bergerak dilakukan
oleh Skogstad (2005). Skogstad menerapkan teknik sinkronisasi SyncML pada
pengembangan  Mobile Repository System pada perangkat bergerak sehingga dapat
melakukan sinkronisasi kontak alamat dan telepon. Hasil penelitian menunjukkan
bahwa perangkat bergerak mampu menyimpan kontak data yang disinkronisasikan
dari server untuk kemudian disimpan di perangkat bergerak. Penelitian yang
dilakukan oleh Skogstad tidak menggunakan basis data.
Jothipriya (2013) juga melakukan penelitian mengenai sinkronisasi data.
Jotiphriya menerapkan teknik sinkronisasi untuk mensinkronisasikan data mahasiswa
dari basis data server ke basis data pada perangkat bergerak. Jotipriya menggunakan
Microsoft Synchronization Framework sebagai teknik sinkronisasinya. Selain
Jotiphriya, Shabani dkk (2012) juga memanfaatkan teknik sinkronisasi yang sama
yaitu Microsoft Synchronization Framework untuk diterapkan pada web aplikasi
Electronic Student Management System. Teknik sinkronisasi ini hanya dapat
diterapkan pada perangkat yang mendukung piranti lunak dari Microsoft.
Teknik sinkronisasi lain dikembangkan oleh OMA (Open Mobile Alliance)
yaitu OMA DS (Open Mobile Alliance Data Synchronization) diterapkan oleh Park
dkk (2011). Park membangun Medication Reminder System yang memiliki fasilitas
untuk sinkronisasi data dengan server. Chun-Iei dkk (2010) juga memanfaatkan
teknik sinkronisasi buatan OMA yaitu SyncML untuk dimanfaatkan pada sistem yang
dibangunnya yaitu aplikasi Traditional Chinese Medicine. Teknik sinkronisasi ini
menggunakan format XML untuk pemaketan data yang akan disinkronisasikan. Hal
ini berbeda dengan penelitian yang akan dikembangkan, dimana format untuk
pemaketan data menggunakan JSON Data.
Penerapan teknik sinkronisasi data juga dilakukan oleh Ramya dkk (2012).
Ramya dkk. Menggunakan teknik SAMD sebagai teknik sinkronisasi pada
pengembangan Patient Monitoring System.  Teknik sinkronisasi ini membandingkan
nilai message digest sebagai acuan dalam melakukan proses sinkronisasi.
Syxaw XML-aware file synchronizer merupakan teknik sinkronisasi yang
diterapkan oleh Lagerspetz (2009) untuk pengembangan aplikasi pencarian dan
sinkronisasi file pada perangkat bergerak. Teknik ini digunakan untuk
mensinkronisasi file yang ada pada perangkat bergerak dengan basis data server.
Selain penelitian mengenai sinkronisasi data beberapa penelitian mencoba
untuk mengembangkan aplikasi mobile yang ditujukan untuk pembelajaran misalnya
penelitian yang dilakukan oleh Iskandar (2010). Aplikasi yang dibangun oleh
Iskandar menggunakan platform Flashlite untuk pengembangan aplikasi berbasis
grafis dan ditujukan untuk pembelajaran fisika bagi siswa SMA. Aplikasi ini diakses
pada perangkat bergerak sehingga siswa dapat belajar dimana saja dan kapan saja.
Aplikasi yang serupa dikembangkan pula oleh Anaraki (2008) yaitu aplikasi
Flash-Based Mobile Learning System. Aplikasi tersebut dimanfaatkan untuk
pembelajaran bahasa Inggris di lingkungan universitas. Baik aplikasi yang
dikembangkan oleh Iskandar (2010) dan Anaraki (2008) merupakan aplikasi offline
mobile learning  dimana data atau content yang ada di dalamnya tidak dapat
perbaharui. Oleh karena itu, bila terdapat tambahan materi baru harus mendownload
aplikasi versi yang lebih baru.
Sementara itu, Minovic dkk (2010) membuat mLMS dengan memanfaatkan
Moodle dilengkapi dengan middleware untuk menghubungkan server dengan client.
Aplikasi yang dibuat tidak dilengkapi dengan basis data sehingga dengan konsep
seperti ini, aplikasi tidak dibangun untuk mode offline atau dalam arti lain, tidak
dapat dimanfaatkan jika tidak terhubung dengan internet.
Tabel 2.1 Beberapa penelitian mengenai aplikasi perangkat bergerak


(2005)
untuk menyimpan kontak data
Tidak menggunakan basis data
pada perangkat

(2013)
Aplikasi data mahasiswa  Menggunakan teknik
sinkronisasi Microsoft
Synchronization Framework
3.  Shabani dkk
(2012)
Web aplikasi Electronic
Student Management System
Menggunakan teknik
sinkronisasi Microsoft
Synchronization Framework
4.  Park dkk.
(2011)
Medication Reminder System Menggunakan teknik
sinkronisasi OMA DS (Open
Mobile Alliance Data
Syncronization)
5.  Chun-Iei dkk
(2010)
Aplikasi Traditional Chinese
Medicine
Menggunakan teknik
sinkronisasi SyncML dengan
format data yang dikirim
berupa data XML
6.  Ramya dkk
(2012)
Pengembangan Patient
Monitoring System yang dapat
bersinkronisasi dengan
aplikasi server
Menggunakan teknik
sinkronisasi SAMD dimana
prosesnya menggunakan
perbandingan nilai message
digest

(2009)
Aplikasi pencarian file di
perangkat bergerak dengan
fasilitas sinkronsiasi file
Sinkronisasi file menggunakan
Syxaw XML-aware file
synchronizer

(2010)
Aplikasi Mobile Learning
dengan Flash Lite
Aplikasi harus diakses secara
offline

(2008)
dengan Flash Lite
Aplikasi tidak dapat
memperbaharui data
10.  Minovic dkk
(2010)
LMS Moodle yang dilengkapi
dengan middleware
Aplikasi tidak dapat diakses
secara offline karena harus
terhubung dengan internet

