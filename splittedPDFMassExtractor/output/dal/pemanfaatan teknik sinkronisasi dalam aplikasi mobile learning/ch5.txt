Setelah mengimplementasikan dan melakukan pengujian terhadap sistem
yang dibangun, maka dapat diambil beberapa kesimpulan yaitu:
1. Teknik sinkronisasi data mampu menangani ketersediaan data pada perangkat
bergerak dengan cara melakukan sinkronisasi data dengan basis data server. Data
yang telah disinkronisasikan kemudian disimpan ke dalam basis data lokal
sehingga ketika pengguna ingin mengakses aplikasi tidak perlu lagi terhubung ke
internet, tetapi cukup mengakses aplikasi secara offline dimana datanya diambil
di basis data lokal.
2. Kebutuhan waktu untuk mengakses aplikasi ketika terhubung dengan internet
membutuhkan waktu lebih lama dibanding ketika mengakses aplikasi secara
offline. Hal ini dikarenakan ketika perangkat bergerak mengakses aplikasi melalui
jaringan internet, perangkat melakukan proses sinkronisasi data. Sedangkan
ketika mengakses aplikasi tanpa terhubung dengan internet, perangkat hanya
perlu mengakses data yang berada di basis data lokal. Kemudian, hasil pengujian
yang dilakukan terhadap berbagai perangkat untuk menguji kecepatan akses
aplikasi menunjukkan bahwa kebutuhan waktu untuk sinkronisasi data pada
aplikasi baik pada saat diakses oleh satu perangkat maupun beberapa perangkat
menunjukkan hasil bahwa tidak memiliki perbedaan lama waktu yang signifikan.
Penelitian yang telah dilakukan memiliki beberapa kekurangan. Oleh karena
itu, diperlukan saran-saran yang membangun sehingga penelitian ini dapat menjadi
lebih bermanfaat. Berikut merupakan beberapa saran yang dapat dijadikan referensi
untuk penelitian berikutnya:
1. Ukuran data yang besar membutuhkan waktu untuk melakukan sinkronisasi data
yang lebih lama. Di sisi lain, koneksi internet pada perangkat bergerak dapat
kapan saja terputus. Ketika proses sinkronisasi dilakukan dan koneksi internet
terputus, maka sinkronisasi akan dibatalkan sehingga perangkat bergerak harus
melakukan sinkronisasi data kembali ketika terhubung dengan internet.
Sinkronisasi bertahap memberikan kemungkinan untuk membagi data yang akan
disinkronisasi menjadi beberapa bagian data, sehingga bila terjadi koneksi
internet yang terputus maka perangkat tidak perlu melakukan sinkronisasi ulang
secara keseluruhan.
2. Aplikasi mobile yang dikembangkan hanya diperuntukkan pada perangkat
bergerak dengan sistem operasi Android. Diharapkan pada penelitian berikutnya
dapat mengembangkan aplikasi mobile sejenis yang dapat diakses dengan
berbagai sistem operasi.

