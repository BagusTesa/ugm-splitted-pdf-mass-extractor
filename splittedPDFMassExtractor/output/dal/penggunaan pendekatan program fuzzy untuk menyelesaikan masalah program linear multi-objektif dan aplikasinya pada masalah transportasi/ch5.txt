Pada bab ini akan diberikan kesimpulan dan saran-saran yang dapat diambil
berdasarkan materi-materi yang telah dibahas pada bab-bab sebelumnya.

Kesimpulan yang dapat diambil penulis setelah menyelesaikan pembuatan
skripsi ini adalah :
1. Formulasi pendekatan program fuzzy yang dihasilkan mencakup beberapa
metode yaitu untuk ? = 1 diperoleh metode weighted average, untuk ? = 2
diperoleh metode program kuadratik, dan untuk ? ? ?? diperoleh metode
max min.
2. Solusi yang diperoleh dari pendekatan program fuzzy merupakan solusi opti-
mal Pareto atau solusi tak terdominasi.
3. Solusi yang diperoleh dengan pendekatan program fuzzy hanya akan menuju
ke beberapa titik optimal.
4. Jika nilai Uk yang diperoleh dengan menggunakan cara pertama tidak sama
dengan Uk yang diperoleh dengan cara kedua maka nilai optimal fungsi ob-
jektif Zk akan kurang dari nilai Uk yang terkecil.

Setelah membahas dan menyelesaikan masalah transportasi multi objektif
dengan pendekatan program fuzzy, penulis ingin menyampaikan beberapa saran
yaitu kedepannya dapat dilakukan penelitian lebih lanjut mengenai :
1. Penggunaan program komputer yang lebih efisien untuk menyelesaiakan masalah
transportasi dalam skala besar, sehingga dapat diaplikasikan dalam masalah
nyata.
2. Masalah transportasi multi objektif yang memaksimumkan fungsi objektifnya.
Hal ini dikarenakan dalam kehidupan nyata masalah transportasi yang terjadi
tidak selalu memiliki bentuk masalah meminimumkan. Misalkan sebuah pe-
rusahaan ingin memaksimumkan jumlah barang yang diproduksi, atau memak-
simumkan keuntungannya, maka dalam kasus ini masalah transportasi yang
digunakan adalah masalah transportasi maksimum.

