1.1        Latar Belakang dan Permasalahan
Sudah berlangsung cukup lama manusia berinteraksi dengan komputer.
Interaksi antara manusia dengan komputer telah mengelami beberapa kali evolusi.
Bermula dari antarmuka teks (Command Line Interface) berevelolusi menjadi
antarmuka berbasis grafis dua dimensi (2D Graphical User Interface), kemudian
berkembang lagi menjadi antarmuka berbasis multimedia, dan kini interaksi
manusia komputer sedang berkembang menuju ke interaksi banyak pengguna
berbasis lingkungan virtual. Perangkat masukan biasa seperti mouse dan keyboard
tidak lagi cukup untuk memenuhi kebutuhan aplikasi berbasis lingkungan virtual,
melainkan membutuhkan integrasi dari berbagai teknologi seperti teknologi untuk
mendeteksi keberadaan manusia, mengenali gerakan, percakapan atau suara,
ekspresi wajah, dan teknologi lainnya yang berkaian dengan keadaan atau tingkah
laku manusia. Perpaduan teknologi-teknologi tersebut menciptakan pengalaman
pengguna yang semakin alami (Chen et al, 2007).
Banyak penelitian dilakukan untuk membuat interaksi antara manusia
dengan komputer menjadi lebih alami layaknya interaksi antara manusia dengan
manusia lainnya. Berbagai teknik dikembangkan mulai dari layar sentuh, layar
proyektor, sampai penggunaan context-aware device. Namun interaksi manusia
komputer tidak akan bisa sealami interaksi antar manusia selama masih digunakan
device penengah  (Hardenberg dan B�rard, 2001).
Salah satu metode masukan yang dianggap cukup membuat interaksi
manusia dengan komputer menjadi alami adalah dengan menggunakan
komunikasi visual menggunakan anggota tubuh manusia semisal tangan. Tangan
manusia merupakan sebuah objek kompleks yang terdiri bagian-bagian yang
tersambung dan bersendi sehingga tangan manusia bisa digerakkan dan
membentuk pose yang berbeda-beda. Untuk membuat tangan manusia menjadi
alat masukan ke komputer berbagai cara telah dikembangkan sebelumnya. Pada
1987, Zimmerman telah mengembangkan alat masukan berupa sarung tangan
yang dapat dipakai untuk mendeteksi gerakan tangan dan mengirimkannya
sebagai perintah ke komputer (Zimmerman et al, 1987). Akan tetapi penggunaan
sarung tangan beserta kabel yang menempel padanya masih berkesan aneh dan
tidak praktis bagi pengguna, terlebih lagi biaya produksi sarung tangan tersebut
juga masih relatif mahal. Oleh karena itu dikembangkanlah pula interaksi manusia
dengan komputer menggunakan tangan berbasiskan visual. Perkembangan terkini
pada bidang visi komputer, pengolahan citra digital, dan pengenalan pola
memungkinkan untuk terciptanya sistem interaksi berbasis visual yang dapat
memenuhi performa real-time dan akurasi tinggi.
Akan tetapi pengembangan sistem interaksi visual umumnya dilakukan
pada komputer desktop. Pengembangan interaksi visual pada perangkat bergerak
sangat terbatas dikarenakan kemampuan hardware pada perangkat bergerak yang
masih berada di bawah kemampuan perangkat desktop. Perangkat bergerak dibuat
dengan mengutamakan mobilitas dan efisiensi agar dapat beroperasi lebih lama,
dapat digunakan di manapun dan kapanpun meski harus mengorbankan
kemampuan komputasinya. Kemampuan komputasi yang terbatas ini membuat
metode-metode yang dapat digunakan dalam pengolahan citra menjadi terbatas
pula, terutama jika ingin mencapai kecepatan komputasi real-time dengan akurasi
tinggi. Metode Force Field Analysis misalnya, menyajikan pendekatan baru dalam
segmentasi objek tangan yang dapat bekerja dengan baik meski objek tangan
berpotongan dengan wajah (Gupta, 2011). Akan tetapi metode tersebut memiliki
kompleksitas O(N2) yang tentunya akan sulit untuk mencapai performa real-time,
terlebih pada perangkat yang memiliki kemampuan komputasi terbatas.
Selain memiliki keterbatasan hardware, perangkat bergerak memiliki
tingkat mobilitas yang tinggi. Mobilitas yang tinggi ini sedikit banyak akan
berpengaruh pada citra yang menjadi masukan pada sistem. Pengaruh yang
diberikan di antaranya adalah munculnya variasi dalam pencahayaan dan latar
belakang (background) yang digunakan dalam pengambilan citra. Oleh karena itu
diperlukan metode yang tidak terlalu terpengaruh oleh pencahayaan yang
bervariasi serta background yang digunakan ketika mengambil citra.
Pada penelitian ini akan dikembangkan sebuah sistem pada perangkat
bergerak yang memanfaatkan interaksi visual berupa gerak dan isyarat tangan
sebagai media antarmuka sebuah aplikasi. Sistem akan dikembangkan
menggunakan metode-metode yang sesuai untuk digunakan dalam perangkat
bergerak.
Berdasarkan latar belakang yang telah dipaparkan, maka dapat
dirumuskan bahwa permasalahan yang dikaji dalam penelitian ini adalah
bagaimana membangun sistem antarmuka yang memanfaatkan interaksi visual
menggunakan gerak dan isyarat tangan pada perangkat bergerak.
Adapun batasan masalah dalam penelitian ini agar lebih mudah dipahami
dan tidak terlalu luas dalam penelitiannya yaitu sebagai berikut:
1. Gerak dan isyarat tangan dibatasi pada satu tangan saja dan gerakan-
gerakan sederhana seperti gerakan menunjuk dan menggeser
menggunakan isyarat tangan tertutup ataupun dengan satu sampai
lima jari terbuka.
2. Pengambilan citra tangan dilakukan pada area yang cukup
pencahayaan.
3. Jarak tangan dengan kamera ketika pengambilan gambar berkisar 30
sampai 60 cm.
4. Sistem akan dibuat dalam bentuk purwa rupa antarmuka sebuah
aplikasi Windows Phone.
1.4        Tujuan dan Manfaat Penelitian
Tujuan penelitian ini adalah untuk merancang sebuah sebuah sistem pada
perangakat bergerak yang dapat memanfaatkan interaksi visual berupa gerak dan
isyarat tangan sebagai media antarmuka aplikasi. Dengan adanya sistem ini
diharapkan dapat membuat interaksi manusia komputer pada piranti bergerak
menjadi lebih interaktif.
Metode penelitian yang digunakan dalam penelitian ini adalah studi
literatur, analisis, perancangan, implementasi, dan pengujian.

Pengumpulan informasi dan teori-teori mengenai pengolahan citra
digital, metode-metode yang digunakan dalam pendeteksian tangan,
serta pengaplikasiannya pada bidang interaksi manusia komputer dari
buku, karya tulis ilmiah, artikel, dan jurnal yang diperoleh dari
perpustakaan dan internet.

Untuk dapat memahami sistem yang akan dikembangkan perlu
dilakukan identifikasi terhadap spesifikasi dari sistem. Sistem yang
dikembangkan merupakan sistem yang mampu mengambil masukan
dari pengguna melalui interaksi visual menggunakan gerak dan isyarat
tangan. Sistem akan mampu melakukan perintah tertentu sesuai dengan
gerak dan isyarat tangan tersebut. Kemudian dari hasil studi literatur
dianalisis pula metode yang tepat untuk digunakan di dalam sistem.

Pembahasan dilakukan dengan membahas hasil penelitian yang didapat
dari pengujian yang telah dilakukan. Semua hasil penelitian dicatat dan
dibahas secara logis sesuai dengan teori yang sudah ada.

Berdasarkan hasil analisis, dilakukan perancangan dengan
memodelkan sistem. Dengan pemodelan ini akan diperoleh suatu
gambaran mengenai struktur, cara kerja dan pendekatan penyelesaian
masalah dari sistem yang dikembangkan. Data masukan dari sistem
adalah data frame video yang dimasukkan ke dalam sistem secara real-
time. Pada tiap-tiap frame tersebut dilakukan pemrosesan mulai dari
pendeteksian tangan untuk menentukan posisi area tangan. Setelah
diketahui posisi area tangan, dilakukan pendeteksian posisi ujung jari
pada area tersebut. Setelah itu, dilakukan pula pengenalan terhadap
isyarat tangan yang terdeteksi berdasarkan jumlah jari. Kemudian
dilakukan pengecekan apakah jari menyentuh objek virtual atau
memberikan isyarat tertentu. Jika memang maka terdeteksi maka akan
dilakukan perintah sesuai dengan isyarat atau gerakan tangan yang
terdeteksi. Gambar 1.1 Diagram alur kerja sistem Gambar 1.1
menggambarkan alur kerja sistem ini.

Pada tahap ini dilakukan pengujian terhadap sistem yang telah
dibangun. Pengujian dilakukan dengan cara melakukan pengetesan
terhadap performa sistem dalam mengenali keberadaan tangan manusia
dan gerak isyaratnya. Beberapa pengujian yang akan dilakukan adalah
sebagai berikut.
a. Pengujian kemampuan sistem dalam melakukan segmentasi
objek tangan dari citra.
b. Pengujian kemampuan sistem dalam mendeteksi posisi ujung
jari tangan.
c. Pengujian kemampuan sistem dalam mendeteksi perintah yang
diberikan oleh isyarat dan gerak tangan yang terdeteksi.
Gambar 1.1 Diagram alur kerja sistem
Sistematika dalam penulisan tugas akhir ini akan terdiri dari enam bab,
yaitu:
Berisi mengenai latar belakang, rumusan masalah, batasan masalah,
tujuan penelitian, manfaat penelitian, metodologi penelitian dan
sistematika penulisan.
Bab ini memuat pembahasan mengenai penelitian terdahulu yang
digunakan sebagai bahan referensi dalam penulisan penelitian ini.
Selain itu juga memuat penjelasan yang membedakan penelitian ini
dengan peneltian sejenis yang pernah ada sebelumnya.
Bab ini berisi teori-teori yang menjadi landasan dalam penulisan
penelitian ini yaitu mengenai citra digital, pengolahan citra digital,
metode segmentasi citra, serta metode pengenalan objek pada citra.
Bab ini berisi perancangan sistem yang akan dikembangkan.
Perancangan meliputi perancangan akuisisi citra, perancangan proses
pengenalan tangan, dan perancangan pengujian.
Pada bab ini akan dijelaskan hasil implementasi dari sistem yang telah
dibangun berdasarkan perancangan yang telah dilakukan sebelumnya.
Pada bab ini akan dipaparkan hasil pengujian yang diperoleh setelah
sistem selesai diimplementasikan. Disertai pula pembahasan hasil
pengujian tersebut.
Bab ini berisi mengenai kesimpulan dari penelitian yang telah
dilakukan dan saran untuk pengembangan penelitian selanjutnya.

