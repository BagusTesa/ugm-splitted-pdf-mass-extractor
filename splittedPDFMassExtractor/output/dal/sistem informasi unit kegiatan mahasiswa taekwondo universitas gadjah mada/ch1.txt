1.1 Latar belakang
Kemajuan teknologi masa kini berkembang dengan sangat pesat. Hal ini dapat
dibuktikan dengan banyaknya inovasi-inovasi yang telah dibuat. Teknologi berperan
besar dalam meningkatkan kualitas hidup manusia dan dapat mendukung terciptanya
kinerja suatu perusahaan yang optimal dan dapat menyelesaikan pekerjaan secara
lebih baik dan efisien.
Pada zaman era globalisasi ini teknologi menimbulkan dampak positif dalam
perkembangan perekonomian melalui teknologi informasi. Hal ini juga membuat
manusia mengetahui informasi-informasi terbaru secara cepat dan mudah, baik dalam
bidang usaha ataupun bidang olahraga. Bidang olahraga memiliki beraneka ragam
jenis seperti basket, volley, karate, dan lain-lain. Salah satu bidang olahraga adalah
beladiri taekwondo.
Beladari taekwondo ini berasal dari Korea. Olahraga ini juga menjadi
olahraga nasional Korea. Taekwondo adalah seni beladiri yang banyak dimainkan
tingkat dunia dan juga dipertandingkan dalam olimpiade. Masyarakat Indonesia
khususnya sudah banyak yang telah menekuni beladari taekwondo ini sejak usia dini
hingga dewasa.
Unit Kegiatan Mahasiswa (UKM) Universitas Gadjah Mada merupakan suatu
lembaga yang memberikan sarana dan prasarana bagi mahasiswa Universitas Gadjah
Mada untuk menyalurkan hobi, minat, bakat, seni dan lain sebagainya. Khususnya
UKM Taekwondo yang menyediakan sarana dan prasarana bagi mahasiswa
Universitas Gadjah Mada ataupun masyarakat umum untuk berlatih dan berprestasi
dalam bidang seni beladiri.
Unit Kegiatan Mahasiswa Taekwondo memerlukan media yang meliput
tentang informasi terbaru dari UKM Taekwondo Universitas Gadjah Mada seperti
profil UKM Taekwondo, prestasi-prestasi yang diraih, dokumentasi kegiatan, dan
data keanggotaan UKM Taekwondo bukan hanya dari media sosial facebook ataupun
twitter tetapi juga dari website resmi UKM Taekwondo Universitas Gadjah Mada.
Proses pendaftaran anggota UKM Taekwondo yang masih menggunakan formulir
pendaftaran, dimana membuat calon anggota harus datang ke sekretariat UKM
Taewondo sehingga kurang efektif dan efesien. Pendaftaran seperti ini juga membuat
penumpukan dokumentasi-dokumentasi atau formulir pendaftaran di sekretariat UKM
Taekwondo Universitas Gadjah Mada. Dokumentasi-dokumentasi tersebut memiliki
kemungkinan untuk hilang sehingga membuat pengurus UKM Taekwondo harus
mendata ulang anggota. Begitu juga dengan pendaftaran ujian kenaikan tingkat yang
masih menggunakan Microsoft excel yang dapat menyebabkan kesalahan pada saat
meng-input-kan data anggota ke excel. Hal ini dapat menyebabkan kesalahan
penulisan nama pada saat penyetakan sertifikat ujian. Dalam Melakukan pengecekan
anggota yang berhak mengikuti ujian kenaikan tingkat pelatih harus merekap satu per
satu presensi dan iuran angggota yang telah memenuhi persyaratan.
Dalam UKM Taekwondo ini memerlukan suatu pendaftaran online yang
mampu mengolah semua data-data pendaftaran yang masuk perperiode dan pendataan
anggota yang telah mengikuti ujian. Hal ini dapat mempermudah ketua UKM
Taekwondo dalam mendata anggota dan mempermudah dalam pencarian anggota.
Untuk itu diperlukan suatu sistem informasi berbasis web yang memberikan
informasi dengan cepat dan akurat terkait dengan kegiatan, jadwal latihan, prestasi-
prestasi yang diraih, dokumentasi kegiatan, dan data keanggotaan UKM Taekwondo
itu sendiri. Selain itu sistem menyediakan form pendaftaran online sehingga
memudahkan mahasiswa Universitas Gadjah Mada untuk mendaftar sebagai anggota
UKM Taekwondo. Sistem ini diharapkan mampu membantu kinerja pengurus UKM
Taekwondo agar lebih efektif dan efisien, sehingga pengurus tidak perlu menunggu
mahasiswa datang untuk mendaftar. Sebaliknya mahasiswa tidak perlu datang ke
UKM Taekwondo untuk melakukan pendaftaran. Sistem ini dibangun dapat
melakukan pengecheckan anggota yang telah memenuhi persyaratan ujian kenaikan
tingkat. Disamping itu diharapkan mampu melakukan pencarian data anggota dengan
cepat jika diperlukan. Sistem ini diharapkan pula mampu menampung kritik dan
saran dari mahasiswa Universitas Gadjah Mada maupun masyarakat umum demi
membangun UKM Taekwondo Universitas Gadjah Mada yang lebih baik.
Permasalahan pada tugas akhir ini adalah bagaimana membuat Sistem
Informasi Unit Kegiatan Mahasiswa Taekwondo Universitas Gadjah Mada yang
didalamnya terdapat proses pendataan anggota baru dan anggota yang mengikuti
ujian kenaikan tingkat serta melakukan pengecheckan secara otomatis anggota yang
telah memenuhi persyaratan ujian kenaikan tingkat.
1.3 Batasan Masalah
Dalam pembuatan sistem informasi ini penulis memberikan beberapa batasan
masalah agar dalam menyusun tugas akhir ini menjadi terarah dan sesuai dengan
ketentuan yang ada. Batasan-batasan masalah meliput:
1. Sistem ini hanya membahas tentang pendaftaran anggota baru dan ujian
kenaikan tingkat pada Taekwondo Universitas Gadjah Mada, tidak
membahas tentang raport anggota selama mengikuti pelatihan.
2. Sistem tidak membahas pencetakan sertifikat ujian kenaikan tingkat.
3. Sistem tidak membahas tentang pendaftaran kejuaraan yang akan
diselenggarakan oleh Taekwondo Universitas Gadjah Mada.
4. Sistem ini tidak dapat menampilkan form pendaftaran ujian kenaikan
tingkat sebelum divalidasi oleh pelatih.
5. Pengembangan sistem ini tidak memperhatikan masalah keamanan.
Tujuan dari sistem informasi ini adalah untuk merancang dan membangun
sistem pendaftaran online anggota baru dan ujian kenaikan tingkat taekwondo di
Ada beberapa manfaat yang ingin dicapai dalam penelitian adalah:
1. Memberikan kemudahan kepada mahasiswa dalam melakukan pendaftaran
anggota baru.
2. Memberikan kemudahan kepada anggota dalam melakukan pendaftaran ujian
kenaikan tingkat.
3. Memberikan informasi kepada masyarakat mengenai profil Taekwondo
Universitas Gadjah Mada dan event-event terbaru yang akan diselenggarakan
oleh Taekwondo Universitas Gadjah Mada
4. Memberikan kemudahan kepada ketua UKM Taekwondo Universitas Gadjah
Mada dalam hal pelaporan data anggota baru dan data anggota yang
mengikuti ujian kenaikan tingkat.
Metode-metode yang digunakan dalam penelitian ini adalah sebagai berikut:

Metode pengumpulan data adalah hal yang pertama kali dilakukan
untuk membuat perencanaan sebuah sistem. Metode ini dibagi menjadi
beberapa metode, antara lain:

Metode ini dilakukan untuk mengumpulkan data secara langsung dari

Wawancara dilakukan untuk mengumpulkan informasi-informasi
mengenai UKM Taekwondo. Wawancara dilakukan dengan ketua

Studi literature dilakukan untuk mengumpulkan data melalui berbagai
referensi seperti buku, artikel dan materi yang dapat dijadikan acuan
dalam pembuatan Sistem Informasi Pendaftaran Anggota dan Ujian
kenaikan tingkat Taekwondo Universitas Gadjah Mada.

Tahap analisis kebutuhan sistem ini menganalasis kebutuhan yang
dibutuhkan oleh sistem. Menjelaskan kebutuhan fungsional dan non
fungsional pada sistem.

Dalam tahap ini dilakukan penerapan data pada sistem menggunakan
Diagram Alir Data (DAD), proses bisnis yang bekerja pada sistem
menggunakan Flowchat, perancangan basisdata, perancangan struktur menu
dan tahap perancangan antarmuka.

Pada tahap ini dilakukan pembangunan sistem berdasarkan analisis
dan perancangan yang telah dibuat sebelumnya.

Tahap pengujian merupakan tahap terakhir dari pembuatan sistem.
User mencoba sistem untuk memakai sistem dan melaporkan jika masih
terjadi kekurangan untuk kemudian disempurnakan lagi.
Untuk menjelaskan sistem yang dibuat, maka sistematika penulisan laporan
tugas akhir ini dibagi menjadi beberapa bab, antara lain :
Bab satu menguraikan tentang latar belakang masalah, perumusan masalah,
batasan masalah, tujuan penelitian, manfaat penelitian, metode
pengumpulan data serta sistematika penulisan.
Bab dua berisi uraian sistematis tentang informasi dari hasil penelitian yang
menjadi referensi yang berkaitan dengan penelitian yang sedang dilakukan.
Bab tiga berisi penjelasan mengenai teori-teori dan prinsip ilmu yang
menjadi landasan dalam penelitian yang dilakukan.
Bab empat berisi uraian analisis serta perancangan sistem yang menjadi
konsep dalam pembuatan sistem yang akan dibuat. Analisis kebutuhan ini
meliputi analisis sistem, rancangan proses, rancangan basis data, dan
rancangan antar muka pengguan.
Bab lima menguraikan tentang implementasi hasil perancangan desain yang
menampilkan aplikasi antar muka menggunakan bahasa pemrograman PHP
dan MySQL disertai cara kerja dan penggunaan program.
Bab enam ini membahas tentang pengujian dan pembahasan sistem
yang menguji kesesuaian sistem dengan perancangan yang sudah dibuat.
Pengujian bertujuan untuk mengevaluasi kinerja sistem, mengurangi adanya
kesalahan serta memastikan sistem dapat digunakan dengan baik.
Bab ini berisi kesimpulan dan saran dari hasil penelitian yang telah
dilakukan.
Memuat tentang sumber referensi literature yang digunakan dalam
menyusun laporan tugas akhir ini.

