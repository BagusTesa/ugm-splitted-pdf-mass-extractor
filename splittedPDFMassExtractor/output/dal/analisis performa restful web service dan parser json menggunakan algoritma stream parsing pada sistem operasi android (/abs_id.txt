oleh.
Web Service memungkinkan dua perangkat elektronik untuk
berkomunikasi melalui Internet. Web Service digunakan sebagai suatu fasilitas
yang disediakan oleh suatu website untuk menyediakan layanan (dalam bentuk
informasi) kepada sistem lain, sehingga sistem lain dapat berinteraksi dengan
sistem tersebut melalui layanan-layanan (service) yang disediakan oleh suatu
sistem yang menyediakan web service. Salah satu arsitektur web service adalah
REST (Representational State Transfer). REST dapat menyimpan data informasi
dalam tipe teks, baik yang berformat XML maupun JSON sehingga data ini dapat
diakses oleh sistem lain walaupun berbeda platform, sistem operasi, maupun
bahasa compiler. JSON (JavaScript Object Notation) adalah format pertukaran
data yang ringan, mudah dibaca dan ditulis oleh manusia, serta mudah
diterjemahkan dan dibuat (generate) oleh komputer. Untuk membaca suatu data
dengan format JSON, diperlukan suatu perantara yang disebut parser. Ada begitu
banyak parser berbasis bahasa pemrograman Java yang dapat digunakan untuk
membaca data JSON. Setiap parser memiliki kelebihan dan kekurangan masing-
masing. Salah satu kelebihan tersebut adalah dalam hal kecepatan.
Pada penelitian ini dilakukan analisis performa RESTful web service dan
JSON parser pada Android. Parser yang digunakan adalah SimpleJason (org.json),
Jackson, dan GSON. Hal ini bertujuan untuk mengetahui performa RESTful web
service dan perbandingan performa kecepatan antara org.json, Jackson, dan
GSON. Hasil pengujian pada penelitian ini menunjukkan RESTful web service
yang dibuat dengan PHP mampu memberikan respon dari setiap request. Parser
Jackson memiliki kecepatan lebih baik daripada org.json dan GSON dalam
melakukan proses parsing menggunakan algoritma stream parsing pada JSON
array.
Kata kunci: web service, RESTful, JSON, JSON parser, Stream parsing, Android,
kecepatan parsing

