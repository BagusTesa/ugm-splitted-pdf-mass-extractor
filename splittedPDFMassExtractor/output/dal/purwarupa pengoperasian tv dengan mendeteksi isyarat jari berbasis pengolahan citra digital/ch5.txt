Dari penelitian yang telah dilakukan dapat diambil kesimpulan sebagai berikut:
1. Dalam penghitungan jumlah jari tangan sebagai input belum berjalan
dengan baik.
2. Metode yang diterapkan untuk mengenali objek dengan memilah warna
dengan latar belakangnya sudah berjalan dengan baik.
3. Intensitas cahaya terbaik dalam pengujian sebesar 12 lux.
4. Jarak tangan terhadap webcam yang terbaik adalah 70 cm.
5. Background terbaik dalam pengujian berwarna biru.
Beberapa saran dari penulis untuk pengembangan selanjutnya antara lain:
1. Input dibuat lebih banyak lagi, karena sementara ini hanya menggunakan 5
input saja.
2. Pendeteksian ujung jari dibuat lebih sempurna, sehingga masukkan citra
berupa ujung jari bisa lebih stabil.

