Berdasarkan hasil penelitian ini diperoleh desain magnet siklotron proton 13
MeV yang memiliki dimensi 1,96 m � 1,3 m � 1,21 m. Desain magnet siklotron
ini telah memenuhi persyaratan desain magnet siklotron, yaitu :
1. Induksi medan magnet B pada bidang tengah kutub magnet telah mendekati
kurva isokronous pada 0,05 m < r < 0,41 m. Pengaturan B supaya mengikuti
kurva isokronous dilakukan dengan mengatur lebar celah hill pada 0,09 m <
r < 0,2 m dan mengatur luas hill pada 0,2 m < r < 0,41 m.
2. Desain magnet sudah memberikan pemfokusan ke arah aksial dan radial
yang ditunjukkan dengan nilai ?r dan ?z lebih dari 0. Pemfokusan pada
daerah pusat diberikan dengan menambahkan bump.
3. Kurva ?z terhadap ?r hanya melintasi garis resonansi ?r =1 pada saat energi
partikel masih rendah dan saat partikel telah diekstraksi.
4. Desain magnet mengakibatkan pergeseran fase yang cukup kecil yaitu � 13�.

1. Perlu dilakukan simulasi lintasan berkas, untuk mengetahui apakah desain
magnet ini dapat mencapai energi 13 MeV.
2. Karena desain magnet ini merupakan pendekatan yang dijadikan dasar dalam
pembuatan magnet siklotron, maka setelah magnet dibuat perlu dilakukan
shimming perubahan model kutub magnet berdasarkan hasil pengukuran
(mapping).

