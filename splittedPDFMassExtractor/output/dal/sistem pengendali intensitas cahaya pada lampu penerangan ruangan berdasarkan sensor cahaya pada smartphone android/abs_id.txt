Penerangan ruangan dengan intensitas cahaya berlebihan tentu dapat
menyilaukan mata, sebaliknya intensitas cahaya yang terlalu gelap dapat membuat
benda-benda di dalam ruangan tidak dapat terlihat dengan jelas, kondisi intensitas
cahaya yang berubah-ubah juga dapat menganggu dalam melakukan aktivitas di
dalam ruangan. Kondisi gelap terang pada suatu ruangan dapat berubah-ubah
mengikuti kondisi di luar ruangan, misalnya saja pada saat kondisi mendung pada
siang hari, matahari terbit pada pagi hari ataupun matahari terbenam pada sore
hari. Intensitas cahaya yang berubah sewaktu-waktu serta terus-menerus dan tidak
sesuai dengan kebutuhan pengelihatan pada suatu ruangan dapat menganggu
pengguna ruangan dalam melakukan suatu aktivitas. Oleh karena itu, diperlukan
sebuah sistem pengendali intensitas cahaya pada lampu penerangan ruangan yang
dapat menyesuaikan dengan kondisi intensitas cahaya pengguna ruangan.
Untuk dapat menyesuaikan kondisi intensitas cahaya lampu penerangan
ruangan, pada penelitian ini digunakan sensor cahaya pada smartphone Android
disertai dengan aplikasi pengguna (antarmuka) yang terhubung pada sebuah
saklar. Saklar dan smartphone Android bekerja secara terpadu melalui
konektivitas WiFi. Perancangan aturan main atau protokol komunikasi data antara
saklar dengan aplikasi pengguna dibutuhkan agar keduanya dapat bekerja secara
terpadu. Hasil implementasi sistem tersebut diuji dengan beberapa pengujian
seperti pengujian fungsi koneksi WiFi, pengujian perbandingan intensitas cahaya
sensor cahaya smartphone Android dengan luxmeter, pengujian dimmer lampu,
pengujian protokol, serta pengujian sistem pengendalian.
Hasil dari penelitian yang dilakukan diperoleh bahwa nilai intensitas
cahaya berdasarkan pembacaan sensor cahaya pada smartphone Android yang
digunakan pada penelitian, tidak akurat untuk melakukan pengendalian karena
tidak sesuai dengan intensitas cahaya yang sebenarnya berdasarkan alat ukur
Luxmeter LX-100. Pada sistem pengendalian dengan setpoint 403 Lux
berdasarkan pembacaaan sensor cahaya pada smartphone Android diperoleh
waktu naik sebesar 1.6 detik, waktu puncak sebesar 2,2 detik, overshoot sebesar
59%, dan settling time sebesar 2,4 detik.
Kata Kunci : sensor cahaya, smartphone Android, WiFi, pengendali intensitas
cahaya

