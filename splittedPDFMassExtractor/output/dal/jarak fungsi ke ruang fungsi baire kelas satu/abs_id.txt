Jarak suatu titik x ke himpunan A dalam suatu ruang metrik dinyatakan se-
bagai supremum jarak titik tersebut ke setiap titik di dalam himpunan A. Dalam
tesis ini, akan dibahas jarak fungsi f : X ? E ke B1(X,E), dengan B1(X,E)
menyatakan ruang fungsi Baire kelas satu dari ruang metrik X ke ruang Banach E.
Untuk mencari estimasi jarak tersebut, akan digunakan dua konsep, yaitu konsep in-
deks fragmentability fungsi dan konsep osilasi fungsi. Sebagai salah satu akibat dari
hasil estimasi dengan menggunakan konsep osilasi, diperoleh bahwa untuk setiap
fungsi kontinu terpisah (separately continuous) f : X �K ? R dengan X ruang
topologi completely metrizable dan K ruang topologi kompak, terdapat himpunan
D ? X sehingga D himpunan G? dan dense di X serta f kontinu pada D �K.

