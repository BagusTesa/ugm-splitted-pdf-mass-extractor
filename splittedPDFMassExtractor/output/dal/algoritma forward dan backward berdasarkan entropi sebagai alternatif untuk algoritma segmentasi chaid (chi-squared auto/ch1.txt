Chi-square Automatic Interaction Detection (CHAID) adalah merupakan
suatu kasus khusus dari algoritma pendeteksian interaksi otomatis yang biasa
disebut Automatic Interaction Detection (AID). Algoritma ini dapat memodelkan
respon, dimana variabel dependen dan independennya berupa variabel kategorik
(Kass, 1980). Menurut Lehmann dan Eherler (2001), CHAID merupakan salah
satu metode dependensi yang sering diaplikasikan dalam proses segmentasi.
Menurut Magidson dalam Bagozzi (1994), CHAID adalah sebuah
algoritma statistik yang akan menggabungkan beberapa kategori dari sebuah
prediktor yang dianggap homogen berdasarkan sebuah variabel terikat, dan akan
mengatur juga kategori yang dianggap heterogen. Jadi CHAID akan
menggabungkan kategori-kategori yang sama dan memisahkan yang dianggap
berbeda dengan yang lain, secara signifikan berdasarkan nilai uji chi square-nya.
Algoritma CHAID saat ini dianggap sebagai sebuah algoritma umum
dalam proses segmentasi variabel-variabel kualitatif, dan kegunaannya telah
dikenal luas diantara para peneliti di bidang sosial. Menurut Magidson dan
Vermunt (2005), algoritma CHAID telah terbukti sebagai sebuah pendekatan yang
efektif untuk dapat memperoleh suatu segmentasi dengan cepat tetapi penuh arti,
dimana segmen-segmennya didefinisikan berdasarkan variabel demografis dan
atau variabel-variabel lainnya yang bersifat prediktif untuk variabel kriteria
(dependen) kategoriknya. Algoritma ini telah banyak diaplikasikan dalam bidang
pemasaran, riset-riset politik, kesehatan, pendidikan, dan lain sebagainya.
CHAID menggunakan serangkaian prosedur seleksi forward (arah maju)
dalam proses pemisahan atau segmentasi suatu himpunan individu awal menjadi
subgrup-subgrup, dengan tujuan untuk menunjukkan perbedaan antar subgrup
secara maksimal dalam profil responnya. Selanjutnya hasil dari proses tersebut
disajikan dalam bentuk sebuah diagram pohon (Villardon dkk., 2009).
Secara singkat, CHAID menggunakan algoritma dengan tiga langkah
utama, yaitu:
1. Untuk masing-masing prediktor, akan digabungkan beberapa kategori
yang memiliki pola respon yang sama. Jenis prediktor akan menentukan
pengelompokan yang bisa diterima.
2. Selanjutnya akan dicari prediktor terbaik melalui uji signifikansi chi-
square terhadap semua tabulasi silang (cross-tabulation) yang terbentuk
antara respon dan masing-masing prediktor.
3. Akan dilakukan pemisahan atau segmentasi sampel awal dengan
menggunakan kategori-kategori prediktor yang memiliki nilai p-value
paling rendah.
Algoritma CHAID tersebut didasarkan pada perbedaan independensi marginal.
Avila dkk. (2004) mengemukakan bahwa algoritma CHAID tidak dapat
dengan pasti menunjukkan adanya interaksi dalam data. Dengan kata lain, ketika
beberapa interaksi kompleks ada dalam data, algoritma CHAID tidak dapat
mendeteksi secara otomatis keseluruhan interaksi tersebut. Kesalahan yang terjadi
pada prosedur CHAID tersebut adalah didasarkan pada independensi marginalnya.
Penggunaan marginalisasi ini bersifat tidak stabil dikarenakan CHAID tidak
memberlakukan persyaratan lipatan (collapsibility) untuk menghindari fenomena
yang dikenal sebagai Paradoks Simpson.
Oleh karena itu diperlukan independensi bersyarat untuk mempelajari
contoh penggunaan prosedur CHAID tersebut. Dengan kata lain, untuk suatu
kombinasi kategori dari dua variabel independen yang dilibatkan, maka responnya
akan tergantung pada variabel independen yang ketiga. Apabila seperti ini, maka
akan menjadi lebih masuk akal untuk menyimpulkan suatu prosedur atau
algoritma berdasarkan pada independensi bersyaratnya dibandingkan pada
independensi marginalnya.
Diperlukan alternatif algoritma lain, untuk algoritma CHAID, yang dapat
menghindari munculnya fenomena tersebut. Disini akan dibahas suatu algoritma
backward (arah mundur) yang diawali dari sebuah himpunan prediktor lengkap
(atau diagram pohon utuh) dan kemudian mengeliminasi prediktor-prediktor
tersebut secara bertahap. Akan ditunjukkan bahwa algoritma ini dapat mendeteksi
interaksi secara otomatis berdasarkan pada independensi bersyarat dibandingkan
dengan independensi marginal yang biasa digunakan, dan kemudian akan
digunakan entropi untuk menguji independensinya tersebut. Akan dibahas pula
sebuah algoritma  forward, yang ekuivalen dengan algoritma klasik CHAID, akan
tetapi algoritma ini akan didasarkan pada entropi. Selanjutnya akan ditunjukkan
apakah terjadi perbedaan susunan pohon keputusan yang dihasilkan oleh ketiga
algoritma tersebut, dimana hal ini berarti ingin ditunjukkan apakah terjadi
perbedaan hasil segmentasi diantara algoritma CHAID dan algoritma forward
serta backward berdasarkan entropi.
1.2. Rumusan dan Batasan Masalah
Berdasarkan latar belakang dan permasalahan, berikut rumusan masalah:
1. Bagaimana menunjukkan letak kelemahan dalam algoritma CHAID?
2. Bagaimana menyusun algoritma alternatif untuk algoritma CHAID, yaitu
algoritma forward dan backward yang didasarkan pada entropi?
3. Bagaimana menunjukkan perbedaan hasil segmentasi dari algoritma
CHAID dan algoritma forward serta backward tersebut melalui
pemeriksaan terhadap susunan pohon keputusan yang dihasilkan?
1.3. Tujuan dan Manfaat Penelitian
Berdasarkan pada apa yang telah diuraikan pada latar belakang di atas
maka tujuan dari penulisan tesis ini adalah:
1. Mempelajari adanya kelemahan dari algoritma segmentasi CHAID.
2. Menyusun algoritma alternatif untuk algoritma segmentasi CHAID, yaitu
algoritma forward dan backward yang didasarkan pada entropi.
3. Menyusun pohon keputusan dari algoritma klasik CHAID dan algoritma
forward serta backward yang didasarkan pada entropi.
4. Melakukan analisis segmentasi terhadap hasil dari pohon keputusan algoritma
klasik CHAID dan algoritma forward serta backward yang didasarkan pada
entropi, untuk menunjukkan perbedaan susunan segmentasi yang dihasilkan
oleh ketiga algoritma tersebut.
Manfaat yang diharapkan diperoleh dari penulisan tesis ini adalah:
1. Bagi penulis diharapkan dapat menambah pemahaman mengenai penggunaan
pohon keputusan dalam melakukan analisis terhadap data kategorik,
khususnya yang menggunakan algoritma CHAID dan beberapa algoritma
alternatifnya.
2. Dapat memberikan sumbangan pemikiran bagi ilmu pengetahuan bidang
matematika dan statistika pada khususnya mengenai prosedur algoritma
CHAID, menunjukkan kelemahannya sekaligus menjelaskan algoritma-
algoritma alternatif yang diharapkan bisa membantu kelemahan yang muncul
tersebut.
3. Bagi pembaca adalah sebagai motivasi untuk mengembangkan lebih lanjut
terhadap apa yang diperoleh pada penelitian ini.

CHAID (Chi-squared Automatic Interaction Detector) pertama kali
An Exploratory Technique for
Investigating Large Quantities of Categorical Data
1980. Prosedurnya merupakan bagian dari teknik terdahulu yang dikenal dengan
Automatic Interaction Detector (AID), dan menggunakan statistik chi-square
sebagai alat utamanya. Jadi dengan kata lain, CHAID adalah sebuah kasus khusus
dalam algoritma AID (Kass, 1980).
AID sendiri merupakan teknik yang sudah bertahun-tahun digunakan.
Pada tahun 1975, dalam jurnalnya yang berjudul Significance Testing in
Automatic Interaction Detection (AID), G.V. Kass menjelaskan bahwa AID
mengoperasikan vektor-vektor data yang terdiri dari sebuah variabel dependen
dan setidaknya satu variabel prediktor yang dikategorikan (Kass, 1975). Tujuan
metode ini adalah untuk memisahkan data secara berurutan dengan pembagian
biner menjadi beberapa subgrup. Pada tiap tahap, pembagian sebuah grup menjadi
dua bagian didefinisikan oleh salah satu variabel prediktor, sebuah himpunan
bagian dari kategori-kategorinya mendefinisikan salah satu bagian, dan sisa
kategori lainnya mendefinisikan bagian yang lain. Pada AID, prediktornya
memiliki dua tipe utama, (Kass, 1980).
Beberapa modifikasi penting yang dibuat oleh CHAID dan relevan dengan
AID yang standar adalah, adanya pengujian signifikansi sebagai akibat dari
penggunaan prediktor yang paling signifikan (bukan dengan yang paling bersifat
menjelaskan), adanya pemisahan secara multi-way (tidak hanya secara biner),
serta pada CHAID digunakan sebuah tipe prediktor baru yang khususnya akan
berguna dalam menangani informasi yang hilang, yaitu prediktor floating (Kass,
1980).
Menurut Avila dkk. (2004), CHAID adalah salah satu metode analisis
segmentasi yang merupakan suatu teknik dengan target utamanya adalah untuk
mendeteksi adanya interaksi dalam sebuah model prediksi. CHAID sendiri
didasarkan pada marginalisasi, yaitu dibentuknya tabulasi silang dua arah untuk
tiap prediktor dengan responnya. Namun, hal ini bersifat tidak stabil. Ketika
beberapa interaksi kompleks muncul dalam data, maka algoritma CHAID tidak
dapat mendeteksi kesemuanya. Sehingga menurut Avila dkk. (2004), CHAID
tidak selalu berhasil seperti yang diasumsikan.
Oleh karena itu, alternatif yang diusulkan untuk algoritma CHAID adalah
algoritma forward dan backward berdasarkan entropi. Penggunaan entropi disini
dikarenakan selisih nilai entropi memiliki keeratan hubungan dengan fungsi rasio
likelihood. Pada dasarnya algoritma forward ini ekuivalen dengan algoritma
klasik CHAID, akan tetapi disini digunakan entropi untuk menguji independensi
marginalnya. Sedangkan pada algoritma backward akan dilakukan eliminasi yang
dimulai dengan satu set penuh prediktor (atau satu diagram pohon penuh), dimana
prosedur eliminasi ini akan didasarkan pada perbedaan independensi bersyarat,
yang selanjutnya diuji menggunakan konsep entropi. Dalam teori informasi,
entropi adalah suatu ukuran ketidakpastian yang terhubung dengan suatu variabel
random, dalam pengertian bahwa ukuran tersebut akan mengukur jumlah
informasi yang terkandung di dalamnya (Avila dkk., 2004).

Metodologi yang digunakan dalam penelitian ini adalah studi literatur.
Langkah-langkah yang dilakukan penulis adalah sebagai berikut:
1. Mencari dan menentukan jurnal yang akan dijadikan bahan acuan.
2. Mengumpulkan jurnal-jurnal lain yang relevan dengan materi dalam jurnal
acuan.
3. Mempelajari buku-buku pendukung yang berkaitan dengan topik
permasalahan penelitian.
4. Mempelajari dan membahas topik penelitian yang meliputi: algoritma
CHAID dalam melakukan analisis untuk mendeteksi secara otomatis interaksi
yang ada dalam data, menunjukkan kelemahan algoritma CHAID, serta
menjelaskan tentang algoritma forward dan backward yang didasarkan pada
entropi, sebagai alternatif untuk algoritma klasik CHAID.
5. Melakukan percobaan melalui contoh kasus untuk menunjukkan penggunaan
algoritma CHAID, serta penggunaan algoritma forward dan backward yang
didasarkan pada entropi sebagai alternatifnya, kemudian ditunjukkan
perbedaan susunan segmentasi yang dihasilkan ketiganya.
6. Menyusun laporan penelitian sesuai dengan buku petunjuk penulisan tesis
yang diberlakukan.

Tesis ini disusun dengan sistematika penulisan sebagai berikut.
BAB I berisikan latar belakang dan permasalahan, tujuan penelitian,
manfaat penelitian, tinjauan pustaka, metodologi penelitian, dan sistematika
penulisan.
BAB II berisikan landasan teori yang dipergunakan sebagai alat untuk
membahas bab-bab selanjutnya. Landasan teori yang diberikan meliputi teori
dasar yang menunjang pembahasan tentang algoritma CHAID dan algoritma
alternatifnya yang didasarkan pada entropi, diantaranya tentang pohon keputusan,
uji chi-square, uji rasio likelihood, dan entropi.
BAB III berisikan penjelasan tentang CHAID sebagai algoritma inti yang
selanjutnya akan didapatkan alternatifnya.  Disini akan ditunjukkan definisi dari
CHAID, jenis variabel-variabelnya, algoritmanya, dan lain-lain.
BAB IV adalah pembahasan, yaitu penjelasan menyeluruh dalam
pembentukan algoritma alternatif untuk CHAID. Pada bab ini akan dipaparkan
mengenai kelemahan yang muncul dalam algoritma CHAID, yaitu munculnya
Paradoks Simpson, berikut juga penjabaran penggunaan rasio likelihood yang
mempunyai hubungan erat dengan selisih nilai entropi. Selanjutnya akan
dipaparkan pula tentang dua algoritma alternatif untuk CHAID yang dapat pula
melakukan pendeteksian interaksi secara otomatis dimana keduanya didasarkan
pada entropi.
BAB V berisikan contoh kasus. Pada bab ini akan dibahas penerapan
algoritma CHAID, serta penerapan algoritma forward dan backward yang
didasarkan pada entropi. Kemudian membandingkan susunan segmentasi dari
penerapan ketiga algoritma tersebut untuk mengetahui perbedaan susunan
segmentasi yang dihasilkan.
BAB VI ini berisikan kesimpulan yang diperoleh dari pembahasan
mengenai bab-bab sebelumnya dan saran untuk penelitian selanjutnya sebagai
akibat dari kekurangan atau kelebihan dari hasil penelitian yang telah dilakukan.

