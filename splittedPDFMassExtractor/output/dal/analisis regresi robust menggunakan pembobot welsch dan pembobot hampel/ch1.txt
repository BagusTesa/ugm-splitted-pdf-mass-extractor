Analisis regresi merupakan suatu metode analisis dalam statistika yang
digunakan untuk mencari hubungan antara suatu variabel terhadap variabel lain.
Dalam analisis regresi, variabel yang mempengaruhi biasa disebut Independent
Variable (Variabel Bebas) dan variabel yang dipengaruhi biasa disebut Dependent
Variable (Variabel Terikat). Jika dalam persamaan regresi hanya terdapat satu
variabel bebas dan satu variabel terikat, maka kasus tersebut disebut sebagai
persamaan regresi sederhana, sedangkan jika variabel bebasnya lebih dari satu dan
variabel terikat ada satu maka disebut sebagai persamaan regresi berganda. Disebut
berganda karena pengaruh beberapa variabel bebas akan dikenakan kepada variabel
tergantung.
Untuk analisis regresi sederhana biasanya sangat jarang digunakan dalam
kehidupan sehari. Salah satu contoh yang menggunakan regresi sederhana yaitu :
�menghitung berat badan ideal melalui tinggi badan� Rumus : Berat Badan ideal =
Tinggi badan � 100(konstanta). Dalam hal ini, terdapat 1 variabel dependen(berat
badan ideal) dan 1 variabel independen(tinggi badan). Dan kebanyakan kasus yang
ditemukan dalam kehidupan sehari-hari terkait dengan regresi, biasanya
menggunakan persamaan regresi berganda. Yaitu dengan 1 variabel dependen dan
terdapat 2 atau lebih variabel independen di dalamnya. Salah satu contoh yang
menggunakan regresi berganda yaitu : �menghitung penghasilan  ideal seorang kepala
rumah tangga� Untuk menghitung penghasilan rata-rata,  diperlukan beberapa
variabel independen (jumlah anggota keluarga, usia, status pekerjaan, dll).
Dan pada kenyataannya, tidak semua kasus dapat dikerjakan dengan analisis
regresi. Hal tersebut dapat dibuktikan karena adanya beberapa uji asumsi yang harus
dipenuh sebelum melakukan uji regresi sederhana maupun berganda. Asumsi-asumsi
yang harus dipenuhi yaitu normalitas, noautokorelasi, heteroskedastisitas dan
nomultikolinearitas.
Asumsi di atas sangat berpengaruh besar terhadap keakuratan dalam
mendapatkan hasil dalam variabel dependen. Adanya outlier atau pencilan akan
memberikan kemungkinan besar bahwa asumsi-asumsi dalam regresi tidak dapat
terpenuhi. Terdapat 1 data yang merupakan outlier sudah dapat merusak asumsi.
Untuk beberapa kasus, karena data outlier ini merupakan data yang jarang munc;ul
dalam data. Beberapa peneliti menghapus data tersebut dari penelitian. Rousseeuw
dan Leroy (1987) menyatakan bahwa tidak mengatasi masalah outlier dalam data,
maka dapat mengakibatkan kesimpulan kurang akurat.
Rousseeuw dan Leroy (1987) menyatakan terdapat dua cara untuk mengatasi
masalah outlier yang mengakibatkan metode kuadrat terkecil tidak dapat bekerja
dengan baik :
1. Mengeluarkan outlier yang telah diidentifikasi, lalu tetap menggunakan
metode kuadrat terkecil.
2. Tetap menggunakan seluruh data, tetapi dengan memberikan bobot yang
rendah untuk observasi yang terindikasi sebagai outlier, metode ini dikenal
dengan nama metode regresi robust.
Metode regresi robust merupakan metode penting untuk menganalisis data
yang dipengaruhi oleh outlier sehingga menghasilkan model regresi yang kekar
terhadap outlier. Suatu estimator yang kekar adalah relatif tidak terpengaruh oleh
perubahan besar pada bagian kecil data atau perubahan kecil pada bagian besar data.
Pengamatan yang jauh dari pusat data mungkin berpengaruh besar terhadap koefisien
regresi (Sembiring, 1995).
Dalam skripsi ini, akan dibahas salah satu metode dalam analisis regresi
robust yaitu analsis regresi robust dengan pembobot welsch dan pembobot hampel.
Dalam metode ini, estimasi yang digunakan pada regresi robust adalah penduga S.
1.2 Tujuan dan Manfaat Penulisan
Tujuan dari penulisan skripsi ini adalah :
1. Sebagai syarat untuk memperoleh gelar sarjana pada program studi Statistika
2. Mempelajari lebih jauh tentang akibat adanya data outlier pada analisis
regresi linear sederhana dan regresi berganda
3. Mempelajari lebih dalam mengenai regresi robust dengan pembobot Welsch.
4. Mempelajari lebih dalam mengenai regresi robust dengan pembobot Hampel.
5. Mempelajari lebih dalam mengenai regresi robust dengan estimasi S.
6. Membandingkan apakah regresi Robust dengan metode pembobot Welsch
atau pembobot Hampel lebih baik dibanding metode MKT.
7. Menunjukkan penerapan regresi robust dengan pembobot Welsch dan
pembobot Hampel dalam mengatasi masalah outlier pada analisis regresi
berganda
Manfaat dari penulisan skripsi ini adalah :
1. Menambah pengetahuan tentang metode regresi robust, khususnya regresi
robust pembobot welsch dan pembobot hampel sebagai metode pembobot
parameter alternatif untuk mengatasi masalah outlier dalam analisis regresi
linear sederhana dan regresi berganda.
2. Sebagai bahan tinjauan pustaka yang berguna bagi setiap pihak yang
membutuhkan, khususnya bagi mahasiswa program studi statistika.
Dalam penulisan skripsi ini, untuk mengatasi adanya penyimpangan dari judul
dan inti skripsi yang akan dibahas. Dalam studi kasus akan menggunakan data yang
mengandung outlier dan akan dilakukan analisis regresi robust dengan pembobot
welsch dan pembobot hampel.
Dari segi materi, akan dibahas topik-topik yang merupakan dasar dari asal
regresi robust, pemahaman estimasi S, dan fungsi pembobot welsch dan pembobot
hampel.
Metode penulisan yang digunakan dalam penulisan skripsi ini adalah studi
literatur, baik literatur yang diperoleh dari buku-buku, jurnal-jurnal, serta bahan
pendukung lainnya yang dapat digunakan oleh penulis sebagai referensi dalam
penulisan skripsi ini, yang berkaitan dengan materi penelitian yaitu regresi linear,
outlier, regresi robust serta metode regresi robust pembobot welsch dan pembobot
hampel.
Salah satu metode untuk mengatasi outlier adalah regresi robust. Regresi
robust merupakan metode regresi yang digunakan ketika distribusi dari residual tidak
normal dan atau mengandung beberapa  outlier  yang berpengaruh pada model (Ryan,
1997).
Sebelumnya sudah ada beberapa laporan skripsi yang membahas mengenai
Regresi Robust, diantaranya laporan skripsi :
- M.Helmi menggunakan regresi robust estimasi-S dengan fungsi tukey�s
biweight dan dari metode tersebut dibandingkan nilai std.error dengan
- Zaid menggunakan regresi robust dengan metode estimasi LTS (Least
Trimmed Square) dan dibandingkan dengan metode MKT (Metode
- M Sulthoni Ardhi menggunakan regresi robust estimasi-Generalized M
(estimasi GM) dengan fungsi Huber dan dibandingkan dengan MKT
Namun untuk laporan skripsi penulis kali ini, mengangkat topik berbeda yaitu
fungsi Hampel dan Welsch dengan metode estimasi-S dan dalam pengolahan data,
dapat mengangkat nilai pembanding metode serta koreksi dari skripsi sebelumnya.
Adanya software lain yang digunakan untuk menganalisis dan mengolah data.
Regresi robust digunakan dengan tujuan untuk memperoleh model terbaik yang kekar
terhadap outlier (Fox, 2002). Dalam suatu himpunan data biasanya ditemukan rata-
rata 10 persen amatan yang merupakan outlier (Hampel, 1986).
Outlier  tidak dapat dibuang atau dihapus begitu saja dari pengamatan.
Menurut Draper dan Smith (1992), adakalanya  outlier  memberikan informasi yang
tidak bisa diberikan oleh titik data lainnya,  misalnya karena  outlier  timbul dari
kombinasi keadaan yang tidak biasa yang mungkin saja sangat penting dan perlu
diselidiki lebih jauh.  Outlier  dapat diabaikan apabila setelah ditelusuri ternyata
merupakan akibat dari kesalahan mencatat amatan yang bersangkutan atau kesalahan
ketika menyiapkan peralatan. Pengamatan yang jauh dari pusat data mungkin
berpengaruh besar terhadap koefisien regresi (Sembiring, 1995).
1.6 Sistematika Penulisan
Skripsi ini akan disusun dengan sistematika penulisan sebagai berikut :
Bab ini berisi pendahuluan dari tema yang diangkat dalam penulisan
skripsi ini, meliputi latar belakang masalah, tujuan dan manfaat
penulisan, pembatasan masalah, metode penulisan, tinjauan pustaka,
dan sistematika penulisan yang memberikan arah terhadap penulisan
skripsi ini.
Bab ini memaparkan teori-teori yang digunakan sebagai landasan
dalam penulisan skripsi ini. Teori-teori ini meliputi uraian teoritis
tentang variabel random, matriks, regresi linear beserta asumsinya,
MLE, serta hal-hal mendasar yang akan digunakan lebih lanjut dalam
pembahasan.
BAB III REGRESI ROBUST PEMBOBOT WELSCH dan PEMBOBOT
Bab ini membahas mengenai outlier, beberapa metode pendeteksian
outlier, metode regresi robust estimasi S, dan metode regresi robust
pembobot welsch dan pembobot hampel pada regresi linear sebagai
solusi dari masalah outlier.
Bab ini membahas mengenai aplikasi regresi robust dengan
menggunakan pembobot welsch dan pembobot hampel dalam studi
kasus untuk menangani masalah outlier pada regresi linear berganda.
Bab ini berisi tentang kesimpulan-kesimpulan yang diperoleh dan juga
saran-saran yang terkait dengan penulisan skripsi. Harapan
kedepannya penulisan skripsi mengenai regresi robust dapat
dikembangkan menjadi lebih baik.

