Kain adalah bahan dasar dari pakaian yang biasa digunakan sebagai
kebutuhan pokok manusia untuk melindungi dan menutup dirinya. Kain pun dapat
menjadi identitas suatu bangsa dalam berbusana. Indonesia sebagai negara
kesatuan yang terdiri atas berbagai suku bangsa memiliki banyak kekayaan
budaya dalam bentuk kain tradisional, seperti songket, batik dan tenun. Kain-kain
tradisional tersebut mempunyai ciri motif dan warna yang berbeda-beda yang
merepresentasikan kekhasan masing-masing daerah.
Salah satu provinsi yang memiliki kain tradisional dengan motif yang begitu
beragam adalah Nusa Tenggara Timur (NTT). Tiap kesatuan etnik yang ada di
NTT, baik suku-suku di Pulau Flores, Pulau Timor, Pulau Sabu dan Pulau Sumba,
semuanya memiliki ciri budaya yang diterjemahkan dalam kain tenun tangan
tradisional. Tiap etnik tersebut menciptakan pola dan motif kain tenun masing-
masing yang merupakan manifestasi dari kehidupan sehari-hari masyarakat
setempat, kebudayaan, keadaan alam, kepercayaan bahkan simbol-simbol magis
yang dianut oleh masyarakat setempat (Therik, 2012).
Menurut Therik (2012), di mata ahli atau pemerhati kain tenun NTT, asal
sehelai kain tenun dapat diketahui dari motifnya. Di sini peranan ornamen utama
kain bersangkutan, pasti menonjolkan dengan kuat corak khas tenunan suku
bangsa itu. Namun begitu, tidak semua orang dapat membedakan asal daerah dari
suatu motif kain tenun tertentu. Kesulitan dalam mendefinisikan secara jelas
karaktersistik dari motif kain tenun dari suatu daerah mengakibatkan sulitnya
untuk dapat mengenali asal daerah dari suatu motif kain tenun, selain itu begitu
beragamnya motif kain tenun yang ada dan dengan komposisi warna yang
beragam pula semakin mempersulit orang awam dalam mengenali asal daerah dari
suatu motif kain tenun.
Klasifikasi suatu objek dapat dilakukan secara tidak langsung dengan cara
melakukan klasifikasi citra objek tersebut, sebab Citra menurut kamus Webster
adalah �suatu representasi, kemiripan, atau imitasi dari suatu objek atau benda�
(Abdullah, 2006). Sebuah citra dapat dikenali secara visual berdasarkan fitur-
fiturnya. Pemilihan ciri yang tepat akan mampu memberikan informasi yang detail
tentang kelas suatu citra serta dapat membedakannya dari citra pada kelas yang
berbeda. Beberapa fitur yang dapat diekstrak dari sebuah citra adalah warna,
bentuk dan tekstur (Lu, 1999). Vadivel dkk. (2007) mengatakan bahwa ciri warna
dan tekstur banyak digunakan pada klasifikasi citra, pengindeksan citra dan sistem
temu kembali citra. Ciri warna, yang biasanya menggunakan ekstraksi ciri statistik
orde pertama, merepresentasikan distribusi warna secara global dari sebuah citra.
Kekurangan utama dari metode ini adalah distribusi spasial dan variasi lokal dari
warna pada citra diabaikan. Variasi spasial lokal dari intensitas piksel biasa
digunakan untuk menangkap informasi tekstur dari sebuah citra (Vadivel dkk.,
2007).
Analisis tekstur adalah salah satu teknik analisis citra berdasarkan anggapan
bahwa citra dibentuk oleh variasi intensitas piksel, baik citra keabuan (grayscale)
maupun citra warna (Wibawanto, 2011). Variasi intensitas piksel pada sebuah
bidang citra ini membentuk apa yang oleh manusia disebut sebagai tekstur.
Toennies (2012) mengatakan bahwa sangat sulit untuk mendefinisikan secara jelas
ciri-ciri yang merepresentasikan karakteristik dari tekstur pada suatu citra. Begitu
pula dengan kain tenun dari NTT dimana motif kain tenun dari suatu daerah
sangat sulit untuk didefinisikan karakteristiknya agar dapat dibedakan dengan
motif kain tenun dari daerah lain. Motif ini terbentuk dari variasi intensitas warna
sehingga motif kain tenun dapat dipandang sebagai tekstur berwarna dari kain
tenun tersebut. Analisis tekstur dilakukan dalam upaya mengekstrak fitur-fitur
atau ciri-ciri dari sebuah citra agar dapat dilakukan pengenalan atau pembedaan
citra pada suatu kelas dengan citra pada kelas lainnya.
Dari sudut pandang statistika, tekstur citra adalah pola rumit sehingga
statistika dapat digunakan untuk mendapatkan karakteristiknya (Wibawanto,
2011). Pendekatan statistik merepresentasikan tekstur secara tidak langsung
melalui sifat-sifat non-deterministis yang menentukan distribusi dan hubungan
antar intenstitas piksel dari citra (Materka dan Strzelecki, 1998). Dalam review
yang dilakukan oleh Materka dan Strzelecki (1998) menunjukan bahwa metode
statistik orde kedua memberikan hasil yang lebih baik dalam mengesktrak faktor-
faktor diskriminan dari sebuah tekstur jika dibandingkan dengan metode spektral
dan struktural. Dalam review tersebut disebutkan juga bahwa metode statistik orde
kedua yang paling populer untuk melakukan analisis tekstur adalah yang
dikembangkan oleh Haralick (1973), yang disebut dengan Gray Level Co-
occurrence Matrix atau sering disingkat dengan GLCM, bahkan Siqueira dkk.
(2013) mengatakan bahwa diantara beberapa pendekatan statistik, GLCM terbukti
sangat powerful sebagai deskriptor fitur/ciri dalam merepresentasikan
karakteristik tekstur dari sebuah citra. Namun begitu GLCM yang bekerja pada
domain grayscale memiliki kelemahan yaitu komponen warna dari citra diabaikan
sehingga beberapa peneliti mencoba untuk menggabungkan ciri tekstur GLCM
dan ciri warna untuk menggambarkan tekstur berwarna dari citra, seperti yang
dilakukan oleh Kusrini dkk. (2008) dan Maheshwary dan Sricastava (2009).
Momen warna merepresentasikan ciri warna dari citra secara global, tetapi tidak
memberikan informasi spasial dan warna dari piksel-piksel pada citra. Oleh
karena itu beberapa peneliti mencoba untuk menerapkan analisis tekstur
menggunakan metode GLCM pada citra berwarna, dikenal dengan istilah Color
Co-occurrence Matrix (CCM). Seperti yang dilakukan oleh Drimbarean dan
Whelan (2001), Shim dan Choi (2003), Arvis dkk. (2004), Liang dan Lam (2006),
Vadivel dkk. (2007), Benco dan Hudec (2007), Akhloufi dkk. (2008) dan Kong
(2009) dalam kasus yang berbeda. Penelitian-penelitian tersebut menunjukkan
bahwa CCM merepresentasikan warna dan intensitas dari piksel-piksel yang
bertetangga pada sebuah citra sehingga CCM dapat digunakan sebagai deskriptor
fitur/ciri dalam merepresentasikan karakteristik tekstur berwarna dari sebuah citra.
Berdasarkan uraian tersebut di atas maka pendekatan analisis tekstur dengan
ekstraksi ciri statistik orde kedua menggunakan metode GLCM dapat digunakan
sebagai deskriptor fitur/ciri untuk melakukan klasifikasi motif kain tenun
berdasarkan asal daerah di wilayah NTT. Dalam mengenali asal daerah dari suatu
motif kain tenun, unsur warna juga diperhatikan, oleh karena itu perlu digunakan
juga ciri warna dalam melakukan klasifikasi motif kain tenun. Namun perlu
diteliti, untuk klasifikasi motif kain tenun, metode manakah yang memberikan
hasil lebih baik, apakah pendekatan analisis tekstur menggunakan metode GLCM
yang dikombinasikan dengan ciri warna ataukah pendekatan analisis tekstur
menggunakan metode CCM, yang merupakan penerapan GLCM pada domain
warna.
Fitur-fitur yang dihasilkan oleh metode ekstraksi ciri akan digunakan
sebagai masukan untuk proses klasifikasi. Metode klasifikasi yang digunakan
dalam penelitian ini adalah Nearest Mean Classifier (NMC) dan K-Nearest
Neighbours (KNN). Pertimbangan pemilihan metode klasifikasi tersebut, yang
sering dikategorikan sebagai pemilah (classifier) sederhana, agar hasil klasifikasi
motif kain tenun lebih merepresentasikan kinerja metode ekstraksi cirinya
daripada kinerja metode klasifikasinya, seperti yang dilakukan pada penelitian
Arvis dkk. (2004).
Berdasarkan latar belakang di atas, maka dapat dirumuskan permasalahan
sebagai berikut:
1. Bagaimana hasil klasifikasi motif kain tenun Nusa Tenggara Timur
berdasarkan analisis tekstur menggunakan metode GLCM yang
dikombinasikan dengan momen warna ?
2. Bagaimana hasil klasifikasi motif kain tenun Nusa Tenggara Timur
berdasarkan analisis tekstur menggunakan metode CCM ?
Untuk menjaga fokus dari penelitian ini, maka beberapa batasan yang
diberikan dalam penelitian ini adalah sebagai berikut:
1. Pengambilan gambar/citra kain tenun menggunakan perangkat kamera
digital dan dilakukan dari jarak yang sama.
2. Kain tenun yang digunakan masih berupa kain yang belum diolah menjadi
barang siap pakai, seperti sarung, pakaian, korden dan sebagainya.
3. Kain tenun yang akan diklasifikasikan dibatasi yaitu yang mewakili
beberapa daerah di wilayah NTT yaitu Flores, Timor, Sabu dan Sumba.
4. Matriks ko-okurensi yang dibentuk pada CCM dan GLCM menggunakan
hubungan ketetanggaan dengan jarak 1 (satu) piksel dan 4 arah (0
, 225
,
270
0
, 315
0
). Ciri statistik dari matriks ko-okurensi yang digunakan dalam
penelitian ini adalah energi, entropi, kontras dan homogenitas.
5. Metode klasifikasi yang digunakan adalah Nearest Mean Classifier (NMC)
dan K-Nearest Neighbours (KNN).
1.4 Tujuan Penelitian
Tujuan penelitian ini adalah untuk mengetahui diantara pendekatan analisis
tekstur menggunakan metode GLCM yang dikombinasikan dengan momen warna
dan pendekatan analisis tekstur menggunakan metode CCM, metode manakah
yang memberikan hasil lebih baik untuk klasifikasi motif kain tenun Nusa
Hasil dari penelitian ini diharapkan dapat menjadi pilihan tentang metode
ekstraksi ciri yang dapat digunakan dalam pengembangan sistem klasifikasi
maupun sistem identifikasi motif kain tenun di wilayah Nusa Tenggara Timur
pada khususnya dan kain tradisional Indonesia pada umumnya.
Beberapa penelitian telah dilakukan mengenai ekstraksi ciri citra dan
klasifikasi dari objek berupa kain, seperti ekstraksi ciri tekstur dan klasifikasi pada
pola kain batik. Metode yang digunakan antara lain Gray Level Difference
Method, Point Minuteae dan Transformasi Wavelet. Metode-metode tersebut
melakukan analisa ciri tektur dari citra dan bekerja di domain Gray Level
sedangkan komponen warna dari citra diabaikan. Pada penelitian ini akan
mengkombinasikan metode ekstraksi ciri tekstur dan ciri warna dari citra kain
tenun.
Tesis ini disusun dengan sistematika penulisan sebagai berikut:
BAB I Pendahuluan: Bab ini membahas mengenai latar belakang penelitian,
perumusan masalah, batasan masalah, tujuan dan manfaat penelitian,
keaslian penelitian dan sistematika penulisan.
BAB II Tinjauan Pustaka: Bab ini membahas mengenai penelitian-penelitian
tentang pengenalan pola, ekstraksi ciri dan klasifikasi citra yang telah
dilakukan oleh para peneliti sebelumnya.
BAB III Landasan Teori: Bab ini membahas mengenai teori-teori penunjang
yang digunakan dalam penelitian ini, meliputi motif kain tenun NTT,
ekstraksi ciri momen warna, analisis tekstur GLCM dan CCM, serta
metode klasifikasi Nearest Mean Classifier dan K-Nearest
BAB IV Metode Penelitian: Bab ini membahas mengenai bahan penelitian, alat
penelitian, tahapan pemrosesan data, dan pengujian hasil klasifikasi.
BAB V Implementasi: Bab ini membahas mengenai proses penerjemahan
perancangan kedalam instruksi yang dapat dikenali komputer melalui
bahasa pemrograman tertentu.
BAB VI Hasil dan Pembahasan: Bab ini membahas mengenai hasil penelitian
yang digunakan untuk pengujian dan pembahasan dari hasil pengujian.
BAB VII Penutup: Bab ini berisi tentang kesimpulan yang dapat diambil dari
hasil penelitian dan analisis yang telah dilakukan serta saran-saran
potensial untuk penelitian selanjutnya.

