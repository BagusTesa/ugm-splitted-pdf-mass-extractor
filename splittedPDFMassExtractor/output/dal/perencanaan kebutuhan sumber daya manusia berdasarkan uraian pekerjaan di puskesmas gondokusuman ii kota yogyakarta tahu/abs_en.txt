Background of Problems:To support patient care, public health center need
some, one is human resources or labor. With the fulfillment of the amount of labor
in accordance with the job description of each unit of work, then the service will
be maximal. Therefore, it is necessary for decision-making planning manpower
requirements in accordance existing job descriptions for services provided to run
optimally. In Health Care Center II Gondokusuman of Yogyakarta City have ever
done calculations with reference to human resource needs of the Circular Letter
Human Resources Development Agency and the Empowerment Ministry of
Health of the Republic of Indonesia, and results in a unit of work still unmet health
care workers. In addition to the preparation of the Quality Management System in
2014, the future human resource needs of the calculations in this study will be
used as preparation for Public Service Board in 2014 as well as a reference in
the recruitment of civil servants need power in order to achieve quality service
standards and health professionals who are competent in their field.
Purpose: To determine the amount of human resources requirements in
accordance with the analysis of the existing job description at Health Care Center
of Gondokusuman II in Yogyakarta City.
Methods: This type of research is a descriptive study using a qualitative
approach and the phenomenological research design. Retrieval of data by means
of observation, interviews, and documentation study. Some form of human
research subjects in each work unit in Health care Center of Gondokusuman II in
Yogyakarta City. Research object in the form of need for human resources at
Health care Center  of Gondokusuman II in Yogyakarta City.
Results: There are still some activities carried out by workers at the health care
center II Gondokusuman Yogyakarta City is not in accordance with the applicable
reference. Some labor work in the unit of work that is not in accordance with their
competence. Based on calculations using the human resource method obtained
results WISN (Work Load Indicator Staff Need) shortage of human resources,
with details: 2 general practitioners, 1 midwife, 2 general nurses, one dental
nurse, one assistant pharmacist, 1 nutritionist, 1 sanitarian, 1 epidemiologist, 1
surveillance, 2 clerical officer business and inventory, 3 treasurer, 1 night
watchman. With total human resource needs as many as 17 people.
Keywords: Planning, Human resource requirements, Job descriptions, WISN

