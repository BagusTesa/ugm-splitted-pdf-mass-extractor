Dewasa  ini  perkembangan  perangkat  mobile berkembang  dengan  amat
pesatnya.  Seiring  dengan  kecanggihannya,  kini  penggunaan  perangkat  mobile
tidak hanya pada wilayah komunikasi saja, tapi juga memasuki sendi kehidupan
lainnya.  Mulai  dari  pertukaran data,  sumber informasi,  jejaring sosial  dan lain
sebagainya. Para vendor perangkat  mobile berlomba-lomba menambahkan fitur-
fitur  yang  dapat  meningkatkan  manfaat  perangkat  mobile ini  baik  dari  segi
perangkat keras maupun perangkat lunak. Dengan beragam fitur yang ditawarkan,
menjadi  mungkin  untuk  memanfaatkan  perangkat  mobile  guna  meningkatkan
kualitas pariwisata Indonesia.
Salah  satu  tempat  yang  sering  dikunjungi  wisatawan  adalah  situs
bersejarah, khususnya candi. Pada beberapa situs wisata candi yang tersebar di
Daerah  Istimewa Yogyakarta  dan  Jawa Tengah,  seringkali  terdapat  relief  yang
menggambarkan  sebuah  cerita.  Mulai  dari  cerita  pewayangan,  keagamaan,
kehidupan kalangan kerajaan, kehidupan masyarakat, adat istiadat bahkan sampai
pada kekuatan militer kerajaan pada saat bangunan candi tersebut dibuat, dan lain
sebagainya (Rahmah, 1994; Agusmiyati, 1996).
Tidak  setiap  wisatawan  mengetahui  apa  pesan  yang  disampaikan  dari
sebuah relief. Untuk itu, seorang wisatawan perlu ditemani oleh seorang pemandu
wisata.  Pemandu  wisata  adalah  orang  pertama  yang  diajak  bicara  oleh  para
wisatawan  dan  seringkali  dianggap  sebagai  wakil  atau  representasi  dari  suatu
tempat (Cole, 2008). Namun dengan beberapa alasan, terkadang pemandu wisata
tidak dapat setiap saat menemani wisatawan untuk menerjemahkan pesan yang
disampaikan  dari  sebuah  relief.  Menurut  Purwaningsih  (2012)  salah  satu
penyebabnya adalah terus meningkatnya jumlah wisatawan yang tidak diimbangi
oleh jumlah pemandu wisata.
Penelitian ini mengembangkan sebuah perangkat lunak untuk identifikasi
objek relief pada suatu situs wisata, sehingga dapat membantu wisatawan dalam
menerjemahkan  pesan  dan  informasi  yang  terkandung  didalamnya.   Dengan
perkembangan  teknologi  mobile  yang  begitu  pesatnya,  akan  lebih  bermanfaat
apabila  perangkat  lunak  pengenalan  relief  tersebut  dibangun  pada  perangkat
mobile, karena lebih fleksibel dan dapat dibawa kemana saja.
Karena  setiap  objek  relief  memiliki  cerita  tersendiri,  maka  cerita  pada
sebuah  relief  dapat  dikenali  berdasarkan  pada  pola  tekstur  yang  dimilikinya.
Menurut  Acharya  dan  Ray (2005),  pengenalan  pola  adalah  sebuah  cara  yang
digunakan untuk mengenali objek dalam skema tertentu dengan suatu perangkat
pengukuran. Dalam hal ini, tekstur relief dapat digunakan sebagai nilai masukan
yang dapat diukur melalui sebuah metode pengenalan pola.
Terdapat beberapa penelitian yang telah dilakukan sebelumnya berkaitan
dengan  identifikasi  objek  menggunakan  perangkat  mobile.  Diantaranya  adalah
penelitian  yang  menggunakan  algoritma  ekstraksi  ciri  Linear  Binary  Pattern
(LBP),  yaitu  Nainggolan (2012),  Prasvita  (2012) dan Wahyuni  (2012).  Namun
menurut Zou, dkk (2007), metode ini memiliki kelemahan pengenalan terhadap
variasi  skala  atau jarak pengambilan objek dan pengaruh pencahayaan.  Begitu
juga dengan algoritma lain seperti Graylevel Co-occurence Matrix (GLCM) yang
selain  memiliki  kelemahan  pada  skala  juga  memiliki  kelemahan  pada  variasi
rotasi  (Cao  dkk,  2009).  Ini  dikarenakan penampilan  teksur  pada  citra  dapat
dipengaruhi oleh skala, sudut pandangan dan kondisi pencahayaan di sekitar objek
Speeded-Up Robust Feature (SURF) adalah sebuah algoritma ekstraksi ciri
yang dapat digunakan mengatasi  kelemahan dari  kedua algoritma ekstraksi ciri
diatas.  Beberapa  penelitian  telah  mencoba  menggunakannya  untuk  melakukan
identifikasi terhadap objek tertentu dengan perangkat mobile. Penelitian-penelitian
tersebut  diantaranya adalah Temmermans (2011),  Bay (2006) dan Chu (2007).
Penelitian  tersebut  telah  mempertimbangkan  faktor  intensitas  cahaya  maupun
sudut dalam pengenalan sebuah objek.
Pengenalan  sebuah  objek,  dalam  hal  ini  adalah  objek  relief  Candi
Borobudur  menggunakan  perangkat  mobile memiliki  permasalahan  tersendiri,
yaitu  setiap wisatawan dapat  mengambil  citra  relief  dari  posisi  yang fleksibel,
kapan saja, beragam kondisi cuaca dan intensitas cahaya, dan seringkali bidang
relief tidak diambil secara utuh, dsb. Pengembang perangkat lunak dalam hal ini
tidak dapat mencegah atau mengatur seorang wisatawan untuk mengambil citra
relief dari posisi yang ditentukannya sendiri secara sepihak.
Penelitian ini merancang perangkat lunak berbasiskan  mobile yang dapat
membantu wisatawan untuk menerjemahkan cerita dari sebuah relief. Disamping
itu,  juga  akan  dilakukan  analisa  terhadap  beragam  faktor  yang  dapat
mempengaruhi  hasil  pengenalan  relief.  Faktor-faktor  ini  belum dibahas  secara
komprehensif atau menjadi titik fokus pada penelitian sebelumnya. Faktor-faktor
tersebut diantaranya adalah skala, perubahan orientasi (rotasi),  masukan bidang
relief yang tidak utuh, dan lain sebagainya.
1.2 Rumusan Masalah
Berdasarkan  latar  belakang  diatas  maka  masalah  yang  dibahas  dalam
penelitian ini adalah:
1. Bagaimana membangun perangkat  lunak pada perangkat  mobile untuk
identifikasi relief pada situs Candi.
2. Bagaimana pengaruh fitur yang diujikan, yaitu: sudut, jarak, perubahan
orientasi,  intensitas  cahaya  dan  keutuhan  bidang  citra  relief  terhadap
sistem yang dibangun dalam pengenalan objek relief tertentu.
1. Citra  relief  untuk  identifikasi  dan  pelatihan  diperoleh  dari  situs  wisata
2. Metode  Speeded-Up  Robust  Feature  (SURF)  digunakan  dalam  proses
ekstraksi ciri citra.
3. Basis data fitur  citra  pelatihan yang digunakan untuk pengenalan objek
diletakkan pada perangkat server.
4. Metode  pengenalan  yang  digunakan  adalah  hierarchical  k-means  tree
nearest-neighbor.
Tujuan  dari  penelitian  ini  adalah  untuk  membangun sistem yang  dapat
melakukan  identifikasi  citra  relief  Candi  Borobudur  menggunakan  perangkat
mobile. Selain itu juga dilakukan analisa pengaruh faktor sudut, jarak, perubahan
orientasi, intensitas cahaya dan keutuhan bidang tekstur relief sebagai masukan
sistem  dalam identifikasi  citra  relief  sehingga dapat  ditentukan masukan citra
yang terbaik bagi hasil pengenalan.
1.5 Manfaat Penelitian
Penelitian  ini  diharapkan  dapat  membantu  wisatawan  menerjemahkan
pesan  yang  disampaikan  dari  sebuah  relief.  Selain  itu  juga  penelitian  ini
diharapkan  dapat  memberikan  referensi  pengetahuan  mengenai  bagaimana
teknologi  informasi  dapat  membantu  meningkatkan  kualitas  sektor  pariwisata
Beberapa  penelitian  telah  dilakukan  mengenai  bagaimana  melakukan
identifikasi  terhadap  sebuah  objek  menggunakan  perangkat  mobile.  Beberapa
metode dan algoritma telah digunakan mulai dari  Linier Binary Pattern (LBP)
sampai dengan  Speeded-Up Robust Feature (SURF). Namun penelitian-penetian
tersebut  belum membahas secara komprehensif  mengenai  bagaimana pengaruh
faktor sudut, jarak, perubahan orientasi (rotasi), intensitas cahaya dan keutuhan
bidang tekstur relief bagi hasil identifikasi.
Penelitian ini akan membangun sebuah perangkat lunak untuk identifikasi
citra relief menggunakan perangkat mobile Android dengan metode ekstraksi ciri
SURF dengan  metode  pengenalan  hierarchical  k-means  tree  nearest-neighbor.
Hierarchical k-means tree berperan dalam pengindeksan ciri SURF dan nearest-
neighbor digunakan dalam pencocokan antara ciri citra masukan dengan ciri yang
ada pada basis data pelatihan.  Perangkat lunak tersebut akan diujikan terhadap
objek  relief  dengan  melihat  pengaruh  faktor  sudut,  jarak,  perubahan orientasi,
perbedaan intensitas cahaya dan keutuhan bidang tekstur relief citra masukan.
Tesis ini disusun dengan sistematika penulisan sebagai berikut:
BAB I Pendahuluan: Bab ini membahas mengenai latar belakang penelitian,
perumusan  masalah,  batasan  masalah,  tujuan  penelitian,  manfaat
penelitian, metodologi penelitian dan sistematika penulisan.
BAB II Tinjauan Pustaka: Bab ini membahas mengenai penelitian-penelitian
tentang  pengolahan  citra,  ekstraksi  ciri  dan  pengenalan  pola  yang
telah dilakukan oleh para peneliti sebelumnya.
BAB III Landasan Teori: Bab ini membahas mengenai teori-teori penunjang
yang digunakan  dalam pembuatan  perangkat  lunak yang meliputi:
tekstur, Android, metode ekstraksi ciri SURF, metode pengindeksan
vektor  ciri  dengan  heirarchical  tree  clustering dan  metode
pengenalan dengan hierarchical k-means tree nearest-neighbor.
BAB IV Analisis  dan  Perancangan  Sistem:  Bab  ini  membahas  mengenai
analisis  dan  perancangan  sistem yang  meliputi:  analisa  kebutuhan
sistem, diagram alir, algoritma dan arsitektur yang digunakan dalam
pembuatan perangkat lunak.
BAB V Implementasi:  Bab  ini  membahas  mengenai  proses  penerjemahan
hasil analisa dan rancangan algoritma serta arsitektur sistem ke dalam
intruksi yang dapat dikenali komputer melalui bahasa pemrograman
tertentu.
BAB VI Hasil dan Pembahasan: Bab ini membahas mengenai cara pengujian
terhadap perangkat lunak yang dihasilkan.
BAB VII Penutup:  Bab  ini  berisi  tentang  kesimpulan  dan  saran  yang  dapat
diambil dari hasil penelitian dan analisis yang telah dilakukan selama
penelitian berlangsung.

