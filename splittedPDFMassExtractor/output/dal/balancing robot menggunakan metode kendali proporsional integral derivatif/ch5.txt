Berdasarkan penelitian yang dilakukan maka dapat diambil kesimpulan
bahwa telah berhasil dibuat balancing robot dengan menggunakan sistem kendali
PID untuk sudut pitch dengan nilai konstanta Kp = 1.5, Ki = 0.75, Kd = 1.875 dan
koefisien filter (a) pada algoritma complementary filter adalah 0.96.

Untuk pengembangan lebih lanjut dalam sistem ini disarankan untuk
melakukan hal sebagai berikut :
1. Metode tuning konstanta kendali PID dilakukan dengan metode auto uning
agar hasil yang diperoleh lebih optimal.
2. Metode fusion sensor menggunakan algoritma lain seperti Kalman Filter
atau DCM (Discrete Cosine Matrix) sebagai perbandingan dengan metode
complementary filter.

