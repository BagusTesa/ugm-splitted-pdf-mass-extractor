Perkembangan teknologi adanya Short Message Service (SMS) diharapkan
akan dapat memberikan kemudahan bagi pihak pengguna agar dapat difungsikan
sebagai pemantau dan pengendali suatu kondisi yang ada di suatu ruangan secara
langsung tanpa harus terjun langsung ke lokasi.
Alat yang dibuat berupa miniatur ruangan yang dilengkapi dengan sistem
pemroses menggunakan mikrokontroler ATmega 16, sensor suhu dan kelembaban
SHT11, sensor LDR untuk menentukan intensitas cahaya, kipas untuk pendingin,
dan hairdryer untuk pemanas. Sistem ini bekerja secara otomatis, baik saat akan
mendinginkan maupun memanaskan ruangan. Pendinginan dan pemanasan
ruangan dilakukan berdasarkan pada suhu yang terbaca oleh sensor SHT11.
Hasil pengujian secara keseluruhan menunjukkan bahwa sistem telah
mampu memantau dan mengkondisikan suhu dalam miniatur ruangan. Pada
pengujian suhu yang dapat dikendalikan antara 25�C sampai 34�C.
Purwarupa pemantau dan pengendalian suhu suatu ruangan berbasis Short
Message Service (SMS) dengan ATMega16, telah diimplementasikan dengan baik
sesuai dengan perancangan sistem. Menampilkan data-data tersebut pada LCD,
dan kemudian mengirimkannya melalui SMS dengan format PDU. Sistem dapat
menyimpan data pada EEPROM,sehingga jika pada saat mati listrik user tidak
perlu men-setting kembali
Kata kunci: Short Message Service (SMS), ATMega16, EEPROM, PDU, set point

