Data tahan hidup atau data survival adalah lama waktu sampai suatu peristiwa
terjadi. Istilah data survival sendiri banyak digunakan dalam bidang ilmu kesehatan,
epidemiologi, demografi dan aktuaria.
Kendala yang sering muncul dalam analisis data survival adalah adanya
pengamatan yang tidak lengkap, yang secara umum dapat dikelompokkan menjadi
data tersensor (censored) dan data terpotong (truncated). Ketidaklengkapan data yang
diperoleh dapat disebabkan oleh beberapa faktor, beberapa faktor tersebut misalnya
keterbatasan informasi, keterbatasan sumber daya, terjadi hal yang tidak terduga.
Salah satu bentuk dari data survival adalah data tersensor. Masalah dari data
tersensor dapat dibagi menjadi beberapa tipe penyensoran, salah satunya adalah
interval tersensor. Sensor interval dapat terjadi ketika mengamati nilai yang
memerlukan tindak lanjut. Data tersensor kiri, tersensor kanan, serta data lengkap
adalah kasus khusus dari sensor interval. Data tersensor kiri memiliki nilai interval
bawah nol, data tersensor kanan memiliki nilai interval atas tak hingga, dan data
lengkap memiliki nilai interval atas sama dengan interval bawah. Salah satu yang
dapat diketahui dari tersensor interval adalah jarak (range), yaitu sebuah interval,
yang berada pada saat terjadinya peristiwa event.
Contoh yang paling sering ditemukan data dengan observasi tersensor interval
adalah pada ilmu kedokteran atau studi kesehatan yang memerlukan penanganan
lanjut secara periodik, beberapa uji klinis atau studi cohort misalnya seperti studi
cohort untuk AIDS dan studi tindak lanjut penyakit kanker.
Dalam beberapa situasi, data tersensor interval dapat terlihat. Misalnya,
seseorang mungkin kehilangan satu atau lebih waktu pengamatan yang telah
dijadwalkan sebelumnya untuk mengamati kemungkinan perubahan status penyakit
secara klinis dalam suatu pengamatan mengenai suatu penyakit dan kemudian
kembali dengan status yang sudah berubah. Seseorang yang mengunjungi pusat klinik
pada waktu yang tepat akan lebih baik dibandingkan dengan waktu pengamatan yang
telah ditetapkan sebelumnya tetapi bukan pada saat terjadinya kejadian yang menjadi
perhatian. Pengamat menginginkan semua subyek yang masuk dalam pengamatannya
pada waktu yang telah ditentukan atau dijadwalkan sebelumnya adalah saat yang
tepat, tetapi ini menjadi suatu kendala karena adanya keterbatasan. Pada situasi
seperti ini data yang diperoleh adalah data tersensor interval.
Dalam membandingkan dua fungsi survival untuk data tersensor kanan atau
lengkap dilakukan uji nonparametrik yaitu uji log-rank. Sedangkan uji log-rank
kurang tepat untuk diaplikasikan pada data tersensor interval. Finkelstein dan Wolfe
tahun 1985 mengajukan model semiparametrik untuk analisis regresi untuk data uji
hidup tersensor interval yang mendiskusikan contoh dari studi pada pasien kanker
payudara. Pasien dibagi menjadi dua kelompok menurut perlakuan terhadap pasien
kanker tersebut. Kelompok pertama mendapat perlakuan dengan radioterapi dan
kelompok lainnya mendapat perlakuan radioterapi dan kemoterapi. Dua kelompok
pasien diperiksa setiap 4-6 bulan. Dalam studi ini yang menjadi kejadian (event)
adalah waktu sampai terlihatnya retraksi payudara (breast retraction) dan
dibandingkan pada masing-masing perlakuan. Beberapa pasien melewatkan beberapa
jadwal pemeriksaan berturut-turut dan kembali lagi kemudian dengan status klinis
yang sudah berubah. Observasi terhadap beberapa pasien ini termasuk observasi
tersensor interval.
Model proporsional hazard adalah model yang paling dapat diterima secara luas
untuk analisis survival, karena hasil dari estimasinya berguna dan mudah dipahami
oleh peneliti kesehatan. Dari permasalahan di atas akan digunakan pengembangan
dari metode dengan menggunakan model hazard proporsional untuk data uji hidup
tersensor interval dalam membandingkan beberapa fungsi survival.
1.2 Tujuan dan Manfaat Penulisan
Penyusunan skripsi ini adalah untuk memenuhi salah satu syarat untuk
mencapai derajat sarjana S1 Program Studi Statistika, Jurusan Matematika, Fakultas
Matematika dan Ilmu Pengetahuan Alam, Universitas Gadjah Mada.
Berdasarkan latar belakang yang telah diuraikan, penulis merumuskan beberapa
tujuan penulisan, antara lain:
1. Memberi gambaran tentang data tersensor interval
2. Menentukan fungsi likelihood dibawah asumsi model hazard proporsional
untuk data tersensor interval
3. Mengaplikasikan model hazard proporsional untuk data uji hidup yang
tersensor interval
4. Mencari nilai NPMLE (Nonparametric Maximum Likelihood Estimation)
dengan metode Turnbull
5. Menurunkan uji skor untuk data tersensor interval
6. Membandingkan fungsi survival
Manfaat penulisan skripsi ini adalah sebagai berikut:
1. Memberikan gambaran tentang data tahan hidup khususnya data tersensor
interval
2. Memberikan penjelasan mengenai model hazard proporsional yang digunakan
untuk data uji hidup tersensor interval
3. Memberikan gambaran tentang langkah menurunkan uji skor untuk dapat
digunakan sebagai perbandingan dua fungsi survival.
1.3 Perumusan dan Batasan Masalah
Berdasarkan latar belakang masalah dan kajian-kajian pendukung lainnya,
maka penulis membatasi hanya pada metodologi untuk data yang tersensor interval
dengan mekanisme penyensoran adalah independen antara waktu respon dan
kovariat,dan keindependenan pada tiap interval waktu.
Karya tulis ini akan menyajikan bagaimana aplikasi dari model hazard
proporsional dapat diterapkan pada data tersensor interval. Pada tahun 1978 Prentice
dan Gloeckler mengajukan �grouped data model� untuk diaplikasikan pada data
yang memiliki observasi tersensor interval. Dibawah asumsi dari hazard proporsional
dapat dituliskan fungsi likelihood dari data yang tersensor interval dan kemudian akan
dicari turunan pertama dan kedua dari fungsi log likelihood. Dibawah hipotesis nol ,
akan dilakukan kombinasi dua sampel pada masing-masing perlakuan untuk
mendapatkan estimasi dari fungsi survival dengan menggunakan pendekatan
algoritma EM pada �self-consistent estimate� (Turnbull, 1976).
Metode pada penulisan ini yaitu dengan mengaplikasikan metode yang diajukan
oleh Finkelstein (1986), menganggap ketepatan penggunaan model hazard
proporsional pada data tersensor interval dengan menurunkan atau menggunakan
metode skor (score) dalam inferensinya untuk dapat digunakan membandingkan dua
fungsi survival . Metode skor ini juga tidak lepas dari uji log-rank pada data tersensor
kanan atau data lengkap, karena penurunan skor statistik ini dapat ditulis seperti
dengan bentuk statistik uji log-rank.
Metode penulisan dalam karya tulis ini adalah berdasarkan studi literatur yang
didapat dari perpustakaan, jurnal-jurnal dan buku-buku yang berhubungan dengan
tema dari skripsi ini. Sumber lainnya juga diperoleh melalui situs-situs pendukung
yang tersedia di internet. Pengerjaan karya tulis ini juga ditunjang dengan software R
3.0.0.
Sistematika penulisan skripsi ini terdiri dari lima bab, isi masing-masing bab
diuraikan sebagai berikut :
BAB I Pendahuluan
Bab ini membahas tentang latar belakang, tujuan dan manfaat penulisan,
perumusan dan batasan masalah, tinjauan pustaka, metode penelitian,
sistematika penulisan.
Bab ini membahas tentang teori penunjang yang akan digunakan dalam
pembahasan, diantaranya matriks, turunan, analisis tahan hidup (survival),
data ketahanan hidup, algoritma EM (Expectation-Maximization), metode
nonparametrik Kaplan-Meier dan uji log-rank, regresi cox.
BAB III Model Hazard Proporsional untuk Data Uji Hidup Tersensor Interval
Bab ini membahas tentang  hazard proporsional untuk data berkelompok
(grouped data), data tersensor interval, MLE (Maximum Likelihood
Estimation), estimasi NPMLE (Nonparametric Maximum Likelihood
Estimation), fungsi skor dan variansi fungsi skor, uji skor untuk 0 .
BAB IV Aplikasi Model Hazard Proporsional untuk Data Breast Cosmesis
Bab ini membahas tentang aplikasi dari model hazard proporsional untuk
data uji hidup tersensor interval diantaranya adalah mengenai deskripsi
data,  estimasi NPMLE (Nonparametric Maximum Likelihood Estimation),
skor statistik dan variansi skor statistik,  uji hipotesis untuk 0  .
BAB V Penutup
Bab ini berisi kesimpulan dan saran.

