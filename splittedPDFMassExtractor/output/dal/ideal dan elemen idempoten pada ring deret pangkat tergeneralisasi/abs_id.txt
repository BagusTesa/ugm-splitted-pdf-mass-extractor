Dalam skripsi ini akan dibahas tentang ideal I1 di Ring Deret Pangkat Terge-
neralisasi (RDPT) yang dikonstruksi dari ideal I di ring komutatif R dengan elemen
identitas. Selanjutnya dibahas pula ideal I2 di RDPT yang dikonstruksi dari radikal
ideal I dan radikal dari I1 di RDPT. Selain itu, dalam skripsi ini juga dibahas elemen
idempoten RDPT dan sifat-sifatnya.

