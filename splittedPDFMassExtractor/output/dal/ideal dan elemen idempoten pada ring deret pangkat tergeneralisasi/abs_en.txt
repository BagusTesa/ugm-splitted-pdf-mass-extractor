In this final project, we discuss about ideal I1 of Generalized Power Series
Rings (GPSR) which is constructed from ideal I of commutative ring R with identity.
We also discuss about ideal I2 of GPSR which is constructed from radical of ideal I
and radical of ideal I1 of GPSR. Moreover, we discuss about idempotents of GPSR
and its characteristics.

