Synthesis and antibacterial test of bromochloro pyrazoline from vanilin
had been carried out. The research was conducted on two major steps, first step i.e
synthesis 5-bromovanillin, chalcone synthesis of 5-bromovanilin and 4-
chloroacetophenon and pyrazoline synthesis of chalcone and phenylhidrazine and
second step the pyrazoline was tested its activity as antibacterial.
Bromination of vanilin was performed using KBrO3 and HBr under acidic
condition. Chalcone synthesis was done by stirring 5-bromovanilin, 4-
chloroacetophenone and NaOH 2M at room temperature for 24 hours. Pyrazoline
was synthesized by refluxing the related chalcone, phenylhidrazine and glacial
acetic acid for 5 hours. The structure of all products were confirmed by FTIR,
GC-MS, 1H- and 13C-NMR spectrometers. The final step of in vitro antibacterial
activity test of pyrazoline compound was conducted by agar well-diffusion against
Staphylococcus aureus, Bacillus cereus, Bacillus subtillis, Escherichia coli and
Shigella flexnerri bacterials.
The result showed that 5-bromovanilin was obtained as white-brown solid
in 75.32% yield. Bromochloro chalcone was yielded in 62.04% as red solid and
bromochloro pyrazoline was produced as yellow solid in 66.25%. Antibacterial
test indicated that bromochloro pyrazoline were active against selected Gram
positive and negative bacterial with low to medium inhibition zone. Bromochloro
pyrazoline was found to exhibit an antibacterial activity and its zone of
inhibition/concentration (mm/ppm) againts S. aureus (4.75/300), B. cereus
(4.20/300), B. subtillis (2.25/100), E. coli (5.75/500) and S. flexnerri (6.75/1000).
The antibacterial test showed that pyrazoline derivatives which were subtituted
bromo, chloro, hydroxyl and methoxy groups, were able to inhibit the growh of
the tested pathogenic bacteria.
Keyword: vanilin, bromovanilin, bromochloro chalcone, bromochloro pyrazoline,
antibacterial.

