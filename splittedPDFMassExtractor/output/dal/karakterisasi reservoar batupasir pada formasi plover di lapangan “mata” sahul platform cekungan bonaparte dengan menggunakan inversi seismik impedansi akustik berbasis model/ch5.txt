Dari hasil penelitian Karakterisasi Reservoar Batupasir Pada Formasi
Menggunakan Inversi Seismik Impedansi Akustik Berbasis Model, penulis telah
mendapatkan beberapa kesimpulan yaitu sebagai berikut:
1).  Reservoar batupasir pada Formasi Plover di lapangan �Mata� memiliki
range  nilai impedansi akustik yang rendah yaitu berkisar antara 5400
hingga 9250 m/s.gr/cc dan juga mempunyai nilai gamma ray  yang
rendah dengan range nilai gamma ray yaitu nilai 10-74 API.
2). Dari peta akustik impedansi diketahui bahwa reservoar batupasir gas
pada Formasi Plover menyebar ke arah timur-laut dan barat daya.
Keberadaan sumur AP1 dan AP2 sudah tepat berada di puncak reservoar.

1).  Menggunakan data seismik yang lebih luas dan data sumur yang lebih
banyak.
2).  Perlu dilakukan penelitian dengan menggunakan atribut seismik untuk
mengetahui kandungan reservoar  batupasir yang ada pada formasi

