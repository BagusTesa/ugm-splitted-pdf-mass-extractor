Metode seismik merupakan metode geofisika yang sangat populer
digunakan dalam dunia eksplorasi untuk mengetahui struktur bawah pemukaan
(struktur geologi). Namun terkadang hasil rekaman seismik tidak dapat meng-
gambarkan strutur geologi yang sebenarnya. Hal ini disebabkan adanya gangguan
(noise) seperti ground roll, air blast/air wave, random noise, multiple, dan lain-
lain. Gambaran struktur bawah permukaan yang mendekati sebenarnya dapat di-
capai jika gangguan (noise) dapat dihilangkan secara maksimal, serta pengolahan
data seismik dilakukan dengan baik dan benar (Nguyen, 2007).
Akuisisi data seismik di lepas pantai dapat menimbulkan multiple yang
disebabkan oleh adanya gelombang yang terperangkap dalam lapisan air laut
maupun lapisan batuan. Gelombang ini merupakan gelombang yang dipancarkan
oleh sumber peledak kemudian dipantulkan oleh batas lapisan permukaan batuan.
Pada eksplorasi di lepas pantai, gelombang seismik tidak hanya sekali terpantul
oleh lapisan, akan tetapi gelombang tersebut dipantulkan lagi oleh batas muka air,
dasar laut, maupun lapisan batuan, sehingga ketika gelombang ini ditangkap oleh
hidrophone akan memberikan informasi waktu rambat gelombang yang lebih la-
ma dibandingkan jika gelombang tersebut hanya dipantulkan satu kali oleh per-
lapisan batuan (Adriaan, 2010). Dari informasi waktu rambat gelombang yang
lebih lama ini akan menimbulkan efek lapisan baru seperti perulangan lapisan
atau sering disebut multiple. Hal ini tentunya akan memberikan informasi lapisan-
lapisan batuan semu yang sebenarnya lapisan tersebut tidak ada. Oleh sebab itu
dibutuhkan suatu metode khusus agar multiple tersebut dapat dihilangkan tanpa
mengganggu informasi event primernya.
Untuk menghasilkan penampilan data seismik yang baik, berbagai metode
dikembangkan untuk memisahkan gelombang utama (primary) dan multiple.
Metode yang dapat dilakukan diantaranya yaitu radon demultiple dan f-k
demultiple. Radon demultiple bekerja dengan mengubah data dari domain t-x
(time-offset) menjadi domain ?-p (intercept time-ray parameter). Sedangkan f-k
demultiple menggunakan prinsip transformasi Fourier dengan mengubah data dari
domain t-x (time-offset) menjadi domain f-k (frequency-wavenumber).
Berdasarkan perumusan masalah tersebut di atas maka tujuan dari penelitian
ini adalah:
a. Melakukan pengolahan data seismik refleksi 2D offshore untuk
menghilangkan multiple dengan menggunakan metode radon demultiple,
dan f-k demultiple.
b. Membandingkan hasil pelemahan multiple dengan metode radon demulti-
ple, dan f-k demultiple.
Pembahasan masalah akan dibatasi pada hal-hal yang berkenaan langsung
dengan konsep demultiple seperti teori-teori dan konsep-konsep dasar yang
digunakan, penjelasan tentang konsep demultiple serta analisis hasil yang
diperoleh dari penerapan konsep demultiple tersebut.
1.4 Lokasi dan Waktu Penelitian
Penelitian dilakukan di PT. PERTAMINA Upstream Technology Center,
Jakarta Pusat. Waktu penelitian dimulai tanggal 9 Juli 2013 s.d 26 Agustus 2013
dan dilanjutkan dengan tahapan penyelesaian di Universitas Gadjah Mada,
Data yang digunakan pada studi ini adalah sebuah lintasan seismik 2D.
Lintasan ini terletak pada lapangan Prabu, Laut Jawa Utara.

