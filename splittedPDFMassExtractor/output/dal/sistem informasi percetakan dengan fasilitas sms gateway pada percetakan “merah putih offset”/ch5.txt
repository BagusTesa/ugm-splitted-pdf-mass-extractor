Dari hasil pengembangan sistem dan penulisan laporan tugas akhir ini,
maka dapat ditarik kesimpulan bahwa:
1. Sistem Informasi Percetakan pada Percetakan Merah Putih Offset
berhasil dibangun serta dapat memenuhi tujuan dan manfaat dari
penelitian yang telah dilakukan.
2. Sistem informasi ini dapat mengolah data transaksi, pemesanan,
produk, pelanggan, user, dan laporan.
3.  Laporan  yang dibuat oleh sistem ini adalah laporan pemesanan
dan lapoan pendapatan yang dapat diolah oleh pegawai dan pemilik.
Sedangkan pegawai dapat mengolah nota belanja pada setiap transaksi
yang dilakukan.
Penulis menyadari masih banyak kekurangan dalam penelitian dan
penulisan laporan tugas akhir ini. Oleh karena itu, saran dan harapan dari penulis
guna menghasilkan program yang lebih baik adalah :
1. Perlu dikembangkan dan ditambahkan lagi fasilitas pembayaran
melalui penghitungan yang sesuai dengan hitungan dalam bisnis
percetakan.
2. Pelaporan dalam sistem ini dapat dikembangkan dengan
menambahkan fitur filter data pelaporan statistik untuk memudahkan
pemilik maupun pegawai pembacaan data.

