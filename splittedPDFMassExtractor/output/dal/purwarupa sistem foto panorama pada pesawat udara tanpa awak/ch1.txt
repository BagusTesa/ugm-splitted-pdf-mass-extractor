1.1. Latar belakang
Penggunaan foto dan video udara telah lama dikenal sejak awal perang
Dunia I. Dengan teknologi udara saat itu masih dominan untuk keperluan militer.
Di masa sekarang, walaupun telah hadir teknologi satelit imaging, penggunaan
pesawat dan helikopter untuk sebuah foto telah digantikan dengan pesawat model.
Di kalangan militer biasa digunakan sebagai penyusup ke daerah musuh atau
sebagai penghancur daerah musuh. Di kalangan sipil, pesawat tanpa awak ini
banyak digunakan untuk pencarian dan penyelamatan korban bencana alam,
penginderaan, survey vegetasi hutan, pemantauan daerah perbatasan dan lain
sebagainya. Bahkan juga kini bisa menjadi lahan bisnis dengan pemotretan udara
untuk iklan ataupun sekedar pengambilan gambar. Adanya foto panorama untuk
udara akan membantu objek yang akan diamati dengan lebih luas.
Di dunia modern seperti sekarang ini ilmu fotografi telah berkembang pesat,
salah satunya ialah foto panorama. Dengan teknik panorama diterapkan di udara
akan mencakup wilayah foto yang lebih luas dan lebih efisien dalam mengamati
suatu daerah atau wilayah untuk keperluan penelitian ataupun keperluan lainnya.
Akan tetapi di lain sisi sebuah foto panorama mempunyai kelemahan yaitu harus
mempunyai perpotongan foto satu sama lain agar bisa digabungkan.
Indonesia adalah sebuah negara yang luas meliputi beberapa kepulauan,
untuk mengamankan dan memantau pulau-pulau kecil yang berada di pelosok
Indonesia, maka teknologi ini dapat berperan penting dan dapat digunakan untuk
menjaga wilayah kedaulatan NKRI. Dengan kemampuan yang dimiliki UAV,
bermacam-macam kebutuhan seperti pemantauan lalu lintas dari udara, patroli
perbatasan dan pencarian korban banjir atau tanah longsor dapat terpenuhi. Selain
lebih murah biaya pembuatan dan operasionalnya untuk beberapa misi tertentu
yang berbahaya, tentunya dengan menggunakan pesawat udara tanpa awak dapat
dihindarkan dari resiko korban jiwa. Oleh karena itu pengembangan teknologi di
bidang ini mutlak dibutuhkan. Akan Tetapi UAV mempunyai kekurangan yaitu
jarak jelajah yang terbatas dikarenakan hardware yang dipakai untuk ground
station mempunyai kemampuan yang terbatas.
UAV (Unmanned Aerial Vehicle) adalah pesawat udara tanpa awak yang
juga dikenal sebagai pesawat udara dikendalikan dari jarak jauh atau
RPV/Remotely Piloted Vehicle yang salah satu fungsinya yaitu untuk pemotretan
suatu wilayah dari udara. Untuk mempermudah pemotretan wilayah yang luas
maka diperlukan suatu sistem pemotretan dengan sistem foto panorama dengan
UAV sebagai pembawa perangkat kerasnya.
Manfaat yang diperoleh dari sistem foto panorama pada pesawat udara
tanpa awak yaitu dapat menjangkau wilayah pemotretan yang lebih luas  dan lebih
efisien dalam pemantauan suatu wilayah.

Rumusan masalah yang menjadi dasar penelitian ini adalah bagaimana
membuat suatu sistem foto panorama untuk mendukung fungsi dari pesawat udara
tanpa awak.

Dalam pembuatan penelitian ini memiliki beberapa batasan, yakni :
1. Pembuatan antarmuka foto panorama dengan visual studio 2010
menggunakan bahasa C#.
2. Sistem Stitching foto menggunakan Emgu CV yang prosesnya dilakukan
di komputer.
3. Komunikasi Antarmuka dengan motor servo dan webcam menggunakan
jaringan nirkabel.
4. Motor servo yang digunakan memiliki sudut maksimal sebesar 180 derajat
dan perpindahan sudut ditentukan oleh pengguna agar foto satu sama lain
saling berpotongan.
5. Format yang digunakan dalam foto ini hanya berformat JPG dan berasal
dari webcam.
6. Pengendalian servo dan webcam dengan fasilitas jaringan nirkabel
1.4.  Tujuan dan Manfaat Penelitian
Tujuan dari tugas akhir ini dapat dituliskan sebagai berikut:
Membuat suatu sistem penggabungan foto sehingga membentuk
suatu foto panorama yang diproses di antarmuka untuk pesawat udara
tanpa awak dengan kontrol webcam nirkabel.
Dari tugas akhir ini diharapkan didapatkan manfaat sebagai berikut:
a.  Dapat menjadi referensi dalam pengembangan foto udara untuk misi
militer maupun misi sipil.
b. Menghasilkan sistem foto panorma yang dapat diintegrasikan dengan
ADAHRS yang berada di pesawat udara tanpa awak.

1.  Melakukan pendahuluan dengan cara mencari referensi umum mengenai
bidang UAV dan foto udara, setelah itu melakukan diskusi singkat dengan
dosen pembimbing dan ahli/pakar pada bidang tersebut.
2. Menentukan permasalahan, tujuan penelitian dan batasan masalah dengan
melakukan diskusi dengan tim dan peneliti dari komunitas UAV N2 untuk
mendapatkan kebutuhan dasar operasional foto panorama sebagai
keperluan pesawat terbang tanpa awak
3. Studi pustaka, mencari referensi lanjutan yang sesuai dengan tujuan
perancangan, untuk menentukan rancangan yang akan dibuat untuk solusi
dari permasalahan, didapatkan dari jurnal-jurnal penelitian terdahulu dan
diskusi dengan tim bersama dosen pembimbing, serta para ahli dan pelaku
terdahulu di bidang tersebut.
4. Membuat rancangan sistem dan penggunaan perangkat keras pendukung
yang akan diaplikasikan yang terdiri dari dua bagian:
a. Penggunaan perangkat keras
Meliputi sistem kerja dari rangkaian perangkat keras yang
digunakan, yang berisikan modul single board computer, motor
servo dan  kamera yang hasil datanya dikirimkan ke antarmuka
melalui jaringan nirkabel

