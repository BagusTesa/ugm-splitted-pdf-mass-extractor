Kristal cair merupakan material yang memiliki fase yang terletak antara
fase padat dan cair. Fase ini memiliki sifat-sifat padat dan cair secara bersama-
sama. Molekul-molekulnya memiliki keteraturan arah dan juga dapat bergerak
bebas seperti pada cairan. Fase kristal cair ini berada lebih dekat dengan fase cair
karena dengan sedikit penambahan temperatur (pemanasan), fasenya langsung
berubah menjadi cair. Sifat ini menunjukkan sensitivitas yang tinggi terhadap
temperatur.  Selain temperatur, kristal cair juga sangat sensitif terhadap pemberian
medan luar. Sifat-sifat inilah yang menjadi dasar utama pemanfaatan kristal cair
dalam teknologi (Yohanes, 2010).
Molekul-molekul penyusun kristal ada yang berbentuk batang dan ada
yang berbentuk cakram. Ukuran molekulnya biasanya berada dalam orde
nanometer (nm).  Perbandingan panjang molekul dengan diameter molekul pada
molekul batang atau perbandingan antara diameter molekul dengan ketebalan
pada molekul cakram adalah lima atau lebih.  Karena molekulnya tidak berbentuk
bola, maka selain  posisi, molekul-molekul tersebut juga dapat memiliki arah yang
teratur (Yang dan Wu, 2006).
Terdapat tiga tipe pada kristal cair, yaitu tipe nematik, tipe smektik, dan
tipe kolesterik.  Molekul-molekul pada tipe nematik, memiliki orientasi yang
searah tetapi tidak memiliki posisi yang beraturan.  Molekul-molekul pada tipe
smektik, selain memiliki orientasi yang searah, molekul-molekulnya juga saling
menyusun pada satu arah membentuk lapisan tertentu, tetapi posisi molekul dalam
lapisan tidak beraturan.  Fase kolesterik mengandung  molekul-molekul yang
tersusun secara helik (Yusuf, 2002)
Molekul kristal cair memiliki kecenderungan untuk terorientasi sejajar
pada arah tertentu yang disebut direktor dan diberi lambang n
?
.  Bila dikenakan
medan eksternal, arah dari direktor n
?
akan berubah secara perlahan yang disebut
dengan deformasi (Chandrasekhar dalam Wahyuni, dkk., 2012).  Respon suatu
kristal cair nematik terhadap medan listrik dapat dipelajari dengan cara
menempatkan bahan kristal cair di antara plat kaca sejajar berlapis Indium Tin
Oxide (ITO), sebagai elektroda. Penempatan kristal cair dapat dilakukan dengan
tiga macam kondisi penjajaran arah dari direktor  n
?
terhadap plat tipis elektroda
tersebut.  Kondisi planar ketika direktor  n
?
sejajar sumbu-x ( n
?
= 1, 0, 0), kondisi
homogen ketika direktor  n
?
sejajar sumbu-y ( n
?
= 0, 1, 0),  dan  kondisi
homeotropik ketika  n
?
dalam keadaan sejajar sumbu-z ( n
?
= 0, 0, 1).
Ketika medan listrik diberikan pada kristal cair nematik, maka direktor
n
?
akan mengalami deformasi.  Deformasi direktor n
?
dapat berbentuk splay,
bend dan juga dapat berbentuk twist.  Ketika medan listrik diberikan, maka
molekul kristal cair akan mengalami reorientasi yang pertama kali yang disebut
dengan transisi Freederickz.  Transisi Freedericksz  terjadi ketika medan listrik
yang diberikan berada pada interval cE E? ,  dengan Ec adalah medan listrik
kritis.  Potensial listrik pada saat Ec disebut potensial kritis (Vc).
Berbagai macam eksperimen dan pengkajian dilakukan oleh para
peneliti dalam mempelajari sifat kristal cair nematik pada transisi Freedericksz.
Sebelum melakukan eksperimen, para peneliti terlebih dahulu merumuskan
persamaan matematik medan listrik kritis (Ec) dan potensial kritis (Vc).  Setelah
mendapatkan persamaan tertentu, para peneliti melakukan eksperimen kemudian
medan listrik kritis (Ec) dan potensial kritis (Vc) dihitung menggunakan persamaan
yang telah diperoleh sebelumnya.
Para peneliti tertarik untuk mempelajari fenomena transisi Freedericksz
terhadap kristal cair,  karena pada fenomena ini dapat ditentukan besarnya medan
listrik kritis (Ec) dan potensial kritis (Vc) dengan berbagai asumsi.  Biasanya,
eksperimen dilakukan dengan menempatkan kristal cair diantara plat kaca sejajar
kemudian diberikan  medan listrik.  Arah medan listrik yang diberikan bervariasi.
Secara umum, medan listrik yang diberikan pada eksperimen tersebut memiliki
arah sejajar atau tegak lurus molekul-molekul kristal cair.  Sehingga, perumusan
persamaan medan listrik kritis (Ec) dan potensial kritis (Vc) lebih banyak
dilakukan dengan menganggap bahwa medan listrik yang diberikan memiliki arah
sejajar atau tegak lurus molekul-molekul kristal cair.
Perumusan tentang medan kritis (Ec) dan potensial kritis (Vc) sangat
penting dalam mempelajari fenomena transisi Freedericksz, karena medan listrik
diatas medan kritis (Ec)  merupakan awal direktor kristal cair mengalami
reorientasi.  Dengan menentukan medan kritis (Ec) dan potensial kritis (Vc), dapat
diketahui besarnya medan listrik yang harus diberikan agar terjadi fenomena
transisi Freedericksz.  Namun secara umum penentuan medan kritis (Ec) dan
potensial kritis (Vc), lebih banyak dilakukan dengan menganggap medan luar yang
diberikan tegak lurus atau sejajar direktor kristal cair.  Oleh karena itu, pada
penelitian ini dirumuskan medan kritis (Ec) dan potensial kritis (Vc) dengan
menganggap arah medan listrik yang diberikan berada pada kemiringan ?
terhadap sumbu-x pada kondisi planar, kemudian arah medan listrik membentuk
kemiringan ?  terhadap sumbu-y pada kondisi homogen, dan arah medan listrik
membentuk kemiringan ?  terhadap sumbu-z pada kondisi homeotropik.
1.2 Batasan Masalah
Pada penelitian ini, masalah dibatasi pada molekul kristal cair dianggap
berbentuk batang.  Direktor kristal cair dianggap terletak pada tiga kasus, yaitu:
1. Berada pada kondisi planar dengan direktor  n
?
sejajar sumbu-x ( n
?
= 1, 0, 0).
2. Berada pada kondisi homogen dengan direktor  n
?
sejajar sumbu-y
( n
?
= 0, 1, 0).
3. Berada pada kondisi homeotropik dengan direktor n
?
sejajar sumbu-z
( n
?
= 0, 0, 1).
Kristal cair yang ditinjau adalah kristal cair nematik.  Sumbu-x dipilih sejajar
terhadap plat kaca Indium Tin Oxide (ITO).  Molekul-molekul kristal cair yang
berdekatan dengan plat kaca, dianggap dalam kondisi terjangkar kuat.  Setelah
diberi medan listrik, direktor kristal cair dianggap melakukan penjajaran dengan
medan listrik yang diberikan ( 0?? ? ).
Rumusan masalah penelitian ini adalah bagaimana persamaan medan
listrik kritis (Ec) dan potensial kritis (Vc) pada kristal cair nematik untuk kasus
medan listrik luar yang memiliki arah membentuk kemiringan ( ) terhadap
masing-masing arah sumbu penjajaran direktor.
1.4 Tujuan dan Manfaat Penelitian
Tujuan penelitian adalah merumuskan persamaan medan listrik kritis
(Ec) dan potensial kritis (Vc) pada kristal cair nematik untuk kasus medan listrik
luar yang memiliki arah membentuk kemiringan ( ) terhadap masing-masing arah
sumbu penjajaran director.
Manfaat penelitian adalah:
1. Bagi peneliti, dapat menjadi tambahan ilmu pengetahuan pada bidang minat
kristal cair dan sebagai dasar untuk melanjutkan pengkajian tentang transisi
2. Bagi peneliti lainnya, dapat dijadikan acuan untuk mengembangkan penelitian
selanjutnya yang berkaitan dengan transisi Freedericksz pada Kristal cair.
Penelitian ini bersifat teori dengan mengacu pada beberapa literatur.
Pengetahuan tentang Persamaan Diferiansial Biasa (PDB) disarankan dalam
penyelesaian kasus ini. Asas variasi Euler-Lagrange digunakan untuk
penyelesaian awal kasus dalam tesis ini.  Kondisi molekul terjangkar kuat
digunakan sebagai syarat batas awal dan syarat batas akhir dalam penyelesaian

