Di dalam kehidupan kita sehari-hari, suatu sistem dengan perencanaan yang
sangat kompleks sangat dibutuhkan guna mempermudah dalam membantu kehidupan
manusia.  Apalagi jika sistem tersebut bergerak dengan suatu kontrol yang terpadu,
maka hal ini akan membawa dampak kepada manusia untuk bisa memikirkan dan
membuat suatu bentuk kontrol yang sekiranya akan dapat membantu dengan efisien.
Salah satunya adalah sistem pengontrolan pada rumah kaca.  Rumah kaca
merupakan suatu bangunan yang berfungsi untuk membudidayakan tanaman.  Jenis
tanaman yang dibudidayakan tergantung dari kebutuhan akan pengembangannya itu
sendiri. Jika ditinjau dengan lebih seksama, maka pengembangan tanaman atau
budidaya di dalam rumah kaca tidaklah semudah yang di bayangkan.  Banyak faktor-
faktor yang mempengaruhi dalam pengembangan budidaya tanaman tersebut,
misalnya faktor suhu, kelembaban, kebutuhan akan penyinaran atau intensitas cahaya
yang digunakan, dan lain-lain.
Semua itu merupakan kombinasi yang harus diketahui dalam meneliti
pertumbuhan serta perkembangan tanaman. Untuk mempermudah proses penyiraman
tanaman pada rumah kaca maka dibuat suatu sistem kontrol yang terpadu dengan
tujuan untuk mengatur serta mengendalikan keseluruhan sistem penyiraman otomatis
serta mempermudah perawatannya tanpa harus melakukan campur tangan manusia
secara langsung.
Salah satu faktor yang mempengaruhi pada perkembangan tanaman yaitu
penyiraman.  Penyiraman merupakan suatu hal yang tidak dapat dilepaskan dalam
menjaga serta merawat agar tanaman dapat tumbuh dengan subur.  Kebutuhan air
yang cukup merupakan salah satu hal yang sangat penting. Jika hal ini tidak
diperhatikan maka akan berdampak fatal bagi perkembangan tanaman itu sendiri.
Faktor lain yang mempengaruhi kebutuhan air pada tanaman ( Sintia, 2008 ) adalah :
1) Jenis, bentuk, dan umur tanaman
Berdasarkan kebutuhan air, umumnya ada tiga jenis tanaman, yaitu:
a. Jenis yang suka air, yaitu tanaman yang memerlukan kebutuhan air yang cukup
banyak.  Untuk dapat hidup dengan baik, contohnya jenis Adiantum, Begonia,
Calathea, Dracaena, Dieffenbachia, Monstera, Peperomia serta jenis pakis-pakisan.
b. Jenis yang menyukai air dalam jumlah sedang, yaitu tanaman yang memerlukan air
yang cukup tapi tidak berlebih untuk tumbuh dalam kondisi yang sehat, contohnya
adalah Aglaonema, Anthurium, Philodendron, dan lainnya.
c. Jenis yang menyukai sedikit air, yaitu jenis tanaman yang dapat tumbuh dengan baik
dalam keadaan sedikit air, contohnya berbagai jenis tanaman sukulen, kaktus,
Sansiviera, Chryptanthus dan lainnya.  Bentuk daun juga harus diperhatikan, jika
daunnya besar dan tipis, berarti tanaman tidak kuat pada kondisi kering dan
membutuhkan relatif lebih banyak air dalam penyiraman.  Jika daun ada lapisan
lilinnya berarti sedikit tahan akan kondisi kering.  Daun kecil akan menghindari
penguapan air saat siang hari.  Akan tetapi penting pula diketahui jenis tanamannya,
apakah tanaman menyukai air atau tidak.
2) Lokasi dan Kondisi Sekitar Tanaman.
Lokasi juga mempunyai peran yang besar di dalam menentukan banyaknya air untuk
penyiraman.  Tanaman di dalam pot yang diletakkan di bawah naungan dengan yang
langsung di bawah sinar matahari akan mempunyai perbedaan kebutuhan air.
Umumnya tanaman yang berada di daerah naungan membutuhkan jumlah air yang
relatif lebih sedikit dari pada tanaman yang terkena sinar matahari langsung.
Media merupakan material yang bersentuhan langsung dengan akar, bagian tanaman
yang sangat penting untuk penyerapan air dan unsur hara lainnya.  Media tanaman
yang umum digunakan adalah tanah, humus, sekam, cocopeat, pasir malang, dan akar
pakis.  Masing-masing jenis mempunyai daya ikat air yang berbeda-beda.
Pot yang kecil akan mempunyai tingkat kelembaban yang lebih kecil jika
dibandingkan dengan media pada pot yang besar.  Tetapi pot yang besar mempunyai
kelebihan dalam pertumbuhan akar tanaman.  Banyaknya ruang yang tersedia dapat
memberikan ruang yang cukup untuk bernafasnya akar.
Dua musim di Indonesia, musim kering dan musim hujan, akan mempengaruhi
penyiraman terhadap tanaman.  Pada musim kering, tanaman harus diperiksa apakah
memerlukan penyiraman satu-dua hari sekali sedangkan musim hujan apakah harus
disiram setiap hari atau tidak.  Jika hal ini tidak dilakukan maka tanaman akan cepat
mengalami kematian.
Untuk menjaga kualitas agar tanaman terawat dengan baik dan benar, maka
banyak sekali hal yang harus dilakukan, salah satunya adalah dengan cara menyiram
tanaman sesuai dengan kondisi lingkungan khususnya pada tanah tanaman.  Berdasarkan
uraian yang terdapat dalam latar belakang di atas, maka dalam tugas akhir ini akan
dibuat sebuah � Purwarupa Sistem Penyiraman Tanaman  Otomatis Berbasis
Sensor Kelembaban Tanah Dan Arduino UNO �.  Pada alat ini akan digunakan
sebuah mikrokontroler Arduino UNO, 2 buah sensor soil moisture untuk mengukur
kelembaban tanah, sensor DHT11 untuk mengukur suhu dan kelembaban udara,
grove relay sebagai saklar untuk on/off  pompa air dan LCD 16x2 sebagai penampil
nilai sensor soil moisture dan sensor DHT11.
Dalam pembuatan dan uji coba sistem ini, diberikan beberapa batasan
masalah sebagai berikut:
1)  Sensor kelembaban tanah  ( Sensor Soil Moisture Immersion Gold Arduino
Compatible ) hanya digunakan untuk mengindra kelembaban tanah di dalam pot.
2) Sensor suhu dan kelembaban udara yang digunakan adalah sensor DHT11 hanya
berfungsi sebagai pengindra suhu dan kelembaban udara di ruangan pot.
3)  Relay yang digunakan adalah grove relay sebagai saklar ON/OFF pompa air.
4)  Informasi pengindraan sensor soil moisture dan sensor DHT11 ditampilkan
melalui LCD 16x2.
5) Bahasa pemrograman yang di gunakan yaitu bahasa C.
Adapun tujuan penelitian ini adalah sebagai berikut :
1) Memanfaatkan mikrokontroler ATmega328 dengan board Arduino UNO sebagai
pusat kendali dari sistem penyiraman tanaman otomatis.
2) Membuat sebuah purwarupa rumah kaca menggunakan toples plastik sebagai
tempat tanaman dan menggunakan sensor soil moisture untuk mengindra
kelembaban tanah.
Metode-metode yang digunakan dalam penelitian ini adalah sebagai berikut:
1) Menentukan topik yang akan diangkat yang dilatarbelakangi dengan keadaan dan
permasalahan.
2) Melakukan kajian dan pembelajaran lebih lanjut tentang sistem yang dibahas pada
penelitian ini dengan metode:
a) Studi literatur, yaitu mempelajari artikel, makalah, jurnal, karya tulis, serta buku-
buku yang terkait dengan Arduino UNO, sensor soil moisture, sensor DHT11, grove
relay, dan LCD 16x2.
b) Konsultasi dengan dosen pembimbing mengenai rancangan sistem dan inovasi-
inovasi yang bisa diterapkan pada sistem.
3) Membuat perancangan sistem yang terdiri dari dua bagian, yaitu:
a) Perangkat Keras (Hardware)
Membuat desain kontruksi hardware dan membuat skematik rangkaian.
b) Perangkat Lunak (Software)
Membuat program yang akan ditanam di dalam mikrokontroler dengan menggunakan
bahasa C melalui compiler Arduino 1.0.4 dan library AVRLib sehingga sistem yang
ada dapat berfungsi sebagaimana yang diharapkan.  Untuk software antarmuka pada
komputer menggunakan serial monitor yang terdapat pada software Arduino 1.0.4
dan LCD 16x2.
4) Pengujian untuk setiap bagian sistem, pengujian terhadap kinerja sensor dalam
berbagai kondisi, pengujian penggabungan data antara kelembaban tanah dengan
sensor.
5) Langkah terakhir adalah melakukan pengujian sistem secara keseluruhan untuk
memastikan apakah sistem telah dapat bekerja dengan baik dan hasilnya sesuai
dengan yang diinginkan atau tidak.
Untuk mempermudah pembahasan dan pemahaman maka penulis membuat
sistematika pembahasan bagaimana sebenarnya prinsip kerja dari maka penulis
menulis laporan ini sebagai berikut :
Dalam bab ini berisikan mengenai latar belakang, rumusan masalah, batasan
masalah, tujuan penulisan, metode penelitian serta sistematika penulisan.
Bab ini memaparkan hasil penelitian terdahulu atau teknologi yang berkaitan
dengan penelitian yang dilakukan oleh penulis.
Landasan teori, dalam bab ini dijelaskan tentang teori pendukung yang
digunakan untuk pembahasan dan cara kerja dari rangkaian. Teori pendukung itu
antara lain tentang board minimum sistem Arduino UNO ( hardware dan software ),
mikrokontroler ATmega328, sensor soil moisture, sensor DHT11, grove relay, LCD
16x2 dan dasar teori tentang kelembaban tanah.
Pada bagian ini akan dibahas perancangan dari alat, yaitu diagram blok dari
rangkaian, skematik dari masing-masing rangkaian dan diagram  alir dari program
yang akan diisikan ke mikrokontroler ATmega328.
Bab ini berisi penjelasan tentang implementasi dari rancangan yang telah dibuat
sebelumnya yang meliputi implementasi secara hardware maupun secara software.
Bab ini berisi pengujian sistem secara keseluruhan untuk kemudian dibahas dan
dianalisis hasil dan kinerjanya.
Bab ini merupakan penutup yang meliputi tentang kesimpulan dari
pembahasan yang dilakukan dari tugas akhir ini serta saran apakah rangkaian ini
dapat dibuat lebih efisien dan dikembangkan perakitannya pada suatu metode lain
yang mempunyai sistem kerja yang sama.

Berdasarkan hasil penelitian tugas akhir ini, maka dapat diambil kesimpulan
sebagai berikut :
1. Telah diimplementasikan dengan baik sesuai perancangan sistem, purwarupa
sistem penyiraman otomatis berbasis sensor soil moisture  dan Arduino UNO.
2. Grove relay akan memicu  pompa air on/off, saat pengindraan soil moisture 1
dan 2 nilai kelembaban tanah di bawah 300 berati tanah kering maka pompa
air akan on, dan jika nilai kelembaban tanah pada soil moisture 1 dan 2 di atas
700 berati tanah basah  maka pompa akan off.
3. Pengindraan suhu dan kelembaban udara oleh sensor DHT11 di dalam pot,
diperoleh nilai yang kurang akurat dan respon terhadap perubahan suhu
kelembaban udara kurang cepat.
Untuk pengembangan sistem penyiraman otomatis yang lebih baik, diberikan
saran sebagai berikut:
1. Sistem dapat dibuat dengan menggunakan teknologi wireless agar dapat
diakses jarak jauh, seperti pengambilan data dan pemantauan penyiraman.
2. Dapat dibuat sistem penyiraman otomatis dengan menggunakan  kendali PID
agar dapat bekerja dengan lebih optimal.
3. Penggunaan sensor DHT11 sebagai pengindra suhu dan kelembaban udara
kurang akurat, sebaiknya menggunakan sensor SHT22.
4. Dapat ditambahkan penyemprotan air melalui udara menggunakan shower
dengan nilai pengindraan kelembaban udara sensor DHT11.

