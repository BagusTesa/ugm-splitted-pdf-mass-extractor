Unit Kegiatan Mahasiswa (UKM) sepak bola Universitas Gadjah Mada
(UGM) merupakan sebuah wadah bagi aktivitas kemahasiswaan untuk
mengembangkan minat, bakat dan keahlian di dalam bidang sepak bola.
Mahasiswa yang ingin mengembangkan bakatnya dapat bergabung bersama UKM
sepak bola UGM. Kegiatan latihan sepak bola dilaksanakan di lapangan Pancasila,
UKM sepak bola UGM saat ini memerlukan suatu cara pendaftaran yang
mampu mengolah data pendaftar, serta dapat mempermudah cara pendaftaran.
Selama ini cara pendaftaran yang digunakan pada UKM sepak bola UGM masih
menggunakan cara manual. Mahasiswa yang ingin mendaftar pada UKM sepak
bola UGM harus datang pada acara Gelanggang Expo untuk kemudian mengisi
kertas formulir pendaftaran yang telah disediakan. Untuk menjaga data pendaftar
agar tidak hilang atau rusak akibat kerusakan pada kertas formulir maka panitia
pendaftaran harus melakukan penyimpanan kembali data pendaftar kedalam
software Microsoft Excel.
Selama ini informasi mengenai jadwal latihan diberitahukan melalui papan
pengumuman yang berada di gelanggang mahasiswa. Untuk informasi lainnya
seperti profil UKM, jadwal pertandingan, dokumentasi kegiatan, galeri foto, data
anggota dan data kepengurusan belum ada media komunikasi yang memberikan
informasi tersebut kepada anggota UKM Sepak Bola khususnya maupun
mahasiswa UGM pada umumnya.
Seiring perkembangan zaman, dengan semakin mudahnya akses internet
yang dapat dilakukan oleh mahasiswa. Sehingga pada saat ini mahasiswa
cenderung melalukan pencarian informasi melalui media online. Berdasarkan
masalah tersebut, maka akan dibuatlah sebuah sistem informasi UKM Sepak Bola
UGM berbasis web yang dapat memberikan kemudahan bagi mahasiswa dan
panitia pendaftaran dalam hal pendaftaran anggota baru, pengeloalaan data
pendaftar serta dapat memberikan informasi secara cepat dan akurat kepada
anggota UKM sepak bola UGM khususnya maupun mahasiswa UGM pada
umumnya. Sistem ini juga dilengkapi dengan fitur memberikan masukan berupa
kritik dan saran. Sehingga nantinya diharapkan dengan kritik dan saran yang
diberikan dapat menjadi masukan yang baik untuk kemajuan UKM sepak bola
Berdasarkan latar belakang yang telah dijelaskan, permasalahan yang akan
dibahas adalah bagaimana membuat sebuah sistem informasi  UKM sepak bola
UGM yang memiliki fitur pendaftaran online serta dapat memberikan informasi
mengenai Unit Kegiatan Mahasiswa sepak bola Universitas Gadjah Mada.
Adapun batasan masalah yang akan dibahas pada penyusunan tugas akhir
ini meliputi hal � hal sebagai berikut :
1. Sistem dirancang berbasis web.
2. Sistem dapat melayani pendaftaran anggota baru UKM Sepak Bola UGM.
3. Pendaftaran hanya dibuka berdasarkan waktu pendaftaran yang telah di
tentukan oleh pihak UKM Sepak Bola Universitas Gadjah Mada.
4. Sistem dapat mencatat data konfirmasi pembayaran.
5. Laporan yang akan dihasilkan adalah laporan pendaftar, laporan anggota dan
laporan pengurus UKM sepak bola UGM.
6. Sistem tidak menyediakan fasilitas pembayaran.
7. Pengembangan sistem tidak memperhatikan keamanan jaringan.
Tujuan yang ingin dicapai dalam penelitian ini adalah merancang dan
mengimplementasikan sistem informasi UKM sepak bola UGM yang memiliki
fitur  pendaftaran anggota baru. Serta  dapat memberikan informasi mengenai
Adapun manfaat dari penelitian ini adalah sebagai berikut :
1. Memberikan kemudahan bagi mahasiswa dalam melakukan proses
pendaftaran.
2. Memudahkan panitia pendaftaran dalam melakukan pendataan anggota baru.
3. Mempercepat proses pendataan anggota baru.
4. Mempercepat proses penyebaran informasi mengenai UKM sepk bola UGM.
5. Memudahkan mahasiswa untuk mendapatkan informasi mengenai UKM
sepak bola UGM.
6. Memudahkan anggota UKM sepak bola maupun mahasiswa UGM untuk
memberikan masukan.
Metode � metode yang di gunakan di dalam penelitian ini adalah sebagai
berikut :
Metode pengumpulan data dengan melakukan pengamatan langsung suatu
kegiatan yang sedang di lakukan. Pengamatan dilakukan pada UKM sepak bola
UGM untuk memahami proses pendaftaran anggota baru serta peneliti pernah
beberapa kali mengikuti secara langsung proses pendaftaran anggota baru.
Wawancara dilakukan dengan ketua koordinator UKM sepak bola UGM.
Wawancara ini dilakukan dengan tujuan untuk mendapatkan data dan informasi
yang di butuhkan dan berhubungan dengan penelitian atau perancangan sistem.
Metode ini dilakukan dengan mengumpulkan dan mempelajari referensi
informasi pustaka mengenai sistem yang serupa atau pernah dibuat yang berkaitan
dengan sistem informasi UKM sepak bola UGM. Serta mengumpulkan dan
mempelajari informasi � informasi yang berkaitan dengan penulisan termasuk
perancangan, analisis, dan implementasi sistem. Kegiatan ini dilaksanakan di
perpustakaan.
Metode ini digunakan dalam proses pembuatan sistem. Adapun proses
yang terlibat adalah sebagai berikut :
a. Analisis Sistem
Analisis untuk mencari kebutuhan sistem. Kegiatan ini dilakukan untuk
mencari kebutuhan fungsional dan non-fungsional dari sistem yang akan di
rancang.
b. Perancangan Sistem
Merancang proses-proses apa saja yang dapat dikerjakan oleh sistem
kemudian merancang data yang dibutuhkan didalam proses tersebut kedalam
sistem database serta merancang user interface sistem.
c. Implementasi Sistem
Pada tahap ini peneliti menerapkan semua hasil analisis dan rancangan
yang telah dilakukan sebelumnya kedalam sebuah sistem informasi berbasis web
dengan menggunakan bahasa pemrograman HTML, CSS, PHP, dan Javascript.
Untuk memudahkan dalam memahami pembahasan. Maka sistematika
penulisan laporan ini dibagi menjadi beberapa bab dengan pokok pembahasan
sebagai berikut :
Pada bab ini menjelaskan uraian informasi tentang latar belakang masalah,
batasan masalah, tujuan dan manfaat penelitian berikut tentang metodologi
penelitian serta sistematika penulisan.
Pada bab ini berisi uraian tentang informasi hasil penelitian yang di
peroleh dari data pustaka dan menghubungkannya dengan sistem informasi UKM
sepak bola UGM.
Pada bab ini berisi tentang konsep dan teori-teori yang pernah ada untuk
mendukung pembuatan sistem informasi ini dan dapat digunakan sebagai
pembanding dalam pembahasan masalah. seperti pengetahuan tentang sistem
pakar, bahasa pemrograman, serta software basis data yang digunakan.
Pada bab ini, berisi analisis dan perancangan sistem yang akan dibangun
dan di gunakanan dalam implementasi aplikasi sistem. Analisis dan perancangan
sistem terdiri dari analisis kebutuhan sistem, perancangan alur proses kerja dan
data, perancangan model basisdata, serta perancangan antar muka pengguna.
Pada bab ini berisi penjelasan dari implementasi analisis dan perancangan
sistem yang telah di buat sebelumnya menjadi aplikasi sistem beserta antar muka
aplikasi dengan menggunakan bahasa pemrograman PHP dengan framework
CodeIgniter disertai cara kerja dan penggunaan program.
Pada bab ini akan menjelaskan mengenai proses pengujian sistem yang
telah di buat sebagai tolak ukur apakah sistem telah sesuai dengan yang di
harapkan atau belum.
Pada bab ini berisi uraian penutup dari laporan tugas akhir ini. Uraian
kesimpulan dari laporan tugas akhir dan saran terhadap seluruh kegiatan tugas
akhir yang telah dilakukan juga terdapat dalam bab ini sehingga dapat dijadikan
acuan bila ingin mengembangkan sistem lebih lanjut.

Berdasarkan pengujian yang telah dilakukan pada Sistem Informasi UKM
Sepak Bola UGM dapat di ambil beberapa kesimpulan sebagai berikut :
1. Sistem Informasi UKM Sepak Bola UGM telah dibangun menggunakan
bahasa pemrograman PHP dengan software framework codeighniter dan
menggunakan basisdata MySql.
2. Sistem Informasi UKM Sepak Bola UGM dapat melakukan proses
pengolahan data seperti profil UKM, jadwal pertandingan, dokumentasi
kegiatan, galeri foto, data anggota dan data kepengurusan serta dapat
melakukan proses pendaftaran dan pendataan anggota baru.
3. Sistem Informasi UKM Sepak Bola UGM dapat melakukan pengolahan data
laporan seperti laporan pendaftar, laporan anggota dan laporan pengurus.
Untuk melakukan pengembangan terhadap Sistem Informasi UKM Sepak
Bola UGM, penulis memberikan beberapa saran diantaranya:
1. Sistem dilengkapi dengan fitur sms gateway untuk memberikan informasi
mengenai pengujian para mahasiswa yang mendaftar UKM Sepak Bola dan
pengumuman penerimaan calon anggota baru UKM Sepak Bola.

