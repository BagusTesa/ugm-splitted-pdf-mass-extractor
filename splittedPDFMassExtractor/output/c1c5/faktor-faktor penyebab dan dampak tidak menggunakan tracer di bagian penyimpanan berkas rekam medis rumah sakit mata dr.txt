A. Latar Belakang
Menurut Wolper dan Pena dalam Azwar (1996) rumah sakit adalah
tempat dimana orang sakit mencari dan menerima pelayanan kedokteran
serta tempat dimana pendidikan klinik untuk mahasiswa kedokteran,
perawat dan tenaga profesi kesehatan lainnya diselenggarakan. Rumah
sakit merupakan salah satu tempat pelayanan kesehatan mempunyai
kewajiban administrasi untuk membuat dan memelihara rekam medis
pasien. Hal ini sesuai dengan Permenkes 269 tahun 2008 pasal 7 yaitu
sarana pelayanan kesehatan wajib menyediakan fasilitas yang diperlukan
dalam rangka  penyelenggaraan rekam medis.
Rekam medis adalah berkas yang berisikan catatan dan dokumen
tentang identitas pasien, pemeriksaan, pengobatan, tindakan dan pelayanan
lain yang telah diberikan kepada pasien (Permenkes 269, 2008). Setiap
rumah sakit harus membuat rekam medis baik itu rekam medis rawat jalan
maupun rekam medis rawat inap. Rekam medis juga berguna sebagai bukti
tertulis atas tindakan-tindakan pelayanan terhadap seorang pasien, juga
mampu melindungi kepentingan hukum bagi pasien yang bersangkutan,
rumah sakit, dokter, dan tenaga kesehatan lainnya, apabila di kemudian
hari terjadi sesuatu hal yang tidak diinginkan menyangkut rekam medis itu
sendiri.
Rekam medis sangat penting dalam mengemban mutu pelayanan
medik yang diberikan rumah sakit. Salah satu faktor dalam peningkatan
mutu dan pelayanan rekam medis di rumah sakit adalah di bagian
penyimpanan, karena penyimpanan berkas rekam medis yang baik akan
menunjang penyelenggaraan rekam medis yang bermutu pada suatu rumah
sakit. Pengelolaan rekam medis khususnya di bagian penyimpanan tidak
terlepas dari sarana yang tersedia seperti rak penyimpanan, sampul berkas
dan petunjuk keluar (outguide), serta peralatan lainnya. Menurut Hatta
(2008), outguide atau kartu petunjuk atau yang sering disebut dengan
tracer adalah kartu yang digunakan untuk mengganti berkas rekam medis
yang diambil untuk berbagai keperluan. Tracer berisi tentang tanggal
peminjaman, nama peminjam, unit pengguna,  serta keperluan peminjam.
Kartu ini harus diisi sebelum rekam medis dipinjam sebagai pengganti
rekam medis yang keluar dari rak penyimpanan. Penggunaan tracer
bertujuan untuk mempermudah petugas dalam menelusuri keberadaan
rekam medis yang tidak berada di rak penyimpanan. Dengan adanya
tracer, proses pencarian jejak atau kontrol terhadap rekam medis pasien
akan lebih mudah untuk ditelusuri sehingga apabila berkas rekam medis
yang dibutuhkan tidak berada di rak penyimpanan, maka dengan mudah
petugas dapat mengetahui keberadaan berkas tersebut dengan bantuan
tracer.
Berdasarkan studi pendahuluan di Rumah Sakit Mata �Dr. Yap�
Yogyakarta pada tanggal 21 Maret 2013, petugas di bagian penyimpanan
rekam medis di Rumah Sakit Mata �Dr. Yap� Yogyakarta hanya
berjumlah 2 (dua) orang. Petugas yang pertama datang pada pukul 08.00
WIB dan petugas yang kedua datang pada pukul 11.00 WIB. Jadi kalau
masuk shift pukul 08.00 WIB, maka petugas filing tersebut akan
mencarikan berkas dan mendistribusikan berkas rekam medis sendiri
sampai pukul 11.00 WIB.
Di Rumah Sakit Mata �Dr. Yap� Yogyakarta akan dilaksanakan
sertifikasi akreditasi, namun petugas rekam medis tidak menggunakan
tracer pada waktu mengambil berkas rekam medis. Penggunaan tracer
adalah salah satu hal yang dinilai oleh tim akreditasi. Oleh sebab itu,
peneliti mengambil judul Faktor-Faktor Penyebab Dan Dampak Tidak
B. Rumusan Masalah
Berdasarkan latar belakang tersebut, maka rumusan masalah dalam
penelitian ini adalah �Apa saja faktor-faktor penyebab dan dampak tidak
menggunakan tracer di bagian penyimpanan berkas rekam medis di
C. Tujuan Penelitian
Mengetahui pelaksanaan pengambilan dan penyimpanan kembali
berkas rekam medis di Rumah Sakit Mata �Dr. Yap� Yogyakarta.
a. Mengetahui apa saja faktor-faktor penyebab tidak menggunakan
tracer di bagian penyimpanan berkas rekam medis di Rumah Sakit
b. Mengetahui apa saja dampak tidak menggunakan tracer di bagian
penyimpanan berkas rekam medis di Rumah Sakit Mata �Dr. Yap�
D. Manfaat Penelitian
Peneliti dapat menambah pengetahuan dan pengalaman dengan
mengetahui permasalahan yang diteliti serta menerapkan teori yang di
dapat di perguruan tinggi.
Memberikan masukan materi yang berharga sebagai sumber
pembelajaran bagi pendidikan mahasiswa D3 rekam medis.
Sebagai masukan dan sumbangan pemikiran dalam hal penggunaan
tracer.
E. Keaslian Penelitian
Menurut sepengetahuan peneliti, penelitian mengenai Faktor-Faktor
Berkas Rekam Medis Di Rumah Sakit Mata �Dr. Yap� Yogyakarta belum
pernah dilakukan sebelumnya. Namun beberapa penelitian yang serupa
pernah dilakukan antara lain :
1. Mahendra (2011), dengan judul �Pemanfaatan Tracer di Penyimpanan
Berkas Rekam Medis di UPT Puskesmas Wonosari 1�. Hasil
penelitian Mahendra (2011) bahwa saat petugas penyimpanan di UPT
Puskesmas Wonosari 1 sebelum menggunakan tracer mengalami
banyak kendala, antara lain: berkas tidak ditemukan, banyak misfile.
Setelah menggunakan tracer masalah-masalah tersebut teratasi.
Dengan adanya tracer di penyimpanan Berkas Rekam Medis UPT 1
Puskesmas Wonosari 1 dapat mengurangi berkas misfile. Persamaan
penelitian ini dengan penelitian Mahendra terletak pada metode
penelitian deskriptif dengan pendekatan kualitatif yaitu melihat
langsung saat penelitian. Sedangkan perbedaan terletak pada waktu,
tempat, dan tujuan penelitian. Penelitian Mahendra dilakukan pada
tahun 2011 di UPT Puskesmas Wonosari 1 yang bertujuan untuk
mengetahui pelaksanaan pengambilan dan penyimpanan berkas rekam
medis sebelum dan sesudah menggunakan tracer. Sedangkan
penelitian ini dilaksanakan pada tahun 2013 di Rumah Sakit Mata
�Dr. Yap� Yogyakarta dengan tujuan mengetahui faktor-faktor
penyebab dan dampak tidak menggunakan tracer di bagian
penyimpanan berkas rekam medis Rumah Sakit Mata �Dr. Yap�
2. Suryaningsih (2006), dengan judul �Penggunaan Tracer (Outguide)
Medis Di Rumah Sakit Umum Daerah Wates�. Hasil penelitian ini
menunjukkan tracer mempunyai peranan yang sangat penting dalam
pengambilan dan penyimpanan berkas rekam medis. Persamaan
penelitian ini dengan penelitian Suryaningsih sama-sama tentang
tracer. Perbedaanya adalah penelitian Suryaningsih bertujuan untuk
mengetahui peranan tracer dalam pelaksanaan pengambilan dan
penyimpanan berkas rekam medis di Rumah Sakit Umum Daerah
Wates. Sedangkan penelitian ini bertujuan mengetahui faktor-faktor
penyebab dan dampak tidak menggunakan tracer di bagian
penyimpanan berkas rekam medis Rumah Sakit Mata �Dr. Yap�
3. Wulandari (2006) dengan judul �Efektifitas Penggunaan Tracer di
bagian Penyimpanan Berkas Rekam Medis Rumah Sakit Jiwa Prof Dr.
Soeroyo Magelang�. Hasil penelitian Wulandari (2006) bahwa
penggunaan tracer di bagian Penyimpanan Berkas Rekam Medis
Rumah Sakit Jiwa Prof Dr. Soeroyo Magelang sudah efektif. Petugas
di bagian penyimpanan sudah menggunakan tracer saat pelaksanaan
pengambilan berkas rekam medis. Dengan adanya tracer dapat
membantu petugas filing dalam melaksanakan kegiatan di bagian
penyimpanan berkas rekam medis. Persamaan penelitian ini dengan
penelitian Wulandari terletak pada metode penelitian deskriptif
dengan pendekatan kualitatif yaitu melihat langsung saat penelitian.
Sedangkan perbedannya Wulandari meneliti keefektifan penggunaan
tracer dan apa saja faktor yang mempengaruhi efektifitas pelaksanaan
penggunaan tracer di bagian penyimpanan berkas rekam medis.
Penelitian ini meneliti tentang faktor-faktor penyebab dan dampak
tidak menggunakan tracer di bagian penyimpanan berkas rekam
medis Rumah Sakit Mata �Dr. Yap� Yogyakarta.
F. Gambaran Umum Rumah Sakit
Berdasarkan buku 80 tahun RS. Mata Dr. YAP (1923), terdapat
gambaran umum tentang Rumah Sakit Mata �Dr.Yap� Yogyakarta yang
menyebutkan bahwa Rumah Sakit Mata �Dr. Yap � Yogyakarta adalah
rumah sakit khusus penyakit mata yang juga melakukan tindakan bedah
mata milik yayasan Yap Prawirohusodo. Rumah Sakit Mata �Dr. Yap�
Yogyakarta terletak di Jl. Cik Ditiro no 5 Yogyakarta 55223, telp/fax
sekarang telah mengalami perkembangan dalam segi fasilitas dalam
perawatan maupun fasilitas penunjang lainnya seperti laboratorium dan
lainnya.
Berdasarkan sertifikat akreditasi rumah sakit no HK.03.01/C.
III/SK/970/2010 Rumah Sakit Mata �Dr. Yap� Yogyakarta adalah rumah
sakit swasta yang berjenis khusus dan bertipe B.
1 Kunjungan rawat jalan 6340 Orang 6191 Orang
2 Pasien One Day care (ODC) 21 Orang 12 Orang
4 LOS (Length of Stay) 2,5 Hari 2,44 Hari
7 Jumlah pasien operasi 366 Orang 433 Orang
Sumber: Laporan bulanan Inst. Rekam Medis RS Mata �Dr. Yap� Yogyakarta
(2013)
3. Jenis Pelayanan di Rumah Sakit Mata �Dr. Yap� Yogyakarta
Pelayanan yang dilakukan adalah pelayanan khusus penyakit mata terdiri
dari pelayanan rawat jalan, rawat inap, tindakan bedah dan pelayanan gawat
darurat khusus mata buka 24 jam. Kemudian terdapat pelayanan sub
spesialistik sebagai berikut:
a. Layanan Sub Spesialistik :
1) Refraksi dan Lensa Kontak
2) Kornea dan infeksi mata luar
3) Uvea dan immunologi
4) Lensa dan katarak
7) Neuro oftalmologi dan Genetika Oftalmologi
8) Okuloplasti dan Rekonstruksi
9) Strabismus dan PO
b. Layanan Penunjang Diagnostik :
1) Refrakto keratometri
2) Pachimetri dan Orbscan mata
3) Tonometri non katarak
5) Biometri mata
7) Perimetri statik
8) Perimetri kinetik
9) CT scan mata (OCT)
10) Foto fundus/foto fundus angiografi (FFA)�USG
11) Specular endotel
c. Tindakan Unggulan :
1) Laser glaukoma dan laser retina
3) Bedah plastik dan rekonstruksi mata
4) Bedah refraktif dan phakoemulsifikasi
6) Bedah orbita
d. Pelayanan Penunjang :
1) Farmasi rumah sakit
3) Protesa mata

Bebarapa hal yang dapat disimpulkan dari penelitian ini adalah:
1. Faktor-faktor penyebab tidak menggunakan tracer di bagian
penyimpanan berkas rekam medis RS Mata �Dr. Yap� Yogyakarta:
a. Sumber Daya Manusia
Petugas filing tergesa-gesa dalam melakukan kegiatan pengambilan
berkas rekam medis.
b. Sarana di bagian penyimpanan
Kondisi rak penyimpanan berkas rekam medis yang sudah penuh
sesak. Sehingga petugas akan mengalami kesulitan apabila akan
memasukkan tracer.
c. Protap
Terdapat prosedur tentang penyimpanan dan pengambilan berkas
rekam medis yang tidak dijalankan oleh petugas dalam hal
penggunaan tracer
2. Dampak tidak menggunakan tracer di bagian penyimpanan berkas
rekam medis RS Mata �Dr. Yap� Yogyakarta:
a. Misfile
Petugas kadang salah dalam memasukkan berkas rekam medis ke
dalam rak penyimpanan karena tidak menggunakan tracer, sebab
petugas harus menentukan urutan berkas rekam medis dengan
sangat teliti.
b. Berkas rekam medis sulit terlacak
Bila ada permintaan berkas rekam medis pasien lama dan berkas
rekam medisnya tidak ada di rak penyimpanan, maka petugas akan
kesulitan dalam melacak berkas rekam medis tersebut
1. Sebaiknya petugas penyimpanan berkas rekam medis perlu diberikan
sosialisasi penggunaan tracer dalam hal pengambilan dan
penyimpanan berkas rekam medis. Sosialisasi penggunan tracer dapat
dilaksanakan pada waktu rapat yang dipimpin oleh Kepala Instalasi
Rekam Medis dan ditujukan kepada semua petugas rekam medis.
2. Sebaiknya perlu adanya peningkatan sumber daya manusia (SDM)
petugas penyimpanan. Misalnya diikutkan pelatihan rekam medis
terkait penyimpanan atau pelatihan dalam bentuk in house training.

