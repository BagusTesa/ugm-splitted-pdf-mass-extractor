Sistem kebijakan Badan Perencanaan Pembangunan Daerah (BAPPEDA)
untuk menentukan alokasi anggaran pada umumnya bersifat dinamis, artinya sistem
aktualnya ditentukan oleh usulan dari beberapa wilayah yang ada dan disesuaikan
dengan kebutuhan. Pola penyusunan anggaran berdasarkan Peraturan Menteri
Dalam Negeri (Permendagri) Nomor 59 Tahun 2007 tentang Pedoman Pengelolaan
Keuangan Daerah, telah dijelaskan bahwa struktur Belanja Daerah dibagi menjadi
Belanja langsung dan Belanja tidak langsung. Belanja tidak langsung merupakan
belanja yang dianggarkan tidak terkait secara langsung dengan pelaksanaan
program dan kegiatan. Jenis belanja tidak langsung antara lain meliputi: belanja
pegawai, bunga, subsidi, bantuan sosial, belanja bagi hasil, bantuan keuangan dan
belanja tidak terduga. Sedangkan belanja langsung merupakan jenis belanja yang
sengaja dianggarkan dan terkait secara langsung dengan pelaksanaan program dan
kegiatan. Jenis belanja langsung, antara lain meliputi: belanja pegawai, belanja
barang dan jasa, serta belanja modal menurut prioritas pembangunan jangka
menengah yang telah ditentukan dari analisis kondisi daerah dan kesesuaiannya
dengan program pembangunan jangka menengah nasional serta pembangunan
jangka panjang nasional.
Untuk pemenuhan kebutuhan dua belanja tersebut, BAPPEDA menyusun
Rencana Kerja Pemerintah Daerah (RKPD) yang merupakan perencanaan tahunan
pemerintah daerah yang termasuk dalam rencana jangka pendek sebagai penjabaran
rencana jangka panjang dan menengah. Rencana jangka panjang memuat sekuen-
sekuen rencana jangka menengah, sedangkan rencana jangka menengah memuat
sekuen-sekuen rencana jangka pendek. Dengan kata lain apa yang terdapat dalam
perencanaan yang lebih pendek jangka waktunya merupakan penjabaran dari
perencanaan yang lebih panjang jangka waktunya.
Mekanisme penyusunan RKPD diawali dari identifikasi potensi dan
permasalahan, Visi dan Misi daerah, rancangan ekonomi daerah, tema
pembangunan, prioritas pembangunan, sasaran pembangunan sebagai referensi
penyusunan program/kegiatan, dan alokasi dana baik yang bersumber dari
Anggaran Pendapatan dan Belanja Daerah (APBD) Kabupaten, APBD Propinsi,
maupun Anggaran Pendapatan dan Belanja Negara (APBN) yang ditempuh dengan
mendorong partisipasi masyarakat.
Dalam pelaksanaannya, alokasi dana yang ditentukan BAPPEDA untuk
program pembangunan, tidak bisa dilepaskan dari tujuan pembangunan nasional,
yaitu mensejahterakan masyarakat dengan meningkatkan Indeks Pembangunan
Manusia (IPM). Tercapainya tujuan pembangunan manusia yang tercermin pada
Indeks Pembangunan Manusia (IPM) sangat tergantung komitmen pemerintah
sebagai penyedia sarana penunjang. Pembangunan tiga aspek yang menjadi fokus
perhatian dalam penghitungan IPM tidak dapat berdiri sendiri dan membutuhkan
sinergi di antara ketiganya. Peran pemerintah sebagai penyusun kebijakan sangat
dibutuhkan untuk memberi kesempatan bagi seluruh lapisan masyarakat untuk
memperbaiki kualitas hidup melalui keterlibatan masyarakat dalam pembangunan
(Anand, 1993). Pentingnya peran tersebut tidak terlepas dari tiga fungsi pemerintah,
yaitu memelihara keamanan dan pertahanan dalam negeri, menyelenggarakan
peradilan, dan menyediakan barang-barang yang tidak mampu disediakan oleh
pihak swasta, seperti misalnya jalan, dam, dan sarana publik lainnya (Azril, 2000).
Salah satu perangkat yang selama ini banyak digunakan oleh pemerintah untuk
mewujudkan peran tersebut adalah perangkat kebijakan fiskal. Di antara instrument
kebijakan fiskal tersebut, ada instrumen dalam bidang pengalokasian dana atau
anggaran pembangunan ke bidang yang berkaitan dengan fasilitas publik seperti
pendidikan, kesehatan, irigasi, transportasi, dan sebagainya (Sasana, 2009).
Kaitan antara pengeluaran untuk sektor publik terhadap pembangunan manusia
sebenarnya mudah untuk ditelusuri. Pengeluaran untuk bidang kesehatan
diharapkan mampu meningkatkan angka harapan hidup maupun menurunkan angka
kematian ibu hamil dan bayi sebagai salah satu komponen dalam penentuan indeks
pembangunan manusia. Anggaran dalam bidang pendidikan akan meningkatkan
akses masyarakat pada pendidikan yang baik dan murah, sehingga mampu
meningkatkan angka melek huruf. Anggaran dalam bidang infrastruktur diharapkan
mampu meningkatkan akses masyarakat dalam bidang ekonomi sehingga akan
terjadi efisiensi dan pada gilirannya akan meningkatkan konsumsi riil per kapita
(Anonim, 2012). Mencermati alokasi pengeluaran pemerintah terhadap akses
publik terjadi kondisi yang cukup memprihatinkan. Hal tersebut tampak dari masih
relatif tingginya alokasi anggaran belanja rutin dibanding anggaran belanja
pembangunan, baik dalam skala nasional maupun regional yang belum dapat
menjawab tingkat kesejahteraan masyarakat.
Hal yang tersulit dalam penentuan alokasi dana anggaran di Kabupaten
Sleman adalah upaya menghilangkan faktor subyektifitas dari pengambil
keputusan. Subyektifitas tersebut terjadi karena tidak adanya metode standar yang
sistematis. Selama ini penentuan anggaran dilakukan oleh pihak anggaran
berdasarkan ketersediaan anggaran dan kebutuhan masyarakat. Hal ini banyak
mempunyai kekurangan antara lain dalam hal efektifitas dan efisiensi waktu serta
perkiraan capaian angka indeks pembangunan manusia yang nanti akan dicapai.
Oleh karena itu dalam penentuan rekomendasi anggaran di Kabupaten Sleman
dibutuhkan sebuah sistem yang dapat mendukung pengambilan keputusan.
Model sistem pendukung keputusan untuk penentuan prediksi indeks
pembangunan manusia dan rekomendasi alokasi dana anggaran ini menggunakan
Fuzzy Takagi Sugeno dengan mengaplikasikan persamaan regresi untuk melakukan
perhitungan hasil outputnya. Regresi adalah suatu metode yang dapat
menggambarkan hubungan antara anggaran dengan indeks pembangunan manusia.
Sedangkan metode fuzzy Takagi Sugeno digunakan untuk mengolah input alokasi
anggaran berupa linguistik. Dengan metode ini, diharapkan rekomendasi akan lebih
tepat karena didasarkan pada nilai kriteria dan bobot yang sudah ditentukan.
Berdasarkan pada uraian latar belakang, maka rumusan masalah yang akan
dilaksanakan pada penelitian ini adalah bagaimana membangun perangkat lunak
yang dapat memberi solusi prediksi capaian indeks pembangunan dan rekomendasi
dana anggaran pembangunan menggunakan model fuzzy TSK di Bappeda
a. Sistem fuzzy yang dibangun hanya memberi rekomendasi alokasi dana
anggaran yang di usulkan dalam program pembangunan dengan capaian
indeks pembangunan manusia.
b. Dinas pembangunan sebagai parameternya adalah dinas pembangunan
pendidikan kepemudaan dan olah raga,  kesehatan, tenaga kerja dan sosial,
pertanian dan ketahanan pangan dan pekerjaan umum.
c. Data alokasi dana anggaran yang digunakan adalah data anggaran dari tahun
penganggaran 2001 sampai dengan tahun 2012.
Tujuan dari penelitian ini adalah menerapkan metode Takagi Sugeno Kang
(TSK) sebagai salah satu metode fuzzy untuk menentukan prediksi capaian indeks
pembangunan manusia dan rekomendasi alokasi dana anggaran dinas
pembangunan.
Sistem ini dapat membantu perencana dan pelaksana anggaran dengan
memberikan output capaian angka indeks pembangunan manusia. Beberapa
manfaat yang dapat diperoleh antara lain :
Memberi rekomendasi alokasi dana anggaran kepada dinas pembangunan yang
terkait, tercermin pada prioritas program kegiatan pembangunan untuk dianggarkan
yang dapat mengurangi faktor subyektifitas dari pengambil keputusan dalam
penentuan alokasi anggaran pembangunan daerah di Kabupaten Sleman, Daerah
Untuk mengetahui adanya keterkaitan antara pengeluaran pemerintah pada
bidang sektor publik dalam APBD dengan capaian angka indeks pembangunan
manusia.
Sepanjang sepengetahuan dan pengamatan penulis, implementasi metode
fuzzy-Takagi Sugeno menghitung prediksi indeks pembangunan manusia dengan
memberikan solusi berupa rekomendasi usulan dana anggaran kegiatan
pembangunan belum pernah dipublikasikan.
Penelitian ini menggunakan metode pengembangan perangkat lunak yang
terdiri dari tahap-tahap berikut ini :
a. Observasi awal: tahap untuk mengumpulkan data awal dan seiring dengan
penelitian yang dilakukan, data yang diobservasikan akan terus ditambah.
b. Analisa Kasus: tahap untuk menganalisa kasus yang diperoleh dari observasi.
c. Perancangan: tahap untuk melakukan perancangan terhadap representasi
kasus. Sistem yang akan dibangun melibatkan data kasus sehingga
memerlukan penyimpanan dengan menggunakan database.
d. Implementasi: tahap untuk mengimplementasikan hasil rancangan menjadi
perangkat lunak (software).
e. Pengujian: tahap untuk melakukan uji coba dari perangkat lunak yang
dibangun.
f. Evaluasi: tahap pengujian yang akan dijadikan sebagai dasar untuk
mengevaluasi dan perbaikan-perbaikan yang diperlukan untuk
menyempurnakan sistem seperti yang diharapkan.
g. Hasil dan penyusunan laporan: tahap untuk memberikan hasil dan laporan
penelitian.
Penulisan tesis ini akan dibuat dalam bentuk Sistematika Penulisan sebagai
berikut :
Bab ini berisikan latar belakang permasalahan, perumusan masalah
dalam penelitian, batasan masalah, keaslian penelitian, tujuan
penelitian, manfaat penelitian, metode penelitian dan sistematika
penelitian.
Bab ini berisikan mengenai uraian sitematis informasi hasil penelitian
yang disajikan dalam pustaka dan menghubungkannya dengan masalah
penelitian yang sedang diteliti.
Bab ini berisikan teori-teori dan referensi-referensi serta pengertian
dasar yang berkaitan dengan topic penelitian dan analisa data dalam
penyusunan tesis yang berhubungan dengan judul yang dipilih.
Bab yang berisikan desain untuk membangun sebuah system aplikasi
yang diusulkan.
Bab untuk memaparkan langkah-langkah implementasi pengembangan
aplikasi penentuan alokasi anggaran kegiatan pembangunan daerah
dengan metode fuzzy Takagi-Sugeno-Kang.
Bab ini membahas system yang telah dibangun dan menganalisa
validitas sistem.
Bab berisikan kesimpulan dari keseluruhan implementasi system yang
dibangun dan saran untuk pengembangan system selanjutnya.

Berdasarkan penelitian yang telah dilakukan untuk merancang,
mengimplementasikan, serta hasil running program yang diujikan dengan data-data
yang berbeda dapat disimpulkan bahwa :
a. Telah dibangun perangkat lunak yang dapat memberi solusi rekomendasi dana
anggaran pembangunan menggunakan metode fuzzy Takagi Sugeno yang
berpengaruh pada capaian indeks pembangunan manusia.
b. Rekomendasi berupa alokasi dana anggaran dan prosentase anggaran, apabila
input dana anggaran setiap dinas pembangunan berupa linguistik. Namun input
numerik hanya menampilkan kelayakan penganggaran dari data dana anggaran
yang diinputkan.
c. Pengujian penilaian bobot preferensi pada tahun penganggaran 2013,
diprioritaskan pada pembangunan dinas pendidikan dan kesehatan, dengan
bobot preferensi 40 untuk pendidikan dan 30 untuk kesehatan didapat capaian
prediksi indeks pembangunan dengan capaian yang bagus dibandingkan
dengan memprioritaskan pembangunan dinas lainnya. Didapat hubungan yang
saling mempengaruhi antara tema pembangunan dengan capaian prediksi IPM.
d. Perbedaan nilai input linguistik sangat berpengaruh pada output capaian
prediksi angka indeks pembangunan manusia, namun untuk perubahan nilai
yang kecil pada input numerik tidak berpengaruh terhadap hasil akhir.
e. Sistem telah diuji dan menunjukkan bahwa sistem bekerja dengan baik dan
dapat menghasilkan rekomendasi dana anggaran pembangunan serta capaian
prediksi angka indeks pembangunan manusia.
Rancang bangun sistem fuzzy yang dibangun merupakan alat bantu atau salah
satu alternatif yang dapat membantu Bappeda Kabupaten Sleman dalam
merekomendasikan alokasi anggaran di masing-masing program kegiatan SKPD
yang berpengaruh terhadap capaian prediksi angka Indeks Pemabangunan Manusia.
Bagi peneliti yang ingin mengembangkan sistem fuzzy penentuan
rekomendasi alokasi anggaran yang berpengaruh terhadap angka Indeks
Pembangunan Manusia ini dapat mengembangkannya dengan menambahkan
variabel yang lebih banyak sesuai dengan SKPD yang terdapat di suatu Kabupaten.
Sehingga diharapkan hasil akhir yang diperoleh semakin akurat.
Pengembangan selanjutnya dari sistem fuzzy ini diharapkan dapat
menggunakan pemodelan yang berbeda dengan menggunakan metode fuzzy yang
lainnya, dengan harapan dapat membandingkan metode fuzzy TSK dengan metode
fuzzy yang lainnya sebagai pembanding hasil akhir yang baik untuk output sistem.
Pengembang dapat menggunakan software komputasi dan database yang lebih baik
atau dengan berbasis web.

