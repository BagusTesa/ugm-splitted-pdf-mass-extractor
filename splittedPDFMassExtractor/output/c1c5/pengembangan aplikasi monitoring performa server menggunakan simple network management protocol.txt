Server memiliki peran dalam mengatur atau menjadi koordinator pada sistem
jaringan. Sebuah server harus mampu selalu tersedia (available) melayani
user/client secara terus menerus, sehingga stabilitas performa server harus tetap
terjaga agar selalu dalam kondisi optimal. Namun server tidaklah selalu bekerja
secara optimal tanpa ada gangguan. Gangguan terhadap hardware maupun software
kerap terjadi sehingga mengganggu fungsionalitas dan stabilitas pada server.
Masalah yang sering ditemukan ditandai dengan gejala yang berbeda-beda, seperti
PC host server mengalami error yang diakibatkan dari penggunaan beban CPU
yang terlalu berat, atau mesin server mengalami down yang menyebabkan seluruh
komputer dalam jaringan tidak dapat mengakses server tersebut. Masalah tersebut
sebenarnya bisa diatasi dengan memonitor kinerja server secara terus menerus.
Namun tentu saja pekerjaan monitoring ini harus secara otomatis, sehingga
administrator tidak perlu harus selalu berada di depan komputer. Maka untuk
menunjang stabilitas performa server perlu di lakukan monitoring secara real time
terhadap performa server. Beberapa riset terkait pengembangan sistem monitoring
performa server telah banyak dilakukan seperti Sugianto (2005), Hamid (2007) dan
Keberadaan seorang administrator sangat dibutuhkan untuk melakukan
monitoring pada server agar selalu bekerja secara optimal. Dalam hal monitoring
server, tentunya seorang administrator tidak bekerja secara 24 jam untuk
melakukan monitor pada server, sehingga membutuhkan layanan untuk mengolah
catatan performa server secara otomatis yang dapat diakses kapan saja dan di mana
saja.
Monitoring server merupakan bagian dari manajemen jaringan, untuk
menjalankan aktifitas monitoring tersebut, antara manajer dan elemen-elemen
jaringan yang dimonitor haruslah ada komunikasi. Ada dua arah komunikasi yaitu
yang pertama, manajer yang bertanya kepada client, dan yang kedua adalah client
yang memberitahu manajer. Simple Network Management Protocol merupakan
protocol yang menangani manajemen jaringan dan komonikasi antar manajer dan
client, sehingga proses komunikasi antar manajer dan client dapat berjalan dengan
lancar dan terjadi secara realtime. Administrator hanya perlu menginstall SNMP
(Simple Network Management Protocol) satu kali pada server yang ingin dimonitor
sehingga dapat mengurangi beban kerja seorang admnistrator.
Layanan aplikasi monitoring server dan SNMP sebagai manajemen jaringan
merupakan solusi yang tepat guna memecahkan permasalahan monitoring server
yang, realtime, dapat diakses dimana saja dan kapan saja. Tersedianya sebuah server
layanan berfungsi sebagai manajer dalam monitoring performa server user, dan
protocol membantu membuat proses monitoring menjadi semakin mudah. Aplikasi
ini akan memberikan informasi kondisi performa server secara otomatis dan
realtime sehingga membantu kinerja administrator dalam hal monitoring server.
Berdasarkan latar belakang yang telah dibahas sebelumnya, maka dapat
dirumuskan sebuah permasalahan utama, yaitu:
1. Bagaimana membangun sebuah aplikasi monitoring yang menggunakan
protocol management jaringan SNMP dalam hal komunikasi pengambilan
data hasil monitor antara manajer dan client.
2. Bagaimana membangun sebuah layanan aplikasi yang dapat mengolah dan
mengirimkan data hasil monitor secara real time dan dapat diakses secara
lagsung.
Adapun batasan masalah dalam penelitian ini adalah :
1. Penelitian ini menitik beratkan proses monitoring server menggunakan
SNMPv3 dengan otentikasi pada proses pengambilan data hasil
monitoring secara real time.
2. Obyek yang dimonitor oleh aplikasi ini adalah penggunaan harddisk,
penggunaan memory, beban cpu, penggunaan cpu user, dan penggunaan
cpu system.
Tujuan dari penelitian ini adalah membuat sebuah layanan aplikasi
monitoring performa server yang real time dengan menggunakan Simple Network
Beberapa manfaat yang diharapkan dari penelitian ini adalah :
1. Membantu administrator jaringan untuk memonitoring performa server
secara real time.
2. Membuat sebuah layanan aplikasi yang menutup kekurangan kekurangan
dari beberapa aplikasi serupa.
Metode yang digunakan dalam penelitian dan penulisan tugas akhir ini
sebagai berikut :
1. Pengumpulan data, informasi dan teori-teori mengenai implementasi
SNMPv3 dalam hal monitoring performa server, yang diperoleh melalui
bahan-bahan kepustakaan sebagai data referensi atau dari buku-buku,
artikel, jurnal, karya tulis ilmiah, dan internet yang berhubungan dengan
obyek penelitian.
2. Analisis kebutuhan dan pengumpulan informasi yang dibutuhkan.
3. Perancangan aplikasi yang meliputi library dan framework yang digunakan,
rancangan proses, rancangan user interfaces, rancangan basis data, dan
rancangan pengucjian.
4. Mengimplementasikan rancangan yang telah dibuat sebelumnya ke dalam
sistem aplikasi secara nyata yang kemudian siap untuk diujicoba.
5. Pengujian terhadap aplikasi.
6. Mendokumentasikan aplikasi, yaitu dengan pencatatan hasil pengujian dan
pembuatan laporan akhir.
Sistematika penulisan laporan yang digunakan dalam ini penelitian ini adalah
sebagai berikut :
Bab ini berisikan latar belakang, perumusan masalah, batasan masalah,
tujuan penelitian, manfaat penelitian, metode penelitian dan sistematika
penulisan laporan tugas akhir.
Pada bab ini akan dipaparkan penelitian-penelitian sebelumnya yang
berkaitan dengan penelitian ini.
Bab ini menjelaskan berbagai macam teori dan konsep yang mendasari dan
mendukung penelitian ini.
Bab ini menjelaskan analisis dan rancangan sistem yang akan dibuat.
Bab ini berisi pembahasan mengenai implementasi sistem berdasarkan
analisis dan perancangan yang telah dibuat sebelumnya.
Dalam bab ini membahas hasil penelitian dan hasil yang diperoleh dari
implementasi rancangan yang telah dibuat.
Pada bab ini ditarik kesimpulan dan saran-saran untuk penelitian
selanjutnya yang serupa.

Berdasarkan hasil perancangan, implementasi dan pengujian terhadap
aplikasi dan sistem yang dibangun dapat diambil kesimpulan sebagai berikut :
1. Telah berhasil dibangun sebuah aplikasi Server Monitoring menggunakan
Simple Management Network Protocol yang dapat memonitor server secara
real time dan mengolah sekaligus menampilkan data hasil monitor ke dalam
bentuk grafik.
2. Aplikasi yang dijalankan ini dapat menambah server baru yang didaftarkan
oleh pengguna aplikasi.
3. Kemanan trasportasi data yang menggunakan otentikasi pada SNMPv3 dapat
diimplementasikan dengan baik, pihak luar yang ingin masuk kedalam sistem
tidak dapat mengakses apabila tidak mengetahui kunci otentikasi tersebut.
4. Pengguna dapat memonitor server yang telah didaftarkan melalui sebuah
grafik yang terus menampilkan hasil monitor secara real time.
Saran yang dapat diberikan untuk penelitian selanjutnya adalah sebagai
berikut :
1. Fitur proses monitoring dapat dikembangkan lagi dengan cara membuat
virtual machine sesuai dengan user, atau satu virtual machine untuk satu user.
2. Adanya pengembangan untuk melewati proses monitor apabila server yang
terdaftar mengalami gangguan, atau belum terinstall SNMP.

