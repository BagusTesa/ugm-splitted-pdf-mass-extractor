Perkembangan dunia saat ini dipengaruhi oleh perkembangan Teknologi
Informasi (TI) , yang memungkinkan terjadinya perpindahan data informasi
dengan cepat. Hal ini menuntut setiap instansi , organisasi maupun institusi untuk
terus mengikuti perkembangan Teknologi Informasi . Salah satu Teknologi
Informasi yang berkembang saat ini adalah Sistem Informasi.
Sistem Informasi merupakan sistem yang dapat mempermudah pihak
organisasi maupun instansi dalam mengolah data-data dan menginformasikannya
dalam bentuk digital kepada klien atau pengguna. Ada banyak jenis dari sistem
informasi , salah satunya adalah sistem informasi berbasis SMS gateway .
SMS gateway merupakan suatu sistem aplikasi yang digunakan untuk
mengirim dan menerima SMS. Dimana pihak instansi atau organisasi  yang
menerapkan sistem ini , dapat menyebarkan informasi kepada ratusan klien atau
banyak pengguna tanpa harus mengetik ratusan atau ribuan nomor, karena nomor-
nomor tersebut sudah tersimpan dan diambil dari suatu database SMS
gatewaySaat ini sistem informasi berbasis  SMS gateway banyak digunakan oleh
instansi, organisasi maupun institusi. Sebagai contoh sistem informasi
kepegawaian dalam suatu instansi. Dimana sistem tersebut menyampaikan suatu
informasi kepada pegawainya melalui smsgateway, selain contoh tersebut juga
masih ada contoh lain, salah satunya adalahsistem poling sms dalam ajang
pencarian bakat di televisi.
Walaupun banyak Instansi yang sudah menggunakansistem informasi
berbasis SMS gateway, namun masih ada juga instansi-instansi  yang belum
menggunakan sistem ini, salah satunya adalah instansi sekolah yaitu SMA Negeri
SMA Negeri Jumapolo , merupakan sebuah instansi pendidikan yang
terletak di daerah Surakarta. Tepatnya yaitu di kecamatan jumapolo kabupaten
karanganyar. Sekolah ini cukup berkembang tiap tahunya, untuk tahun ajaran
2011/2012 SMA Negeri Jumapolo menerima murid sejumlah sekitar 280 anak,
untuk tingkatan siswa baru yang menduduki sebagai kelas X. Sedangkan untuk
kelas XI ada sekitar 260 anak, dan kelas XII sekitar 240 anak. Untuk setiap
tingkatan kelas dikelompokan menjadi delapan kelas. Namun untuk kelas XI dan
XII dikelompokan menjadi dua jurusan sesuai minat dan bakat. Yaitu empat kelas
untuk jurusan IPA dan empat kelas untuk jurusan IPS.
Pada sekolah ini masih banyak proses pengolahan data yang dilakukan
secara manual. Cara Manual yaitu masih dicatat diatas kertas, belum ada
mediakomputer yang mem-backup nya. Pada sekolah teersebut belum tersedia
sistemkomputer yang mengelolanya data-data seperti : pencatatan ketidakhadiran
siswa, pencatatan nilai � nilai siswa, pencatatan data konseling  siswa,
sertapencatatan pembayaran SPP . Sehingga pencatatan dan penyimpanan data-
data tersebut tidak efisien dan rawan terjadinya kerusakan dan kehilangan data.
Selain masalah tersebut juga ada masalah lagi yangpaling utama yaitu
penyampaian informasi kepada orang tua siswa yang masih dilakukan dengan cara
konvensional. Konvensional yaitu masih menggunakan metode surat(kertas) yang
dititipkan kepada siswa.Hal tersebut selain membutuhkan waktu yang lama, juga
mengindikasitidak sampainya surat tersebut, dan dapat menghambat kualitas
belajar siswa karena tidak setiap hari dipantau oleh orangtuanya.
Berdasarkan masalah-masalah diatas. Sekolah tersebut membutuhkan suatu
sistem informasi berbasis SMS gateway, penulis beri judul yaitu Sistem Informasi
Pencatatan dan Penyampaian Informasi Hasil Belajar Siswa dan Kegiatan
Sekolah. Hasil Belajar siswa disini meliputi pencatatan dan penyampaian melalui
sms diantaranya data nilai tugas, nilai harian, data konseling siswa, data
ketidakhadiran siswa dan data pembayaran SPP. Sementara untuk  kegiatan atau
acara sekolah, meliputi agenda sekolah dan kalender akademik. Agenda sekolah
merupakan kegiatan  sekolah yang belum terjadwal, artinya pihak sekolah dapat
membuat dan mengirim agenda tersebut setiap waktu. Sedangkan untuk kalender
akademik merupakan kegiatan sekolah yang sudah terjadwal, artinya sistem akan
mengirim informasi kalender akademik kepada orang tua secara otomatis, sejak
kalender tersebut dibuat dan tersimpan di dalam database, pada sistem ini
kalender akademik akan mengirim informasi kepada orang tua lima hari sebelum
hari pelaksanaan kalender akademik.
Berdasarkan latar belakang di atas ,maka masalah yang akan dikaji dalam
sistem ini meliputi,  bagaimana membangun sistem informasi berbasis web yang
mencatat dan menyimpan data-data hasil belajar siswadan kegiatan sekolah, yang
dilengkapi dengan fasilitas sms gateway.
Batasan masalah dalam sistem ini antara lain :
1. Sistemhanya dapat mengirim sms kepada orangtua.
2. Pembuatan aplikasi ini berbasis web , menggunakan framework codeigniter,
dan gammu sebagai server SMS gatewaynya
3. Aplikasi ini akan terhubung dengan jaringan internet.
4. Informasi sms hanya untuk nilai harian, nilai tugas, ketidakhadiran siswa,
pembayaran spp, pencatatan konseling dan acara sekolahyang berupa
agenda sekolah dan kalender akademik.
5. Sistem ini hanya mencetak laporan yang memuat tentang  nilai harian,nilai
tugas,nilai akhir semester, ketidak hadiran siswa, catatan konseling dan
pembayaran SPP.
Tujuan penelitian ini adalah membuat sistem informasi berbasis web yang
dilengkapi dengan fasilitas sms gateway  yang mampu menyimpan , mencetak
laporan dan mengirim informasi terkait nilai harian, nilai tugas, ketidakhadiran
siswa,catatan konseling dan pembayaran SPP melalui sms.
Manfaat dari penelitian tugas akhir ini antara lain :
1. Membantu pihak sekolah, terkait pencatatan dan penyimpanan data
hasilbelajar siswa dan kegiatan sekolah yang selama ini masih
menggunakan cara manual dan konvensional.
2. Menghasilkan aplikasi yang dapat mencatat, menyimpan dan mengirim via
sms, terkaithasil belajarsiswa dan kegiatan sekolah .
3. Fasilitas sms gateway secara tidak langsung dapat membantu pihak sekolah
dalam meningkatkan kualitas belajar siswa, yaitu melalui pesan sms yang
dikirim dari pihak sekolah kepada orang tua secara rutin.
4. Menghasilkan aplikasi yang bisa mencetak laporan terkait nilai harian, nilai
tugas, nilai akhir semester, ketidakhadiran siswa, catatan konseling, serta
informasi pembayaran SPP.
5. Menyampaikan informasi melalui sms terkait acara yang berupa agenda dan
kalender akademik sekolah kepada orang tua siswa.
Metode Penelitian  yang digunakan dalam penelitian ini adalah sebagai
berikut:
Metode yang digunakan dalam pembuatan laporan dan pembuatan aplikasi
ini adalah sebagai berikut :
a. Wawancara
Metode wawancara dilakukan kepada Waka Kurikulum SMA Negeri
Jumapolo. Metode ini dilakukan dengan tujuan mendapatkan informasi
mengenai sistempencatatan ,penyimpanan serta pengiriman informasi
mengenai data hasil belajar siswa dan kegiatan sekolah, yang ternyata
masih dilakukan dengan cara manual dan konvensional. Informasi yang
didapat dari metode ini akan dianalisis dan membantu dalam menetukan
kebutuhan-kebutuhan sistemyang diinginkan dari pihak SMA Negeri
b. Observasi
Metode Observasi dilakukan dengan cara melakukan peninjauan dan
pengamatan langsung ke lokasi, untuk mengambil data dan informasi yang
ada di SMA Negeri Jumapolo. Data dan informasi yang diperoleh akan
digunakan untuk menyusun Tugas Akhir ini. Hal-hal yang diamati antara
lain prosedur pencatatan ketidakhadiran siswa ,penilaian, pembayaran
SPP, dan pencatatn konseling siswa. Serta prosedur penyampaian
informasi kepada orang tua terkait acara dan hasil belajar siswa di sekolah.
c. Studi Literatur
Studi Literatur dilakukan dengan cara mengumpulkan informasi-
informasi , baik yang berupa buku-buku, situs internet ,catatan kuliah dan
modul praktikum yang erat kaitanya dengan tema penelitian Tugas Akhir
ini.
Proses Analisis sistem dilakukan dengan menganalisis hal-hal yang
diperlukan terkait proses penyimpanan data dan penyampaian informasi di SMA
Negeri Jumapolo. Analisis yang dilakukan pada proses pembuatan Tugas Akhir
ini antara lain analisis kebutuhan sistem, analisis kebutuhan data, analisis
kebutuhan fungsionalitas dan analisis kebutuhan non fungsionalitas.
Perancangan sistem merupakan proses penerjemahan kebutuhan sistem
dalam bentuk desain database, Diagram Alir Data (DAD), Entity Relation
Diagram(ERD), serta pembuatan  struktur menu  dan desain rancangan
antarmuka sistem.
Tahap Implementasi sistem  merupakan proses penerjemahan desain
perancangan kedalam bentuk pemograman. Bahasa pemrograman yang
digunakan dalam pembuatan sistem ini adalah PHP dengan Framework
Codeigniter. Selain itu untuk manajemen basis datanya menggunakan Mysql ,
sedangkan untuk membangun SMS Gatewaynya menggunakan gammu.
Pengujian sistem merupakan testing terhadap sistem yang telah dibangun.
Sistematika penulisan dalam laporan ini dibagi menjadi beberapa bab ,
dengan pokok pembahasan sebagai berikut :
Bab ini berisi latar belakang masalah, rumusan masalah, batasan
masalah, tujuan penelitian, manfaat penelitian, metode penelitian
dan sistematika penulisan.
Bab ini berisi uraian sistematis hasil penelitian yang disajikan
dalam pustaka dan menghubungkanya dengan masalah yang akan
diteliti dalam penelitian tugas akhir ini.
Bab ini berisi tentang penjelasan mengenai teori-teori ilmu yang
menjadi landasan dalam penelitian yang sedang dilakuakan.
Bab ini memaparkan tentang analisis kebutuhan sistem yang
digunakan untuk mendefinisikan hal-hal dalam pengembangan
sistem . Analisis kebutuhan tersebut meliputi analisis  sistem ,
rancangan proses , rancangan basis data serta rancangan antar
muka pengguna.
Bab ini menguraikan tentang bab sebelumnya yaitu analisis dan
perancangan sistem yang dibuat pada pembangunansistem dengan
menampilkan antarmuka dan potongan kode yang disertai dengan
penjelasan terkait cara kerja sistem .
Bab ini menguraikan tentang pengujian sistem yang telah
dikerjakan.
Bab ini menyajikan kesimpulan dan saran dari laporan tugas akhir
yang telah dikerjakan. Saran diperlukan sebagai upaya untuk
pengembangan sistem berikutnya.

Berdasarkan pembahasan mengenai Sistem Informasi Pencatatan dan
Penyampaian Hasil Belajar Siswa dan Kegiatan Sekolah dengan Fasilitas SMS
Gateway yang telah diimplementasikan, maka dapat diambil beberapa kesimpulan
antara lain sebagai berikut :
1. Sistem informasi pencatatan dan penyampaian hasil belajar siswa dan
kegiatan sekolah berhasil dibangun, dan diharapkan mampu member
kemudahan bagi pihak SMA Negeri Jumapolo dalam hal pencatatan dan
penyampaian hasil belajar siswa dan kegiatan sekolah .
2. Sistem ini dapat menyampaikan informasi terkait nilai harian, nilai
tugas, ketidakhadiran siswa, catatan konseling dan pembayaran SPP
melaui sms.
3. Sistem ini dapat mengeluarkan output berupa laporan nilai harian siswa,
nilai tugas, catatan konseling, ketidak hadiran siswa,nilai akhir semester
dan pembayaran SPP.
Untuk melakukan pengembangan pada sistem informasi penacatatan dan
penyampaian hasil belajar siswa dan kegiatan sekolah berbasis SMS Gateway ,
penulis memberikan beberapa saran diantaranya:
1. Perbaikan user interface.
2. Menerapkan sistem SMS request dengan keyword tertentu, sehingga
menghasilkan output berupa SMS respon dari sistem tersebut.
3. Menambah fitur pesan yang disampaikan melalui sms, seperti jadwal
mata pelajaran.
4. Menerapkan sistem SMS masking.

