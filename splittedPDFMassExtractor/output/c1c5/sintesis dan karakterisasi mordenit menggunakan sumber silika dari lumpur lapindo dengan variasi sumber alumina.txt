Semburan lumpur panas di lokasi pengeboran PT. Lapindo Brantas pada
tanggal 29 Mei 2006 telah menimbulkan dampak kerugian luar biasa dan
diperkirakan akan berakhir 31 tahun yang akan datang. Laju semburan lumpur
Lapindo pernah mencapai 148.000 m3/hari pada akhir 2006. Volume luapan
lumpur yang semakin besar menjadi masalah serius sehingga diperlukan usaha-
usaha pemanfaatan lumpur untuk memberikan nilai produktif dan nilai tambah
bagi lumpur Lapindo. Hasil penelitian menunjukkan bahwa lumpur Lapindo
termasuk kategori B3 (Bahan Berbahaya dan Beracun). Material tersebut
mengandung fenol, raksa, hidrogen sulfida, nitrit, logam berat Cd dan Pb yang
melebihi baku mutu limbah cair (Malik dkk., 2009).
Komposisi lumpur Lapindo terdiri dari material lempung, lanau dan
sisipan pasir. Hasil analisis keseragaman butir menunjukkan bahwa komponen
terbesarnya adalah lempung (81,5%). Lempung dengan mineral utama
montmorillonit tersebut mengandung 50% partikel klastik berdiameter kurang dari
0,0625 mm. Mineral-mineral yang terdapat dalam lumpur Lapindo antara lain
montmorillonit, kaolinit, illit, khlorit, kuarsa, kalsedon, opal, feldspar, mika, besi
oksida, glaukonit dan campuran komposit mineral amorf (Malik dkk., 2009).
Pemanfaatan lumpur Lapindo hingga saat ini masih terbatas pada pembuatan
bahan bangunan yang bernilai ekonomi rendah. Hasil penelitian menunjukkan
bahwa kandungan silika pada lumpur Lapindo cukup tinggi (Fadli dkk., 2013).
Oleh karena itu lumpur Lapindo berpeluang besar dijadikan sebagai sumber silika
pada sintesis zeolit.
Silika gel atau silika amorf telah banyak disintesis dari bahan alam dengan
berbagai metode. Metode yang paling konvensional adalah melalui pembakaran
pada temperatur tinggi. Silika amorf yang dihasilkan menggunakan metode ini
masih mengandung banyak pengotor sehingga komposisi silikanya relatif tidak
terlalu tinggi. Oleh karena itu, saat ini telah dikembangkan beberapa metode
untuk menghasilkan silika gel dengan kandungan silika yang lebih tinggi.
Beberapa metode tersebut antara lain aktivasi asam dan pelarutan basa
(Olawale dkk., 2012; Burns dkk., 2006; Okoronkwo dkk., 2013). Hasil-hasil
penelitian tersebut menunjukkan kandungan silika dari silika gel yang disintesis
melalui pelarutan basa lebih besar dari pada hasil aktivasi asam.
Kombinasi menggunakan kedua metode tersebut kemudian dilakukan
untuk mengoptimalkan kandungan silika yang lebih tinggi (Nayak, 2010). Namun
kombinasi perlakuan pada produksi silika tidak efektif digunakan karena
kandungan silika yang telah optimal dari hasil aktivasi asam kemudian menurun
dengan dilakukannya pelarutan basa. Selain itu, pengaruh kedua perlakuan
tersebut juga tidak terlalu besar tehadap perubahan kandungan silika yang
dihasilkan.
Metode baru produksi silika gel kemudian dikembangkan untuk
memperoleh silika yang lebih optimal, yaitu dengan resin (Affandi dkk., 2009).
Meskipun penggunaan resin dapat menghasilkan silika yang paling optimal
namun material tersebut harus selalu dilakukan regenerasi setiap setelah
digunakan. Selain itu, penggunaan yang terus menerus dapat menurunkan
kemampuan pertukaran kationnya sehingga harus diganti secara berkala. Hal
tersebut membuat metode ini bukan menjadi pilihan yang tepat untuk
memproduksi silika gel.
Berdasarkan penelitian-penelitian tersebut maka pada penelitian ini
dilakukan produksi silika menggunakan metode pelarutan dengan NaOH dari
bahan dasar lumpur Lapindo. Sampai saat ini belum banyak yang mempelajari
pengaruh konsentrasi NaOH terhadap jumlah silika yang terlarut dari hasil
pelarutan silika. Oleh karena itu, penelitian ini juga dilakukan pengaruh
konsentrasi NaOH terhadap jumlah silika yang terlarut dari hasil pelarutan silika
dalam lumpur Lapindo. Hasil pelarutan silika yang paling besar akan digunakan
untuk sintesis silika gel sebagai sumber silika pada sintesis zeolit.
Zeolit tipe mordenit memiliki stabilitas terhadap termal dan asam yang
tinggi. Mordenit telah banyak digunakan sebagai katalis pada reaksi alkilasi,
isomerisasi, disproporsionasi, konversi metana menjadi bensin, pereduksi emisi
gas dan fotokatalis limbah organik (Pirajan dkk., 2010; Kostrab dkk., 2006;
Padervand dkk., 2012; Tsai, 2006; Cordoba dkk., 2001; Dimitrova dkk., 2006).
Sintesis mordenit telah dilakukan oleh banyak peneliti dengan berbagai metode,
diantaranya menggunakan templat organik, alkohol maupun dengan penambahan
bibit (Hellring dkk., 1993; Feng dan Balkus, 2003; Mohamed dkk., 2005;
Sano dkk., 2001; Hincapie dkk., 2004; Pirajan dkk., 2010; Mignoni dkk., 2008).
Pengunaan templat organik pada sintesis zeolit bukan pilihan yang tepat
karena harganya yang mahal dan sangat toksik. Hal tersebut disebabkan karena
limbah templat organik dapat mengkontaminasi lingkungan. Ketika mordenit
disintesis menggunakan templat organik maka dibutuhkan biaya tambahan untuk
menghilangkan material toksik tersebut. Selain itu, bahaya kontaminasi
lingkungan juga menjadi semakin tinggi (Kim dkk., 2008). Menurut Jacobs dan
Martens (1987), penggunaan alkohol pada sintesis zeolit juga hanya dapat
mencapai efsiensi 80%. Hal ini dikarenakan selalu dibutuhkannya penambahan
amonia pada sistem sintesis supaya dapat mencapai alkalinitas yang sesuai untuk
sintesis zeolit.
Penggunaan bibit pada sintesis mordenit juga bukan pilihan yang tepat
karena sulitnya untuk memperoleh mordenit komersial. Beberapa peneliti
sebelumnya telah mengembangkan metode sintesis mordenit yang lebih efektif
dan ramah lingkungan dengan menggunakan templat anorganik. Reaktan yang
digunakan untuk sintesis tersebut hanya berasal dari sumber silika, alumina, basa
dan air (Bajpai dkk., 1981; Kim dan Ahn, 1991). Basa anorganik seperti KOH dan
NaOH merupakan templat anorganik yang umum digunakan pada sintesis zeolit
(Fitoussi dan Amir, 1997). Oleh karena itu, sintesis zeolit tipe mordenit pada
penelitian ini dilakukan menggunakan templat anorganik berupa NaOH.
Salah satu faktor yang mempengaruhi proses pembentukan zeolit adalah
keberadaan logam-logam pengotor dalam sistem reaksi. Adanya logam pengotor
tersebut dapat mengganggu proses nukleasi dan kristalisasi yang merupakan tahap
utama dalam sintesis zeolit (Catalfamo dkk., 1994). Zeolit alam, aluminium foil
dan natrium aluminat telah digunakan sebagai sumber alumina dalam sintesis
zeolit (Ozlem dan Onal, 2008; Dey dkk., 2013; Kim dan Ahn, 1991). Ketiga
material tersebut memiliki kandungan pengotor yang berbeda-beda. Oleh karena
itu, pada penelitian ini akan dikaji pengaruh ketiga sumber alumina tersebut
terhadap kualitas zeolit yang dihasilkan.
Tujuan penelitian yang dilakukan kali ini ialah:
1. Mengkaji pengaruh konsentrasi NaOH pada pelarutan silika terhadap
jumlah silika yang terlarut dari lumpur Lapindo,
2. Mengkaji pengaruh jenis sumber alumina pada sintesis zeolit terhadap
kualitas zeolit yang dihasilkan,
Hasil penelitian ini diharapkan dapat memberikan kontribusi dalam
pengembangan ilmu pengetahuan dan teknologi dalam bidang kimia dan
aplikasinya, terutama dalam sintesis zeolit mordenit. Hasil penelitian ini juga
diharapkan dapat membuka wawasan tentang pemanfaatan dan pengolahan
lumpur Lapindo menjadi sesuatu yang lebih bernilai guna.

Berdasarkan penelitian yang telah dilakukan, dapat disimpulkan bahwa:
1. Lumpur Lapindo merupakan material aluminosilikat berupa lempung dengan
kandungan mineral berupa kuarsa, montmorillonit, kaolinit dan illit.
2. Peningkatan konsentrasi NaOH dapat meningkatkan jumlah silika yang terlarut
dari lumpur Lapindo. Kondisi optimal proses pelarutan silika diperoleh pada
konsentrasi NaOH 6 M. Silika gel yang disintesis memiliki kemurnian 98,1 %.
3.  Keberadaan logam pengotor dalam reaktan dapat mengganggu proses nukleasi
dan kristalisasi. Hal tersebut terbukti dengan terjadinya pergeseran jalur
pembentukan produk mordenit menjadi analcim.
4. Zeolit yang disintesis menggunakan zeolit alam dan aluminium foil sebagai
sumber alumina hanya dapat membentuk analcim dengan dan rasio Si/Al =
3,15 dan 2,75.
5. Mordenit bersilika tinggi telah berhasil disintesis menggunakan natrium
aluminat sebagai sumber alumina. Mordenit yang disintesis memiliki rasio
Si/Al 10,6 sehingga termasuk zeolit bersilika tinggi.
Berdasarkan hasil karakterisasi mordenit yang telah diperoleh, dapat
dilakukan penelitian lanjutan untuk mengetahui berbagai faktor yang
mempengaruhi proses sintesis zeolit, seperti pengaruh komposisi reaktan, waktu
aging, waktu hidrotermal, temperatur hidrotermal dan pH. Setiap penurunan dan
peningkatan faktor tersebut dapat menggeser jalur pembentukan mordenit menjadi
fasa lain.

