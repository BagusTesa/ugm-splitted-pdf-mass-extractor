Investasi adalah istilah yang berhubungan dengan keuangan dan ekonomi
dengan harapan mendapatkan keuntungan dimasa depan atau dengan kata lain
investasi adalah mengeluarkan sejumlah uang atau menyimpan uang pada sesuatu
dengan harapan suatu saat mendapat keuntungan financial.
Jaman sekarang investasi menjadi sebuah alternatif yang cukup
menjanjikan untuk memperoleh laba serta dapat menjadi lahan pekerjaan baru
yang menjanjikan, tetapi masyarakat masih sedikit yang berkecimpung dalam
dunia pasar modal karena faktor minimnya pengetahuan tentang pasar modal itu
sendiri, sehingga masyarakat enggan untuk melakukan investasi yang dinilai
berisiko besar. Padahal risiko merupakan bagian dari setiap investasi yang
dilakukan dan risiko berbanding lurus dengan expected return, dimana semakin
besar expected return maka tingkat risikonya pun semakin besar.
Dalam sebuah investasi dipasar modal keinginan mendapatkan keuntungan
menyebabkan munculnya jalan keluar yang digunakan sejak dahulu. Terdapat dua
analisis yang digunakan dalam melakukan pemilihan saham, yaitu analisis
fundamental dan analisis teknikal.  Analisis fundamental adalah metode analisis
yang didasarkan pada fundamental ekonomi suatu perusahaan. Teknis ini menitik
beratkan pada rasio finansial dan kejadian - kejadian yang secara langsung
maupun tidak langsung memengaruhi kinerja keuangan perusahaan dan sebagian
investor banyak yag mengandalkan analisis ini untuk bermain dipasar modal.
Namun dipihak lain sebagian masyarakat beranggapan bahwa dalam memilih
investasi saham, analisis fundamental saja tidaklah cukup dalam mengambil
sebuah keputusan berinvestasi karena analisis Fundamental hanya melihat dari
faktor-faktor fundamental yaitu laporan keuangan perusahaan yang bersangkutan
terhadap harga pasar maka dibutuhkan juga  Analisis teknikal karena Analisis
Fundamental hanya melihat dari faktor-faktor fundamental yaitu laporan
keuangan perusahaan yang bersangkutan terhadap harga pasar. Sebaliknya,
Analisis Teknikal dapat memperhitungkan faktor-faktor psikologis pasar dan
dapat pula menjelaskan kapan waktunya untuk membeli atau menjual. Analisis
Teknikal dapat memperhitungkan terlebih dahulu kejadian/kondisi jangka pendek
yang bersifat sementara dan sangat berpengaruh besar terhadap pasar. Oleh karena
itu, investor tidak mengalami kerugian sebelum kondisi yang digambarkan
menurut analisis fundamental tersebut tercermin di pasar.
Menganalisis pergerakan harga saham dengan menggunakan analisis
teknikal saat ini tidak lagi hanya dengan mengandalkan bentuk�bentuk grafik
harga yang terbentuk (analisis teknikal klasik). Perhitungan terhadap karakteristik
setiap indikator saham juga dianggap penting disajikan dalam bentuk formula
perhitungan dimana setiap indikator tersebut memiliki sifat dan karakteristik
masing-masing dan analisis nilai masa depan selalu menarik untuk mengetahui
kekuatan sebuah saham, terutama dalam menentukan mana pola perdagangan
yang paling tepat.
Dalam analisis teknikal terdapat beberapa indikator yang dapat
mengindikasikan arah pergerakan saham. Untuk menghasilkan analisis teknikal
yang baik terhadap saham tertentu dibutuhkan panjang periode yang optimal
untuk saham tersebut dan juga dibutuhkan perhitungan dan analisis terhadap
indikator yang berguna untuk memprediksi pergerakan harga saham untuk periode
akan datang.
Indikator StochRSI dikembangkan oleh Tushard Chande dan Stanley Kroll
pada tahun 1994. Pada dasarnya indikator StochRSI merupakan pengembangan
lebih lanjut dari indikator RSI dan indikator Stocahstic Oscillator. RSI merupakan
materi dasarnya dan kemudian diolah menggunakan formula Oscillator, dengan
demikian StochRSI merupakan oscillator yang mengukur level RSI relatif
terhadap suatu daerah sebar tertentu dalam suatu periode waktu yang ditentukan
StochRSI muncul dalam sekali 0-1.
Sang pengembang menjelaskan bahwa RSI kadang-kadang difungsikan di
antara level 20 dan 80 untuk periode yang diperpanjang tanpa menyentuh area
jenuh jual dan jenuh beli. Para trader yang melihat waktu masuk berdasarkan
pembacaan jenuh beli atau jenuh jual pada RSI mungkin akan mendapati diri
mereka tetap berada pada pergerakan mendatar.
Tulisan ini dibuat untuk melihat indikator mana yang lebih baik antara
Stohastic Oscillator dan Stochastic RSI dalam analisis keputusan perdagangan
saham. Akan dibahas perhitungan matematis indikator tersebut serta beberapa
bukti empiris keuntungan yang menghasilkan kesimpulan mengapa orang-orang
dapat memilih indikator ini sebagai alat utama perdagangan saham yang mudah.
1.3.     Maksud dan Tujuan
Penyusunan tugas akhir ini dimaksudkan untuk memenuhi syarat kelulusan
program Strata-1 (S1) Program Studi Statistika, Jurusan Matematika, Fakultas
Matematika dan Ilmu Penetahuan Alam (FMIPA), Universitas Gadjah Mada.
Sedangkan tujuan dari penulisan tugas akhir ini adalah sebagai berikut :
1. Mempelajari dan memahami perhitungan matematis indikator teknikal saham
2. Mengindentifikasi area jenuh jual dan jenuh beli yang kemudian dapat
menjadi titik masuk atau keluar pasar.
3. Menentukan sinyal beli dan sinyal jual dalam indikator Stochastic RSI
4. Membantu trader untuk memilih metode yang lebih baik dari beberapa
metode indikator analisis tekhnikal.
Dalam penyusunan tugas akhir ini batasan masalah sangat diperlukan
untuk menjamin keabsahan dalam kesimpulan yang diperoleh. Agar tidak terjadi
penyimpangan dari tujuan semula dan pemecahan masalah lebih terkonsentrasi,
maka pembahasan difokuskan pada bagaimana melakukan analisis teknikal
menggunakan indikator Stochastic RSI (Relative Strength Index) dalam
menghitung besarnya keadaan jenuh beli maupun jenuh jual dan juga menentukan
sinyal jual ataupun sinyal beli.
Selanjutnya indikator Stochastic RSI (Relative Strength Index) akan memberikan
nilai antara 0,00 sampai dengan 1,00 dimana keadaan jenuh jual terjadi jika tren
harga sudah naik dan teridentifikasi dan Stochastic RSI (Relative Strength Index)
bergerak naik dari nilai dibawah 0,20 kenilai diatas 0,20 begitu juga sebaliknya
keadaan jenuh beli terjadi jika tren harga bergerak turun dan Stochastic RSI
(Relative Strength Index)  menurun dari nilai diatas 0,80 kenilai dibawah 0,80.
Analis tehnikal sering menggunakan berbagai indikator yang secara tipikal
merupakan transformasi matematik dari harga atau volume. Indikator ini
digunakan sebagai alat bantu untuk menentukan apakah suatu asset berada dalam
suatu tren serta arah dari harga aset dalam tern tersebut. Para analis juga
mempelajari korelasi antara harga, volume, dan marjin dalamperdagangan
berjangka. Indikator tersebut misalnya indeks kekuatan relatif dan MACD. Studi
lain juga menggunakan korelasi antara perubahan dalam opsi
Secara esensial, analisis teknikal mempelajari dua bidang investasi yautu
analisis dari psikologi pasar dan analisis terhadap suplai dan permintaan. Para
analis berupaya untuk meramalkan pergerakan harga guna memperoleh
keberhasilan dalam perdagangan serta memperkecil risiko kerugiannya serta
menghasilkan imbal hasil positif dalam masa depan melalui cara pengelolaan
risiko dan
dan opsi jual / beli
beserta harganya.
manajemen keuangan
Analisis teknikal pertama kali dikembangkan oleh Homma Munehisa pada
awal abad ke 18 yang menggunakan teknik grafik lilin (candlestick chart) yang
merupakan perangkat analisis yang utama pada saat ini. Ketika ilmu matematika
dan statistika mulai diaplikasikan dalam analisis teknikal, mulailah banyak
diciptakan indikator. Indikator pertama yang mulai digunakan dalam analisis
teknikal adalah indikator Moving Average (MA) oleh Richard Donchian dan J.M
Hurst sebelum masa perang dunia kedua. Sejak itulah indikator teknikal mulai
banyak digunakan.
Indikator StochRSI dikembangkan oleh Tushard Chande dan Stanley Kroll
pada tahun 1994. Pada dasarnya indikator StochRSI merupakan pengembangan
lebih lanjut dari indikator RSI dan indikator Stocahstic Oscillator. RSI merupakan
materi dasarnya dan kemudian diolah menggunakan formula Oscillator, dengan
demikian StochRSI merupakan oscillator yang mengukur level RSI relatif
terhadap suatu daerah sebar tertentu dalam suatu periode waktu yang ditentukan
StochRSI muncul dalam sekali 0-1.
Metode yang digunakan dalam penulisan tugas akhir ini adalah berupa
studi literatur dan dipadukan dengan aplikasi berdasarkan data yang diperoleh
melalui sumber�sumber resmi seperti perpustakaan, buku-buku elektronik, jurnal-
jurnal.
Penulisan data dilakukan secara kronologis, terutama untuk menjabarkan
proses yang terjadi dalam instrumen Stochastic RSI (Relative Strength Index).
Beberapa data referensi yang dipakai kemudian diubahsuaikan dengan tetap
mengedepankan transparansi dan akuntabilitas.
Penulisan tugas akhir ini dibagi dalam beberapa bab. Susunan pembagian
bab tersebut adalah sebagai berikut:
Bab I membahas tentang latar belakang, maksud dan tujuan, batasan
masalah, tinjauan pustaka, metode penulisan dan sistematika
penulisan.
Bab ini membahas tentang teori-teori penunjang yang akan digunakan
dalam pembahasan.
Bab ini menjelaskan tentang perhitungan indikator Stochastic RSI
(Relative Strength Index) yang mempunyai sifat dapat
mengantisipasi perubahan arah harga suatu saham , dan juga
menjelaskan kapan menghasilkan sinyal jual dan sinyal beli.
Dimana sebelumnya dalam Bab ini akan menjelaskan juga
perhitungan melalui Indikator  Stochastic Oscillator  untuk
dibandingkan dengan Stochastic RSI (Relative Strength Index).
Bab studi kasus ini mengungkapkan hasil perhitungan sesuai
instrumen dan analisis yang digunakan. Dengan menggunakan
semua variabel yang ditentukan sebelumnya, bab ini ditujukan untuk
mendapatkan pembuktian dan kontraindikasi terhadap ide utama,
berusaha menemukan titik temu yang paling optimal sebagaimana
diharapkan.
Bab ini berisi kesimpulan dan saran dari materi-materi yang telah
dibahas dalam bab-bab sebelumnya.

Berdasarkan hasil penelitian dan analisis yang telah dilakukan pada bab-
bab sebelumnya, kesimpulan yang dapat ditarik adalah sebagai berikut :
1. Stochastic RSI (Relative Strength Index) merupakan gabungan
perhitungan antara Stochastic Oscillator dengan RSI sehingga hasil
analisis yang diperoleh  lebih baik.
2. Penggunaan periode lebih panjang dikatakan lebih baik dibanding
menggunakan periode yang singkat.
3. Perbandingan dengan menggunakan jumlah saham yang lebih baik (dalam
kasus ini 5 saham) disinyalir dapat menghasilkan keputusan yang lebih
kuat.
4. Penggunaan sample dalam tiap saham untuk memprediksi saham disinyalir
dapat menghasilkan prediksi yang lebih kuat dimana dalam studi kasus
skripsi ini menggunakan sample rata-rata 96 ditiap sahamnya.
5. Berdasarkan persentase yang diperoleh dari hasil perhitungan
menggunakan periode 25 bahwa Analisis indikator Stochastic RSI
(Relative Strength Index) lebih baik daripada indikator Stochasctic
Oscillator  dimana persentase Stochastic RSI (Relative Strength Index)
menunjukkan persentase diatas 75% sedangkan Stochastic Oscillator biasa
hanya menunjukkan diangka 53,95%.
6. Penggunaan analisis teknikal dapat membantu trader untuk melakukan
transaksi pada saat kegiatan trading berlangsung. Analisis teknikal
membantu memberikan jawaban kapan trader lebih baik melakukan
pengambilan profit (keuntungan) dan kapan waktu yang baik untuk stop
loss (menekan kerugian) dengan cara memberikan sinyal kapan trader
harus masuk dan keluar pasar (menjual dan membeli saham) pada saat
kegiatan trading dilakukan.
7. Stochastic RSI merupakan indikator untuk memprediksi tren dan
membantu menentukan area oversold (kapan harus membeli) dan
membantu menentukan area overbought (kapan harus menjual) atau
dengan kata lain dapat menghasilkan sinyal jual ataupun sinyal beli pada
saat tertentu.
Berdasarkan kesimpulan yang didapat diatas, penulis memberikan saran
sebagai berikut :
1. Masih banyak jenis perbandingan  indikator dalam  analisis tekhnikal yang
dapat dilakukan sehingga bisa menghasilkan prediksi yang lebih tepat.
2. Dalam  analisis terdapat banyak software yang dapat membantu untuk
mempermudah melakukan analisis sehingga hasil yang diperoleh juga
semakin baik.
3. Penulis juga menyarankan untuk membandingkan indikator dengan
menguatkan aspek, seperti Stochastic Oscillator lebih baik jika
dibandingkan terlebih dahulu menggunakan SMA (Simple Moving
Average) ataupun dengan mengunakan EMA (Exponential Moving
Average) agar dapat menimalisir sinyal palsu pada indikator tersebut.

