I.1. Latar Belakang
Hidrokarbon merupakan suatu senyawa yang berasal dari bahan organik
dalam batuan induk yang mengalami beberapa proses hingga hidrokarbon tersebut
dapat dikatakan matang. Minyak dan gas bumi yang masih menjadi sumber daya
energi paling utama untuk memenuhi kebutuhan sehari-hari hidup manusia
merupakan contoh dari senyawa hidrokarbon. Oleh karena tingginya kebutuhan hidup
manusia akan sumber daya energi ini selalu dilakukan peningkatan terhadap ekplorasi,
eksploitasi, dan pengembangan dalam suatu lapangan minyak agar diperoleh produksi
yang lebih maksimal.
Minyak dan gas bumi yang dapat diproduksi haruslah merupakan hidrokarbon
yang sudah terakumulasi pada suatu tempat. Terdapat 5 syarat yang dikenal sebagai
�Petroleum System� agar hidrokarbon ini dapat terakumulasi yaitu : batuan induk
(source rock) yang matang, perangkap (trap), batuan reservoir (reservoir rock) yang
bersifat porous dan permeable, batuan penutup (caprock/seal) yang impermeable,
serta lapisan pembawa (carrier bed) dan waktu migrasi yang memungkinkan minyak
bumi berpindah dan terjebak ke dalam perangkap (proper timing of migration).
Dengan  memadukan bidang keilmuan geofisika dan geologi, hidrokarbon ini
biasanya dapat ditemukan tempat akumulasinya bahkan dapat diperkirakan
cadangannya.  Perkiraan  ini dilakukan dengan analisis petrofisika untuk
mendapatkan nilai dari properti petrofisika. Properti petrofisika yang didapatkan
adalah kandungan serpih, porositas, permeabilitas, dan  saturasi air dalam suatu
formasi yang dianggap sebagai tempat akumulasi dari hidrokarbon.
JOB Pertamina-Talisman (Ogan Komering) Ltd, sebagai salah satu kontraktor
bagi hasil  dengan SKK Migas melihat adanya peluang yang cukup besar dalam
eksplorasi minyak bumi. Hal inilah yang kemudian menjadi alasan bagi JOB
Pertamina-Talisman (Ogan Komering) Ltd, untuk mengembangkan salah satu
lapangannya yang berada di blok Ogan Komering, Cekungan Sumatera Selatan.
Dari blok Ogan Komering telah didapatkan beberapa reservoir yang bernilai
ekonomis, salah satu reservoir tersebut merupakan jenis reservoir karbonat.
Selanjutnya dilakukan proses pengkarakterisasian dan perhitungan cadangan
berdasarkan properti reservoir karbonat tersebut. Hasil yang diperoleh dari
serangkaian kegiatan tersebut tentunya adalah apakah hidrokarbon yang terindikasi
pada suatu lapangan dapat bernilai ekonomis dan mampu untuk turut memenuhi
kebutuhan akan sumber daya energi berupa minyak dan gas bumi.
I.2. Tujuan
Penelitian tugas akhir ini bertujuan untuk :
1. Mengidentifikasi reservoir hidrokarbon pada daerah penelitian.
2. Menentukan parameter fisika batuan, yaitu: kandungan serpih, porositas,
resistivitas, dan saturasi air.
3. Menghitung perkiraan cadangan  hidrokarbon pada daerah penelitian.
I.3. Waktu dan Tempat
Penelitian tugas akhir ini dilakukan pada :
Tempat : Lapangan AINO dan AKASHI, Sub-cekungan Central
I.4. Data yang Digunakan
Data-data yang digunakan dalam penelitian tugas akhir ini adalah sebagai
berikut :
1. Data Well Log dan data test dari beberapa sumur di lapangan  AINO dan
2. Seismik 3D (penampang dan basemap) dan VSP dari data sumur
3. Data analisis air dari beberapa sumur yang dekat dengan data sumur
I.5. Ruang Lingkup Penelitian
Penelitian ini memiliki ruang lingkup dalam karakterisasi resevoir karbonat
pada lapangan AINO dan AKASHI, dimulai dengan melakukan analisis petrofisika
dari data Well Log secara kualitatif dikoreksi dengan data Mud Log untuk
menentukan formasi yang akan dihitung properti petrofisikanya. Analisis secara
kuantitatif dilakukan untuk mendapat nilai-nilai properti petrofisika seperti
kandungan serpih, porositas, saturasi air, dan ketebelan formasi reservoir. Kemudian
dilakukan pula proses well seismic tie, yaitu proses pengkoreksian lapisan batuan
bawah permukaan yang didapat dari data seismik di sekitar lapangan AINO dan
AKASHI untuk dapat mengetahui luas juga volume formasi yang berisi hidrokarbon.
Kemudian hasil dari perhitungan properti petrofisika, juga dengan hasil luas yang
diperoleh dari interpretasi seismik yang telah dilakukan, dilakukan perhitungan
perkiraan cadangan hidrokarbon.

1. Daerah prospek hidrokarbon diidentifikasi pada Formasi Baturaja di lapangan
AINO dan AKASHI, dengan batas GOC pada kedalaman 1446,5 m dan OWC
pada kedalaman 1468,5 m.
2. Berdasarkan perhitungan analisa petrofisikanya diperoleh properti petrofiska
yang ditunjukkan pada tabel VI.1.
Tabel VI.1. Total hasil perhitungan analisa petrofisika pada setiap data sumur
Sumur Net to gross  Vsh (%)  Porositas (%)  Sw (%)
3. Berdasarkan peta struktur kedalaman, daerah perhitungan cadangan
hidrokarbon dibagi menjadi 3 daerah yakni LKG1 dengan luas 22.678 m2,
LKG2 dengan luas 14.421 m2, dan LKO dengan luas 66.188 m2.
Berdasarkan properti fisika batuan didapatkan dari analisa petrofisika,
dihitunglah nilai dari cadangan dari tiap daerah perhitungan cadangan dengan
membuat peta net to gross, porositas, dan saturasi air. Hasil perhitungannya
dapat dilihat pada tabel VI.2.
Tabel VI.2. Hasil perhitungan cadangan
Berdasarkan penelitian kali ini terdapat beberapa saran yang dapat dilakukan
di kemudian hari, yaitu :
1. Perhitungan permeabilitas dan pembuatan peta isopermeabilitas dapat
dilakukan untuk mengetahui batas-batas zona yang ekonomis.
2. Tinjauan mendalam untuk mengetahui zona dan penyebaran hidrokarbon
secara detail seperti inversi.

