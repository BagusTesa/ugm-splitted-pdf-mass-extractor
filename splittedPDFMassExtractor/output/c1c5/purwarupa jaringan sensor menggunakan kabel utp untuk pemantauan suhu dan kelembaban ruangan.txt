Perkembangan Jaringan Sensor (Sensor Network) mempunyai potensi
yang sangat besar dalam upaya untuk memanfaatkan informasi dari lingkungan.
Untuk mendukung fleksibilitas jaringan, umumnya jaringan sensor menggunakan
komunikasi nirkabel dan kabel sebagai media transmisi datanya. Jaringan sensor
telah berkembang cepat karena berpotensi memberikan solusi biaya rendah yang
merupakan tantangan dunia nyata. Biaya yang rendah ini memungkinkan
penyebaran sensor dalam jumlah yang besar untuk aplikasi kehidupan seperti
Smart office, eksplorasi dan pemantauan lahan, penanggulangan bencana,
pengintaian medan perang, dan lain-lain. Dengan protocol TCP/IP untuk
komunikasi berbasis Internet Protocol (IP) telah dikenalkan untuk aplikasi
jaringan sensor karena dapat meningkatkan efisiensi energi dan skalabilitas. Pada
jaringan berbasis Internet Protocol (IP), node dikelompokan dalam beberapa IP
Dengan adanya jaringan sensor tersebut maka penelitian ini akan mencoba
menggunakan sensor suhu dan kelembaban karena proses pemantauan suhu dan
kelembaban sudah banyak diperlukan dalam hal-hal tertentu. Contohnya, pada
suatu gudang penyimpanan yang sangat penting diperhatikan suhu dan
kelembaban dari ruangan tersebut untuk menyimpan barang dengan baik, pada
ruang server komputer juga dibutuhkan suhu dan kelembaban tertentu agar server
tetap dapat bekerja dengan baik dan masih banyak lagi aplikasi lainnya. Tetapi
seringkali juga data pemantauan suhu dan kelembaban masih secara manual yaitu
mendatangi ruangan yang dipantau dan mencatat suhu dan kelembaban. Namun
hal ini menimbulkan permasalahan dalam pencatatan dengan cara tersebut,
bahkan seringkali terjadi kekeliruan. Oleh karena itu harus dikembangkan riset
untuk memaksimalkan kemampuan pemantauan suhu dan kelembaban dengan
jaringan sensor sekaligus mengurangi kesalahan dalam pencataan dalam
pengambilan data.
Permasalahan yang akan diselesaikan pada penelitian ini adalah
bagaimana merancang dan mengimplementasikan alat untuk mengetahui keadaan
suhu dan kelembaban jika berada dalam dua ruangan yang berbeda.
Pembatasaan masalah diperlukan untuk mempermudah pelaksanaan
maupun penulisan Tugas Akhir sehingga tidak menyimpang dari judul Tugas
a. Hanya dapat diakses pada Jaringan lokal melalui kabel UTP RJ45.
b. Penyimpanan data suhu dan kelembaban dalam berkas tipe *.txt.
c. Hanya mampu melakukan pemantauan otomatis maksimal selama 24 jam.
Tujuan dari penelitian ini adalah membuat sebuah sistem jaringan sensor
dalam pemantauan suhu dan kelembaban berbasis Internet Protocol (IP).
Metode-metode yang digunakan dalam penelitian ini adalah sebagai berikut:
1. Menentukan topik yang akan diangkat yang dilatarbelakangi dengan keadaan dan
permasalahan dari ruang penyimpanan pada suatu gudang atau server yang masih
menggunakan pemantauan suhu dan kelembaban secara manual.
2.  Melakukan kajian dan pembelajaran lebih lanjut tentang sistem yang dibahas pada
penelitian ini dengan metode:
(a) Studi literatur, yaitu mempelajari artikel, makalah, jurnal, karya tulis, serta
buku-buku yang terkait dengan Arduino Uno, Sensor DHT11, Rangkaian RTC
DS1307, Visual Basic 6.0, dan teknologi-teknologi yang mendukung sistem
tersebut.
(b) Konsultasi dengan dosen pembimbing mengenai rancangan sistem dan
inovasi-inovasi yang bisa diterapkan pada sistem.
3. Membuat perancangan sistem yang terdiri dari dua bagian, yaitu:
(a) Perangkat Keras (Hardware)
Meliputi perancangan Sensor DHT11, Rangkaian DS1307, Ethernet Shield dan
Arduino Uno sehingga menjadi satu sistem hardware terintegrasi.
(b) Perangkat Lunak (Software)
Meliputi pembuatan program mikrokontroler ATMega328 pada Arduino Uno
memakai Arduino IDE, PC sebagai penerima data dari hasil pengiriman data
memakai antarmuka dengan software Visual Basic 6.0 yang memakai bahasa
pemongraman Basic.
4.  Pengujian untuk setiap bagian sistem, pengujian terhadap kinerja sensor dan untuk
menampilkan hasil data menggunakan tampilan Visual Basic 6.0, kemudian hasil
data tersebut di simpan dengan sistem berkas teks.
5.  Langkah terakhir adalah melakukan pengujian sistem secara keseluruhan sehingga
untuk memastikan apakah sistem telah bekerja dengan baik dan hasilnya sesuai
dengan yang diinginkan atau tidak.
KELEMBABAN RUANGAN” sebagai berikut:
Berisi latar belakang pembuatan tujuan, batasan masalah yang dikerjakan
dan sistematika pembahasan.
Memuat uraian sistematis tentang informasi rancangan dari alat yang
sudah ada sebelumnya.
Pada Bab ini akan diuraikan tentang landasan teori penunjang yang
digunakan dalam pembahasan masalah meliputi karakteristik komponen-
komponen yang digunakan dalam sistem dan prinsip dasar pemantauan suhu dan
kelembaban memakai Arduino Uno dan Ethernet Shield, Module Sensor DHT11
dan rangkaian RTC DS13
Bab ini berisi perancangan sistem yang terdiri atas bagian perangkat keras
(hardware) yaitu Arduino Uno dan Ethernet Shield, perangkat lunak (software)
yaitu program untuk mikrokontroler dengan software Arduino IDE yang memakai
bahasa C yang kemudian ditampilkan pada antarmuka Visual Basic 6.0. Software
Interface sebagai penampil dan penyimpan data yang ditampilkan tanpa
penyimpanan basis data. Pada Bab ini juga disajikan data-data hasil percobaan
dan pengujian sekaligus analisa dan sistem kerja tiap blok rangkaian yang secara
keseluruhan membentuk sistem pemantauan pengukuran suhu dan kelembaban.
Bab ini berisi penjelasan dari tentang implementasi rancangan yang telah
dibuat sebelumnya yang meliputi implementasi rancangan hardware maupun
software.
Bab ini berisi kesimpulan atas penelitian yang telah dilaksanakan serta
memberikan saran untuk pengembangan sistem lebih lanjut.
Penelitian yang memanfaatkan teknologi internet pernah dilakukan oleh
Listiarga (2011) dalam tugas akhirnya yang berjudul Purwarupa Sistem
Pemantauan Suhu menggunakan Atmega 168 dan ENC28J60 Berbasis Embedded
Ethernet. Tugas Akhir yang dibuat adalah suatu sistem webserver sederhana yang
melakukan monitoring suhu berbasis embedded ethernet yang mana data suhu
ditampilkan melalui web browser sederhana. Sensor yang digunakan adalah jenis
LM35. Atmega 168 berfungsi sebagai unit kendali dan Ethernet Module
ENC28J60 sebagai kontrol jaringan yang menangani komunikasi data secara
TCP/IP. Selain itu juga digunakan sistem embedded ethernet dengan
menggunakan Bascom AVR sebagai software untuk konfigurasi.
Sedangkan dalam penelitian lain yang telah dilakukan Jannah (2011) yaitu
pemantauan suhu dengan sistem penunjang penyimpanan data dalam tugas
akhirnya yang berjudul Purwarupa Sistem Pemantauan Suhu berbasis Web.
Sistem pemantauan suhu menggunakan Mikrokontroler Atmega 32 sebagai unit
kendali dengan LM35 sebagai sensor suhu. Sistem Pemantauan suhu berbasis web
yang dirancang IC MAX 232 mengubah sinyal data komputer ke TTL
(Transistor-Transistor Logic) yang diolah oleh Mikrokontroler. Pada software ini
memakai Visual Basic 6.0 sebagai antarmuka server dan MySQL sebagai
penyimpanan basis data. Setelah data tertampung pada server, kemudian data
tersebut dikirimkan ke web. Data yang dimunculkan pada web bisa disimpan
dengan format berkas excel.
Berbeda dengan penelitian di atas, implementasi pemantauan suhu tidak
hanya di ruangan bertembok tetapi juga terdapat di ruangan berkaca dalam tugas
akhir yang berjudul Perancangan Sistem Instrumentasi Pemantauan Suhu Rumah
Kaca berbasis Web, sistem pemantauan suhu pada rumah kaca menggunakan
Atmega 168 dan hasil data suhunya ditampilkan melalui web. Sistem ini berfungsi
untuk mengetahui kestabilan suhu pada rumah kaca melalui port serial. Basis data
penyimpanan yang digunakan Microsoft Access. Bahasa pemrograman yang
digunakan adalah Visual Basic, PHP dan webserver Apache. Dalam sistem server
dapat memantau dan mengendalikan suhu yang ada pada ruangan rumahnya.
Rancangan alat dapat diakses dengan website melalui internet dengan
menggunakan browser, sehingga dapat dipantau dan dikendalikan secara jarak
jauh (Prihandoko, 2012).
Selain pemantauan menggunakan sensor suhu, sekarang ditambahkan
dengan sensor lain.  Dalam makalah yang berjudul Rancang Bangun Sistem
Monitoring Ruang Terintegrasi Berbasis Ethernet, sebuah sistem monitoring
berbasis ethernet yang menangani pemantauan suhu, cahaya, dan gerak di dalam
suatu ruangan, yang mana merupakan salah satu solusi dalam efisiensi listrik
dalam ruangan tersebut. Sistem yang dibuat menggunakan Mikrokontroler
Atmega 8535 dan sebuah ethernet controller ENC28J60 sebagai kontrol jaringan
yang menangani proses komunikasi protokol TCP/IP. Mikrokontroler
dikonfigurasi menggunakan CodeVision AVR Compiler. Sistem ini menggunakan
sensor suhu LM35, sensor cahaya LDR, dan sensor gerak PIR. Sedangkan
aplikasi antarmuka yang menggunakan Visual Basic 6.0 (Siregar, 2012).
Sedangkan penelitian selanjutnya yaitu pemantauan sensor suhu yang
ditambahkan dengan sensor kelembaban, telah dilakukan oleh Taufan (2012)
dalam tugas akhirnya yang berjudul Purwarupa Sistem Monitoring Suhu dan
Kelembaban Ruangan Berbasis Ethernet, sebuah sistem monitoring berbasis
ethernet yang menangani pemantauan suhu dan kelembaban di dalam suatu
ruangan. Alat yang dibuat menggunakan Arduino Uno sebagai unit kendali dan
sebuah Ethernet Shield sebagai komunikasi protokol TCP/IP. Sistem ini
menggunakan Module Sensor SHT11 dan Rangkaian RTC DS1307 sebagai
pengatur waktu. Sedangkan aplikasi antarmuka yang digunakan dikembangkan
menggunakan Visual Basic 6.0.
Berdasarkan penelitian sebelumnya, maka dibuat alat yang menggunakan
Arduino Uno, modul Ethernet, Module Sensor DHT11, Rangkaian RTC (Real
Time Clock) DS1307 dan antarmuka browser dan Visual Basic 6.0 menggunakan
sistem jaringan sensor melalui kabel UTP. Perbandingan dari alat yang dibuat
dengan alat yang sudah dibuat sebelumnya ditunjukan pada Tabel 2.1.
No Penulis dan
(2011)
menggunakan
Atmega168 berbasis
Sensor LM35 sebagai pendeteksi
dan untuk mengetahui hasil nilai
suhu ditampilkan melalui LCD
(2011)
berbasis Web
Sistem ini akan memantau suhu
dengan sistem web
(2012)
Rumah Kaca berbasis
Sistem ini masih menggunakan web
tetapi untuk memantau suhu di
rumah kaca
Sistem ini menggunakan jaringan
sensor dengan sensor suhu LM35,
sensor cahaya, sensor gerak PIR.
(2012)
Monitoring Suhu dan
berbasis Ethernet
Pada sistem ini menggunakan
sensor SHT11 untuk sensor suhu
dan kelembaban data ditampilkan
pada tampilan Visual Basic 6.0
(2014)
Sensor menggunakan
Kabel UTP untuk
pemantauan Suhu dan
Sistem ini menggunakan jaringan
sensor dengan sensor DHT11 untuk
mengukur suhu dan kelembaban
serta menyimpan data pada
tampilan Visual Basic 6.0

Dari penelitian yang penulis lakukan, dapat diambil kesimpulan sebagai
berikut:
1. Telah diimplementasikan dengan baik sesuai perancangan sistem, sistem
jaringan sensor dalam pemantauan suhu dan kelembaban berbasis Internet
2. Sistem yang telah dibuat dapat mempermudah aktivitas pemantauan dan
pencatatan suhu dan kelembaban dalam suatu ruangan dari jarak jauh.
3. Kelebihan dari purwarupa alat ini yaitu adanya fitur penyimpanan data waktu,
suhu, dan kelembaban dalam sebuah file teks secara manual dan otomatis.
Berikut saran-saran yang penulis sampaikan untuk penelitian-penelitian
selanjutnya yang sejenis.
1. Komunikasi yang dilakukan masih menggunakan kabel, bisa di tambahkan
mengunakan jaringan nirkabel.
2. Memperbaiki tampilan program antarmuka dan tampilan web sehingga lebih
nyaman saat digunakan oleh pemantau.
3. Diharapkan tidak hanya dapat diakses pada jaringan lokal saja tetapi
menambahkan jaringan internet yang lebih luas.

