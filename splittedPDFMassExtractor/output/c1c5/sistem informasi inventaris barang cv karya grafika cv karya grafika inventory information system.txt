Semakin berkembangnya era informasi dan teknologi saat ini, kebutuhan akan
pelayanan informasi sangat pesat. Hal ini ditunjukkan dengan semakin meningkatnya
kegiatan pembangunan dibidang teknologi informasi salah satu sarana yang dipakai
untuk pengolahan data adalah menggunakan komputer. Pentingnya penggunaan
komputer dapat dirasakan baik instansi pemerintah maupun swasta.
Adapun keunggulan dalam pemakaian komputer pada suatu pekerjaan baik
mengenai penyimpanan data dan pencarian berkas-berkas. Maka dalam keunggulan
komputer ini instansi atau perusahaan sangat membutuhkannya, karena mereka ingin
suatu pengolahan data yang efisien dan efektif.
CV Karya Grafika merupakan sebuah perusahaan peretakan yang melayani
pembuatan  penjilidan buku-buku, majalah-majalah, penerbitan surat kabar, brosur-
brosur dan periklanan. Perusahaan ini terletak di JL. Balai Pustaka no.51A,
Pengelolaan barang di CV Karya Grafika  hingga saat ini masih menggunakan
buku  bahkan  terkadang proses barang masuk dan barang keluar dilakukan dengan
sistem kepercayaan atau dengan  hanya menghafal barang yang masuk dan yang
keluar. Karena hal itu jumlah barang yang tersedia digudang CV Karya Grafika tidak
terdata dengan baik sehingga bila terjadi kekosongan persediaan barang pemilik tidak
mengetahuinya.
Oleh karena itu dperlukan sebuah solusi agar data barang dapat diolah dengan
baik serta menghasilkan informasi yang lebih akurat, yaitu dengan menggunakan
sebuah sistem informasi. Dengan adanya sistem ini memudahkan pemilik dan
karyawan dalam pendataan barang masuk dan barang keluar serta dapat
mengeluarkan output laporan barang masuk dan barang keluar dengan atau tanpa
periode waktu yang diinginkan.
Sehubungan dengan yang dikemukakan diatas penulis berkeinginan untuk
membahasnya lebih lanjut dalam bentuk penulisan Tugas Akhir dengan judul �Sistem
Sesuai dengan latar belakang permasalahan diatas, maka permasalahan yang
timbul adalah bagaimana membangun sebuah Sistem Informasi Inventaris Barang CV
Karya Grafika yang bisa memberikan informasi tentang data barang, data barang
masuk dan data barang keluar, laporan data barang serta mencegah terjadinya
kesalahan atau ketidak sesuaian persediaan barang yang sering dialami karena masih
menggunakan  pencatatan  langsung dibuku.
Sistem ini dapat memberikan data inventaris barang CV Karya Grafika,
Batasan-batasan sistem ini meliputi :
1. Sistem ini hanya menangani persediaan inventaris milik CV Karya Grafika
2. Persediaan barang bergantung pada transaksi masuk dan keluar
3. Sistem ini tidak menangani produksi pencetakan pesanan
4. Pengembangan sistem tidak memperhatikan masalah keamanan.
Tujuan yang hendak dicapai adalah merancang dan mengimplementasikan
Sistem Informasi inventaris barang CV. Karya Grafika yang dapat membantu proses
memonitoring keadaan inventaris barang, merubah sistem manual menjadi sistem
yang baru dengan menggunakan sistem terkomputerisasi, meningkatkan efisiensi dan
efektivitas inventaris dan guna mempercepat dan mempermudah.
Adapun manfaat dari penelitian ini adalah :
1. Membuat suatu sistem inventaris yang baru dan diharapkan nantinya dapat
memberikan informasi yang dibutuhkan dengan cepat, tepat dan akurat serta
mudah dalam proses pengolahan datanya sehingga proses yang dilakukan dapat
berjalan dengan lancar.
2. Lebih mempermudah pembuatan laporan, baik laporan keseluruhan, harian,
bulanan maupun tahunan. Dimana laporan yang terbentuk lebih mudah untuk
dipahami dan mengarah ke bentuk yang kita harapkan.
3. Guna membantu proses inventaris barang di CV. Karya Grafika dan
mempercepat waktu pencarian data barang.
4. Mengefektifkan penyelesaian dan pengambilan keputusan dan dapat
mempermudah dalam memelihara inventaris barang.
5. Membantu mempercepat karyawan dalam penyampaian informasi data barang
dan melakukan transaksi barang.
Metode�metode yang digunakan dalam penelitian ini adalah sebagai berikut:
Metode pengumpulan data dengan mengadakan pengamatan secara langsung di
Wawancara dilakukan dengan cara tanya jawab secara lisan terhadap kepala
perusahaan dan bagian-bagian tertentu yang ada hubungannya dengan sistem
yang akan dibuat. Wawancara dilakukan untuk memperoleh informasi dan data-
data yang dibutuhkan dalam penelitian ini.
Studi literatur dilakukan dengan mengumpulkan dan mempelajari informasi-
informasi yang berhubungan dengan penulisan, termasuk perancangan, analisis,
dan implementasi sistem.
Metode ini digunakan dalam proses pembuatan sistem. Adapun tahapan-
tahapannya adalah:
a. Analisis Sistem
Menganalisis kebutuhan sistem baik kebutuhan fungsional maupun
kebutuhan non fungsional sistem.
b. Perancangan Sistem
Merancang sistem menggunakan UML (Unified Modeling Language)
seperti menggunakan use case diagram, activity diagram, class diagram
untuk merancang  proses sistem dan diagram  relasi entitas untuk merancang
basis data dari sistem.
c. Implementasi Sistem
Mengimplementasikan hasil perancangan sistem menjadi sebuah aplikasi
dengan menggunakan pemrograman PHP, HTML dan Java dan basis data
menggunakan MySql.
Berikut sistematika penulisan perancangan dan pembangunan aplikasi Sistem
Dalam bab ini membahas tentang latar belakang masalah, perumusan
masalah, batasan masalah, tujuan penelitian, manfaat penelitian,
metode penelitian, serta sistematika penulisan laporan Tugas Akhir.
Berisi uraian sistematis tentang informasi hasil penelitian sebelumnya
yang disajikan dalam pustaka dan menghubungkannya dengan Sistem
Berisi konsep dan teori yang mendukung dalam pembuatan aplikasi,
seperti pengetahuan tentang sistem pakar, bahasa pemrograman, serta
software basis data yang digunakan.
Bab ini memaparkan analisis kebutuhan perangkat lunak yang
digunakan untuk mendefinisikan hal-hal yang diperlukan dalam
pengembangan perangkat lunak. Analisis kebutuhan tersebut meliputi
analisis sistem, rancangan proses, rancangan basis data, dan rancangan
antar muka pengguna.
Bab ini menguraikan tentang implementasi hasil perancangan desain
yang menampilkan aplikasi antar muka dengan menggunakan bahasa
pemrograman PHP, HTML,  Java dan MySql disertai cara kerja dan
penggunaan program.
Bab ini menyajikan kesimpulan dari laporan tugas akhir dan saran
terhadap seluruh kegiatan tugas akhir yang telah dilakukan. Saran
diperlukan sebagai upaya pengembangan sistem selanjutnya.

Berdasarkan pengujian yang telah dilakukan pada Sistem Informasi Inventaris
Barang CV Karya Grafika dapat diambil beberapa kesimpulan sebagai berikut :
1. Sistem Informasi Inventaris Barang CV Karya Grafika berhasil dibangun dan
dapat menangani pendataan barang, transaksi barang masuk dan transaksi
barang keluar.
2. Sistem Informasi Inventaris Barang CV Karya Grafika dapat menangani
laporan barang masuk dan barang keluar.
3. Dengan adanya Sistem Informasi Inventaris Barang CV Karya Grafika ini
dapat membantu mempermudah admin dan karyawan dalam proses
mendapatkan informasi dan pengelolaan data barang.
Untuk memberikan pelayanan yang lebih baik terkait dengan kemudahan
pelayanan informasi, beberapa saran yang penulis berikan yaitu :
1. Perbaikan pada user interface agar lebih menarik dengan tujuan
mempermudah  para pengguna.
2. Menampilkan grafik hasil transaksi barang masuk dan barang keluar
perminggu ataupun perbulan dan pertahun.

