Penyelesaian masalah fisika pada dasarnya adalah pemahaman berbagai
materi yang berhubungan dengan fenomena-fenomena alam dari sains fisika.
Adapun untuk memudahkan pemahaman materi tersebut diperlukan suatu media
peraga atau alat penunjang. Beberapa materi yang memerlukan media peraga
diantaranya seperti gerak suatu benda. Gerak suatu benda jatuh dipengaruhi oleh
percepatan gravitasi yang ada juga gaya gesekan.
Eksperimen merupakan salah satu metode yang biasa digunakan untuk
memudahkan pemahaman, tetapi dalam kenyataannya metode ini mengandung
beberapa kendala diantaranya peralatan yang kurang memadai dan juga  mahal.
Oleh karena itu, diperlukan pengembangan dari segi fasilitas untuk media
penyelesaian masalah fisika yakni menggunakan media virtual dengan
memanfaatkan aplikasi komputer. Penggunaan aplikasi visual dengan komputer
merupakan salah satu metode yang dapat digunakan untuk meggambarkan
fenomen�fenomena fisika secara jelas atau secara visual sehingga mudah untuk
diamati dan dipahami. Oleh karena itu diperlukan software yang dapat membantu
pemahaman secara jelas dan singkat. Dalam hal ini bahasa pemrograman Reduce
dapat mengambil peran yang besar.
Saat ini kita dapat merasakan kemudahan dan memanfaatkan komputer
karena sudah didukung oleh sistem operasi dan software yang mudah digunakan
(user-friendly). Demikian juga dengan game yang kini semakin mengasyikkan
untuk dimainkan. Namun, pernahkah kita membayangkan bagaimana jerih payah
para programmer dalam membuatnya? Dalam membuat software, seorang
programmer berbekal logika dan konsep yang dia miliki, harus membuat alur
logika (skema) dari software yang akan dia buat. Misalnya, mau seperti apa
software yang dia buat, apa saja kemampuan yang dia miliki, dan beragam faktor
lainnya. Kemudian, dengan sebuah bahasa pemrograman, dia akan berusaha
menyusun pernyataan-pernyataan yang dapat dimengerti dan dijalankan oleh
prosesor untuk menghasilkan sebuah output. Semakin �pintar� software yang dia
buat, maka semakin rumit dan panjang pernyataan/perintah yang harus dia buat
dalam bahasa pemrograman. Bersyukurlah kita dengan perkembangan bahasa
pemrograman saat ini, yang sudah didukung dengan bahasa pemrograman yang
high-level. Seorang programmer lebih dipermudah dalam membuat
pernyataan/perintah sehingga dia dapat lebih memikirkan bagaimana membuat
software yang dibuat semakin komplet dan pintar. Kondisi itu berbeda dengan
zaman dahulu. Kala itu, komputer dijalankan dengan pemrograman manual, yang
menggunakan instruksi binary dan hex. Untuk membuat dan menjalankan
program tersebut, dibutuhkan waktu yang lama dan sering dijumpai banyak
kesalahan. Program kemudian menjadi sangat sulit untuk dibaca dan dimodifikasi.
Begitu susah dan rumitnya, pada saat itu tidak banyak orang yang tertarik untuk
menjadi programmer komputer. Jadi, hanya segelintir orang yang menekuninya.
Ini diperparah lagi dengan masih mahalnya harga komputer kala itu. Bayangkan,
sebuah software harganya bisa mencapai dua sampai empat kali harga komputer.
Fenomena itulah yang kemudian memacu orang membuat bahasa pemrograman
yang semakin mudah digunakan. Maka, sejak tahun 1957 terus dikembangkan dan
bermunculan berbagai bahasa pemrograman. Tidak kurang 150 bahasa
pemrograman dirilis sejak tahun 1957 sampai sekarang (Deky, 2011).
Bagi masyarakat menengah ke atas, komputer di zaman sekarang bukanlah
merupakan barang mewah lagi, bahkan ada kampung melek internet. Bahasa
pemrogaman Reduce merupakan bahasa pemrogaman baru yang bisa digunakan
secara interaktif (sebagai kalkulator) dan bisa sebagai progam. Penyelesaian
masalah fisika selain bersifat kualitatif juga bersifat kuantitatif, dimana
matematika sangat diperlukan. Dalam hal ini komputer berperanan mempercepat
proses perhitungan tanpa membuat kesalahan. Komputer berperan dalam
pengembangan bahasa pemrogaman baik yang bersifat numerik maupun simbolik,
seperti bahasa pemrogaman Reduce. Bahasa pemrogaman Reduce berlaku umum,
yakni dapat diaplikasikan untuk menyelesaikan persoalan matematika secara
sederhana (yang juga dapat diselesaikan dengan metode analitik secara manual)
maupun persoalan matematika yang tergolong rumit (yang metode analitikpun
belum tentu dapat menyelesaikannya).
Hasil eksperimen selalu harus dinyatakan beserta ralatnya sedangkan hasil
perhitungan teoretik biasanya dinyatakan tanpa ralat. Padahal dalam perhitungan
teoretik ada ralat yang berasal dari aproksimasi dalam metode numerik (yang
disebut ralat pemotongan) dan ralat yang berasal dari kalkulator atau komputer
(yang disebut ralat pembulatan). Jika hasil perhitungan teoretik tidak disertai
ralatnya dan kemudian dibandingkan dengan hasil eksperimen yang selalu disertai
ralat, maka dapat timbul miskonsepsi bahwa perhitungan teoretik adalah eksak
sedangkan eksperimen selalu mengandung ralat.(Hermanto, 2012
a
)
Penelitian sebelumnya dihasilkan bahwa penentuan ralat komputasi
penting karena perbedaan antara dua nilai menjadi kabur oleh besarnya ralat.
Dalam kasus yang kita ditinjau misalnya adanya gaya penghambat dan faktor
relativistik dianggap sebagai  koreksi (karena nilai ? dan ?(0) yang relatif kecil),
maka dibandingkan hasil komputasi dengan nilai posisi benda seandainya tidak
ada penghambat dan faktor relativistik. Bahasa pemrograman yang mampu
melakukan komputasi bilangan rasional dan polinomial (di sini dicontohkan
UBASIC) mempunyai potensi sangat besar dalam penyelesaian persamaan
diferensial yang dijumpai dalam semua bidang fisika.(Hermanto, 2012
b
).
Berdasarkan uraian dalam latar belakang di atas, maka dapat dirumuskan
beberapa masalah, diantaranya adalah sebagai berikut :
1.2.1. Bagaimanakah komputasi gerak benda jatuh relativistik dengan variasi
percepatan gravitasi dan gesekan dengan metode deret Taylor
1.2.2. Bagaimanakah perhitungan ralat komputasi gerak benda jatuh relativistik
dengan metode deret Taylor
1.2.3. Berapakah perbedaan antara gerak jatuh non-relativistik dengan yang
dikoreksi relativistik dengan variasi percepatan gravitasi dan gesekan
Tujuan penelitian ini adalah menjawab masalah dengan metode deret Taylor
dan bahasa Reduce.
Penelitian yang dilakukan terutama untuk menunjukkan metode
komputasinya. Kita tidak terikat pada gerak benda di permukaan Bumi. Tidak
dibahas aplikasi dari komputasi ini.
Manfaat yang didapatkan dari pembuatan aplikasi ini adalah :
1.5.1. Memberikan kontribusi pada materi fisika kuantitatif baik yang bersifat
numerik maupun simbolik
1.5.2. Diharapkan mampu memberikan kontribusi pada dunia pendidikan,
sehingga akan lebih mudah memahami materi fisika
1.5.3. Praktikum fisika yang sulit dan bahannya mahal dapat diganti dengan
aplikasi komputer yang lebih murah, lebih jelas, dan lebih cepat
mendapatkan hasil yang benar.
Tugas Akhir ini terdiri dari beberapa bab dan masing�masing bab dipecah
dalam beberapa sub�sub dengan memperinci pokok-pokok permasalahan,
sehingga penyajian Tugas Akhir ini dapat dilakukan secara sistematis.
Bab I : berisi uraian mengenai latar belakang masalah, perumusan masalah,
tujuan penelitian, batasan masalah, manfaat penelitian, dan sistematika penelitian.
Bab II : berisi uraian mengenai tinjauan pustaka tentang penelitian yang
membahas tentag bahasa pemrogaman.
Bab III : berisi uraian mengenai teori�teori pokok yang diperlukan dalam
penelitian.
Bab IV : berisi uraian mengenai komputasi dan pembahasan dari analisis teori,
beberapa penggunaan metode deret dengan bilangan rasional, penggunaan
program reduce hukum Newton relativistik, dan  ralat pemotongan.
Bab V : berisi uraian mengenai kesimpulan dan saran

Dalam penelitian ini telah berhasil dilakukan komputasi dua kondisi antara
gerak benda jatuh non relativistik dengan gerak benda yang dikoreksi relativistik
serta variasi percepatan gravitasi dan gesekan menggunakan bahasa Reduce.
Dalam komputasi ada ralat pemotongan sehingga hasil komputasi adalah ? =
1,0999 � 0,001 (satuan panjang). Perbedaan nilai antara gerak benda jatuh non
relativistik dengan gerak benda yang dikoreksi relativistik serta variasi percepatan
gravitasi dan gesekan adalah sebesar 0,00537 (satuan panjang).
Dalam tulisan ini masih banyak yang perlu dikaji dan dikembangkan lebih
jauh lagi, seperti materi gerak benda dengan menggunakan variasi besaran yang
berbeda, atau materi gerak benda jatuh relativistik variasi percepatan gravitasi
dengan nilai konstanta yang sudah ada.

