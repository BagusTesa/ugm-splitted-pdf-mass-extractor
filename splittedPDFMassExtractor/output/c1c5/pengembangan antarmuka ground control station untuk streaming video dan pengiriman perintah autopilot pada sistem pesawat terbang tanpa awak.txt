Saat ini, beberapa negara maju sedang mencoba untuk mengembangkan
teknologi pesawat tanpa awak atau sering disebut dengan Unmanned Aerial
Vehicle (UAV). UAV adalah suatu sistem. Sistem ini terdiri dari beberapa
subsistem  diantaranya pesawat itu sendiri, muatannya, Ground Control Station
(GCS) atau sering disebut stasiun pemantauan, peluncuran pesawat dan recovery
pada subsistem, komunikasi subsitem dan lain sebagainya (Austin, 2010).
UAV ini memiliki banyak kelebihan diantaranya, pesawat ini dapat
digunakan untuk menjalankan misi yang berbahaya ataupun misi rahasia. Salah
satu misi UAV yang paling sering digunakan adalah pemantauan kondisi di suatu
tempat. Ketika menjalankan misi pemantauan ini, UAV harus memiliki kamera
yang digunakan untuk streaming video. Video yang diambil oleh kamera UAV
akan dikirim dan ditampilkan pada stasiun pemantauan. Maka pemantauan tidak
harus dilakukan secara manual atau menggunakan pesawat atau helikopter dengan
orang di dalamnya yang tentunya dapat membahayakan pilotnya. Namun dengan
menggunakan UAV yang dilengkapi dengan kemampuan streaming video maka
pemantauan dapat dilakukan dari layar stasiun pemantau.
Selain digunakan untuk menampilkan streaming video dari kamera, GCS
juga dapat digunakan untuk mengirimkan perintah. Dalam penelitian ini perintah
yang dikirim adalah perintah autopilot. Perintah autopilot ini sangat penting dalam
misi pemantauan. Perintah ini digunakan untuk mengarahkan UAV ke suatu
posisi yang ingin dipantau. Data perintah yang dikirim berupa titik koordinat garis
bujur dan lintang tujuan. Untuk mengaktifkan autopilot tersebut diperlukan suatu
sistem pengiriman perintah dari GCS. Sehingga setelah data diterima oleh UAV,
maka UAV mampu memroses data yang diperlukan dan menjalankan perintah
autopilot berdasarkan data perintah yang telah dikirim.
Pengembangan GCS ini dilakukan secara bertahap, penelitian ini adalah
tahapan selanjutnya dimana GCS yang dibuat ini mengacu pada GCS yang telah
dibuat sebelumnya. Penelitian ini akan dilakukan dengan melakukan
pengembangan antarmuka streaming video dan pengiriman perintah autopilot
pada Ground Control Station agar lebih mampu menunjang setiap misi yang
dijalankan.
Berdasarkan latar belakang dapat dirumuskan suatu permasalahan yaitu
bagaimana mengembangkan antarmuka streaming video dan pengiriman perintah
autopilot pada Ground Control Station (GCS) untuk pesawat terbang tanpa awak.
Tujuan dari penelitian ini yakni mengembangkan suatu antarmuka Ground
Control Station (GCS) yang mampu menampilkan streaming video dan
mengirimkan perintah autopilot  untuk pesawat terbang tanpa awak.
Dari beberapa permasalahan yang ada maka akan dibatasi oleh hal � hal
sebagai berikut:
1. Pembuatan antarmuka stasiun pemantauan  yang digunakan menggunakan
Visual Studio 2010 dengan bahasa pemrograman C#.
2. Penelitian ini berfokuskan pada fungsi Ground Control Station sebagai
stasiun pemantauan sebagai  pendukung fungsi kontrol UAV yang berdiri
sendiri.
3. Perintah autopilot  yang dilakukan hanya sebatas pengiriman perintah
autopilot yang dilakukan dengan mengirimkan data koordinat posisi
latitude dan longitude titik yang dituju.
4. Format Video Streaming yang digunakan dalam penelitian ini adalah
MJPEG yang bersumber dari Webcam.
5. Cara dan mendapatkan data sensor yang akan  ditampilkan pada panel
instrumen pesawat ini  berasal pada modul ADAHRS pesawat yang tidak
dijelaskan pada penelitian ini.
6. Dalam pemantauan UAV dibatasi hanya dapat menggunakan satu device
saja.
7. Pemantauan dilakukan dengan pengamatan secara horizontal dan konstan
pada ketinggian 1 m.
Metode Penelitian yang digunakan dalam penelitian ini meliputi beberapa
hal, diantaranya :
a. Menentukan topik utama dalam penelitian
Di dalam penelitian ini topik utama yang diangkat adalah streaming video
dan pengiriman perintah melalui Stasiun Pemantau.
b. Studi Literatur
Studi Literatur di lakukan untuk mengetahui dasar � dasar teori mengenai
konsep pada penelitian ini. Pada tahap ini dilakukan pengumpulan data
berupa materi dan kajian yng diperlukan dalam menyusun sistem Stasiun
Pemantauan ini. Terutama pengkajian ini dilakukan mengenai
pemrograman dengan Visual Studio menggunakan bahasa C#, alat-alat
yang bahan digunakan dan ilmu � ilmu yang mendukung sistem ini.
c. Perancangan dan pembuatan Sistem
a. Perancangan Perangkat Keras
Perancangan ini meliputi penggunaan SBC Raspberry Pi, Wi-Fi USB,
ADAHRS,dan komputer yang berfungsi sebagai Stasiun Pemantau.
b. Perancangan Perangkat Lunak
Perancangan Perangkat Lunak ini meliputi pemrograman pada Visual
Studio dengan bahasa pemrograman C# yang akan digunakan pada
Stasiun Pemantau. Pemrograman pada ADAHRS, pemrograman pada
Raspberry Pi untuk mengolah hasil video streaming yang berasal dari
d. Pengujian Sistem
Pengujian sistem dilakukan dengan menghitung banyaknya data yang
diterima oleh Stasiun Pemantau. Selanjutnya menghitung nilai framerate
yang dihasilkan melalui video streaming
e. Pembahasan
Pembahasan dilakukan untuk membahas hasil yang didapatkan dari
pengujian yang telah dilakukan pada penelitian ini.
Sistematika penulisan laporan ini adalah sebagai berikut :
Bab ini berisi tentang latar belakang penelitian, rumusan masalah,
batasan masalah, tujuan penelitian, metodologi penelitian dan
sistematika penulisan.
Bab ini berisi tentang penelitian-penelitaian sebelumnya yang
digunakan sebagai acuan dalam penelitian ini. Selanjutnya terdapat
tabel perbandingan penelitian yang berisi penelitian sebelumnya
dibandingkan dengan penelitian yang dilakukan sekarang ini.
Bab ini berisi tentang penjelasan � penjelasan dari beberapa komponen
yang digunakan dalam penelitian ini.
Bab ini berisi tentang pernacangan sistem, meliputi perancangan secara
keseluruhan, perancangan perangkat lunak, perancangan perangkat
keras dan perancangan pengujian.
Pada bab ini akan dibahas mengenai implementasi dari keseluruhan
sistem meliputi perangkat keras dan perangkat lunak yang sudah
dirancang dari bab sebelumnya.
Berisikan tentang hasil pengujian sistem yang telah di
implementasikan sebelumnya. Hasil pengujian ini akan dianalisis dan
dibahas hsil dan kinerjanya.
Bab ini berisi tentang kesimpulan dan saran mengenai pengembangan
dari sistem ini dan tentunya menggunakan metode yang lebih baik lagi.

Dari Penelititan yang telah dilakukan dapat diambil kesimpulan sebagai
berikut:
1. Penelitian ini mampu mengimplementasikan antarmuka GCS yang dapat
menampilkan kondisi pesawat, menampilkan video streaming dan
mengirimkan perintah untuk menyalakan dan mematikan autopilot.
2. Video streaming terbaik dilakukan dengan resolusi 320x240 pixels
dengan jarak optimal 80 m dengan nilai framerate sebesar 23 fps pada
ketinggian 1 m.
3. Modul ADAHRS mampu membaca perintah autopilot dan membutuhkan
waktu 8.78 � 0.01 ms untuk perintah menyalakan autopilot dan 6.75 �
0.01 ms untuk perintah mematikan autopilot(manual).
4. Jarak terjauh unit pengiriman didarat yang mampu dipantau oleh stasiun
pemantauan adalah 139,5 m dengan kemampuan menerima 17 paket data
tiap detiknya.
Dari Penelitian ini, untuk pengembangan penelitian ini lebih lanjut dapat
disarankan:
? Hendaknya untuk penelitian selanjutnya, pengiriman perintah autopilot ini
dapat di ujikan pada sistem autopilot yang sesungguhnya sehingga hasil
penelitian dapat maksimal.

