I.1  Latar Belakang
Kabupaten Grobogan, Jawa tengah terkenal dengan adanya sumur minyak
tua peninggalan Belanda yang tersebar di beberapa tempat. Sebagian dari sumur-
sumur tersebut masih dapat memproduksi minyak yang ditambang secara
tradisional oleh warga setempat dan sebagian lagi sudah tidak berproduksi.
Diduga, masih ada sumur-sumur yang terpendam di daerah tersebut. Pemerintah
Kabupaten  Grobogan berniat akan membuka sumur minyak di Dusun Bapo, Desa
Bendoharjo, Kecamatan Gabus karena ditengerai, sumur minyak peninggalan
zaman Belanda tahun 1929 itu masih menyimpan minyak cukup besar
Jika rencana pembukaan sumur minyak tersebut benar-benar akan di
laksanakan, maka banyak hal yang harus dipersiapkan termasuk salah satunya
investigasi geoteknik guna mengetahui karakteristik tanah di daerah tersebut
sebelum dilakukan pembangunan. Karakterisasi geoteknik perlu dilakukan agar
dapat dianalisis stabilitas dan perhitungan desain fondasi dan dapat diketahui
respon seismik lokasi, untuk merancang bangunan tahan gempa.
of Surface Waves/ MASW) merupakan metode nondestruktif dan noninvasif yang
dapat digunakan untuk investigasi geoteknik berdasarkan nilai  insitu. Metode
ini memanfaatkan sifat dispersi gelombang seismik permukaan yaitu gelombang
Rayleigh. Kurva dispersi yang diekstrak dari gelombang Rayleigh dalam domain
frekuensi-kecepatan fase, dapat diinversi untuk memperoleh beberapa parameter
fisik tanah seperti kecepatan gelombang S ( ), kecepatan gelombang P ( ),
densitas ( ), berserta ketebalannya (?). Dari semua parameter elastik material,
merupakan indikator terbaik yang menunjukkan kekakuan tanah. Kekakuan tanah
merupakan salah satu parameter material yang sangat dipertimbangkan pada tahap
awal konstruksi geoteknik (Park dan Richard, 2004). Nilai kekakuan ini
berhubungan langsung dengan kestabilan kapasitas struktural khususnya
berhubungan dengan kemungkinan bahaya gempa bumi.
Ada beberapa metode selain MASW yang dapat digunakan untuk
melakukan pengukuran   insitu, antara lain Crosshole, Downhole, Suspension
Logging, Spectral Analysis of Surface Wave (SASW), dan Refraction
Microtremor (ReMi). Sedangkan metode yang paling umum digunakan untuk
karakteristik geoteknik adalah Standart Penetration Testing (SPT).
Metode MASW dapat menjadi alternatif untuk investigasi geoteknik
karena mempunyai beberapa kelebihan antara lain biaya lebih terjangkau, lebih
mudah dalam akuisisi maupun pengolahan data dan dapat mengetahui nilai
kecepatan gelombang geser yang berhubungan langsung dengan modulus geser
elastik.
I.2  Tujuan Studi
1. Menentukan profil   di 3 area survei yaitu lokasi 1 di sekitar sumur
minyak Bapo yang berada di Formasi Ledok, lokasi 2 di Formasi Mundu
dan lokasi 3 di Formasi Lidah.
2. Mengetahui karakteristik tanah di 3 Formasi berbeda yaitu Formasi
Ledok, Formasi Mundu dan Formasi Lidah.
3. Mengetahui tebal lapisan lapuk/ kedalaman bedrock.
I.3  Batasan Masalah
Penelitian ini hanya terbatas pada 3 area survei yaitu lokasi 1 di Formasi
Ledok, lokasi 2 di Formasi Mundu dan lokasi 3 di Formasi Lidah. Ketiga lokasi
tersebut berada di sekitar Kecamatan Gabus. Kabupaten Grobogan, Jawa Tengah.
Metode yang digunakan untuk karakterisasi geoteknik ini adalah
Multichannel Analysis of Surface Wave (MASW). Jadi yang dimaksud dengan
karakterisasi geoteknik pada penelitian ini hanya terbatas pada hasil yang dapat
diperoleh dari metode MASW. Dari metode ini dapat diperoleh nilai  insitu
yang mecerminkan karakteristik tanah pada lokasi survei. Data profil  dianalisis
untuk  klasifikasi tingkat kekerasan tanah sebagai fungsi kedalaman, analisis
bedrock, dan sebagai informasi tambahan, dilakukan perhitungan nilai  dari
salah satu sampel data yang mewakili masing-masing area survei.
I.4  Waktu dan Tempat Penelitian
Penelitian ini dilakukan pada tanggal 5-12 Juli 2013 di area sekitar
Kecamatan Gabus, Kabupaten Grobogan, Jawa Tengah. Survei dilakukan di 3
lokasi yaitu lokasi 1 (lintasan 1, 2 dan 3) di area sekitar sumur minyak Bapo yang
berada di Formasi Ledok, lokasi 2 (lintasan 4 dan 5) di sebelah utara lokasi 1
sekitar 0,5 m yang berada di Formasi Mundu dan lokasi 3 (lintasan 6 dan 7)
berjarak 0,5 m di sebelah utara lokasi 2 yang berada di Formasi Lidah. Untuk
lebih jelasnya, peta lokasi survei dapat dilihat pada gambar 1.1.
Gambar 1. 1 Peta regional daerah penelitian

Dari survei investigasi geoteknik dengan metode Multichannel Analisys of
Surface Wave (MASW) di  Desa Bendoharjo, Kecamatan Gabus, Kabupaten
Grobogan, Jawa Tengah ini dapat disimpulkan:
1. Lokasi survei pertama yaitu yang berada diFormasi Ledok dengan litologi
dominan batupasir glaukonitan, diperoleh nilai  < 180 m/s (tanah lunak)
pada kedalamam 0 sampai sekitar 7-8 m, 180-360 m/s (tanah kaku) pada
kedalaman sekitar 8-16 m dan 360-736 m/s (tanah sangat padat) pada
kedalaman sekitar 16- 30 m.
2. Lokasi survei kedua yang berada pada Formasi Mundu dengan litologi
dominan napal, diperoleh nilai  < 180 m/s (tanah lunak) pada
kedalamam 0 sampai sekitar 8 m, 180-360 m/s (tanah kaku) pada
kedalaman 8-17 m dan 360-658 m/s (tanah sangat padat) pada kedalaman
sekitar 17 � 30 m.
3. Lokasi survei ketiga yaitu yang berada di Formasi Lidah dengan litologi
dominan batulempung, hanya terdiri dari dua jenis tanah yaitu tanah kelas
yaitu tanah lempung halus dan tanah kaku. Nilai  < 180 m/s (tanah
lunak) berada pada kedalamam 0 sampai sekitar 12 m dan nilai  180-360
m/s (tanah kaku) pada kedalaman di bawah 12 � 30 m.
4. Lokasi pertama dan kedua mempunyai karakteristik tanah yang hampir
sama, sedangkan lokasi ketiga mempunyai karakter tanah yang lebih lunak
dari kedua lainnya.
5. Kedalaman permukaan bedrock lokasi 1, berundulasi dengan variasi
kedalaman mulai dari 6 m sampai sekitar 14 m, lokasi 2 juga bervariasi
mulai dari kedalaman 6 sampai 14 m dan variasi topografi bedrock pada
lokasi ketiga relatif datar yaitu pada kedalaman sekitar 12 m.
1. Untuk menguji hasil pengolahan data dari survei MASW ini, dapat
dilakukan uji Standar Penetration Testing (SPT) pada beberapa lokasi
survei agar data yang diperoleh data yang lebih valid dan kedalaman
permukaan bedrock yang lebih dapat dipercaya.
2. Dibuat desain survei dalam bentuk grid sehingga dapat dilakukan
pemodelan dalam bentuk kubus (cube) dan dapat dilakukan pengkonturan
nilai .
3. Lebih diperhatikan lagi kontrol kualitasnya baik saat akuisisi maupun
pengolahan data agar diperoleh hasil yang mempunyai kualitas tinggi.

