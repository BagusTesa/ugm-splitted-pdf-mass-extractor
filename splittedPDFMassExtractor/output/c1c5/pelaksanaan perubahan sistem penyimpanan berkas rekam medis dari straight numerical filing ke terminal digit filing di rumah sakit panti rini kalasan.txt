A. Latar Belakang
Berdasarkan Undang-Undang No. 44 tahun 2009 tentang
rumah sakit, rumah sakit adalah institusi pelayanan kesehatan bagi
masyarakat dengan karakteristik tersendiri yang dipengaruhi oleh
perkembangan ilmu pengetahuan kesehatan, kemajuan teknologi
dan kehidupan sosial ekonomi masyarakat yang harus tetap
mampu meningkatkan pelayanan yang lebih bermutu dan
terjangkau oleh masyarakat agar terwujud derajat kesehatan yang
setinggi-tingginya.
Dalam memberikan pelayanan, semua data sosial dan
riwayat kesehatan pasien dicatat dalam suatu berkas yang bersifat
rahasia dan penting yang disebut Rekam Medis. Rekam medis
menurut  Permenkes Nomor 269/ Menkes/ per/ III/ 2008 adalah
suatu berkas yang berisikan catatan, dokumen tentang identitas
pasien, pemeriksaan, pengobatan, dan tindakan pelayanan lain
yang telah diberikan kepada pasien. Syarat dari sebuah dokumen
rekam medis sendiri adalah sebuah data yang akurat, informatif,
rasional,beralasan, dan dapat dipertanggung jawabkan.
Salah satu kegiatan yang dilaksanakan dalam rekam medis
adalah tentang sistem penyimpanan berkas. Menurut Budi (2011),
pengelolaan penyimpanan berkas rekam medis sangat penting
untuk dilakukan dalam suatu institusi pelayanan kesehatan karena
dapat mempermudah dan mempercepat ditemukan kembali berkas
rekam medis yang disimpan dalam rak penyimpanan, mudah dalam
pengambilan dari tempat penyimpanan, mudah pengembaliannya,
melindungi berkas rekam medis dari bahaya pencurian, bahaya
kerusakan fisik, kimiawi, dan biologi.
Sistem penyimpanan berkas rekam medis yang baik
merupakan salah satu kunci keberhasilan atau kebaikan
manajemen dari suatu pelayanan kesehatan, salah satunya
didukung dengan sistem yang baik. Menurut Depkes (1997), dari
beberapa jenis sistem penyimpanan rekam medis, sistem
penyimpanan angka akhir lebih mengajukan untuk dipilih karena
umum dipakai dan lebih mudah, efisien dan efektif. Namun, sistem
penyimpanan numerik dengan terminal digit filing memiliki
kekurangan diantaranya membutuhkan biaya awal yang lebih besar
karena harus menyiapkan rak penyimpanan terlebih dahulu.
Berdasarkan hasil studi pendahuluan di Rumah Sakit Panti
Rini Kalasan pada bulan Oktober 2012 diperoleh informasi bahwa
Rumah Sakit Panti Rini Kalasan dalam proses perubahan sistem
penyimpanan dari straight numerical filing ke terminal digit filing.
Perubahan sistem penyimpanan ini sudah dimulai pada bulan Juli
tahun 2012. Sebelum dilakukan perubahan sistem penyimpanan
telah diketahui bahwa rak penyimpanan yang ada tidak mencukupi
apabila sistem penyimpanan diubah menjadi terminal digit filing,
namun pihak Sub Seksi Rekam Medis Rumah Sakit Panti Rini
Kalasan tetap melakukan perubahan tanpa adanya penambahan
rak penyimpanan berkas rekam medis.
Perubahan sistem penyimpanan berkas rekam medis
dilakukan pada ruang filing yang sama. Pelaksanaan perubahan
sistem penyimpanan berkas rekam medis di Rumah Sakit Panti Rini
Kalasan dilakukan oleh 1 orang tenaga outsourcing. Petugas
tersebut merupakan pegawai di Rumah Sakit Panti Kalasan yang
bertugas  membantu di unit lain pada  hari kerja. Perubahan sistem
penyimpanan biasanya dilakukan pada shift pagi di hari rabu, jumat
dan sabtu apabila petugas tidak diperbantukan pada unit lain,
sehingga menyebabkan perubahan sistem penyimpanan
berlangsung lama. Dari uraian diatas maka penulis tertarik untuk
mengetahui pelaksanaan perubahan sistem penyimpanan berkas
rekam medis di Rumah Sakit Panti Rini Kalasan.
B. Rumusan Masalah
Berdasarkan latar belakang di atas, maka rumusan masalah
yang diambil peneliti adalah �Bagaimana Pelaksanaan Perubahan
Sistem Penyimpanan Berkas Rekam Medis dari Straight Numerical
Filing ke Terminal Digit Filing di Rumah Sakit Panti Rini Kalasan?�
C. Tujuan Penelitian
Untuk mengetahui pelaksanaan perubahan sistem
penyimpanan berkas rekam medis dari straight numerical filing
ke terminal digit filing di Rumah Sakit Panti Rini Kalasan.
a. Mengetahui alasan perubahan sistem penyimpanan berkas
rekam medis dari straight numerical filing ke terminal digit
filing di Rumah Sakit Panti Rini Kalasan.
b. Mengetahui pelaksanaan perubahan sistem penyimpanan
berkas rekam medis di Rumah Sakit Panti Rini Kalasan.
c. Mengetahui hambatan yang dihadapi dalam perubahan
sistem penyimpanan berkas rekam medis dari straight
numerical filing ke terminal digit filing di Rumah Sakit Panti
D. Manfaat Penelitian
a. Bagi Rumah Sakit
Hasil penelitian ini diharapkan dapat digunakan sebagai
masukan dalam melakukan penyimpanan berkas rekam
medis sehingga dapat meningkatkan kualitas pelayanan.
b. Bagi Peneliti
1) Dapat menerapkan teori yang diperoleh dari perkuliahan
ke dalam praktek yang sesungguhnya.
2) Menambah pengetahuan dan pengalaman tentang
permasalahan pada objek penelitian.
3) Mendapatkan pengalaman dalam upaya pengembangan
ilmu rekam medis di masa mendatang.
a. Bagi Institusi Pendidikan
Penelitian ini diharapkan mampu dijadikan sebagai tolok
ukur sejauh mana ilmu rekam medis diterapkan, terutama
mengenai sistem penyimpanan berkas rekam medis.
b. Bagi Peneliti Lain
Sebagai referensi untuk dasar atau acuan dalam
pengembangan penelitian lain.
E. Keaslian Penelitian
Menurut pengamatan peneliti, penelitian dengan menggunakan
judul �Pelaksanaan Perubahan Sistem Penyimpanan Berkas
Rekam Medis dari Straight Numerical Filing ke Terminal Digit Filing
di Rumah Sakit Panti Rini Kalasan� belum pernah dilakukan,
namun ada beberapa penelitian yang hampir sama, yaitu:
1. Wahyuni (2012) dengan judul �Evaluasi Perubahan Sistem
Penelitian Wahyuni (2012) bertujuan untuk mengevaluasi
perubahan sistem  penyimpanan dengan membandingkan
sistem penyimpanan yang baru secara nomor akhir (terminal
digit filing) dan sistem penyimpanan lama secara sistem nomor
langsung (straight numerical filing) untuk mengetahui sistem
penyimpanan yang lebih efisien.
Hasil penelitian ini adalah sistem penyimpanan secara
nomor akhir (terminal digit filing) lebih efisien, dapat diketahui
dari mudahnya mengambil berkas rekam medis, dapat
meminimalisir missfile, serta petugas penyimpanan yang
terbagi rata dalam bekerja sehingga proses penyimpanan lebih
cepat. Namun, hal ini didukung dengan sarana penyimpanan
yang lebih mahal dibandingkan sistem penyimpanan secara
nomor langsung (straight numerical filing). Adanya perubahan
penyimpanan secara nomor langsung (straight numerical filing)
ke nomor akhir (terminal digit filing) terdapat perbedaan pada
cara penyortiran berkas sebelum disimpan, penulisan
keterangan pada guide, perubahan pada prosedur
penyimpanan, perubahan folder baru, pertambahan rak
penyimpanan, serta perbedaan dalam membaca nomor rekam
medis pada proses pengembalian.
Persamaan penelitian Wahyuni (2012) dengan penelitian
penulis adalah jenis penelitian yang akan digunakan yaitu
deskriptif kualitatif dengan rancangan penelitian cross sectional.
Perbedaannya adalah penelitian penulis bertujuan untuk
mengetahui perubahan sistem penyimpanan dari straight
numerical filing ke terminal digit filing di rumah sakit sedangkan
penelitian ini bertujuan untuk mengevaluasi perubahan sistem
penyimpanan dengan membandingkan sistem penyimpanan
yang baru secara nomor akhir (terminal digit filing) dan sistem
penyimpanan lama secara sistem nomor langsung (straight
numerical filing).
2. Mugasari (2008) yang berjudul �Rancangan Perubahan Sistem
Penyimpanan dari Straight Numerical Filing menjadi Terminal
Digit Filing untuk Mengurangi Missfile di RSUD Kota
Penelitian ini bertujuan untuk menawarkan suatu
pemecahan masalah dengan perubahan sistem penyimpanan,
dengan mendeskripsikan hal-hal yang perlu dipersiapkan
sebelum melakukan perubahan dan mengetahui gambaran
kesiapan apabila menuju ke arah perubahan sistem
penyimpanan dari straight numerical filing menjadi terminal digit
filing di RSUD Kota Yogyakarta.
Hasil dari penelitian ini dalah hal- hal yang perlu
dipersiapkan dalam melakukan suatu perubahan sistem
penyimpanan dari Straight Numerical Filing menjadi Terminal
Digit Filing yaitu sarana (rak penyimpanan, ruang
penyimpanan, petunjuk, sampul/map rekam medis dengan
kode warna, petunjuk keluar/tracer dan kotak sortir), prasarana
(SOP/Protap, rapat koordinasi, waktu pelaksanaan serta SDM).
Gambaran kesiapan sarana dan prasarana yang ada diurusan
rekam medis RSUD Kota Yogyakarta belum mendukung dalam
menghadapi perubahan penyimpanan. SDM belum memiliki
cukup pengetahuan dan ketrampilan serta sebagian dari
mereka pernah mengikuti pelatihan sistem penyimpanan nomor
akhir.
Persamaan dari penelitian ini adalah kesamaan tema
mengenai sistem penyimpanan berdasarkan nomor. Selain itu
penelitian Mugasari (2008) merupakan penelitian deskriptif
dengan rancangan penelitian cross sectional, teknik
pengambilan data yang akan digunakan yaitu observasi dan
wawancara. Perbedaannya penelitian Mugasari (2008)
bertujuan menawarkan suatu pemecahan masalah dengan
perubahan sistem penyimpanan berupa Rancangan Perubahan
Sistem Penyimpanan dari Straight Numerical Filing menjadi
Terminal Digit Filing untuk Mengurangi Missfile di RSUD Kota
Yogyakarta. Sedangkan penelitian penulis bertujuan untuk
mengetahui pelaksanaan perubahan sistem penyimpanan
berkas rekam medis dari straight numerical filing ke terminal
digit filing di Rumah Sakit Panti Rini Kalasan.
3. Asa (2006) dengan judul � Perubahan Sistem Penomoran Seri
ke Sistem Penomoran Unit di RSUD Atambua, Kabupaten Belu,
Penelitian ini bertujuan untuk melakukan perubahan
sistem penomoran seri ke sistem penomoran unit, dengan
parameter hal-hal yang perlu dipersiapkan sebelum melakukan
perubahanan sistem dan tingkat kesiapan petugas (SDM) untuk
melakukan perubahan sistem.
Hasil dari penelitian ini adalah hal-hal yang perlu
dipersiapkan sebelum melakukan perubahan sistem
penomoran yaitu sarana, kebijakan dan prosedur tetap, SDM.
Kesiapan petugas rekam medis di RSUD Atambua sudah
cukup untuk melakukan perubahan sistem penomoran dari seri
ke unit. Pengembangan staff di RSUD Atambua terbentur dari
keterbatasan dana dari pihak rumah sakit.
Persamaan dari penelitian ini adalah kesamaan tema
mengenai perubahan. Selain itu jenis penelitian ini adalah
deskriptif kualitatif dengan rancangan penelitian cross sectional
dan pengambilan data dengan cara wawancara dan observasi.
Perbedaanya penelitian Asa (2006) membahas mengenai
sistem penomoron rekam medis pasien. Sedangkan penelitian
penulis membahas mengenai pelaksanaan perubahan sistem
penyimpanan berkas rekam medis.
F. Gambaran Umum Rumah Sakit
Rumah Sakit Panti Rini merupakan rumah sakit dengan tipe
pratama di bawah yayasan Panti Rapih Yogyakarta. Rumah Sakit
Panti Rini terletak di jalan Solo Km. 13,2 Tirtomartani, Kalasan,
Fax: (0274) 497206. Rumah sakit Panti Rini memiliki visi, misi,
falsafah, tujuan dan motto sebagai berikut:
Dalam kesadaran bahwa kami terpanggil untuk
meneruskan karya Allah dalam menyehatkan manusia
seutuhnya, untuk melaksanakan karya Allah dalam
menyehatkan manusia seutuhnya serta untuk melaksanakan
amanat penyembuhan Kristus kepada manusia yang adalah
ciptaan dan citra Allah, tanpa membedakan agama, budaya,
suku dan sosial. Sebab itu kami sebagai tim akan memberikan
pelayanan yang optimal dengan empati, cinta, cepat dan tepat
demi kepuasan yang kami layani. Kami juga peduli kepada
mereka yang kurang mampu dan bersesuaian hidup.
Organisasi kami bersifat sosio-ekonomi, di mana pendapatan
yang kami peroleh kami gunakan untuk kelangsungan hidup
dan pelayanan kami.
Dengan dijiwai semangat cinta kasih sesama, kami
berupaya menggalah hidup yang sehat secara menyeluruh
(holistik). Dalam pelayanan, kami mengutamakan kepuasan,
kesejahteraan dan produktifitas yang optimal bagi semua.
��Pendamping setia anda di kala sehat dan sakit�
��Melayani dengan cinta kasih dan bela rasa dalam ketekunan
dan kerjasama tim� bersumber dari:
Luk. 10:27    : Kasihilah sasamamu manusia seperti dirimu
sendiri
Mat. 11:29        : Belajarlah dari padaKu yang lemah lembut dan
rendah hati
Kor. 1:10     : Seia sekata sehati sepikir dalam memenuhi
hukum Kristus
Visi Sr.CB    : Cinta tanpa syarat dan bela rasa dari Kristus
yang tersalib
a. Meningkatkan derajat kesehatan masyarakat
b. Meningkatkan kesadaran dan kemampuan hidup sehat
masyarakat
c. Meningkatkan mutu pelayanan sesuai perkembangan
jaman dan teknologi
d. Mengupayakan kesejahteraan karyawan
Untuk memenuhi kebutuhan pelayanan kesehatan
masyarakat, Rumah sakit Panti Rini menyediakan fasilitas
pelayanan kesehatan, meliputi Rawat Jalan, Rawat Inap,
Gawat Darurat, dan Unit Penunjang.

A. Kesimpulan
1. Alasan perubahan sistem penyimpanan berkas rekam medis
dari straight numerical filing ke terminal digit filing di Rumah
Sakit Panti Rini Kalasan adalah untuk mengurangi missfile dan
memudahkan pencarian berkas rekam medis serta
memudahkan penghitungan rak penyimpanan.
2. Pelaksanaan perubahan dimulai setelah rapat internal rekam
medis pada tanggal 2 juli 2012. Pelaksanaan perubahan sistem
penyimpanan berkas rekam medis dilakukan oleh 1 orang
tenaga outsourcing. Perubahan sistem penyimpanan berkas
rekam medis tetap pada ruangan filing yang sama.
3. Hambatan yang dihadapi dalam perubahan sistem
penyimpanan berkas rekam medis dari straight numerical filing
ke terminal digit filing  di Rumah Sakit Panti Rini Kalasan yaitu:
a. Sumber daya manusia
Hanya terdapat 1 tenaga outsourcing yang
melaksanakan perubahan sistem penyimpanan dan tidak
adanya penanggung jawab di bagian penyimpanan berkas
rekam medis sehingga kerapian berkas rekam medis
kurang diperhatikan.
b. Sarana penyimpanan
Tidak ada penyekat antar berkas rekam medis dan
tidak semua rak penyimpanan dilengkapi dengan penunjuk
(guide).
B. Saran
1. Sebaiknya dilakukan pengkajian ulang mengenai beban kerja
petugas outsourcing yang melaksanakan perubahan sistem
penyimpanan untuk kegiatan perubahan selanjutnya.
2. Sebaiknya setiap petugas diberi tanggung jawab atas 1 rak
penyimpanan untuk menjaga kerapian berkas rekam medis.
3. Sebaiknya antar kelompok berkas rekam medis diberi
penyekat untuk menjaga agar berkas rekam medis tetap
dalam posisi berdiri dan tidak jatuh.
4. Sebaiknya setiap section diberi guide untuk membantu
petugas dalam pengambilan atau penyimpanan berkas rekam
medis.

