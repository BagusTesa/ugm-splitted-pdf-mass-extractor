Dewasa ini kebutuhan masyarakat akan kredit merupakan hal yang tidak
asing. Menyadari bahwa kegiatan kredit pada masyarakat umum semakin
meningkat, maka perlu adanya pengelolaan kredit secara akurat dari pihak
pemberi kredit atau yang biasa disebut kreditur.
Analisis resiko kredit adalah pihak yang menganalisis permohonan kredit
dari berbagai aspek yang terkait untuk menilai kelayakan usaha yang akan
dibiayai dengan kredit. Analisis tersebut antara lain meliputi aspek hukum,
lingkungan, keuangan, pemasaran, produksi, manajemen, ekonomi, dan
tersedianya jaminan yang cukup.
Peminjam yang gagal mengembalikan pinjaman, dapat mengakibatkan
kerugian pada kreditur, yang memungkinkan timbulnya micro systematic risk,
yaitu ambruknya industri lembaga keuangan (kreditur) secara keseluruhan. Untuk
menghindari kondisi tersebut, maka pihak kreditur sendiri perlu mengantisipasi
dengan menerapkan early warning system, untuk mengatasi risiko kredit.
Kejadian debitur gagal membayar atau menunggak pembayaran diistilahkan
�default�. Default merupakan kejadian yang sangat merugikan kreditur karena di
satu sisi kreditur  harus mengembalikan dana peminjam yang �dititipkan� kepada
kreditur, namun di sisi lain dana peminjam yang notabene sudah diputar oleh
kreditur untuk disalurkan kepada debitur ternyata tidak bisa kembali seutuhnya
kepada kreditur karena ada kreditur yang default. Sehingga diperlukan unit
manajemen risiko kredit mikro untuk meminimalisasi risiko kredit mikro yang
dihadapi kreditur.
Banyak cara yang dapat dilakukan untuk mencegah kredit macet. Salah
satunya kita dapat mengelompokkan karakteristik debitur. Kita dapat mengetahui
debitur yang mengajukan pinjaman dapat terbagi kedalam berapa kelompok dan
karakteristik debitur yang mengajukan pinjaman pada setiap kelompok yang
terbentuk dilihat dari beberapa variabel. Melalui data yang ada penulis ingin
mengetahui, apakah durasi peminjaman, besarnya pinjaman, tingkat angsuran,
lama tinggal di hunian, usia, jumlah kredit di bank, dan jumlah tanggungan
signifikan membedakan antar kelompok yang terbentuk, profil serta
kecenderungan-kecenderungan dari masing-masing kelompok yang terbentuk dan
posisi masing-masing objek terhadap objek lainnya dari segmen yang terbentuk
menggunakan metode analisis cluster x-means.
Batasan masalah merupakan hal yang sangat penting dilakukan dalam suatu
penulisan agar tidak terjadi penyimpangan dari tujuan yang ingin dicapai. Dalam
skripsi ini difokuskan pada mengelompokkan karakteristik debitur menggunakan
x-means cluster. Algoritma x-means cluster digunakan untuk menentukan jumlah
cluster optimal menggunakan kriteria BIC dan menggunakan analisis regresi
logistik untuk menghitung probabilitas default tiap cluster.
Penelitian ini bertujuan untuk:
1. Sebagai salah satu syarat untuk mendapatkan gelar S1 pada Program Studi
2. Menentukan jumlah cluster optimal karakteristik debitur melalui data kredit
German untuk jumlah cluster lebih dari dua, dimana karakteristik debitur
diukur melalui pinjaman, tingkat angsuran, lama tinggal di hunian, usia,
jumlah kredit di bank, dan jumlah tanggungan.
3. Melihat profil serta kecenderungan-kecenderungan dari masing-masing
cluster yang terbentuk.
Hasil penelitian ini diharapkan dapat berguna:
1. Mengaplikasikan ilmu statistika untuk clustering terutama  k-means cluster.
2. Memperkenalkan metode pengembangan k-means cluster yaitu x-means.
3. Menambah ilmu pengetahuan.
Metode penulisan yang digunakan penulis adalah studi literatur yang
diperoleh dari perpustakaan, jurnal-jurnal ilmiah dan sumber-sumber lain yang
diperoleh dari internet. Penulis dalam menyelesaikan penelitian ini menggunakan
bantuan software R 3.0.1, SPSS 15.0, dan Microsoft Excel 2010. Data yang
diambil merupakan data sekunder dari internet yang dapat
dipertanggungjawabkan.
K-means clustr adalah salah satu metode clustering yang sering digunakan
untuk mengelompokkan sekumpulan objek kedalam beberapa cluster sehingga
kemiripan objek dalam kelompok lain akan bernilai minimal dengan jumlah
cluster yang telah ditentukan sebanyak �k�. Menurut Pelleg dan Moore (2000) k-
means cluster memiliki beberapa kekurangan yaitu menggunakan komputasi yang
sangat sederhana dan nilai k harus diinputkan oleh user. Karena algoritma ini
sederhana, sehingga untuk suatu waktu t, k-means cluster mempunyai
kompleksitas ? ?O kN  untuk k cluster yang ditentukan dan N jumlah record.
Untuk menyelesaikan permasalahan tersebut, Pelleg dan Moore memperkenalkan
metode clustering yang merupakan pengembangan dari k-means cluster yaitu x-
means cluster.
X-means cluster diperkenalkan pertama kali oleh Pelleg dan Moore (2000)
dalam jurnal yang berjudul �X-means: Extending K-means with Efficient
Estimation of the Number of Clusters�. Algoritma geometrik �blacklisting�
digunakan sebagai algoritma pencarian pusat cluster, dimana algoritma ini hanya
melakukan perhitungan pada pusat yang memang dipertimbangkan sebagai pusat
cluster untuk daerah tertentu. X-means membentuk jumlah cluster awal
menggunakan k-means cluster. Kemudian setiap cluster yang terbentuk dibagi
menggunakan k-means cluster. Pemilihan model yang digunakan berdasarkan
kriteria BIC, jika BIC model yang terbagi menjadi �k� cluster maka lebih baik
cluster akan terbagi. Proses ini berulang hingga tidak ada lagi cluster yang dapat
dibagi.
Ishioka, Tsunenori (2000) membahas x-means cluster menggunakan  metode
pembagian k-means cluster dengan k sebesar 2 dan pemilihan model
menggunakan BIC lebih baik daripada AIC. Kemudian dalam penelitian
selanjutnya Ishioka, Tsunenori (2005) menjelaskan bahwa x-means cluster yang
menggabungkan cluster yang tidak harus terbagi lebih baik dari pada yang tidak
dan kompleksitas algoritma x-means cluster lebih kecil dari pada k-means cluster
yaitu sebesar ? ?logO N k . Lafore, Robert (1999) menyebutkan bahwa algoritma
dengan komplektisitas ? ?logO N  lebih baik daripada ? ?O N . Sehingga dapat
disimpulkan bahwa algoritma x-means cluster lebih baik daripada k-means
cluster.
Penelitian dalam credit scoring terus menerus mengalami perkembangan.
Data yang digunakan adalah data kredit: German Credit. Penulis
mengklasifikasikan karakteristik debitur berdasarkan informasi durasi
peminjaman, jumlah kredit, tingkat angsuran, lama tinggal di hunian saat ini, usia,
jumlah kredit yang ada di bank saat ini dan jumlah orang yang menjadi tanggung
jawab debitur menggunakan x-means cluster dengan pembagian k-means cluster,
k sebesar 2 dan jumlah cluster awal sebanyak 2.
Penelitian dalam clustering terus menerus mengalami perkembangan,
beberapa penelitian mengembangkan clustering dengan menggabungkan beberapa
metode yang diharapkan akan memaksimalkan hasil dan menutupi kekurangan
masing-masing metode. Berikut adalah penjelasan singkat penelitian-penelitian
sebelumnya tentang clustering.
Ayuningtyas (2009) membahas credit scoring untuk kredit konsumtif bank
menggunakan regresi logistik ganda dan analisis diskriminan. Data yang
digunakan adalah data dari BPR di Yogyakarta. Hasil akhir dari penelitian ini
menunjukkan bahwa regresi logistik ganda relatif lebih baik daripada analisis
diskriminan linier.
Chalisa (2009) membahas permodelan credit scoring untuk kredit konsumtif
ban menggunakan algoritma CHAID (Chi-Square Automatic Intereaction
Detection) dan CART (Classification and Regression Trees). Data yang
digunakan adalah data dari BPR di Yogyakarta. Berdasarkan nilai estimasi,
standar error, tabel klasifikasi, dan aturan klasifikasi yang terbentuk menunjukkan
bahwa algoritma CHAID relatif lebih baik daripada CART dalam memodelkan
credit scoring.
Putri (2010) membahas analisis credit coring menggunakan metode k-
Nearest Neighbors. Studi kasus dalam penelitian ini menggunakan data BPR
Tasikmalaya. Metode ini merupakan metode klasifikasi dengan mengukur
kedekatan suatu kasus baru dengan kasus lama yang tersimpan dalam database.
Hasil akhir analisis menunjukkan probabilitas kesalahan untuk aturan k-Nearest
Neighbors masih dianggap rendah dan ketepatan prediksi masih dianggap tinggi.
Ramadhan (2010) membahas credit scoring menggunakan metode decision
tree dengan algoritma QUEST (Quick, Unbiased, Efficient Statistical Tree).
Penelitian ini menggunakan data salah satu BPR di Tasikmalaya sebagai studi
kasusnya. Metode pada penelitian ini merupakan perpaduan antara statistika,
machine learning, database, dan visualisasi. Penelitian ini juga membahas
perbandingan decision tree antara algoritma QUEST dan CART. Kesimpulan
akhir yang didapat adalah metode QUEST dan CART memiliki tingkat kesalahan
pembentukan model yang berbeda. Dibandingkan dengan QUEST, model
classification rule yang dibentuk oleh CART memiliki lebih banyak risiko
kesalahan data mengkualifikasi objek baru.
Pada penelitian ini penulis membahas credit scoring menggunakan
penggabungan x-means cluster dan regresi logistik. Data yang digunakan adalah
data kredit yang diambil di internet dan dapat dipertanggungjawabkan yaitu
German Credit. Pada x-means cluster akan dibentuk kelompok-kelompok
berdasarkan kemiripannya dan regresi logistik yang akan menentukan profil atau
probabilitas default kelompok-kelompok yang terbentuk dari x-means cluster.
Sistematika penulisan yang digunakan dalam penelitian ini adalah sebagai
berikut:
Bab ini berisi tentang latar belakang masalah, tujuan penelitian, manfaat
penelitian, batasan masalah, metode penelitian, sistematika penulisan laporan, dan
tinjauan pustaka.
Bab ini membahas tentang landasan teori yang relevan dengan penelitian ini
yaitu aljabar matriks, matriks multivariat, distribusi normal multivariat, fungsi
likelihood, dan BIC.
Bab ini membahas tentang landasan teori yang relevan dengan penelitian ini
yaitu distribusi normal multivariat yang terdiri dari fungsi likelihood, rata-rata dan
kovariansi sampel, estimasi parameter dan moment generating function,
clustering, k-means cluster yang terdiri dari pengertian k-means cluster dan
algoritma k-means cluster, dan x-means cluster yang terdiri dari pengertian x-
means cluster dan algoritma x-means cluster.
Bab ini berisi analisis cluster menggunakan x-means dimana probabilitas
default tiap cluster dihitung menggunakan regresi logistik serta pembahasan hasil
yang didapat.
Bab ini berisi kesimpulan dari hasil analisis dan saran-saran untuk penelitian
selanjutnya.

Setelah dilakukan analisis cluster pada data menggunakan algoritma x-
means cluster, diperoleh beberapa kesimpulan yaitu:
1. X-means cluster menggunakan data standarisasi menghasilkan cluster
optimal untuk karakteristik debitur sebesar 5 cluster berdasarkan informasi
durasi peminjaman, jumlah kredit, tingkat angsuran, lama tinggal di hunian
saat ini, usia, jumlah kredit yang ada di bank saat ini dan jumlah orang yang
menjadi tanggung jawab debitur.
2. Data dengan satuan yang bervariasi, perlu dilakukan standarisasi agar
menghasilkan jumlah cluster lebih tepat.
3. Cluster 1 adalah debitur yang relatif cukup baik dan berusia lanjut atau tua
dengan jumlah kredit yang rendah dan pembayaran jangka pendek.
4. Cluster 2 adalah debitur yang relatif perlu dipertimbangkan dan berusia
masih muda dengan jumlah kredit yang tinggi dan jangka pembayaran
cukup panjang.
5. Cluster 3 adalah debitur yang sangat dipertimbangkan dan relatif berusia
dewasa dengan jumlah kredit yang cukup rendah dan jangka pembayaran
cukup panjang.
6. Cluster 4 adalah debitur yang relatif berusia baik dewasa dengan jumlah
kredit yang cukup tinggi dan pembayaran jangka pendek.
7. Cluster 5 adalah debitur yang relatif perlu dipertimbangkan dan berusia
dewasa dengan jumlah kredit yang tinggi dan pembayaran jangka panjang.
Perkembangan penelitian tentang segmentasi terus berlangsung. Sebagai
bahan perbandingan dengan algoritma x-means dan metode-metode yang telah
ada sebelumnya, diharapkan dalam penelitian selanjutnya dapat dikembangkan
segmentasi x-means untuk mixed data dan  x-means dengan pembentukan cluster
awal menggunakan k-means fast. Terkait data penelitian yang menggunakan data
kredit, diharapkan dalam penelitian selanjutnya dapat dikembangkan credit
scoring berdasarkan cluster yang terbentuk dengan menggunakan algoritma C4.5,
(genetic algorithm), dan neuro-fuzzy syste.

