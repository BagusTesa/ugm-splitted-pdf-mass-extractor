1.1. Latar Belakang dan Permasalahan
Peningkatan  perekonomian yang terjadi saat ini memicu masyarakat
untuk membuka usaha di berbagai sektor. Upaya ini dilakukan untuk
meningkatkan taraf hidup masyarakat. Salah satu usaha yang dilakukan oleh
masyarakat adalah bengkel mandiri.
Bengkel merupakan salah satu jenis usaha yang bergerak dalam bidang
jasa perbaikan kendaraan bermotor. Semakin pesat perkembangan jenis
motor mengharuskan perawatan kendaraan bermotor akan berbeda untuk
setiap kendaraan bermotor. Tidak heran banyak dijumpai bengkel yang
menyediakan berbagai macam jenis perawatan (service) motor. Selain
perawatan yang berbeda suku cadang yang berada di dalam kendaraan
bermotor juga berbeda-beda.
Pemberian layanan perawatan service kendaraan bermotor yang baik
akan meningkatkan minat pelanggan untuk melakukan perbaikan di bengkel
tersebut. Seiring dengan peningkatan jumlah pelanggan yang datang telah
memicu bengkel untuk melakukan peningkatan pelayanan demi menjaga
kelancaran operasional kerja terutama dalam bidang administrasi dan rekap
data. Setiap bengkel memiliki rekap data bengkel, tetapi seiring
berkembangnya zaman, bengkel membutuhkan media penyimpanan data
yang lebih besar.
Banyaknya pelanggan yang datang ke bengkel untuk melakukan
perawatan membuat bengkel memberlakukan sistem antrian. Sistem antrian
ini digunakan untuk mengatur arus pelanggan yang datang dan pelanggan
yang dilayani. Bagian administrasi memerlukan data pelanggan dan data
kendaraan bermotor ketika membuat antrian pelanggan. Berbagai macam
profesi yang dimiliki pelanggan telah menyebabkan pelanggan sering
meninggalkan kendaraan bermotornya setelah melakukan antrian.
Selanjutnya, bagian administrasi akan menginformasikan kepada pelanggan
melalui telepon bahwa motor telah selesai diperbaiki. Banyaknya jenis
profesi pelanggan menyebabkan pihak bengkel merasa kesulitan ketika
memberi informasi kepada pelanggan. Hal ini terjadi karena ada beberapa
telepon yang tidak mendapat respon dari pelanggan. Terkait dengan berbagai
macam profesi pelanggan sehingga diperlukan informasi yang cepat dan
akurat. Media penyampaian informasi ini bisa melalui pesan singkat.
Implementasi penyampaian informasi melalui pesan singkat salah satunya
adalah dengan menggunakan SMS (Short Message Service). Pengiriman
informasi melalui SMS dapat diterima di semua jenis telepon selular.
Telepon selular ini yang kemudian akan menjadi media penghubung
informasi antara bengkel dan pelanggan.
Berdasarkan uraian di atas perlu dibangun Sistem Informasi Jasa
Bengkel berbasis Web dengan fasilitas SMS Gateway.  Sistem ini bertujuan
untuk mengelola data pelanggan dan motor.
Berdasarkan uraian latar belakang di atas, maka rumusan masalah
dalam penelitian tugas akhir ini adalah bagaimana membangun sebuah
sistem informasi jasa bengkel yang memiliki fasilitas SMS Gateway.
Tujuan dari penelitian ini adalah membangun sistem informasi jasa
bengkel dengan fasilitas pesan singkat (SMS).
Sistem yang akan dibuat di dalam penelitian ini memiliki batasan:
1. Sistem ini hanya sebatas melayani kegiatan yang berkaitan dengan
pemesanan, pengolahan data bengkel, riwayat data motor,
pelaporan serta informasi pelanggan melalui via pesan singkat /
2. Sistem informasi ini dibangun untuk pelayanan jasa bengkel
kendaraan bermotor roda dua.
3. Sistem ini tidak membahas tentang penjualan sukucadang dan
sistem penggajian karyawan.
Manfaat yang akan didapatkan dari pembuatan sistem ini adalah :
1. Mempermudah dalam membuat laporan bengkel.
2. Mempermudah dalam melakukan rekap data pelanggan.
3. Meningkatkan pelayanan bengkel terhadap pelanggan.
4.  Mempermudah pihak bengkel untuk menyampaikan informasi
kepada pelanggan.
5. Mampu membantu pelanggan untuk membantu menepati jadwal
perbaikan kendaraan bermotor.
Dalam penyusunan Tugas Akhir ini terdapat beberapa metode yang
digunakan untuk mengumpulkan data, antara lain:
Metode yang digunakan dalam pengumpulan data yang berkaitan
dengan penyusunan laporan serta pembuatan sistem ini antara lain:
a. Observasi
Observasi dilakukan dengan pengamatan langsung terhadap
objek  yang  diteliti yang ada di bengkel Ahass Buana Sari
Motor. Metode ini digunakan untuk memperoleh data yang
bersifat objektif.
b. Wawancara
Wawancara dilakukan kepada pihak-pihak yang berwewenang
(seperti : manager dan bagian administrasi) untuk mendapatkan
informasi yang dibutuhkan dalam analisis sistem.
c. Studi Literatur
Studi literatur dilakukan berdasarkan dari beberapa buku, ,
tugas akhir, situs web, e-book, modul praktikum dan referensi
yang digunakan untuk pembanding terhadap kasus yang
ditemui.
Pada tahap ini dibuat rincian kebutuhan sistem berdasarkan masalah
yang ada. Rincian kebutuhan yang dibuat didasarkan pada analisis
kebutuhan non-fungsional dan analisis kebutuhan fungsional. Analisis
fungsional meliputi hak akses user dan  kemampuan sistem. Analisis
non-fungsional meliputi perangkat keras (hardware), perangkat
lunak(software).
Dari hasil analisis sistem akan diimplementasikan kedalam bentuk
yang mudah dimengerti oleh pengguna berupa  perancangan antarmuka,
perancangan proses dan perancangan basisdata. Perancangan desain
antarmuka dibuat sesuai dengan analisis yang telah dilakukan. Proses
yang terjadi di dalam sistem dibuat berdasarkan perancangan proses.
Perancangan basisdata akan dilakukan berdasarkan dari analisis sistem.
Pada tahap sebelumnya telah dilakukan tahap analisis sistem dan
perancangan sistem. Setelah kedua tahap tersebut dilalui maka tahap
selanjutnya adalah implementasi. Tahap implementasi menggunakan
bahasa pemrograman HTML, PHP, CSS, Javascript dan CodeIgniter.
Database Management Sistem yang digunakan sistem ini menggunakan
Sistem informasi bengkel yang telah dibangun akan melalui tahap
uji coba setelah proses pembuatan sistem selesai. Uji coba dilakukan
dengan melakukan testing oleh pihak-pihak yang akan melakukan
sistem ini. Selain itu, uji coba sms gateway  dilakukan oleh responden
dengan mengirim pesan singkat ke salah satu nomor yang telah
ditetapkan.
Penulisan tugas akhir ini akan dibagi menjadi 6 bab, antara lain :
Bab ini berisi latar belakang masalah, rumusan masalah, tujuan
penelitain, batasan masalah manfaat penelitian, metodologi penelitian,
dan sistematika penelitian.
Bab ini  berisi uraian sistematis tentang informasi hasil penelitian yang
disajikan dalam pustaka dan menghubungkannya dengan masalah yang
diteliti.
Berisi teori, pendapat, prinsip, dan sumber-sumber lain yang dapat
dipertanggungjawabkan secara ilmiah dan dapat dipergunakan sebagai
perbandingan atau acuan dalam pembahasan masalah.
Bab ini berisi analisis kebutuhan dalam membangun perangkat lunak
tehadap spesifikasi sistem yang mencakup analisis prosedur yang
berjalan, analisis pengguna dan analisis basis data, selain itu terdapat
juga perancangan antarmuka untuk aplikasi akan dibangun sesuai
dengan hasil analisis yang telah dibuat.
Pada  bab ini menjelaskan tentang penerapan analisis sistem dan
perancangan yang akan menghasil antarmuka yang disertai kode
program.
Bab ini terdiri dari kesimpulan dan saran yang berisikan hal-hal
terpenting yang dibahas .Kemudian hal-hal terpenting tersebut dijadikan
kesimpulan. Bab ini juga berisi saran-saran yang memungkinkan
dilakukannya pengembangan perangkat lunak selanjutnya.
Daftar pustaka berisi informasi buku, tugas akhir dan sumber lainnya
yang digunakan sebagai acuan ketika merancang laporan tugas akhir ini.

Sistem informasi jasa bengkel dengan fasilitas sms gateway telah berhasil
dibangun dan dapat diambil kesimpulan sebagai berikut:
1. Sistem yang  telah berhasil dibangun memiliki fasilitas sms gateway.
Sistem yang dibangun mampu menangani pencatatan data transaksi,
pengiriman pesan singkat kepada pelanggan untuk melakukan
perbaikan pada waktu yang telah ditentukan, pemesanan(booking)
melalui pesan singkat oleh pelanggan.
2. Sistem ini mampu memberikan informasi laporan keuangan dan
informasi grafik pelanggan untuk waktu tertentu.
Sistem Informasi Jasa Bengkel dengan Fasilitas SMS Gateway yang telah
dibuat, penulis memberikan beberapa saran untuk pengembangan selanjutnya
diantaranya :
1. Sistem memiliki fitur penentuan waktu yang lebih detail ketika
pelanggan melakukan pemesanan melalui pesan singkat.
2. Agar memudahkan pegawai dalam melakukan pengecekan daftar
booking maka sistem perlu diberi notifikasi setiap ada pesan baru.

