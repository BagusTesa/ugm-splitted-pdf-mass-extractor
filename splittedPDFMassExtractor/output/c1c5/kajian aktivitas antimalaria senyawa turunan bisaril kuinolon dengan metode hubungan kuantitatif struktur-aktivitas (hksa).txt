Malaria merupakan penyakit endemis dari 104 negara di seluruh dunia,
termasuk Indonesia. Infeksi malaria terjadi sebanyak 300-500 juta kasus tiap
tahunnya (WHO, 2012). Wilayah Indonesia yang endemis malaria adalah 338
kabupaten/kota terutama pada kawasan Indonesia Timur. Penduduk yang  tinggal
pada daerah endemis diperkirakan 70% beresiko tertular malaria (Depkes, 2008).
Malaria disebabkan oleh infeksi protozoa dari genus Plasmodium. Infeksi malaria
sebagian besar disebabkan oleh P. falciparum dan mengakibatkan kematian
Senyawa antimalaria golongan kuinolin telah lama digunakan untuk
pengobatan penyakit malaria seperti 4-aminokuinolin (kuinin, klorokuin), 8-
aminokuinolin (primakuin) dan fluorokuinolon. Namun, mutasi pada P.
falciparum menyebabkan resistansi spesies ini terhadap obat-obat tersebut
(Farooq dan Mahajan, 2004). Resistansi menyebabkan efektivitas obat malaria
menjadi menurun sehingga diperlukan pencarian senyawa antimalaria baru yang
lebih efektif. Senyawa antimalaria ini haruslah memiliki mekanisme yang berbeda
dari obat konvensional sehingga resistansi akan sulit terjadi. Senyawa tersebut
diantaranya adalah Aurachin B-D, 1-hidroksi-2-dodesil-4(1H)kuinolon (HDQ)
dan turunan bisaril kuinolon (Pidathala et al., 2012; Rodrigues et al., 2010; Saleh
et al., 2007). Senyawa ini menunjukkan aktivitas antimalaria dengan mekanisme
penghambatan pada enzim PfNDH2, yaitu enzim yang berperan dalam sistem
transpor elektron pada mitokondria P. falciparum.
Senyawa turunan bisaril kuinolon memiliki aktivitas antimalaria yang
sangat tinggi dengan nilai IC50 dalam konsentrasi nanomolar. Senyawa ini
berpotensi sebagai antimalaria baru yang lebih efektif dengan mekanisme
penghambatan yang berbeda dibandingkan dengan obat malaria yang telah ada.
Oleh karena itu, pada penelitian ini akan dilakukan desain dengan cara modifikasi
substituen pada senyawa turunan bisaril kuinolon. Namun, identifikasi pusat aktif
senyawa turunan bisaril kuinolon dengan menggunakan metode HKSA
(Hubungan Kuantitatif Struktur-Aktivitas) perlu dilakukan terlebih dahulu.
Deskriptor yang akan digunakan adalah deskriptor elektronik (muatan bersih
atom, polarisabilitas, momen dipol, EHOMO dan ELUMO) dan deskriptor molekuler
(log P dan luas permukaan). Deskriptor-deskriptor tersebut telah dilaporkan
berperan dalam penentuan aktivitas antimalaria (Amanatie et al., 2005; Tahir et
al., 2010). Perancangan senyawa obat dengan metode HKSA akan menghemat
waktu dan biaya untuk mencari obat baru. Desain senyawa turunan bisaril
kuinolon akan dilakukan dengan mengganti substituen yang ada dengan gugus
isosteriknya dengan tujuan meningkatkan aktivitas antimalarianya.
Penelitian ini bertujuan untuk :
1. Memprediksi aktivitas antimalaria dan aktivitas inhibisi terhadap PfNDH2
dari senyawa analog turunan bisaril kuinolon dengan metode HKSA yang
melibatkan deskriptor elektronik dan molekuler. Deskriptor elektronik adalah
muatan bersih atom, polarisabilitas, momen dipol, EHOMO dan ELUMO, ?E.
Deskriptor molekuler adalah luas permukaan, volume, log P, refraktivitas
molar dan berat molekul.
2. Merancang senyawa analog turunan bisaril kuinolon dengan mengganti
substituennya dengan gugus isosterik untuk meningkatkan aktivitas
antimalaria dan aktivitas inhibisi terhadap PfNDH2 teoritisnya.
Penelitian ini diharapkan dapat bermanfaat sebagai :
1. Rekomendasi senyawa turunan bisaril kuinolon dengan aktivitas antimalaria
yang lebih tinggi.
2. Sumber informasi ilmiah dalam kajian HKSA untuk senyawa-senyawa
antimalaria.

Berdasarkan hasil penelitian dan pembahasan pada bab sebelumnya, dapat
diambil beberapa kesimpulan sebagai berikut :
1. Aktivitas antimalaria dan aktivitas inhibisi terhadap PfNDH2senyawa analog
turunan bisaril kuinolon dapat diprediksi dengan metode linear HKSA yang
melibatkan deskriptor elektronik dan molekul. Model persamaan MLR HKSA
terbaik untuk kajian aktivitas antimalaria adalah:
pIC50 = 2,064 � 0,003 (V) � 11,338 (qC5)  � 2,068 (qC6) + 2,036 (qC7)
- 59,693 (qN1) + 49,063 (qO11)
n = 27; r2 = 0,751; SEE = 0,301; Fhitung/Ftabel = 3,859
Adapun aktivitas inhibisi senyawa turunan bisaril kuinolon terhadap enzim
PfNDH2 dapat dimodelkan dengan metode JST dengan arsitektur 3-4-1
dengan r2 = 0,834 dan PRESS 0,257. Deskriptor yang terlibat dalam model
JST adalah ?, qN1 dan qC22.
2. Model persamaan HKSA terbaik digunakan untuk merancang senyawa usulan
dan memprediksi nilai aktivitas antimalaria dari senyawa usulan tersebut.
Senyawa usulan terbaik adalah (6,7-difluoro-2-(3-(4-(trifluorometoksi)
fenoksi)fenil)kuinolin-4(1H)-on) dengan aktivitas antimalaria teoritis terhadap
P. falciparum sebesar 3,387 nM dan aktivitas inhibisi terhadap PfNDH2
sebesar 7,588 nM.
Adapun saran untuk dapat dilakukan adalah metode ab initio dan DFT
dalam perhitungan deskriptor senyawa turunan bisaril kuinolon, sehingga bisa
dibandingkan analisis HKSA dengan metode semiempiris yang telah dilakukan
dalam penelitian ini. Selain itu, analisis HKSA 3D juga disarankan untuk dapat
dilakukan, sehingga dapat memprediksi aktivitas inhibisi terhadap PfNDH2
dengan hasil yang lebih akurat. Senyawa usulan terbaik dari penelitian ini
diharapkan dapat disintesis dan diuji secara eksperimen untuk mendapatkan
senyawa antimalaria yang lebih baik.

