Analisis regresi merupakan salah satu alat statistika yang banyak
digunakan untuk mengetahui hubungan antara sepasang variabel atau lebih.
Misalkan X adalah variabel prediktor dan Y adalah variabel respon untuk n
pengamatan berpasangan { ?? ,?? }?=1
? , maka hubungan linier antara variabel
prediktor dan variabel respon tersebut dapat dinyatakan sebagai berikut :
?? = ? ?? + ?? , ? = 1,2,� ,?
Dengan ??  adalah error yang diasumsikan independen dengan mean nol dan
variansi ?2 , serta ? ??  adalah fungsi regresi atau kurva regresi.
Tujuan analisa regresi adalah untuk menentukan hampiran untuk fungsi
m(.). Pendekatan yang digunakan untuk mengestimasi fungsi regresi ada dua jenis
yaitu pendekatan model regresi parametrik dan pendekatan model regresi
nonparametrik. Pendekatan yang paling umum dan seringkali digunakan  adalah
pendekatan model regresi parametrik. Pendekatan model regresi parametrik
digunakan bila fungsi m(.) diketahui dari informasi sebelumnya,berdasarkan teori
ataupun pengalaman masa lalu, sedangkan pendekatan model regresi
nonparametrik dilakukan berdasarkan pendekatan yang tidak terikat dengan
asumsi bentuk kurva regresi tertentu, yang memberikan fleksibilitas yang lebih
besar dalam bentuk yang mungkin dari kurva regresi atau fungsi regresi m(.).
Dalam regresi nonparametrik tidak ada asumsi tentang bentuk fungsi m(.).
Membuat asumsi tentang bentuk kurva regresi m tidak mudah dilakukan, sehingga
pendekatan yang tepat digunakan adalah model regresi nonparametrik. Umumnya
fungsi regresi hanya diasumsikan termuat dalam suatu ruang fungsi yang
berdimensi tak hingga. Untuk mengkonstruksi model regresinya dipilih fungsi
yang sesuai dimana fungsi regresinya diyakini termasuk di dalamnya. Pemilihan
ruang fungsi ini biasanya dimotivasi oleh sifat kelicinan yang diasumsikan
dimiliki oleh fungsi regresi.
Estimasi fungsi regresi nonparametrik dilakukan berdasarkan data
pengamatan dengan  menggunakan teknik smoothing. Ada beberapa teknik
smoothing dalam regresi nonparametrik antara lain histogram, estimator spline,
estimator kernel, deret fourier, deret orthogonal, k-NN, dan estimator wavelet.
Diantara metode-metode pendekatan nonparametrik tersebut, estimator spline dan
estimator kernel merupakan metode yang sering digunakan. Kedua metode
tersebut memiliki keunggulan masing-masing. Estimator kernel memiliki bentuk
yang lebih fleksibel dan perhitungan matematisnya mudah disesuaikan.
Sedangkan estimator spline dapat menyesuaikan diri secara efektif terhadap data
tersebut, sehingga didapatkan hasil yang mendekati kebenaran.
Dalam penelitian  ini, pembatasan  masalah sangat diperlukan untuk
menjamin keabsahan dalam mengambil kesimpulan yang diperoleh. Agar tidak
terjadi penyimpangan pada tujuan semula dan pemecahan masalah lebih
terkonsentrasi. Berdasarkan pada latar belakang masalah dan kajian-kajian
pendukung lain maka penulis dapat memberikan rumusan dan batasan masalah
sehubungan dengan kompleksnya masalah yang akan muncul dalam pembahasan.
Secara garis besar akan dibahas tentang dua metode regresi nonparametrik yaitu
metode spline dan metode kernel. Kedua metode tersebut akan dibandingkan
dalam mengestimasi data pertumbuhan balita menggunakan software R, S-Plus,
dan Minitab.
Berdasarkan latar belakang masalah dan pembatasan masalah di atas,
maka penelitian dan penulisan skripsi ini dilaksanakan dengan tujuan untuk :
1. Mempelajari aplikasi metode spline dan kernel pada contoh data
berdasarkan sumber literature yang diperoleh.
2. Mengestimasi kurva regresi data pertumbuhan balita dengan
menggunakan metode spline dan kernel.
3. Membandingkan kedua metode yaitu metode spline dan kernel dalam
mengestimasi data pertumbuhan balita, kemudian disimpulkan mana
metode yang lebih baik.
Penulisan tugas akhir ini diharapakan bisa memberikan manfaat
diantaranya :
1. Mengaplikasikan ilmu statistika untuk analisis regresi nonparametrik
khususnya estimator spline dan kernel.
2. Mengetahui mana model regresi yang lebih baik,apakah model regresi
spline atau model regresi kernel.
3. Mengestimasi kurva regresi pertumbuhan anak balita menggunakan
regresi nonparametrik.
4. Menambah wacana ilmu pengetahuan yang selanjutnya dapat
dikembangkan ke tingkat yang lebih lanjut.
Beberapa buku, artikel dan jurnal menyajikan hasil-hasil penelitian
sebelumnya yang berhubungan atau mendukung penelitian tentang regresi
nonparametrik. Sifat-sifat asimtosis dibahas oleh Bain dan Engelhardt (1992)  ,
Spiegel (1986), dan Baisuni (1986) .Ada beberapa teknik penghalusan yang
dibahas dalam Hardle (1990), salah satunya penghalusan kernel. Untuk teknik
penghalusan kernel juga di bahas oleh Wand M.P dan M.C. Jones (1995). Untuk
penghalusan spline dibahas oleh Eubank R. (1998), Wahba G. (1990), dan I
Nyoman Budiantara (2000). Dursun Aydin (2007) membahas tentang
perbandingan model regresi nonparametrik yaitu regresi spline dan kernel.
Metode penulisan yang digunakan penulis adalah studi literatur yang
diperoleh dari perpustakaan, jurnal-jurnal ilmiah dan sumber-sumber lain yang
diperoleh dari internet. Penulis dalam menyelesaikan penelitian ini menggunakan
bantuan software R, S-Plus, dan Minitab. Data yang diambil merupakan data
sekunder dari internet yang dapat dipertanggungjawabkan.
1.7  Sistematika penulisan
Tugas akhir ini disusun dengan sistematika penulisan sebagai berikut :
Bab ini berisi latar belakang dan permasalahan, tujuan
penelitian, manfaat penelitian, pembatasan masalah, metode
penulisan, tinjauan pustaka, dan sistematika penulisan yang
memberikan arah terhadap penulisan tugas akhir ini.
Bab ini membahasan tentang teori dasar yang menunjang
pembahasan mengenai model regresi nonparametrik khususnya
regresi spline dan kernel serta hal-hal mendasar yang akan
digunakan lebih lanjut dalam pembahasan.
Bab ini membahas mengenai teori perhitungan estimasi regresi
nonparametrik yaitu estimasi spline dan kernel. Teori tersebut
berkaitan dengan metode perhitungan dan rumus dari regresi
spline dan kernel.
Bab ini membahas aplikasi metode spline dan kernel,dimana
studi kasusnya dengan menggunakan data pertumbuhan balita
sehingga diperoleh estimasi kurva regresi dan perbandingan
metodenya. Perhitungan ini dibantu dengan software R, S-Plus,
dan Minitab.
Bab ini berisi tentang kesimpulan yang diperoleh, pemecahan
masalah dan saran sebagai akibat dari kekurangan atau
kelebihan dari hasil penelitian yang dilakukan.

Metode pendekatan spline dan kernel telah dijelaskan dalam pembahasan
dan dilakukan studi kasus untuk data pertumbuhan balita pada bab-bab
sebelumnya, sehingga dapat diperoleh beberapa kesimpulan dan saran sebagai
berikut :
? Estimasi dengan regresi nonparametrik hanya membutuhkan sedikit
asumsi dibanding estimasi dengan regresi parametrik
? Estimasi spline dan kernel dalam regresi nonparametrik memiliki sifat
fleksibilitas yang tinggi dan kemampuan mengestimasi perilaku data yang
cenderung berbeda pada interval yang berlainan.
? Jika ingin mengestimasi kurva regresi nonparametrik dengan regresi
spline,maka dapat dilakukan dengan cara mencari model spline terbaik
berdasarkan titik knot optimal yang diperoleh dengan metode GCV
? untuk mengestimasi kurva regresi nonparametrik dengan regresi
kernel,dapat dilakukan dengan mencari nilai bandwidth optimal karena
pemilihan bandwidth optimal merupakan langkah yang sangat penting
dalam mengestimasi fungsi regresi kernel karena akan memberikan bentuk
pada estimasi funsi regresinya.
? Bebereapa cara pengestimasian kernel smoothing :
1. Nadaraya-watson estimate
2. Priestly-chao estimate
3. Gasser-muller estimaste
? Terdapat sedikitnya tiga cara pendekatan yang mungkin dilakukan untuk
menentukan bandwidth yang optimum :
1. Teknik leave-out (CV-method)
2. Memodifikasi p(h)(penalizing function)
3. Metode plug-in
? Dalam hasil studi kasus pada data pertumbuhan balita laki-laki dan
perempuan dapat diperoleh kesimpulan bahwa estimasi kernel lebih baik
daripada estimasi spline karena nilai R
pada regresi kernel lebih besar dari
pada regresi spline dan untuk MSE dari regresi kernel lebih kecil daripada
regresi spline.
? Masih banyak pilihan metode dalam regresi nonparametrik yang dapat
digunakan untuk mengestimasi kurva regresi.
? Tidak selalu regresi kernel lebih baik daripada regresi spline dalam
mengestimasi kurva regresi pada suatu data.

