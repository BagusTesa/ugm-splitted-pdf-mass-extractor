Sumber energi menjadi kebutuhan penting dalam kehidupan manusia di
seluruh dunia. Semakin berkurangnya cadangan minyak bumi sebagai bahan
bakar utama telah menyadarkan manusia untuk mencari alternatif pengganti yang
bersifat terbarukan dan juga lebih ramah lingkungan.
Sumber energi yang mulai banyak dikembangkan adalah cahaya matahari.
Pemanfaatan energi matahari sebagai sumber energi alternatif untuk mengatasi
krisis energi, khususnya minyak bumi, yang terjadi sejak tahun 1970-an mendapat
perhatian yang cukup besar dari banyak negara di dunia. Di samping jumlahnya
yang tidak terbatas, pemanfaatannya juga tidak menimbulkan polusi yang dapat
merusak lingkungan. Seperti yang kita ketahui cahaya atau sinar matahari dapat
dikonversi menjadi listrik dengan menggunakan teknologi sel surya atau
fotovoltaik. Indonesia yang terletak di khatulistiwa memiliki potensi penyinaran
matahari yang luar biasa besar. Matahari yang bersinar selama 12 jam setiap hari,
sepanjang tahun, dengan intensitas yang tinggi 4,8kWH/m2/hari. Ini sangatlah
menguntungkan untuk pengembangan- pengembangan sumber energi alternatif.
Dengan memanfaatkan solar cell, dapat menghemat pemakaian energi berbahan
bakar minyak bumi. Hal ini di dukung dengan perkembangan teknologi saat ini
sehingga energi dari solar cell hasil konversi cahaya matahari dapat disimpan
sebagai energi cadangan.
Dalam penggunaan solar cell kebanyakan, bahwa solar cell dipasang diam
(static). Hal ini menyebabkan intensitas matahari yang diterima kurang optimal.
Untuk mendapatkan arus listrik yang maksimal harus  selalu  berada  dalam
keadaan cahaya  yang  datang  tegak lurus dengan permukaan bidang. Oleh karena
itu perlu dibuat suatu model alat yang dapat diimplementasikan pada sistem solar
cell yang dapat mengikuti arah pergerakan matahari berdasarkan perhitungan
waktu edar matahari.
Fokus masalah pada tugas akhir ini adalah bagaimana membuat sebuah
purwarupa sistem penjejak cahaya dengan menggunakan sensor photodioda dan
motor servo serta dengan perhitungan menentukan posisi matahari untuk dapat
diimplementasikan pada pemasangan solar cell.
Dalam perancangan dan penulisan tugas akhir ini akan di tentukan
batasan- batasan masalah yang meliputi lain:
1. Menggunakan bahasa pemrograman Basic Compiler (BASCOM AVR)
untuk pengolahan data pada mikrokontroler.
2. Menggunakan Liquid Cyristal Display (LCD) 16x2 sebagai penampil data.
3. Menggunakan rangkaian Real Time Clock (RTC) sebagai pewaktuan.
4. Pengambilan data diambil diruang uji dengan kondisi pencahayaan yang
lebih rendah dibandingakan dengan sumber cahaya.
5. Sumber cahaya menggunakan sumber cahaya dari senter police yang
sudah ditentukan dengan jarak tetap.
6. Digunakan 2 servo untuk gerak utara-selatan dan timur-barat.
7. Perhitungan yang digunakan adalah perhitungan menentukan matahari
terbit dan terbenam yang biasa digunakan pada perhitungan waktu sholat.
8. Waktu penjejakan cahaya dengan perhitungan adalah matahari terbit
hingga matahari terbenam pada perhitungan.
9. Digunakan tombol push button sebagai mode pemilihan.
1.4 Tujuan dan Manfaat Penelitian
Tujuan dari penelitian ini adalah merancang dan mengimplementasikan
sebuah purwarupa sistem penjejak cahaya berbasis mikrokontroler ATmega 32
dengan menggunakan servo dan sensor photodioda serta menggunakan
perhitungan menentukan posisi matahari terbit sampai matahari terbenam .
Manfaat dari sistem purwarupa dapat digunakan untuk aplikasi lebih lanjut dalam
sistem solar tracker  serta diharapkan dapat mengatasi permasalahan pada sistem
solar tracker terdahulu apabila cuaca sedang mendung maupun matahari tertutup
awan.
Metode penelitian yang dilakukan dalam penelitian tugas akhir ini adalah
sebagai berikut:
1. Studi literatur :
- Memahami, mempelajari sensor photodioda dan data keluarannya,
melalui serangkaian percobaan sehingga diketahui karakteristik dari
sensor tersebut.
- Memahami tentang pengolahan data ADC dari keluaran sensor
photodioda yang diterjemahkan melalui mikrokontroler dan
ditampilkan pada Light Cyristal Display.
- Memahami bahasa BASCOM AVR (basic compailer) untuk
pengaturan gerakan servo.
- Memahami perhitungan menentukan waktu terbit dan terbenam
matahari yang digunakan sebagai acuan sistem bekerja.
2. Perancangan alat meliputi:
- Analisis desain sistem mikrokontroler, sistem mekanik gerakan lengan
servo, rangkaian sistem sensor, rangkaian RTC serta penampil LCD.
- Pembuatan sistem mikrokontroler, sistem mekanik lengan servo,
rangkaian sistem sensor dan penampil LCD.
- Pembuatan program ADC dan  program perhitungan posisi matahari
serta gerak servo untuk mikrokontroler Atmega 32 dengan
menggunakan bahasa pemrograman Bascom AVR (Basic  Compiler
3. Pengujian alat meliputi pengambilan data dengan alat yang telah dibuat
dan memastikan bahwa alat bekerja dengan baik.
Penyusunan  laporan  tugas akhir  ini dilakukan dengan sistematika
sebagai berikut:
Meliputi  latar  belakang, perumusan masalah, tujuan  penelitian, batasan
masalah, metode penelitian, dan sistematika penulisan.
Berisi  uraian  sistematis tentang informasi rancangan dari alat yang sudah
ada sebelumnya.
Berisi  pembahasan  komponen-komponen  yang  akan  digunakan  pada
sistem.  Pembahasan  berdasarkan  sifat,  fungsi,  dan  karateristik  dari
komponen yang digunakan.
Berisi  tentang  perancangan  sistem  yang  dibuat,  meliputi  perancangan
sistem  secara  keseluruhan,  perancangan  perangkat  keras,  dan
perangkat lunak.
Berisi uraian  tentang  implementasi  sistem  secara  detail  sesuai  dengan
rancangan  berdasarkan  komponen  serta  bahasa  pemrograman  yang
dipakai, serta  menjelaskan pengujian sari setiap bagian sistem yang
dibuat.
Membahas  tentang  hasil  pengujian  sistem  yang  dilakukan  meliputi
pengamatan hasil dari kinerja sistem.
Berisi kesimpulan yang memuat  uraian singkat tentang hasil penelitian
dan  saran-saran  sehingga  sistem  ini dapat dikembangkan lebih  lanjut,
dengan  harapan  dapat  digunakan untuk mendukung kehidupan
masyarakat.

Berdasarkan hasil penelitian dalam pembuatan Purwarupa Sistem Penjejak Cahaya
Dengan Sensor Photodioda Berbasis ATMega32 ini dapat diperoleh kesimpulan sebagai
berikut:
1. Purwarupa sistem penjejak matahari berdasarkan penentuan posisi matahari berbasis
ATMega 32 telah dapat diimplementasikan dengan baik sesuai dengan perancangan
sistem.
2. Sistem penjejak matahari bekerja dengan optimal apabila diberikan sumber cahaya
yang dideteksi mempunyai intensitas cahaya yang lebih besar dari kondisi
pencahayaan lingkungan dengan jarak tetap antara sensor dan sumber cahaya.
3. Pergerakan dari sistem penjejak matahari ini menggunakan dua buah motor servo
sebagai pengerak 4 arah kebebasan yang diatur berdasarkan perhitungan pewaktuan
dari RTC serta nilai ADC yang diterima pada sensor.
4. Sistem ini dapat melakukan penjejakan cahaya dari sumber cahaya dengan baik
sehingga dapat diimplementasikan dalam sistem pemasangan solar cell.
5. Sistem ini dapat memberikan solusi kelemahan pada sistem solar tracker apabila
matahari tertutup mendung, karena sistem ini menentukan posisi berdasarkan
pewaktuan nyata.
6. Tingkat error sistem ini sebesar 1,5� untuk sudut deklinasi dan 1� untuk sudut
penjejakan
Dari hasil penelitiian tugas akhir ini masih terdapat beberapa kekurangan sehingga
masih diperlukan perbaikan dan penggembangan lebih lanjut, diantaranya:
1. Dalam hal sistem mekanik perlu diberikan motor servo dengan torsi lebih besar agar
beban yang ditopang tidak menganggu kinerja sistem.
2. Untuk mempermudah dalam penentuan posisi dengan perhitungan dapat digunakan
servo yang memiliki keakurasian tinggi dalam penentuan derajat putar.

