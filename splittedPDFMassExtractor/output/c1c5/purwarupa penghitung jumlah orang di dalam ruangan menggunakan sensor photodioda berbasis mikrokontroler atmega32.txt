Pada era modern seperti sekarang ini, perkembangan teknologi telah
mempermudah pekerjaan manusia. Dalam masa perkembangan teknologi, banyak
bermunculan alat-alat yang canggih yang dapat bekerja secara otomatis. Dalam
bidang komputasi misalnya, sudah berkembang alat penghitung jumlah orang
yang masuk dan keluar pada suatu ruangan. Awalnya untuk menghitung jumlah
orang yang masuk ke dalam suatu ruangan tertentu yang digunakan adalah dengan
menggunakan tulisan tangan dan mengisi daftar tamu, namun sekarang sistem
presensi tersebut sudah tidak disarankan, hal ini karena berkembangnya alat
penghitung jumlah orang yang dilengkapi dengan sensor tertentu yang dapat
mendeteksi atau mengetahui ketika ada orang yang masuk ke dalam ruangan, dan
secara otomatis menambahkan angka pada display. Untuk dapat mengolah sinyal
yang dikirimkan oleh sensor dan manambahkan angka pada display, maka
dibutuhkan pula rangkaian pengolah sinyal dan rangkaian penghitung, dalam hal
ini digunakan sebuah mikrokontroler.
Dari latar belakang diatas maka pada tugas akhir ini dibuat sebuah alat
penghitung jumlah yang berada di dalam ruangan yang digunakan pada
perpustakaan untuk memudahkan penghitungan jumlah pengunjung. Pada alat ini
cara menghitung orang yang masuk  atau keluar secara otomatis dengan Sensor
Photodioda berbasis mikrokontroler ATmega32. Pada alat ini akan digunakan
sensor photodiode yang diletakkan pada pintu masuk dan keluar, sehingga ketika
ada orang yang masuk dan keluar, maka sensor tersebut dapat mendeteksinya.
Untuk dapat mengolah sinyal yang dikirimkan oleh sensor, maka digunakan
sebuah mikrokontroler, dalam hal ini mikrokontroler yang digunakan adalah
mikrokontroler ATmega32 yang kemudian ditampilkan menggunakan LCD 16x2
dan suara dengan media speaker.
Berdasarkan latar belakang permasalahan di atas maka masalah yang akan
dikaji pada penelitian ini tentang bagaimana membuat suatu alat penghitung orang
masuk dan keluar perpustakaan yang mampu memberikan informasi berapa
jumlah orang yang ada di dalam ruang perpustakaan.
Tujuan yang ingin dicapai dalam Tugas Akhir ini adalah merancang sistem
penghitungan jumlah orang yang masuk dan keluar ruangan agar dapat membantu
memudahkan dalam pekerjaan manusia dalam bidang teknologi dan dapat
memudahkan untuk melakukan pengecekan di dalam suatu ruangan dengan tidak
susah payah harus menghitung dengan cara manual.
Dalam tugas akhir ini, memiliki batasan-batasan masalah sebagai berikut:
1. Pembuatan sistem ini menggunakan LED sinar laser sebagai transmitter
untuk mendeteksi orang masuk dan Photodioda sebagai receiver.
2. Mikrokontroler yang digunakan yaitu Mikrokontroler Atmega 32 sebagai
pengolah data.
3. Sistem hanya dapat mendeteksi 1 masukan, apabila terdapat lebih dari satu
orang masuk secara bersamaan, hanya terhitung satu.
4. Sistem hanya dapat menghitung sampai 99 orang, dan penghitungan
kembali ke 0.
5. Menggunakan bahasa pemrograman bascom AVR untuk mengolah data
pada mikrokontroler .
6. Informasi penampil jumlah orang masuk dan keluar ditampilkan dalam
display LCD 16x2 dan suara menggunakan speaker.
Metodologi penelitian yang digunakan dalam penelitian tugas akhir ini
adalah sebagai berikut :
1. Menentukan tujuan dan batasan masalah dengan melihat beberapa faktor
dari sistem lain yang bermanfaat bagi pengembangan sistem pada
penelitian ini.
2. Melakukan kajian dan pembelajaran lebih lanjut tentang sistem yang
dibahas pada penelitian ini dengan melakukan studi literatur, yaitu
mempelajari artikel, makalah, jurnal, karya tulis, serta buku-buku yang
terkait dengan topik yang dibahas, untuk kemudian dijadikan sebagai
acuan dan referensi dalam merancang dan membuat penelitian ini.
3. Melakukan perancangan dan pembuatan sistem yang terdiri dari :
a. Hardware (perangkat keras)
Meliputi pembuatan desain rangkaian skematik dan PCB sistem minimum
b. Software (perangkat lunak)
Meliputi pembuatan program untuk mikrokontroler AVR ATMega32
dengan menggunakan BASCOM AVR.
4. Melakukan pengujian terhadap sistem yang telah dibuat serta menganalisis
hasil yang didapatkan dari perangkat guna mengetahui bahwa perangkat yang
telah dibuat bisa berfungsi sesuai dengan yang diharapkan.
5. Menarik kesimpulan dari perilaku perangkat yang telah dibuat.
Penyusunan laporan penelitian Tugas Akhir �PURWARUPA SISTEM
MIKROKONTROLER ATMEGA32� disusun dengan sistematika sebagai
berikut:
Berisi latar belakang dan permasalahan, rumusan masalah, tujuan dan
manfaat penelitian, batasan masalah, metodologi, dan sistematika penulisan.
Memuat tentang informasi-informasi tentang hasil penelitian yang  telah
dilakukan terkait dengan perancangan sistem penghitung orang masuk dan keluar
ruangan dengan mikrokontroler ATMega32.
Memuat tentang landasan teori penunjang dalam pembuatan dan
pembahasan masalah meliputi karakteristik komponen-komponen yang digunakan
dalam sistem pada Tugas Akhir ini.
Berisi gambaran secara keseluruhan dan perancangan sistem baik
perangkat keras maupun perangkat lunak pada sistem alat yang dibuat.
Memuat uraian tentang implementasi sistem secara detail sesuai dengan
rancangan dan berdasarkan komponen serta bahasa pemrograman yang dipakai
secara penjelasan ilmiah, yang secara logis dapat menerangkan alasan
diperolehnya hasil data dari penelitian. Berisi pengujian baik sistem kerja alat
pengukuran meliputi hardware, software, dan antarmuka, serta pengujian sistem
alat secara keseluruhan.
Membahas tentang hasil pengujian sistem yang dilakukan meliputi
pengamatan hasil dari kinerja sistem.
Berisi kesimpulan yang memuat uraian singkat tentang hasil penelitian
yang diperoleh sesuai dengan tujuan penelitian, serta sarana untuk penelitian lebih
lanjut.

Berdasarkan hasil penelitian dalam pembuatan purwarupa penghitung
orang masuk dan keluar ruangan dengan Sensor Photodioda ini dapat diambil
kesimpulan sebagai berikut:
1. Purwarupa sistem penghitung orang masuk dan keluar ruangan
menggunakan sensor photodiode berbasis ATMega32 ini dapat berjalan
dengan baik sesuai dengan keinginan penulis. Sistem ini dapat
memberikan informasi melewati display LCD dan speaker sebagai media
suara untuk mengetahui jumlah orang yang masuk dan keluar ruangan.
2. Apabila terdapat orang yang masuk secara bersamaan atau lebih dari 1
orang, sensor hanya dapat mendeteksi 1 orang yang melewati sensor
terlebih dahulu.
3. Sistem ini hanya dapat menghitung jumlah orang sampai 99, setelah
mencapai 99 orang, maka perhitungan akan kembali ke 0.
4. Proses utama pada sistem ini ada pada rangkaian sistem minimum
ATMega32 yang kemudian diteruskan ke display LCD untuk
menampilkan informasi dan menyalakan speaker.
5. Sistem ini dapat membantu memudahkan kepada publik bahwa dapat
memberikan informasi ada berapa jumlah orang yang berada di dalam
ruangan tanpa dengan melakukan perhitungan secara manual.
Dari hasil penelitian tugas akhir ini masih terdapat beberapa kekurangan
sehingga masih diperlukan perbaikan untuk pengembangan lebih lanjut,
diantaranya:
1. Pada alat ini hanya dapat mendeteksi  jumlah orang sampai dengan 99
orang. Untuk pengembangan sistem lebih lanjut dapat dibuat dengan
menambahkan hitungan bisa sampai ratusan atau ribuan. Perlu
dikembangkan sistem penghitungan jumlah orang dengan range yang
lebih banyak serta keakuratan yang lebih baik.
2. Penggunaan media laser, karena jika menggunakan laser dan Photodioda
maka keduanya harus sejajar dan memiliki tingkat presisi yang tinggi.
Misalnya dapat menggunakan infrared.

