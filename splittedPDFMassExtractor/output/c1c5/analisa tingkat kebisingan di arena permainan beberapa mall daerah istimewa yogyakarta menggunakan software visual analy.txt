Setiap aktivitas manusia disadari atau tidak dapat menjadi suatu sumber
kebisingan. Semakin berkembangnya suatu zaman, semakin berkembang pula
kemajuan di bidang teknologi dan industri. Di Indonesia,salah satu kemajuan di
bidang teknologi dapat dilihat pada pertokoan khususnya arena permainan,
dimana semua wahana permainan  terbuat dari mesin berteknologi modern.
Wahana permainan yang ada telah dilengkapi berbagai macam aplikasi hardware
dan software. Contoh dari aplikasi software adalah musik atau suara pendukung
dari wahana permainan sedangkan contoh dari aplikasi hardware adalah setir
mobil dan alat tembak mainan. Aplikasi dari suara atau musik yang terdapat di
wahana permainan dapat menimbulkan bunyi atau suara dengan intensitas yang
cukup tinggi sehingga dapat menyebabkan terjadinya kebisingan bagi pengunjung
yang datang.
Kebisingan merupakan suatu gangguan suara yang tidak diinginkan yang
dapat berdampak serius bagi kesehatan masyarakat dan lingkungan. Menurut
WHO, bising dikategorikan sebagai salah satu jenis dari polutan ( Bunde, 2012).
Kebisingan dapat menimbulkan banyak efek negatif jika tingkat kebisingan
melebihi nilai ambang batas yang telah ditetapkan oleh Menteri Kesehatan,
Menteri Lingkungan Hidup, dan Menteri Tenaga Kerja sesuai waktu dan
tempatnya.  Salah satu efek negatif dari kebisingan adalah terjadinya gangguan
pendengaran bahkan sampai menyebabkan ketulian.
menjelaskan bahwa di Indonesia, gangguan pendengaran dan ketulian saat ini
masih merupakan satu masalah yang dihadapi masyarakat. Berdasarkan hasil
Survei Nasional Kesehatan Indera Penglihatan dan Pendengaran di 7 provinsi
tahun 1993-1996, prevalensi ketulian 0,4% dan gangguan pendengaran 16,8%.
Penyebabnya, adalah infeksi telinga tengah, presbikusis, tuli akibat obat ototoksik,
tuli sejak lahir/congenital, dan tuli akibat pemaparan bising.
Jika seseorang mengalami ketulian, maka akibatnya adalah ia tidak dapat
melakukan aktifitas dengan normal, menurunnya konsentrasi, tidak dapat
merespon pembicaraan orang lain dengan baik, terhambatnya pendidikan terutama
pada anak-anak, adanya ketidakcakapan sosial, tidak percaya diri, dan  kurangnya
pembentukan karakter terhadap anak sehingga hal tersebut akan berpengaruh pada
masa kecil seorang anak dan hubungannya antarmanusia.
Di beberapa mall Daerah Istimewa Yogyakarta terdapat fasilitas yang
menyediakan arena permainan. Pengunjungnya mulai dari anak-anak hingga
orang dewasa. Kebisingan yang timbul dari arena permainan bersumber dari suara
musik dan peralatan pendukung wahana permainan  serta suara pengunjung yang
berada di sekitar arena permainan. Menurut Keputusan Menteri Negara
Lingkungan Hidup Nomor 48 Tahun 25 November 1996 tentang  Baku Tingkat
Kebisingan Peruntukan Kawasan/ Tingkat Kebisingan  Lingkungan Kegiatan dB
(A) untuk batas kebisingan tempat pertokoan adalah 70 dB. Jika tingkat
kebisingan di arena permainan berada di bawah nilai ambang batas yang telah
ditetapkan menunjukkan bahwa arena permainan tersebut dianggap aman bagi
masyarakat. Sedangkan sebaliknya, jika tingkat kebisingan di arena permainan
telah melebihi nilai ambang batas kebisingan yang telah ditetapkan maka
sebaiknya diperlukan penanganan yang lebih lanjut untuk mengurangi tingkat
kebisingan yang terjadi. Pemerintah setempat perlu mengambil suatu tindakan
terhadap pelaku usaha arena permainan untuk mengambil langkah agar tidak
terjadi kebisingan yang berada di atas nilai ambang batas yang telah ditetapkan
oleh pemerintah sehingga tidak mengganggu kesehatan masyarakat.
Berdasarkan permasalahan kebisingan tersebut, diperlukan adanya suatu
penelitian di arena permainan untuk mengetahui berapa nilai  tingkat kebisingan
yang terjadi. Oleh karena itu, penulis melakukan penelitian di tiga tempat arena
permainan yang terdapat di dalam mall sekitar Daerah Istimewa Yogyakarta
dengan menggunakan sound level meter dan perangkat lunak visual analyser 2011
yang sebelumnya telah diunduh secara gratis di internet lalu diinstall di notebook
sebagai alat untuk mengukur intensitas kebisingan yang terjadi.
Penelitian ini berjudul “ANALISA TINGKAT KEBISINGAN DI ARENA
Berdasarkan penjelasan permasalahan di atas, adapun rumusan masalah
yang akan dibahas pada penelitian ini meliputi :
1. Berapakah tingkat intensitas kebisingan sinambung (dalam dB(A))
yang terjadi di ketiga arena permainan masing-masing mall sekitar
Daerah Istimewa Yogyakarta berdasarkan variasi hari dengan
menggunakan perangkat lunak visual analyser 2011 dan sound level
meter?
2. Berapakah tingkat intensitas kebisingan sinambung (dalam dB(A))
yang terjadi di ketiga arena permainan masing-masing mall sekitar
Daerah Istimewa Yogyakarta berdasarkan variasi waktu/pukul dengan
menggunakan perangkat lunak visual analyser 2011 dan sound level
meter?
3. Bagaimanakah perbedaan yang terjadi terhadap pengukuran tingkat
intensitas kebisingan sinambung (dalam dB(A)) dengan menggunakan
perangkat lunak visual analyser 2011 terhadap sound level meter?
4. Apakah tingkat kebisingan yang diukur di ketiga arena permainan
masing-masing mall sekitar Daerah Istimewa Yogyakarta telah
melebihi nilai ambang batas yang telah ditetapkan oleh Menteri
Kesehatan dan Menteri Lingkungan Hidup?
5. Apakah faktor yang menyebabkan terjadinya kebisingan di arena
permainan masing-masing mall sekitar Daerah Istimewa Yogyakarta?
Pada penelitian ini, diperlukan adanya batasan masalah untuk menjadikan
penelitian lebih terarah dan tidak keluar dari masalah utama. Adapun beberapa
batasan masalah yang diperlukan dalam penelitian ini adalah sebagai berikut :
1. Penelitian ini dilakukan di arena permainan yang terdapat di dalam
mall sekitar Daerah Istimewa Yogyakarta, yaitu Mall A, Mall B, dan
Mall C.
2. Penelitian ini dilakukan dengan memvariasikan hari dan
memvariasikan waktu(pukul) dari masing-masing arena permainan di
Mall A, Mall B, dan Mall C.
3. Variasi hari dilakukan pada hari Senin, Selasa, Rabu, Kamis, Jumat,
Sabtu, dan Minggu dengan pukul yang sama masing-masing arena
permainan di setiap mall.
4. Variasi waktu(pukul) dilakukan pada pukul 11.00-12.00, 12.00-13.00,
13.00-14.00, 14.00-15.00, 15.30-16.30, dan 16.30-17.30 yang
dibedakan menjadi 6 shift masing-masing arena permainan di setiap
mall.
5. Pengambilan sampel kebisingan dilakukan selama 60 menit setiap 1
menit sekali di seluruh data penelitian.
6. Penelitian menggunakan sound level meter dan perangkat lunak visual
analyser 2011 sebagai alat pengukur intensitas kebisingan dan
stopwatch sebagai penghitung waktu.
7. Faktor suhu, kelembapan, jumlah pengunjung, dan luas arena
permainan tidak diperhitungkan pada penelitian ini.
Adapun tujuan penulis dalam melakukan penelitian ini adalah :
1. Untuk mengukur nilai intensitas kebisingan sinambung ?????????  ±
????????? ) di arena permainan yang berada di Mall A, Mall B, dan Mall C
sekitar Daerah Istimewa Yogyakarta dengan menggunakan sound level
meter dan perangkat lunak visual analyser 2011 dari variasi hari.
2. Untuk mengukur nilai intensitas kebisingan sinambung ?????????  ±
????????? ) di arena permainan yang berada di Mall A, Mall B, dan Mall C
sekitar Daerah Istimewa Yogyakarta dengan menggunakan sound level
meter dan perangkat lunak visual analyser 2011 dari variasi
waktu(pukul)
3. Untuk menentukan apakah kebisingan yang terjadi di arena permainan
yang berada di Mall A, Mall B, dan Mall C telah atau belum melebihi
nilai ambang batas yang telah ditetapkan oleh Menteri Kesehatan dan
4. Untuk memperlihatkan bahwa dengan menggunakan peralatan yang
sederhana, murah, dan praktis yaitu sound level meter dan perangkat
lunak visual analyser 2011 mampu mengukur tingkat kebisingan yang
terjadi di arena permainan di Mall A, Mall B, dan Mall C.
5. Untuk menentukan faktor-faktor penyebab kebisingan yang terjadi di
arena permainan di Mall A, Mall B, dan Mall C sekitar Daerah
Penelitian ini diharapkan dapat memberikan manfaat kepada pembaca,
mahasiswa khususnya program studi fisika, perguruan tinggi, dan  masyarakat
umum yang khususnya sering pergi ke arena permainan maupun karyawan yang
bekerja di tempat tersebut serta pemilik usaha. Adapun manfaat penelitian ini
yakni :
1.5.1 Bagi pembaca
Pembaca yang membaca tulisan ini diharapkan mendapatkan informasi
tambahan, wawasan, dan pengetahuan tentang kebisingan khususnya di arena
permainan di beberapa mall.
1.5.2 Bagi mahasiswa
Mahasiswa dapat menambah ilmu pengetahuan mereka tentang kebisingan
dan dapat mempraktekkan pengukuran internsitas kebisingan secara langsung di
berbagai tempat yang ingin diketahui tingkat kebisingannya menggunakan
perangkat lunak visual analyser 2011 maupun mengguanakan alat pengkur
kebisingan sound level meter secara mudah, praktis, cepat, dan tidak
membutuhkan biaya yang besar dalam hal pembelajaran maupun penelitian karena
hanya dengan meng-download  software tersebut melalui computer/notebook.
1.5.3 Bagi perguruan tinggi
Penelitian ini merupakan suatu wacana  sebagai salah satu pengembangan
metode penelitian terhadap ilmu pengetahuan dan teknologi di lingkungan
perguruan tinggi.
1.5.4 Bagi masyarakat
Dampak negatif dari kebisingan di atas nilai ambang batas yang wajar
diharapkan dapat menyadarkan masyarakat umum agar dapat menghindari dan
mengurangi bepergian ke tempat-tempat yang kebisingannya cukup tinggi dan
lebih menyadari pentingnya menjaga kesehatan terutama dalam hal pendengaran
agar terhindar dari gangguan pendengaran yaitu ketulian.
1.5.5 Bagi pemilik usaha
Hasil pemelitian dapat digunakan bagi pemilik usaha tempat permainan agar
lebih mengantisipasi untuk mengurangi tingkat kebisingan yang terjadi agar
terciptanya suatu kenyamanan dan keamanan kesehatan bagi setiap pengunjung.

Berdasarkan hasil penelitian yang telah dilakukan di arena permainan Mall
A, Mall B, dan Mall C Daerah Istimewa Yogyakarta, maka dapat di simpulkan
sebagai berikut :
1. A. Intensitas kebisingan sinambung (LAeq) yang terjadi berdarkan variasi
hari dengan menggunakan software visual analyser 2011 meliputi :
a. Arena permainan di Mall A sebesar ????????  ±  ?????????  = (92 ± 3) dB(A)
b. Arena permainan di Mall B sebesar ????????  ± ?????????  = (93 ± 4) dB(A)
c. Arena permainan di Mall C sebesar ????????  ± ?????????  = (94± 5) dB(A)
B.  Intensitas kebisingan sinambung (LAeq) yang terjadi berdarkan variasi
waktu (pukul) dengan menggunakan software visual analyser 2011
meliputi :
a. Arena permainan di Mall A sebesar ????????  ±  ?????????  = (90 ± 1) dB(A)
b. Arena permainan di Mall B sebesar ????????  ± ?????????  = (95 ± 4) dB(A)
c. Arena permainan di Mall C sebesar ????????  ± ?????????  = (93 ± 2) dB(A)
2.  A. Intensitas kebisingan sinambung (LAeq) yang terjadi berdarkan variasi
hari dengan menggunakan sound level meter sl-4012 meliputi :
a. Arena permainan di Mall A sebesar ????????  ±  ?????????  = (88 ± 2) dB(A)
b. Arena permainan di Mall B sebesar ????????  ± ?????????  = (88 ± 2) dB(A)
c. Arena permainan di Mall C sebesar ????????  ± ?????????  = (85 ± 4) dB(A)
B.  Intensitas kebisingan sinambung (LAeq) yang terjadi berdarkan variasi
waktu (pukul) dengan menggunakan sound level meter sl-4012 meliputi :
a. Arena permainan di Mall A sebesar ????????  ±  ?????????  = (88 ± 1) dB(A)
b. Arena permainan di Mall B sebesar ????????  ± ?????????  = (89± 2) dB(A)
c. Arena permainan di Mall C sebesar ????????  ± ?????????  = (81 ± 1) dB(A)
3. Dengan peralatan yang cukup sederhana yakni berupa software visual
analyser 2011 yang terinstall di notebook bisa digunakan untuk mengukur
intensitas kebisingan di suatu tempat dengan hasil yang tidak jauh
perbedaan  pengukurannya terhadap penggunaan sound level meter.
4. Hasil yang ditunjukkan dari keseluruhan data penelitian dengan
menggunakan sound level meter  dan perangkat lunak visual analyser
2011 menunjukkan bahwa tingkat kebisingan sinambung yang terjadi pada
arena permainan di Mall A,  Mall B , dan Mall C  melebihi Nilai Ambang
Batas yang telah ditetapkan pemerintah baik menurut Menteri Kesehatan
60 dB(A) maupun Menteri Lingkungan Hidup 70 dB(A). Perbedaannya
berkisar antara 25 dB(A) hingga 40 dB(A) terhadap Nilai Batas Ambang
maksimum menurut Menteri Kesehatan dan berkisar 10 dB(A) hingga 20
dB(A) menurut Menteri Lingkungan Hidup.
5. Faktor – faktor penyebab kebisingan di arena permainan di tiga mall
meliputi ramainya suara percakapan pengunjung yang dating mulai dari
percakapan biasa hingga percakapan yang kuat, suara teriakan,suara tepuk
tangan, tangisan anak-anak, hingga suara tertawa yang terbahak-bahak,
suara dari wahana permainan mulai dari musik aplikasi wahana permainan
maupun suara yang ditimbulkan ketika wahana permainan yang tidak
dimainkan maupun yang sedang dimainkan, dan suara yang berasal dari
mesin penghitung tiket (tempat pengisian voucher dan penukaran tiket)
Dari penelitian ini dapat dijadikan bahan pertimbangan untuk melakukan
upaya meredam tingkat kebisingan di arena permainan dalam mall dan sekaligus
dapat dijadikan bahan penelitian lebih lanjut untuk mendapatkan hasil penelitian
yang lain yang lebih spesifik dan lebih lengkap, maka dari itu berikut ini saran
yang bisa dijadikan bahan pertimbangan :
1. Diperlukan penelitian lebih lanjut untuk pengukuran intensitas kebisingan
di arena permainan dengan tingkat kebisingan sinambung yang berbeda
dari penelitian ini, misalnya tingkat kebisingan siang dan malam sehingga
dapat diketahui lebih spesifik dengan menggunakan software visual
analyser dan sound level meter.
2. Penelitian selanjutnya dapat menggunakan alat pengukur kebisingan yang
memperhitungkan faktor suhu maupun kelembaban dan faktor lainnya
agar hasilnya lebih akurat..
3. Diperlukan upaya penanggulangan tingkat kebisingan di arena permainan
dengan cara memasang dinding penghalang dari bahan material yang
mudah menyerap bunyi, salah satunya bisa menggunakan material pasir
pantai yang sudah dibentuk pada dinding ruangan atau bisa juga
menggunakan bahan busa yang biasanya dibuat untuk jok kursi mobil atau
motor. Hal ini didasarkan pada hasil penelitian bahwa material pasir pantai
ataupun busa memiliki koefesien serapan bunyi paling tinggi.
4. Pekerja/karyawan yang bekerja di arena permainan diupayakan
menggunakan  earplug  untuk mengurangi kebisingan yang terdengar oleh
telinga.

