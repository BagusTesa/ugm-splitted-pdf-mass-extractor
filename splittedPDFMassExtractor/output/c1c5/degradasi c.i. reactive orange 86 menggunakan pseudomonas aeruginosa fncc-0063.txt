Zat warna merupakan salah satu pencemar utama di dalam limbah cair dari
industri tekstil. Industri pencelupan tekstil dalam proses produksinya
menghasilkan produk samping berupa air limbah dalam jumlah yang besar dan
mengandung berbagai macam bahan kimia, salah satunya adalah zat warna
sintetik yang digunakan pada proses pewarnaan atau pencelupan tekstil. Dari
berbagai macam zat warna sintetik yang digunakan di dalam industri tekstil, lebih
dari 50% diantaranya merupakan zat warna golongan azo, yaitu zat warna sintetik
yang mengandung paling sedikit satu ikatan N=N. Terdapat sekitar 3000 macam
zat warna azo yang memiliki beragam warna dan sangat praktis untuk digunakan
dalam keperluan industri, namun demikian ternyata beberapa jenis zat warna
golongan azo diketahui bersifat toksik bahkan karsinogenik bagi organisme
akuatik maupun organisme lain (Puvaneswari, 2006). Zat warna azo diketahui
tidak mudah rusak oleh perlakuan kimia maupun fotolitik, maka apabila limbah
tekstil yang mengandung zat warna azo dibuang begitu saja ke lingkungan, hal
tersebut dapat mengakibatkan terjadinya akumulasi hingga tingkat konsentrasi
tertentu yang pada akhirnya akan menimbulkan dampak negatif terhadap daya
dukung lingkungan (Sastrawidana, 2011).
Fakta menunjukkan bahwa sebagian besar limbah cair dari industri tekstil
biasanya langsung dialirkan ke badan air. Beberapa dampak negatif yang
ditimbulkan oleh keberadaan zat warna, terutama zat warna azo di badan perairan
antara lain dapat mengganggu estetika badan perairan serta dapat menghalangi
penetrasi sinar matahari ke dalam badan perairan, sehingga hal tersebut dapat
mengganggu proses fotosintesis dari tumbuhan air, akibatnya kandungan oksigen
dalam  air  menjadi  berkurang  dan  pada  gilirannya  akan menurunkan kualitas
air serta dapat berakibat fatal terutama bagi organisme akuatik (Lukito dkk.,
2013). Di samping itu air yang tercemar limbah zat warna azo apabila terkonsumsi
oleh manusia tentunya dapat membahayakan kesehatan.
Untuk menjamin terpeliharanya sumber daya air dari pembuangan limbah
industri, pemerintah dalam hal ini Menteri Negara Kementerian Lingkungan
Hidup  telah menetapkan  baku  mutu  limbah  cair  bagi kegiatan industri yang
sudah beroperasi yang dituangkan dalam Kep.Men. L.H. No.51/MENLH/10/1995
tentang  baku  mutu  limbah  cair  bagi  kegiatan industri. Konsekuensi dari
perundangan tersebut adalah, pelaku industri yang aktivitas industrinya
menghasilkan  limbah  dalam  jumlah  besar  dan  berpotensi mencemari
lingkungan  harus  membangun  instalasi  pengolahan  air  limbah yang memadai
(Sastrawidana dan Sukarta, 2011).
Untuk mengatasi permasalahan tersebut, berbagai metode pengolahan
limbah cair zat warna, baik secara fisika maupun kimia telah dilakukan, diantara
yang umum digunakan adalah metode koagulasi serta metode adsorpsi
menggunakan karbon aktif. Namun demikian ternyata kedua metode tersebut
memiliki beberapa kekurangan diantaranya, metode koagulasi dalam aplikasinya
ternyata menghasilkan lumpur dalam jumlah yang besar yang justru akan
menimbulkan masalah baru untuk penanganan lumpurnya (Park dkk., 2007),
selain itu metode adsorpsi menggunakan karbon aktif aplikasinya masih sangat
terbatas dikarenakan mahalnya biaya operasional. Oleh karena itu perlu
dikembangkan suatu metode alternatif untuk pengolahan limbah cair zat warna,
terutama zat warna azo, yang tidak hanya efektif, namun juga ekonomis serta
ramah lingkungan.
Proses degradasi biologi menggunakan bakteri diyakini sebagai salah satu
metode yang efektif, ekonomis, serta ramah lingkungan dalam mengatasi limbah
zat warna. Bakteri diketahui memiliki siklus pertumbuhan yang relatif singkat,
sehingga hal tersebut menjadi salah satu faktor yang menunjukkan keunggulan
penggunaan bakteri dibandingkan mikroorganisme lain dalam proses degradasi
zat warna. Kemampuan beberapa spesies bakteri dalam proses degradasi zat
warna, khususnya zat warna azo didasarkan pada aktivitas enzim azoreduktase
yang dapat membantu dalam proses pemutusan ikatan azo.
Pseudomonas merupakan salah satu genus bakteri fakultatif anaerob yang
digunakan secara luas dalam berbagai penelitian mengenai degradasi zat warna
secara biologi, selain itu beberapa hasil penelitian telah melaporkan bahwa
penggunaan bakteri tersebut terbukti mampu untuk mendegradasi beberapa jenis
zat warna azo secara efektif (Silveira dkk., 2009). Berdasarkan latar belakang
permasalahan yang telah diuraikan di atas, maka pada penelitian ini dilakukan
pengkajian kemampuan salah satu bakteri dari genus Pseudomonas, yaitu
Pseudomonas aeruginosa dalam proses degradasi terhadap zat warna tekstil yang
tergolong ke dalam zat warna azo, yaitu C.I. Reactive Orange 86 yang selanjutnya
disebut RO 86. Pada penelitian ini sistem bakteri dengan zat warna dikondisikan
dalam keadaan tanpa penambahan medium untuk mengetahui kemampuan bakteri
dalam mendegradasi zat warna tanpa sumber nutrisi yang berasal dari medium,
selain itu dalam penelitian ini juga dipelajari beberapa faktor yang dapat
berpengaruh di dalam proses degradasi zat warna azo oleh bakteri Pseudomonas
aeruginosa.
1. Mengetahui kemampuan bakteri Pseudomonas aeruginosa dalam proses
degradasi zat warna RO 86.
2. Mengetahui faktor yang mempengaruhi degradasi zat warna RO 86 oleh
bakteri Pseudomonas aeruginosa.
3. Mengidentifikasi terjadinya perubahan struktur molekul zat warna setelah
proses degradasi melalui perbandingan spektra absorbansi UV-visibel zat
warna RO 86 sebelum dan sesudah proses degradasi oleh Pseudomonas
aeruginosa.
Dengan adanya penelitian mengenai degradasi zat warna azo oleh
Pseudomonas aeruginosa ini diharapkan dapat menambah khasanah ilmu
pengetahuan khususnya pada bidang lingkungan, dalam pengolahan limbah cair
industri tekstil maupun berbagai industri lain yang menggunakan zat warna azo
sebagai salah satu bahan baku utamanya.

Berdasarkan hasil penelitian yang telah dilakukan dapat disimpulkan
bahwa:
1. Bakteri Pseudomonas aeruginosa mampu mendegradasi zat warna RO 86
dalam kondisi inkubasi tanpa agitasi dan tidak mampu mendegradasi zat
warna tersebut dalam kondisi inkubasi yang disertai perlakuan agitasi.
2. Persentase degradasi RO 86 oleh Pseudomonas aeruginosa meningkat
seiring dengan peningkatan waktu kontak dan mencapai optimum pada 96
jam dengan nilai persentase sebesar 54,01%. Penambahan jumlah koloni
bakteri dan penambahan pH larutan zat warna dari asam hingga mendekati
pH netral juga dapat meningkatkan degradasi zat warna hingga mencapai
optimum pada penggunaan jumlah koloni bakteri sebesar 2,3 x 10
cfu
dan pH 6 yang menghasilkan persentase degradasi masing-masing sebesar
87,50% dan 70,63%. Peningkatan konsentrasi larutan zat warna
menyebabkan penurunan persentase degradasi zat warna dari nilai
optimumnya, yaitu dari 90,52% menjadi 75,86%.
3. Perbandingan spektra absorbansi UV-visibel pada sampel zat warna
sebelum dan setelah proses degradasi menunjukkan bahwa setelah
mengalami proses degradasi, terjadi penurunan puncak absorbansi zat
warna di daerah visibel serta pergeseran puncak absorbansi pada daerah
UV yang mengindikasikan bahwa setelah dikontakkan dengan bakteri,
struktur molekul zat warna mengalami degradasi yang mengakibatkan
terputusnya ikatan azo dan terbentuk senyawa aromatik sebagai hasil
degradasinya.
Berkaitan dengan penelitian ini, saran-saran yang diajukan adalah sebagai
berikut:
1. Perlu dilakukan penelitian lebih lanjut mengenai perbandingan efisiensi
degradasi zat warna azo pada perlakuan penambahan nutrisi serta tanpa
penambahan nutrisi di dalam sistem degradasi.
2. Perlu dilakukan penelitian lebih lanjut terhadap senyawa hasil degradasi
zat warna azo oleh bakteri menggunakan instrumen yang lebih lengkap
seperti HPLC, MS, serta FT-IR untuk mengetahui produk akhir hasil
degradasi zat warna oleh bakteri.
3. Perlu dilakukan penelitian lebih lanjut untuk mengetahui secara kuantitatif
mengenai kontribusi biodegradasi zat warna maupun biosorpsi zat warna
pada proses penghilangan zat warna oleh bakteri.

