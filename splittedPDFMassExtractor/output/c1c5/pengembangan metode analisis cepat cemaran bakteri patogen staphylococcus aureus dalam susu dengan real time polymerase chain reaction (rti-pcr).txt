Keamanan pangan merupakan salah satu  isu yang harus menjadi perhatian
baik pemerintah maupun masyarakat. Pengolahan makanan yang tidak bersih
dapat memicu terjadinya keracunan makanan. Keracunan makanan akibat
kontaminasi bakteri patogen atau foodborne disease (FBD) disebabkan karena
bakteri patogen tersebut masuk ke dalam tubuh bersama makanan. Salah satu
produk pangan yang banyak dikonsumsi dan rentan kontaminasi bakteri adalah
susu. Hal ini dikarenakan susu mengandung kandungan nutrisi yang lengkap
sehingga merupakan media yang ideal untuk tumbuhnya bakteri. Bakteri patogen
yang ditemukan pada susu adalah bakteri Staphylococcus aureus. Proses
pengolahan susu yang tidak bersih yang berasal dari proses pemerahan susu,
penggunaan alat-alat dan distribusi dapat memicu kontaminasi susu oleh bakteri S.
aureus (Zakary, 2011). Pekerja dalam industri makanan yang memiliki infeksi
luka pada kulit biasanya merupakan sumber kontaminasi dari makanan, walaupun
terkadang susu mentah dan produk olahan susu pada dasarnya sudah
terkontaminasi oleh S.aureus (Hein et al., 2001).
S. aureus merupakan bakteri gram positif, berbentuk bulat seperti
bergerombol seperti anggur. S. aureus dapat ditemukan di lingkungan masyarakat
seperti udara, debu, kotoran, air, susu dan makanan atau terdapat pada peralatan
makan, manusia maupun pada hewan. Sumber utama penyebab kontaminasi
makanan oleh S. aureus adalah individu yang mengolah makanan, di samping itu
dapat juga dari peralatan masak dan lingkungan sekitar (LeLoir et al., 2003). S.
aureus memproduksi staphylococcal enterotoxins (SE) yaitu bersifat patogen
yang dapat menyebabkan keracunan pangan. Gejala umum keracunan SE ditandai
dengan mual, muntah, kram perut dan diare yang terjadi selama 24-48 jam, masa
penyembuhan biasanya terjadi antara 1-3 hari (Balaban & Rasooly, 2000).
Kasus keracunan makanan akibat kontaminasi S. aureus di Indonesia
dilaporkan terjadi di beberapa daerah. Pada bulan September 2004 telah terjadi
keracunan setelah minum susu pada 72 siswa Sekolah Dasar (SD) di Tulung
Agung, Jawa Timur, 300 siswa SD di Bandung, dan 73 karyawan di Surabaya.
Menurut Badan Pemeriksaan Obat dan Makanan (BPOM), kasus tersebut
disebabkan oleh E. coli dan S. aureus (Suwito, 2010). Kasus serupa terjadi pada
Juni 2009, 10 siswa SD di Jakarta Timur dan 293 siswa SD di Bandung
mengalami mual-mual setelah mengonsumsi susu dalam kemasan. Berdasarkan
pemeriksaan BPOM, toksin yang dihasilkan S. aureus dianggap sebagai penyebab
keracunan setelah minum susu. Pada Juni 2012, sedikitnya 20 warga Kabupaten
Sukabumi keracunan massal setelah menyedot susu dalam kemasan yang dibeli
dari suatu minimarket. Gejala yang dialami yaitu perut sakit, mengalami mual dan
muntah-muntah. Gejala yang dialami memiliki kemiripan dengan gejala
keracunan makanan akibat S. aureus.
Bahaya yang ditimbulkan akibat susu terkontaminasi S. aureus  memberikan
efek yang besar terhadap kesehatan masyarakat. Oleh karena itu pengujian susu
terhadap kemungkinan cemaran bakteri menjadi penting. Di Indonesia analisis
cemaran bakteri pada makanan khususnya pada susu segar dan hasil olahannya
hingga sejauh ini menggunakan metode konvensional yaitu dengan cara kultur
dan uji sifat biokimia. Uji mikrobiologis ini merujuk pada SNI 2897:2008 tentang
metode pengujian cemaran mikroba dalam daging, telur dan susu serta hasil
olahannya. Metode ini menetapkan pengujian cemaran mikroba S. aureus dengan
menggunakan pengujian total perhitungan pelat (TPC) (Anonim, 2008). Metode
konvensional ini mempunyai kelemahan karena umumnya memerlukan waktu 5-7
hari untuk mendapatkan hasil. Hal ini kurang menguntungkan karena apabila
terjadi keracunan makanan harus segera diketahui penyebabnya agar dapat
ditangani secara lebih cepat. Oleh karena itu  upaya pengembangan untuk
mendeteksi S. aureus terus dilakukan.
Dalam beberapa tahun terakhir ini, banyak diusulkan metode analisis
penyebab FBD dengan teknik biologi molekular yaitu Polymerase Chain Reaction
(PCR). PCR merupakan metode amplifikasi fragmen dari DNA templat untuk
menghasilkan fragmen DNA identik secara in vitro (Pelt-Verkuil et al., 2007).
Tahap-tahap PCR meliputi tahap denaturasi, penempelan primer pada cetakan
DNA (annealing) dan tahap pemanjangan primer melalui reaksi polimerisasi
nukelotida. Bakteri S.aureus memproduksi nuklease termostabil (TNase). TNAse
merupakan endonuklease yang berhubungan dengan DNA dan RNA. Pengujian
ini kemudian dijadikan dasar untuk analisis PCR dengan target gen nuc pada S.
aureus. Pendekatan PCR untuk mendeteksi kontaminasi S. aureus pada makanan
telah dilakukan sebelumnya. Hein et al. (2001) menggunakan nuc target primer
untuk mendeteksi  S.aureus pada sampel keju. Pendekatan PCR menggunakan nuc
target primer juga telah terbukti spesifik untuk mendeteksi dari S. aureus pada
sampel daging (Alarcon et al., 2005). Penelitian tentang analisis bakteri S. aureus
dengan PCR masih memiliki kekurangan. Hal ini dikarenakan pendeteksian hasil
dilakukan dengan teknik elektroforesis yang hanya dapat dilakukan setelah
seluruh reaksi amplifikasi selesai (titik akhir).
Pengembangan metode yang dapat dimanfaatkan untuk mengatasi
kekurangan tersebut adalah teknik Real-Time PCR (RTi-PCR). Pada teknik ini
amplifikasi fragmen DNA dimonitor secara realtime dengan sinyal fluoresens
sehingga analisis hasil dapat diketahui secara langsung sebelum reaksi amplifikasi
selesai (Fajardo et al., 2010). Sinyal fluoresens pada RTi-PCR dapat dihasilkan
dari molekul fluoresens yang berikatan dengan DNA. Jika pada PCR spesifisitas
hasil PCR ditunjukkan oleh ukuran fragmen hasil elektroforesis, spesifisitas
produk RTi-PCR ditunjukkan dengan hasil Melting Curve Analysis (MCA).
Deteksi produk amplifikasi RTi-PCR dengan senyawa fluoresen juga menjadikan
teknik dapat menghasilkan data kuantitatif. Hal ini menjadikan RTi-PCR lebih
efisien dibandingkan dengan metode PCR konvensional (Camma et al., 2011).
Berdasarkan uraian di atas, penelitian yang dilakukan saat ini difokuskan
untuk menentukan metode deteksi yang efektif dan cepat dalam mengetahui
keberadaan cemaran bakteri S. aureus pada produk pangan yaitu susu. Oleh
karena itu, dilakukan pengembangan metode deteksi S. aureus pada susu dengan
RTi-PCR yang dikombinasikan dengan MCA. Untuk menentukan kebenaran dan
keakuratan dari pengembangan metode, perlu dilakukan validasi metode. Validasi
metode perlu dilakukan untuk mengetahui spesifitas metode dalam mendeteksi
S.aureus dalam susu, mengetahui presisi dari uji repitabilitas (uji pengulangan
metode) dan penentuan cut off detection dari metode. Metode yang telah
tervalidasi diharapkan dapat menghasilkan data yang akurat apabila diaplikasikan
pada analisis kontaminasi S. aureus dalam susu yang dijual di masyarakat.
Penelitian ini akan menerapkan metode RTi-PCR sebagai metode deteksi
cemaran bakteri S.aureus dengan rumusan masalah:
1. Apakah primer gen nuc mampu mendeteksi cemaran S.aureus secara spesifik
dengan RTi-PCR?
2. Bagaimana tingkat spesifitas, presisi dan cut off detection metode RTi-PCR
untuk deteksi cemaran bakteri S.aureus pada susu ?
3. Bagaimana hasil aplikasi metode RTi-PCR terhadap sampel susu yang dijual
di masyarakat ?
Adapun tujuan dari penelitian yang dilakukan yakni:
1. Mengembangkan metode deteksi cemaran bakteri S. aureus  dengan
pendekatan molekular DNA.
2. Mempelajari penggunaan metode RTi-PCR untuk mendeteksi cemaran
bakteri S.aureus pada susu.
3. Melakukan validasi metode RTi-PCR untuk mendeteksi cemaran S.aureus
pada susu.
4. Mengaplikasikan metode RTi-PCR untuk mendeteksi cemaran S.aureus pada
susu.
Manfaat yang dapat diperoleh dari penelitian ini adalah :
1. Metode RTi-PCR yang telah tervalidasi dapat digunakan untuk pengujian
cemaran S. aureus pada makanan sehingga didapatkan informasi yang tepat
dan cepat.
2. Memberikan kontribusi dalam pengembangan metode deteksi cemaran
bakteri pada makanan dengan pendekatan molekular DNA yang dapat
dijadikan alternatif  selain pengujian secara konvensional yang selama ini
dilakukan.

Berdasarkan penelitian yan telah dilakukan dan uraian pembahasan dapat
diperoleh kesimpulan sebagai berikut :
1. Metode analisis bakteri S.aureus dalam susu dengan RTi-PCR dengan primer
nuc A1 dan nuc A2 spesifik untuk mendeteksi cemaran bakteri S. aureus
dalam susu. Ta (temperature annealing) optimal yang didapatkan adalah 58
�C. Tm (melting temperature) amplikon yang didapatkan adalah 79 �C.
2. Validasi metode yang dilakukan meliputi :
a. Uji spesifitas menunjukkan metode spesifik mendeteksi cemaran bakteri S.
aureus dalam susu.
b. Uji presisi menunjukkan sepuluh kontrol positif memiliki nilai rata-rata Ct
(Cycle number threshold) adalah 32,63 � 1,08 dan Tm amplikon pada
c. Penentuan cut off detection menunjukkan metode RTi-PCR dapat
mendeteksi S. aureus hingga 1,04 � 10
3. Hasil aplikasi metode pada sampel susu yang dijual di Yogyakarta adalah
empat sampel susu yaitu dua sampel susu pasteurisasi, susu pasar tradisional
dan susu yang dijual di warung susu positif terdeteksi adanya S. aureus
dengan RTi-PCR dan susu UHT tidak terdeteksi.
Penelitian lanjut dapat dilakukan dengan metode isolasi DNA yang lain
selain menggunakan proteinase-K dan SDS agar dihasilkan kemurnian DNA yang
lebih baik. Selain itu, metode RTi-PCR dapat dilakukan dengan primer selain
primer nuc A1 dan nuc A2 untuk mendeteksi cemaran bakteri S. aureus dalam
susu.

