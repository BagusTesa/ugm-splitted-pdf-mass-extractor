Penelitian tentang hubungan diantara fenomena-fenomena real merupakan
dasar dari tujuan sains dan memainkan peranan penting dalam kehidupan sehari-
hari. Saat ini analisis regresi merupakan alat yang populer untuk mengetahui
hubungan tersebut. Analisis regresi adalah salah satu metode untuk menentukan
hubungan sebab-akibat antara satu variabel dengan variabel yang lain. Variabel
penyebab disebut dengan variabel independen, variabel penjelas atau variabel X.
Sementara variabel yang terkena akibat dikenal sebagai variabel yang
dipengaruhi, variabel dependen, variabel terikat, variabel respon atau variabel Y.
Pendugaan kurva regresi digunakan untuk menjelaskan hubungan antara
peubah penjelas dengan peubah respon. Pendekatan pendugaan yang paling sering
digunakan adalah pendekatan parametrik. Asumsi yang mendasari pendekatan ini
adalah kurva regresi dapat diwakili oleh suatu model parametrik (Hardle, 1990).
Dalam regresi parametrik, diasumsikan bahwa bentuk kurva regresi diketahui
berdasarkan teori, informasi sebelumnya, atau sumber- sumber lain yang dapat
memberi pengetahuan secara terperinci.
Apabila  model  dari  pendekatan  parametrik  diasumsikan
benar,  maka  pendugaan  parametrik  akan  sangat  efisien. Tetapi jika salah,
maka akan menyebabkan interpretasi data yang menyesatkan. Selain itu, model
parametrik mempunyai keterbatasan untuk menduga pola data yang tidak
diharapkan. Jika asumsi bentuk kurva parametrik ini tidak
terpenuhi,  maka  kurva  regresi  dapat  diduga  menggunakan  model regresi dari
pendekatan nonparametrik.
Pendekatan  nonparametrik  merupakan  metode  pendugaan
model  yang  dilakukan  berdasarkan  pendekatan  yang  tidak  terikat
asumsi  bentuk  kurva  regresi  tertentu.
Analisis regresi klasik mempunyai syarat pemenuhan asumsi linieritas dan
asumsi data berdistribusi normal. Analisis ini bertujuan untuk mengetahui arah
hubungan antara variabel independen dengan variabel dependen apakah positif
atau negatif sekaligus untuk memprediksi nilai dari variabel dependen apabila
nilai variabel independen mengalami kenaikan atau penurunan. Data yang
digunakan biasanya berskala interval atau rasio.  Jika jumlah variabel independen
lebih dari satu, maka digunakan analisis regresi  linier berganda.
Dalam praktek dilapangan, data yang ditemukan seringkali tidak
memenuhi asumsi yang diisyaratkan regresi linier klasik.Generalizedliniermodel
(GLM) merupakan perluasan dari model regresi linier dengan asumsi prediktor
memiliki efek linier akan tetapi tidak mengasumsikan distribusi tertentu dari
variabel respon dan digunakan ketika variabel respon merupakan anggota dari
keluarga eksponensial(Nelder dan Weddeburn, 1972).
Generalized linier mixed models (GLMM) merupakan salah satu metode
alternatif untuk menganalisa suatu data dimana distribusi variabel respon masuk
kedalam keluarga eksponensial, tetapi masih memiliki hubungan linier antara
variabel respon dan variabel prediktor.  GLMM merupakan teori model linier
yang menyertakan efek acak dan efek tetap dalam model. Sementara
itu,generalizedadditivemixed models (GAMM) mengganti fungsi linier menjadi
fungsi additive pada GLMM.
Generalized additivemodels (GAM) merupakan perluasan dari regresi
linier biasa dengan menggantikan fungsi linier menjadi fungsi aditif sehingga
model ini dapat digunakan meskipun hubungan variabel respon dan beberapa
variabel prediktor tidak linier. Dan seperti halnya GLM, distribusi respon pada
GAM tidak hanya pada distribusi normal saja tapi juga  distribusi yang termasuk
dalam keluarga eksponensial dapat dianalisis dengan metode ini.
Teori model aditif bersifat menyeluruh dalam mengungkapkan hal-hal
yang lebih kompleks terutama yang berkaitan dengan pengaruh acak, komponen
ragam dan bentuk sebaran data peubah yang tidak normal. Selanjutnya, model
GAMM ini diharapkan lebih efisien dalam mengidentifikasi sebaran pengaruh
komponenen acak sehingga mampu menerangkan lebih tepat pengaruh
komponenen acak tersebut dalam suatu model.
Berdasarkan latar belakang dan rumusan masalah yang telah dipaparkan
diatas, Maka penulisan tugas akhir ini bertujuan :
1. Mempelajari metode generalized additive mixed models dalam
menganalisa suatu data.
2. Mempelajari estimasi fungsi penghalus smoothing spline
3. Mempelajari estimasi generalized additive mixed models
Adanya pembatasan masalah dalam penulisan ini agar tercapai tujuan
penulisan dan tidak terdapat penyimpangan dari tujuan yang telah ditetapkan
diatas. Pembatasan masalah  pada tugas akhir ini meliputi pemaparan serta
penggunaan generalized additive mixed models untuk data variabel kuantitatif
dengan estimasi fungsi penghalus menggunakan  smoothing spline.
Analisis regresi linier hanya memiliki kemampuan untuk mengetahui pola
hubungan variabel respon dan variabel prediktor jika asumsi linieritas dan
distribusi normal terpenuhi. Generalized linier modelslebih luas lagi cakupannya,
karena bisa digunakan untuk keluarga eksponenesial lainnya tidak terpaku pada
distribusi normal saja (Nelder dan Weddeburn, 1972).
Generalized linier mixed models(GLMM) merupakan gabungan dari dua
pengaruh efek yaitu efek tetap dan efek acak pada variabel prediktor yang
nantinya akan mempengaruhi variabel respon (Brelow dan Clayton,1993).
Generalizedlinier mixed models (GLLM) ini tidak dapat menjelaskan secara
sempurna untuk sebuah analisis data yang tidak memiliki hubungan linier.
Generalized additive models (GAM) memiliki kemampuan untuk
menjelaskan pengaruh dari setiap variabel prediktor terhadap variabel respon
seperti halnya pada model linier. Dengan menerapkan smoothing spline dalam
mengestimasi fungsi aditif, kecenderungan � kecenderungan dalam data seperti
adanya non linier atau bahkan kecenderungan yang berbeda pada titik data dapat
terlihat. Hal tersebut karena smoothing spline merupakan salah satu metode dalam
pendekatan piecewise fitting of regression equation (Takezawa, 2006).
Generalized additive mixed models (GAMM) menjelaskan penggunaan
fungsi aditif pada regresi. Adanya efek tetap, efek acak, komponen ragam dan
bentuk sebaran data peubah yang tidak normal, dengansmoothing splineyang
berfungsi sebagai estimasi fungsi penghalus.
Metode yang digunakan dalam penulisan tugas akhir ini adalah studi
literatur. Studi literatur adalah penelusuran literatur materi terkait yang bersumber
dari buku, media, pakar ataupun dari hasil penulusuran di internet,  yang bertujuan
untuk menyusun dasar teori yang digunakan dalam melakukan penelitian ini.
Sistematika penulisan tugas akhir disusun sebagai berikut :
Bab ini membahas tentang latar belakang masalah, tujuan
penelitian, pembatasan masalah, tinjauan pustaka, metode
penulisan  dan sistematika penulisan.
Bab ini membahas teori-teori dasar yang akan digunakan sebagai
landasan dalam penulisan tugas akhir.
Bab ini akan menjelaskan tentanga generalized additive mixed
models atau yang akan disingkat dengan GAMM, dengan
smoothing spline sebagai estimasi fungsi penghalus.
Bab ini membahas tentang aplikasi dari model aditif campuran
tergeneralisasi untuk data sekunder yang diperoleh  dari  skripsi
(Sutrisni, 2010). Variabel respon loyalitas pelanggan dan variabel
prediktorefek tetap kualitas produk, kualitas pelayanan dan tingkat
kepercayaan pelanggan, sedangkan kota sebagai efek acak dalam
model yang akan dibentuk.
Bab ini berisi kesimpulan-kesimpulan yang diperoleh dari
pembahasan sebelumnya  dan saran-saran terkait.

Berdasarkan pembahasan yang telah dilakukan pada penulisan skripsi ini,
penulis dapat mengambil kesimpulan, yaitu :
1. Generalized Additive Mixed Models (GAMM) dapat digunakan dalam
kasus dimana variabel respon termasuk dalam keluarga eksponenesial,
tidak memiliki hubungan linier antara variabel respon dengan beberapa
variabel prediktor, serta memiliki efek campuran (mixed) dalam model,
efek tetap dan efek acak.
2. Generalized Additive Mixed Models (GAMM) memiliki fungsi hubung
yang dibentuk dari jumlahan fungsi tunggal dari masing-masing
prediktor yang diestimasi menggunakan smoothing spline.
3. Nilai estimasi parameter generalized additive mixed models
menggunakan metode numerik iterasi Newton-Raphson, yaitu
menggunakan turunan pertama dan kedua dari fungsi log-likehoodnya.
4. Langkah  langkah yang digunakan dalam proses pemodelan GAMM
diawali dengan identifikasi distribusi dari variabel respon dan linieritas
variabel respon pada setiap variabel prediktornya. Kemudian dilakukann
pembentukan GAMM,  dan model terpilih.
5. Data yang digunakan adalah Data loyalitas pelanggan indosat IM3
(Sutrisni, 2010) dengan variabel dependen yang digunakan adalah
loyalitas pelanggan indosat IM3 di Universitas Diponogoro dan
Universitas Negeri Sebelas Maret, sedangkan variabel independen dibagi
menjadi dua yaitu variabel independen yang bersifat efek tetap (fixed
effect ) dan variabel independen yang bersifat efek acak (random effect).
Variabel independen semacamdesain produk, harga produk, kualitas
produk dan kualitas pelayananbersifat efek tetap(fixed effect). Sedangkan
tingkat kepercayaan pelanggan sebagai variabel independen bersifat efek
acak  (random effect).
Pengembangan Generalized Additive Mixed Models adalah Vector

