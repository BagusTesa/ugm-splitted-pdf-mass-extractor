Secara umum investasi adalah kegiatan pengalokasian sejumlah dana
dengan harapan mendapatkan keuntungan di masa yang akan datang. Manajemen
investasi diperlukan untuk memenuhi tujuan investasi yaitu keuntungan bagi
investor. Salah satu bentuk manajemen investasi adalah membentuk portofolio
yang efisien.
Dalam pembentukan portofolio, investor berusaha memaksimalkan
pengembalian yang diharapkan (expected return) dari investasi dengan tingkat
risiko tertentu yang diterima. Dengan kata lain, investor berusaha meminimalkan
risiko yang dihadapi untuk sasaran tingkat pengembalian terntentu. Portofolio
yang dapat mencapai tujuan tersebut disebut portofolio yang efisien.
Untuk membentuk portofolio yang efisien, perlu dibuat beberapa asumsi
mengenai perilaku investor dalam membuat keputusan investasi. Asumsi yang
wajar adalah investor cenderung menghindari risiko (risk-averse). Investor
penghindar risiko adalah investor yang jika dihadapkan pada dua investasi dengan
expected return yang sama dan risiko yang berbeda, maka ia akan memilih
investasi dengan tingkat risiko yang lebih rendah.
Portofolio yang optimal adalah portfolio yang dapat memaksimalkan
preferensi investor sehubungan dengan pengembalian dan risiko. Jika seorang
investor memiliki beberapa pilihan portofolio yang efisien, maka portofolio yang
paling optimal-lah yang akan dipilihnya.
Harry Markowitz memperkenalkan teori pembentukan portofolio yang
efisien dan cara pemilihan portofolio yang optimal dari beberapa pilihan
portofolio yang efisien. Model yang dikenal dengan model optimisasi mean-
variance ini meliputi alokasi modal investasi untuk sejumlah aset dengan tujuan
memaksimalkan return sambil meminimalkan risiko tertentu. Return diukur dari
nilai harapan (expected value), sedangkan risiko diukur dengan variansi
portofolio.
Model optimisasi mean-variance memang membutuhkan komputasi yang
tidak rumit, namun dengan demikian model ini memiliki beberapa kelemahan.
Kelemahan tersebut adalah portofolio optimal yang dihasilkan model ini tidak
terdiversifikasi dengan baik. Portofolio yang dihasilkan cenderung terkonsentrasi
pada sebagian kecil aset. Jika diaplikasikan untuk portofolio dengan skala besar
maka model ini menjadi tidak efisien dalam komputasinya. Selain itu model
mean-variance juga sangat sensitif terhadap perubahan parameter input.
Pada skripsi ini akan dibahas analisis pembentukan portofolio optimal
yang mampu mengatasi sensitivitas pada metode mean-variance, yang disebut
optimisasi portofolio robust. Istilah optimisasi robust akan digunakan dalam
terminologi untuk menunjukkan sebuah teknik optimisasi yang membentuk model
ketidakpastian parameter kemudian membawanya ke dalam kasus terburuk dari
model itu. Istilah ini perlu diperhatikan karena menurut Tutuncu dan Koenig
(2002) terdapat setidaknya 17 definisi yang berbeda tentang robustness dalam
konteks yang berbeda, yang akan mengarah ke formulasi matematika yang
berbeda pula.
Dalam optimisasi  portofolio robust, parameter inputnya dianggap tidak
pasti, dalam hal ini terletak dalam sebuah interval konfidensi yang selanjutnya
disebut himpunan ketidakpastian (uncertainty set). Hal ini disebabkan karena pada
realita mengestimasi nilai kedua parameter ini tidak mudah, selain itu nilainya
selalu berubah setiap saat. Setelah menentukan himpunan ketidakpastian, masalah
optimisasi akan diselesaikan untuk kasus terburuk yakni kondisi dengan nilai
pengembalian (expected return) portofolio minimum dan risiko portofolio
maksimum. Masalah optimisasi dalam analisis portofolio robust ini akan dibawa
ke dalam bentuk second-order cone programming (SOCP). Masalah optimisasi
SOCP sendiri dapat diselesaikan dengan metode titik interior primal-dual.
Skripsi ini dibatasi pada pembahasan mengenai analisis portofolio robust
yaitu pembentukan portofolio optimal dengan ketidakpastian parameter yang
dimodelkan dalam masalah SOCP. Portofolio optimal yang terbentuk dari metode
ini kemudian akan dibandingkan dengan portofolio optimal dari metode mean-
variance.
Penulisan skripsi ini bertujuan untuk:
1. Mempelajari analisis investasi pada saham dan manajemen portofolio.
2. Mempelajari pemilihan portofolio yang efisien melalui pemilihan bobot
optimal saham menggunakan teknik optimisasi.
3. Mempelajari pembentukan portofolio robust dengan optimisasi SOCP
dan membandingkannya dengan portofolio mean-variance.
Investasi bertujuan untuk mendapatkan keuntungan di masa depan dari
sejumlah dana yang dialokasikan saat ini. Dalam memenuhi tujuan ini investor
memerlukan manajemen investasi yang salah satu kegiatannya adalah
pembentukan portofolio yang efisien (Bodie, 2006). Jika seorang investor
memiliki beberapa pilihan portofolio yang efisien, maka portofolio yang paling
optimallah yang akan dipilihnya.
Markowitz (1952) memperkenalkan pemilihan portofolio yang optimal
dengan parameter risiko (variansi) dan expected return (mean) yang dihitung dari
data historis. Hubungan antara mean return dan variansi return dimanfaatkan
untuk memperoleh variansi portofolio yang paling kecil. Teknik ini kemudian
disebut optimisasi portofolio mean-variance dan sering dipakai dalam
permasalahan alokasi aset investasi.
Fabozzi et al. (2007) dan Kim dan Boyd (2007) menyatakan bahwa model
mean-variance ini mempunyai kelemahan dalam hal diversifikasi portofolio,
yakni portofolio yang dihasilkan tidak terdiversifikasikan dengan baik. Optimisasi
mean-variance juga menghasilkan bobot yang ekstrim dan non-intuitif  untuk
beberapa aset dalam portofolio. Selain itu model mean-variance juga sangat
sensitif terhadap perubahan parameter input.
Untuk mengatasi sensitivitas pada model mean-variance, diperkenalkan
analisis portofolio robust. Istilah robust dalam hal ini dijelaskan oleh Tutuncu dan
Koenig (2002) dan oleh Kim dan Boyd (2007) sebagai sebuah teknik optimisasi
yang membentuk model ketidakpastian parameter kemudian membawanya ke
dalam kasus terburuk dari model itu. Teknik moving window dalam menentukan
himpunan ketidakpastian parameter dikenalkan oleh Tutuncu dan Koenig (2002).
Model portofolio robust ini dibawa ke bentuk optimisasi second-order
cone programming (SOCP) yang penyelesaiannya menghasilkan bobot saham
yang optimal dalam portofolio, seperti yang ditunjukkan oleh Engels (2004), Kim
dan Boyd (2007), dan Fabozzi et al. (2007).
Masalah optimisasi SOCP dapat diselesaikan dengan metode titik interior
primal-dual seperti yang dibahas oleh Lobo et al. (1998), Alizadeh dan Goldfarb
(2003) dan Ye (2004). Algoritma metode titik interior primal-dual dalam
menyelesaikan masalah optimisasi SOCP dibahas oleh Sturm (2002) dan
Antoniou dan Lu (2007). Pada skripsi ini, penyelesaian masalah SOCP dengan
metode titik interior primal-dual dilakukan dengan bantuan software MATLAB
dan solver SeDuMi seperti yang dijelaskan oleh Engels (2004) dan Lu (2009).
Metode yang dipakai dalam penulisan skripsi ini adalah studi literatur dan
studi kasus. Studi literatur dilakukan dengan mempelajari referensi-referensi yang
berupa buku teks, buku elektronik, artikel dan jurnal. Sedangkan studi kasus
dilakukan dengan melakukan analisis terhadap data saham di Bursa Efek
Indonesia  yang diperoleh dari situs web Yahoo! Finance.
Skripsi ini disusun dengan sistematika sebagai berikut:
1. Bab I Pendahuluan
Bab ini membahas latar belakang, pembatasan masalah, tujuan penulisan,
tinjauan pustaka, metode penulisan, dan sistematika penulisan.
Bab ini berisi definisi, teorema, dan contoh yang mendasari dan
mendukung pembahasan dalam bab selanjutnya.
Pada bab ini dibahas mengenai pembentukan portofolio mean-variance
dengan metode pengali Lagrange dan portofolio robust dengan teknik optimisasi
Bab ini membahas pembentukan portofolio menggunakan metode
optimisasi mean-variance dan SOCP dari tujuh saham yang terdaftar di Bursa
Efek Indonesia, serta membandingkan kinerja kedua portofolio tersebut.
5. Bab V Kesimpulan dan Saran
Bab ini berisi kesimpulan dari seluruh pembahasan di bab sebelumnya dan
saran yang mendukung riset di masa depan.

1. Investasi bertujuan untuk mendapatkan keuntungan di masa yang akan
datang dari sejumlah dana yang dialokasikan pada masa sekarang. Dalam
memenuhi tujuan ini, diperlukan manajemen investasi. Salah satu bentuk
manajemen investasi adalah membentuk portofolio yang efisien.
2. Prinsip dasar dalam teori portofolio medern adalah bahwa untuk tingkat
expected return tertentu, seorang investor yang rasional akan memilih portofolio
yang memiliki risiko/variansi terkecil dari antara portofolio-portofolio yang
memungkinkan (dalam hal ini portofolio optimal-lah yang akan dipilih).
3. Metode mean-variance oleh Markowitz (1952), untuk pembentukan
portofolio optimal diketahui memiliki beberapa kelemahan, di antaranya
portofolio tidak terdiversifikasi dengan baik dan sensitif terhadap perubahan
parameter input.
4. Analisis optimisasi portofolio robust merupakan analisis yang secara
sistematis mengatasi masalah sensitivitas metode mean-variance dengan cara
membentuk model ketidakpastian parameter dan membawanya ke dalam kasus
terburuk dari model.
5. Permasalahan optimisasi portofolio robust dapat dibawa ke dalam bentuk
optimisasi second-order cone programming (SOCP) yang secara efisien dapat
diselesaikan dengan metode titik interior.
6. Optimisasi portofolio robust dengan SOCP ini dilakukan untuk asumsi
kasus terburuk, yaitu expected return minimum dan risiko maksimum. Dengan
demikian investor dijamin mendapat return portofolio yang pasti, karena kasus
terburuk tersebut sudah dioptimisasi.
7. Berdasarkan studi kasus yang telah dilakukan, portofolio robust (SOCP)
menunjukkan kinerja yang lebih baik daripada portofolio mean-variance (MV),
ditinjau dari tingkat keuntungan, rate  of return dan nilai Sharpe ratio portofolio.
1. Bagi investor yang memiliki keengganan yang tinggi terhadap risiko (risk-
averter) dapat menggunakan analisis optimisasi portofolio robust dengan SOCP
sebagai pedoman alternatif pembentukan portofolio optimal.
2. Bagi peneliti yang memiliki minat tentang teknik optimisasi dapat
mengembangkan macam-macam permasalahan optimisasi dalam analisis
portofolio atau investasi secara umum, baik dengan teknik SOCP maupun
pemrograman nonlinear yang lain.
3. Bagi mahasiswa statistika diharapkan lebih mampu menerapkan dan
mengembangkan ilmu statistika dalam lingkup dunia investasi atau finansial
secara umum.

