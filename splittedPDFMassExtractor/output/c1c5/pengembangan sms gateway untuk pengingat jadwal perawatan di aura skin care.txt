SMS atau Short Message Service merupakan teknologi yang telah menjadi
sarana komunikasi antara semua orang di dunia. Dimulai dari sekedar
mengirimkan pesan dari jaringan lokal hingga mengirim pesan antar negara.
Teknologi ini banyak dipilih karena selain praktis, biaya yang harus dikeluarkan
untuk mengirim satu SMS cukup terjangkau (hanya Rp. 0,- sampai Rp. 150,-
saja). Karena faktor inilah, SMS telah banyak digunakan oleh berbagai bidang
jenis usaha.
Salah satu contoh dari jasa atau servis yang menggunakan SMS antara lain
adalah perbankan. SMS banking misalnya, kita hanya perlu mengirim SMS untuk
mengetahui saldo dari tabungan kita. Pada dunia hiburan di televisi, seperti
pengiriman SMS pada kontes idola untuk mendukung popularitas idola pilihan
kita. Pada industri IT, data center yang memanfaatkan SMS untuk mengawasi
kinerja dari server. Serta masih banyak lagi jasa dan industri yang menggunakan
media ini.
Aura Skin Care merupakan sebuah klinik kecantikan yang terletak di Solo,
Jawa Tengah. Aura Skin Care dalam pelayanannya memberikan jasa tindakan
dengan tujuan meningkatkan derajat kesehatan kecantikan kulit wajah dan tubuh
yang optimal. Petugas klinik memberikan informasi melalui SMS mengenai
jadwal perawatan pasien setiap bulan. Namun cara ini masih dilakukan secara
manual dengan mencari satu persatu data pasien, maka diperlukan sebuah sistem
yang mampu mempermudah dalam pengiriman sms dengan mudah dan cepat.
Dari latar belakang yang diuraikan di atas, maka rumusan masalah yang
dibahas dalam laporan tugas akhir ini adalah bagaimana membuat Sistem
Informasi Klinik Kecantikan dengan fitur SMS Gateway yang memiliki
kemampuan mengolah data member, treatment, dan produk secara runtun untuk
dapat menghasilkan informasi yang valid mengenai jadwal perawatan
berikutnya.Bagaimana membangun sebuah sistem yang mampu memberikan
pelayanan informasi yang mudah didapatkan oleh admin dan juga oleh member /
pasien.
Batasan masalah untuk pembuatanSMS gateway meliputi beberapa hal
diantaranya:
1. Layanan informasi via SMS hanya untuk menyediakan informasi yang
berkaitan dengan jadwal perawatan pasien.
2. Dalam sistem ini belum terdapat fitur untuk menerima SMS.
Tujuan penelitian ini adalah membuat sebuah sistem dengan fitur SMS yang
berfungsi sebagai pengingat jadwal untuk pasien di Aura Skin Care.
Manfaat yang diperoleh dari pembuatan Tugas Akhir ini:
1. Memudahkan Petugas untuk memberikan informasi kepada pasien.
2. Mengingatkan kembali kepada pasien kapan harus melalukan perawatan.
Metodologi yang digunakan untuk tercapainya tujuan dari penelitian ini
adalah:
Mendatangi dan melihat secara langsung mekanisme yang sedang
berjalan pada klinik, penulisan akan melalukan observasi yang berguna
untuk mencari data-data penulisan yang nyata di lapangan.
Dalam melakukan penulisan tugas akhir ini penulis melakukan
wawancara kepada aktor yang terkait untuk mendapatkan dan
mengumpulkan data-data yang dibutuhkan seperti sistem yang sedang
berjalan dan laporan.
Mempelajari buku-buku atau literatur sebagai referensi untuk merancang,
membuat, dan mengimplementasikan sistem.
Pengembangan sistem menggunakan metode waterfall dengan tahap
sebagai berikut:
a. Analisis
Melakukan analisa terhadap kebutuhan sistem.
b. Perancangan
Melakukan perancangan Pengembangan SMS Gateway untuk
Pengingat Jadwal Perawatan di Aura Skin Care untuk mendapat
solusi dari tahap analisa. Sehingga dapat memenuhi kebutuhan
sistem.
c. Implementasi
Pada tahap ini, dilakukanlah penerapan dari pembentukan sistem
yang telah dirancang sebelumnya dengan melakukan pembuatan
aplikasi lewat pembuatan kode (coding).
d. Pengujian
Pengujian aplikasi dilakukan untuk memastikan bahwa aplikasi yang
dibuat telah sesuai dengan desainnya dan semua fungsi dapat
dipergunakan dengan baik tanpa ada kesalahan.
Sistematika penulisan laporan berguna untuk memberikan gambaran umum
dari keseluruhan isi laporan serta untuk mempermudah pembacaan agar lebih jelas
dan akurat. Sistematika penulisan dan garis besar isi laporan ini adalah sebagai
berikut:
Bab ini akan membahas mengenai latar belakang masalah, batasan
masalah, tujuan penelitian, manfaat penelitian, metodologi
penelitian yang dilakukan, serta sistematika penulisan untuk
menjelaskan pokok-pokok pembahasan.
Bab ini berisi uraian sistematika tentang informasi hasil penelitian
yang disajikan dalam pustaka dan menghubungkan dengan sistem
informasi.
Bab ini akan menguraikan teori-teori yang mendukung penelitian
ini, yang menjadi dasar bagi pemecahan masalah dan didapat
dengan melalukan studi pustaka sebagai landasan dalam
melakukan penelitian.
Bab ini menjelaskan implementasi perangkat lunak, sarana yang
dibutuhkan, dan contoh cara pengoperasian perangkat lunak yang
dirancang. Bab ini juga menguraikan hasil evaluasi dari penelitian
ini.
Bab ini berisi pembahasan tentang implementasi dari sistem yang
telah dibuat.
Bab ini berisi mengenai hasil dari penelitian terhadap sistem yang
telah dibuat dan juga membahas mengenai kekurangan dan
kelebihan sistem setelah dilakukan implementasi
Bab ini berisikan kesimpulan-kesimpulan yang didapat dari
pembuatan tugas akhir perancangan aplikasi SMS, disertai dengan
saran-saran yang membangun.

Dari analisis dan penulisan laporan tugas akhir ini dapat diambil beberapa
kesimpulan, yaitu:
1. Sistem Informasi Klinik Kecantikan dengan Fitur SMS Gateway di Aura
Skin Careberhasil dibangun agar dapat digunakan untuk memberikan
kemudahan bagi pengelola klinik yang selama ini masih menggunakan
cara manual dalam mengelola data.
2. Sistem Informasi Klinik Kecantikan dengan Fitur SMS Gateway di Aura
Skin Care ini mempunyai kemudahan dalam memberikan reminder /
informasi kepada pasien / member.
3. Untuk memberikan informasi yang lebih, terikat kemudahan pelayanan
informasi pengiriman SMS dibuat otomatis.
4. Selain menangani pengelolaan data, sistem informasi ini dapat
menghasilkan laporan dalam bentuk tabel dengan kolom � kolom yang
informatif sehingga mudah untuk dipahami.
Untuk memberikan pelayanan yang lebih, terikat kemudahan pelayanan
informasi disarankan agar sistem diimplementasikan dengan cara menginstallnya
pada web hosting, sehingga pengguna dapat mengakses informasi dari mana saja
secara online. Dan juga yang tidak kalah penting adalah mengenai keamanan data,
alangkah lebih baik dilakukan pengembangan sistem terkait enkripsi untuk
menjamin bahwa data yang ada di dalam sistem tersebut benar � benar aman dan
valid.

