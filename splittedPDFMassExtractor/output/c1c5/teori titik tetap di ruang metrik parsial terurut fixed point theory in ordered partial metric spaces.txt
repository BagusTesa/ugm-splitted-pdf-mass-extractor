Teori titik tetap merupakan salah satu subjek yang menarik untuk dika-
ji karena memiliki banyak aplikasi dalam berbagai bidang. Selama kurun wak-
tu sepuluh tahun terakhir, pengembangan teori titik tetap telah menyita perhatian
banyak ahli. Hal ini karena peranannya baik di dalam maupun di luar bidang mate-
matika, beberapa di antaranya adalah dalam permasalahan persamaan integral, per-
samaan diferensial, persamaan matriks, sistem dinamik, dan ekonomi. Pada tahun
1994, Matthews dalam papernya yang berjudul Partial Metric Topology menje-
laskan mengenai konsep ruang metrik parsial yang merupakan perumuman dari ru-
ang metrik. Dalam ruang metrik parsial setiap titik tidak harus mempunyai jarak
sama dengan nol terhadap dirinya sendiri. Pengenalan konsep metrik parsial antara
lain dilatarbelakangi oleh suatu tujuan untuk memperoleh model matematika dalam
teori komputasi atau untuk memodifikasi prinsip kontraksi Banach.
Pengembangan teori titik tetap pada umumnya terfokus pada dua hal yaitu
pemetaan kontraksi dan himpunan atau ruang yang akan dikenakai pemetaan kon-
traksi tersebut. Dalam teori titik tetap prinsip kontraksi Banach memegang peranan
penting untuk menunjukkan eksistensi titik tetap dari suatu fungsi pada himpunan.
Oleh karena itu, sejumlah matematikawan berusaha mengembangkan prinsip kon-
traksi Banach dengan membuat perumuman pada pemetaan kontraksinya. Salah
satu hasil yang telah diperoleh yaitu pemetaan kontraksi lemah yang merupakan
perumuman dari pemetaan kontraksi. Selain itu, eksistensi titik tetap dapat juga
dikaji berdasarkan himpunan atau ruangnya. Pada prinsip kontraksi Banach, dalam
menunjukkan eksistensi titik tetap dilakukan pada ruang metrik lengkap. Hal ini
dapat dikembangkan seperti yang telah dijelaskan oleh ACM Ran dan Reurings
(2003) yang menunjukkan eksistensi titik tetap di himpunan terurut parsial (par-
tially ordered set) yang selanjutnya menjadi landasan untuk menunjukan eksistensi
titik tetap di ruang metrik terurut (ordered metric spaces).
Selain itu dapat juga dilakukan perumuman secara bersamaan baik dari ru-
angnya maupun pemetaan kontraksinya seperti yang dilakukan oleh Harjani dan
Sadarangani (2009). Dalam penelitiannya Harjani dan Sadarangani (2009) me-
nunjukkan eksistensi titip tetap pada ruang metrik terurut dengan menggunakan
pemetaan kontraksi lemah dan dengan dengan menambahkan beberapa syarat. Penu-
lis dalam penelitian ini telah melihat hasil yang diperoleh oleh Harjani dan Sadara-
ngani (2009) sehingga penulis mempunyai motivasi untuk mengkaji lebih dalam
tentang eksistensi titik tetap di ruang metrik parsial terurut dengan menggunakan
perumuman pemetaan kontraksi lemah.
Berdasarkan latar belakang di atas maka beberapa permasalahan yang akan
dibahas adalah sebagai berikut:
1. Diberikan (X,?X) himpunan terurut parsial dan (X, d) ruang metrik lengkap.
Misalkan fungsi f : X ? X naik monoton sehingga untuk setiap y ?X x
berlaku d(f(x), f(y)) ? cd(x, y) dengan c ? [0, 1) dan terdapat x0 ? X sehing-
ga x0 ?X f(x0).
a. Akan dipelajari eksistensi titik tetap apabila f kontinu pada (X, d).
b. Akan dipelajari eksistensi titik tetap apabila untuk sebarang barisan naik {xn}
yang konvergen ke x ? X berlaku xn ?X x untuk setiap n.
2. Akan dipelajari eksistensi titik tetap pada (1) dengan menggunakan fungsi f :
X ? X naik monoton sehingga untuk setiap y ?X x berlaku d(f(x), f(y)) ?
d(x, y)? ?(d(x, y)).
3. Akan ditunjukkan eksistensi titik tetap pada (2) dengan mengganti ruang metrik
(X, d) dengan ruang metrik parsial terurut.
4. Akan ditunjukkan eksistensi titik tetap pada (3) dengan menggunakan fungsi f :
X ? X naik monoton sehingga untuk setiap y ?X x berlaku ?(p(f(x), f(y))) ?
?(d(x, y))? ?(d(x, y)).
Tujuan utama dari penelitian ini adalah untuk mempelajari/meneliti eksis-
tensi titik tetap pada ruang metrik parsial terurut dengan menggunakan pemetaan
yang merupakan perumuman dari pemetaan kontraksi lemah. Oleh karena itu, hasil
dari tesis ini diharapkan dapat memperluas dan memperdalam wawasan tentang
konsep teori titik tetap di ruang metrik parsial terurut baik dengan menggunakan
pemetaan kontraksi lemah maupun dengan menggunakan perumumannya serta mem-
buka peluang aplikasi baik di dalam maupun di luar bidang matematika.
Konsep ruang metrik telah dibahas oleh Royden (1989) dalam bukunya Real
Analysis sedangkan konsep ruang metrik parsial yang merupakan perumuman dari
ruang metrik telah dibahas oleh S.G. Matthews (1994) dalam papernya Partial
Metric Topology. Dalam paper tersebut S.G. Matthews menjelaskan definisi ruang
metrik parsial dan topologi pada ruang metrik parsial. Sementara untuk pembahasan
tentang teori titik tetap khususnya prinsip kontraksi Banach telah dijelaskan oleh
Ravi Agarwal, Maria Meehan dan Donal O�regan (2004) dalam bukunya berjudul
Fixed Point Theory and Applications, dan dilanjutkan dengan penjelasan lebih lan-
jut dalam An Introduction to Metric Spaces and Fixed Point Theory oleh Khamsi
dan Kirk (2000). Buku ini memberikan penjelasan tentang Teorema Titik Tetap
Banach beserta aplikasinya. Untuk mempelajari mengenai himpunan terurut parsial
penulis merujuk ke buku Lattices and Ordered Sets karangan Steven Roman (2000).
Dalam paper hasil penelitian Rhoades (2001) dijelaskan mengenai pemetaan
kontraksi lemah dan eksistensi titik tetap di ruang metrik lengkap dengan menggu-
nakan pemetaan kontraksi lemah. Berdasarkan dua hasil tersebut maka Dutta dan
Chouddury (2008) mendefiniskan perumuman pemetaan kontraksi lemah yang den-
gan pemetaan tersebut ditunjukkan eksistensi titik tetap di ruang metrik lengkap.
Dilain pihak, Ran dan Reurings (2003) menunjukkan eksistensi titik tetap
pada himpunan terurut parsial dan kemudian dikembangkan oleh Nieto dan Lopez
(2006). Selanjutnya, Harjani dan Sadarangani (2009) mengembangkan di ruang
metrik terurut dengan menggunakan pemetaan kontraksi lemah dalam papernya
Fixed Point Theorems for Weakly Contractive Mappings in Partially Ordered Set.
Berdasarkan hasil tersebut Aydi (2011) menunjukkan eksistensi titik tetap di ruang
metrik parsial terurut dengan menggunakan pemetaan kontraksi lemah.
Metode yang digunakan pada penyusunan tesis ini adalah studi literatur (ka-
jian teori). Pembahasan pada penelitian ini dilakukan dengan terlebih dahulu mem-
pelajari konsep ruang metrik dan ruang metrik parsial meliputi definisi serta sifat-
sifat yang terdapat di dalamnya. Selain itu dipelajari juga himpunan terurut par-
sial serta pemetaan kontraksi lemah. Dalam pemetaan kontraksi lemah dipelajari
mengenai prinsip kontraksi Banach, pemetaan kontraksi lemah dan perumuman-
nya. Mengacu pada konsep ruang metrik, himpunan terurut parsial, serta konsep
pemetaan kontraksi, akan dipelajari eksistensi titik tetap di ruang metrik terurut
dengan menambahkan syarat fungsi kontinu pada ruang metriknya. Selanjutnya
apabila berlaku jika barisan naik {xn} konvergen ke x ? X maka xn ?X x untuk
setiap n, maka akan dipelajari eksistensi titik tetap di ruang metrik terurut.
Dari hasil yang telah diperoleh akan dipelajari eksistensi titik tetap di ruang
metrik terurut dengan menggunakan pemetaan kontraksi lemah. Dalam hal ini akan
dipelajari eksistensi titik tetap dengan menggunakan pemetaan kontraksi lemah
di ruang metrik terurut dengan menambahkan syarat fungsi kontinu pada ruang
metriknya serta apabila berlaku jika barisan naik {xn} konvergen ke x ? X ma-
ka xn ?X x untuk setiap n.
Dalam tesis ini juga diberikan konsep ruang metrik parsial serta pemetaan
kontraksi lemah. Berdasarkan konsep tersebut diformulasikan eksistensi titik tetap
seperti hasil yang telah diperoleh sebelumnya. Dengan menggunakan fungsi kon-
tinu akan ditunjukkan eksistensi titik tetap di ruang metrik parsial terurut dengan
menggunakan pemetaan kontraksi lemah. Selanjutnya apabila berlaku jika barisan
naik {xn} konvergen ke x ? X maka xn ?X x untuk setiap n, maka akan dipelajari
eksistensi titik tetap di ruang metrik parsial terurut dengan menggunakan pemetaan
kontraksi lemah.
Selanjutnya berdasarkan hasil tersebut akan dibuat perumuman dengan meng-
ganti pemetaan kontraksi lemah dengan perumuman pemetaan kontraksi lemah.
Jadi akan ditunjukkan eksistensi titik tetap di ruang metrik parsial dengan meng-
gunakan perumuman pemetaan kontraksi lemah dengan syarat fungsinya kontinu
atau berlaku jika barisan naik {xn} konvergen ke x ? X maka xn ?X x untuk
setiap n.
Pada penulisan tesis ini, penulis menggunakan sistematika sebagai berikut.
Pada bab ini dibahas mengenai latar belakang masalah, tujuan dan manfaat peneli-
tian, tinjauan pustaka, metode penelitian, dan sistematika penulisan yang akan di-
lakukan dalam penyusunan tesis.
Pada bab ini berisi landasan teori yang dipergunakan sebagai alat untuk membahas
Bab III. Landasan teori yang diberikan meliputi ruang metrik parsial, himpunan
terurut parsial, dan pemetaan kontraktif.
Pada bab ini berisi hasil yang diperoleh yaitu eksistensi titik tetap di ruang metrik
parsial terurut.
Pada bab ini berisi kesimpulan dari hasil penelitian dan saran untuk pengembangan
lebih lanjut.

Dalam tesis ini dibahas eksistensi titik tetap di ruang metrik terurut dengan
menggunakan fungsi f : X ? X naik monoton sehingga d(f(x), f(y)) ? cd(x, y)
untuk c ? [0, 1). Diberikan (X, d) ruang metrik lengkap dan X terurut parsial ter-
hadap �?X�. Diketahui fungsi f : X ? X naik monoton sehingga d(f(x), f(y)) ?
cd(x, y) untuk c ? [0, 1) dan setiap y ?X x. Jika terdapat x0 ? X sehingga
x0 ?X f(x0) dan f kontinu pada (X, d) maka f mempunyai titik tetap. Teorema ini
tetap benar untuk fungsi f yang tidak kontinu pada (X, d) asalkan terdapat barisan
naik monoton {xn} yang konvergen ke x ? X memenuhi xn ?X x untuk setiap
n. Untuk menunjukkan ketunggalan titik tetap dari hasil yang telah diperoleh perlu
ditambahkan syarat yaitu untuk setiap {x, y} ? X mempunyai batas atas atau batas
bawah. Berdasarkan hipotesis-hipotesis dari teorema di atas dan menggunakan
fungsi f : X ? X naik monoton sehingga d(f(x), f(y)) ? d(x, y) ? ?(d(x, y))
untuk setiap y ?X x dengan ? : [0,+?) ? [0,+?) fungsi kontinu dan naik
monoton serta memenuhi ?(t) = 0 jika dan hanya jika t = 0, dapat ditunjukkan
eksistensi dan ketunggalan titik tetap di ruang metrik parsial terurut.
Diberikan (X, p) ruang metrik parsial lengkap dan X terurut parsial ter-
hadap �?X�. Fungsi f : X ? X kontinu dan naik monoton sehingga d(f(x), f(y)) ?
d(x, y) ? ?(d(x, y)) untuk setiap y ?X x dengan ? : [0,+?) ? [0,+?) fungsi
kontinu dan naik monoton serta memenuhi ?(t) = 0 jika dan hanya jika t = 0.
Jika terdapat x0 ? X sehingga x0 ?X f(x0) dan f kontinu pada (X, p) maka f
mempunyai titik tetap. Teorema ini tetap benar untuk fungsi f yang tidak kon-
tinu pada (X, p) asalkan terdapat barisan naik monoton {xn} yang konvergen ke
x ? X memenuhi xn ?X x untuk setiap n. Untuk menunjukkan ketunggalan
titik tetap dari hasil yang telah diperoleh perlu ditambahkan syarat yaitu untuk se-
tiap {x, y} ? X mempunyai batas atas atau batas bawah. Berdasarkan hipotesis-
hipotesis dari teorema di atas menggunakan fungsi f : X ? X naik monoton
sehingga ?(d(f(x), f(y))) ? ?(d(x, y)) ? ?(d(x, y)) untuk setiap y ?X x den-
gan ?, ? : [0,+?) ? [0,+?) fungsi kontinu dan naik monoton serta memenuhi
?(t) = 0 = ? jika dan hanya jika t = 0, dapat ditunjukkan eksistensi titik tetap di
ruang metrik parsial terurut.
Dalam tesis ini penulis hanya membahas eksistensi titik tetap di ruang metrik
terurut parsial dengan menggunakan pemetaan kontraksi lemah dan perumuman-
nya. Oleh karena itu pembahasan untuk pemetaan yang lain dapat diteliti lebih
lanjut.

