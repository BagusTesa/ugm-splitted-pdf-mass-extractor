A. Latar Belakang
Kesehatan merupakan salah satu bagian dari pembangunan nasional.
Pembangunan kesehatan merupakan upaya bangsa Indonesia untuk
meningkatkan derajat kesehatan yang optimal dalam rangka peningkatan
kualitas dan taraf hidup, serta kesehatan dan kecerdasan seluruh bangsa
Indonesia. Salah satu wujud dari upaya meningkatkan derajat kesehatan
masyarakat tersebut adalah dengan adanya sarana kesehatan, salah satunya
adalah Rumah Sakit. Berdasarkan Undang � undang No. 44 Tahun 2009 tentang
rumah sakit, yang dimaksud dengan rumah sakit adalah institusi pelayanan
kesehatan yang menyelenggarakan pelayanan kesehatan perorangan secara
paripurna yang menyediakan pelayanan rawat inap, rawat jalan, dan gawat
darurat.
Dalam melaksanakan pelayanan kesehatan, rumah sakit harus dapat
mendokumentasikan setiap tindakan dan pengobatan yang telah diberikan
kepada pasien kedalam suatu dokumen yang disebut rekam medis. Menurut
Peraturan Menteri Kesehatan No. 269/MENKES/PER/III/2008 pasal (1), rekam
medis adalah berkas yang berisikan catatan dan dokumen tentang identitas
pasien, pemeriksaan, tindakan, dan pelayanan lain yang telah di berikan kepada
pasien. Menurut Abdelhak, dkk (2001) Rekam medis dikatakan bermutu apabila
rekam medis tersebut akurat, lengkap, dan dapat dipercaya, valid dan tepat
waktu. Salah satu bentuk pengolahan dalam rekam medis adalah
pendokumentasian serta pengodean (coding) diagnosis.
Menurut WHO (2002), �Coding is procedure that assigns a numeric code
to diagnostic and procedural data based on a clinical classification system�.
(Pengodean adalah sebuah prosedur pemberian kode numerik untuk diagnosis
dan data sistem yang didasarkan pada sistem klasifikasi klinik). Pelaksanaan
pengodean harus lengkap dan akurat sesuai dengan ICD � 10 (WHO, 2002).
Berdasarkan Keputusan Menteri Republik Indonesia No 50 Tahun 1988 tentang
pemberlakuan Klasifikasi Statistik Internasional Mengenai Penyakit Revisi
Kesepuluh, Badan Kesehatan Sedunia telah menetapkan �Internasional
Statistical Classification of Diseases and Related Health Problems Tenth
Revision (ICD-10) sebagai klasifikasi statistik internasional mengenai penyakit
revisi  yang akan digunakan oleh seluruh Negara anggota WHO, sehingga
Indonesia perlu menyesuaikan. Berdasarkan hal tersebut, Menteri Kesehatan
Republik Indonesia memberlakukan klasifikasi internasional mengenai penyakit
revisi kesepuluh (ICD-10) secara nasional di Indonesia sejak 13 Januari 1998.
Kualitas data terkode merupakan hal yang penting bagi kalangan tenaga
personel Manajemen Informasi Kesehatan, fasilitas asuhan kesehatan, dan para
profesional Manajemen Informasi Kesehatan. Ketepatan data diagnosis sangat
krusial di bidang manajemen data klinis, penagihan kembali biaya, beserta hal-
hal yang berkaitan dengan asuhan dan pelayanan kesehatan (Hatta, 2008). Hal
tersebut dikarenakan pengodean memiliki peran penting dalam manajemen di
rumah sakit. Ketepatan dalam pemberian kode penyakit akan berpengaruh
terhadap laporan yang akan dibuat oleh rumah sakit. Menurut Depkes RI (1997),
faktor-faktor yang sangat mempengaruhi dalam pengodean adalah tenaga medis
yang menetapkan diagnosis, tenaga rekam medis sebagai pemberi kode, tenaga
kesehatan lainya, sarana, kelengkapan berkas rekam medis dan kebijakan
rumah sakit. Faktor-faktor ini harus diperhatikan oleh pihak rumah sakit agar
dapat dihasilkan kode yang sesuai berdasarkan ICD-10.
Berdasarkan studi pendahuluan yang telah dilakukan oleh peneliti di
Rumah Sakit Panti Rapih Yogyakarta pada tanggal 1 Maret 2013, melalui hasil
wawancara dan hasil pengamatan selama studi pendahuluan diperoleh informasi
bahwa terdapat kode diagnosis yang tidak spesifik dan tidak sesuai dengan ICD-
10 pada berkas rekam medis pasien rawat inap obstetri dan ginekologi. Pada
proses awal penelitian, peneliti terlebih dahulu mengambil data diagnosis pada
lembar ringkasan masuk keluar pasien obstetri dan ginekologi sebanyak 30
diagnosis, dari analisis keakuratan kode tersebut diperoleh hasil sebesar 20%
kode tidak akurat .
Keakuratan kode diagnosis pada lembar ringkasan masuk dan keluar
obstetri dan ginekologi berpengaruh terhadap laporan statistik data keadaan
morbiditas pasien rawat inap, tetapi juga berpengaruh terhadap Laporan
Sepuluh Besar Penyakit. Apabila kode diagnosis tidak tepat akan menyebabkan
data yang dihasilkan mempunyai tingkat validasi data yang rendah,
menyebabkan ketidakakuratan dalam pembuatan laporan, dan mempengaruhi
kebijakan yang akan diambil oleh pihak manajemen rumah sakit. Sehingga
keadaan ini mendorong peneliti untuk mengetahui prosentase tingkat keakuratan
kode diagnosis pasien obstetri dan ginekologi di Rumah Sakit Panti Rapih
Yogyakarta. Selain itu di Rumah Sakit Panti Rapih Yogyakarta belum pernah
diadakan evaluasi mengenai topik ini. Untuk itu peneliti mengambil judul
Obstetri dan Ginekologi di Rumah Sakit Panti Rapih Yogyakarta.
B. Rumusan Masalah
Bagaimana pelaksanaan pengodean diagnosis pasien obstetri dan ginekologi
pada lembar ringkas masuk keluar di Rumah Sakit Panti Rapih Yogyakarta.
C. Tujuan
Mengetahui tingkat keakuratan kode diagnosis pada lembar ringkasan masuk
keluar pasien obstetri dan ginekologi di Rumah Sakit Panti Rapih
a. Mengetahui pelaksanaan pengodean diagnosis pada lembar ringkasan
masuk keluar pasien obstetri dan ginekologi di Rumah Sakit Panti Rapih.
b. Mengetahui prosentase keakuratan pengodean diagnosis pada lembar
ringkasan masuk keluar pasien obstetri dan ginekologi di Rumah Sakit
c. Mengetahui faktor-faktor penyebab ketidakakuratan pengodean diagnosis
pada lembar ringkas masuk keluar  pasien obstetri dan ginekologi di
D. Manfaat
Dapat digunakan sebagai bahan evaluasi bagi pihak rumah sakit dalam
menyusun kebijakan dan pelaksanaan pengodean yang berguna untuk
meingkatkan mutu rumah sakit.
Memberi masukan atau materi yang berharga sebagai bahan pembelajaran
bagi kemajuan pendidikan terutama yang berkaitan dengan keakuratan
pemberian kode pada berkas rekam medis.
a. Menambah pengalaman dan pengetahuan dibidang rekam medis
khususnya dalam pelaksanaan pengodean diagnosis di rumah sakit.
b. Mengetahui  perbandingan antara teori yang didapat di bangku
perkuliahan dengan kenyataan di rumah sakit.
4. Bagi Peneliti lain
Dapat digunakan sebagai dasar, acuan, dan bahan referensi bagi peneliti
yang serupa dan juga dapat digunakan untuk pengembangan penelitian yang
lebih kompleks.
E. Keaslian Penelitian
Penelitian tentang �Keakuratan Kode Diagnosis Pada Lembar Ringkasan
Masuk Keluar Pasien Obstetri dan Ginekologi di Rumah Sakit Panti Rapih
Yogyakarta�  belum pernah dilakukan oleh peneliti lain, tetapi ada penelitian
yang hampir sama, antara lain :
1. Penelitian dengan judul � Ketepatan Kode Diagnosis Pada Berkas Rekam
Medis Pasien Rawat Inap Diabetes Mellitus Berdasarkan ICD-10 di RSUD
Muntilan Kabupaten Magelang� Andriani (2011). Penelitian ini menggunakan
metode deskriptif kualitatif dengan rancangan cross sectional. Penelitian ini
mengemukakan tentang proses pemberian kode Diagnosis Diabetes Mellitus,
prosentase ketepatan kode diagnosis Diabetes Mellitus, dan faktor penyebab
ketidaktepatan kode diagnosis Diabetes Mellitus berdasarkan ICD-10. Dari
124 sampel, prosentasee ketepatan kode diagnosis  penyakit diabetes
mellitus menunjukan untuk kriteria A (kode tepat dengan kode dikoreksi
sampai digit terakhir) sebesar 25,81 %, kriteria B (kode tepat dengan kode
koreksi hanya sampai digit ke tiga) sebesar  67,74%, kriteria C (kode tidak
tepat dengan kode dikoreksi) sebesar 4,03%, kriteria D (kode tidak tepat di
berkas rekam medis) sebesar 2,42%. Persamaan penelitian ini dengan
Andriani, (2011) yaitu salah satu tujuannya menghitung prosentase
ketepatan kode diagnosis. Perbedaannya yaitu pada jenis kasus yang
diambil. Penelitian Andriani meneliti kasus untuk penyakit Diabetes Mellitus.
2. Penelitian dengan judul � Evaluasi Ketepatan Kodefikasi Diagnosis Utama
Pasien Rawat Inap Berdasarkan ICD-10 di Rumah Sakit Pertamina Cirebon�
Sadiyah (2004). Dari 180 kasus pasien rawat inap bulan Januari 2004 yang
diteliti, ketepatan kodefikasi diagnosis utama dengan ICD-10 hanya sebesar
36,70%. Persamaanya yaitu mengangkat tema tentang ketepatan pemberian
kode, selain itu persamaan yang lain pada jenis penelitian yang diambil yaitu
penelitian deskriptif dengan pendekatan kualitatif. Sedangkan perbedaanya
adalah pada lokasi, tujuan dan pada objek penelitian, penelitian ini lebih
mengarah pada keakuratan kode diagnosis pada lembar ringkas masuk
keluar pasien obstetri dan ginekologi.
Lembar Ringkasan Masuk Keluar Ibu, Bayi, dan Anak Di RSIK Sadewa
Yogyakarta�. Penelitian ini menggunakan penelitian deskriptif kualitatif dan
rancangan fenomenologis. Hasil penelitiannya yaitu pelaksanaan pengodean
di RSIK Sadewa Yogyakarta dilakukan oleh petugas rekam medis. Fasilitas
pengodean menggunakan buku pintar dan ICD-10. Proses pengodean
dilakukan setelah berkas rekam medis selesai di assembling. Persamaannya
pada tujuannya yaitu mengetahui pelaksanaan pengodean diagnosis pada
lembar ringkasan masuk dan keluar. Perbedaanya yaitu pada metode
penelitian. Penelitian ini menggunakan metode penelitian deskriptif kualitatif.

A. Kesimpulan
1. Pelaksanaan pengodean diagnosis pada lembar ringkasan masuk keluar
pasien rawat inap obstetri dan ginekologi di Instalasi Rekam Medis
Rumah Sakit Panti Rapih Yogyakarta sudah terlaksana dengan baik.
2. Keakuratan pengodean diagnosis pada lembar ringkasan masuk keluar
pasien rawat inap obstetri dan ginekologi di Rumah Sakit Panti Rapih
Yogyakarta belum tercapai secara maksimal, hasil analisis keakuratan
kode obstetri dan ginekologi terdapat 44,56% kode yang sudah sesuai
dengan ICD-10.
3. Faktor penyebab ketidakakuratan kode diagnosis pada lembar ringkasan
masuk keluar pasien rawat inap obstetri dan ginekologi yaitu : faktor
manusia (SDM), update ICD-10 dan database, belum dilakukannya
evaluasi mengenai keakuratan pengodean di Rumah Sakit Panti Rapih
B. Saran
1. a. Sebaiknya petugas pengodean lebih spesifik dalam menentukan kode
diagnosis.
b. Sebaiknya petugas pengodean menggunakan ICD-10 yang sudah
di-update dan memperbaharui database yang digunakan sebagai alat
pengodean.
2. Sebaiknya petugas rekam medis memberikan sosialisasi kepada dokter
baru mengenai peraturan tentang daftar nama singkatan penyakit dan
peraturan lain yang ada di rekam medis.
3. Sebaiknya Kepala Instalasi Rekam Medis segera merencanakan
evaluasi/audit pengodean.

