Seiring dengan perkembangan teknologi informasi yang semakin maju.
Memungkinkan untuk melakukan terobosan baru dengan memanfaatkan teknologi
informasi dalam bidang perdagangan. Dalam bidang perdagangan persaingan
bisnis untuk saat ini sudah sangat ketat sekali. Dengan persaingan yang semakin
ketat tersebut perlu adanya kreatifitas untuk memenangkan pasar. Salah satunya
dengan memanfaatkan e-commerce. (Suyanto, 2003)
Penjualan secara online atau biasa disebut dengan e-commerce sangat
memberikan kemudahkan dalam hal penjualan dan pemasaran produk
dibandingkan dengan penjualan secara manual, tanpa terbatas waktu baik pemilik
toko maupun pembeli dapat melakukan transaksi. Disamping itu pemilik toko juga
dapat dengan mudah untuk memeriksa data hasil penjualan. Apalagi untuk  masa
sekarang teknologi informasi seperti internet bukan hal yang tabu lagi bagi
masyarakat kita, penting bagi para pelaku wirausaha memiliki toko online untuk
meningkatkan layanan penjualan kepada konsumen. (Suyanto, 2003)
Toko Godonkstore merupakan toko yang menjual berbagai macam kaos
bola, seperti kaos bola real madrid, kaos bola barcelona, kaos bola chelsea.
Pengelolaan data yang terjadi selama ini masih dilakukan secara manual dengan
menggunakan buku untuk pencatatan penjualan produk. Disamping itu penjualan
yang dilakukan pada toko Godonkstore bekerjasama dengan pihak reseller untuk
menjualkan produknya. Komunikasi yang terjadi di toko Godonkstore  dengan
para reseller belum berjalan efektif dan efisien. Khususnya untuk pemberitahuan
produk terbaru kepada seluruh anggota reseller secara bersamaan. Maka dari itu,
perlu adanya integrasi e-commerce dengan teknologi terbarukan, dalam hal ini
pemilik toko Godonkstore menginginkan sistem yang terbangun nantinya dapat
terintegrasi dengan layanan informasi pesan, seperti SMS Gateway.
Sesuai dengan latar belakang yang telah diuraikan sebelumnya, maka perlu
dibangun sebuah sistem informasi penjualan kaos bola pada �Toko Godonkstore�
yang terintegrasi dengan SMS Gateway. Dengan dibangunkan sistem tersebut
diharapkan mampu menyelesaikan permasalah-permasalahan yang terjadi pada
toko Godonkstore.
Perumusan masalah dalam penelitian ini, bagaimana menghasilkan sebuah
sistem informasi yang dapat digunakan sebagai media pemasaran, transaksi
penjualan secara online, penyampaian informasi secara tepat dan cepat melalui
pesan SMS, serta dapat melakukan proses pengolahan data yang lebih praktis.
Batasan masalah dalam penelitian ini diperlukan untuk memudahkan
dalam pembahasan agar penelitian nantinya bisa lebih terarah dan fokus pada
pokok permasalahan.
Adapun batasan masalah dalam penelitian ini adalah sebagai berikut :
1. Sumber data yang digunakan dari toko Godonkstore
2. Sistem dirancang berbasis web
3. Sistem tidak membuat laporan penjualan dalam bentuk print out
4. Tidak membahas mengenai masalah SEO (search engine optimization).
5. Layanan informasi via SMS hanya untuk sebatas pemberitahuan ketika
barang yang dipesan sudah diproses, dipacking, dikirim dan pemberitahuan
produk baru keseluruh reseller yang terdaftar.
Pada penelitian ini bertujuan untuk membuat sistem informasi penjualan
secara online yang dapat melakukan proses transaksi dan penyampaian informasi
melalui SMS Gateway  pada toko Godonkstore.
Ada beberapa manfaat yang ingin dicapai dari penelitian sistem informasi
ini, yaitu sebagai berikut :
1. Membantu pemilik toko untuk memperkenalkan atau memasarkan produk
secara online kepada masyarakat luas.
2. Memudahkan pemilik toko dalam pengolahan data penjualan produk
3. Memudahkan proses transaksi pembelian yang dilakukan konsumen.
4. Dapat memberikan layanan informasi yang cepat kepada konsumen melalui
SMS broadcast.
Metode penelitian yang dilakukan dalam proses penyusunan Tugas Akhir
ini adalah sebagai berikut :
Pengumpulan data dilakukan untuk menunjang proses penelitian yang lebih
mudah, beberapa metode yang digunakan dalam pengumpulan data antara
lain :
a. Observasi
Metode ini merupakan metode yang dilakukan dengan melihat atau
membaca buku arsip yang ada pada toko Godonkstore secara langsung.
Gunanya untuk mengetahui proses bisnis yang terjadi sehingga
permasalahan yang ada pada toko Godonkstore dapat ditemukan sehingga
memudahkan dalam proses pemecahan masalah.
b. Studi Literatur
Studi literatur dilakukan dengan mencari referensi buku-buku, situs
website, modul perkuliahan, tugas akhir yang terkait dengan permasalahan
sistem informasi penjualan untuk sebagai pembanding.
c. Wawancara
Wawancara dalam penelitian ini dilakukan dengan terjun langsung ke
kantor toko Godonkstore untuk bertanya secara langsung dengan pemilik
toko. Tujuannya untuk memperoleh informasi data yang lebih detail dan
jelas, sehingga nantinya infomasi yang dihasilkan akan sesuai dengan yang
diharapkan oleh pemilik toko Godonkstore.
2. Analisis dan Perancangan antarmuka Sistem
Dalam pembangunan sistem ini dilakukan dengan tahap-tahap antara lain :
a. Analisis Sistem
Analisis sistem dilakukan dengan melakukan analisis kebutuhan sistem
yang akan dibangun sesuai keinginan daripada pemilik toko Godonkstore.
b. Perancangan antarmuka Sistem
Dalam proses perancangan antarmuka sistem ini merupakan kelanjutan
dari analisis sistem, desain database, perancangan antarmuka desain antar
muka pengguna.
Implimentasi merupakan tahapan pembangunan sistem dimana perancangan
antarmuka sistem tersebut telah selesai dibuat.
Merupakan proses yang dilakukan untuk menguji sistem yang telah selesai
dibuat, nantinya diharapkan mampu menemukan (bug) atau celah kesalahan
dalam sistem yang dibangun.
5. Dokumentasi dan pelaporan
Tahapan ini berguna untuk membuat dokumentasi laporan penelitian dan
dokumentasi dari sistem yang telah dibuat.
Untuk memberikan gambaran yang lebih jelas secara menyeluruh dalam
proses pembuatan penelitian Tugas Akhir kali ini. Maka sistematika penulisan
laporan ini dibagi menjadi beberapa bab sebagai berikut :
Dalam bab ini membahas mengenai latar belakang masalah, rumusan masalah,
tujuan penelitian, manfaat penelitian, metodologi penelitian, serta sistematika
penelitian.
Dalam bab ini membahas mengenai uraian tentang informasi dari penelitian-
penelitian yang dilakukan pada penelitian sebelumnya, yang berguna untuk
menjadi referensi penelitian yang sedang dilakukan.
Dalam bab ini membahas mengenai teori-teori ilmu yang menjadi landasan dalam
penelitian yang sedang dilakukan.
Dalam bab ini membahas mengenai konsep analisi dan perancangan antarmuka
sistem yang menjadi dasar pembuatan sistem. Pembahasan ini meliput alur kerja
yang berjalan dalam sistem sampai perancangan antarmuka antar muka pengguna.
Dalam bab ini membahas mengenai tentang penerapan dari bab sebelumnya yaitu
mulai dari analisis dan perancangan antarmuka sistem yang dibuat pada
pembangunan sistem. Dengan menampilkan antar muka pengguna dan juga
disertai dengan kode alur logika sistem.
Dalam bab ini membahas mengenai tentang pengujian sistem yang telah dibangun
dengan menyertakan alur kerja sistem tersebut.
Dalam bab ini membahas mengenai akhir dan pembuatan laporan penelitian yang
berisi kesimpulan dan saran untuk pembangunan sistem selanjutnya.

Berdasarkan pembahasan mengenai sistem informasi penjualan kaos bola
pada �Toko Godonkstore� pada bab sebelumnya, maka dapat diambil dua
kesimpulan antara lain :
1. Sistem informasi penjualan kaos bola pada �Toko Godonkstore� berhasil
dibangun, sistem ini dapat digunakan untuk pengolahan data, pendaftaran
reseller dan penjualan online.
2. Sistem ini terintegrasi dengan layanan SMS Gateway sehingga
mempermudah dalam monitoring dan penyebaran iklan keseluruh anggota
reseller.
Sistem yang dibangun ini masih mempunyai beberapa kekurangan, berikut
ini penulis paparan dua saran untuk pengembangan antara lain :
1. Sistem informasi ini perlu dikembangan fitur untuk biaya pengiriman
sehingga ketika membeli produk bisa langsung tergenerate keseluruhan
biaya baik biaya untuk membeli produk dan biaya pengiriman.
2. Untuk memudahkan dalam proses pembelian perlu juga diintegrasikan
dengan pembayaran online seperti Paypal, Kredit card.

