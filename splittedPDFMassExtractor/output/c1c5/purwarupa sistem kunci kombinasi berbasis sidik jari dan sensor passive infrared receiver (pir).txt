Asrama atau kost merupakan rumah tinggal sementara bagi mahasiswa/i
yang merantau keluar kota untuk menuntut ilmu di sekolah maupun perguruan
tinggi. Selain untuk menuntut ilmu, asrama atau kost dimanfaatkan juga sebagai
tempat tinggal sementara bagi karyawan/ti yang bekerja jauh dari daerah asalnya.
Maraknya pembangunan asrama atau kost di beberapa kota besar, misalnya
Jakarta, Yogyakarta, dan Surabaya membuat pemilik asrama atau kost
memberikan fasilitas yang memadai untuk mengatasi persaingan bisnis.
Fasilitas yang memadai misalnya kamar tidur yang luas disertai kamar
mandi dalam, sambungan internet, halaman parkir yang luas dan adanya jam
malam. Tetapi fasilitas tersebut tidak didukung oleh sistem keamanan yang
memadai, sehingga banyak tindakan pencurian yang terjadi. Pintu yang
merupakan akses utama untuk keluar masuk penghuni juga tidak dijaga dengan
baik. Meskipun terdapat penjaga yang memantau pintu tersebut, tetapi hal ini
tidak efisien karena pemantauan tidak bisa dilakukan selama 24 jam.
Maka dari itu diperlukan adanya sistem untuk membantu pemantauan
akses pintu. Sistem ini bekerja untuk menghindari orang lain yang masuk tanpa
izin. Sistem ini nantinya akan dipasang sebelum pintu masuk supaya lebih aman
dan hanya yang mempunyai hak akses tersebut boleh diizinkan untuk masuk. Hal
ini menjadi pertimbangan dan seiring dengan kemajuan perkembangan teknologi
menyebabkan timbulnya pemikiran untuk menciptakan suatu sistem keamanan
berlapis berupa kunci kombinasi yang memanfaatkan teknologi sidik jari dan
pendeteksi orang.
Rumusan masalah dari tugas akhir ini sesuai uraian latar belakang yaitu
bagaimana membuat sistem keamanan berupa kunci kombinasi dengan
memanfaatkan teknologi sidik jari dan pendeteksi orang dilengkapi dengan basis
data yang mampu menampilkan hasil pengujian dan penyimpanan serta dapat
dipantau melalui antarmuka yang nantinya digunakan sebagai obyek penelitian
dan dapat dihandalkan.
Pada sistem ini permasalahan dibatasi oleh :
1. Satu sampel jari tangan hanya untuk satu penghuni.
2. Pola sidik jari yang teridentifikasi adalah pola sidik jari yang bersih, tidak
berminyak dan normal tanpa luka.
3. Pencocokan pola sidik jari dilakukan langsung oleh modul sidik jari.
4. Sensor Passive Infrared Receiver (PIR) hanya mendeteksi keberadaan
seseorang dengan jarak 0,25 meter sampai 2 meter.
1.4 Tujuan dan Manfaat Penelitian
Tujuan dari penelitian ini adalah untuk untuk meningkatkan sistem
keamanan dengan menggunakan pola sidik jari yang telah terdaftar sehingga
sistem akan mengizinkan atau menolak akses masuk kost atau asrama.
Manfaat dari penelitian ini adalah memudahkan pemilik kost atau asrama
melakukan pemantauan akses penghuni kost dan mencegah terjadinya pencurian
di lingkungan asrama atau kost.
Untuk menyelesaikan penelitian ini, maka dilakukan metode-metode
sebagai berikut :
1. Studi literatur
Mempelajari dan mengambil data dari pustaka, referensi kuliah,
mempelajari artikel, makalah, jurnal, karya tulis, serta buku-buku yang
terkait sistem dan teknologi-teknologi yang mendukung sistem kunci
kombinasi.
2. Pengambilan, dan pemrosesan data
Pengambilan data dilakukan dengan menggunakan sepuluh sidik jari
penghuni. Pemrosesan data dilakukan dengan tujuan untuk menghasilkan
data berupa laporan yang akurat.
3. Implemetasi sistem
Sistem yang telah dirancang lalu diimplemetasikan dalam bentuk software
pengolah data serta hardware pengambilan sample sidik jari dan
implementasi GUI (Graphical User Interface).
4. Pembahasan dan kesimpulan
Pembahasan dari pengambilan sampel sidik jari, pengolahan data sampel
sampai hasil pengujian berupa laporan dan mengambil kesimpulan dari
sistem yang diimplementasikan.
Secara garis besar penulisan laporan penelitian ini terdiri dari tujuh bab,
antara lain sebagai berikut :
Bab ini berisi penguraian tentang latar belakang masalah yang diteliti,
batasan masalah pada penelitian, tujuan penelitian, metode penulisan yang
dilakukan serta sistematika penulisan laporan penelitian.
Bab ini berisi penjelasan mengenai penelitian yang sesuai bidang yang
telah diterapkan sebelumnya, serta menghubungkannya dengan penelitian
yang akan dilakukan.
Bab ini berisi tentang penjelasan dan dasar teori yang meliputi biometrika,
biometrika sidik jari, sensor, bahasa pemrograman, dan basis data.
Bab ini berisi penjelasan tentang rancangan sistem yang akan dibuat
meliputi rancangan sistem keseluruhan, rancangan rangkaian elektronik,
rancangan program elektronik, rancangan program fingerprint, rancangan
program pengujian, dan rancangan GUI (Graphical User Interface).
Bab ini menjelaskan semua implementasi sistem yang berasal dari
rancangan yang telah dibuat sebelumnya. Meliputi implementasi sistem
keseluruhan, implementasi rangkaian elektronik, implementasi program
elektronik, implementasi program fingerprint, implementasi program
pengujian, dan implementasi GUI (Graphical User Interface).
Bab ini berisi pengujian dari sistem yang telah dibuat dan dilakukan
pembahasan secara terperinci dari proses pengujian tersebut. Hasil dari
pengujian ini didukung dengan gambar saat sistem dijalankan.
Bab ini berisi kesimpulan atas penelitian yang telah dilakukan, serta
memberikan saran untuk pengembangan sistem lebih lanjut.

Dari penelitian yang  telah dilakukan dapat diambil kesimpulan sebagai
berikut :
1. Modul fingerprint dapat mendeteksi obyek dengan keakuratan sebesar
53,14%. Hal ini dipengaruhi oleh sudut kemiringan yang semakin
besar sehingga modul tidak dapat mendeteksi obyek.
2. Telah berhasil dibuat sebuah sistem kunci kombinasi berbasis sidik jari
dan sensor Passive Infrared Receiver dengan Microsoft Visual Basic
6.0 sebagai Graphical User Interface yang diimplemetasikan dalam
sistem keamanan.
3. Motor servo bergerak membuka pintu jika jumlah orang yang terhitung
oleh sensor fotodioda sama dengan jumlah sidik jari yang terverifikasi
oleh scanner optic.
4. Hasil pengujian data dapat berupa laporan berisi nama dan waktu yang
menunjukkan akses masuk penghuni sehingga pemilik kost mudah
melakukan pemantauan akses penghuni dan mencegah terjadinya
pencurian di lingkungan asrama atau kost.
Pada penelitian ini masih terdapat beberapa hal yang perlu disempurnakan.
Berikut saran-saran yang penulis sampaikan untuk penelitian yang serupa.
1. Sensor fotodioda diharapkan dapat melakukan perhitungan mundur untuk
akses pintu keluar.
2. Sensor fotodioda sebaiknya diletakkan sebelum sensor Passive Infrared
Receiver agar langsung mendeteksi jumlah orang.

