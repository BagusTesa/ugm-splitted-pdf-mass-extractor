1.1.  Latar Belakang
Saat ini banyak masyarakat di Indonesia yang sudah menyadari pentingnya
asuransi, meskipun jika dibandingkan dengan negara lain, Indonesia masih kalah
jauh. Kebanyakan masyarakat mengikuti asuransi jiwa karena mereka ingin
mengurangi resiko kerugian finansial ketika mereka atau salah satu anggota
keluarga yang menjadi tulang punggung meninggal dunia. Jika tulang punggung
keluarga meninggal, maka keluarga tersebut akan mengalami kesulitan finansial.
Dengan membeli produk asuransi jiwa, kesulitan finansial tersebut dapat
berkurang karena keluarga pemegang polis akan menerima manfaat atau santunan
dari perusahaan asuransi yang produknya dibeli.
Dilain pihak, ketika ada salah satu pemegang polis asuransi jiwa yang
meninggal, perusahaan asuransi harus mengeluarkan sejumlah uang untuk
membayar manfaat atau santunan yang telaah dijanjikan. Pada dasarnya kematian
tidak dapat diketahui datangnya. Dalam dunia asuransi jiwa kematian seseorang
merupakan suatu variable random. Kematian seseorang bisa dating sewaktu-waktu
begitu juga dengan kematian seorang pemegang polis. Dengan begitu perusahaan
asuransi juga dapat mengeluarkan sejumlah uang sewaktu waktu jika ada salah
satu pemegang polis asuransinya yang meninggal. Perusahaan asuransi bisa saja
mengalami kerugian kettika ada pemegang polis yang meninggal, sementara
perusahaan asuransi tidak mempunyai dana, padahal harus mengeluarkan
sejumlah dana untuk manfaat.
Salah satu penyebab kerugian adalah karena para pemegang polis membeli
produk asuransi jiwa dengan premi yang dibayarkan tiap periode waktu. Jika
pemegang polis membayar dengan premi sekali bayar atau sebesar nilai sekarang (
present value ) dari manfaat yang telah diperjanjikan, perusahaan asuransi akan
2
mempunyai dana yang sama besarnya dengan manfaat pada saat jatuh
tempo. Jika pemegang polis asuransi jiwa membeli produk dengan cara membayar
premi disetiap periode, maka kerugian perusahaan asuransi adalah selisih dari
nilai sekarang ( present value ) manfaat dengan akumulasi premi yang pada waktu
tertentu.
Dibanyak bidang usaha lain, cadangan diartikan kelebihan sejumlah dana
yang sebenarnya tidak terpakai tetapi dapat dipakai sewaktu waktu jika terjadi
suatu kendala. Misalnya, suatu restoran mempunyai sejumlah dana cadangan, dan
tiba-tiba restoran tersebut kebakaran, cadangan dana tersebut bisa digunakan
untuk membangun kembali bangunan restoranya. Lain halnya dengan didunia
asuransi, cadangan diartikan sejumlah dana yang harus ada pada waktu tertentu
agar perusahaan asuransi dapat memenuhi kewajiban sejumlah santunan yang
telah diperjanjikan pada saat kontrak polis berakhir. Bisa juga dikatakan,
cadangan merupakan dana yang harus ada supaya terjadi keseimbangan antara
nilai sekarang ( present value ) manfaat dengan akumulasi premi yang pada waktu
tertentu. Hal ini sering disebut prinsip ekuivalensi. Prinsip inilah yang akan
menjadi dasar perhitungan cadangan premi di bahasan selanjutnya.
Cadangan premi merupakan salah satu bentuk pertanggung jawaban
departemen aktuaria dalam perusahaan asuransi. Cadangan premi juga bisa
menggambarkan keadaan keuangan dari perusahaan asuransi, sesuai dengan
Peraturan Menteri Keuangan Nomor 53/PMK.010/2012 yang menyatakan bahwa
cadangan premi bisa disebut juga dengan kesehatan keuangan perusahaan
asuransi. Seorang aktuaris atau departemen aktuaria harus dapat menghitung
besarnya cadangan yang tepat sehingga perusahaan tidak salah tafsir tentang
berapa dana yang dibutuhkan. Banyak metode yang dapat digunakan untuk
menghitung cadangan premi, ada metode cadangan premi bersih yang menghitung
tanpa mengikutsertakan beban atau biaya biaya perusahaan asuransi. Ada juga
cadangan premi kotor yang perhitunganya memasukan unsur biaya kedalamnya,
serta cadangan biaya yang hanya menghitung unsur biaya saja, yang mana biaya
tersebut dibagi menjadi dua yaitu biaya awal tahun dan biaya tiap tahun. Namun,
3
cadangan biaya akan bernilai negatif karena premi biaya mengandung unsur biaya
awal tahun sementara biaya awal tahun sudah harus dibayar diawal kontrak polis.
Besarnya biaya awal tahun lebih besar daripada premi biaya, sehingga diawal
tahun perusahaan asuransi perlu tambahan dana yang bukan berasal dari premi
yang dibayar pemegang polis atau dengan kata lain membutuhkan dana pinjaman
untuk membayar biaya awal tahun. Pinjaman tersebut akan dilunasi oleh
kelebihan dana yang diperoleh dari premi biaya yang terkumpul. Cadangan premi
kotor nilainya akan sama dengan cadangan premi bersih ditambah cadangan
biaya. Dengan nilai cadangan biaya yang bernilai negatif, maka cadangan premi
kotor akan lebih kecil dari cadangan premi bersih. Hal tersebut mengakibatkan
kerancuan dalam menentukan dana yang sebenarnya dibutuhkan oleh perusahaan
asuransi. Terdapat metode peritungan cadangan lain yang bisa lebih pasti dalam
menentukan besarnya dana yang sebenarnya dibutuhkan oleh perusahaan asuransi,
yaitu metode perhitungan cadangan full preliminary term atau bisa disebut
cadangan awal penuh. Dengan perhitungan cadangan full preliminary term
perusahaan asuransi tidak membutuhkan dana pinjaman yang digunakan untuk
membayar biaya awal tahun.  Karena cadangan biaya pada metode full
preliminary term akan bernilai nol sehingga cadangan premi bersih nilainya sama
dengan cadangan premi kotor. Jadi perusahaan asuransi mendapatkan nilai
besaran dana yang pasti agar perusahaan asuransi dapat memenuhi kewajibanya
diakhir kontrak polis.
1.2. Batasan Masalah
Cakupan ilmu aktuaria sangat luas, jika penulis memasukan semuanya,
maka tulisan ini akan menjadi panjang sekali. Untuk itu penulis sengaja membuat
batasan batasan masalah agar penulisan ini lebih terarah dan tepat sasaran.
Batasan batasan masalah yang penulis buat sebagai berikut:
1. Modifikasi cadangan premi dengan metode full preliminary term pada
asuransi jiwa dwiguna untuk manfaat yang tetap diakhir tahun polis.
4
2. Manfaat dibayarkan diakhir tahun kematian, sehingga yang digunakan
ialah asumsi model diskrit.
3. Peluang hidup dan kematian seseorang berdasar pada tabel mortalita
Indonesia 2011.
4. Dasar perhitungan cadangan premi menggunakan metode prespektif.
1.3. Tujuan Penulisan
Setiap yang dilakukan harus mempunyai tujuan, begitu juga yang penulis
lakukan. Termasuk tujuan penulisan ini dan tujuan perhitungan modifikasi
cadangan premi dengan metode full preliminary term. Tujuan yang penulis buat
antara lain :
1. Memenuhi syarat kelulusan derajat sarjana sains proram studi statistika di
Fakultas MIPA Universitas Gadjah Mada
2. Mengetahui dan memahami cara perhitungan modifikasi cadangan premi
dengan metode full preliminary term
3. Mengetahui dan memahami perbedaan perhitungan cadangan metode
prosperktif dengan perhitungan cadangan premi metode full preliminary
term
4. Mengetahui dan memahami manfaat dari perhitungan modifikasi cadangan
premi dengan metode full preliminary term
1.4. Tinjauan Pustaka
Konsep tentang asuransi dan pembayaranya dijelaskan oleh Bowers, dkk
dalam bukunya “ Actuarial Mathematics(1997) “. Dalam buku Actuarial
Mathematics dijelaskan bagaimana menghitung premi tunggal bersih, anuitas,
premi serta cadangan preminya. Perhitungan cadangan premi kotor dan modifikasi
perhitungan cadangan premi metode full preliminary term juga dijelaskan dalam
buku tersebut. Penulis menggunakan jurnal dari komite pendidikan Society of
Actuaries yang berjudul “Suplementary Notes For Actuarial Mathematics For Life
5
Contingent Risk “ oleh Hardy dkk. (2011) sebagai tembahan referensi untuk
perhitungan cadangan premi kotor dan perhitungan cadangan dengan metode full
preliminary term.
1.5. Metode Penulisan
Metode yang digunakan dalam skripsi ini lebih kepada studi literature
secara sistematis yang dipelajari dari buku-buku di perpustakaan, catatan-catatan
selama perkuliahan, maupun media lain seperti internet serta jurnal-jurnal yang
penulis gunakan sebagai referensi penulisan skripsi ini.
1.6. SISTEMATIKA PENULISAN
Skripsi ini disusun dengan sistematika sebagai berikut :
BAB I  PENDAHULUAN
Sebagai pendahuluan yang menggambarkan keseluruhan bahasan bahasan
yang ada pada bab selanjutnya, bab ini terdiri dari latar belakang, batasan
masalah, tujuan penelitian, manfaat penelitian, metode penelitian, tinjauan pustaka
dan sistematika penulisan.
BAB II DASAR TEORI
Berisi tentang dasar dasar yang digunakan untuk menghitung cadangan
premi bersih metode prospektif dan yang digunakan dalam menghitung cadangan
premi metode full preliminary term, seperti tingkat suku bunga, nilai sekarang dan
anuitas.
BAB III MODIFIKASI CADANGAN PREMI METODE FULL PRELIMINARY
TERM PADA ASRANSI JIWA DWIGUNA MODEL DISKRIT
6
Berisi tentang perhitungan cadangan premi biaya dan cadangan premi
kotor serta perhitungan modifikasi premi dan cadangan premi dengan metode
metode full preliminary term.
BAB IV STUDI KASUS
Pada bab ini, membahas sebuah studi kasus berupa asuransi jiwa dwiguna
yang akan dihitung cadangan premi bersih, cadangan premi kotor dan cadangan
premi menggunakan metode full preliminary term.
Bab ini berisi tentang kesimpulan dari perhitungan cadangan dengan
metode full preliminary term berupa kelebihan dan manfaat menggunakan metode
tersebut. Pada bab ini juga berisi saran-saran yang berisi hal yang belum dibahas
ada penulisan skripsi ini.

5.1. Kesimpulan
1. Perhitungan cadangan metode full preliminary term menghasilkan satu
nilai cadangan yang pasti karena nilai cadangan premi bersih full
preliminary term sama dengan nilai cadangan premi kotor full preliminary
term.
2. Perhitungan cadangan menggunakan full preliminary term dapat
membayar biaya awal periode ( initial expenses ) secara penuh pada awal
periode.
3. Jika initial expenses lebih besar dari selisih antara ?fpt dengan ?fpt, maka
nilai cadangan premi full preliminary term diawal periode bernilai positif
dan cadangan premi full preliminary term ditahun selanjutnya akan lebih
besar daripada cadangan premi kotor prospektif.
4. Jika initial expenses lebih besar dari selisih antara ?fpt dengan ?fpt, maka
nilai cadangan premi full preliminary term diawal periode bernilai negatif
dan cadangan premi full preliminary term ditahun selanjutnya akan lebih
kecil daripada cadangan premi kotor metode prospektif.
5. Perhitungan cadangan metode full preliminary term lebih baik digunakan
jika premi kotor lebih kecil dari pada premi kotor full preliminary term.
5.2. Saran
1. Mesih banyak metode perhitungan cadangan yang bisa dieksplorasi.
2. Perhitungan cadangan bisa menggunakan suku bunga yang bergerak
seperti CIR dan vasicek.

