1.1 Latar Belakang
Wedding Organizer adalah suatu jasa yang memberikan pelayanan khusus
secara pribadi membantu calon pengantin dan keluarga calon pengantin dari mulai
tahan perencanaan (planning) sampai tahap pelaksanaan. Wedding Organizer
memberikan informasi mengenai berbagai macam hal yang berhubungan dengan
acara pernikahan dan membantu merumuskan segala hal yang dibutuhkan pada
saat pernikahan. Wedding Organizer memfasilitasi, negosiasi, dan koordinasi
dengan pihak gedung dan segala sesuatu yang dibutuhkan pada saat acara
pernikahan berlangsung.
Larasati Griya Rias & Dekorasi merupakan suatu wedding organizer yang
biasa menangani acara-acara pernikahan dan sejenisnya. Larasati Griya Rias &
Dekorasi merupakan wedding organizer yang cukup terkenal di daerah Blora,
Pati, Semarang, dan sekitarnya. Untuk memperluas cakupan wilayah pelayanan,
Larasati Griya Rias & Dekorasi tentu harus memiliki media promosi dan
pencatatan pemesanan yang baik dan rapi.
Berdasarkan data pada tahun 2012 Larasati Griya Rias & Dekorasi hanya
menangani rata-rata 10 sampai 20 event pernikahan tiap bulan di kota Blora,
Rembang, Semarang, dan Pati. Agar dapat memperbanyak jumlah event, maka
wilayah layanan di kota lain diperluas.
Selain itu, sebagai wedding organizer yang berkembang, Larasati Griya
Rias & Dekorasi dikelola oleh pemilik saja, sehingga sering dijumpai masalah
calon pelanggan tidak dapat menemui ataupun menelpon pemilik karena sedang
tidak berada di tempat untuk mengurus persiapan-persiapan di tempat lainnya.
Web merupakan salah satu  cara untuk memudahkan pemesanan dengan calon
pelanggan.
2
Permasalah di atas memberikan gagasan untuk membuat Sistem Informasi
Pemesanan Paket Pernikahan pada Larasati Griya Rias dan Dekorasi. Dimana
calon pelanggan dapat melakukan pemesanan paket pernikahan secara online agar
pelanggan dapat melakukan pemesanan secara mudah. Pemilik atau petugas lebih
mudah melakukan pendataan dibandingkan secara manual.
1.2 Rumusan Masalah
Rumusan masalah dalam penulisan tugas akhir ini adalah bagaimana
merancang dan mengimplementasikan sistem informasi pemesanan paket
pernikahan di Larasati Griya Rias dan Dekorasi berbasis web untuk
mempermudah proses pengolahan data dan proses pemesanan paket pernikahan.
1.3 Batasan Masalah
Agar masalah yang penulis tinjau lebih terarah dan mencapai sasaran yang
telah ditentukan, maka penulis membatasi masalah hanya kepada hal-hal dibawah
ini:
1. Sistem hanya memuat proses pengolahan data yang meliputi data paket, data
pelanggan, data pemesanan, data pembayaran, data jadwal gedung, data
gedung, data rias, data dekorasi, dan data laporan.
2. Sistem pemesanan terdiri dari pemesanan paket pernikahan, rias, dekorasi, dan
gedung saja.
3. Pemesanan di web digunakan sebagai bentuk perjanjian untuk mempertemukan
pihak Larasati dan dengan pelanggan untuk membicarakan detail konsep dan
pekerjaan yang akan dilakukan oleh Larasati
1.4 Tujuan Penelitian
Tujuan dari penelitian ini adalah untuk merancang dan
mengimplementasikan sistem informasi pemesanan paket pernikahan untuk
Larasati Griya Rias dan Dekorasi.
3
1.5 Manfaat Penelitian
Manfaat yang bisa didapatkan terbagi menjadi dua yaitu bagi pihak
Larasati Griya Rias dan Dekorasi, dan bagi pengguna jasa (pelanggan) Larasati
Griya Rias dan Dekorasi. Manfaat yang bisa didapatkan pemilik Larasati Griya
Rias dan Dekorasi antara lain:
1. Perusahaan akan lebih dikenal oleh masyarakat luas sehingga akan mudah
mendapatkan pelanggan.
2. Proses pemesanan paket pernikahan di Larasati Griya Rias & Dekorasi
tercatat lebih rapi, cepat, dan praktis.
Sedangkan bagi pengguna jasa Larasati Griya Rias dan Dekorasi antara lain:
1. Pengguna jasa dapat melakukan reservasi lebih cepat dan praktis karena dapat
dilakukan di manapun.
2. Pengguna jasa dapat memilih berbagai paket pernikahan yang tersedia dengan
mudah sesuai petunjuk yang ada.
1.6 Metodologi Penelitian
Ada beberapa metode yang digunakan dalam pengumpulan data yaitu:
1. Tahapan Analisis
Tahapan analisis melakukan analisis terhadap kebutuhan sistem. Tahapan
analisis berisi metode observasi, metode wawancara, dan metode
dokumentasi.
a. Metode Observasi
Metode observasi merupakan metode pengumpulan data yang cukup
efektif untuk mempelajari suatu sistem. Metode ini menggunakan
pengamatan secara langsung terhadap objek yang diteliti yaitu
Larasati Griya Rias dan Dekorasi.
4
b. Metode Wawancara
Metode wawancara merupakan metode pengumpulan data dengan
cara wawancara atau tanya jawab langsung terhadap objek yang
berhubungan yaitu pemilik Larasati Griya Rias dan Dekorasi.
c. Metode Dokumentasi
Metode dokumentasi merupakan pengumpulan data dengan
mendapatkan atau mengumpulkan data dan arsip dari Larasati Griya
Rias dan Dekorasi.
2. Tahapan Pengembangan Sistem
Tahapan pengembangan sistem yang digunakan adalah melalui tahap -
tahap sebagai berikut:
a. Perancangan, merancang sistem yang akan dibangun dengan membuat
Diagram Alir Data, Entity Relationship Diagram dan tabel - tabelnya.
b. Implementasi, melakukan coding atau pembuatan web menggunakan
PHP dan MySQL.
c. Pengujian, menguji hasil aplikasi menggunakan web server Apache.
1.7 Sistematika Penulisan
BAB I  PENDAHULUAN
Menjelaskan latar belakang masalah mengenai
diperlukannya pembuatan dan perancangan Sistem
Informasi Pemesanan Paket Pernikahan di Larasati Griya
Rias dan Dekorasi, rumusan masalah, batasan masalah,
tujuan, manfaat, metodologi penelitian, serta sistematika
penulisan laporan.
BAB II TINJAUAN PUSTAKA
Berisi uraian sistematis mengenai informasi hasil penelitian
yang disajikan dalam pustaka dan menghubungkannya
5
dengen Sistem Informasi Pemesanan Paket Pernikahan di
Larasati Griya Rias dan Dekorasi.
BAB III LANDASAN TEORI
Bab ini menjelaskan tentang sumber yang digunakan dalam
penyusunan tugas akhir. Dalam bab ini menjelaskan tentang
teori umum yang berkaitan dengan judul , teori program
yang berkaitan dengan aplikasi yang digunakan dalam
pembuatan sistem informasi, serta teori khusus yang
berkaitan dengan istilah-istilah yang dipakai.
BAB IV ANALISIS DAN PERANCANGAN SISTEM
Bab ini berisi analisis kebutuhan dalam membangun
perangkat lunak, analisis terhadap seluruh spesifikasi sistem
yang mencakup analisis prosedur yang sedang berjalan,
analisis pengguna dan analisis basis data, selain itu terdapat
perancangan antarmuka untuk aplikasi yang akan dibangun
sesuai dengan hasil analisis yang telah dibuat sebelumnya.
BAB V IMPLEMENTASI SISTEM
Bab ini membahas secara terperinci mengenai penerapan
dari desain yang dibuat dengan menampilkan antarmuka
program disertai cara kerja dan penggunaan sistem.
BAB VI PENGUJIAN DAN PEMBAHASAN SISTEM
Bab ini membahas tentang pengujian sistem yang telah
dikerjakan.
Bab ini terdiri dari kesimpulan dan saran yang berisikan
hal-hal terpenting yang dibahas dan kemudian dijadikan
kesimpulan. Bab ini juga berisi saran-saran yang
dimungkinkan untuk pengembangan perangkat lunak
selanjutnya.

7.1 Kesimpulan
Berdasarkan pengujian yang dilakukan pada sistem informasi pemesanan
paket pernikahan di Larasati Griya Rias dan Dekorasi dapat diambil beberapa
kesimpulan, antara lain sebagai berikut:
1. Sistem Informasi Pemesanan Paket Pernikahan di Larasati Griya Rias dan
Dekorasi telah berhasil dibangun dan dapat memenuhi kebutuhan sistem
admin, admin gedung, dan pelanggan.
2. Sistem Informasi Pemesanan Paket Pernikahan di Larasati Griya Rias dan
Dekorasi mampu mengolah data paket, data pelanggan, data pemesanan,
data pembayaran, data jadwal gedung, data gedung, data rias, data dekor,
dan data laporan
7.2 Saran
Untuk melakukan pengembangan sistem informasi pemesanan paket
pernikahan di Larasati Griya Rias dan Dekorasi berbasis web, penulis
memberikan beberapa saran diantaranya:
1. Ditambahkan fitur peringatan pembatalan pemesanan melalui e-mail atau
sms secara otomatis apabila melebihi jangka waktu pembayaran.
2. Ditambahkan fitur pemberitahuan progress pemesanan melalui e-mail atau
sms secara otomatis.
3. Ditambahkan fitur pemesanan paket foto.
4. Pada halaman paket ditambahkan lebih detil jenis rias yang ditawarkan
agar pelanggan mempunyai banyak pilihan dalam proses pemesanan.

