1.1.  Latar Belakang
Politeknik Negeri Malang (Polinema) merupakan salah satu penyelenggara
pendidikan tinggi yang diberi kepercayaan untuk melaksanakan pola pengelolaan
hibah penelitian secara mandiri. Program hibah  penelitian Dikti di lingkungan
Polinema yang di kelola oleh Unit Pelaksana Teknis Penelitian dan Pengabdian
Masyarakat (UPT P2M), antara lain:  Penelitian Hibah Bersaing (PHB), Penelitian
Strategis Nasionl (PSN), Penelitian Fundamental dan Penelitian Kerja Sama Antar
Perguruan Tinggi (PEKERTI).
Secara umum proses pola pengelolaan hibah penelitian yang berlaku saat
ini terbagai dalam beberapa tahapan, pada setiap tahap pelaksanaan UPT P2M
mengacu kepada ketentuan yang telah ditetapkan oleh DP2M  Dikti. Pada tahap
pertama UPT P2M akan melakukan seleksi administratif untuk setiap proposal
penelitian yang diajukan, jika proposal belum memenuhi syarat administratif
maka proposal akan dikembalikan ke pengusul untuk di lengkapi. Setelah
dinyatakan lolos tahap pertama selanjutnya proposal akan dikirimkan kepada tim
penilai (peer review, evaluator) yang direkomendasikan oleh DP2M Dikti. Hasil
evaluasi dari tim penilai digunakan sebagai dasar untuk menyatakan diterima atau
ditolaknya proposal penelitian. Pada tahap akhir pihak UPT P2M akan
mengumumkan proposal yang diterima sesuai jumlah kuota proposal.
Pada tahap seleksi administratif muncul permasalahan terkait dengan
efisiensi dan efektifitas pengelolaan penilaian administratif proposal oleh staff
UPT P2M. Sementara pada proses penentuan bobot dan evaluasi proposal oleh
beberapa evaluator muncul beberapa kendala diantaranya: subjektivitas penilaian
bobot kriteria dan sub krteria, ambiguitas proses evaluasi proposal,  kecepatan dan
ketepatan agregasi antara bobot kriteria dan hasil evaluasi untuk mendapatkan
prioritas proposal PHB. Sementara pada peneliti dibutuhkan penyampaian hasil
prioritas proposal PHB yang akurat, interaktif dan dinamis. Untuk mengatasi
2
permasalahan tersebut salah satu alternatif yang diusulkan dalam penelitian ini
adalah melakukan pembobotan kriteria dan evaluasi proposal menggunakan
komputer, media jaringan dan sistem pendukung keputusan kelompok/ Group
Decision Support System (GDSS).
GDSS didefinisikan oleh Desancts dan Gallupe pada tahun 1987 sebagai
sebuah sistem yang mengkombinasikan komunikasi, komputerisasi, dan teknologi
pendukung keputusan untuk memfasilitasi perumusan dan penyelesaian masalah
yang tidak terstruktur oleh sekolompok orang. Kini, GDSS didefinisikan sebagai
sistem berbasis komputer yang mendukung sekelompok orang yang
menyelesaikan tugas bersama dan menyediakan sebuah interface lingkungan
untuk berbagi (Kusrini, 2007).  Hal ini sesuai dengan hasil penelitian yang
dilakukan oleh Suryadi (2005) yang mengkombinasikan metode pendukung
keputusan Analytic Hierarchy Process (AHP) dan metode Geometric Mean
(Geomean) untuk mengelompokkan keputusan dari beberapa pengambil
keputusan.
AHP oleh Saaty (2008) didefinisikan sebagai sebuah metode pengambil
keputusan yang mendekomposisi permasalahan yang kompleks kedalam bentuk
hirarki. Iranmanesh menyatakan bahwa metode AHP konvensional tidak cocok
untuk permasalahan yang memiliki sifat ketidakpastian  (uncertainty), sementara
Zadeh memperkenalkan teori himpunan Fuzzy untuk menyelesaikan
permasalahan yang mengandung unsur ketidakpastian (Zadeh,1965). Pada dunia
nyata kenyataan yang dihadapi banyak bersifat  ambiguities atau dengan kata lain
bersifat kabur (fuzzy), maka dalam beberapa penelitian metode AHP di
kombinasikan dengan teori fuzzy menjadi FAHP (Iranmanesh dkk,  2008).
Dalam penelitian ini akan dilakukan rancang bangun GDSS untuk seleksi
proposal PHB.  Proses diawali dengan seleksi administratif, penentuan bobot
kriteria menggunakan metode FAHP, evaluasi proposal dan penentuan nilai
sintesa proposal menggunakan metode FMCDM, agregasi hasil akhir nilai sintesa
proposal tim evaluator untuk menentukan  peringkat proposal PHB dan diakhiri
penentuan proposal PHB yang lolos atau tidak lolos seleksi.
3
1.2.  Rumusan Masalah
Penelitian yang diajukan menitikberatkan pada permasalahan sebagai
berikut:
1. Bagaimana membuat aplikasi yang dapat menilai kelengkapan administratif,
kelayakan proposal PHB dan menampilkan hasil penilaian ke pengguna secara
interaktif dan dinamis?
2. Bagaimana merancang sistem pendukung keputusan kelompok dengan FAHP
yang dapat menghitung bobot kriteria, bobot sub kriteria dan kriteria total ?
3. Bagaimana merancang sistem pendukung keputusan kelompok dengan metode
FMCDM yang dapat mengevaluasi proposal, menghitung prioritas proposal
dan menentukan diterima atau ditolaknya proposal PHB ?
1.3.  Batasan Masalah
Dalam penelitian yang dilakukan, terdapat beberapa batasan masalah yang
digunakan, diantaranya :
1. Penelitian di Lakukan di UPT P2M Polinema, program hibah penelitian yang
diteliti adalah Penelitian Hibah Bersaing (PHB).
2. Pembobotan kriteria oleh setiap penilai dilakukan dengan menggunakan
FAHP.
3. Penentuan peringkat untuk semua proposal dilakukan dengan menggunakan
FMCDM.
4. Agregasi hasil nilai sintesa proposal untuk mendapatkan peringkat  proposal
PHB yang diajukan oleh peniliti.
5. Penentuan lolos tidaknya proposal PHB berdasarkan peringkat, kuota dan nilai
minimal yang harus terpenuhi.
1.4.  Tujuan Penelitian
Tujuan penelitian adalah membangun GDSS yang dapat membantu proses
seleksi proposal penelitian hibah bersaing di UPT P2M Polinema yang melibatkan
beberapa evaluator menjadi lebih efisiensi dalam hal waktu dan biaya, lebih
4
objektif dalam proses penilaian bobot kriteria dan proposal, lebih akurat dalam
menentukan prioritas proposal sehingga memudahkan UPT P2M menetapkan
proposal PHB yang lolos dan tidak lolos seleksi.
1.5.  Manfaat Penelitian
Penelitian tentang rancang bangun GDSS untuk seleksi proposal program
hibah penelitian menggunakan FAHP dan FMCDM dapat memberikan tambahan
referensi, dasar pengembangan dan implementasi sistem pendukung keputusan
kelompok, baik bagi penulis maupun peneliti lain yang memiliki topik penelitian
yang sejenis. Untuk Perguruan Tinggi yang melaksanakan program hibah
penelitian dan belum memiliki tim evaluator dapat menggunakan hasil penelitian
ini untuk mengoptimalkan proses seleksi proposal hibah penelitian.
1.6.  Metodologi Penelitian
Metodologi yang digunakan dalam penelitian ini adalah
1. Studi Kepustakaan
Pada tahap ini, penulis mencari dan merangkum kepustakaan apa saja
yang dapat menunjang pengerjaan penelitian ini. Diantaranya yaitu:
? Pengumpulan informasi tentang sistem pendukung keputusan dan sistem
pendukung keputusan kelompok.
? Pengumpulan informasi tentang proses seleksi proposal hibah penelitian
yang dilakukan dilingkungan UPT P2M Polintema.
? Pengumpulan informasi tentang kriteria dan sub kriteria yang digunakan
untuk seleksi proposal hibah penelitian.
? Pengumpulan informasi tentang metode Fuzzy AHP dan FMCDM untuk
perangkingan didalam sistem pendukung keputusan.
2. Analisis dan Perancangan Sistem
Pada tahap ini dilakukan analisa kebutuhan sistem yang akan dibuat dan
menjadi dasar untuk perancangan sistem. Pada tahap awal dilakukan
pengumpulan data dari hasil proses seleksi proposal hibah penelitian yang
selama ini dilakukan oleh UPT P2M Polinema. Data yang dikumpulkan
5
adalah data hasil proses seleksi dan berkas-berkas Penilaian yang dilakukan
oleh para tim penilai dalam kurun waktu antara tahun 2009 s.d 2010.  Hasil
pengumpulan data digunakan untuk menentukan  kriteria dan sub kriteria yang
akan digunakan dalam penelitian.
Tahap selanjutnya setelah pengumpulan data dilakukan penyusunan
hirarki permasalahan menggunakan metode AHP. Tujuan alternatif
penyelesaian dan kriteria distrukturkan menjadi suatu bentuk diagram pohon,
dengan demikian konstruksi dari premasalahan akan terlihat lebih sistematis
dan mudah untuk diselesaikan. Bobot masing-masing kriteria dari pengambil
keputusan diolah menggunakan metode FAHP.  Penilaian proposal, sintesa
keputusan dan penentuan peringkat proposal diolah menggunakan metode
FMCDM.
3. Implementasi
Setelah tahap perancangan, maka selanjutnya yang akan dilakukan adalah
pembuatan perangkat lunak. Tahap ini meliputi implementasi proses-proses
yang dirancang pada tahap selanjutnya ke dalam baris-baris code.
4. Pengujian
Pada tahap ini dilakukan pengujian sistem apakah hasil penelitian yang
telah dilakukan sesuai dengan tujuan penelitian dan menjawab rumusan
masalah yang telah ditentukan.  Pada uji coba akan dilakukan dengan
beberapa skenario untuk mengetahui fungsionalitas-fungsionalitas dari
program. Pertama akan dilakukan perbandingan antara hasil pengujian dari
sistem GDSS dengan hasil seleksi proposal yang pernah dilakukan di UPT
P2M. Kedua akan dilakukan pengisian secara acak untuk mengetahui apakah
sistem bisa memberi peringatan ketika proses perbandingan yang dilakukan
telah memenuhi standar yang ditentukan atau belum. Ketiga membandingkan
hasil pembobotan kriteria dan sub kreteria yang dilakukan oleh tim UPT P2M
dan tim penilai Dikti. Selain itu, uji coba digunakan untuk menguji
kesesuaian aplikasi dengan fitur yang dibuat.
6
1.7.  Sistematika Penulisan
BAB  I  PENDAHULUAN
Bab ini akan menguraikan secara singkat mengenai latar belakang
masalah, rumusan masalah, tujuan penelitian, batasan penelitian,
manfaat penelitian, metodologi penelitian dan sistematika penulisan.
BAB  II  TINJAUAN PUSTAKA
Bab ini membahas tinjauan pustaka yang digunakan sebagai bahan
referensi dalam penelitian ini.
BAB  III  LANDASAN TEORI
Bab ini menguraikan teori dasar yang berkaitan dengan penelitian
yang dilakukan dan akan menjadi dasar dalam pemecahan masalah.
BAB  IV  ANALISIS DAN RANCANGAN SISTEM
Bab ini akan menguraikan tentang analisis dan rancangan sistem yang
dipergunakan sebagai acuan dalam pemecahan masalah serta desain
tampilan antarmuka sebagai penghubung antara pengguna dengan
sistem.
Bab ini berisikan cuplikan kode program dan tampilan antarmuka
program dari implementasi sistem.
Bab ini berisikan hasil akhir dari sistem yang dibangun, disertai
dengan penjelasan dari setiap keluaran proses.
Berisikan kesimpulan dari hasil penelitian.

7.1 Kesimpulan
Kesimpulan yang diperoleh dari  pengembangan model sistem pendukung
keputusan kelompok untuk seleksi proposal PHB menggunakan FAHP, Geomean,
dan FMCDM dapat disimpulkan bahwa:
1. Aplikasi GDSS berbasis komputer yang interaktif dan dinamis dapat
meningkatkan efisiensi, efektifitas, akurasi dan objektifitas seleksi proposal
PHB di UPT P2M Politeknik Negeri Malang.
2. Metode FAHP dapat diterapkan pada Aplikasi GDSS untuk penentuan bobot
kriteria, sementara Geomean sebagai bagian dari FAHP berperan didalam
agregasi hasil penilaian perbandingan berpasangan antar kriteria beberapa
pengambil keputusan (evaluator).
3. Metode FMCDM dapat diterapkan dalam penilaian proposal, sintesa hasil
keputusan, dan penentuan peringkat proposal sebagai dasar pengambilan
keputusan diterima atau ditolaknya proposal PHB.
4. Hasil pengujian data proposal PHB di UPT P2M Politeknik Negeri Malang
dengan beberapa kriteria dan sub kriteria menunjukkan bahwa sistem
pengambilan keputusan kelompok dengan menggunakan metode FAHP,
Geomean dan FMCDM terbukti dapat menghasilkan peringkat alternatif dan
kepastian diterima atau ditolaknya proposal PHB yang diajukan oleh peneliti.
7.2 Saran
Penelitian ini memfokuskan pada perancangan model sistem pendukung
keputusan kelompok untuk seleksi proposal PHB dengan studi kasus di UPT P2M
Politeknik Negeri Malang. Saran yang diberikan pada penelitian selanjutnya
adalah mengimplementasikan model yang telah dikembangkan dalam bentuk
perangkat lunak untuk membantu pengambilan keputusan secara kelompok pada
proses seleksi proposal Penelitian Fundamental, Penelitian Reguler dan Jenis
Penelitian lainya. Selain itu, disarankan adanya tambahan fitur pada aplikasi  yang
memungkinkan pengambil keputusan dapat saling berkomunikasi secara
kelompok.

