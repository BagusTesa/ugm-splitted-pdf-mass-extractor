1.1. Latar Belakang dan Permasalahan
Di dalam aspek kehidupan ini, banyak ditemui permasalahan yang
berkaitan dengan data. Entah dalam bidang ekonomi, sosial, kebudayaan, dan
lain-lain. Dari data itu dapat diperoleh informasi dan proses pengambilan
keputusan. Pengolahan data merupakan salah satu bagian rangkaian kegiatan
penelitian setelah pengumpulan data. Dari hasil pengolahan data tersebut akan
diperoleh suatu hasil yang selanjutnya didapatkan suatu kesimpulan yang berupa
informasi.
Salah satu contoh penggunaan data adalah analisis regresi. Analisis regresi
merupakan salah satu metode statistika yang paling populer. Analisis regresi
merupakan cabang ilmu statistika yang bertujuan untuk mengetahui hubungan
antara dua variabel, yaitu variabel dependen (tak bebas) dan variabel independen
(bebas). Analisis regresi berdasarkan jumlah variabel independennya diantaranya
adalah regresi sederhana yang melibatkan satu variabel independen dan regresi
ganda yang melibatkan lebih dari satu variabel independen. Contoh dari
penggunaan data dalam analisis regresi di bidang kesehatan adalah untuk
mengetahui apakah terdapat hubungan antara kebiasaan merokok dengan terkena
penyakit paru-paru. Sedangkan di bidang ekonomi adalah untuk mengetahui
apakah periklanan suatu produk mempengaruhi penjualan produk tersebut.
Salah satu permasalahan yang sering ditemui oleh peneliti adalah tidak
lengkapnya data yang digunakan. Data yang tersedia kurang lengkap biasanya
dikarenakan karena adanya beberapa faktor dalam proses pengumpulan data.
Diantaranya adalah tidak terisinya kuesioner, responden menolak untuk mengisi,
kesalahan dalam pengambilan data dan lain sebagainya. Hal ini menyebabkan
hasil yang didapatkan menjadi tidak valid dan tujuan dari penelitian tidak tercapai.
2
Teknik atau metode paling tradisional yang digunakan diantaranya adalah
listwise deletion dan pairwise deletion. Listwise deletion merupakan metode untuk
mengatasi data hilang dengan cara menghapus data yang hilang tersebut dari
sampel. Kelebihan dari metode ini adalah dapat digunakan untuk setiap jenis
analisis statistik dan tidak membutuhkan komputasi yang rumit. Namun
kelemahannya adalah akan membuat kesimpulan menjadi tidak valid karena
hanya sekedar menghapus data hilang yang ada. Kemudian ide dari pairwise
deletion adalah membuang sepasang pengamatan yang mengandung data hilang.
Selain itu Little dan Rubin (1987) juga memperkenalkan berbagai macam untuk
mengatasi missing data, diantaranya adalah: complete case analysis yaitu
membuang observasi yang terdapat missing data dan estimasi mengarah pada
standar error yang lebih besar dikarenakan jumlah sampel yang berkurang.
Metode yang lebih modern dibandingkan dengan kedua metode tradisional
tersebut di atas adalah metode modern yang terdiri dari maksimum likelihood dan
imputasi ganda. Kedua metode ini tidak hanya sekedar menghapus data yang
hilang dari suatu sampel, namun mengganti data yang hilang tersebut dengan nilai
estimasi. Metode maksimum likelihood merupakan metode yang digunakan untuk
mengestimasi nilai data yang hilang menggunakan algoritma ekspektasi
maksimisasi. Sedangkan imputasi ganda merupakan metode yang digunakan
untuk mengestimasi nilai data yang hilang menggunakan beberapa nilai yang
mungkin yang mewakili dari distribusi kemungkinannya dan dilakukan sebanyak
m kali.
1.2. Tujuan Penulisan
Tujuan yang ingin dicapai dari penulisan skripsi ini adalah:
1. Sebagai salah satu syarat untuk memperoleh gelar sarjana pada Program Studi
Statistika, Jurusan Matematika, Universitas Gadjah Mada.
2. Mempelajari metode imputasi ganda sebagai salah satu metode untuk
mengestimasi nilai data hilang pada suatu data.
3
1.3. Manfaat Penulisan
Manfaat dari penulisan skripsi ini adalah:
1. Memberikan gambaran tentang data hilang dan cara mengatasinya.
2. Memberikan penjelasan tentang estimasi nilai data hilang yang digunakan
untuk menangani data hilang pada suatu kasus.
3. Memperkenalkan metode imputasi ganda sebagai salah satu cara mengatasi
data hilang terutama pada kasus analisis statistik regresi.
4. Mengaplikasikan teori imputasi ganda untuk menyelesaikan permasalahan
pada studi kasus.
1.4. Perumusan dan Batasan Masalah
Berdasarkan latar belakang dan kajian-kajian pendukung lainnya, maka
penulis hanya membatasi penulisan skripsi ini. Bahwa metode imputasi ganda
pada skripsi ini digunakan pada kasus analisis statistik regresi ganda dan
digunakan untuk mengestimasi nilai data hilang pada variabel dependen.
1.5. Tinjauan Pustaka
Analisa data hilang pertama kali diteliti oleh Rubin (1976) pada jurnalnya
yang berjudul Inference and Missing Data. Sebelumnya banyak metode alternatif
yang telah diusulkan, diantaranya adalah metode listwise deletion, pairwise
deletion dan masih banyak lagi. Namun kemudian berkembang dua pendekatan
baru untuk mengatasi data hilang yaitu maximum likelihood dan multiple
imputation.  Selanjutnya metode imputasi ganda atau multiple imputation pertama
kali diteliti oleh Donald B. Rubin dalam bukunya yang berjudul Multiple
Imputation for Nonresponse in Surveys pada tahun 1987. Imputasi ganda
merupakan suatu teknik yang mengatasi data hilang dengan mengganti data yang
hilang dengan dua atau lebih nilai yang mungkin yang mewakili distribusi
kemungkinan (Rubin, 1977, 1978).
4
1.6. Metode Penulisan
Metode penulisan yang digunakan dalam karya tulis ini adalah
berdasarkan studi literatur yang didapatkan dari perpustakaan serta jurnal-jurnal
dan buku-buku yang berhubungan dengan tema dari tugas akhir ini. Sumber
lainnya juga diperoleh melalui situs-situs pendukung yang tersedia di internet.
Pengerjaan karya tulis ini juga ditunjang dengan beberapa perangkat lunak yaitu
Microsoft Excel 2007, SPSS 16.0 dan R versi 3.0.2.
1.7. Sistematika Penulisan
Sistematika penulisan tugas akhir ini terdiri dari lima bab, yaitu sebagai
berikut:
BAB I  Pendahuluan
Bab ini membahas tentang latar belakang dan permasalahan, tujuan
penulisan, manfaat penulisan, perumusan dan batasan masalah,
tinjauan pustaka, metode penulisan dan sistematika penulisan.
BAB II Dasar Teori
Bab ini membahas tentang teori-teori yang berhubungan dengan
tema tugas akhir ini, khususnya teori-teori yang berhubungan
dengan data hilang dan imputasi ganda.
BAB III Estimasi Nilai Data Hilang Menggunakan Imputasi Ganda dengan
Metode Regresi
Bab ini membahas tentang metode imputasi ganda dalam
menangani permasalahan data hilang.
BAB IV Studi Kasus
Bab ini membahas tentang aplikasi metode imputasi ganda dalam
menangani permasalah data hilang. Pengolahan data digunakan
software R versi 3.0.2.
BAB V Penutup
Bab ini membahas tentang kesimpulan yang diperoleh dari hasil
pembahasan pada bab-bab sebelumnya. Bab ini juga membahas
saran-saran yang diberikan penulis atas permasalahan yang terjadi.

5.1 Kesimpulan
Kesimpulan yang didapatkan dari pembahasan pada bab-bab sebelumnya
adalah:
1. Penulis telah mempelajari dan menyajikan imputasi ganda dengan metode regresi
untuk mencari estimasi nilai yang hilang pada suatu data.
2. Estimasi nilai data hilang dapat dilakukan dengan menggunakan imputasi ganda
pada data dengan model regresi linear ganda dengan data yang hilang adalah
variabel dependen.
3. Imputasi ganda merupakan salah satu cara yang cukup baik untuk mengestimasi
nilai data yang hilang.
5.2 Saran
1.  Pada skripsi ini imputasi ganda dengan metode regresi digunakan untuk
mengestimasi nilai data hilang pada model regresi. Perlu dipelajari lebih lanjut
apabila kasus data yang hilang bukan merupakan model regresi.
2. Perlu dipelajari juga untuk kasus model regresi dengan data hilang variabel
independen dan variabel dependen lebih dari satu.

