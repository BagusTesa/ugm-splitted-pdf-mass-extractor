Bab 1 ini berisi latar belakang penelitian yang telah dilakukan.
Berdasarkan latar belakang, dirumuskan masalah yang diteliti, selanjutnya
tujuan yang ingin dicapai dan manfaat yang ingin diperoleh dari penelitian ini
dan yang terakhir diberikan sistematika penulisan.
1.1    Latar Belakang Masalah
Aljabar max-plus, yaitu himpunan semua bilangan real  dilengkapi
dengan operasi max-plus. Aljabar max-plus telah dapat digunakan dengan
baik untuk memodelkan dan menganalisis secara aljabar masalah-masalah
jaringan, seperti jaringan transportasi kereta, sistem manufaktur fleksibel,
jaringan telekomunikasi, sistem proses paralel, dsb. Macam-macam sistem
yang telah disebutkan tadi adalah contoh dari Sistem Event Diskrit (SED).
Klas SED utamanya memuat sistem buatan manusia yang terdiri dari sejumlah
resources (misalnya, mesin, kanal-kanal komunikasi, processor, dll) yang
dipakai bersama oleh beberapa pengguna (misalnya, macam produk, paket
informasi, job, dll) kesemuanya itu berkontribusi untuk mencapai beberapa
tujuan bersama (misalnya, produk perakitan, transmisi dari sekumpulan paket
informasi, komputasi paralel, dll). Oleh karena itu, untuk sistem dengan klas
besar, waktu mulai dari suatu kegiatan dapat dinyatakan sebagai maximum
dari rangkaian satu kali kedatangan, yang masing-masing pada waktu memulai
kegiatannya plus proses dan waktu transportasi antara aktivitas node yang
sesuai.
Suatu gambaran karakteristik dari SED adalah �kedinamikannya� yaitu
�event-driven system� yang bertolak belakang dengan �time-driven system�.
Menurut  Necoara et.al. (2008: 1), dalam SED keadaan sistem pasti akan
2
bergantung dengan waktu. Setiap waktu bertambah, maka keadaan sistem
dipastikan berubah pula. Sistem yang demikian ini disebut dengan sistem
terkendali waktu (time-driven system). Selain sistem tersebut, sering dijumpai
pula suatu sistem yang berkembang berdasarkan kemunculan kejadiannya.
Transisi keadaan merupakan hasil dari kejadian lain yang selaras (kejadian-
kejadian yang bertindak sebagai kejadian input bagi transisi keadaan yang
bersangkutan). Dengan kata lain, perubahan keadaan merupakan hasil dari
kejadian sebelumnya. Sistem seperti ini disebut dengan sistem terkendali
kejadian (event-driven system). Umumnya kedinamikan dari SED
dikarakteristikkan oleh �kesinkronisasian� dan �konkurensi�. Sinkronisasi
memerlukan ketersediaan dari beberapa resources pada saat yang bersamaan
(misalnya, sebelum kereta berangkat dari suatu stasiun, kereta tersebut harus
menunggu kereta yang lain sampai terlebih dahulu pada stasiun tersebut).
Proses-proses yang disebut konkurensi adalah apabila proses-proses (lebih
dari satu proses) berada pada saat yang sama. Proses-proses konkuren dapat
sepenuhnya tak bergantung dengan lainnya tapi dapat juga saling berinteraksi.
Proses-proses yang berinteraksi memerlukan sinkronisasi agar terkendali
dengan baik.
Pendekatan aljabar max-plus dapat menentukan dan menganalisa berbagai
sifat sistem, tetapi pendekatan hanya bisa diterapkan pada sebagian klas SED
yang bisa diuraikan dengan model waktu invarian max-linear. Subklas ini
adalah subklas dari waktu invarian SED deterministik dimana hanya
sinkronisasi tanpa kejadian yang konkurensi. Walaupun hanya sinkronisasi
saja yang dipertimbangkan dalam aljabar max-plus, hal ini sudah dapat
menganalisa perilaku suatu  sistem yang ada. Dalam hal aljabar max-plus
sistem model yang terjadi adalah linear dan non-linear pada aljabar biasa.
1.2    Perumusan Masalah
Berdasarkan latar belakang diatas yang menjadi permasalahan dalam
penelitian ini adalah :
3
a. Bagaimana memodelkan sistem jaringan transportasi berdasarkan SED
dengan aljabar max-plus.
b. Bagaimana pengaruh nilai eigen dalam kestabilan suatu sistem
jaringan transportasi.
1.3    Batasan Masalah
Pada penulisan skripsi ini, penulis membatasi masalah pada konsep sistem
event diskrit untuk max plus linear dan analisa pengkajian menggunakan
aljabar max plus khususnya sistem aljabar max-plus linear.
1.4    Maksud dan Tujuan
Berdasarkan pada permasalahan diatas, dirumuskan tujuan penelitian
yaitu :
a. Mengetahui model dari sistem jaringan transportasi pada aljabar max-plus.
b. Mengetahui pengaruh nilai eigen dalam kestabilan suatu sistem jaringan
transportasi
1.5    Tinjauan Pustaka
Tulisan pada skripsi ini secara keseluruhan mengacu pada tiga jurnal yang
masing-masing ditulis oleh R. de Vries, B. Schutter, dan B. De Moor (1998)
serta Ton van den Boom dan B.De Schutter (2004) yang berjudul On max-
algebraic models for transportation networks dan Modelling and control of
railway networks. Dasar teori yang beirisi definisi, teorema dan proposisi
tentang dasar-dasar aljabar max-plus dan teori graf diambil dari buku
Synchronization and Linearity : An Algebra for Discrete Event Systems yang
ditulis oleh F. Bacceli, G. Gohen, G.J. Olsder, and J.P. Quadrat (1992). Dalam
skripsi ini juga digunakan hasil-hasil jurnal dari Norman Biggs (1993) yang
berkaitan dengan teori graf yang juga dibahas oleh R.M. Karp dalam jurnal A
Characterization of The Minimum Cycle Mean in A Digraf (1978).
4
Dalam tesis yang ditulis oleh J.G Braker (1993) yang berjudul Algorithms
and Applications in Timed Discrete Event Systems mengenai jadwal
keberangkatan, kestabilan dan vektor kontrol yang berhubungan dengan
sistem jaringan transportasi.
1.6    Metode Penelitian
Metode penelitian adalah studi literatur. Pada awalnya dibuat model sistem
jaringan transportasi berdasarkan pada sistem aljabar max-plus, kemudian
ditinjau sistem tersebut terhadap suatu jadwal keberangkatan. Selanjutnya
dianalisis hasil dari nilai eigen sistem tersebut dan akhirnya ditinjau kestabilan
daripada sistem jaringan dengan jadwal keberangkatan tersebut. Penulis
melakukan pencarian di internet yang selanjutnya didiskusikan bersama dosen
pembimbing.
1.7    Sistematika Penulisan
Penulisan skripsi ini dibagi dalam lima bab dengan susunan pembagian
tiap-tiap bab sebagai berikut :
Pada bab I berisi pendahuluan : latar belakang masalah, perumusan
masalah, maksud dan tujuan, pembatasan masalah, tinjauan pustaka, metode
penulisan dan sistematika penulisan.
Pada bab II dibahas mengenai landasan teori yang digunakan pada bab
pembahasan yang meliputi : nilai eigen, teori graf dan sistem terminologi pada
aljabar max-plus linear.
Pada bab III berisi pembahasan mengenai model sistem jaringan
transportasi, jadwal keberangkatan, kestabilan sistem dan variabel kontrol.
Pada bab IV berisi simulasi sistem jaringan transportasi terhadap
keterlambatan pada jadwal keberangkatan.
Pada bab V berisi kesimpulan dari pembahasan mengenai kestabilan
sistem terhadap keterlambatan yang terjadi pada jadwal keberangkatan.

Pada bab ini akan diberikan kesimpulan dan saran-saran yang dapat
diambil berdasarkan materi-materi yang telah dibahas pada bab-bab
sebelumnya.
5.1    Kesimpulan
Telah dikaji suatu model sistem jaringan kereta yang diturunkan dari
jadwal keberangkatan kereta dengan menggunakan aljabar max-plus.
Selanjutnya telah ditunjukkan pula bila terjadi keterlambatan dari beberapa
kereta pada sistem yang ada keterlambatan ini akan semakin mengecil sampai
pada suatu keberangkatan tertentu berikutnya sudah tidak ada keterlambatan
lagi. Hal ini menunjukkan sistem kembali beroperasi dengan jadwal
keberangkatan sebagaimana bila tidak ada keterlambatan. Dengan kata lain
sistem yang dikaji adalah stabil.
5.2     Saran
Diharapkan unutk penelitian selanjutnya adalah dimungkinkan untuk dapat
menurunkan sistem jaringan kereta dengan kasus jadwal keberangkatan kereta
tidak periodik, artinya kereta-kereta beroperasi dengan frekuensi
keberangkatan yang tidak sama. Selain itu juga memungkin bagi peneliti
selanjutnya untuk menurunkan model yang lebih komples dari sistem jaringan
kereta.

