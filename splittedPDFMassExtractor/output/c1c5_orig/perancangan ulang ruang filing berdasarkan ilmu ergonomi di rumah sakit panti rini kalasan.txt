Rumah Sakit adalah suatu organisasi yang menaungi tenaga medis
profesional dan terorganisir serta sarana kedokteran yang permanen,
menyelenggarakan pelayanan kedokteran, asuhan keperawatan, yang
berkesinambungan, diagnosis, serta pengobatan penyakit yang di derita oleh
pasien (Azwar, 1996).
Setiap sarana pelayanan kesehatan di Rumah Sakit wajib membuat
Rekam Medis yang kemudian diisi  oleh dokter dan tenaga kesehatan yang
terkait dengan pelayanan yang telah diberikan oleh dokter dan tenaga
kesehatan lainnya.
Di dalam sarana pelayanan kesehatan Rumah Sakit, perekam medis
mempunyai peranan yang sangat penting dalam memberikan informasi dan
juga dapat melaksanakan kegiatan dalam memberikan informasi atau
melakukan pencatatan dokumen dan pendokumentasian terhadap berkas
Rekam Medis pasien. Menurut Permenkes No. 269/MENKES/PER/III/2008
tentang Rekam Medis pasal 1 ayat (1) yang berbunyi sebagai berikut :
Rekam Medis adalah berisikan tentang catatan dan dokumen identitas
pasien, pemeriksaan, pengobatan, tindakan, dan pelayanan lain yang telah
2
diberikan kepada pasien. Rekam Medis juga dapat digunakan sebagai alat
komunikasi dokter dan penyedia jasa pelayanan kesehatan lain di Rumah
Sakit. Fungsi utama Rekam Medis (Kertas) adalah untuk menyimpan data
dan informasi pelayanan pasien.
Agar fungsi Rekam Medis sebagai penyimpanan data dan informasi
pelayan pasien tetap terjaga kualitasnya, terdapat berbagai persyaratan yang
harus tetap diperhatikan. Ada enam unsur yang berkaitan dengan
penyimpanan, yaitu mudah di akses, berkualitas, terjaga keamanan
(Security), fleksibilitas, dapat di hubungkan dengan berbagai sumber (Conn
Eutivity), dan efisien (Hatta, 2008). Penyimpanan berkas Rekam Medis
merupakan salah satu bagian dari sistem Rekam Medis Rumah Sakit.
Dengan demikian, penyimpanan mempunyai peranan yang sangat penting
dari berbagai informasi yang dimiliki oleh jasa pelayanan kesehatan.
Dalam pelaksanaan penyimpanan berkas rekam medis diperlukan adanya
fasilitas yang memadai bagi berkas rekam medis maupun bagi petugas
pelaksanaan penyimpanan berkas rekam medis. Banyak pilihan yang
tersedia dalam melakukan penjajaran rekam medis diantaranya dengan
menempatkan rekam medis kedalam lemari terbuka (open solves), lemari
cabinet (filing cabinet), atau dengan menggunakan teknologi microfilm
maupun digital scanning dan terakhir secara komputerisasi (rekam kesehatan
3
elektronik). Pilihan terhadap cara yang digunakan tergantung pada
kebutuhan dan fasilitas rumah sakit (Hatta, 2008).
Berdasarkan studi pendahuluan yang perancang lakukan, ruang
penyimpanan berkas rekam medis di Rumah Sakit Panti Rini Kalasan dibagi
menjadi tiga ruangan dan masing-masing ruangan berada pada lokasi yang
terpisah antara ruangan satu dengan ruangan lainnya, yaitu ruangan A atau
ruang penyimpanan aktif satu terletak lumayan jauh dari ruang Instalasi
Rekam Medis tepatnya berada di bawah tangga menuju ruang Direktur persis
disamping laboratorium berukuran panjang 7,5 m dan lebar 2,9 m didalam
ruangan ini terdapat 6 rak terbuka sebagai tempat menyimpan berkas rekam
medis aktif, kemudian ruangan B atau ruang penyimpanan aktif dua berada
tepat dibelakang ruang pendaftaran berukuran panjang 4 m dan lebar 1,4 m
ruangan ini berisi 2 rak terbuka tempat menyimpan berkas rekam medis aktif
dan satu meja kecil untuk meletakkan galon air, sedangkan ruangan C atau
ruang penyimpanan berkas non aktif berada di belakang berukuran panjang 4
m dan lebar 8 m. Berikut  gambar denah lokasi ruang penyimpanan yang ada
di Rumah Sakit Panti Rini Kalasan :
4
Gambar 1. Peta Subsi Rekam Medis Rumah Sakit Panti Rini Kalasan
5
Berkas rekam medis di Rumah Sakit Panti Rini Kalasan disimpan di rak
terbuka dan jarak antara satu rak dengan rak lainnya berdekatan,
berdasarkan hasil pengukuran yang perancang lakukan jarak antara rak satu
dengan rak lainnya yaitu 60 cm sehingga tidak memungkinkan dua orang
petugas mencari berkas rekam medis pada rak yang sama. Jarak tersebut
juga membatasi ruang gerak petugas saat hendak melalui sela antara dua
rak.
Susunan berkas pada rak penyimpanan dan penataan ruang juga tidak
teratur, sehingga menyebabkan kesulitan dalam pencarian. Dalam studi
pendahuluan yang dilakukan perancang terjadi beberapa kali berkas rekam
medis sulit ditemukan pada rak penyimpanan rekam medis saat akan
digunakan. Kemudian dari segi pencahayaan dan udara juga kurang baik,
dari hasil pengukuran udara yang perancang lakukan suhu didalam ruang
penyimpanan cukup tinggi yaitu sekitar 28-30 derajat celcius sehingga
ruangan terasa panas, ruangan juga tidak memiliki jendela dan ventilasi yang
cukup sebagai tempat pertukaran udara sehingga ruangan menjadi terasa
pengap. Berikut gambar suasana ruang penyimpanan yang ada di Rumah
Sakit Panti Rini Kalasan :
6
Gambar 2. Suasana Ruang Penyimpanan Rumah Sakit Panti Rini Kalasan
Gambar 3. Suasana Ruang Penyimpanan Rumah Sakit Panti Rini Kalasan.
7
Berdasarkan wawancara yang dilakukan pada saat studi pendahuluan,
di dapatkan keterangan bahwa Rumah Sakit Panti Rini Kalasan berencana
menggabungkan dua ruang penyimpanan berkas rekam medis agar
mempermudah dan mempercepat proses pengambilan dan pengembalian
berkas rekam medis, ruangan tersebut rencananya akan di bangun dilantai
dua persis diatas ruang instalasi rekam medis. Berikut peta lokasi ruang
instalasi rekam medis yang ada di Rumah Sakit Panti Rini Kalasan :
Gambar 4. Peta Lokasi Ruang Instalasi Rekam Medis
8
Dari pertimbangan di atas perancang berencana merancang ulang ruang
penyimpanan yang efektif, nyaman, aman, sehat, dan efisien berdasarkan
prinsip ergonomi di Rumah Sakit Panti Rini Kalasan baik bagi petugas
maupun berkas.
B. RUMUSAN MASALAH
Dari uraian pada latar belakang yang telah perancang kemukakan di atas,
maka perancang telah merumuskan masalah yaitu : �Perlunya dilakukan
perancangan ulang ruang filing berdasarkan ilmu ergonomi di Rumah Sakit
Panti Rini Kalasan.�
C. TUJUAN PERANCANGAN
Tujuan dalam perancangan ini adalah agar terwujudnya ruang filing yang
efektif, nyaman, aman, sehat, dan efisien sesuai dengan prinsip ergonomi
baik bagi petugas maupun berkas di Rumah Sakit Panti Rini Kalasan.
D. MANFAAT PERANCANGAN
1. Manfaat Praktisi
a. Bagi Institusi/Rumah Sakit
Hasil rancangan ini diharapkan dapat digunakan sebagai bahan
masukan terkait dengan ruang filing yang sesuai dengan ilmu
9
ergonomi dan meminimalisir dampak cedera serta kecelakaan
bagi pengguna ruang filing.
b. Bagi Perancang/Penulis
1) Dapat mengembangkan pengetahuan
2) Dapat meningkatkan wawasan dan potensi akademis yang
dimiliki untuk merancang ulang interior ruang filling
berdasarkan ilmu ergonomis
3) Menerapkan teori yang didapat di bangku kuliah
2. Manfaat Teoritis
a. Bagi Institusi Pendidikan
Dapat memberikan materi yang berharga bagi sumber
pembelajaran dan sebagai bahan untuk mengetahui sejauh mana
kemampuan mahasiswa dalam memahami teori yang telah
diberikan.
b. Bagi Perancang Lain
Sebagai dasar atau acuan dan referensi untuk
pengembangan yang berhubungan dengan materi yang diambil
oleh peneliti lain dimasa yang akan datang.
10
C. KEASLIAN PERANCANGAN
Kerancangan ini belum pernah dilakukan sebelumnya, tetapi ada
beberapa perancangan yang serupa, yaitu:
1. Ratnasari, Annisa (2012), dengan judul Perancangan Ulang Ruang
Penyimpanan Berkas Rekam Medis di RSUP Sleman. Hasil
perancangan tersebut adalah ruang penyimpanan di RSUP Sleman
terdiri dari atas 2 ruang dengan pembangian luas ruang 7,2 m x 6,2 m
untuk ruang A dan 8,9 m x 6,2 m untuk ruang B dengan banyak
furniture yang ada di dalamnya. Rak penyimpanan berkas rekam
medis yang digunakan ada 2 jenis yaitu rak terbuka dan roll �o pack.
Perancang merancang ulang rak penyimpanan berkas rekam medis
dengan sarana penyimpanan roll �o pack yang di anggap paling tepat
untuk aplikasinya.
Persamaan yang ada dengan penelitian ini adalah sama-sama
menyangkut perancangan ulang ruangan. Sedangkan perbedaannya
adalah penelitian yang perancang lakukan yaitu merancang ulang
ruang filing dengan lebih berfokus ke arah ke-ergonomisan suatu
ruangan, sehingga bisa mewujudkan sebuah ruang penyimpanan
yang efektif, nyaman, aman, sehat, dan efisien baik bagi petugas
maupun berkas.
11
2. Saputro, Sri Kabul (2011) dengan judul Perancangan Ruang
Penyimpanan dan Rak Berkas Rekam Medis Aktif Untuk 5 Tahun
Mendatang di RSUP Muntilan Kabupaten Magelang.
Hasil dari penelitian ini berdasarkan perhitungan kebutuhan rak
penyimpanan berkas rekam medis untuk 5 tahun mendatang yang
dibutuhkan adalah 12 unit rak statis terbuka dengan pembagian 8 unit
rak statis 2 muka 5 shaf dan 4 unit rak statis terbuka satu muka 5
shaf.
Persamaan yang ada dengan penelitian ini adalah sama-sama
menyangkut perancangan ulang ruangan. Sedangkan perbedaannya
adalah penelitian yang perancang lakukan yaitu merancang ulang
ruang filing dengan lebih berfokus ke arah ke-ergonomisan suatu
ruangan, sehingga bisa mewujudkan sebuah ruang penyimpanan
yang efektif, nyaman, aman, sehat, dan efisien baik bagi petugas
maupun berkas.
3. Dwi Margawati (2010), melakukan penelitian dengan judul
Perancangan Ulang Ruang Rekam Medis Berbasis pada Desain Rak
Penyimpanan Berkas Rekam Medis di Puskesmas Klaten.
Hasil yang diperoleh dari penelitian ini adalah desain rak
penyimpanan berkas rekam medis dengan partisi yang dirancang
agar mudah dipindah atau digeser. Dasar perancangan adalah
matriks criteria dan diagram keterkaitan serta metode bubble.
Persamaan yang ada dengan penelitian ini adalah sama-
sama menyangkut perancangan ulang ruangan. Sedangkan
perbedaannya adalah penelitian yang perancang lakukan yaitu
merancang ulang ruang filing dengan lebih berfokus ke arah
keergonomisan suatu ruangan, sehingga bisa mewujudkan sebuah
ruang penyimpanan yang efektif, nyaman, aman, sehat, dan efisien
baik bagi petugas maupun berkas.

1. Kondisi Ruang Penyimpanan
Tidak adanya Air Conditioner (AC) maupun kipas angin pada pada
masing-masing ruang penyimpanan ditambah kurangnya jumlah
jendela sangat mempengaruhi suhu udara pada ruang
penyimpanan berkas rekam medis di Rumah Sakit Panti Rini
Kalasan, akibatnya ruangan menjadi pengap dan panas. Selain itu
ruang penyimpanan yang terlalu jauh dari ruang Instalasi Rekam
Medis menjadikan berkas rekam medis di Rumah Sakit Panti Rini
Kalasan tidak terjaga keamanannya, padahal isi dari berkas rekam
medis itu sendiri adalah hal yang sangat rahasia sehingga harus
dijaga keamanannya.
2. Kondisi Rak Penyimpanan
Menurut hasil observasi yang dilakukan diketahui bahwa rak yang
digunakan di Rumah Sakit Panti Rini Kalasan adalah rak model
terbuka yang terbuat dari rangkaian besi yang dapat dibongkar
pasang.
75
Tahap Perancangan
1. Perhitungan Kebutuhan Rak
Rak yang dibutuhkan untuk lima tahun mendatang di Rumah Sakit
Panti Rini Kalasan jika menggunakan rak yang dipilih oleh
perancang adalah 10 rak.
2. Rak Yang Tepat Berdasarkan Ilmu Ergonomi
Rak sebagai sarana penyimpanan terdiri dari berbagai jenis.
ukuran, bahan, dan bentuk sarana penyimpanan mempengaruhi
kemampuan daya tampung masing-masing rak. Rak yang paling
tepat untuk digunakan di Rumah Sakit Panti Rini Kalasan menurut
perancang adalah rak besi terbuka. Dengan spesifikasi terbuat dari
lempengan baja dengan ukuran panjang 3,5 m dan lebar 60 cm,
terdiri dari 5 shaft dan masing-masing shaft berukuran tinggi 30 cm.
3. Rancangan Ruang Peyimpanan Berdasarkan Ilmu Ergonomi
Rancangan ulang ruang penyimpanan berkas rekam medis di
Rumah Sakit Panti Rini Kalasan menurut perancang adalah :
76
Gambar 17. Rancangan Ruang Penyimpanan Berkas Rekam Medis
Yang Dirancang Oleh Perancang.
77
B. SARAN
1. Diharapkan adanya pengadaan kipas  angin atau Air Conditioner
(AC) agar udara diruang penyimpanan tidak panas atau pengap,
selain itu Air Conditioner (AC) juga berguna agar ruang
penyimpanan terhindar dari debu.
2. Sebaiknya ada pembatasan akses ke ruang penyimpanan
berkas rekam medis agar keamanan berkas rekam medis dapat
terjaga.
3. Untuk rak penyimpanan yang sudah ada sekarang, karena
adanya masalah kekurangan rak dan ruangan yang terbatas
sebaiknya dilakukan perubahan ukuran tinggi shaft rak,
mengingat rak yang dipakai  di Rumah Sakit Panti Rini Kalasan
terbuat dari lempengan besi yang dapat dibongkar pasang,
maka memungkinkan ukuran shaft yang sekarang 50 cm
diturunkan menjadi 30 cm, dan banyak shaft sekarang yang
hanya 5 ditambah menjadi 6, hal ini bisa dilakukan karena
berkas yang ada di Rumah Sakit Panti Rini mempunyai tinggi 23
cm.
4. Rancangan yang dibuat oleh perancang dapat digunakan
sebagai acuan untuk merancang ulang ruang penyimpanan
berkas rekam medis di Rumah Sakit Panti Rini Kalasan.

