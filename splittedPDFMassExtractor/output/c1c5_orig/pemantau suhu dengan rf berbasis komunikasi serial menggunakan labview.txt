1.1 Latar Belakang Masalah
Pengukuran suhu banyak dilakukan untuk mengukur ruangan, benda, zat cair dan lain-
lain. Pengukuran suhu ini tentunya berkaitan dengan panas dingin suatu zat atau keadaan
ruangan. Manusia sendiri sebenarnya sudah dibekali alat penginderaan yang dapat
mengatakan benda atau ruangan tersebut panas atau dingin namun secara kualitatif. Akan
tetapi didalam fisika panas, hangat, dan dingin suatu zat dinyatakan secara kuantitatif (
dengan nominal angka).
Temperatur dapat didefinisikan sebagai sifat fisik suatu benda untuk menentukan
apakah keduanya berada dalam kesetimbangan termal. Dua buah benda akan berada dalam
kesetimbangan termal jika keduanya memiliki temperatur yang sama. Ada beberapa sifat
benda yang berubah apabila benda itu dipanaskan, antara lain adalah warnanya, volumenya,
tekanannya dan daya hantar listriknya. Pengukuran suhu ini biasanya menggunakan alat
manual yaitu termometer. Dalam pengukuran suhu ruangan adapun kekuranganya
menggunakan termometer manual yaitu kurangnya keakuratan dalam pembacaan dan perlu
pengamatan jarak dekat ke termometernya untuk membaca nilai yang ditunjukkan pada
termometer tersebut dan kadang perlu mencatatnya untuk suatu pengukuran tertentu. Untuk
menutupi kekurangan tersebut maka dibuat alat pemantau suhu  dengan RF berbasis
komunikasi serial menggunakan LabVIEW.
1.2 Maksud dan Tujuan
Pengukuran suhu ruangan menggunakan media radio transmitter berbasis komunikasi
serial mode dengan dua mikrokontroler ATMega8, ini merupakan sistem pengukuran suhu
nirkabel dengan jarak kurang dari 40 m, pengukuran dapat dilakukan selama 24 jam nonstop,
data pengukuran ini dapat diolah pada LabVIEW dengan bermacam-macam fasilitas sehingga
kedepannya dapat dikembangkan untuk membantu pemantauan suhu pada suatu industri dan
perusahaa
2
1.3 Perumusan Masalah
Permasalahan yang dihadapi adalah bagaimana pengukuran suhu dapat diukur dengan
akurat secara jarak jauh yang ditampilkan dalam PC dengan user interface yang mudah
dibaca , yang menggunakan komponen hardware seminimal mungkin dan juga dengan tidak
mengurangi unjuk kerja alat.
1.4 Batasan Masalah
Tugas akhir ini diambil pembatasan masalah supaya tidak menyimpang dari judul
yang dibahas.  Alat ini digunakan untuk mengukur suhu ruangan, data suhu dari sensor suhu
dikonversikan oleh ADC mikrokontroler dan dikirim menggunakan media gelombang radio
frekuensi, diterima oleh CDC-232 yang dikomunikasikan secara serial dan selanjutnya
ditampilkan pada komputer.
1.5 Metodelogi Penulisan
Tugas akhir ini ditekankan pada perancangan dan perakitan rangkaian, agar dapat
menampilkan unjuk kerja sesuai yang direncanakan. Perangkat keras ini dirancang
sesederhana mungkin, dengan tidak mengurangi unjuk kerja sistem secara keseluruhan,
demikian juga perangkat lunak dirancang sedemikian sehingga bisa menampilkan hasil
dengan user interface yang mudah dibaca dan nyaman dilihat dan tentunya akan bersinergi
secara baik. Kemudian setelah dibuat, diujicobakan dan dianalisis dengan mengintegrasikan
semua bagian dari sistem untuk memastikan bahwa sistem berjalan dengan baik dan sesuai
harapan.
1.6 Sistematika Penulisan
Penulis membagi sistematika penulisan, dengan beberapa pokok permasalahan, yang
antara lain :
BAB I PENDAHULUAN
3
Membahas tentang latar belakang dan permasalahan, maksud dan tujuan,
alasan pemilihan judul dan metodologi penulisan.
BAB II TINJAUAN PUSTAKA
Menguraikan informasi hasil penelitian sebelumnya yang mendasari
penelitian ini.
BAB III LANDASAN TEORI
Memuat tentang landasan teori setiap komponen yang menunjang dalam
pembuatan dan pembahasan Tugas Akhir ini.
BAB IV PERENCANAAN DAN PERANCANGAN SISTEM
Berisi gambaran secara keseluruhan dan perancangan sistem baik
perangkat keras maupun perangkat lunak.
Memuat uraian tentang implementasi sistem secara detail sesuai dengan
rancangan dan berdasarkan komponen serta bahasa pemrograman yang dipakai
serta penjelasan ilmiah.
Berisi pengujian baik sistem kerja alat pengukuran dan pengujian alat
secara keseluruhan, termasuk pembahasan sistem hardware dan software.
Berisi tentang kesimpulan atas penelitian yang telah dilakukan dan
memberikan saran untuk pengembangan sistem lebih lanjut.

Build Table tersebut terdiri dari 5 input dan 2 output, pada program yang dibuat
hanya menggunakan 3 input dan 1 output, yang pertama Signals, Signals ini
adalah masukan utama data yang akan dibuat tabel, kemudian menggunakan
include time date, include time date ini bertipe Boolean jadi cukup di berikan
masukan Boolean. Dan input yang terakhir adalah reset, input ini juga bertipe
Boolean. Sedangkan untuk output pada tabel diberi indicator saja dengan
pengaturan 2 kolom yaitu kolom tanggal & waktu dan suhu.
PENUTUP
7.1 Kesimpulan
Kesimpulan yang didapat dari tugas akhir ini adalah :
1. Telah dirancang alat Pemantau suhu dengan RF berbasis komunikasi serial
menggunakan LabVIEW.
2. Alat ini dapat mengukur suhu jarak jauh dan ditampilkan pada PC
menggunakan software LabVIEW dengan interval perdetik.
3. Dapat menyimpan data pengukuran kedalam PC.
4. Pengukuran sudah memperhitungkan suhu rata-rata, suhu maksimum, dan
suhu minimum.
5. Alat mampu bekerja stabil selama 24 jam nonstop.
7.2 Saran
Alat yang penulis rancang ini bukanlah alat yang sempurna dan tentunya masih ada
kekurangan, diantaranya :
1. Pengukuran jaraknya terbatas sesuai dengan kemampuan modul radio
transmitter dan receiver-nya. Untuk kedepannya dapat diganti modul RF
dengan jangkauan yang lebih jauh.
2. Nilai rata-rata bisa di dapat dengan men-stop dulu pada software LabVIEW,
jadi tidak secara realtime seperti pada suhu maksimum dan minimum. Untuk
kedepannya fungsi ini dapat berjalan secara realtime.
3. Pengukuran yang akurat di ukur dalam jarak kurang dari 12 meter,
selebihnya data akan ada yang hilang karena sesuai dengan baik tidaknya
kemampuan modul RF tersebut.
4. Data akan terjadi bentrok ketika ada frekuensi lain yang sesuai dengan
modul RF receiver dengan frekuensi 433Mhz.

