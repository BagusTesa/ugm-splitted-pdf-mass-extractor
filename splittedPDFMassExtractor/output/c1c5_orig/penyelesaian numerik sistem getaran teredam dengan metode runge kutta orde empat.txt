1.1 Latar Belakang dan Permasalahan
Ilmu fisika merupakan ilmu yang mempelajari berbagai macam fenomena
alam dan berperan penting dalam kehidupan sehari-hari. Salah satu peran ilmu fisika
adalah mendukung perkembangan teknologi dan industri. Banyak sekali cabang-
cabang dari ilmu fisika yang diterapkan untuk perkembangan dan kemajuan teknologi
dan industri, salah satu contohnya adalah bidang yang mempelajari getaran. Dasar-
dasar dari ilmu getaran dimanfaatkan oleh industri motor untuk pengembangan salah
satu produk komponen penting pada motor yaitu shock breaker. Shock breaker
dirancang dengan menggunakan prinsip sistem getaran teredam.
Shock breaker atau peredam kejut adalah alat yang terbuat dari logam baja
yang berfungsi sebagai peredam goncangan agar body kendaraan tidak mengalami
guncangan berlebihan saat melewati jalan bergelombang. Shock breaker pada
kendaraan dibuat untuk memberikan kenyamanan bagi pengendara. Akan tetapi
dalam kenyataannya tidak semua shock breaker sesuai keinginan konsumen.
Ketidaksesuaian ini diakibatkan karena tujuan penggunaan shock breaker oleh
konsumen berbeda antar satu dengan yang lain, misalnya seorang pembalap
menginginkan sebuah shock breaker yang memiliki redaman yang kuat. Berbeda
dengan khalayak umum yang tidak terlalu memperhatikan shock breaker yang
digunakan karena hanya mementingkan kenyamanan dalam berkendara. Untuk itu
diperlukan modifikasi sistem getaran teredam pada shock breaker yang berhubungan
dengan koefisien redaman dan koefisien pegas sehingga nantinya didapatkan shock
breaker yang baik dan nyaman digunakan.
Penyelesaian model matematis sistem getaran teredam bisa menggunakan
metode analitik dan metode numerik. Metode analitik membutuhkan waktu yang
2
lama. Untuk itu dibutuhkan metode numerik dalam penyelesaian sistem getaran
teredam. Metode analitik yaitu metode yang dapat memberikan hasil sesungguhnya
(eksak). Metode ini memang akurat tetapi memiliki kelemahan. Metode ini hanya
terbatas menyelesaikan masalah matematis tertentu saja. Oleh karena itu dibutuhkan
metode numerik. Metode numerik adalah pendekatan dari hasil eksak suatu masalah
matematika. Meskipun begitu kesalahan (galat) metode numerik dapat diminimalisasi
sehingga mendekati hasil eksak.
Sistem getaran teredam merupakan salah satu permasalahan kompleks dalam
fisika. Persamaan sistem getaran teredam merupakan Persamaan Differensial Biasa
(PDB), dapat diselesaikan menggunakan metode analitik namun membutuhkan waktu
yang lama dan proses penyelesaian yang panjang. Untuk itu dibutuhkan metode
numerik untuk mendapatkan solusi PDB yang lebih baik dan lebih efisien untuk
menyelesaikan masalah dalam sistem getaran teredam. Metode numerik dipilih untuk
mempercepat proses perhitungan. Beberapa metode numerik yang digunakan dalam
penyelesaian PDB diantaranya adalah metode Euler, metode Heun, metode Deret
Taylor, metode Runge-Kutta. Metode-metode tersebut memiliki cara pendekatan
yang sama tapi memiliki tingkat ketelitian yang berbeda. Metode Euler memiliki
tingkat ketelitian yang lebih rendah dibandingkan dengan metode Runge-Kutta.
Dalam proses penyelesaian suatu persamaan metode Runge-Kutta memiliki langkah
uraian matematis yang lebih panjang dibandingkan metode Euler, tapi memiliki
tingkat ketelitian yang lebih baik daripada metode Euler.
Pada sistem getaran teredam variabel-variabel yang akan dicari adalah
simpangan, kecepatan, dan percepatan. Simpangan, kecepatan, dan percepatan benda
pada sistem tersebut dapat diperoleh dengan menggunakan variasi waktu. Metode
Runge-Kutta merupakan salah satu metode yang digunakan untuk menyelesaikan
masalah tersebut. Metode Runge-Kutta yang dipilih adalah metode Runge-Kutta orde
empat. Apabila diberikan suatu nilai untuk kondisi awal maka nilai-nilai kondisi
3
berikutnya akan diperoleh. Komputasi numerik untuk menyelesaikan sistem tersebut
menggunakan Microsoft Office Excel.
1.2 Rumusan Masalah
Rumusan masalah dalam penelitian ini adalah :
1. Bagaimana memperoleh variabel-variabel sistem getaran teredam
dengan menggunakan metode Runge-Kutta orde empat?
2. Variabel-variabel tersebut adalah simpangan ( ), kecepatan ( ?), dan
percepatan ( ??).
1.3 Batasan Masalah
Masalah yang akan diteliti adalah penyelesaian numerik sistem getaran
teredam dari sebuah massa 	yang terhubung pada suatu benda tegar yang keduanya
terhubung pegas linier yang konstanta pegasnya  dan peredam getar yang koefisien
redamannya . Variabel-variabel getaran yaitu simpangan (x), kecepatan ( ?) dan
percepatan ( ) kemudian dicari menggunakan metode Runge-Kutta orde empat.
Dari variabel-variabel yang diperoleh digambarkan grafik sebagai fungsi waktu ( ).
1.4 Tujuan Penelitian
Tujuan dari penelitian ini adalah untuk menemukan solusi numerik dari sistem
getaran teredam menggunakan lembar kerja Microsoft Office Excel dengan metode
Runge-Kutta orde empat.
1.5 Manfaat Penelitian
Manfaat dari penelitian ini yaitu :
1. Memperoleh solusi numerik untuk menyelesaikan sistem getaran
teredam.
4
2. Menghasilkan program komputasi untuk menghitung nilai gaya dan
gerak dalam sistem getaran teredam.
3. Menghasilkan suatu simulasi sederhana untuk melakukan rekayasa
teknologi yang memiliki prinsip sistem getaran teredam.
4. Sebagai sumbangan ide untuk memecahkan masalah numerik pada
sistem getaran teredam.
5. Melatih kemampuan dalam meyelesaikan suatu permasalahan fisika
secara numerik.
1.6 Sistematika Penulisan
Penyajian skripsi ini telah mengikuti aturan baku dalam susunan penulisan
skripsi. Skripsi ini disusun menjadi lima bab. Masing-masing bab terdiri dari
beberapa sub-bab. Adapun susunan bab secara rinci adalah sebagai berikut:
a. Bab I Pendahuluan: berupa penjelasan dari latar belakang dan
permasalahan yang mendasari penelitian ini. Pada latar belakang
disampaikan penelitian-penelitian yang sudah ada sebelumnya dan
beberapa aplikasi ilmu yang sudah ada menggunakan metode
penelitian ini. Batasan masalah dari penelitian dan apa saja yang
dirumuskan dalam penelitian ini. Di bab ini juga dibahas tujuan dan
manfaat dari penelitian.
b. Bab II Tinjauan Pustaka: berisi rujukan dan referensi serta
penelitian-penelitian yang telah dilakukan yang berhubungan dengan
penelitian ini.
c. Bab III Landasan Teori: berisi teori-teori yang menunjang penelitian
ini diantaranya adalah sistem getaran pegas teredam, persamaan
differensial biasa, metode Excel komputasi, dan metode numerik
dengan menggunakan Runge-Kutta orde empat.
5
d. Bab IV Metode Penelitian: berisi tentang metode yang digunakan
dalam penelitian ini, yaitu metode Runge-Kutta orde empat dan
algoritma komputasi Excel.
e. Bab V Hasil Penelitian dan Pembahasan: merupakan hasil
komputasi berupa plotting grafik dari metode analisis dan data-data
referensi dengan menggunakan komputasi Excel.
f. Bab VI Kesimpulan dan Saran: pada bab ini akan disimpulkan hasil
penelitian dan pembahasan yang telah dilakukan serta menyampaikan
saran terkait dengan penelitian untuk menunjang penelitian-penelitian
berikutnya.

Berdasarkan hasil pengujian program komputasi dan batasan masalah yang
telah disebutkan, diperoleh kesimpulan dan saran sebagai berikut:
6.1.  Kesimpulan
Dari penelitian yang telah dilakukan dapat disimpulkan bahwa:
1) Telah diperoleh solusi numerik dari sistem getaran teredam menggunakan
rumus lembar kerja Microsoft Office Excel dengan metode Runge-Kutta
orde empat.
2) Telah ditemukan variabel-variabel sistem getaran teredam menggunakan
metode Runge-Kutta orde empat yaitu simpangan , kecepatan ?, dan
percepatan ??.
3) Komputasi  Excel dapat digunakan sebagai simulasi untuk penyelesaian
sistem getaran teredam.
4) Simulasi komputasi Excel bisa digunakan untuk membuktikan kondisi
getaran teredam lemah, getaran teredam kritis, dan getaran teredam kuat.
5) Komputasi Excel lebih mudah digunakan dan dipahami untuk membuat
sebuah simulasi komputasi.
6.2  Saran
Komputasi menggunakan rumus lembar kerja Excel memiliki kelemahan yaitu
keterbatasan dalam pengolahan data, semakin lambat jika data yang diolah semakin
banyak. Untuk jumlah data yang sangat besar, disarankan menggunakan program
yang memang dirancang untuk komputasi, misalnya Fortran, MATLAB, dan
sebagainya.

