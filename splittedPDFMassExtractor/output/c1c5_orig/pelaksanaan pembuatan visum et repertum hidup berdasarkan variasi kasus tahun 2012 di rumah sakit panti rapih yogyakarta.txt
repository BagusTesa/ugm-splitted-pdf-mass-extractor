A. Latar Belakang
Kemajuan  ilmu  pengetahuan  dan  teknologi  dalam  era  globalisasi
khususnya di bidang kesehatan semakin pesat. Begitu juga dengan
perkembangan  ilmu  hukum  kedokteran  atau  yang  lebih  kita  kenal  dengan
hukum kesehatan.
Hukum   kesehatan   diartikan   sebagai   hukum   yang   berhubungan
langsung  dengan  pemeliharaan  kesehatan  meliputi  penerapan  perangkat
hukum   perdata,   pidana,   dan   tata   usaha   negara.   Sejak diterbitkannya
Permenkes  RI  No.  269/MENKES/PER/III/2008  tentang  rekam  medis  sejak
saat   itu   penyelenggaraan   rekam   medis   mempunyai   kekuatan   hukum   di
bidang administrasi.
Rekam   medis   memiliki   peran   dan   fungsi   yang   sangat   penting,
yaitu   sebagai   dasar   pemeliharaan   kesehatan   dan   pengobatan   pasien,
bahan pembuktian dalam perkara hukum, bahan untuk keperluan penelitian
dan pendidikan, dasar pembayaran biaya pelayanan kesehatan dan
terakhir sebagai bahan untuk membuat statistik kesehatan (Hatta, 2010).
Rekam medis harus berisi informasi lengkap perihal proses
pelayanan kesehatan dimasa lalu, masa kini, dan perkiraan dimasa
mendatang. Kepemilikan rekam medis seringkali menjadi perdebatan
dilingkungan   kesehatan,   dokter   beranggapan   bahwa   mereka   berwenang
penuh terhadap pasien beserta pengisian rekam medis akan tetapi petugas
rekam medis bersikeras untuk mempertahankan berkas rekam medis untuk
tetap  selalu  berada  di  lingkungan  kerjanya.  Selain  itu  banyak  pula  pihak
internal   maupun   pihak   eksternal   yang   ingin   mengetahui   isi   dari   rekam
medis itu sendiri. Hal tersebut menunjukkan bahwa rekam medis itu sangat
penting dan besar kaitannya dengan aspek hukum (Hatta, 2010).
Rekam medis erat kaitannya dengan aspek hukum  yang berkaitan
dengan   menjaga   keamanan,   privacy,   dan   kerahasiaan.   Rekam   medis
1
mempunyai  kegunaan  penting  di  bidang  hukum  karena  isi  dalam  rekam
medis  itu  sendiri  menyangkut  masalah  adanya  jaminan  kepastian  hukum
atas dasar keadilan dalam rangka usaha menegakkan hukum serta
penyediaan   bahan   tanda   bukti   untuk   menegakkan   keadilan.   Kegunaan
rekam  medis  adalah  sebagai  alat  bukti  yang  sah  dan  nyata  tentang  telah
diberikannya pelayanan kesehatan dan pengobatan selama pasien
tersebut  dirawat  di suatu sarana pelayanan kesehatan.  Rekam  medis  yang
teratur  dan  rapi  dibuat  secara  kronologis  dengan  baik  dan  lengkap  akan
menjadi bukti yang kuat di pengadilan.
Beberapa  hal  yang  berkaitan  dengan  aspek  hukum  rekam  medis
yaitu kerahasiaan, kepemilikan, dan keamanan dari berkas rekam medis itu
sendiri. Oleh karena rekam medis adalah milik pelayanan kesehatan dan isi
rekam  medis  adalah  milik  pasien  maka  pihak  rumah  sakit  maupun  praktisi
kesehatan lainnya bertanggungjawab mengatur penyebaran, menjaga
kerahasiaan, menjaga keamanan informasi kesehatan, dan juga
melindungi isi daripada informasi yang ada di berkas rekam medis,
terhadap   kemungkinan   hilangnya   keterangan   maupun   manipulasi   data
yang  ada  di  dalam  rekam  medis  atau  dipergunakan  oleh  pihak  yang  tidak
seharusnya (Hatta, 2010)
Pelepasan  informasi  kesehatan  dari  rekam  medis  atau  yang  biasa
disebut surat keterangan medis adalah  suatu surat keterangan yang dibuat
dan  ditandatanagni  oleh  staff  medis  fungsional  dan  tim  medis  yang  berisi
informasi  medis  sesui  dengan  isi  berkas  rekam  medis  pasien,  ahli  waris
pasien,  institusi  pemerintah  atau  swasta.  Surat  keterangan  medis  secara
umum  dibagi  menjadi  dua  yaitu  surat  keterangan  medis  non  pengadilan
dan untuk pengadilan.
Jenis  surat  keterangan  medis  untuk  pengadilan  adalah  visum  et
repertum.  Visum  et  repertum  adalah  keterangan  yang  dibuat  oleh  dokter
forensik atas permintaan tertulis dari penyidik berdasarkan sumpah tentang
apa  yang  dilihat  dan  ditemukan  pada  benda  yang  diperiksa  berdasarkan
pengetahuan  yang  sebaik  baiknya  untuk  kepentingan  pengadilan.  Dalam
pembuatan visum et repertum dibutuhkan kerjasama antara dokter forensik
dan  perekam  medis.  Untuk  itu  penerapan  etika  profesi  harus  diterapkan
2
dalam   kerjasama   ini   supaya   menghasilkan   hubungan   yang   baik   antar
profesi kesehatan di sarana pelayanan kesehatan (Waluyadi, 2005).
Penyelenggaraan   rekam   medis   yang   baik   bukan   semata-mata
untuk  keperluan  medis  dan  administrasi,  tetapi  juga  karena  isinya  sangat
diperlukan  oleh  individu  dan  organisasi  yang  secara  hukum  berhak  untuk
mengetahuinya.  Pengadilan  sebagi  salah  satu  badan  resmi  secara  hukum
berhak untuk meminta pemaparan isi rekam medis jika kasus  yang sedang
ditanganinya membutuhkan rekam medis sebagai alat bukti penyelidikan.
Petugas  rekam  medis  harus  memahami  dan  mengerti  bagaimana
prosedur pemaparan isi rekam  medis untuk pengadilan. Peraturan ataupun
prosedur   tersebut   disosialisasikan   untuk   dilaksanakan   oleh   pihak-pihak
yang  bersangkutan   dengan   pemaparan   isi   rekam  medis,   sehingga   tidak
terjadi kesalahan prosedur dan tidak menimbulkan adanya tuntutan dimasa
yang akan datang.
Berdasarkan hasil studi pendahuluan yang dilakukan pada tanggal
18   Februari   2013   ditemukan   suatu   permasalahan   dalam   pelaksanaan
pengeluaran visum et repertum hidup di Rumah Sakit Panti Rapih
Yogyakarta. Banyak ditemukan pembuatan visum et repertum yang
melebihi  aturan  standar  prosedur  operasional  yang  ada  yaitu  lebih  dari  7
hari. Selain itu penyimpanan arsip visum et repertum juga belum
semuanya  disimpan  dalam  satu  stockholder,  masih  ada  beberapa  yang
ikut  disimpan  di  rak  filling  bersama  dengan  berkas  rekam  medis  pasien
yang bersangkutan.
Pembuatan   visum   et   repertum   yang   tidak   tepat   waktu   sesuai
dengan standar operasional prosedur yang sudah ada dapat
mengakibatkan  tertundanya  persidangan,  sehingga  dibutuhkan  pembuatan
visum   et   repertum yang   tepat waktu   untuk   kepentingan   persidangan.
Penyimpanan   arsip   visum   et   repertum   yang   baik   dapat   memudahkan
dalam  pencarian  kembali  jika  sewaktu  waktu  diperlukan.  Oleh  karena  itu
penulis ingin meneliti tentang bagaimana pelaksanaan pembuatan visum et
repertum  hidup  berdasarkan  variasi  kasus  pada  tahun  2012  ,  khususnya
terkait pelayanannya di bagian rekam medis untuk pembuatan surat
keterangan   medis pengadilan khususnya visum   et repertum hidup di
Rumah Sakit Panti Rapih Yogayakarta.
3
B. Rumusan Masalah
Berdasar latar belakang yang telah disebutkan diatas maka
rumusan   masalah   dalam   penelitian   ini   adalah   �Bagaimana   pelaksanaan
pembuatan   Visum   et   Repertum   Hidup   berdasarkan   Variasi   Kasus   Pada
Tahun 2012 di Rumah Sakit Panti Rapih Yogyakarta ?�
C. Tujuan Penelitian
1. Tujuan Umum
Mengetahui   pelaksanaan   pembuatan   Visum   et   Repertum   hidup
berdasarkan   variasi   kasus   pada   tahun   2012   di   Rumah   Sakit   Panti
Rapih Yogyakarta.
2. Tujuan Khusus
a. Mengetahui pelaksanaan permintaan pembuatan visum et
repertum di Rumah Sakit Panti Rapih Yogyakarta yang meliputi :
1. Prosedur  permintaan  visum  et  repertum  di  Rumah  Sakit  Panti
Rapih Yogyakarta.
2. Pihak yang berhak untuk meminta pengeluaran visum et
repertum.
3. Pihak yang berhak memaparkan isi rekam medis untuk
pengadilan.
4. Macam-macam  informasi  kesehatan  dan  isi  data  yang  terdapat
dalam  pelepasan  informasi  kesehatan  dari  rekam  medis  untuk
pembuatan visum et repertum hidup di Rumah Sakit Panti Rapih
Yogyakarta.
b. Mengetahui hasil analisa ketepatan waktu pelaksanaan
pengeluaran  visum  et  repertum  berdasarkan  variasi  kasus  selama
periode tahun 2012 di Rumah Sakit Panti Rapih Yogyakarta.
c. Mengetahui kendala   yang   mempengaruhi   proses   pelaksanaan
pengeluaran  visum  et  repertum  hidup  di  Rumah  Sakit  Panti  Rapih
Yogyakarta
4
D. Manfaat Penelitian
1. Manfaat Praktis
a. Bagi Rumah Sakit
Secara  teoritis  penelitian  ini  diharapkan  dapat  memberikan  manfaat
dalam  bentuk  sumbang  saran  untuk  perkembangan  ilmu  pelayanan
kesehatan di Rumah Sakit.
b. Bagi Peneliti
Menambah pengetahuan dan wawasan serta pengalaman langsung
terjun   ke   Rumah   Sakit   dengan   mencoba   menerapkan   teori   yang
diperoleh dari institusi pendidikan.
2.   Manfaat Teoritis
a. Bagi Institusi Pendidikan
Penelitian ini dapat memberikan masukan ilmu yang berguna
sebagai bahan pembelajaran dan memperkaya ilmu pengetahuan
b. Bagi Peneliti Lain
Sebagai acuan dalam pendalaman materi yang bersangkutan untuk
kelanjutan penelitian yang relevan.
E. Keaslian Penelitian
Berdasarkan informasi penelusuran studi kepustakaan dan
pemantauan   yang   penulis   lakukan,   maka   diketahui   bahwa   belum   ada
dilakukan  penelitian   yang  serupa  dengan  apa  yang  menjadi  bidang  dan
ruang lingkup penelitian penulis ini, yaitu mengenai : ��Bagaimana
pelaksanaan   pengeluaran   visum   et   repertum   hidup   berdasarkan   variasi
kasus  pada  tahun  2012  di  Rumah  Sakit  Panti  Rapih  Yogyakarta�,  namun
demikian penelitian serupa pernah dilakukan, antara lain :
1. Permati, Ciptaning Dyah Kusuma (2012), dengan judul Visum et
Repertum  Jenasah  Sebagai  Pertimbangan  Hakim  Dalam  Menjatuhkan
5
Putusan  Di  Pengadilan  Negeri  Yogyakarta.  Hasil  penelitian  ini  secara
umum visum et repertum di pengadilan negeri yogyakarta
pembuatannya  sudah  sesuai  dengan  standar  pembuatan  yang  ada  dan
tidak   memiliki masalah   dalam hakim menggunakan   visum tersebut
dalam   pertimbangan   pemutusan   perkara.   Namun   visum   et   repertum
yang  belum  sesuai  dengan  standar  pembuatannya  justru  ditemui  oleh
penyidik di Polres Ngaglik sebelum Visum et Repertum tersebut
diproses di pengadilan.
Persamaan penelitian ini dengan   penelitian  tersebut adalah sama
sama   menggunakan   jenis   penelitian   deskriptif   dengan   menggunakan
pendekatan kualitatif, rancangan penelitian cross-sectional dan
pengumpulan  data  dengan  menggunakan  metode  wawancara  dan  studi
dokumentasi serta mebahas subjek yaitu visum et
repertum.Perbedaannya  adalah  pada  penelitian  diatas  menitikberatkan
pada visum et repertum jenazah sebagai pertimbangan hakim,
sedangkan  penelitian  ini  menitikberatkan  pada  pelaksanaan  pengeluran
visum et repertum hidup berdasarkan variasi kasusnya.
2.   Sari,   Septriani   Ade   (2012)   dengan   judul   Pelaksanaan   Pelepasan
Informasi Kesehatan di  Balai pengobatan  Lembaga Pemasyarakatan klas
II A Yogyakarta.
Hasil penelitian ini adalah pembuatan surat Informasi yang
dilepaskan ke pihak ke tiga dalam pelepasan informasi kesehatan di balai
pengobatan  Lembaga  Pemasyarakatan  Klas  II  A  Yogyakarta  yaitu  dibuat
oleh   pihak Balai Pengobatan. Dibuat   atas   sepengetahuan   dan
kewenangan  dokter  lembaga  pemasyrakatan.  Dari  klinik  akan  dibawa  ke
Seksi   Binapi   (Pembinaan)   untuk   dicek   tujuan   isi   surat,   Warga   Binaan
Pemasyrakatan (WBP) dan untuk di teruskan ke kepala lembaga
pemasyarakatan   dan   kembali   lagi   ke   Seksi   Binapi   untuk   pengawasan
WBP  dan  pelepasan  informasi  kesehatan.  Pembuatan  surat  rujukan  di
Balai  Pengobatan  Lembaga  Pemasyarakatan  Klas  II  A  Yogyakarta  masih
terdapat  perawat  yang  membuat  surat  rujukan  keluar.  Yang  melepaskan
informasi kesehatan keluar kepihak ketiga yaitu dari seksi Binapi. Dimana
6
informasi  kesehatan  pasien  hanya  dapat  disampaikan  oleh  dokter  yang
berwenang atau yang menjaga data rekam medis.
Persamaan penelitian ini dengan Septriana Ade Sari adalah sama
sama membahas tentang pelepasan informasi kesehatan di suatu
pelayanan  kesehatan.  Selain  itu  jenis  penelitian  yang  digunakan  sama
yaitu  deskriptif  kualitatif  dengan  rancangan  cross  sectional.Pebedaannya
adalah  jika  penelitian  Septriana  ade  Sari  membahas  pelepasan  informasi
secara   umum   ,   penelitian   ini   menspesifikasikan   pelepasan   informasi
kesehatan secara khusus untuk pembuatan visum et repertum.
3. Astuti,   Gencar   Puji   (2004),   dengan   judul   Pengelolaan   Visum   et
Repertum Psikiatrik di Rumah Sakit Jiwa Daerah Surakarta.
Hasil  dari  penelitian  ini  pada  pelaksanaannya,  proses  pembuatan
visum   et   repertum   psikiatrik   tidak   sesuai   dengan   prosedur   tetap   yang
telah ditentukan oleh RSJ Daerah Surakarta. Proses pembuatan  visum et
repertum psikiatrik dilakukan di Sub bagian tata usaha. Proses pelepasan
atau  penyerahan  visum  et  repertum  kepada  pihak  peminta  melalui  pos
atau diambil langsung ke sub bagian tata usaha. Faktor yang
menyebabkan proses pembuatan visum et repertum psikiatrik tidak
dilaksanakan  di  Sub  Bag  Rekam  Medis  adalah  sumber  daya  manusia,
yaitu kurangnya pengetahuan petugas rekam medis  tentang fungsi-fungsi
rekam  medis  sehingga  dari  pihak  rekam  medis  tidak  ada  pembenaran
tentang   pelaksanaan   fungsi   rekam   medis   sebagai   penyuguh   informasi
pasien dalam visum et repertum psikiatrik.
Persamaan penelitian tersebut  dengan penelitian ini adalah  sama-
sama menggunakan  jenis penelitian deskriftif  kualitatif  dengan rancangan
cross   sectional,   serta   sama-sama   meneliti   objek   visum   et   repertum.
Perbedannya   pada   penelitian   diatas   lebih   memfokuskan   pada   proses
pembuatan dan  pengelolaan  visum  et  repertum psikiatrik,  pada penelitian
ini  memfokuskan  pada  pelaksanaan  pembuatan  visum  et  repertum  yang
ditinjau ketepatan waktu pembuatannya jika dilihat dari variasi kasusnya.
7
F. Gambaran Umum Rumah Sakit Panti Rapih Yogyakarta
1.  Jenis Rumah Sakit
Rumah Sakit Panti Rapih berada dibawah naungan keuskupan Agung
Semarang,   dikelola   bersama   sama   suster   suster   Tarekat   Cinta   Kasih
Santa   Corollus   Borromeus   dan   sebagai   pelaksanaan   adalah   Yayasan
Panti   Rapih.   Rumah   Sakit   Panti   Rapih   adalah   salah   satu   rumah   sakit
swasta   di   Daerah   Istimewa   Yogyakarta   yang   merupakan   rumah   sakit
dengan  tipe  B.  Selain  sebagai  pelayanan  kesehatan  Rumah  Sakit  Panti
Rapih juga digunakan sebagai tempat pendidikan bagi calon perawat, dan
institusi kesehatan lain seperti apoteker, fisioterapi, dan lain sebagainya.
2.  Kepemilikan
Rumah Sakit Panti Rapih merupakan rumah sakit swasta di
Yogyakarta milik Yayasan Panti Rapih.
3.  Jenis Pelayanan
a.  Pelayanan Medis
1)  Instalasi Gawat Darurat (IGD)
2)  Rawat Inap
3)  Rumah  Sakit  Panti  Rapih  Yogyakarta  memiliki  poliklinik  rawat  jalan
sebagai berikut :
a)  Poliklinik Umum
i.   Subspesialis Endokrinolog
ii.   Subspesialis Hematology
iii.   Subspesialis Infeksi
iv.   Subspesialis Cardiology
v.   Subspesialis Gatroenterology
vi.   Subspesialis Hepatology
b)  Poliklinik Kesehatan Anak
i.  Subspesialis Neo/Perinatology
ii. Subspesialis Hematology Anak
c)  Poliklinik Gigi
i.  Spesialis Ortodentist
ii. Spesialis Bedah Mulut
iii. Spesialis Protesa
8
iv. Spesialis Konversi Gigi
d)  Poliklinik Endrokopik
i.  Gastroscopy
ii. Bronchoscopy
iii. Coloncopy
iv. Urethroscopy
v. Urethrorenscopy
e)  Poliklinik Bedah
i.   Bedah Umum
ii. Digestive
iii. Orthopedic
iv. Oncologic
v. Neuro
vi. Urology
vii.   Anak
viii.   Thorax dan vasculer
ix. Mulut
x. Plastik
xi. Laparoskopik
f)   Klinik Kebidanan dan Kandungan
g)  Klinik Penyakit Mata
h)  Klinik Kulit Kelamin
i) Klinik Syaraf
j) Klinik Jiwa
k)  Klinik Psikologi
l) Klinik Penyakit Paru
m) Klinik Penyakit Kulit Kosmetik
n)  Klinik Penyakit Asma dan Alergi
o)  Klinik Gizi
p)  Klinik Rehab Medik
q)  Klinik Radiotherapy
r)   Pelayanan Pengobatan Alternatif
s)  Klinik Akupuntur dan Jamu
t)   Medhical Check Up
9
Rawat   jalan   Rumah   Sakit   Panti   Rapih   didukung   oleh   beberapa
dokter umum, dokter spesialis, dan dokter subspesialis.
b.  Pelayanan Penunjang
Rawat   jalan   Rumah   Sakit   Panti   Rapih   juga   mempunyai   fasilitas
layanan pemeriksaan penunjang sebagai berikut :
1)  Pemeriksaan Penunjang: Audiometri, Electroencephalography
(EEG), Spirometri Liver Function Test (LFT), Treadmill,
Ultrasonography  (USG),  Electrocardiography  (ECG),  Densitometri,
Fisioterapi, Radiologi, Diagnostic, Hemodialisa.
2)  Ganti Verban
3)  Medical Check Up
4)  Pojok  Tuberculosis  Direct  Observed  Treatment  Short  Course  (TB
DOTS)
5)  Unit Pelayanan Perempuan
6)  Pelayanan Voluntery Counseling and Testing (VCT) HIV-AIDS
7)  BERA (Pelayanan penunjang untuk penyakit syaraf)
8)  Konsultasi Bidan Anak (BA)
9)  Senam Hamil
4.  Jumlah Tempat tidur :
Rumah Sakit Panti Rapih Yogyakarta mempunyai fasilitas tempat tidur
sebanyak dengan perincian sebagai berikut :
10
Tabel 1
Jumlah Tempat Tidur Rumah Sakit Panti Rapih Berdasarkan Kelas Tahun 2011
Sumber : Instalasi Rekam Medis Rumah Sakit Panti Rapih tahun 2011
11
N
o
RUPER
PERINCIAN KELAS
JML
VVIP VIP 1A 1B 1C 2 3
PU
S
1 CB 2 RA  4 2 6  8 16 2 38
2 CB 2 RI  1 1 2  3 2 1 10
3 CB 3 KK  4       4
4 CB 3 IMC   1 1  1 1  4
5
CB
3
ICCU
1 1 2  3 3 1 11
6 CB 4 BK   3 7  14 7 1 32
7 CB 4 BL  2 2 4 1 5 8 1 23
8 CB 5 DB   23      23
9
CB 6
DB
11 18     29
10 EG 1 PB      15 20 3 38
11 EG 2 PB      15 16 3 34
12 EG 3 PD      15 20 3 38
13 EG 4 PD      15 20 3 38
14 LK 2 DB     17    17
15 LK 3 DB     17    17
16 MY DB 1 10 4      15
JUMLAH 1 22 48 40 35 94 113 18 371
Keterangan :
1. RUPER = Ruang Perawatan
2. PUS = Puspita (kelas bagi pasien yang tidak mampu)
5.  Performance
Tabel 2
Performance Rumah Sakit Panti Rapih Yogyakarta tahun 2010-2012
Sumber : Sub Seksi Rekam Medis Rumah Sakit Panti Rapih Yogyakarta,2012
INDIKATOR 2010 2011 2012
BOR 82,50% 75,85% 78,65%
BTO 59,50 kali 54,91 kali 56,85 kali
LOS 4,99hari 4,98 hari 5 hari
TOI 1,07 hari 1,60 hari 1,37 hari
GDR 27,66 � 30,53 � 31,24�
NDR 40,52 � 43,20 � 43,14�

1. Pelaksaan pengeluaran visum et repertum di Rumah Sakit Panti
Rapih Yogyakarta adalah sebagai berikut :
a. Prosedur   permintaan   visum   et   repertum   di   Rumah   Sakit
Panti Rapih Yogyakarta telah memenuhi prosedur
permohonan  visum  et  repertum  menurut  Waluyadi  (2005),
dimana  secara  garis  besar  permohonan  visum  et  repertum
harus memperhatikan bahwa permohonan harus
dilaksanakan  secara  tertulis,  dan  dilakukan  oleh pihak-pihak
yang diperkenankan untuk itu.
b. Pihak yang berhak meminta visum et repertum di Rumah Sakit
Panti Rapih Yogyakarta sudah sesuai dengan Waluyadi (2005)
yaitu penyidik.
c. Berdasarkan  ketentuan  pemaparan  informasi  rekam  medis
menurut Depkes RI (1997), permohonan pasien untuk
memperoleh  informasi  mengenai  dirinya  diserahkan  kepada
dokter yang merawat. Berdasarkan Permenkes RI
No.269/Menkes/Per/III/2008 pasal 12 ayat 1 dijelaskan
bahwa  pemaparan  isi  rekam  medis  hanya  boleh  dilakukan
oleh dokter yang merawat pasien dengan ijin tertulis pasien
d. Bentuk dan isi visum et repertum yang dikeluarkan oleh
Rumah   Sakit   Panti   Rapih   Yogyakarta   sudah   sesuai   dengan
Idris (1997) dimana secara garis besar pada visum et
repertum   harus   tercantum   �pro   justicia�,   data   sosial   pasien,
emuat  identitas  dokter  pemeriksa,  memuat  hasil  pemeriksaan,
kesimpulan, dan penutup  yang menyatakan  visum et  repertum
tersebut dibuat atas dasar sumpah dokter
2. Di   rumah   sakit   Panti   rapih   Yogyakarta   belum   melaksanakan
pembuatan visum et repertum sementara. Hal ini belum
sepenuhnya sesuai dengan pembagian visum et repertum hidup
menurut Soeparmono(2002).
84
Lama pembuatan visum et repertum di Panti Rapih
yogyakarta belum sepenuhnya sesuai dengan standar
operasional   prosedurnya,   sebagian   besar   visum   et   repertum
dibuat  lebih  dari  standar  yang  telah  ditetapkan.  Keterlambatan
banyak ditemui di permintaan visum et repertum yang
memerlukan  rawat  inap  dan  dikerjakan  oleh  dokter  umum/IGD
sedangkan  untuk  variasi  kasus  yang  paling  banyak  terlambat
adalah kasus penganiayaan dan kecelakaan lalu lintas.
Penyimpanan arsip copy visum et repertum belum
semuanya  disimpan  dalam  stockholder  ,  saat  penelitian  masih
banyak   dijumpai   konsep visum   et repertum yang   disimpan
bersama berkas rekam medis pasien.
3. Kendala � kendala yang dihadapi dalam pelaksanaan
pengeluaran   visum   et   repertum   hidup   di   Rumah   Sakit   Panti
Rapih Yogyakarta dibedakan menjadi dua macam yaitu Sumber
Daya   Manusia  seperti  ketidakterbacaan   tulisan   dokter,   jadwal
dokter  yang  tidak  setiap  hari  praktek  dan  juga  kelengkapan  isi
berkas  rekam  medis.  Selain  itu  kendala  dalam  hal  sarana  dan
prasarana  yaitu  belum  tersosialisasikannya  standar  operasional
prosedur kepada dokter pembuat visum et repertum.
B. SARAN
1. Sebaiknya dilakukan revisi terhadap standar prosedur
operasional  pembuatan  visum  et  repertum  yang  ada,  terutama
dilakukan penambahan item  pada standar  operasional prosedur
tersebut terkait   tentang   pelaksanaan kegiatan   penyimpanan
arsip   copy   visum et repertum dalam stockholder   dan
pengecekan kesesuaian isi berkas rekam medis dan isi visum et
repertum yang dilakukan oleh Kepala Instalasi Rekam Medis.
2. Sebaiknya   dilakukan   pengkajian   ulang   terhadap   standar
prosedur operasional pembuatan visum et repertum terkait
dengan lama waktu pembuatan visum et repertum.
85
3. Sebaiknya   dilakukan   sosialisasi   standar   operasional   prosedur
mengenai  lama  waktu  pembuatan  visum  et  repertum kepada
semua  pihak  yang  bertanggung  jawab  dalam  pembuatan  visum
et  repertum,  agar  masing  masing  pihak  mengetahui  dan  dapat
bekerja sesuai dengan standar operasional yang ada.
4. Sebaiknya  semua visum  et  repertum  yang  telah  selesai  dibuat
di  copy  untuk  kemudian  arsip  copy  nya  disimpan  menjadi  satu
dalam   stockholder agar   memudahkan pencarian apabila
diperlukan kembali.

