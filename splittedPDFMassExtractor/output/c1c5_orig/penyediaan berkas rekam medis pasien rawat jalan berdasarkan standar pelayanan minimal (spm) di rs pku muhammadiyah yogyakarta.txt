A. Latar Belakang
Rumah sakit adalah suatu organisasi yang melalui tenaga medis
profesional yang terorganisir serta sarana kedokteran yang permanen
menyelenggarakan pelayanan kedokteran, asuhan keperawatan yang
berkesinambungan, diagnosis serta pengobatan penyakit yang diderita oleh
pasien (Azwar,1996). Setiap sarana pelayanan kesehatan di rumah sakit wajib
membuat rekam medis yang dibuat oleh dokter dan tenaga kesehatan yang
terkait dengan pelayanan yang telah diberikan oleh dokter dan tenaga kesehatan
lainnya.
Menurut Permenkes RI No. 269/Menkes/Per/III/2008 tentang Rekam
Medis pada pasal 1, rekam medis adalah berkas yang berisikan catatan dan
dokumen tentang identitas pasien, pemeriksaan, pengobatan, tindakan dan
pelayanan lain yang telah diberikan kepada pasien. Setiap rumah sakit harus
membuat rekam medis baik itu rekam medis rawat jalan maupun rekam medis
rawat inap. Rekam medis juga berguna sebagai bukti tertulis atas tindakan-
tindakan pelayanan terhadap seseorang pasien, juga mampu melindungi
kepentingan hukum bagi pasien yang bersangkutan, rumah sakit maupun dokter
dan tenaga kesehatan lainnya, apabila dikemudian hari terjadi suatu hal yang
tidak diinginkan menyangkut rekam medis itu sendiri.
Di setiap sarana pelayanan kesehatan berkas rekam medis disimpan di
tempat penyimpanan berkas rekam medis (filing). Cara penyimpanan berkas
rekam medis di setiap sarana pelayanan kesehatan berbeda. Ada dua cara
�
�
�
2
penyimpanan yaitu secara sentralisasi dan desentralisasi. Cara penyimpanan
sentralisasi adalah penyimpanan berkas seorang pasien dalam satu kesatuan
baik catatan-catatan kunjungan poliklinik maupun catatan-catatan selama pasien
dirawat. Cara penyimpanan desentralisasi adalah penyimpanan berkas rekam
medis pasien rawat inap dan rawat jalan dipisah.
Mutu pelayanan kesehatan dapat dikatakan baik bila didukung oleh suatu
sistem pengolahan rekam medis dalam mendapatkan kembali berkas rekam
medis yang cepat dan tepat waktu sesuai dengan ketentuan yang telah
ditetapkan oleh rumah sakit (Sabarguna, 2004). Penyediaan berkas rekam medis
yang cepat pun merupakan salah satu faktor yang dapat mempengaruhi
kepuasan pasien. Semakin cepat pula pasien mendapatkan pelayanan
kesehatan di rumah sakit.
Menurut Sabarguna (2004), pelayanan yang cepat dan tepat merupakan
keinginan semua konsumen baik pemberi pelayanan maupun penerima
pelayanan. Kecepatan penyediaan berkas rekam medis ke klinik juga dapat
menjadi salah satu indikator dalam mengukur kepuasan. Semakin cepat rekam
medis sampai ke klinik maka semakin cepat pelayanan yang dapat diberikan
kepada pasien. Standar kecepatan pendistribusian rekam medis terhitung dari
pasien melakukan registrasi di pendaftaran.
Berdasarkan studi pendahuluan pada tanggal 24 Desember 2012 oleh
peneliti di RS PKU Muhammadiyah Yogyakarta dengan melakukan wawancara
dengan kepala petugas rekam medis tentang penyediaan berkas rekam medis
pasien rawat jalan. Peneliti memperoleh informasi bahwa pada pelaksanaan
penyediaan berkas rekam medis pasien rawat jalan sampai ke poliklinik masih
kurang maksimal, dimana pada proses penyediaan berkas rekam medis masih
�
�
�
3
mengalami keterlambatan. Keterlambatan penyediaan berkas rekam medis
tersebut berpengaruh terhadap  pelayanan medis kepada pasien. Semakin lama
penyediaan berkas rekam medis, maka semakin lama juga pelayanan medis
yang diberikan kepada pasien. Sementara itu RS PKU Muhammadiyah
Yogyakarta telah menetapkan standar pelayanan minimal 10 menit dalam
penyediaan berkas rekam medis sampai ke unit pelayanan. Kemudian penulis
juga melakukan pengamatan pada proses penyediaan berkas rekam medis.
Pengamatan penulis hanya terbatas pada pengamatan secara langsung tanpa
disertai dengan perhitungan waktu, maka penulis tertarik mengambil
permasalahan tersebut untuk meneliti tentang kecepatan penyediaan  berkas
rekam medis pasien rawat jalan di RS PKU Muhammadiyah Yogyakarta.
B. RUMUSAN MASALAH
Berdasarkan latar belakang di atas yang akan dibahas dalam penelitian
ini adalah  �bagaimana pelaksanaan penyediaan berkas rekam medis pasien
rawat jalan berdasarkan Standar Pelayanan Minimal (SPM) di RS  PKU
Muhammadiyah Yogyakarta?� .
C. TUJUAN
1. Tujuan Umum
Mengetahui pelaksanaan penyediaan berkas rekam medis pasien
rawat jalan berdasarkan Standar Pelayanan Minimal (SPM) di RS PKU
Muhammmadiyah Yogyakarta.
�
�
�
4
2. Tujuan Khusus
a. Mengetahui proses penyediaan berkas rekam medis pasien rawat jalan di
RS PKU Muhammadiyah Yogyakarta.
b. Mengetahui rata-rata kecepatan  penyediaan berkas rekam medis terkait
dengan Standar Pelayanan Minimal di RS PKU Muhammmadiyah
Yogyakarta.
c. Mengetahui faktor-faktor yang mempengaruhi kecepatan penyediaan
berkas rekam medis di RS PKU Muhammadiyah Yogyakarta.
D. MANFAAT
1.  Manfaat Praktis
a. Bagi Institusi
Sebagai alat evaluasi mengenai pengelolaan rekam medis di
institusi kesehatan. Selain itu, dapat menjadi bahan pertimbangan untuk
menyusun sebuah kebijakan dalam meningkatkan mutu pelayanan rekam
medis.
b. Bagi Peneliti
Sebagai aplikasi teori yang diperoleh dari bangku pendidikan dan
menambah pengetahuan, wawasan serta pengalaman yang berharga yang
dapat menjadi bekal untuk memasuki dunia kerja.
2.  Manfaat Teoritis
a. Bagi Institusi Pendidikan
Sebagai bahan bacaan untuk menambah wawasan bagi mahasiswa
khususnya dan masyarakat umumnya juga sebagai bahan referensi serta
�
�
�
5
sebagai bukti bahwa penulis telah menyelesaikan tugas akhir sebagai
syarat menyelesaikan pendidikan Program DIII Rekam Medis UGM.
b. Bagi Peneliti lain
Sebagai referensi bagi peneliti lain untuk perkembangan sesuai
dengan materi yang berhubungan dengan tema yang diambil.
E. Keaslian Penelitian
Penelitian yang berjudul �Penyediaan Berkas Rekam Medis Rawat Jalan
Berdasarkan Standar Pelayanan Minimal (SPM) di RS PKU Muhammadiyah
Yogyakarta � ini belum pernah dilakukan, namun ada beberapa penelitian yang
hampir sama, yaitu:
1. Emilia (2005) dengan judul � Faktor-Faktor Penyebab Lama Ditemukannya
Berkas Rekam Medis di RSJD Dr. Soejarwadi Klaten�.
Hasil : penyebab lama ditemukannya berkas rekam medis meliputi
beberapa faktor yaitu pasien tidak membawa kartu berobat, penyimpanan
KIUP yang tidak teratur, berkas rekam medis salah simpan (misfile), berkas
rekam medis masih berada di tempat pengolahan berkas dan sumber daya
manusia. Salah satu upaya untuk mengurangi lama ditemukannya berkas
rekam medis yaitu dengan cara memberi sosialisasi kepada pasien untuk
selalu membawa kartu berobat pada saat berobat ulang.
Persamaan: metode penelitian yang digunakan sama yaitu menggunakan
metode penelitian deskriptif dengan pendekatan kualitatif.
Perbedaan: penelitian Emilia (2005) bertujuan untuk mengetahui penyebab
lama ditemukannya berkas rekam medis pasien lama dan mengetahui
upaya untuk mengurangi lama ditemukannya berkas rekam medis pada
saat dibutuhkan di RSJD Dr. RM. Soejarwadi Klaten sedangkan pada
�
�
�
6
penelitian ini bertujuan untuk mengetahui proses penyediaan berkas rekam
medis,rata-rata kecepatan penyediaan berkas rekam medis dan
mengetahui faktor-faktor yang mempengaruhi kecepatan penyediaan
berkas rekam medis.
2. Kusnaedi (2002), yang berjudul � Faktor-Faktor yang Menyebabkan
Terlambatnya Berkas Rekam Medis Sampai ke Poliklinik dalam Menunjang
Pelayanan Kepada Pasien Rawat Jalan Khususnya Pada Pasien Lama RS
PKU Muhammadiyah Yogyakarta�.
Hasil: faktor-faktor yang menyebabkan berkas rekam medis terlambat
sampai ke poliklinik diantaranya adalah: berkas rekam medis masih di
bagian keuangan, dan peletakan berkas rekam medis yang salah, salah
penyebutan nomor rekam medis saat dimintakan oleh bagian pendaftaran
pasien.
Persamaan: persamaan dengan penelitian ini adalah tempatnya sama
yaitu di RS PKU Muhammadiyah Yogyakarta, selain itu jenis penelitian
yang digunakan sama yaitu sama-sama menggunakan jenis penelitian
deskriptif dengan pendekatan kualitatif.
Perbedaan: pada penelitian Kusnaedi ( 2002) tujuannya adalah untuk
mengetahui pelaksanaan dalam penyimpanan serta faktor-faktor yang
menjadi penyebab terlambatnya kelancaran pendistribusian berkas rekam
medis. Sedangkan pada penelitian ini bertujuan untuk mengetahui proses
penyediaan berkas rekam medis pasien rawat jalan,rata-rata kecepatan
penyediaan berkas rekam medis dan mengetahui faktor yang
mempengaruhi kecepatan penyediaan berkas rekam medis.
�
�
�
7
3. Muninggar (2006), yang berjudul � Kecepatan Retrieval Berkas Rekam
Medis Pasien Rawat Jalan di RSUD kota Yogyakarta�.
Hasil: kecepatan retrieval berkas rekam medis di Urusan Rekam Medis
RSUD Kota Yogyakarta masih dikatakan belum baik karena dari 180
berkas rekam medis yang dicari, 172 berkas rekam medis (sebanyak
95,56%) masih membutuhkan waktu pencarian lebih dari 1 menit dengan
rata- rata 11,07 menit. Faktor-faktor yang mempengaruhi kecepatan
retrieval berkas rekam medis pasien rawat jalan adalah sistem
penyimpanan yang digunakan masih straight numerical filing system ,
lokasi ruangan penyimpanan yang terpisah, keadaan rak yang tidak sesuai,
tracer sering menumpuk di bagian pencatat tracer  sebelum diberikan
kepada petugas filing dan kurang tenaga rekam medis khususnya di bagian
penyimpanan.
Persamaan : persamaan dengan penelitian ini adalah tujuannya sama
untuk menghitung kecepatan , jenis penelitian yang digunakan muninggar
(2006) dengan peneliti sama yaitu penelitian kualitatif dengan pendekatan
deskriptif.
Perbedaan: lokasi penelitian, kecepatan yang diamati Muninggar (2006)
adalah kecepatan retrieval di ruang filing sedangkan pada penelitian ini
peneliti mengamati kecepatan dalam penyediaan berkas rekam medis
pasien rawat jalan, dimulai dari pasien mendaftar sampai berkas ke
poliklinik.

1. Proses penyediaan berkas rekam medis rawat jalan di RS PKU
Muhammadiyah Yogyakarta dimulai dari pasien mendaftar, petugas
meng-entry data pasien, petugas mencetak print out (antrian, tracer),
petugas mencari berkas dan menurunkan berkasnya, petugas
mendistribusikan berkas rekam medis ke poliklinik.
2. Rata-rata waktu yang dibutuhkan dalam penyediaan berkas rekam
medis rawat jalan yaitu 14,52 menit. Waktu tercepat adalah 3 menit dan
waktu terlama adalah 55 menit.
3. Faktor-faktor yang dapat mempengaruhi kecepatan  penyediaan berkas
rekam medis  yaitu :
a. Machine (alat )
Proses penyediaan berkas rekam medis pasien rawat jalan, alat
yang digunakan adalah tracer, sedangkan untuk pendistribusian
masih manual diantar langsung oleh petugas pendaftaran.
Penggunaan tracer di RS PKU Muhammadiyah Yogyakarta  belum
maksimal.
b. Man ( manusia)
Sikap dan perilaku petugas masih belum disiplin karena petugas
kurang mematuhi aturan prosedur tetap yang ada.
54
55
c. Method (cara)
Di RS PKU Muhammadiyah Yogyakarta mempunyai aturan
mengenai penyediaan berkas rekam medis yaitu berupa pedoman
pengukuran sasaran mutu.
d. Environment (lingkungan)
Kondisi ruangan di pendaftaran sudah cukup memadai akan tetapi
untuk kondisi ruangan di filing terlalu sempit sehingga petugas
filing mengalami kesulitan ketika mengambil berkas rekam medis.
1. Sebaiknya petugas rekam medis khususnya dibagian penyimpanan
lebih teliti dalam proses penyediaan berkas rekam medis dan lebih
mematuhi aturan kebijakan yang ada.
2. Sebaiknya petugas filing lebih memaksimalkan penggunaan tracer .

