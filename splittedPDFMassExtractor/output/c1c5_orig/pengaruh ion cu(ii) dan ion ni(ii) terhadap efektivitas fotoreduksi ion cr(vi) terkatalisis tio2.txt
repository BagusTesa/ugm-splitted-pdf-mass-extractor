1.1 Latar Belakang
Perkembangan industri di Indonesia, termasuk di Yogyakarta, selain
membawa dampak positif juga menimbulkan dampak negatif, seperti terjadinya
peningkatan jumlah limbah yang dibuang ke lingkungan. Salah satu penghasil
limbah yang ada di Yogyakarta adalah industri elektroplating (pelapisan logam).
Limbah elektroplating biasanya mengandung berbagai ion logam seperti krom
(Cr), nikel (Ni), tembaga (Cu), dan seng (Zn). Diantara ion-ion logam tersebut
yang konsentrasinya relatif tinggi dalam limbah adalah ion Cr(VI) karena
merupakan logam pelapis yang utama.
Pembuangan limbah yang mengandung ion Cr(VI) ke lingkungan dapat
menyebabkan terjadinya pencemaran lingkungan perairan. Pencemaran
lingkungan perairan oleh Cr(VI) dapat membahayakan kesehatan manusia karena
spesies Cr(VI) sangat beracun yang dapat menimbulkan iritasi kulit, rasa mual,
kanker saluran pernapasan dan paru-paru (Roney dkk., 2002).
Mengingat bahaya yang ditimbulkan ion Cr(VI) maka batas kadar maksimum
yang diperbolehkan oleh pemerintah sangat rendah yaitu 0,1 mg/L (Anonim,
2004). Agar memenuhi syarat penetapan baku mutu lingkungan tersebut maka
limbah yang mengandung ion Cr(VI) harus diolah terlebih dahulu sebelum
dibuang ke lingkungan.
Berbagai metode untuk menghilangkan ion Cr(VI) dari lingkungan telah
banyak dipelajari, antara lain metode biologi, adsorpsi dan fotoreduksi. Metode
penghilangan Cr(VI) secara biologi telah dilakukan dengan menggunakan bakteri
atau biomassa (Mistry dkk., 2010; Liu dkk., 2007 dan Park dkk., 2005). Metode
ini cukup efektif karena konsentrasi ion Cr(VI) menurun cukup signifikan, namun
teknik ini relatif sulit dan mahal.
Metode lain yang telah digunakan untuk penghilangan ion Cr(VI) adalah
adsorpsi, yaitu dengan menggunakan adsorben karbon aktif (Mohamed dkk., 2012
dan Gupta dkk, 2011) dan zeolit (Silva dkk., 2008 dan Barakat, 2008).
2
Metode tersebut sederhana, murah dan efektif, namun tidak menghilangkan sifat
racun dari logam tersebut, melainkan hanya memindahkan ion logam dari larutan
ke padatan adsorben atau bahan penukar ion. Selain itu, jika adsorben telah jenuh
oleh ion logam berat maka akan menjadi limbah padat yang berbahaya karena
masih mengandung ion logam berat dengan konsentrasi yang relatif besar.
Selain metode bioadsorpsi dan adsorpsi yang lain telah dikembangkan untuk
mengatasi ion Cr(VI) adalah fotoreduksi (Rusmini, 2005). Metode ini merupakan
reduksi suatu ion dengan menggunakan cahaya dan serbuk TiO2 sebagai
fotokatalis. Reduksi ion logam dapat terjadi oleh adanya elektron yang dihasilkan
oleh air sebagai pelarut dan fotokatalis selama penyinaran dengan sinar UV.
Reduksi semacam ini juga disebut dengan fotoreduksi. Dalam proses fotoreduksi
ion Cr(VI) yang toksik akan tereduksi menjadi ion Cr(III) yang tidak toksik,
bahkan berguna bagi mamalia. Jadi dengan metode ini, ion Cr(VI) mengalami
detoksifikasi. Metode fotoreduksi terkatalisis TiO2 memiliki kelebihan karena
hanya memerlukan cahaya ultraviolet dan fotokatalis yang relatif murah serta
aman (Kanki dkk., 2004 dan Wang dkk., 2004).
Keberadaan ion Cr(VI) dalam limbah cair elektroplating biasanya bersama
dengan logam lain seperti ion Fe(III), Cu(II), Ni(II) dan Zn(II). Dalam proses
fotoreduksi keberadaan logam tersebut dimungkinkan dapat berpengaruh terhadap
fotoreduksi ion Cr(VI). Pengaruh ion logam terhadap fotoreduksi ion Cr(VI) telah
menarik perhatian dan diteliti beberapa peneliti.
Pengaruh ion Fe(III) terhadap ion Cr(VI) telah dipelajari oleh (Prasetya
2009). Hasil penelitian tersebut menunjukkan bahwa adanya ion Fe(III) dapat
meningkatkan efektivitas fotoreduksi ion Cr(VI). Dalam proses tersebut, ion Fe3+
tereduksi menjadi ion Fe2+ dan ion Fe2+ ini dengan cepat mengalami oksidasi.
Oksidasi oleh radikal OH yang ada dalam sistem larutan akan membentuk Fe(III)
dengan melepaskan elektron yang kemudian digunakan untuk mereduksi ion
Cr(VI). Oleh karena itu jumlah elektron yang tersedia dalam sistem reaksi terjaga
dan efektivitas fotoreduksi ion Cr(VI) meningkat. Penelitian tentang pengaruh
asam organik dan logam berat terhadap fotoreduksi ion Cr(VI) juga telah
dipelajari oleh Wahyuni dkk. (2011). Dari penelitian tersebut diperoleh
3
kesimpulan bahwa adanya asam oksalat dan malonat sebagaiman ion Fe(III) dan
Pb(II) dapat meningkatkan fotoreduksi ion Cr(VI) menjadi ion Cr(III) yang tidak
toksik. Asam organik dan ion logam berat tersebut berperan dalam meningkatkan
fotoreduksi ion Cr(VI) karena keduanya dapat bereaksi dengan radikal OH
sehingga dapat mencegah penggabungan kembali radikal OH dengan elektron.
Dengan demikian elektron dalam sistem tidak berkurang, sehingga reaksi
fotoreduksi berjalan lebih efektif. Namun sejauh ini pengaruh ion Cu(II) dan
Ni(II), yang ada bersama dengan ion Cr(VI), terhadap efektivitas ion Cr(VI)
belum pernah dilaporkan.
Sementara itu telah dilaporkan bahwa ion Cu(II) dapat mengalami fotoreduksi
menjadi ion Cu(0) (Day dan Underwood, 2002). Selain itu ion Cu(II) juga dapat
teradsorp di permukaan fotokatalis sehingga dapat mempengaruhi aktivitas TiO2.
Keadaan ini dimungkinan dapat mempengaruhi proses fotoreduksi ion Cr(VI)
menjadi ion Cr(III). Namun hal ini belum banyak dilaporkan. Untuk ion Ni(II)
yang tidak dapat mengalami reduksi, namun dapat teradsop pada permukaan
fotokatalis sehingga dapat mengganggu kinerja fotokatalis (Lin dan Rajeshwar,
1997). Namun pengaruh adanya ion Cu(II) dan Ni(II) belum banyak dilaporkan.
Hal ini mendorong dilakukannya penelitian ini untuk mempelajari pengaruh
adanya ion Cu(II) dan Ni(II) terhadap efektivitas fotoreduksi ion Cr(VI) dalam
limbah cair elektroplating terkatalisis TiO2.
1. 2 Tujuan Penelitian
Tujuan dari penelitian ini yaitu:
1. Mempelajari pengaruh konsentrasi ion Cu(II) terhadap efektivitas
fotoreduksi ion Cr(IV) dalam limbah buatan yang terkatalisis TiO2 .
2. Mempelajari pengaruh konsentrasi ion Ni(II) terhadap efektivitas
fotoreduksi ion Cr(IV) dalam limbah buatan yang terkatalisis TiO2.
3. Mempelajari fotoreduksi Cr(VI) dalam limbah cair elektroplating yang
mengandung Cu(II) dan Ni(II).
4. Mempelajari pengaruh fotoreduksi ion Cr(VI) terhadap kristalinitas
TiO2 sebelum dan setelah digunakan dalam proses fotoreduksi.
4
1. 3 Manfaat Penelitian
Hasil penelitian ini diharapkan dapat memberikan informasi tentang pengaruh
ion Cu(II) dan Ni(II) terhadap efektivitas fotoreduksi ion Cr(VI) terkatalisis TiO2
sebagai alternatif pengolahan limbah.
5
BAB II
TINJAUAN PUSTAKA
2.1 Kromium
2.1.1. Sifat-sifat kromium
Kromium merupakan salah satu logam transisi dengan simbol Cr, memiliki
nomor atom 24 dengan massa atom relatif sebesar 51,996. Logam kromium
berwarna putih, berkilau, dan rapuh dengan titik lebur tinggi sekitar 2000 °C
(Costa dan Klein, 2006).
Di dalam lingkungan perairan, kromium membentuk tiga jenis ion, yaitu
kromium(II), kromium(III) dan kromium(VI). Kromium dengan bilangan oksidasi
+3 dan +6 relatif stabil dibandingkan Cr(II), sehingga lebih banyak ditemukan di
perairan. Dalam air dengan pH asam, Cr(II) dan Cr(III) berbentuk kation yaitu
Cr2+ dan Cr3+ sedangkan Cr(VI) berbentuk anion antara lain CrO42- atau Cr2O72-.
Ion Cr2+ cepat teroksidasi menjadi Cr3+ yang lebih stabil dalam lingkungan
aerobik, dan mengendap sebagai Cr(OH)2 pada pH mendekati 6, sedangkan Cr3+
pada rentang pH 5,5-9 membentuk endapan Cr(OH)3.
Dalam lingkungan perairan, ion Cr(VI) berada dalam bentuk anion yang
spesiasinya sangat dipengaruhi oleh pH larutan (Gambar 2.1). Gambar 2.1
menunjukkan bahwa pada pH 1-8, spesies tersebut terdeprotonasi membentuk
spesies HCrO4- yang berada dalam kesetimbangan dengan ion dikromat Cr2O72-.
Jumlah spesies ini akan mengalami penurunan dan hilang pada pH 9-10, yang
disertai dengan munculnya spesies CrO42- yang jumlahnya semakin banyak
seiring dengan kenaikan pH larutan. Larutan dengan pH di atas 9, spesies Cr(VI)
yang ada adalah ion kromat (CrO42-).
6
Gambar 2.1.  Distribusi Cr(VI) pada berbagai fungsi pH (Palmer dan Puls, 1994)
Adapun reaksi kesetimbangan ketiga spesies Cr(VI) ditunjukkan oleh
persamaan 2.1 sampai 2.3 (Cotton dan Wilkinson, 1999) sebagai berikut:
HCrO4-   ?  CrO42-  + H+    K = 10-5,9 ..(2.1)
H2CrO4  ?  HCrO4-  + H+    K = 4,1 ..(2.2)
Cr2O72-  + H2O  ?  2HCrO4- K = 10-2,2 ..(2.3)
Jumlah kromium(VI) total dinyatakan sebagai :
Cr(VI)  total = H2CrO4  +  HCrO4
-  +  CrO4
2-  +  2x Cr2O7
2-
Ion dikromat (Cr2O72-) pada suasana asam dalam larutan berair merupakan
oksidator yang kuat atau mudah mengalami reduksi, sebagaimana ditunjukkan
oleh harga potensial reduksi yang besar yaitu +1,33 volt (Cotton dan Wilkinson,
1999). Reaksi setengah selnya dapat dituliskan seperti persamaan (2.4).
Cr2O72- (aq) + 14H+ (aq)  + 6e-   ?   2Cr3+(aq)  + 7H2O E0 = 1,33 V..(2.4)
Dalam larutan basa, ion Cr(VI) dapat mengalami reduksi menjadi Cr3+,
kemudian dengan cepat membentuk endapan Cr(OH)3 seperti tertulis pada reaksi
setengah sel pada persamaan (2.5).
CrO42- (aq) + 4H2O + 3e-   ?   Cr(OH)3 (s) + 5OH- (aq) E0 = 0,13 V..(2.5)

6.1 Kesimpulan
Berdasarkan hasil penelitian maka dapat diambil kesimpulan sebagai berikut:
1. Efektivitas fotoreduksi ion Cr(VI) dengan adanya ion Cu(II) mengalami
penurunan seiring dengan kenaikan konsentrasi ion Cu(II) karena terjadi
kompetisi penggunaan elektron dalam larutan maupun adsorpsi pada
permukaan TiO2.
2. Adanya ion Ni(II) tidak berpengaruh terhadap fotoreduksi ion Cr(VI)
namun dapat menyebabkan penurunan adsorpsi.
3. Proses penyinaran dengan adanya TiO2 terhadap limbah elektroplating
yang mengandung ion Cr(VI), ion Cu(II) dan ion Ni(II) dapat menurunkan
konsentrasi ketiga logam tersebut. Peningkatan hilangnya ketiga logam
terjadi seiring dengan kenaikan massa fotokatalis TiO2 sampai 30 mg
untuk ion Cr(VI) dan 15 mg untuk ion Cu(II). Dengan penambahan
fotokatalis yang lebih banyak menyebabkan penurunan ion logam yang
hilang.
4. Fotokatalis TiO2 setelah digunakan dalam fotoreduksi ion Cr(VI) baik
tanpa ataupun dengan adanya ion Cu(II) dan Ni(II) mengalami penurunan
kristalitinitas.
6.2 Saran
Berdasarkan hasil penelitian yang diperoleh, maka disarankan adanya
penelitian lanjutan tentang modifikasi fotokatalis TiO2 guna meningkatkan
kinerjanya sebagai fotokatalis untuk fotoreduksi ion logam berat dalam limbah
cair elektroplating.

