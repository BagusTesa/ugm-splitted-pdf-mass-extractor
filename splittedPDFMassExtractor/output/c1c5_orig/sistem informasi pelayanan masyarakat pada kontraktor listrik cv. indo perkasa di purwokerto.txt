1.1 Latar Belakang Masalah
Pada zaman modern ini sumber energi utama dalam kehidupan manusia
yang sangat fungsional adalah sumber energi listrik. Listrik merupakan sumber
energi yang sangat penting bagi manusia, banyak aktivitas manusia yang
menggunakan sumber energi tersebut dalam kehidupan sehari-hari dan tidak dapat
dipungkiri manusia bergantung pada keberadaannya.
Indonesia memiliki perusahaan besar dalam bidang penyedia layanan
listrik yaitu PT. PLN. Perusahaaan  listrik milik negara ini telah melakukan
banyak kontribusi besar dalam pemasokan kebutuhan listrik masyarakat.
Kontraktor listrik adalah mitra kerja PLN yang bekerjasama dalam bidang
kelistrikan. Kontraktor listrik memiliki peran untuk  melaksanakan pemasangan
instalasi dan pemanfaatan tenaga listrik. CV. Indo Perkasa merupakan salah satu
perusahaan  yang bergerak di bidang kontraktor listrik, perdagangan umum dan
supplier untuk keperluan industri maupun perorangan dan jasa pelayanan. CV.
Indo Perkasa ini berperan untuk membantu masyarakat serta memberikan
kenyamanan dan kesejahteraaan bagi masyarakat luas.
Sarana media sangat penting untuk membantu manusia terhubung dengan
yang lain untuk saling berkomunikasi ketika jarak jauh. Sarana media kini sudah
semakin berkembang, salah satunya adalah media internet. Setiap orang
memanfaatkan internet salah satunya untuk memberi kemudahan dalam
mengembangkan pekerjaan.
CV. Indo Perkasa di Purwokerto melayani pemasangan baru, perubahan
daya dan bongkar pasang. Pelayanan yang terdapat pada sistem lama adalah
pelanggan yang ingin memasang instalasi  listrik untuk pasang baru harus mencari
dan mendatangi langsung ke salah satu  kontraktor listrik yang dituju. Dalam hal
ini, kontraktor mempunyai kewenangan SMP (Saluran Masuk Pelayanan) yaitu
yang  mengatur pemasangan listrik yang berada di dalam rumah. Selain itu, proses
2
pengelolaan data pemesanan  yang  terdapat pada sistem lama masih
menggunakan sistem manual yaitu pencatatan pemesanan  masih ditulis di buku
besar, sehingga menyulitkan untuk mencari dan melihat data-data pemesanan
lama.
Sehubungan dengan hal tersebut, diperlukan sistem terkomputerisasi yang
dapat mempermudah pelayanan masyarakat yaitu Sistem Informasi Pelayanan
Masyarakat Pada Kontraktor Listrik CV. Indo Perkasa. Sistem ini  membangun
layanan pemesanan secara online. Diharapkan pada sistem ini  mampu
memberikan kemudahan bagi pelanggan serta membantu meningkatkan
pelayanan pelanggan menjadi lebih baik.
1.2 Perumusan Masalah
Perumusan dalam  tugas  akhir  ini adalah bagaimana membangun layanan
pemesanan berbasis web dan dapat melakukan  pengolahan data secara sistematis.
1.3 Batasan Masalah
Batasan masalah yang terdapat pada sistem informasi ini adalah:
1. Sistem Informasi Pelayanan Masyarakat Pada Kontraktor Listrik CV. Indo
Perkasa memberikan layanan pemesanan di wilayah Karasidenan
Banyumas yang mencakup daerah Purwokerto, Wangon, Ajibarang,
Banyumas,  Purbalingga, Banjarnegara dan Wonosobo.
2. Sistem ini menangani pemesanan layanan, pengelolaan daftar pemesanan,
pengelolaan pembayaran,  penanggung jawab, dan grafik dari jumlah
ketiga layanan yang sering dipesan.
3. Sistem ini setelah memesan layanan pasang baru dan bongkar pasang,
pembayaran dilakukan secara langsung di lokasi pelanggan.
4. Pembayaran  tambah daya dilakukan dengan mendatangi langsung ke
kantor CV. Indo Perkasa di Purwokerto. Jika pelanggan berasal dari luar
daerah  Purwokerto,  maka penanggung  jawab dari CV. Indo Perkasa pada
tiap kota yang mengurusi.
3
1.4 Tujuan Penelitian
Tujuan dalam penelitian dari sistem yang dibuat di CV. Indo Perkasa
Purwokerto ini adalah memberikan kemudahan bagi pelanggan dalam bidang
pemesanan  melalui  online dan  mempermudah dalam mengatur data pemesanan
layanan.
1.5 Manfaat Penelitian
Manfaat dari sistem informasi pelayanan masyarakat pada kontraktor listrik
CV.Indo Perkasa ini adalah :
1. Memudahkan dalam  melakukan  pemesanan pasang baru, perubahan
daya, dan bongkar pasang.
2. Memudahkan dalam mengatur  pengolaan data pelayanan.
3. Menampilkan persentase ketiga layanan yang sering dipesan dan
presentase jumlah pemesanan pasang baru pada tiap kota pelayanan yang
digambarkan dalam bentuk grafik.
1.6 Metode Penelitian
Metode-metode yang digunakan dalam penelitian ini adalah sebagai berikut :
1. Pengumpulan data
Ada beberapa metode-metode yang digunakan untuk mengumpulkan data
yaitu :
a. Metode Studi Literatur
Metode Studi Literatur ini dilakukan dengan mengumpulkan data-data dan
mempelajari informasi-informasi yang berhubungan dengan permasalahan
yang diangkat menjadi suatu penelitian.
b. Metode Wawancara
Melakukan suatu wawancara tanya jawab dengan pimpinan CV. Indo
Perkasa dan Staff  CV. Indo Perkasa. Wawancara dilakukan agar
mendapat informasi yang dibutuhkan mengenai permasalahan dan
kebutuhan sistem.
4
c. Metode Observasi
Metode ini dilakukan agar peneliti bisa mengetahui permasalahan yang
ada di lapangan secara langsung.
2. Analisis dan perancangan sistem
Analisis yang dilakukan dengan cara mengkaji proses-proses yang terjadi
pada kegiatan pengelolaan data, dan dilanjutkan perancangan sistem dari
perancangan basis data,  perancangan proses, dan perancangan antar muka.
3. Implementasi sistem
Tahapan ini merupakan pembangunan dari sistem berdasarkan rancangan
sistem yang telah dibuat menggunakan bahasa pemograman CI.
1.7 Sistematika Penulisan
Sistematika penulisan mencakup bab-bab yang akan dibahas dalam tugas
akhir ini. Berikut sistematika penulisan tugas akhir meliputi:
BAB I  :  PENDAHULUAN
Berisi tentang latar belakang masalah, perumusan masalah, batasan
masalah,  tujuan penelitian,  manfaat penelitian, metodologi penelitian,
dan sistematika penulisan.
BAB II : TINJAUAN PUSTAKA
Berisi tentang uraian sistematis tentang informasi hasil penelitian
sebelumnya yang disajikan dalam  pustaka dan menghubungkannya
dengan sistem informasi yang akan di buat.
BAB III : LANDASAN TEORI
Berisi konsep dan teori yang  mendukung dalam pembuatan sistem, seperti
pengetahuan tentang sistem informasi, pengembangan sistem, bahasa
pemograman, serta perangkat lunak basis data yang digunakan.
BAB IV : ANALISIS DAN PERANCANGAN SISTEM
Berisi analisis sistem, analisis pengguna sistem informasi, analisis
kebutuhan fungsional, perancangan sistem yang meliputi perancangan
proses, perancangan basis data dan perancangan antar muka.
Berisi rincian penerapan dari desain yang dibuat, kemudian menjadi
sebuah sistem informasi yang menampilkan antarmuka program disertai
penggunaan program tersebut.
Berisi mengenai kesimpulan dan saran dari sebuah penelitian tentang
Sistem Informasi Pelayanan Masyarakat Pada Kontraktor Listrik CV. Indo
Perkasa di Purwokerto.

6.1 Kesimpulan
Dari analisis, perancangan dan implementasi sistem pada Sistem Informasi
Pelayanan  Masyarakat Pada Kontraktor Listrik CV. Indo Perkasa di Purwokerto
dapat dihasilkan kesimpulan :
1. Dibangunnya sistem yang berbasis web ini dapat membantu dalam
menangani pemesanan pasang baru,  perubahan daya dan bongkar pasang.
2. Mampu digunakan dalam proses pengolahan data mulai dari data
pelanggan, data pemasangan dan pembayaran.
6.2 Saran
Saran untuk pengembangan agar sistem menjadi lebih bermanfaat bagi
pengguna antara lain:
1. Dapat dikembangkan tidak hanya pelayanan pasang baru, perubahan daya
dan bongkar pasang saja tetapi pelayanan seperti pemesanan perangkat alat
petir dan pemasangan LPJU juga di ikut sertakan.
2. Pembuatan sistem ini diharapkan kedepannya dapat dikembangkan tidak
hanya pada kontraktor listrik CV. Indo Perkasa saja, tetapi kontraktor
listrik lainnya.

