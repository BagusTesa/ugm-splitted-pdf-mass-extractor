1.1 Latar Belakang
Yogyakarta adalah salah satu daerah di Indonesia yang
mempunyai aset pariwisata yang banyak menarik perhatian para
wisatawan baik domestik maupun manca negara. Sebagai daerah tujuan
wisata, Yogyakarta memiliki keanekaragaman objek wisata mulai dari
wisata budaya, alam, candi, pantai dan sebagainya. Maka dari itu, banyak
orang yang datang dengan motivasi melakukan wisata keluarga selama
musim liburan sekolah, berkunjung untuk melakukan MICE (Meeting
Incentive Converence Exhibition), ada juga yang datang untuk keperluan
studytour, dan ada pula yang datang untuk menikmati libur akhir pekan.
Informasi mengenai objek wisata yang wisatawan ketahui saat ini,
hanya dapat dilihat dari internet atau dari promosi tentang objek tersebut
yang isinya cenderung variatif yaitu mengenai tempat wisata favorit,
kuliner, beserta penginapan sehingga tidak fokus dalam satu informasi
atau penjelasan. Dalam hal lain, kurangnya informasi mengenai tempat
lokasi objek wisata di Yogyakarta seperti tidak adanya papan petunjuk
arah ke objek tersebut juga tempat lokasi yang tidak terdeteksi oleh
aplikasi pencari lokasi adalah menjadi penyebab banyaknya tujuan wisata
tidak terkunjungi. Penentuan objek-objek wisata juga harus dapat
terencana dengan baik dan tepat untuk memperoleh objek wisata yang
diinginkan atau yang direkomendasikan dalam waktu tertentu.
Oleh karena itu, untuk memecahkan persoalan tersebut penulis
membuat aplikasi peta panduan berwisata berbasis desktop. Aplikasi
yang dibuat berbasis desktop karena pada umumnya hampir seluruh
masyarakat memiliki komputer atau laptop yang dapat menginstal dan
2
menggunakan aplikasi ini nantinya, sehingga penyampaian ke pengguna
juga lebih mudah misalnya disebarkan di media sosial seperti facebook
dan twitter, juga bisa diberikan langsung kepada wisatawan yang
menggunakan jasa sewa mobil atau melalui penginapan-penginapan di
Yogyakarta yang merupakan fasilitas tambahan dari penyedia jasa
tersebut. Berbeda halnya jika dibuat dengan basis sistem operasi android
atau lainnya, tidak semua orang menggunakan android bahkan ada yang
tidak menyukainya pun layar yang digunakan lebih kecil sehingga kurang
memuaskan dalam mendapatkan informasi dari aplikasi yang dibuat.
Aplikasi ini tidak pula dibuat berbasis web adalah karena tidak semua
pengakses selalu terkoneksi dengan internet. Aplikasi peta panduan
berwisata ini berisi informasi singkat tentang objek wisata, mengetahui
arah ke lokasi wisata dari pusat kota, dan memilih paket tujuan wisata
sehari dengan objek wisata pilihan.
Penerapan aplikasi ini diharapkan dapat membuat wisatawan
supaya dapat berwisata di Yogyakarta dan sekitarnya dengan efektif dan
efisien sehingga menyenangkan, memuaskan, dan juga memberikan
kesan yang mendalam.
Aplikasi pada sistem ini dibangun berbasis desktop dengan
menggunakan Adobe Flash CS3. Dengan Adobe Flash CS3 tersebut
diharapkan aplikasi yang dibangun lebih fast-learning (cepat dipelajari),
user-friendly (mudah digunakan), attractive (menarik).
1.2 Perumusan Masalah
Dari latar belakang yang diuraikan diatas, maka rumusan masalah
yang dibahas dalam laporan tugas akhir ini adalah bagaimana membuat
Aplikasi Panduan Berwisata di Yogyakarta berbasis multimedia
menggunakan Adobe Flash CS3.
3
1.3 Batasan Masalah
Untuk mengetahui sejauh mana pengembangan sistem informasi
yang dibahas dalam proposal tugas akhir ini, maka batasan masalahnya
antara lain :
1. Aplikasi panduan berwisata ini mengambil studi kasus di Yogyakarta
dan tempat wisata pilihan di sekitarnya.
2. Informasi yang akan disajikan dari aplikasi ini hanya dapat diakses
melalui perangkat berbasis desktop.
3. Aplikasi ini tidak menyediakan informasi mengenai transportasi
publik, penginapan, tempat makan, maupun even-even.
4. Aplikasi yang dikembangkan tidak dilengkapi dengan  fitur update.
5. Pengembangan aplikasi ini tidak memperhatikan lokasi
pemberangkatan wisatawan, hanya dari lokasi pemberangkatan
tertentu.
1.4 Tujuan Penelitian
Tujuan dari penelitian ini adalah membuat aplikasi panduan
berwisata yang mampu mempermudah  wisatawan untuk mengetahui
informasi objek wisata di Yogyakarta, menunjukkan arah jalan ke lokasi
objek wisata kepada wisatawan dan pengguna aplikasi ini serta
memberikan kemudahan dalam memilih tujuan objek-objek wisata dalam
sehari.
1.5 Manfaat Penelitian
Manfaat penelitian yang dapat diambil dari penelitian adalah,
sebagai berikut :
1. Memberikan informasi mengenai wisata yang ada di Yogyakarta dan
sekitarnya.
2. Memudahkan wisatawan untuk mendapatkan informasi lokasi wisata
di Yogyakarta dan sekitarnya.
4
3. Membantu wisatawan untuk memberikan pilihan paket tujuan wisata
berdasarkan waktu yang tersedia.
4. Membantu pemandu wisata dalam penyampaian informasi mengenai
jalan yang dilalui untuk menuju lokasi wisata kepada wisatawan.
5. Membantu Dinas Pariwisata untuk mempromosikan wisata kota
Yogyakarta dengan lebih menarik menggunakan media computer.
1.6 Metode Penelitian
Berikut merupakan beberapa metode yang digunakan dalam
penelitian ini, diantaranya :
1. Observasi
Metode ini digunakan untuk mengumpulkan data dan seluruh
informasi yang akan digunakan untuk membuat perencanaan sistem
informasi. Mengamati langsung dan memahami aktifitas wisatawan
dan juga pemandu wisata.
2. Studi Literatur
Metode ini digunakan dengan studi kepustakaan mengumpulkan dan
mempelajari informasi-informasi yang berkaitan dengan tema yang
dibahas.
3. Wawancara
Metode ini dilakukan dengan mewawancarai secara langsung
wisatawan dan pemandu wisata untuk mendapatkan informasi dan
data-data yang dibutuhkan dalam penelitian.
4. Pengembangan Sistem
Metode ini digunakan dalam pengembangan sistem berdasarkan
dengan konsep  Software Development Life Cycle (SDLC).
a. Analisis Sistem
Menganalisa masalah yang ada untuk merancang sebuah sistem.
5
b. Perancangan sistem
Merancang kebutuhan-kebutuhan suatu sistem berdasarkan
masalah yang ada.
c. Implementasi
Implementasi merupakan hasil dari perancangan sistem yang
dilakukan dengan cara membangun sistem melalui kode program
untuk menghasilkan suatu sistem informasi yang dibutuhkan.
1.7 Sistematika Penulisan
Sistematika penulisan laporan tugas akhir dibagi menjadi tujuh
bab sebagai berikut:
BAB I PENDAHULUAN
Menjelaskan latar belakang masalah, rumusan masalah,
batasan masalah, tujuan penelitian, manfaat penelitian,
metode penelitian serta sistematika penulisan.
BAB II TINJAUAN PUSTAKA
Berisi uraian sistematis tentang informasi hasil penelitian
terdahulu yang dilakukan oleh orang lain dan mempunyai
keterkaitan atau relevansi dengan analisis dan  rancangan
sistem yang kita buat.
BAB III LANDASAN TEORI
Berisi tentang dasar-dasar teori yang digunakan untuk
mendukung perancangan sistem.
BAB IV ANALISIS DAN PERANCANGAN SISTEM
Menjelaskan tentang analisis dan rancangan sistem yang
digunakan meliputi analisis sistem, rancangan proses,
rancangan basis data,  tampilan antar muka dan laporan.
BAB V IMPLEMENTASI SISTEM
Membahas tentang implementasi sistem yang dibuat mulai
dari rancangan sistem menjadi aplikasi.
Membahas kekurangan dan kelebihan sistem setelah
diimplementasikan.
Berisi penutup dari laporan tugas akhir yang berisi
kesimpulan dari Tugas Akhir serta saran untuk
pengembangan sistem selanjutnya.

7.1 Kesimpulan
Dari hasil pengembangan sistem dan penulisan tugas akhir ini,
maka dapat  diambil kesimpulan antara lain:
1. Aplikasi Panduan Wisata Yogyakarta Berbasis Multimedia dengan
menggunakan Adobe Flash CS3 telah berhasil dibangun sebagai
media panduan berwisata bagi wisatawan dan diharapkan dapat
mempermudah  wisatawan untuk mengetahui informasi objek wisata
di Yogyakarta.
2. Aplikasi yang dibangun ini memiliki tiga menu pilihan yaitu pilihan
objek wisata, petunjuk arah, dan paket wisata sebagai  panduan
dalam berwisata sehari.
3. Berdasarkan hasil kuisioner, ketertarikan dan fungsional aplikasi
terhadap aplikasi ini secara keseluruhan diperoleh rata-rata 87% dari
25 responden yang dengan ini berarti menunjukkan bahwa
responden menyatakan aplikasi ini layak digunakan sebagai panduan
berwisata khususnya di Yogyakarta.
7.2 Saran
Adapun saran-saran dalam aplikasi panduan wisata ini adalah
sebagai berikut:
1. Perlu adanya penambahan informasi detail objek wisata, petunjuk
arah, dan paket wisata yang lebih kompleks, lengkap, dan update.
2. Menambahkan video dan animasinya sehingga menjadi lebih
menarik.
3. Memperjelas suara dubbing dan suara lain sehingga pengguna dapat
dengan jelas memahami informasi.
4. Memperbayak foto-foto pada objek wisata sehingga pengguna dapat
lebih tertarik mengunjungi suatu objek wisata.
5. Membuat aplikasi yang berbasis desktop ini menjadi aplikasi yang
dapat dijalankan pada gadget mobile berbasis Android.
6. Meningkatkan penyebaran aplikasi ini atau mem-publish-kannya
melalui media sosial yang lebih banyak lagi.

