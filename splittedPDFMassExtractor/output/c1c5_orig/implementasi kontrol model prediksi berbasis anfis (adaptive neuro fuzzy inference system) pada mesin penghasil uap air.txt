1.1    Latar Belakang
Didalam implementasinya sistem kontrol proses memiliki banyak sekali
permasalahan, seperti terdapatnya sifat interaksi pada suatu proses multivariable
atau multiple-input multiple-output (MIMO), terjadinya kondisi long dead time,
terdapatnya batasan dari suatu aktuator pada plant proses, hingga permasalahan
mengenai optimasi dari suatu aksi kontrol terhadap proses yang terjadi. Pada
banyak kasus, aksi kontrol konvensional sudah dapat menyelesaikan
permasalahan-permasalahan yang ada pada suatu plant proses, seperti feedforward
control diaplikasikan untuk menangani permasalahan gangguan pengukuran,
override control diaplikasikan untuk menangani constraints dari suatu unit plant
proses dan decoupling control digunakan untuk permasalahan sifat interaksi
proses. Namun hal ini masih dinilai kurang, hal ini dikarenakan kemampuan
masing-masing aksi kontrol konvensional tersebut hanya dapat menyelesasikan
satu permasalahan dari setiap aksi kontrolnya. Bagaimanapun jika semua
permasalahan kontrol tersebut terjadi secara bersamaan, maka sangat diperlukan
pendekatan yang sistematis terhadap plant proses yang akan dikontrol.
Salah satu unit plant yang sering ditemui pada banyak industri energi
maupun industri kimia adalah mesin penghasil uap air, Gambar 1.1 menunjukan
bentuk mesin penghasil uap air secara umum. Mesin penghasil uap air merupakan
sebuah boiler yang digunakan untuk mengubah air menjadi uap air dengan
kualitas bervariasi pada suatu tekanan discharge tertentu. Pada dasarnya mesin
penghasil uap air menggunakan parameter masukan fisis berupa air, udara dan
bahan bakar, sedangkan parameter keluaran fisis yang dihasilkan berupa uap air.
2
Gambar 1.1 Mesin penghasil uap air
(http://www.directindustry.com/prod/pirobloc/steam-generators.html, 2014)
Parameter masukan dari mesin penghasil uap air perlu untuk dikendalikan
agar diperoleh parameter keluaran yang sesuai dengan keadaan yang diperlukan
ketika dipergunakan, sehingga mesin penghasil uap air dapat bekerja dengan
optimal. Parameter keluaran dari mesin penghasil uap air berupa dependent
variabel, maksudnya adalah besaran nilai yang dihasilkan merupakan hasil
pengaruh dari interaksi setiap parameter masukan mesin penghasil uap air, maka
diperlukan suatu cara untuk mendapatkan prilaku sistem dari mesin penghasil uap
air.
Buragohain dan Mahanta (2006) mengatakan bahwa pemodelan mampu
mengetahui prilaku sistem dari suatu plant, oleh karena itu dapat membantu dalam
membuat desain kontrol baru. Model dari suatu sistem dapat membantu dalam
mengidentifikasi parameter sistem, deteksi kesalahan dan optimasi. Salah satu
jenis kontroler yang menggunakan model dari plant sebagai unit penyusun
kontrolnya adalah kontrol model prediksi. Pada plant yang besar dan kompleks,
kontrol model prediksi tidak menjadi kontrol pengganti dari kontrol PID, namun
sebagai kontrol tambahan (Halvorsen, 2011).
Dalam memperoleh model dari suatu plant, metode yang sering digunakan
adalah metode pemodelan secara matematika, memperoleh sebuah model
matematika untuk sebuah sistem sangatlah komplek dan memakan banyak waktu,
3
karena pada penggunaannya sering membutuhkan linearization dan mengabaikan
bebearapa parameter sistem (Sivakumar dan Balu, 2010).
1.2    Rumusan Masalah
Rumusan masalah yang menjadi dasar dalam penelitian ini adalah
bagaimana mengimplementasikan kontrol model prediksi berbasis ANFIS
(Adaptive Neuro Fuzzy Inference System) pada mesin penghasil uap air?
1.3    Tujuan Penelitian
Tujuan yang ingin dicapai dalam penelitian ini adalah mengimplementasikan
kontrol model prediksi berbasis ANFIS (Adaptive Neuro Fuzzy Inference System)
pada mesin penghasil uap air.
1.4    Batasan Masalah
Dalam pembuatan dan uji coba sistem ini, diberikan beberapa batasan
masalah sebagai berikut:
1. Mesin penghasil uap air pada penelitian ini berupa mesin virtual penghasil
uap air.
2. Mesin virtual penghasil uap air yang digunakan pada penelitian ini
merupakan luaran yang telah dicapai pada penelitian sebelumnya.
3. Karakteristik mesin penghasil uap air yang digunakan dalam membuat
mesin virtual penghasil uap air berdasarkan steam generator 10, steam
stxation 1, PT. Chevron Pacific Indonesia, Duri.
4. Mesin virtual penghasil uap air bukan merupakan fokus permasalahan pada
penelitian ini, namun hanya sebagai objek untuk mengimplementasikan
kontrol model prediksi yang telah dibuat.
5. Parameter masukan mesin penghasil uap air yang terlibat adalah aliran air,
tekanan air, aliran udara, tekanan bahan bakar dan temperatur bahan bakar,
4
sedangkan untuk parameter keluaran yang terlibat adalah tekanan uap air,
kualitas uap air, aliran uap air dan O2 content.
6. Historical data yang didapat sebagai training data, merupakan bentuk
representasi dari hasil BRC.
7. Basic Regulatory Control (BRC) pada penelitian ini, telah
diimplementasikan pada mesin penghasil uap air sebenarnya, sehingga
bukan merupakan bahasan pada penelitian ini.
8. Implementasi kontrol model prediksi pada mesin penghasil uap air pada
penelitian ini direpresentasikan dengan menggunakan simulasi.
1.5    Manfaat Penelitian
Manfaat yang bisa diperoleh dari penelitian ini, yaitu:
1. Dapat menjadi kontrol tambahan yang mengatasi permasalahan kontrol
proses dalam menangani pengaturan nilai dependent variabel pada plant
suatu industri yang berjenis MIMO atau multivariable, sehingga penggunaan
plant dapat lebih optimal.
2. Memberi suatu alternatif lain dalam mendapatkan hasil pemodelan dari
plant, hasil pemodelan yang diperoleh akan dijadikan sebagai unit penyusun
kontrol model prediksi
1.6    Metode Penelitian
Metodologi penelitian yang digunakan dalam penyelesaian skripsi ini adalah
sebagai berikut:
1. Melakukan studi pendahuluan dengan cara mencari referensi umum
mengenai permasalahan pada sistem kontrol proses, setelah itu melakukan
diskusi singkat dengan dosen pembimbing dan ahli/pakar pada bidang
intelligence control system.
2. Studi pustaka dengan mencari referensi lanjutan yang sesuai dengan
perancangan, untuk menentukan rancangan yang akan dibuat untuk solusi
5
dari permasalahan, dengan mempelajari referansi tentang sistem kontrol,
kecerdasan buatan dan pemodelan sistem.
3. Membuat tampilan antarmuka, yang terdiri dari antarmuka pengambilan
historical data, antarmuka tombol eksekusi pembelajaran ANFIS (Adaptive
Neuro Fuzzy Inference System), antarmuka komunikasi serial, antarmuka
panel validasi data dan antarmuka analisa performance dari mesin virtual
penghasil uap air yang direpresentasikan dengan grafik surf dan plot.
4. Mengimplementasikan algoritma ANFIS (Adaptive Neuro Fuzzy Inference
System) pada tampilan antarmuka yang telah dibuat dan melakukan uji
validasi data hasil pemodelan dengan data sebenarnya.
5. Menginterkoneksikan antara mesin virtual penghasil uap air dan hasil
pemodelan mesin virtual penghasil uap air yang dibuat pada software
MATLAB 2011b dengan software LabVIEW 2011.
6. Membuat tampilan antarmuka pada software LabVIEW 2011 sebagai virtual
control room, sehingga penggabungan antara tampilan antarmuka yang
dibuat dengan software MATLAB 2011b menjadi sebuah sistem simulasi
mesin penghasil uap air.
7. Dokumentasi berupa penulisan laporan tugas akhir dilakukan sejak awal
penelitian. Hasil laporan tiap bab penyusun merupakan keluaran tertulis dari
tiap tahap penelitian. Tahap ini ditutup dengan presentasi sidang tugas akhir.
1.7    Sistematika Penulisan
Sistematika penulisan yang digunakan dalam penyususnan tugas akhir ini
adalah sebagai berikut:
BAB I: PENDAHULUAN
Berisi pemaparan tentang latar belakang penelitian, rumusan masalah, batasan
masalah, tujuan penelitian, manfaat penelitian, metodologi penelitian serta
sistematika penulisan.
6
BAB II: TINJAUAN PUSTAKA
Tinjauan pustaka memuat uraian sistematis tentang informasi hasil penelitian yang
disajikan dalam pustaka dan menghubungkannya dengan masalah penelitian yang
sedang diteliti.
BAB III: DASAR TEORI
Pada bagian ini berisi penjelasan secara teori mengenai metode dan algoritma
yang digunakan pada sistem.
BAB IV: PERANCANGAN SISTEM
Bab ini berisi penjelasan tentang perancangan sistem simulasi yang dibuat,
meliputi deskripsi sistem secara keseluruhan, perancangan perangkat keras dan
perancangan perangkat lunak.
BAB V: IMPLEMENTASI SISTEM
Bab ini berisi tentang penjelasan mengenai implementasi kontrol model prediksi
berbasis ANFIS (Adaptive Neuro Fuzzy Inference System) yang telah dirancang
pada bab sebelumnya.
BAB VI: HASIL PENELITIAN DAN PEMBAHASAN
Bab ini berisi mengenai hasil pengujian sistem simulasi kontrol model prediksi
berbasis ANFIS (Adaptive Neuro Fuzzy Inference System) yang dibuat oleh
peneliti.
BAB VII: PENUTUP
Bab ini berisi kesimpulan dari peneliti dan saran-saran pengembangan penelitian
selanjutnya.
7
BAB II
TINJAUAN PUSTAKA
Bahar dkk. (2006) mengatakan ANN dan ANFIS estimator telah berhasil
dalam memperkirakan komposisi top product dan bottom product dalam kolom
destilasi multi-komponen. Dalam kasus perubahan tingkat reflux, ANFIS lebih
baik dari ANN untuk estimasi koposisi product. ANFIS estimator memiliki
beberapa keunggulan dibandingan ANN estimator. Dalam ANFIS tidak perlu
untuk normalisasi pada pelatihan dan nilai-nilai sebelumnya untuk variabel state
(komposisi) tidak digunakan. Dalam pengolahan data estimator, poin yang
terpenting adalah kumpulan data yang dapat diandalkan/dipercaya dalam rentang
operasi.
Buragohain dan Mahanta (2006) mengatakan ukuran dari kumpulan data
masukan-keluaran sangat penting ketika ketersediaan data sangat sedikit dan
untuk menghasilkan kumpulan data yang banyak diperlukan biaya sangat mahal.
Dalam keadaan seperti itu, optimasi dalam jumlah data yang digunakan untuk
pembelajaran adalah perhatian utama. Penerapan teknik V-Fold pada pemodelan
sistem menggunakan ANFIS dapat digunakan untuk pelatihan data dengan jumlah
pasangan data yang minimal. Sehingga jumlah data yang diperlukan untuk
melakukan pembelajaran dengan menggunakan ANFIS dapat dikurangi secara
signifikan menjadi seperdelapan dari kebutuhan untuk ANFIS konvensional.
Adnan dkk. (2010) mengatakan bahwa estimasi dan validasi model
dilakukan dengan menggunakan data eksperimen yang dikumpulkan dari pilot
plant. Model yang dikembangkan telah divalidasi menggunakan kesesuaian
dengan kriteria terbaik terhadap data terukur dari pilot plant. Sehingga didapatkan
hasil bahwa MISO (Multi Input Single Output) ANFIS yang dikembangkan
mampu memodelkan nonlienaer plant melalui pasangan masukan-keluaran yang
diperoleh dari percoban plant. Gambar 2.1 menunjukan struktur MISO.
8
Gambar 2.1 Struktur MISO (Multi Input Single Output) (Adnan dkk, 2010)
Zhang dkk. (2010) mengatakan manfaat mengkombinasikan ANFIS dengan
metode multiple model adaptive control adalah untuk memastikan stabilitas
sistem dengan loop tertutup dan sekaligus meningkatkan kinerja dari sistem loop
tertutup. Selain itu, metode yang diusulkan dapat mengurangi asumsi global
boundedness dari istilah nonlinear orde yang paling tinggi dengan demikian
meningkatkan kepraktisan dari metode. Selanjutnya ANFIS mengatasi
ketidakpastian jaringan saraf dan menghemat waktu pelatihan dan dengan
demikian menghindari kemungkinan bahwa jaringan menjadi terjebak pada
minimum lokal.
Ekaputri dan Rohman (2012) mengatakan penggunaan kontrol model
prediksi algoritma-3 dapat diaplikasikan pada sistem inverted pendulum model
505 dan diimplementasikan pada mikrokontroler AVR Atmega 32. Algoritma-3
menghitung masukan sistem kontrol optimal berdasarkan prediksi dari system
behavior. Algoritma-3 juga menyelesaikan masalah constraint pada aktuator.
Sistem dilakukan dengan memberikan ganguan batang pendulum pada titik stabil,
maka batang pendulum akan kembali ke titik stabil, Untuk sistem ini nilai horizon
ditetapkan pada 9 berdasarkan waktu respon dalam simulasi. Algoritma kontrol
model prediksi dapat diimplemetasikan pada prosesor yang lebih komplek untuk
komputasi yang komplek.
Kurniawan (2012) mengatakan komponen penting yang dibutuhkan untuk
melakukan analisa kinerja mesin penghasil uap air adalah melakukan pemodelan
dari mesin penghasil uap air dan membuat graphical user interface sebagai
9
komponen penunjang dalam melakukan analisa. Pemodelan dilakukan dengan
mengunakan ANFIS. Didalam analisa kinerja mesin penghasil uap air, parameter
yang memiliki peranan penting adalah uap air. ANFIS memiliki kinerja lebih baik,
jika membership function dari masukan semakin banyak, namun memiliki
kelemahan dari segi komputasi yang sangat berat.
Pada Tabel 2.1 diperlihatkan perbedaan penelitian sebelumnya dan
penelitian yang dilakukan oleh penulis.
Tabel 2.1 Matriks perbandingan dengan penelitian sebelumnya
Judul Peneliti Metode yang dipakai
Design of State
Estimators for The
Inferential Control of an
Industrial Distillation
Column
Bahar dkk.(2006)
Penggunaan artificial
neural network dan
adaptive neuro fuzzy
inference system sebagai
estimator pada distillation
column.
ANFIS Modeling of
Nonlinear system Based
on V-fold Technique
Buragohain dkk.(2006)
Melakukan pemodelan
menggunakan ANFIS
dengan teknik validasi
hasil pelatihan dengan
menggunakan V-fold.
ANFIS Identification
Model of an Advanced
Process Control (APC)
Pilot Plant
Adnan dkk.(2010)
Penggunaan ANFIS
sebagai sistem identifikasi
model pada pilot plant.
An Adaptive Generalized
Predictve Control
Method for Non-linear
Systems Based on ANFIS
and Multiple Models
Zhang dkk.(2010)
Mengkombinasikan
ANFIS dengan metode
multiple model adaptive
control dalam melakukan
switching model dari plant.
Implementation Model
Predictive Control
(MPC) Algorithm-3 for
Inverted Pendulum
Ekaputri dan Rohman
(2012)
Penggunaan algoritma-3
untuk menghitung
masukan sistem kontrol
yang optimal berbasis
kontrol prediksi dari
tingkah laku sistem
Performance Analysis of
Steam Generator Using
Artificial Intelligence Kurniawan (2012)
Membuat mesin virtual
penghasil uap air dengam
menggunakan ANFIS
untuk analisa kinerja mesin
penghasil uap air.
10
Tabel 2.1 Lanjutan
Judul Peneliti Metode yang dipakai
Implementation of Model
Predictive Control Based
On Adaptive Neuro Fuzzy
Inference System
(ANFIS) On Steam
Generator Kurniawan (2013)
Melakukan pemodelan
mesin virtual penghasil
uap air dengan membuat
dua buah struktur
pemetaan model data
ANFIS yang berbeda, yang
kedua model tersebut
dijadikan unsur penyusun
dari kontrol model prediksi
pada mesin virtual
penghasil uap air.
11
BAB III
DASAR TEORI
3.1    Mesin Penghasil Uap Air
Mesin penghasil uap air adalah sebuah perangkat yang digunakan untuk
menghasilkan uap air dengan menerapkan energi panas ke air. Meskipun definisi
tersebut terlihat fleksibel, dapat dikatakan bahwa mesin penghasil uap air yang
lama secara umum diistilahkan katel atau boiler yang bekerja pada tekanan rendah
hingga tekanan yang sedang. Berbagai macam tipe mesin penghasil uap air dapat
dikombinasikan dengan sebuah unit mesin yang berbeda. Sebuah boiler
menggabungkan sebuah firebox atau tungku perapian/pembakaran didalam urutan
dalam pembakaran bahan bakar dan menghasilkan panas. Panas yang dihasilkan
ditransferkan ke air untuk membuat uap. Mesin penghasil uap air akan
menghasilkan uap jenuh sesuai dengan tekanan diatas air mendidih. Komponen-
komponen dari mesin penghasil uap air secara umum adalah sebagi berikut
(PT.Chevron Pacific Indonesia):
1. Feed water system
Feed water system adalah jaringan pipa serta perlengkapannya untuk
mengalirkan air dan uap panas pada mesin penghasil uap air. Feed water
system terdiri dari:
a) Feed water pump
b) Feed water preheater
2. Heater
Heater merupakan komponen dari mesin penghasil uap air yang di
dalamnya terpasang pipa-pipa yang saling berhubungan menjadi satu jalur
lintasan dan berfungsi sebagai tempat perpindahan energi secara konveksi
yang berasal dari proses pembakaran pada burner pipa yang berisi air untuk
diubah atau dikonversikan menjadi uap.
3. Burner
Burner adalah bagian dari mesin penghasil uap air yang berfungsi
mensuplai panas ke pipa-pipa dalam heater.

7.1    Kesimpulan
Berdasarkan penelitian yang telah dilakukan, dapat diambil kesimpulan
sebagai berikut:
- Telah diimplementasikan Adaptive Neuro Fuzzy Inference System
(ANFIS) sebagai unit peyusun model dari kontrol model prediksi pada
mesin virtual penghasil uap air.
- Didapatkan parameter terbaik untuk masing-masing ANFIS pada setiap
struktur model yang diimplementasikan pada mesin virtual penghasil
uap air, dengan parameter sebagai berikut:
a. Data pelatihan dalam melakukan pembelajaran ANFIS menggunakan
historical data ke-4 yang berjumlah 800 data.
b. Rasio presentase pembelajaran untuk training data dan checking data
pada masing-masing ANFIS untuk setiap struktur model secara
berurutan sebesar 90% dan 10%, kecuali rasio presentase untuk
ANFIS pada parameter aliran air secara berurutan sebesar 80% dan
20%.
- Hasil validasi RMSE (Root Mean Square Error) dengan melakukan
pengujian terhadap 100 data, didapatkan nilai sebagai berikut: aliran
air=1.9941, tekanan air=48.0236, aliran udara=604.0621, tekanan bahan
bakar=0.7087, temperature bahan bakar=18.6594, O2 content=0.9591,
tekanan uap air=76.1557, kualitas uap air=3.9734 dan aliran uap
air=264.9173.
7.2    Saran
Dibutuhkan historical data yang lebih banyak dengan variasi yang lebih
variatif untuk masing-masing ANFIS pada setiap struktur model, sehingga tingkat
validasi ANFIS akan semakin tinggi, yang akan berpengaruh terhadap
kemampuan dari kontrol model prediksi.

