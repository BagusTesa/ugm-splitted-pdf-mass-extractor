1.1. Latar Belakang
Teknologi saat ini berkembang sangat cepat dan pesat. Hal tersebut juga
memberikan dampak positif di berbagai bidang. Terlihat dari perubahan di
kehidupan manusia yang semakin hari semakin berkembang. Perubahan tersebut
membuat manusia terus berkembang dalam bidang teknologi, khususnya di dunia
komputer. Karena hampir semua aspek kini membutuhkan teknologi komputer,
seperti aspek pendidikan, bisnis, ekonomi dan sebagainya.
Lestari Wedding Organizer adalah perusahaan yang menangani berbagai
macam acara pernikahan, mulai dari catering, gedung, riasan sampai dekorasi
yang dibutuhkan dalam pernikahan tersebut. Pemesanan semua produk di Lestari
Wedding Organizer masih dilakukan dengan cara yang manual. Calon pemakai
jasa Lestari Wedding Organizer harus datang ke kantor langsung untuk
melakukan pemesanan. Selain itu, penyebaran informasi-informasi terkait dengan
produk-produk yang dimiliki oleh Lestari Wedding Organizer masih dilakukan
melalui orang ke orang, leaflet dan poster. Tidak semua media publikasi seperti
diatas mampu mengefektifitaskan penyebaran informasi kepada semua orang.
Dengan adanya internet, kini semua orang cenderung mencari informasi melalui
media online dibanding membaca informasi memalui media cetak
2
Melihat permasalah yang ada, maka diperlukan suatu sistem informasi
berbasis web yang memberikan segala informasi mengenai produk-produk yang
ditawarkan dari Lestari Wedding Organizer. Selain itu, sistem informasi ini juga
menyediakan fasilitas pemesanan online sehingga mempermudah calon pengguna
jasa untuk melakukan pemesanan. Pembuatan sistem ini diharapkan diharapkan
dapat mengoptimalkan pelayanan terhadap calon pemakai jasa Lestari Wedding
Organizer.
1.2. Perumusan masalah
Sesuai dengan latar belakang diatas, permasalah yang terjadi adalah
bagaimana membangun sistem informasi yang mampu digunakan untuk proses
pemesanan online. Selain itu sistem yang mampu memberikan informasi
mengenai produk-produk yang ditawarkan di Lestari Wedding Organizer.
1.3. Batasan Masalah
Mengingat cakupan sistem informasi lestari wedding organizer sangat luas
dan kompleks, maka perlu batasan-batasan masalah sebagai berikut :
a. Sistem informasi ini berbasis web menggunakanakan DBMS My
SQL.
b. Sistem ini tidak menangani masalah pembayaran.
c. Sistem ini tidak menangani masalah managemen pemesanan.
d. Keamanan data dan hal-hal yang berhubungan dengan jaringan
tidak dibahas dalam penelitian ini.
3
1.4. Tujuan
Sistem ini dibuat dengan tujuan untuk menganalisis, merancang, dan
mengimplementasikan sistem informasi yang berbasis web untuk
mengefektifitaskan dan meningkatkan pelayanan pemesanan  dan memberikan
informasi mengenai produk-produk yang ditawarkan di wedding organizer.
1.5. Manfaat
Manfaat dari sistem yang akan dibuat ini adalah:
1. Membantu pengelola Lestari Wedding organizer dalam pengelolaan
pemesanan wedding organizer.
2. Membantu konsumen dalam melihat dan melakukan pemesanan
produk-produk di Lestari Wedding Organizer secara online.
3. Mengoptimalkan penggunaan komputer dalam proses pengelolaan data.
1.6. Metodologi Penelitian
Berikut metodologi penelitian yang digunakan dalam penelitian ini:
a. Studi Literatur
Studi literature dilakukan dengan pengumpulan data dari buku-buku
referensi dan literature tugas akhir yang relevan yang sesuai dengan yang
dibutuhkan di system informasi  Lestari Wedding organizer.
4
b. Wawancara
Metodologi ini dilakukan dengan cara wawancara langsung dengan pihak
Lestari Wedding Organizer. Wawancara ini bertujuan untukmendapatkan
data-data yang dibutuhkan dalam sistem informasi yang akan dibuat.
c. Pengembangan Sistem
Metodologi ini hanya meliputi tahap-tahap berikut:
a. Analisis Sistem
Menganalisis sistem informasi yang akan dibuat agar sesuai dengan
apa yang diinginkan oleh cutomer.
b. Perancangan Sistem
Perancangan sistem informasi dilakukan sesuai dengan hasil analisis
yang didapat yang kemudian dibuat dalam Entity Relation Diagram
(ERD) dan Unified Model Language (UML).
c. Desain Sistem
Mendesain system informasi ini agar terlihat lebih menarik dan user
friendly.
d. Implementasi
Implementasi sistem informasi ini dilakukan setelah proses
perancangan sudah dilaksanakan. Hasil perancangan system informasi
diimplementasi melalui kode program yang menghasilkan suatu
system informasi.
5
1.7. Sistematika Penulisan
Untuk memberikan gambaran mengenai seluruh yang dibahas dalam tugas
akhir ini, maka sistematika penulisan dibagi ke dalam enam bab, dengan
spesifikasi sebagai berikut:
BAB I PENDAHULUAN
Bab satu menguraikan tentang latar belakang, rumusan masalah, batasan
penelitian, tujuan penelitian, manfaat penelitian, metodologi penelitian,
dan sistematika penulisan.
BAB II TINJAUAN PUSTAKA
Bab dua menjelaskan tentang sistem dan tugas akhir ataupun skripsi yang
pernah dibuat dengan tujuan yang sama yang telah dibuat sebelumnya.
BAB III LANDASAN TEORI
Bab tiga berisi konsep dasar dari sistem informasi, basis data, MySQL dan
teori-teori pendukung lain yang digunakan untuk mendukung tugas akhir
ini.
BAB IV ANALISIS DAN PERANCANGAN SISTEM
Bab empat berisi tentang analisis dan perancangan sistem informasi yang
dibuat, meliputi  analisis sistem, rancangan proses, rancangan dan
rancangan antar muka
Bab lima berisi tentang rincian penerapan dari desain menjadi system
infromasi dengan menampilkan antar muka.
Bab enam berisi tentang penutup dan kesimpulan tugas akhir yang memuat
kesimpulan dari uraian sebelumnya serta saran untuk mengembang
pengembang system selanjutnya.

6.1. Kesimpulan
Berdasarkan  penulisan Sistem Informasi Lestari Wedding Organizer dapat
disimpulkan bahwa:
1. Sistem informasi Lestari Wedding Organizer telah selesai dibangun dan
telah bisa digunakan untuk melakukan proses pemesanan secara online.
2. Sistem informasi Lestari Wedding Organizer telah bisa digunakan sebagai
media penyebaran informasi secara online oleh Lestari Wedding
Organizer.
6.2. Saran
Untuk melakukan pengembangan Sistem Informasi Lestari Wedding
Organizer, penulis mempunyai beberapa saran dianataranya adalah sebagai
berikut:
1. Optimalisasi untuk tampilan agar lebih user friendly, agar lebih
memudahkan dalam penggunaannya.
2. pembuatan format laporan dibuat lebih bervariasi dan dilengkapi dengan
grafik perkembangan pada periode tertentu.
3. Sebaiknya menambah proses pembayaran dengan menggunakan sistem,
seperti paypal.

