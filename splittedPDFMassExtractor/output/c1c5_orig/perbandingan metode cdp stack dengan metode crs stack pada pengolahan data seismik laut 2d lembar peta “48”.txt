I.1. Latar Belakang
Pemetaan struktur bawah laut merupakan hal yang sangat penting terutama
bagi negara Indonesia yang merupakan salah satu negara kepulauan dengan luas
wilayah perairannya hampir 2/3 dari total wilayah. Pemetaan ini sangat penting
baik dalam hal perbatasan negara maupun untuk memetakan potensi dan sumber
dayanya. Dalam pemetaan struktur bawah laut ini digunakan beberapa metode
geofisika seperti metode bathymetri, metode geomagnetik, metode gaya berat,
metode seismik laut, dan metode-metode lainnya. Diantara metode-metode
tersebut, metode yang paling banyak digunakan adalah metode seismik laut.
Dimana hasil akhir dari metode ini berupa penampang struktur bawah laut.
Metode seismik merupakan metode yang memanfaatkan penjalaran
gelombang seismik yang dialirkan kedalam bumi. Gelombang tersebut merambat
pada medium batuan yang bersifat elastik dengan kata lain partikel batuan sebagai
medium rambat gelombang tanpa disertai perpindahan partikel medium
rambatnya. Gelombang akan memantul pada lapisan bumi yang memiliki nilai
kepadatan atau densitas batuan yang berbeda. Nilai perbedaan densitas batuan
dalam subsurface bumi akan membentuk suatu koefisien refleksi atau koefisien
pantulan. Koefisien refleksi digambarkan sebagai nilai amplitudo yang dihasilkan
dari perbandingan antara nilai-nilai impedansi akustik (kemampuan medium
merambatkan gelombang akustik) dari setiap medium. Nilai amplitudo memiliki
energi yang kadangkala mengalami penyerapan atau absorbsi. Penyerapan
gelombang terjadi akibat penjalaran ke dalam subsurface bumi, sehingga hal ini
menjadi kendala utama dalam hal keterjagaan amplitudo.
Pengolahan data seismik adalah imaging awal subsurface bumi. Secara
konvensional tahap ini meliputi koreksi statis, noise reduction dan resolution.
Ketiga tahap tersebut dilakukan untuk menggambarkan secara langsung dengan
memberikan analisis kecepatan. Hal ini dilakukan sebagai faktor koreksi akibat
perbedaan waktu tempuh antara sumber seismik (source) ke penerima (receiver).
Koreksi ini menempatan sumber seismik dan penerima gelombang seismik
2
terletak pada posisi yang sama yang disebut sebagai koreksi Normal Moveout
(NMO). Koreksi tersebut menempatkan sumber seismik dan penerima seismik
pada titik pantul sama disebut dengan Common Depth Point (CDP). Koreksi
NMO dipengaruhi oleh pembuatan model kecepatan makro. Model kecepatan
diperoleh dari beberapa metode salah satunya dengan analisis semblance. Metode
ini diperoleh dari analisis koherensi (Taner dan Koehler,1969 dalam Mann,2003).
Analisis koherensi merupakan nilai kesamaan trace seismik dalam satu CDP
untuk dilakukan koreksi analisis kecepatan. Model kecepatan hasil analisis
kecepatan dilakukan secara manual memungkinkan terjadinya kesalahan dalam
picking kecepatan, untuk itu diperlukan metode alternatif yaitu dengan Common
Reflection Surface Stack.
Common Reflection Surface merupakan metode yang relatif baru dalam
metode stacking pengolahan data seismik. Pada Common Reflection Surface
diharapkan akan terdapat perbaikan nilai signal to noise ratio (rasio S/N) menjadi
lebih baik, karena metode ini menggunakan gather seismik yang berupa CDP
secara keseluruhan/ multicoverage data. Proses ini juga tidak menggantungkan
model kecepatan secara makro namun mensubtitusikan atribut sudut datang
gelombang pantul dipermukaan (?), radius kelengkungan gelombang normal
incident point (RNIP) dan radius kelengkungan gelombang normal (RN) pada
Common Depth Point (CDP).
Penelitian dilakukan pada daerah dengan nomor lembar peta �48�. Pada
daerah ini terdapat dua cekungan dimana pada bagian utara terdapat Cekungan
Buton dan pada bagian tengah daerah penelitian terdapat Cekungan Tukang Besi.
Pada pengolahan data, hanya digunakan 2 lintasan dari total 22 lintasan yaitu
lintasan �AKB� dan �JKT�. Pengolahan data menggunakan dua metode yaitu
metode CDP stack dan CRS stack yang kemudian hasilnya akan dibandingkan
untuk melihat keunggulan dan kekurangan masing-masing metode.
3
I.2. Rumusan Masalah
CRS stack diyakini dapat meningkatkan resolusi penampang seismik hasil
dari pengolahan data. Bidang pantul-bidang pantul pada penampang seismik hasil
dari metode CRS stack akan terlihat lebih jelas dibandingkan dari hasil metode
CDP stack.
I.3. Batasan Masalah
Penelitian ini membatasi masalah pada hal-hal berikut:
? Pengolahan data hanya sampai proses stack.
? Aperature pada CRS stack tidak diatur dan hanya menggunakan aperature
yang sudah diatur secara otomatis oleh software ProMAX R_5000.
I.4. Maksud dan Tujuan
Tujuan dari penulisan tugas akhir ini adalah:
? Melakukan pengolahan data seismik laut menggunakan metode CRS stack
dan CDP stack dengan menggunakan software ProMAX R_5000 dan
kemudian hasilnya dibandingkan.
? Mencari kelebihan dan kekurangan metode CRS stack dan metode CDP
stack.
I.5. Waktu dan Lokasi Penelitian
Penelitian dilakukan dalam dua tahap yaitu akuisisi data dan pengolahan
data. Akuisisi data dilakukan selama 20 hari dimulai dari tanggal 30 Mei 2012
hingga tanggal 19 Juni 2012. penelitian dilakukan di atas kapal Geomarine 3 milik
Pusat Penelitian dan Pengembangan Geologi Kelautan (PPPGL). Kemudian
pengolahan data dilakukan di kantor PPPGL mulai dari tanggal 25 Juni 2013
hingga 26 Juli 2013. Lintasan yang digunakan dalam pengolahan data berjumlah
dua lintasan yang selanjutnya diberi nama �AKB� dan �JKT�. Lintasan seismik
pada daerah penelitian ditunjukkan pada gambar I.1. Arah lintasan �AKB� adalah
selatan-utara sedangkan arah lintasan �JKT� adalah utara-selatan.
Gambar I.1. Gambar lintasan survey. Lintasan �AKB� memiliki arah selatan-utara
dengan panjang lintasan sekitar 210 km . Lintasan �JKT� memiliki arah
utara-selatan dengan panjang lintasan sekitar 200 km.

VI.1. Kesimpulan
? Terjadi peningkatan resolusi pada metode CRS stack sehingga gambar
penampang hasil CRS stack memiliki bidang pantul yang terlihat lebih jelas
dibandingkan hasil dari CDP stack.
? CRS stack memiliki kelebihan dimana smearing tidak berpengaruh pada
metode ini. Hal ini dikarenakan parameter CRS (?, RN, dan RNIP)
memperhatikan kemiringan bidang pantul.
? Kekurangan metode CRS stack dari penelitian ini adalah sebelum
melakukan CRS stack, kita tetap harus melakukan CDP stack. Sehingga
proses akan menjadi semakin lama. Hal ini mungkin disebabkan karena
operator automatic CDP stack belum tersedia pada software.
VI.2. Saran
? Untuk penelitian lebih lanjut, bisa dilakukan dengan menggunakan software
yang sudah memiliki operator automatic CDP stack.
? Penelitian lebih lanjut bisa dilakuakan sampai proses migrasi.
? Bisa dilakukan penelitian lebih lanjut tentang pengaruh aperture terhadap
hasil CRS stack.

