1.1 Latar Belakang
Pada dasarnya mobile device dibuat dengan tujuan hanya untuk
komunikasi suara dan pengiriman pesan. Namun berbeda dengan sekarang, ada
beberapa aspek yang membuat komunikasi suara dan pengiriman pesan  hanya
menjadi bagian kecil. (Kumar dkk., 2009). Setiap individu ingin mendapatkan
lebih banyak informasi dari sebuah mobile device, karena informasi menjadikan
seseorang menginginkan suatu hal, misalkan informasi tentang iklan. Semakin
berkembangnya zaman, semakin banyak pula cara untuk mengiklankan sebuah
produk, mulai dari brosur, pamflet, spanduk hingga baris iklan di internet. Namun
iklan-iklan tersebut dirasa tidak dinamis karena proses pencarian berlangsung
lama.
Yogyakarta adalah salah satu kota besar yang sering didatangi banyak
wisatawan, baik lokal maupun mancanegara. Banyak wisatawan yang
membutuhkan produk, barang, ataupun tempat layanan umum di Yogyakarta,
namun belum mengetahui tempatnya dan harus mendapatkannya dimana. Dalam
era teknologi yang sangat pesat, selain menjadi teknologi pemetaan, GIS
(Geographic Information System) telah menjadi kebutuhan informasi, dan
merupakan suatu hal yang tidak tabu lagi. LBS (Location Based Service) adalah
layanan yang menyediakan informasi berdasarkan tempat, mengacu pada GIS atau
electronic map yang ditunjukkan oleh garis lintang dan bujur sehingga
mendapatkan titik lokasi yang akurat. (Kumar dkk., 2009).
Pengembangan ilmu LBS dapat dikelompokkan menjadi beberapa kategori,
LBA (Location Based Advertising) adalah salah satunya. (Murat dan Ferraro,
2011). LBA juga memiliki beberapa metode yaitu metode push dan pull. Dengan
menggunakan metode tersebut informasi dapat diperoleh secara otomatis dan
dinamis berdasarkan tempat dan dengan waktu yang singkat di Kota Yogyakarta.
Aspek inilah yang menggeser fungsi asli sebuah hanphone sebagai alat
komunikasi suara dan pesan.
2
Salah satu mobile device yang mendukung aspek di atas adalah mobile
device yang  menggunakan Sistem Operasi Android karena didukung dengan
adanya perangkat GPS (Global Positioning System). Terdapat dua hal penting
yang berhubungan dengan GPS yaitu Location Manager atau API (Application
Programming Interface) Maps  dan Location Providers atau API Location
(Rompas dkk., 2010). Dengan Sistem Operasi Android, maka dapat digunakan
untuk menjalankan aplikasi berbasis lokasi pada sistem LBA.
Alasan aplikasi LBA ini dibangun pada sistem operasi Android mengacu
pada lembaga riset pasar Nielsen, menyebutkan bahwa Android saat ini telah
memimpin pasar mobile dengan penguasaan 39%. Angka itu berada di atas Apple
iOS dengan penguasaan pasar 28%. Sedangkan sisanya yaitu 20% untuk
Blackberry OS, 9% Windows Mobile, 4% OS yang lainnya. Riset lainnya yaitu
MarketingCharts.com menyebutkan 56% pengguna Android menggunakannya
untuk men-download berbagai aplikasi, 19% untuk mengirim pesan dan e-mail,
15% untuk telefon, 9% untuk browsing, sisanya 1% untuk penggunaan kamera.
Berdasarkan riset-riset yang telah disebutkan, dapat disimpulkan bahwa pengguna
mobile device akan meningkat, sehingga apabila aplikasi LBA ini dapat dibangun
dan diharapkan dapat dipakai oleh pengguna yang berada di Kota Yogyakarta
khususnya pada pengguna yang menggunakan mobile phone dengan sistem
operasi Android.
3
1.2 Perumusan Masalah
Perumusan masalah berdasarkan latar belakang permasalahan adalah sebagai
berikut :
1. Bagaimana membuat aplikasi LBA untuk mempermudah mendapatkan
informasi barang atau produk dalam  bentuk iklan secara cepat dan
dinamis dengan teknologi LBS.
2. Bagaimana cara memberikan informasi secara otomatis dan dinamis
berdasarkan tempat dan dengan waktu yang singkat dengan metode push
dan pull.
3. Bagaimana membuat aplikasi LBA untuk mempermudah mendapatkan
informasi tempat layanan umum dengan teknologi LBS.
4. Bagaimana membuat aplikasi LBA pada sistem operasi Android.
1.3 Batasan Masalah
Batasan penulisan tugas akhir adalah sebagai berikut :
1. Member yang ingin mengiklankan produk mendaftar diluar aplikasi.
2. Notifikasi pemberitahuan saat push iklan berjalan saat pengguna masuk
pada halaman depan aplikasi.
3. Pada layanan push iklan, lokasi pusat perbelanjaan akan diprioritaskan
yang paling dekat dengan pengguna.
4. Lokasi layanan sistem hanya berlaku pada wilayah Yogyakarta.
1.4 Tujuan Penelitian
Penelitian ini bertujuan untuk membangun sebuah aplikasi Android mobile
phone berupa Location Based Advertising. Aplikasi ini nantinya dapat
memberikan informasi periklanan secara dinamis berdasarkan jarak pengguna
dengan pusat perbelanjaan dan dapat melakukan pencarian lokasi layanan umum
di Kota Yogyakarta memanfaatkan teknologi Location Based Service. Letak titik
lokasi periklanan di konversi dalam bentuk latitude dan longitude dan ditampilkan
pada peta dengan memanfaatkan google map api. Titik � titik lokasi akan tampil
secara otomatis ketika pangguna masuk ke dalam area mendekati lokasi tertentu.
4
1.5 Manfaat Penelitian
Manfaat dari penulisan tugas akhir aplikasi location based advertising ini
terbagi menjadi dua, yaitu bagi pengguna aplikasi dan bagi mahasiswa (penulis).
1.5.1 Manfaat bagi pengguna aplikasi
1. Dapat dengan cepat mendapatkan dan mencari informasi tentang
periklanan di Yogyakarta.
2. Dapat mencari tempat umum di sekitar Yogyakarta.
1.5.2 Manfaat bagi penulis
1. Memanfaatkan kemajuan teknologi, khususnya dalam hal pengembangan
aplikasi perangkat bergerak sehingga dapat memudahkan aktivitas
menusia.
2. Mendapatkan pengalaman membuat aplikasi perangkat bergerak dengan
melakukan penelitian secara langsung.
1.6 Metodologi Penelitian
Penelitian ini dilakukan dengan tahapan � tahapan sebagai berikut :
1. Studi Literatur
Studi Literatur dilakukan dengan mengkaji dan mempelajari paper, e-book
dan situs web yang terkait dengan periklanan, teknologi LBS khususnya
LBA, google map api documents dan phonegap api document.
2. Analisis
Pada tahap analisis dilakukan analisis kebutuhan sistem dalam
membangun aplikasi location based advertising pada platform Android.
3. Desain
Metode ini merupakan perancangan sistem, dilakukan dengan
menggambarkan bagaimana sistem akan dibangun dan persiapan untuk
merancang sistem itu sendiri. Dalam mendesain aplikasi akan
menggunakan UML (Unified Modeling Language) yang didalamnya
terdapat use case, class diagram dan activity diagram.
4. Pengkodean
5
Merupakan tahapan merubah racangan sistem menjadi kode program
computer.
5. Pengujian
Merupakan tahap terakhir, yaitu menguji program yang sudah jadi apakah
sudah berjalan dengan baik ataukah masih memerlukan perbaikan lagi.
Tahap pengujian berupa :
a. Pengujian aplikasi pada perangkat bergerak.
b. Pengujian push dan pull iklan.
c. Pengujian pencarian tempat umum.
d. Pengujian rute navigasi.
1.7 Sistematika Penulisan
Sistematika penulisan yang digunakan dalam penyusunan tugas akhir ini
adalah sebagai berikut :
BAB I PENDAHULUAN
Bab ini menjelaskan tentang latar belakang, perumusan masalah, batasan masalah,
tujuan penelitian, manfaat penelitian, yang digunakan sebagai referensi dalam
penelitian tugas akhir, metode penelitian yang akan digunakan dan sistematikan
penulisan tugas akhir.
BAB II TINJAUAN PUSTAKA
Bab ini menguraikan tentang informasi beberapa hasil penelitian sudah dilakukan
sebelumnya. Kemudian informasi tersebut dijadikan sebagai bahan pembanding
dalam pembuatan aplikasi yang sedang diteliti saat ini.
BAB III DASAR TEORI
Berisi tentang materi � materi, teori ataupun pendapat dari sumber lain yang dapat
digunakan sebagai acuan dalam penelitian. Dasar teori ini harus dapat
dipertanggungjawabkan secara ilmiah dengan menyertakan sumber � sumber
referensi yang sudah diakui kebenarannya.
6
BAB IV ANALISIS DAN PERANCANGAN SISTEM
Bab ini menjelaskan tentang analisis dan desain sistem, didalamnya terdapat
perancangan analisis sistem, perancangan proses dan perancangan antarmuka
pengguna sistem.
BAB V IMPLEMENTASI
Bab ini membahas tentang pengimplementasian sistem sesuai dengan
perancangan sistem yang dilakukan sebelumnya.
Membahasa tentang bagaimana aplikasi dapat berjalan pada platform Android
serta sistem location based advertising dapat berjalan sesuai dengan yang
diharapkan.
Bab penutup berisi tentang kesimpulan hasil penelitian yang diperoleh sesuai
dengan tujuan penelitian. Selain itu berisi pula saran sebagai acuan untuk
penelitian berikutnya agar dapat menghasilkan aplikasi yang lebih baik.

7.1 Kesimpulan
Berdasarkan hasil perancangan, implementasi dan pengujian terhadap
sistem yang dibangun dapat diambil kesimpulan sebagai berikut :
1. Telah berhasil dibangun sebuah aplikasi location based advertising dan
layanan pencarian lokasi fasilitas umum Kota Yogyakarta pada sistem
operasi Android.
2. Aplikasi dapat memberikan feedback informasi berdasarkan inputan
jarak dan inputan string (nama barang, nama tempat dan kategori).
3. Aplikasi menggunakan metode push not requested (layanan yang
diberikan kepada pengguna sampai pengguna meminta untuk tidak
diberi layanan) untuk sistem location based advertising dan metode pull
pada layanan pencarian lokasi umum.
4. Notifikasi iklan dapat diberikan ketika pengguna masuk ke dalam radius
kurang dari 500 meter dari lokasi pusat perbelanjaan.
7.2 Saran
Saran yang dapat diberikan untuk penelitian selanjutnya adalah sebagai
berikut :
1. Notifikasi iklan muncul tidak hanya pada saat membuka aplikasi, namun
diharapkan muncul pada home mobile phone pada pengembangan
penelitian selanjutnya.
2. Dapat dikembangkan pada sistem operasi mobile yang lain seperti
windows phone dan iPhone.
3. Sistem menggunakan data real dan diunggah ke market Android agar
dapat digunakan oleh masyarakat luas.

