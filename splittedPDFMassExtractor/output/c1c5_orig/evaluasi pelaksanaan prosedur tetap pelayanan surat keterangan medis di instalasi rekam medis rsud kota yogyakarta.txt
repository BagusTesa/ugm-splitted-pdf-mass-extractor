A. Latar Belakang
Rumah sakit sebagai salah satu fasilitas pelayanan kesehatan adalah
bagian dari sumber daya kesehatan yang sangat diperlukan dalam
mendukung penyelenggaraan upaya kesehatan. Penyelenggaraan pelayanan
kesehatan di rumah sakit mempunyai karaketristik dan organisasi yang
kompleks, sehingga dalam memberikan pelayanan kesehatan harus
didukung oleh kinerja yang baik dari setiap petugas rumah sakit. Kinerja yang
baik tersebut akan berdampak terhadap pelayanan yang diberikan.
Pelayanan rumah sakit yang memenuhi standar merupakan harapan
bagi setiap institusi rumah sakit. Standar adalah keadaan ideal atau tingkat
pencapaian tertinggi dan sempurna yang dipergunakan sebagai batasan
penerimaan minimal (Azwar, 1996). Dalam pelayanan kesehatan, standar
sangat membantu petugas untuk mencapai pelayanan yang berkualitas.
Standar pelayanan adalah pedoman yang harus diikuti dalam
menyelenggarakan kegiatan pelayanan kesehatan antara lain, standar
prosedur operasional, standar pelayanan medis, dan standar asuhan
keperawatan (Komisi Akreditasi Rumah Sakit, 2001). Untuk mendapatkan
mutu pelayanan harus dipahami suatu standar pelayanan. Adanya standar
operating procedure (SOP) / prosedur tetap (Protap), seperti SOP pelayanan
kesehatan di dalam gedung puskesmas, SOP posyandu, dan sebagainya
dalam analisis lingkungan memiiki kekuatan atau strength (Sutisna, 2011).
Prosedur tetap rekam medis terdiri dari 4 (empat) dasar yang harus
ada di masing-masing unit rekam medis yaitu persyaratan di tiap-tiap unit,
2
sarana, prasarana, dan prosedur (Depkes RI, 1997). Dalam melakukan
pekerjaannya, unit rekam medis membutuhkan prosedur tetap/ Standar
Operating Procedure (SOP) sebagai acuan atau petunjuk dalam melakukan
pekerjaan. Prosedur tetap rekam medis disusun agar petugas dapat menjaga
konsistensi dan tingkat kinerja serta memperjelas alur tugas, wewenang dan
tanggung jawab dari petugas terkait. Prosedur tetap harus dibuat dengan
dasar pertimbangan yang jelas dan menggunakan bahasa yang mudah
dipahami serta disesuaikan dengan keadaan di pelayanan kesehatan
sehingga dalam melaksanakan tugasnya setiap petugas tidak menjadi ragu-
ragu. Dalam jangka panjang prosedur tetap dapat dijadikan sebagai langkah
perbaikan kinerja pelayanan dan kinerja organisasi berdasarkan konsep
manajemen kinerja.
Evaluasi standar operasional prosedur (prosedur tetap) adalah
sebuah analisis yang sistematis terhadap serangkaian proses pelaksanaan
dan aktifitas yang telah dibakukan dalam bentuk standar operasional
prosedur dari sebuah organisasi dalam rangka menentukan efektifitas tugas
dan fungsi organisasi secara keseluruhan. Evaluasi dapat dilakukan dengan
mengacu pada penyempurnaan prosedur yang telah ditetapkan atau
berkaitan dengan penerapannya.  Tujuannya adalah untuk melihat kembali
tingkat keakuratan dan ketepatan SOP yang sudah disusundengan proses
penyelenggaraan tugas dan fungsi organisasi sehinggaorganisasi dapat
berjalan secara efisien dan efektif (Lampiran Peraturan Menteri Negara
Pendayagunaan Aparatur Negara, 2008).
Surat keterangan medis menurut IDI (1994) adalah surat-surat
keterangan yang dikeluarkan berdasarkan kesimpulan dari hasil
3
pemerikasaan seorang dokter tentang keadaan tubuh dan jiwa manusia.
Kesimpulan-kesimpulan tersebut mempunyai akibat sosial tertentu bagi orang
yang diperiksa, karena biasanya juga menyangkut kepentingan pihak ketiga.
Surat keterangan medis menurut Amir (1997) adalah surat yang
dibuat atas dasar keterangan dari seorang dokter tentang keadaan atau
penyakit seorang pasien yang sebenarnya, yang biasanya akan digunakan
untuk kepentingan-kepentingan pasien. Jenis-jenis keterangan medis adalah
keterangan medis untuk kepentingan peradilan (Visum et Repertum),
keterangan medis untuk kepentingan non peradilan (referensi/rujukan;
konsultasi; expertise), bidang asuransi (misal keterangan dokter), bidang
administrasi (keterangan sakit, keterangan kematian, kuitansi, dll).
Berdasarkan studi pendahuluan pada tanggal 16 Januari 2013 di
Instalasi Rekam Medis RSUD Kota Yogyakarta, melalui wawancara dengan
salah satu petugas rekam medis, diketahui bahwa di Instalasi Rekam Medis
RSUD Kota Yogyakarta sedang melakukan kegiatan evaluasi terhadap
pelaksanaan prosedur tetap pelayanan surat keterangan medis, khususnya
untuk surat keterangan medis non pengadilan. Kegiatan evaluasi dilakukan
karena prosedur tetap pelayanan surat keterangan medis berkaitan dengan
masalah hukum sehingga penting dan perlu dipertanggungjawabkan. Selain
itu, ditemukan bahwa prosedur tetap tersebut belum lengkap karena belum
dicantumkannya standar waktu pembuatan surat keterangan medis dan
gambar alur kegiatan pelayanan surat keterangan medis. Kelengkapan
prosedur merupakan bagian standar pelayanan. Berdasarkan Undang-
Undang No.44 Tahun 2009 tentang Rumah Sakit, jika standar pelayanan
rumah sakit dapat tercapai maka akan meningkatkan mutu pelayanan dan
4
akan berpengaruh terhadap pencapaian nilai akreditasi rumah sakit. Dengan
adanya hal tersebut, maka peneliti tertarik untuk mengetahui bagaimana
pelaksanaan prosedur tetap pelayanan surat keterangan medis di Instalasi
Rekam Medis RSUD Kota Yogyakarta.
Prosedur tetap merupakan pedoman yang dapat memberikan acuan
bagi pola kerja yang terarah dan pada pelatihan tenaga akan berperan
penting, sehingga pelatihan akan terarah dan petugas perlu mencoba, agar
pada saatnya sudah mahir (Sabarguna, 2005). Alur kegiatan pada prosedur
tetap ini sangat penting untuk dijadikan acuan bagi petugas rekam medis
untuk melaksanakan kegiatan pelayanan surat keterangan medis.
Menurut Lampiran Peraturan Menteri Negara Pendayagunaan
Aparatur Negara (2008), pelaksanaan penerapan SOP harus secara terus
menerus dipantau sehingga proses penerapannya dapat berjalan dengan
baik. Masukan-masukan dalam setiap upaya monitoring akan menjadi bahan
yang berharga dalam evaluasi sehingga penyempurnaan-penyempurnaan
terhadap SOP dapat dilakukan secara cepat sesuai kebutuhan. Oleh karena
itu, peneliti lebih memfokuskan pada evaluasi proses, pemanfaatan, dan
ketepatan waktu pelaksanaan prosedur tetap pelayanan surat keterangan
medis.
B. Rumusan Masalah
Berdasarkan latar belakang masalah di atas, maka rumusan masalah
dalam penelitian ini adalah pelaksanaan prosedur tetap
pelayanan surat keterangan medis di Instalasi Rekam Medis RSUD Kota
Yogyakarta? .
5
C. Tujuan Penelitian
1. Tujuan Umum
Mengevaluasi pelaksanaan prosedur tetap pelayanan surat
keterangan medis di Instalasi Rekam Medis RSUD Kota Yogyakarta.
2. Tujuan Khusus
1. Mengevaluasi proses, pemanfaatan, dan ketepatan waktu
pelaksanaan pelayanan surat keterangan medis di Instalasi Rekam
Medis RSUD Kota Yogyakarta.
2. Membuat usulan draft prosedur tetap pelayanan surat keterangan
medis.
3. Manfaat Penelitian
1. Manfaat Praktis
a. Bagi Rumah Sakit
Hasil dari penelitian ini dapat digunakan sebagai bahan evaluasi dan
pertimbangan untuk pengambilan keputusan selanjutnya bagi rumah
sakit terkait pelaksanaan prosedur tetap pelayanan surat keterangan
medis di Instalasi Rekam Medis RSUD Kota Yogyakarta.
b. Bagi Peneliti
Hasil dari penelitian ini dapat menambah ilmu pengetahuan dan
wawasan secara langsung di rumah sakit dengan menerapkan teori
yang diperoleh dari institusi pendidikan.
2. Manfaat Teoritis
a. Bagi Institusi Pendidikan
Dapat menjadi bahan referensi untuk membandingkan teori yang ada
dengan praktek di lapangan.
6
b. Bagi Peneliti Lain
Dapat digunakan sebagai acuan atau referensi dalam pengembangan
penelitian selanjutnya yang memiliki bahasan penelitian yang serupa
dengan penelitian ini.
4. Keaslian Penelitian
Penelitian tentang Evaluasi Proses, Pemanfaatan, dan Ketepatan
Waktu Pelaksanaan Prosedur Tetap Pelayanan Surat Keterangan Medis di
Instalasi Rekam Medis RSUD Kota Yogyakarta
orang lain. Namun penelitian yang serupa pernah dilakukan, antara lain :
1. Dara Hastarini (2012 nyusunan Prosedur
Tetap Pengisian Berkas Rekam Medis di RSU Mitra Paramedika
Yo
Tujuan penelitian ini adalah membantu proses penyusunan
prosedur tetap tata cara pengisian berkas rekam medis serta dapat
memberikan masukan pada pihak rumah sakit mengenai tata cara
pengisian berkas rekam medis. Jenis penelitian yang digunakan adalah
deskriptif kualitatif dengan rancangan cross sectional. Hasil penelitian
adalah RSU Mitra Paramedika Yogyakarta belum memiliki prosedur tetap
pengisian berkas rekam medis dan akibatnya pegawai kurang baik dalam
melakukan pekerjaan karena tidak adanya acuan sehinggga berkas
rekam medis sering terisi kurang lengkap.
Persamaan penelitian adalah sama-sama menggunakan jenis
penelitian deskriptif kualitatif dengan rancangan cross sectional dan cara
memperoleh data dengan menggunakan metode observasi, wawancara,
dan studi dokumentasi serta pokok bahasan yang sama yaitu prosedur
7
tetap. Perbedaan penelitian adalah penelitian Hastarini (2012) menyusun
prosedur tetap pengisian berkas rekam medis, sedangkan penelitian ini
bertujuan untuk mengevaluasi proses, pemanfaatan dan ketepatan waktu
pelaksanaan pelayanan surat keterangan medis, serta membuat usulan
draft prosedur tetap pelayanan surat keterangan medis.
2.
Penyimpanan Berkas Rekam Medis Rumah Sakit Santa Elisabeth
Penelitian ini bertujuan untuk mengetahui uraian pekerjaan
kegiatan penyimpanan di unit rekam medis Rumah Sakit Santa Elisabeth
Bantul. Hasil penelitian yaitu isi prosedur tetap penyimpanan berkas
rekam medis di Rumah Sakit Santa Elisabeth Bantul belum sesuai
dengan uraian pekerjaan pada bagian penyimpanan karena ada prosedur
yang tidak sesuai. Prosedur yang tidak sesuai adalah menyortir berkas
rekam medis menurut kelompok umur, seharusnya prosedur tersebut
dicantumkan di prosedur penyusutan agar uraian pekerjaan jelas.
Persamaan penelitian adalah sama-sama menggunakan metode
penelitian deskriptif kualitatif dengan rancangan cross sectional dan cara
memperoleh data dengan menggunakan metode observasi, wawancara,
dan studi dokumentasi serta pokok bahasan yang sama yaitu prosedur
tetap. Perbedaan penelitian adalah penelitian Kuswijayanto (2010)
bertujan mengevaluasi prosedur tetap penyimpanan berkas rekam medis
yang ada di Rumah Sakit Santa Elisabeth Bantul, sedangkan penelitian
ini bertujuan untuk mengevaluasi proses, pemanfaatan dan ketepatan
8
waktu pelaksanaan pelayanan surat keterangan medis, serta membuat
usulan draft prosedur tetap pelayanan surat keterangan medis.
3. Masfuah (2009) dengan judul penelitian,
Tujuan penelitian ini adalah untuk mengetahui pelaksanaan
pembuatan prosedur tetap rekam medis dan dapat memberikan usulan
prosedur tetap rekam medis. Hasil yang diperoleh adalah visi misi rumah
sakit, visi misi rekam medis dan kebijakan rumah sakit di Rumah Sakit
Islam Cawas belum dibuat, pembuatan prosedur tetap rekam medis
diperoleh dua prosedur tetap yaitu pengelolaan sensus harian rawat inap
serta penyimpanan dan pengambilan berkas rekam medis.
Persamaan penelitian ini dengan penelitian Masfuah (2009)
terletak pada metodologi penelitian yang digunakan yaitu menggunakan
metode penelitian deskriptif dengan pendekatan kualitatif dan teknik
pengumpulan data yang digunakan yaitu dengan wawancara,
pengamatan (observasi) dan studi dokumentasi, serta pokok bahasan
yang sama yaitu prosedur tetap. Perbedaan penelitian ini adalah
penelitian Masfuah (2009) membahas tentang pelaksanaan pembuatan
prosedur tetap rekam medis dan memberikan usulan prosedur tetap
rekam medis di Rumah Sakit Islam Cawas, sedangkan penelitian ini
bertjuan untuk mengevaluasi proses, pemanfaatan dan ketepatan waktu
pelaksanaan pelayanan surat keterangan medis, serta membuat usulan
draft prosedur tetap pelayanan surat keterangan medis.

1. a.  Evaluasi   proses    pelaksanaan    prosedur    tetap   pelayanan   surat
keterangan medis di Instalasi Rekam Medis RSUD Kota Yogyakarta
adalah  belum  adanya  ruang  dan  petugas  khusus  untuk  pelayanan
surat keterangan medis, langkah-langkah pelaksanaan pelayanan
tidak berjalan dengan baik dan penerapannya tidak sesuai berkaitan
dengan proses pembuatan yang masih lebih dari 7 hari sehingga perlu
dicantumkan batasan waktu pembuatan dan gambar alur pelayanan.
b. Evaluasi pemanfaatan pelaksanaan prosedur tetap pelayanan surat
keterangan medis di Instalasi Rekam Medis RSUD Kota Yogyakarta
adalah surat keterangan medis telah dibuat sesuai dengan pihak yang
memerlukan dan jenis permintaannya, baik pasien/keluarga pasien,
pihak asuransi, pihak/suatu instansi dan pihak kepolisian.
c. Evaluasi ketepatan waktu pelaksanaan prosedur tetap pelayanan surat
keterangan medis di Instalasi Rekam Medis RSUD Kota Yogyakarta,
berdasarkan hasil rekapitulasi bulan Januari hingga Maret 2013, surat
keterangan medis yang tepat waktu 72,38% dan terlambat 27,62%,
permintaan terbanyak Klaim Asuransi Lain (36 permintaan) dan kasus
penyakit Dalam&Umum (21 kasus), keterlambatan waktu sering terjadi
pada permintaan Jasa Raharja (60%) dan kasus Bedah (36,36%).
2. Usulan draft prosedur tetap pelayanan surat keterangan medis, formatnya
telah mengacu pada KARS, 2001) serta terdapat perubahan
dibandingkan dengan draft prosedur yang ada, yaitu pengertian pasien,
93
ahli waris, penambahan tujuan, dan isi prosedur pada proses pembuatan
surat keterangan medis (usulan draft prosedur tetap terampir).
B. SARAN
1. a.  Adanya   keterlambatan  waktu  pembuatan  surat  keterangan   medis
sebesar 27,62% , maka perlu dilakukan penambahan ruang atau
bagian dan petugas surat keterangan medis khusus untuk
memaksimalkan pelayanan sehingga kepuasan dan kenyamanan
pelanggan dapat meningkat dan informasi mengenai keterangan medis
lebih terjaga kerahasiaanya.
b. Sebaiknya dilakukan sosialisasi terhadap dokter maupun petugas surat
keterangan medis mengenai pentingnya ketepatan waktu pembuatan
surat keterangan medis.
2. Usulan draft prosedur tetap pelayanan surat keterangan medis (terlampir)
dapat dijadikan masukan untuk penyusunan prosedur tetap pelayanan
surat keterangan medis, karena terdapat beberapa perubahan pada
pengertian pasien, ahli waris, penambahan tujuan, dan isi prosedur pada
proses pembuatan surat keterangan medis.

