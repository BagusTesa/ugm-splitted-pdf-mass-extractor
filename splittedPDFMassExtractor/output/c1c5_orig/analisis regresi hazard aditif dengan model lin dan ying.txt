1.1 Latar Belakang
Dalam penelitian-penelitian di bidang kesehatan sering dijumpai salah satu
jenis data yang disebut dengan data antar kejadian atau dikenal pula dengan nama
data survival. Data antar kejadian merupakan data lama waktu hingga suatu
kejadian (event) yang menjadi perhatian terjadi. Contoh data antar kejadian ini
antara lain data lama waktu kambuhnya penyakit seorang pasien setelah diberi
pengobatan, data umur pasien setelah didiagnosis suatu penyakit dan data lama
waktu sembuhnya pasien setelah melakukan terapi. Data antar kejadian dapat pula
ditemui dalam bidang-bidang lain, seperti dalam bidang demografi, sosial,
ekonomi dan ilmu perekayasaan.
Masalah yang sering ditemui dalam data antar kejadian adalah adanya
observasi yang tidak lengkap, yang disebabkan oleh data tersensor dan data
terpotong. Data tersensor dapat ditemui ketika terdapat informasi mengenai waktu
kejadian dari subjek penelitian, namun waktu kejadian yang pasti dari individu
tersebut tidak diketahui (Kleinbaum dan Klein, 2005). Data terpotong dapat
ditemui ketika hanya individu dengan waktu kejadian yang masuk dalam interval
tertentu saja yang terobservasi (Klein dan Moeschberger, 2003).
Seringkali, waktu kejadian diduga dipengaruhi oleh satu atau beberapa
variabel independen (kovariat). Apabila ingin diketahui pengaruh variabel
independen terhadap waktu kejadian maka analisis yang dapat digunakan adalah
analisis regresi. Adanya data tersensor dan terpotong mengakibatkan analisis
regresi linear biasa tidak dapat digunakan. Salah satu analisis yang dapat dipakai
untuk mengetahui hubungan antara lama waktu hingga kejadian dengan variabel-
variabel lain yang mempengaruhinya adalah analisis regresi hazard. Ada dua jenis
model hazard yang dapat digunakan dalam analisis ini, yakni model hazard
multiplikatif dan model hazard aditif.
Pada model hazard multiplikatif, pengaruh dari kovariat diasumsikan
multiplikatif terhadap baseline hazardnya. Model hazard multiplikatif yang cukup
2
sering digunakan adalah model regresi Cox. Karena hazard rate pada model
regresi Cox proporsional, model regresi Cox dikenal pula dengan sebutan model
hazard proporsional. Untuk mengestimasi koefisien regresi pada model regresi
Cox digunakan metode maximum partial likelihood.
Selain model hazard multiplikatif, terdapat pula model hazard aditif
dimana pengaruh dari kovariat diasumsikan aditif terhadap baseline hazardnya.
Model hazard aditif yang telah dikembangkan selama ini antara lain model
hazard aditif Aalen dan model hazard aditif Lin dan Ying (Klein dan
Moeschberger, 2003). Pada regresi hazard aditif dengan model Aalen, digunakan
metode kuadrat terkecil untuk memperoleh estimasi dari koefisien regresi
kumulatifnya, sedangkan pada regresi hazard aditif dengan model Lin dan Ying
digunakan metode yang menyerupai maximum partial likelihood pada regresi
Cox.
Pada regresi hazard aditif dengan menggunakan model Aalen, koefisien
regresi tidak dapat dicari secara langsung. Estimasi kasar dari koefisien regresi
pada model ini diperoleh dari slope estimasi fungsi regresi kumulatif. Model
hazard aditif yang dikembangkan oleh Lin dan Ying, sebaliknya, memiliki
kelebihan dalam hal ini. Koefisien regresi pada model hazard aditif Lin dan Ying
dapat diestimasi secara langsung, sehingga lebih mudah dalam
menginterpretasikannya. Oleh karena itu, penulis memilih untuk mengangkat
model hazard aditif Lin dan Ying dalam tulisan ini, sebagai alternatif dari model
hazard aditif Aalen, yang memiliki kelebihan dalam hal kemudahan
pengestimasian dan penginterpretasian koefisien regresinya.
1.2 Tujuan Penulisan
1. Mempelajari analisis regresi hazard aditif dengan model Lin dan Ying
dan menjelaskan penggunaan partial likelihood untuk mengestimasi
koefisien regresi pada model tersebut
2. Mengaplikasikan analisis regresi hazard aditif dengan model Lin dan
Ying pada data pasien penderita TBC di Puskesmas Mantang di
Lombok Tengah
3
1.3 Pembatasan Masalah
Penulis membatasi pembahasan pada pengkajian dan pengaplikasian
analisis regresi pada data antar kejadian dimana pengaruh kovariat aditif terhadap
baseline hazard dengan menggunakan model hazard aditif Lin dan Ying. Pada
analisis dalam tulisan ini, kovariat terbatas pada yang nilainya tidak bergantung
pada waktu, dengan kata lain kovariat yang nilainya tetap dan diketahui pada awal
penelitian.
1.4 Metode Penulisan
Metode yang digunakan dalam penulisan tugas akhir ini adalah studi
literatur. Sumber literatur yang diperoleh penulis adalah sumber-sumber resmi
seperti buku yang berisi teori-teori yang berkaitan dengan materi, jurnal dan
artikel-artikel dari internet.
1.5 Tinjauan Pustaka
Model hazard multiplikatif merupakan model yang banyak digunakan
dalam analisis regresi pada data antar kejadian. Cox (1972) memperkenalkan
salah satu model hazard multiplikatif ini, yang kemudian disebut dengan model
hazard multiplikatif Cox. Model Cox dikenal pula dengan nama model hazard
proporsional. Cox dan Oakes (1984) memaparkan regresi Cox ini secara lebih
terperinci. Kleinbaum dan Klein (2005) juga membahas mengenai regresi Cox
disertai dengan contoh aplikasinya.
Selain model hazard multiplikatif, terdapat pula beberapa penulis yang
telah mengembangkan model hazard aditif. Lin dan Ying (1994) memperkenalkan
model hazard aditif dengan koefisien regresi yang bersifat konstan. Metode yang
digunakan untuk memperoleh estimasi dari koefisien regresi pada model ini mirip
dengan maximum partial likelihood pada regresi Cox. Klein dan Moeschberger
(2003) juga menulis mengenai regresi hazard aditif dengan model Lin dan Ying
serta model Aalen, dan membahasnya secara lebih mendalam. Risnawati (2008)
dalam tugas akhirnya membahas mengenai regresi hazard aditif dengan model
Aalen.
4
1.6 Sistematika Penulisan
Sistematika penulisan dalam skripsi ini adalah sebagai berikut
BAB I  PENDAHULUAN
Bab Pendahuluan membahas tentang latar belakang masalah yang
diambil, maksud dan tujuan penulisan, metode penulisan yang
digunakan, tinjauan pustaka, serta sistematika penulisan secara
keseluruhan.
BAB II  LANDASAN TEORI
Bab Landasan Teori ini berisi kumpulan teori yang menunjang
pembahasan materi. Teori-teori yang disajikan antara lain mengenai
data antar kejadian, data tersensor dan terpotong, fungsi survival dan
hazard, regresi Cox, regresi hazard aditif dengan model Aalen,
matriks, product integral dan counting process.
BAB III  ANALISIS REGRESI HAZARD ADITIF DENGAN MODEL LIN
Pada bab ini dibahas tentang model hazard aditif yang digunakan
untuk menganalisis data antar kejadian. Selain itu dibahas pula
estimasi koefisien regresi, estimasi variansi dan uji hipotesis.
Pada Bab ini terdapat pengaplikasian analisis regresi hazard aditif
dengan model Lin dan Ying untuk data mengenai pasien penderita
penyakit TBC di Puskesmas Mantang, Lombok Tengah.
Bab ini berisi kesimpulan yang diperoleh dari pembahasan.

5.1 Kesimpulan
Berdasarkan pembahasan pada bab �bab sebelumnya, dapat diperoleh
kesimpulan sebagai berikut:
1. Model hazard Lin dan Ying merupakan model hazard dengan
pengaruh kovariat aditif terhadap baseline hazardnya. Koefisien
regresi pada model hazard Lin dan Ying ini bersifat konstan atau
bernilai tetap dari waktu ke waktu.
Untuk mengestimasi koefisien regresi pada model hazard aditif Lin
dan Ying digunakan metode yang menyerupai maximum partial
likelihood pada regresi Cox. Estimasi koefisien regresi dapat diperoleh
dari score equation regresi Cox yang fungsi hazardnya diganti dengan
fungsi hazard aditif Lin dan Ying.
2. Model hazard Lin dan Ying merupakan salah satu model hazard aditif
alternatif selain model hazard Aalen. Dalam pengaplikasian analisis
regresi hazard aditif dengan model Lin dan Ying pada data pasien
TBC di Puskesmas Mantang di Lombok Tengah, didapatkan hasil
bahwa variabel independen yang signifikan mempengaruhi lama
waktu hingga pengobatan TBC gagal adalah jenis kelamin dan
pengobatan lengkap.
Tidak dibandingkan antara kedua model ini mana yang lebih baik
karena keduanya memiliki kelebihan masing-masing. Dalam hal
interpretasi pengaruh dari masing-masing variabel, analisis dengan
model Lin dan Ying memiliki kemudahan, karena estimasi koefisien
regresi dapat secara langsung diperoleh sehingga tidak harus melihat
dan menginterpretasikan grafik seperti saat menggunakan model
Aalen.
46
5.2 Saran
1. Selain model hazard aditif dan multiplikatif, terdapat pula model
hazard yang menggabungkan antara model hazard aditif dan
multiplikatif. Pembahasaan mengenai model ini dapat digunakan
untuk penelitian selanjutnya.
2. Data antar kejadian dalam analisis regresi hazard aditif dengan model
Lin dan Ying yang digunakan dalam skripsi ini terbatas pada data
dengan masing-masing subjek penelitian hanya mengalami maksimal
1 kejadian saja. Ketika kejadian yang menjadi perhatian dapat terjadi
berulang kali (recurrent event), maka analisis yang digunakan menjadi
sedikit berbeda. Pembahasan mengenai regresi hazard aditif dengan
model Lin dan Ying untuk data dengan recurrent event ini juga dapat
digunakan untuk penelitian selanjutnya.

