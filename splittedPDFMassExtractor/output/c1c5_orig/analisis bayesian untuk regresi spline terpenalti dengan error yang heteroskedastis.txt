1.1 Latar Belakang dan Permasalahan
Analisis regresi merupakan salah satu metode statistika yang digunakan
untuk memodelkan hubungan antara variabel respon dan prediktor. Hubungan
antara variabel respon dan prediktor dapat berbentuk parametrik, nonparametrik,
atau semiparametrik. Semiparametrik merupakan gabungan dari parametrik dan
nonparametrik.
Analisis regresi yang memodelkan hubungan parametrik dinamakan
regresi parametrik. Regresi parametrik mengasumsikan bentuk kurva regresi
sudah ditentukan (Hardle, 1994). Beberapa contoh regresi parametrik diantaranya
regresi linear, regresi kuadratik, regresi eksponensial, dan lain-lain.
Analisis regresi yang memodelkan hubungan nonparametrik disebut
regresi nonparametrik. Regresi nonparametrik tidak bergantung pada asumsi
bentuk kurva tertentu sehingga mempunyai fleksibilitas tinggi (Hardle, 1994).
Terdapat beberapa metode penghalus dalam regresi nonparametrik, salah
satunya adalah regresi spline terpenalti (penalized spline regression). Parameter
dalam regresi spline terpenalti dapat diestimasi dengan menggunakan metode
Bayesian. Metode Bayesian menggabungkan informasi sampel dan informasi
prior. Informasi prior merupakan informasi terdahulu mengenai distribusi
parameter yang tidak diketahui, sifatnya subjektif karena tergantung pendapat ahli
mengenai parameter tersebut.
Regresi spline terpenalti semakin berkembang dan luas pemanfaatannya,
tidak hanya untuk kasus variansi error yang konstan (homoskedastis) namun juga
untuk kasus variansi error yang tidak konstan/berubah-ubah (heteroskedastis).
Kasus heteroskedastisitas error ini perlu diperhatikan karena jika diabaikan akan
menghasilkan pengambilan keputusan yang salah dan estimator yang tidak efisien
(Crainiceanu dkk., 2007). Oleh karena itu perlu dibahas analisis regresi spline
terpenalti dengan metode Bayesian untuk kasus error yang heteroskedastis.
2
1.2  Tujuan Penelitian
Tujuan dari penelitian ini adalah sebagai berikut.
1) Mempelajari analisis Bayesian untuk regresi spline terpenalti dengan error
yang homoskedastis.
2) Mempelajari analisis Bayesian untuk regresi spline terpenalti dengan error
yang heteroskedastis.
3) Mengaplikasikan analisis Bayesian untuk regresi spline terpenalti dengan
error yang heteroskedatis dalam menganalisis hubungan nilai tukar Rupiah
dengan Indeks Harga Saham Gabungan (IHSG).
1.3 Tinjauan Pustaka
Pembahasan mengenai analisis regresi spline terpenalti dengan metode
Bayesian telah dilakukan oleh beberapa peneliti sebelumnya, seperti Crainiceanu
dkk. (2005), Crainiceanu dkk. (2007), dan Fitriani (2013).
Crainiceanu, Ruppert, dan Wand (2005) membahas analisis regresi spline
terpenalti dengan metode Bayesian menggunakan software Winbugs. Regresi
spline terpenalti dimodelkan sebagai model linear campuran. Crainiceanu dkk.
(2005) memberikan beberapa contoh aplikasi penggunaan regresi spline terpenalti
dengan metode Bayesian, salah satunya yaitu untuk memodelkan hubungan antara
usia dengan tingkat pendapatan para pekerja Kanada. Komputasi dilakukan
menggunakan software Winbugs dan package R2WinBUGS pada software R.
Crainiceanu, Ruppert, Carrol, Joshi, dan Goodner (2007) membahas
analisis Bayesian untuk regresi spline terpenalti adaptif spasial untuk error yang
heteroskedastis. Variansi dari error yang heteroskedastis dimodelkan dengan log-
penalized spline dan metode penghalusan yang digunakan yaitu regresi spline
terpenalti adaptif spasial. Estimasi parameter menggunakan metode Bayesian.
Software yang digunakan yaitu MATLAB, C, FORTRAN.
Fitriani (2013) membahas analisis Bayesian untuk regresi spline terpenalti
dan estimasi regresi spline terpenalti menggunakan metode kuadrat terkecil.
Analisis Bayesian yang dilakukan menggunakan pendekatan Gibbs sampling.
Fitriani (2013) memodelkan hubungan antara jumlah uang beredar dengan inflasi
3
di Indonesia. Hasil analisis Bayesian dibandingkan dengan hasil estimasi metode
kuadrat terkecil. Hasil regresi spline terpenalti dengan metode Bayesian
menghasilkan nilai SSE terkecil dan R
2
terbesar.
1.4 Metodologi Penelitian
Metode yang digunakan dalam penelitian ini adalah studi pustaka atau
studi literatur. Bahan penelitian diperoleh dari jurnal, buku dan referensi lain dari
situs penunjang di internet. Bagian awal pembahasan akan dijelaskan mengenai
analisis Bayesian untuk regresi spline terpenalti dengan error yang homoskedastis.
Selanjutnya akan dibahas analisis Bayesian untuk regresi spline terpenalti dengan
error yang heteroskedastis. Studi kasus yang akan diambil yaitu memodelkan
hubungan nilai tukar Rupiah dengan Indeks Harga Saham Gabungan (IHSG).
Pada studi kasus tersebut akan dilihat keakuratan masing-masing metode
menggunakan MSE dan R
2
. Komputasi dilakukan dengan menggunakan bantuan
software R.
1.5 Sistematika Penulisan
Sistematika penulisan Tesis ini adalah sebagai berikut.
BAB I PENDAHULUAN
Bab ini berisi latar belakang dan permasalahan, tujuan penelitian,
tinjauan pustaka, metodologi penelitian, dan sistematika penulisan.
BAB II DASAR TEORI
Bab ini membahas dasar teori yang mendukung pembahasan analisis
Bayesian untuk regresi spline terpenalti dengan error yang
heteroskedastis.
BAB III ANALISIS BAYESIAN UNTUK REGRESI SPLINE TERPENALTI
DENGAN ERROR YANG HETEROSKEDASTIS
Bab ini membahas model regresi spline terpenalti dengan error yang
homoskedastis, model regresi spline terpenalti dengan error yang
heteroskedastis, analisis Bayesian untuk regresi spline terpenalti dengan
4
error yang homoskedastis, dan analisis Bayesian untuk regresi spline
terpenalti dengan error yang heteroskedastis.
Bab ini membahas aplikasi analisis Bayesian untuk regresi spline
terpenalti dengan error yang heteroskedastis dalam menganalisis
hubungan antara nilai tukar Rupiah dengan IHSG. Hasilnya
dibandingkan dengan hasil analisis Bayesian untuk regresi spline
terpenalti dengan error yang homoskedastis dan hasil estimasi
menggunakan model regresi linear sederhana.
Bab ini berisi kesimpulan dari pembahasan pada bab sebelumnya dan
saran atas kekurangan dari hasil penelitian yang telah dilakukan.

5.1 Kesimpulan
Berdasarkan pembahasan pada bab sebelumnya, diperoleh kesimpulan
bahwa:
1. Regresi spline terpenalti dapat digunakan untuk kasus error yang
homoskedastis dan heteroskedastis.
2. Estimasi parameter pada model regresi spline terpenalti dengan error yang
heteroskedastis dapat dilakukan dengan menggunakan metode bayesian.
3. Estimasi distribusi posterior pada metode bayesian dapat dilakukan dengan
menggunakan pendekatan gibbs sampling dan Metropolis Hasting.
4. Hasil studi kasus analisis hubungan antara nilai tukar Rupiah dengan IHSG
menunjukkan bahwa hasil estimasi regresi spline terpenalti dengan error yang
heteroskedastis menggunakan metode bayesian menghasilkan tingkat
keakuratan yang lebih besar dibandingkan metode regresi linear sederhana
dan regresi spline terpenalti dengan error yang homoskedastis menggunakan
metode bayesian.
5.2 Saran
Saran untuk penelitian selanjutnya yaitu:
1. Diperlukan kajian lebih lanjut mengenai analisis bayesian untuk regresi spline
terpenalti dengan error yang heteroskedatis untuk variabel prediktor lebih dari
satu.
2. Dalam metode bayesian diperlukan prior yang tepat agar diperoleh hasil
estimasi dengan tingkat keakuratan yang tinggi.

