1.1 Latar Belakang Permasalahan
Analisis yang sering digunakan dalam penelitian untuk mengetahui hubungan
suatu variabel terhadap variabel lain adalah analisis regresi linear. Pada beberapa
kasus, dibutuhkan suatu model yang berguna untuk perbaikan proses, yaitu model
yang dapat mengoptimalkan karakteristik kualitas yang diteliti.  Untuk memenuhi
model ini Box dan Wilson (1951) melakukan pendekatan statistik secara sistematis
yang menghasilkan metode permukaan respon.
Metode permukaan respon adalah himpunan metode-metode matematika dan
statistika yang digunakan untuk melihat hubungan antara satu atau lebih variabel
perlakuan berbentuk kuantitatif dengan sebuah variabel respon yang bertujuan untuk
mengoptimalkan respon tersebut dalam suatu percobaan (Montgomery, 2001).
Metode ini bermanfaat untuk mengembangkan, meningkatkan, dan mengoptimasi
proses. Penerapannya sangat penting terutama di bidang rancangan, pengembangan
dan perumusan produk baru, serta pada peningkatan rancangan produk yang sudah
ada.
Dasar analisis metode permukaan respon hampir sama dengan analisis regresi,
yaitu prosedur penaksiran parameter fungsi responnya diperoleh dengan
menggunakan metode kuadrat terkecil (Least Square Method). Perbedaan analisis
permukaan respon dengan analisis regresi adalah dalam analisis respon diterapkan
teknik-teknik matematika untuk menentukan titik-titik optimum  agar dapat
ditentukan respon yang optimum (Montgomery, 2001). Respon yang optimum dapat
berupa respon yang maksimum, minimum, atau saddle sesuai dengan tujuannya.
Metode permukaan respon memiliki beberapa bentuk persamaan, yaitu fungsi
berorde satu dan fungsi berorde dua. Fungsi orde satu digunakan bila hubungan
antara respon dengan perlakuan berbentuk linear. Fungsi orde dua digunakan bila
2
hubungan antara respon dengan perlakuan berbentuk kuadratik, sehingga model
fungsi ini disebut juga model kuadratik.
Ada berbagai macam cara dalam mencari titik optimum, salah satunya adalah
analisis ridge, yang akan dibahas lebih lanjut dalam skripsi ini. Analisis ridge
merupakan suatu cara yang digunakan untuk menentukan titik optimasi dengan
menggunakan metode Lagrange Multiplier, yang memenuhi syarat  ??
2 = ?2??=1 .
Tujuan dari analisis ridge adalah untuk melabuhkan titik stasioner ke dalam daerah
percobaan. Analisis ini digunakan ketika titik stasionernya tidak memiliki nilai.
Misalnya ketika titik stasioner berada di titik saddle, atau ketika titik stasioner
merupakan titik maksimum atau minimum, tetapi titik tersebut berada di luar daerah
percobaan sehingga tidak dapat diambil kesimpulan mengenai kondisi pengoperasian
yang harus dilakukan (Myers dkk, 2009).
1.2 Pembatasan Masalah
Pembahasan mengenai metode permukaan respon dan optimasinya memiliki
cakupan yang luas sehingga dalam skripsi ini hanya akan dibahas mengenai optimasi
metode permukaan respon kuadratik menggunakan analisis ridge dengan rancangan
percobaannya adalah central composite design (CCD).
1.3 Tujuan dan Manfaat Penulisan
Tujuan dari penulisan skripsi ini adalah :
1. Mempelajari salah satu rancangan percobaan, yaitu central composite design
(CCD).
2. Mempelajari metode permukaan respon kuadratik.
3. Menentukan titik optimasi variabel respon pada permukaan respon kuadratik
menggunakan analisis ridge.
1.4 Tinjauan Pustaka
Pembahasan mengenai metode permukaan respon pernah dilakukan oleh
beberapa peneliti sebelumnya, seperti Box dan Wilson, Hoerl, Draper, Shu-kai Fan,
serta Myers dan Montgomery.
3
G. E. P. Box dan K. B. Wilson (1951) pertama kali mengenalkan metode
permukaan dalam artikelnya yang berjudul �On the Experimental Attainment of
Optimum Conditions�. Dalam artikel ini, mereka menyatakan bahwa eksperimen
yang berkaitan dengan statistik harus sangat fleksibel dan memungkinkan adanya
iterasi (perulangan). Mereka mengenalkan pula sebuah rancangan percobaan yaitu
central composite design (CCD).
Hoerl (1959) dan Draper (1963) membahas konsep mengenai analisis ridge.
Inti dari analisis ridge adalah prosedur akar temuan dari Lagrange Multiplier untuk
persamaan stasioner sehingga memenuhi persamaan pembatas spherical (bola).
Shu-Kai S. Fan (2003) membahas mengenai analisis ridge pada permukaan
respon kuadratik. Analisis ini bertujuan untuk mencari estimasi dari kondisi optimum
di dalam wilayah spherical (bola) pada percobaan selama proses optimasi.
Myers dan Montgomery (2009) membahas mengenai optimisasi permukaan
respon. Untuk mengidentifikasi dan menentukan model permukaan respon yang
sesuai, diperlukan langkah-langkah mulai dari eksperimen desain, teknik pemodelan
regresi, sampai metode optimasi dasar.
1.5 Metodologi Penulisan
Metode yang digunakan dalam penulisan skripsi ini adalah studi pustaka atau
studi literatur. Bahan tulisan dari skripsi ini bersumber dari jurnal-jurnal, buku-buku,
dan referensi lain yang diperoleh dari situs-situs penunjang di internet.
1.6 Sistematika Penulisan
Sistematika penulisan dalam skripsi ini adalah sebagai berikut.
BAB I  PENDAHULUAN
Bab ini berisi latar belakang masalah, tujuan dan manfaat penulisan,
tinjauan pustaka, metodologi penulisan, dan sistematika penulisan.
BAB II  DASAR TEORI
4
Bab ini membahas tentang dasar teori yang mendukung pembahasan
analisis ridge pada metode permukaan respon kuadratik menggunakan
rancangan central composite design (CCD).
BAB III  OPTIMASI METODE PERMUKAAN RESPON KUADRATIK
Bab ini membahas mengenai optimasi permukaan respon kuadratik
menggunakan analisis ridge dengan rancangan percobaannya adalah
Bab ini membahas aplikasi analisis ridge pada Metode Permukaan
Respon Kuadratik menggunakan rancangan CCD.
Bab ini berisi kesimpulan dari pembahasan pada bab sebelumnya dan
saran atas kekurangan dari hasil penelitian yang telah dilakukan.

5.1 Kesimpulan
Berdasarkan pembahasan pada bab sebelumnya, diperoleh beberapa kesimpulan
sebagai berikut :
1. Analisis ridge merupakan salah satu metode yang dapat digunakan untuk
mengoptimasi model permukaan respon.
2. Analisis ridge dapat digunakan sebagai penanganan lebih lanjut ketika titik
optimal berada pada titik saddle atau berada di luar daerah percobaan.
3. Dari hasil studi kasus analisis optimasi variabel tekanan low air, low air cooling,
dan natural gas terhadap kekuatan torque diperoleh bahwa model permukaan
respon kuadratik untuk kekuatan torque adalah :
? = 0,23969 ? 0,0070788?1 ? 0,0088221?2 + 0,0007214?3
+ 0,0063895?1?2 + 0,0026965?1?3 + 0,0023635?2?3
? 0,0011676?1
2 ? 0,0060728?2
2 ? 0,0059492?3
2
dengan nilai optimum berada pada titik maksimum local dimana x1 menunjukkan
nilai LA koding, x2 adalah nilai LAC koding, dan x3 adalah nilai NG koding.
Kondisi optimal kekuatan torque pada lampu TL tipe FL 10 watt terjadi ketika
tekanan low air sebesar 3,80125 Kpa, tekanan low air cooling sebesar 7,35150
Kpa, dan tekanan natural gas sebesar 4,970 Kpa dengan nilai maksimum
kekuatan torque sebesar 3,11125 Newton meter.
5.2 Saran
Saran untuk pengembangan skripsi selanjutnya yaitu :
1. Ketika rancangan percobaan bukan merupakan rancangan yang rotatable,
penanganan terhadap titik optimal yang berada pada titik saddle atau berada
di luar daerah percobaan dapat dilakukan dengan menggunakan modifikasi
prosedur standar analisis ridge.
2. Analisis ridge juga dapat digunakan untuk menyelesaikan kasus optimalisasi
ketika terdapat variabel pengganggu (noise variable) dalam metode
permukaan respon untuk proses variansi.

