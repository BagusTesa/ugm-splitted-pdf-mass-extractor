1.1. Latar Belakang
Fuzzy Evolutionary Algorithm (FEA) merupakan salah satu model hybrid
yang menggabungkan dua buah model soft computing yaitu algoritma genetika
dan logika fuzzy. FEA adalah konsep hybrid yang menggunakan sistem fuzzy ke
dalam proses algoritma genetika. Karena dalam proses algoritma genetika,
pengguna harus mendefinisikan beberapa parameter pada awal running agar
proses dapat berjalan dengan baik. Terdapat tiga parameter penting dalam
algoritma genetika yaitu ukuran populasi, probabilitas pindah silang, dan
probabilitas mutasi. Ketiga parameter ini harus didefinisikan secara hati-hati agar
tidak terjadi konvergensi dini atau lokal optimum yaitu dimana individu-individu
dalam populasi konvergen pada suatu solusi optimum lokal (Suyanto, 2005).
Xu dan Vukovich (1993), melakukan penelitian terkait penggunaan logika
fuzzy dalam algoritma genetika untuk mengatasi dua permasalahan yang biasa
terjadi pada algoritma genetika yaitu kecepatan pencarian solusi yang lambat dan
konvergensi dini. Pemanfaatan logika fuzzy dalam algoritma genetika ditujukan
untuk memudahkan pengguna dalam mendefinisikan parameter yang digunakan.
Model yang dikenalkan oleh Xu dan Vukovich adalah penggunaan logika fuzzy
untuk penentuan nilai probabilitas pindah silang dan nilai probabilitas mutasi
menggunakan aturan fuzzy dengan dua masukan yaitu ukuran populasi dan
jumlah generasi.
Seiring dengan munculnya konsep FEA, sebuah toolbox yang menerapkan
konsep FEA model Xu dan Vukovich telah dikembangkan. Toolbox FEA tersebut
dapat digunakan untuk menyelesaikan beberapa permasalahan yang dapat
diselesaikan dengan algoritma genetika. Toolbox FEA juga memiliki fungsi-
fungsi yang menerapkan konsep FEA yang dapat digunakan oleh pengguna
melalui command windows (Muzid dkk, 2009).
Konsep FEA yang dikembangkan oleh Xu dan Vukovich masih memiliki
kelemahan yaitu apabila nilai masukan untuk aturan fuzzy pada setiap generasi
2
bernilai statis maka nilai keluaran dari aturan fuzzy yang meliputi nilai
probabilitas pindah silang dan probabilitas mutasi juga akan bernilai statis. Hal ini
memungkinkan hasil yang ditemukan kurang optimum karena parameter yang
digunakan dalam FEA bernilai statis. Oleh karena itu dibutuhkan sebuah model
yang mampu beradaptasi dan menghasilkan nilai dinamis sehingga nilai keluaran
yang dihasilkan oleh FEA bernilai dinamis.
Eiben dkk (2004) melakukan penelitian dan menghasilkan sebuah model
perhitungan yang digunakan untuk menentukan ukuran populasi baru yang akan
digunakan pada generasi berikutnya. Model tersebut adalah Population Resizing
on Fitness Improvement Genetic Algorithm (PRoFIGA) yaitu model perhitungan
untuk menentukan ukuran populasi baru berdasarkan dari perkembangan nilai
fitness terbaik. Ukuran populasi merupakan sebuah parameter yang penting dalam
algoritma genetika. Jika ukuran populasi terlalu kecil akan memungkinkan
terjadinya konvergensi dini, dan jika terlalu besar akan mengakibatkan lamanya
waktu yang dibutuhkan algoritma genetika dalam menghasilkan solusi terbaik.
Dalam model ini ukuran populasi yang dihasilkan dapat beradaptasi menjadi lebih
sedikit atau lebih banyak pada setiap generasi seiring dengan perkembangan nilai
fitness terbaik sehingga dapat meningkatkan performansi algoritma genetika.
Dengan latar belakang tersebut, diperlukan sebuah pengembangan untuk
memperbaiki kelemahan pada konsep FEA model Xu dan Vukovich serta
pengembangan toolbox FEA dengan menggabungkan model PRoFIGA kedalam
konsep FEA tersebut agar sistem fuzzy yang digunakan dapat menghasilkan nilai
keluaran yang dinamis sehingga dapat menghasilkan solusi yang lebih optimum
dan mampu meningkatkan performansi dari algoritma genetika.
1.2. Rumusan Masalah
Berdasarkan latar belakang masalah diatas akhirnya dapat dirumuskan
beberapa rumusan sebagai berikut:
1. Mengembangkan konsep FEA model Xu dan Vukovich yang digabungkan
dengan model penentuan ukuran populasi PRoFIGA.
3
2. Mengembangkan toolbox FEA dengan aturan fuzzy model Xu dan
Vukovich yang dinamis untuk pengontrolan parameter pada setiap
generasinya.
3. Membuat fungsi baru dengan menggunakan algoritma FEA yang dinamis
pada setiap generasi dan dapat digunakan untuk menyelesaikan berbagai
masalah optimasi yang dapat diintegrasikan dengan bahasa pemrograman
tertentu.
1.3. Batasan Masalah
Agar permasalahan yang dibahas tidak melebar terlalu luas diperlukan
batasan masalah. Adapun batasan yang digunakan adalah sebagai berikut:
1. Model FEA yang dikembangkan adalah model Xu dan Vukovich yang
digabungkan dengan model Population Resizing on Fitness Improvement
Genetic Algorithm (PRoFIGA) untuk penentuan ukuran populasi dari
perkembangan nilai fitnes terbaik.
2. Permasalahan yang diuji adalah permasalahan Travelling Salesman
Problem (TSP) dan optimasi fungsi.
3. Hasil pengujian dibandingkan dengan hasil pengujian menggunakan FEA
model Xu dan Vukovich, algoritma genetika.
4. Metode yang digunakan untuk mendeteksi signifikasi perbedaan hasil dari
ketiga metode tersebut menggunakan metode analysis of variance
(ANOVA).
5. Metode yang digunakan untuk mendeteksi keberagaman individu dalam
populasi menggunakan metode perhitungan varians dan standar deviasi.
1.4. Tujuan Penelitian
Adapun tujuan dari penelitian ini adalah sebagai berikut:
1. Mengembangkan model hybrid FEA model Xu dan Vukovich yang
digabungkan dengan model PRoFIGA.
2. Mengembangkan toolbox FEA dengan aturan fuzzy model Xu dan
Vukovich dinamis yang digabungkan dengan model PRoFIGA
didalamnya.
4
3. Membuat fungsi baru untuk penerapan konsep FEA model Xu dan
Vukovich yang digabungkan dengan model PRoFIGA yang dapat
digunakan oleh pengguna yang bisa dintegrasikan dengan bahasa
pemrograman tertentu untuk menyelesaikan berbagai masalah optimasi.
4. Melakukan pengujian konsep FEA model Xu dan Vukovich yang
digabungkan dengan model PRoFIGA pada permasalahan optimasi fungsi
dan Travelling Salesman Problem (TSP) serta membandingkannya dengan
penyelesaian masalah menggunakan algoritma genetika standar dan
konsep FEA model Xu dan kolega.
1.5. Manfaat Penelitian
Manfaat yang didapatkan dari penelitian adalah sebagai berikut:
1. Menghasilkan toolbox FEA yang dinamis agar dapat digunakan untuk
menyelesaikan berbagai masalah optimasi.
2. Menghasilkan fungsi baru yang menerapkan konsep FEA yang dinamis
agar dapat digunakan oleh pengguna untuk diintegrasikan dengan bahasa
pemrograman tertentu untuk menyelesaikan berbagai masalah optimasi.
3. Menerapkan model hybrid FEA model Xu dan Vukovich yang
digabungkan dengan model PRoFIGA untuk menghasilkan solusi yang
lebih optimum dan meningkatkan performansi algoritma genetika.
4. Membantu dan memudahkan pengguna untuk memahami teori algoritma
fuzzy evolusi dan menyelesaikan masalah menggunakannya.
5. Menambah wawasan dan ilmu pengetahuan dalam bidang soft computing
dengan model FEA yang lebih dinamis.
1.6. Keaslian Penelitian
Pada penelitian sebelumnya penulis telah mengembangkan toolbox FEA yang
menerapkan konsep hybrid antara algoritma genetika dan logika fuzzy. Model
yang digunakan pada toolbox FEA tersebut adalah model Xu dan Vukovich.
Toolbox tersebut masih memiliki kelemahan yaitu masukan yang digunakan
dalam logika fuzzy yang berupa ukuran populasi masih bernilai statis sehingga
5
keluaran yang dihasilkan dari FEA juga bernilai statis. Hal ini memungkinkan
hasil yang ditemukan tidak merupakan solusi optimum sehingga membutuhkan
sebuah pengembangan agar toolbox FEA tersebut lebih dinamis dalam
menghasilkan nilai parameter.
Pada penelitian ini menghasilkan sebuah model pengembangan FEA yang
dinamis yang menggabungkan sistem fuzzy model Xu dan Vukovich dengan
model PRoFIGA. Selain itu juga menghasilkan toolbox dan fungsi FEA yang
dinamis dalam pengontrolan parameter-parameter sehingga menghasilkan solusi
yang lebih optimum dan meningkatkan performansi algoritma genetika dalam
menemukan solusi.
1.7. Metode Penelitian
Dalam penelitian ini, metode pengembangan perangkat lunak yang dilakukan
adalah menggunakan metode prototype dan diawali dengan studi kepustakaan.
Tahapan dari metode penelitian tersebut adalah sebagai berikut:
1. Studi pustaka.
Studi pustaka dilakukan dengan cara mempelajari, mendalami, dan
mengutip teori atau konsep dari sejumlah literatur, baik buku, jurnal yang
relevan dengan topik, fokus atau variabel penelitian yang berkaitan dengan
algoritma genetika, sistem fuzzy, dan algoritma fuzzy evolusi khususnya
metode Xu dan Vukovich serta model PRoFIGA.
2. Analisa sistem.
Tahap ini dilakukan untuk menganalisa teori yang ada, teori terkait teori
FEA, model hybrid yang dikembangkan Xu dan Vukovich, serta model
PRoFIGA yang dikembangkan Eiben, Marchiori dan Valko. Dalam tahap
ini juga dilakukan analisa pada toolbox FEA yang sudah dikembangkan
sebelumnya.
3. Desain Sistem.
Desain sistem dilakukan untuk merancang proses dan antarmuka dari
sistem yang akan dikembangkan. Metode desain atau perancangan sistem
yang digunakan adalah menggunakan diagram Flowchart.
6
4. Pengkodean Sistem.
Tahap ini adalah tahap dimana sistem baru mulai dibangun dengan
menuliskan kode program dalam bentuk modul fungsi dan pengembangan
graphic user interface (GUI) serta integrasi dari modul-modul fungsi
tersebut.
5. Percobaan (Testing).
Percobaan dilakukan untuk menguji terkait pengujian unit dan integrasi
modul dan apakah sistem baru ini sudah dapat berjalan sesuai dengan
kebutuhan yang diinginkan. Pada tahap ini juga dilakukan pengujian
terhadap permasalahan TSP dan optimasi fungsi baik.
1.8. Sistematika Penulisan
Sistematika penulisan laporan berguna untuk memberikan gambaran umum
dari keseluruhan isi laporan serta untuk mempermudah pembaca untuk lebih
mudah memahami. Sistematika penulisan dan garis besar isi laporan ini adalah
sebagai berikut:
Bab I berisi Pendahuluan yang menjelaskan latar belakang serta rumusan
masalah yang diteliti, batasan masalah yang menjadi tolak ukur penelitian, tujuan
dan manfaat penelitian, keaslian penelitian dan metodologi penelitian serta
sistematika penulisan yang digunakan.
Bab II berisi Tinjauan Pustaka, berisi tentang pemaparan hal-hal yang
berkaitan serta pustaka yang dipakai pada waktu penelitian. Berisi tentang
penelitian-penelitian sebelumnya dan perbedaan antara penelitian sekarang
dengan penelitian yang pernah dilakukan.
Bab III berisi Landasan Teori, yang merupakan pembahasan tentang teori-
teori tentang algoritma genetika, sistem fuzzy, algoritma fuzzy evolusi, fuzzy rule
based model Xu dan Vukovich, model PRoFIGA, dan toolbox FEA yang sudah
ada.
Bab IV berisi Rancangan dan Implementasi Sistem yang menjelaskan
mengenai perancangan sistem yang akan diterapkan sehingga apa yang dirancang
benar-benar sesuai dengan apa yang dibutuhkan menggunakan diagram alir dan
7
diagram alir data. Serta membahas implementasi sistem yang menjelaskan
implementasi yang merupakan tahap selanjutnya dari perancangan sehingga
menjadi suatu aplikasi yang terdiri dari proses, masukkan (input) dan keluaran
(output).
Bab V berisi Hasil dan Pembahasan yang menjelaskan hasil dari sistem yang
dibangun dan diuji sesuai dengan kebutuhan perangkat lunak yang sebenarnya.
Hasil pengujian akan dibandingkan dengan pengujian menggunakan metode lain.
Bab ini juga menjelaskan bagaimana kelebihan serta kekurangan setelah
penelitian dilakukan agar dapat digunakan untuk penelitian selanjutnya.
Bab VI berisi Kesimpulan dan Saran, penulis mengambil beberapa
kesimpulan dari pengembangan konsep dan toolbox serta saran-saran berdasarkan
keterbatasan dan kekurangan yang ditemukan pada penelitian ini.

7.1. Kesimpulan
Berdasarkan implementasi dan pengujian terhadap penelitian ini maka dapat
terdapat beberapa hal yang dapat disimpulkan. Kesimpulan dari penelitian ini
adalah sebagai berikut:
1. Hasil pengujian perbandingan pada permasalahan optimasi fungsi dan TSP
menunjukkan bahwa algoritma PRoFIFEA mampu menghasilkan solusi yang
lebih baik atau lebih optimal dibandingkan algoritma FEA dan algoritma
genetika. Hal ini dibuktikan pada pengujian berulang untuk permasalahan
optimasi fungsi sebanyak 1000 kali, algoritma PRoFIFEA menemukan solusi
lebih baik dari kedua algoritma lainnya sebanyak 991 kali. Selain itu pada
pengujian berulang untuk permasalahan TSP sebanyak 100 kali, algoritma
PRoFIFEA juga menemukan solusi lebih baik dari kedua algoritma lainnya
sebanyak 77 kali.
2. Berdasarkan hasil uji ANOVA menunjukkan bahwa algoritma PRoFIFEA
memiliki tingkat signifikasi perbedaan hasil yang cukup tinggi dibandingkan
dengan hasil yang ditemukan oleh algoritma FEA dan algoritma genetika. Hal
ini dibuktikan dengan nilai P-value pada uji ANOVA untuk permasalahan
optimasi fungsi dan TSP menghasilkan nilai dibawah batas nilai alpha.
3. Tingkat keberagaman individu dalam populasi pada algoritma PRoFIFEA
lebih baik daripada individu dalam algoritma FEA atau algoritma genetika
standar. Hal ini ditunjukkan dengan nilai perhitungan standar deviasi antara
algoritma PRoFIFEA dengan algoritma genetika sebesar 67% dan sebesar
15% antara algoritma PRoFIFEA dengan algoritma FEA. Sehingga dapat
disimpulkan bahwa algoritma PRoFIFEA dengan tingkat keberagaman
individu yang lebih banyak diharapkan mampu mencegah kemungkinan
terjebak dalam local optimal lebih baik dibandingkan algoritma FEA maupun
algoritma genetika.
91
7.2. Saran
Penelitian yang telah dilakukan masih memiliki banyak kekurangan yang
perlu diperbaiki sehingga membutuhkan saran untuk perbaikan. Beberapa saran
yang untuk penelitian lebih lanjut adalah sebagai berikut:
1. Penelitian ini menggunakan model penentuan ukuran populasi PRoFIGA,
diharapkan pada penelitian selanjutnya dapat digunakan model penentuan
ukuran populasi yang lain.
2. Diharapkan adanya penelitian lain terkait mendeteksi pencegahan
konvergensi dini dari algoritma genetika.
3. Diharapkan ada penelitian lain terkait nilai batasan yang tepat untuk
parameter-parameter masukan sehingga hasil solusi lebih optimum.

