1.1  LATAR BELAKANG
Pada masa sekarang ini, perkembangan teknologi piranti elektronika
dan protokol komunikasi serta informasi, telah membawa kita menuju ke
teknologi generasi baru yang canggih. Salah satunya bidang teknologi
komunikasi yang meliputi perangkat keras seperti komputer, alat elektronik yang
semakin banyak digemari oleh para penggunanya, karena dapat memenuhi
kebutuhan akan sesuatu yang nyaman dan efisien. Telepon seluler (ponsel)
mempunyai kelebihan yang bisa dibawa kemana-mana dan dapat saling
berkomunikasi dengan cepat tanpa dibatasi ruang atau posisi dimana seseorang
itu berada. Tentunya dengan catatan selama di dalam area operator ponsel itu
sendiri. Sehingga tak diragukan lagi, ponsel memang sangat penting sekali
keberadaannya. Salah satu indikasi tersebut adalah munculnya layanan seperti
pesan data pendek atau Short Message Service (SMS) pada sistem GSM. Dengan
memanfaatkan perkembangan teknologi adanya Short Message Service
(SMS) diharapkan akan dapat memberikan kemudahan bagi pihak pengguna
agar dapat difungsikan sebagai pemantau dan pengendali suatu kondisi yang
ada di suatu ruangan secara langsung tanpa harus terjun langsung ke lokasi.
Upaya yang tepat untuk memenuhi harapan ini adalah membuat suatu sistem
pemantauan atau monitoring dan pengendalian jarak jauh dengan
menggunakan sistem GSM  yang menggunakan sensor suhu SHT11 yang ada
di dalam ruangan agar dapat dipantau secara langsung dari mana saja dengan
memanfaatkan piranti bergerak (mobile device) yakni HP atau PDA dengan
menggunakan fasilitas SMS (Short Message Service).
2
1.2 RUMUSAN MASALAH
Berdasarkan latar belakang maka dapat diambil rumusan masalah yaitu
bagaimana membuat sistem pengendalian dan pemantauan suatu ruangan yang
dapat diakses melalui fasilitas SMS (Short Message Service) serta ditampilkan
pada LCD yang meliputi pemantauan dan pengendalian suhu.
1.3 TUJUAN PENELITIAN
Merancang dan mengimplementasikan purwarupa sistem pemantauan dan
pengendalian suhu dari sensor suhu SHT11 dengan pengiriman dan penerimaan
data suhu berbasis Short Message Service (SMS) serta dapat ditampilkan pada
komputer dengan menggunakan Visual Basic 6.0.
1.4 BATASAN MASALAH
Batasan masalah yang digunakan dalam tugas akhir ini adalah :
1. LDR adalah sensor yang digunakan untuk mendeteksi intensitas cahaya.
2. SHT 11 adalah sensor yang digunakan untuk mendeteksi suhu dan
kelembaban.
3. Sistem memiliki set point antara suhu 25�C sampai 34 �C, karena rata-rata
suhu ruangan di Indonesia sekitar 25�C - 34 �C.
1.5 METODOLOGI PENELITIAN
Metode-metode yang digunakan dalam melakukan penelitian ini adalah
sebagai berikut:
1. Melakukan kajian dan pembelajaran lebih lanjut tentang sistem yang dibahas
pada penelitian ini dengan metode:
2. Studi Literatur, yaitu mempelajari artikel, makalah, jurnal, karya tulis, serta
buku-buku yang terkait dengan topik yang dibahas, untuk kemudian dijadikan
sebagai acuan dan referensi dalam merancang dan membuat penelitian ini.
3. Membuat perancangan sistem yang terdiri dari dua bagian, yaitu:
a. Perangkat Keras
3
Meliputi pembuatan desain rangkaian skematik dan PCB slave dan master
dengan perangkat lunak Eagle 5.1.0.
b.  Perangkat Lunak
Meliputi pembuatan program untuk mikrokontroler AVR ATMega16
dengan menggunakan perangkat lunak Bascom (Basic Compiler) AVR
dan antarmuka pada PC (Personal Computer) sebagai penerima data
hasil pengiriman data suhu dari SMS dengan menggunakan Visual Basic
6.0.
4. Setelah sistem dirancang, selanjutnya direalisasikan secara keseluruhan yang
meliputi beberapa bagian, yaitu 1 buah sensor SHT11, 1 buah sensor LDR
dan rangkaian komunikasi serial untuk mengoneksikan mikrokontroler
dengan HP Siemens C55  sebagai alat yang akan mengirim data suhu  melalui
layanan SMS. Sistem yang telah dibuat, diujicobakan dan dianalisa dengan
mengintegrasikan semua bagian secara keseluruhan, untuk memastikan
bahwa sistem telah dapat bekerja dengan baik dan hasilnya sesuai dengan
yang diinginkan.
1.6 SISTEMATIKA PENULISAN
Sistematika pembahasan dalam penyusunan laporan Tugas Akhir ini
adalah sebagai berikut :
BAB I : PENDAHULUAN
Berisi latar belakang pembuatan, tujuan, batasan masalah yang dikerjakan dan
sistematika pembahasan.
BAB II : TINJAUAN PUSTAKA
Memuat uraian sistematis tentang informasi hasil penelitian yang disajikan dalam
pustaka dan menghubungkannya dengan masalah penelitian yang sedang diteliti.
4
BAB III : DASAR TEORI
Bab ini berisi tentang penjelasan dan pembahasan dasar teori yang meliputi:
sensor suhu SHT11, sensor cahaya LDR, mikrokontroler AVR ATMega16,
komunikasi serial, penampil LCD,  dan sistem SMS.
BAB IV: ANALISA DAN RANCANGAN SISTEM
Bab ini berisi analisa dan perancangan sistem yang terdiri atas bagian perangkat
keras serta bagian perangkat lunak, yaitu program untuk mikrokontroler dengan
perangkat lunak Bascom AVR dan antarmuka sebagai penampil dengan perangkat
lunak Visual Basic 6.0 yang dikirim melalui SMS.
Merupakan proses pengujian masing masing perangkat keras dan perangkat lunak.
Bab ini berisi pembahasan mengenai cara kerja keseluruhan alat
Bab ini berisi kesimpulan atas penelitian yang telah dilakukan serta memberikan
saran untuk pengembangan sistem lebih lanjut.

7.1  Kesimpulan
Berdasarkan hasil ujicoba pada penelitian ini, maka dapat diambil
kesimpulan sebagai berikut:
1. Purwarupa sistem pemantau dan pengendalian suhu suatu ruangan berbasis
Short Message Service (SMS) dengan ATMega16, telah
diimplementasikan dengan baik sesuai dengan perancangan sistem.
2. Menampilkan data suhu, kelembaban, LDR, kipas, hairdryer tersebut pada
LCD, dan kemudian mengirimkan data tersebut melalui SMS dengan
format PDU.
3. Sistem dapat menyimpan data set point pada EEPROM, sehingga jika
pada saat mati listrik data set point yang sebelumnya dimasukkan tidak
hilang.
7.2  Saran
Dari hasil tugas akhir ini masih banyak kekurangan dan dimungkinkan untuk
dilakukan pengembangan lebih lanjut. Diantaranya adalah :
1. Sistem ini mengirimkan data suhu dan mengatur suhu sesuai dengan
keinginan. Untuk pengembangan lebih lanjut diharapkan sistem ini dapat
mengirimkan data dengan waktu tertentu
2. Perlu dilakukan studi lebih lanjut agar HP dapat berfungsi lama tanpa
harus memperhatikan kondisi baterai.
3. Kendali hanya menggunakan on/off. Jika ingin lebih baik maka selain itu
dapat digunakan sistem kendali yang lebih baik.

