1.1 Latar Belakang
Perkembangan teknologi dari zaman ke zaman hingga sampai saat ini
sangat pesat. Masyarakat cenderung menggunakan teknologi untuk menunjang
kinerja dan kebutuhan manusia. Segala macam teknologi menunjukan
perkembangan yang sangat cepat. Perkembangan teknologi ini berimbas pada
penggunaan alat bantu manusia untuk menjadikan pekerjaan manusia menjadi
lebih ringan. Karena tuntutan pekerjaan yang semakin banyak dan desakan untuk
lebih cepat dalam pengerjaannya maka teknologi menjadi solusi dalam
menghadapi tuntutan tersebut. Dengan menggunakan teknologi pula seseorang
dapat menerima informasi dengan cepat dan jelas, seperti papan dot matrix yang
menampilkan informasi secara menarik sehingga orang lebih cepat menerima
informasi yang ditulis dalam papan dot matrix.
Sistem papan dot matrix tersebut banyak digunakan sebagai sarana
informasi atau himbauan, salah satu diantaranya digunakan sebagai himbauan
kepada perokok untuk tidak merokok disembarang tempat. Himbauan ini
dilakukan untuk melakukan proteksi terhadap masyarakat mengingat bahaya
rokok yang sangat menganggu kesehatan dan pencemaran udara. Karena minim
akan pemberitahuan yang dilakukan maka perokok tidak mengerti akan tempat-
tempat bebas rokok, sehingga para perokok bebas untuk merokok disembarang
tempat tanpa memikirkan orang lain yang ada di sekitar area tersebut.
Dengan demikian penulis membuat purwarupa sistem deteksi bara rokok
dengan menggunakan sensor api berbasis ATMega32. Alat ini dibuat untuk
digunakan sebagai deteksi api atau bara rokok dengan mengunakan tampilan
papan dot matrix sehingga apa bila terdeteksi orang merokok, papan dot matrix
akan memberikan pemberitahuan berupa tulisan �DANGER.�. Papan dot matrix
digunakan untuk menarik perhatian masyarakat sehingga masyarakat akan lebih
2
memahami dan mematuhi himbauan yang ditampilkan dalam papan dot matrix.
Alat ini dilengkapi dengan buzzer yang digunakan sebagai indikator terdeteksinya
orang merokok sekaligus panggilan bagi para petugas keamanan untuk melakukan
pengamanan area yang telah dipasang alat ini.
1.2 Rumusan Masalah
Berdasarkan latar belakang yang telah diuraikan diatas maka dalam
perumusan masalah ini dapat dijelaskan tentang bagaimana suatu sistem deteksi
rokok dapat digunakan di dalam ruangan dengan indikasi orang merokok
menyalakan korek api dan dari terdeteksinya api dari korek gas tersebut maka
akan ditampilkan himbauan berupa larangan merokok di dalam ruangan tersebut.
Dari beberapa masalah yang diuraikan diatas maka perlu dibuat sebuah sensor api
sebagai deteksi rokok dan papan dot matrix sebagai penampil himbauan larangan
untuk tidak merokok di dalam ruangan.
1.3 Tujuan Penelitian
Adapun tujuan dibuat alat ini adalah sebagai berikut :
1. Dengan dibuatnya alat ini maka diharapkan publik dapat menerima informasi
dan himbauan lewat papan dot matrix bahwa area ini merupakan kawasan
bebas rokok.
2. Dengan adanya sistem sensor dan buzzer pada alat ini maka diharapkan para
petugas keamanan di area tersebut akan sigap untuk mengamankan perokok
yang tidak mengikuti himbauan pada papan dot matrix.
3. Dengan adanya alat ini diharapkan akan mengurangi polusi yang ditimbulkan
oleh rokok di tempat bebas rokok.
1.4 Batasan Masalah
Karena kompleksnya masalah yang ada di lapangan, maka penulis perlu
memberikan batasan dalam penelitian ini. Adapun batasan masalah dalam
penelitian ini meliputi :
3
1. Area kerja alat ini hanya digunakan di dalam ruangan tertutup dengan
intensitas cahaya matahari yang kecil.
2. Sensor ini mendeteksi api dari korek gas yang dinyalakan perokok sebagai
indikasi orang akan merokok.
3. Kemampuan sudut pengukuran sensor api ini hanya 60 derajat dengan
jangkauan maksimal sensor 100cm.
1.5 Metodologi Penelitian
Metodologi yang dilakukan dalam penelitian dan penulisan tugas akhir ini
adalah sebagai berikut:
1. Studi literatur:
? Memahami, mempelajari flame sensor dan keluarannya, melalui
serangkaian percobaan sehingga diketahui karakteristik dari flame sensor
ini.
? Memahami data masukkan sensor dengan data analog dan diubah
menjadi data digital.
? Memahami BASCOM AVR (basic compiler) untuk program dot matrix
dan ADC flame sensor.
? Memahami aksi reaksi dari sistem deteksi rokok ini.
2. Perancangan alat meliputi:
? Membuat rangkaian sistem minimum ATMega32 untuk program Dot
Matrix dan flame sensor.
? Membuat Dot Matrix dengan menggunakan led yang disusun sebanyak 6
karakter.
? Membuat Driver Dot Matrix dengan menggunakan IC HCF 4017BE dan
ULN2803APG.
? Membuat program dot matrix untuk mikrokontroler ATMega32 dengan
menggunakan bahasa BASCOM AVR (Basic Compiler AVR).
? Membuat program ADC untuk flame sensor.
? Membuat casing dari akrilik sebagai finishing alat.
4
3. Implementasi dan pengujian alat meliputi :
? Menyusun rangkaian sistem minimum ATMega32, purwarupa sistem
deteksi api (bara rokok), dan mendownload program ke mikrokontroler.
? Menguji alat meliputi pengecekan teks Dot Matrix dan data masuk dari
flame sensor apakah sudah sesuai dengan text yang di tuliskan dan nilai
yang di tangkap flame sensor pada program BASCOM AVR (Basic
Compiler AVR).
? Menguji sensor dengan memvariasikan jarak pengukuran rokok dengan
flame sensor.
? Menguji aksi dari terdeteksinya rokok dengan menyalakan buzzer.
4. Menganalisa data hasil pengujian alat.
1.6 Sistematika Penuliasan
Laporan penelitian tugas akhir ini disusun dengan sistematika sebagai
berikut:
? BAB I : PENDAHULUAN
Meliputi latar belakang dan permasalahan, rumusan masalah, tujuan
dan manfaat penelitian, batasan masalah, metodologi, dan sistematika
penulisan.
? BAB II : TINJAUAN PUSTAKA
Memuat tentang informasi-informasi tentang hasil penelitian yang
telah dilakukan terkait dengan perencangan sistem deteksi rokok
dengan mikrokontroler ATMega32.
? BAB III : LANDASAN TEORI
Memuat tentang landasan teori setiap kompoen yang menunjang dalam
pembuatan dan  pembahasan tugas akhir ini.
5
? BAB IV : ANALISIS DAN PERANCANGAN SISTEM
Memuat analisa dan perancangan sistem perangkat keras dan perangkat
lunak sistem.
? BAB V : IMPLEMENTASI
Memuat uraian tentang implementasi sistem secara detail sesuai dengan
rancangan dan berdasarkan komponen serta bahasa pemprograman
yang dipakai serta penjelasan ilmiah, yang secara logis dapat
menerangkan alasan diperolehnya hasil data dari penelitian.
Membahas tentang hasil pengujian sistem yang dilakukan meliputi
pengamatan hasil dari kinerja sistem.
Berisi kesimpulan yang memuat uraian singkat tentang hasil penelitian
yang diperoleh sesuai dengan tujuan penelitian, serta sarana untuk
penelitian lebih lanjut.

7.1 KESIMPULAN
Setelah melakukan penelitian dan pengujian, maka dapat diambil
beberapa kesimpulan, yaitu:
a. Hasil dari pengukuran yang dilakukan, sensor api ini mampu mendeteksi api
sejauh 100 cm dengan sudut pengukuran 60 derajat.
b. Sensor ini hanya dapat digunakan pada ruangan tertutup karena pada ruangan
terbuka sensor akan terpengaruh oleh cahaya matahari.
c. Purwarupa sistem deteksi rokok dengan menggunakan sensor api berbasis
ATMega32 ini dapat berjalan dengan baik sesuai dengan keinginan penulis.
Sistem ini dapat memberikan informasi melewati papan  dot matrix bahwa di
area sekitar terdeteksi api atau tidak.
d. Sistem ini dapat membantu publik untuk mendapatkan informasi area sekitar
terdeteksi api(bara rokok) atau tidak dengan mudah, cepat dan dalam kemasan
yang menarik yakni dalam bentuk tulisan berjalan.
7.2 SARAN
Setelah menguji sistem yang dibuat, maka terdapat beberapa kekurangan
dari sistem ini, maka untuk penelitian lebih lanjut mengenai sistem deteksi api
(bara rokok) dengan papan informasi dot matrix, ada beberapa saran yang dapat
dilakukan, antara lain:
a. Untuk ketelitian dalam  mendeteksi  api, maka dapat ditambahkan dengan
sensor asap.
b. Perlu adanya sensor yang jangkauannya lebih panjang dan memiliki sudut
pembacaan api yang lebih lebar sehingga area yang dapat dideteksi lebih
luas.
c. Diperlukan rangkaian penguat agar bara rokok dapat terdeteksi lebih baik
dengan jangkauan yang lebih jauh, karena bara rokok hanya dapat
terdeteksi dalam jarak yang dekat serta memiliki nilai yang kecil sehingga
perlu adanya penguatan.
d. Bila ditempatkan dalam ruangan yang luas maka dibutuhkan sensor yang
lebih banyak.

